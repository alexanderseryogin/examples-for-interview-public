<?php

use App\Models\User;
use App\Models\Role;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = \App\Models\Role::all('name')->pluck('name')->toArray();

        factory(\App\Models\User::class, 50)->create()->each(function ($user) use ($roles) {
            $user->account()->save(factory(\App\Models\Account::class)->make());
            $user->assignRole(array_random($roles));
        });

        // $user = User::first();
        // $role = Role::first();
        // $user->assignRole($role->name);
    }
}
