<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Team::class, 50)->create();
    }
}
