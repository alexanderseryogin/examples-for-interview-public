<?php

use Illuminate\Database\Seeder;
use App\Models\EventInfoBlock;

/**
 * Class EventsTableSeeder
 */
class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Event::class, 200)->create()->each(function ($event) {
            foreach (EventInfoBlock::getTypesTitles() as $type => $title) {
                factory(\App\Models\EventInfoBlock::class)
                    ->create([
                        'type'     => $type,
                        'title'    => $title,
                        'event_id' => $event->id,
                    ]);
            }
        });
    }
}
