<?php

use Illuminate\Database\Seeder;
use App\Code;
use Carbon\Carbon;

class CodeTableSeeder extends Seeder
{
    const CODE_IMPORT_FILE_PATH = 'import/usg2019/Ultra 2018 Unique Code.csv';

    protected $table = 'codes';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ($this->command->confirm("Do you wish to clear table '{$this->table}' before seeding?")) {

            DB::table($this->table)->truncate();

            $this->command->line('Old Codes cleared, starting from blank table.');
        }

        foreach ($this->get_csv_data() as $code) {
            Code::create([
                'code'        => $code,
                'expired_at'  => Carbon::parse('+1 year'),
            ]);
        }
    }

    public function get_csv_data($withoutHeader = true)
    {
        $file_n = resource_path(self::CODE_IMPORT_FILE_PATH);
        $file = fopen($file_n, "r");
        $codes = [];
        while ($line = fgetcsv($file, 200, ",")) {
            if ($withoutHeader) {
                $withoutHeader = false;
                continue;
            }

            $codes = array_merge($codes, $line);
        }
        fclose($file);

        return $codes;
    }
}
