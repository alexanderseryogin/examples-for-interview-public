<?php

use App\Models\Currency;
use Illuminate\Database\Seeder;

/**
 * Class CurrenciesTableSeeder
 */
class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = [
            ['name' => 'Singapore Dollar', 'code' => 'SGD', 'symbol' => '$'],
            ['name' => 'Malaysia Ringgit', 'code' => 'MYR', 'symbol' => 'RM'],
            ['name' => 'US Dollar', 'code' => 'USD', 'symbol' => '$'],
            ['name' => 'Indonesian Rupiah', 'code' => 'IDR', 'symbol' => 'Rp'],
            ['name' => 'Chinese Renminbi', 'code' => 'CNY', 'symbol' => '¥'],
            ['name' => 'Peso', 'code' => 'PHP', 'symbol' => '$'],
            ['name' => 'Japanese Yen', 'code' => 'JPY', 'symbol' => '¥'],
            ['name' => 'Aussie Dollar', 'code' => 'AUD', 'symbol' => '$'],
            ['name' => 'Thai Baht', 'code' => 'THB', 'symbol' => '฿'],
            ['name' => 'Korean Won', 'code' => 'KBW ', 'symbol' => '₩'],
        ];

        foreach ($currencies as $currency) {
            Currency::firstOrCreate($currency);
        }
    }
}