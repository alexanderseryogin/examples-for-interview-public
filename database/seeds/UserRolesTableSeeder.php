<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use App\Models\Role;

/**
 * Class UserRolesTableSeeder
 */
class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::firstOrCreate(['name' => 'Super Admin', 'guard_name' => 'web']);
        Role::firstOrCreate(['name' => 'Company Admin', 'guard_name' => 'web']);
        Role::firstOrCreate(['name' => 'Promoter', 'guard_name' => 'web']);
        Role::firstOrCreate(['name' => 'Event Organizer', 'guard_name' => 'web']);

        $user = User::first();
        $user->assignRole('Super Admin');
    }
}