<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

/**
 * Class PermissionsTableSeeder
 */
class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ids = Permission::all()->pluck('id');

        if ($ids) {
            Permission::destroy($ids);
        }

        $permissions = [
            Permission::ROLE_ASSIGNMENT_REMOVAL      => 'Role assignment & removal',
            Permission::CHANGE_USER_ACCOUNT_STATUSES => 'Change user account statuses',
            Permission::GLOBAL_USERS_MANAGEMENT      => 'User management and invitations',
            Permission::GLOBAL_EVENTS_MANAGEMENT     => 'Global Events Management',
            Permission::GLOBAL_VENUES_MANAGEMENT     => 'Manage venues',
            Permission::GLOBAL_TICKETS_MANAGEMENT    => 'Tickets management',
        ];

        $adminRole = Role::where(['name' => Role::SUPER_ADMIN])->first();

        foreach ($permissions as $name => $description) {
            $permission = Permission::firstOrCreate(['name' => $name, 'description' => $description]);
            $adminRole->givePermissionTo($permission);
        }
    }
}
