<?php

use Illuminate\Database\Seeder;
use App\Models\EventCategory;

/**
 * Class EventCategoriesTableSeeder
 */
class EventCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ids = EventCategory::all()->pluck('id');

        if ($ids) {
            EventCategory::destroy($ids);
        }

        $categories = [
            'Concerts',
            'Festivals',
            'Nightlife',
            'Exhibition & Conference',
            'Theatre & Performing Arts',
            'Theme Parks ',
            'Sports',
            'Esports'
        ];

        foreach ($categories as $category) {
            EventCategory::create([
                'slug' => str_slug($category),
                'name' => $category
            ]);
        }
    }
}
