<?php

use Illuminate\Database\Seeder;

use App\Models\Ticket;

use Faker\Factory as Faker;

class TicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $tickets = [
            [
                'concert_id' => 1,
                'title' => 'Pen A',
                'price' => 1018.00,
                'stock' => 250,
                'description' => "<ul>
                                    <li>General admission</li>
                                    <li>Free standing</li>
                                </ul>"
            ],
            [
                'concert_id' => 1,
                'title' => 'Pen B',
                'price' => 681.00,
                'stock' => 250,
                'description' => "<ul>
                                    <li>General admission</li>
                                    <li>Free standing</li>
                                </ul>"
            ],
            [
                'concert_id' => 1,
                'title' => 'PEN A + 20% AIRASIA PROMO CODE',
                'price' => 1018.00,
                'stock' => 250,
                'description' => "<ul>
<li>General admission</li>
<li>Free standing</li>
<li>Comes with AirAsia Promo Code for discounted flights</li>
</ul>"
            ],
            [
                'concert_id' => 1,
                'title' => 'PEN B + 20% AIRASIA PROMO CODE',
                'price' => 681.00,
                'stock' => 250,
                'description' => "<ul>
<li>General admission</li>
<li>Free standing</li>
<li>Comes with AirAsia Promo Code for discounted flights</li>
</ul>"
            ],

        ];

        foreach ($tickets as $ticket) {
            Ticket::create($ticket);
        }

    }
}
