<?php

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(UserRolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(CouponsTableSeeder::class);
        $this->call(TicketcodesTableSeeder::class);
        $this->call(FlightTicketPurchaseSeeder::class);
        $this->call(CodeTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(EventCategoriesTableSeeder::class);
        $this->call(GenresTableSeeder::class);
        $this->call(VenuesTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(TeamsTableSeeder::class);
        $this->call(EventsTableSeeder::class);        
    }
}
