<?php

use App\Models\Genre;
use Illuminate\Database\Seeder;

/**
 * Class GenresTableSeeder
 */
class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ids = Genre::all()->pluck('id');

        if ($ids) {
            Genre::destroy($ids);
        }

        $genres = [
            'Adventure',
            'Alternative',
            'Animation',
            'Anime',
            'Automotive',
            'Aviation',
            'Badminton',
            'Baseball',
            'Baseball',
            'Basketball',
            'Blues',
            'Boxing',
            'Buddhism',
            'Business',
            'Careers',
            'Children\'s',
            'Christianity',
            'Classic Rock',
            'Classical',
            'Comedy',
            'Conference',
            'Country',
            'Cricket',
            'Dance',
            'DJ',
            'Drama',
            'EDM',
            'Electronic',
            'Fashion & Beauty',
            'Finance',
            'Fitness',
            'Folk',
            'Food',
            'Football',
            'Formula 1',
            'Gospel',
            'Heavy Metal',
            'Higher Education',
            'Hinduism',
            'Hip Hop',
            'History',
            'Hockey',
            'Holiday',
            'Indie',
            'Investing',
            'Islam',
            'J-Pop',
            'Jazz',
            'Judaism',
            'K-Pop',
            'Kayokyoku',
            'Latino',
            'Meet & Greet',
            'MMA',
            'Motorsports',
            'Moto GP',
            'Musicals',
            'New Age',
            'Non-Profit',
            'Oldies',
            'Opera',
            'Party',
            'Pop',
            'R&B',
            'Race',
            'Rap',
            'Reggae',
            'Rock',
            'Rugby',
            'Running',
            'Sci-Fi & Fantasy',
            'Seminar',
            'Soccer',
            'Soul',
            'Spirituality',
            'Teens',
            'Travel',
            'Triatholon',
            'Workshop',
            'World',
            'Yoga',
            'Trance',
            'Afro Beats',
            'Table Tennis',
            'Squash',
            'Swimming',
            'Weightlifting',
            'Volleyball',
            'Surfing',
            'Cycling'
        ];

        foreach ($genres as $genre) {
            Genre::create([
                'name' => $genre
            ]);
        }
    }
}
