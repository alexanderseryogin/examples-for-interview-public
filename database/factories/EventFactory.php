<?php

use Faker\Generator as Faker;

$users = \App\Models\User::all(['id', 'company_id']);
$venues = \App\Models\Venue::all('id')->pluck('id')->toArray();
$organizers = \App\Models\Company::all('id')->pluck('id')->toArray();
$categories = \App\Models\EventCategory::all('id')->pluck('id')->toArray();
$statuses = \App\Models\Event::getStatuses();

$factory->define(App\Models\Event::class, function (Faker $faker) use ($users, $venues, $categories, $organizers, $statuses) {
    $creator = $faker->randomElement($users);

    return [
        'name'                  => $faker->sentence,
        'creator_id'            => $creator->id,
        'company_id'            => $creator->company_id,
        'venue_id'              => $faker->randomElement($venues),
        'organizer_id'          => $faker->randomElement($organizers),
        'status'                => $faker->randomElement($statuses),
        'category_id'           => $faker->randomElement($categories),
        'slug'                  => $faker->slug,
        'start_date'            => $faker->dateTimeInInterval('now', '+ 5 days'),
        'end_date'              => $faker->dateTimeInInterval('+ 5 days', '+ 10 days'),
        'is_estimated_end_time' => $faker->randomElement([0, 1]),
        'show_countdown'        => $faker->randomElement([0, 1]),
        'custom_date'           => $faker->dateTimeInInterval('+ 5 days', '+ 10 days'),
        'description'           => $faker->text,
        'published_at'          => $faker->dateTimeThisMonth,
        'created_at'            => $faker->dateTimeThisMonth,
        'updated_at'            => $faker->dateTimeThisMonth
    ];
});