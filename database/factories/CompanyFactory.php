<?php

use Faker\Generator as Faker;

$types = \App\Models\Company::getTypes();

$factory->define(App\Models\Company::class, function (Faker $faker) use ($types){
    return [
        'name' => 'Company: ' . str_replace('.', '', $faker->unique()->text(15)),
        'type' => $faker->randomElement($types),
        'logo' => $faker->imageUrl(300, 300, 'cats')
    ];
});