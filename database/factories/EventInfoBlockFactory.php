<?php

use Faker\Generator as Faker;

$factory->define(App\Models\EventInfoBlock::class, function (Faker $faker) {
    return [
        'type'       => $faker->slug,
        'title'      => $faker->sentence,
        'is_visible' => 1
    ];
});