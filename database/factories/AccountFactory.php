<?php

use Faker\Generator as Faker;

$countries = \App\Models\Country::all('id')->pluck('id')->toArray();
$companies = \App\Models\Company::all('id')->pluck('id')->toArray();


$factory->define(\App\Models\Account::class, function (Faker $faker) use ($countries, $companies) {
    return [
        'first_name'  => $faker->firstName,
        'last_name'   => $faker->lastName,
        'email'       => $faker->email,
        'avatar'      => $faker->imageUrl(),
        'address1'    => $faker->streetName,
        'address2'    => $faker->buildingNumber,
        'city'        => $faker->city,
        'state'       => $faker->state,
        'postal_code' => $faker->postcode,
        'country_id'  => $faker->randomElement($countries),
        'company_id'  => $faker->randomElement($companies),
        'is_active'   => $faker->optional($weight = 0.9, $default = 0)->numberBetween(1, 1),
        'is_banned'   => $faker->optional($weight = 0.3, $default = 0)->numberBetween(1, 1),
    ];
});
