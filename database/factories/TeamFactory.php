<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Team::class, function (Faker $faker) {
    return [
        'name' => 'Team: ' . str_replace('.', '', $faker->unique()->text(15)),
    ];
});