<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\User::class, function (Faker $faker) {
    return [        
        'email'    => $faker->unique()->safeEmail,
        'password' => \Illuminate\Support\Facades\Hash::make('123456'),
    ];
});
