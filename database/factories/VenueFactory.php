<?php

use Faker\Generator as Faker;

$countries = \App\Models\Country::all('id')->pluck('id')->toArray();
$users = \App\Models\User::all(['id', 'company_id']);

$factory->define(App\Models\Venue::class, function (Faker $faker) use ($countries, $users) {
    $creator = $faker->randomElement($users);

    return [
        'name'       => $faker->company,
        'address1'   => $faker->streetName,
        'address2'   => $faker->buildingNumber,
        'city'       => $faker->city,
        'state'      => $faker->state,
        'postcode'   => $faker->postcode,
        'country_id' => $faker->randomElement($countries),
        'capacity'   => $faker->numberBetween(200, 5000),
        'creator_id' => $creator->id,
        'company_id' => $creator->company_id,
    ];
});