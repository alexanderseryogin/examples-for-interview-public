<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('participant1_name');
            $table->string('participant2_name');
            $table->string('participant1_flag');
            $table->string('participant2_flag');
            $table->string('group');
            $table->integer('cat_B');
            $table->integer('cat_C');
            $table->integer('cat_D');
            $table->string('venue');
            $table->dateTime('date');
            $table->string('map');
            $table->boolean('hidden')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
