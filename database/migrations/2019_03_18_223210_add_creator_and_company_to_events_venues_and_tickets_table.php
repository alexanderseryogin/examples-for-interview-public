<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class AddCreatorAndCompanyToEventsVenuesAndTicketsTable
 */
class AddCreatorAndCompanyToEventsVenuesAndTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->unsignedInteger('company_id')->nullable()->after('creator_id');
            $table->foreign('company_id')->references('id')->on('companies')
                ->onDelete('set null')->onUpdate('cascade');
        });

        $events = DB::table('events')->get();
        $users = DB::table('users')->get()->keyBy('id');
        $company = DB::table('companies')->get()->first();

        foreach ($events as $event) {
            $companyId = $users[$event->creator_id]->company_id ?: $company->id;

            DB::table('events')
                ->where(['id' => $event->id])
                ->update(['company_id' => $companyId]);
        }

        Schema::table('tickets', function (Blueprint $table) {
            $table->unsignedInteger('creator_id')->nullable()->after('event_id');
            $table->unsignedInteger('company_id')->nullable()->after('creator_id');
            $table->foreign('creator_id')->references('id')->on('users')
                ->onDelete('set null')->onUpdate('cascade');
            $table->foreign('company_id')->references('id')->on('companies')
                ->onDelete('set null')->onUpdate('cascade');
        });

        $events = DB::table('events')->get();

        foreach ($events as $event) {
            DB::table('tickets')
                ->where(['event_id' => $event->id])
                ->update(['company_id' => $event->company_id, 'creator_id' => $event->creator_id]);
        }

        Schema::table('venues', function (Blueprint $table) {
            $table->unsignedInteger('creator_id')->nullable()->after('id');
            $table->unsignedInteger('company_id')->nullable()->after('creator_id');
            $table->foreign('creator_id')->references('id')->on('users')
                ->onDelete('set null')->onUpdate('cascade');
            $table->foreign('company_id')->references('id')->on('companies')
                ->onDelete('set null')->onUpdate('cascade');
        });

        $event = $events->first();

        // DB::table('venues')->update(['company_id' => $event->company_id, 'creator_id' => $event->creator_id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropForeign(['company_id']);
            $table->dropColumn(['company_id']);
        });

        Schema::table('tickets', function (Blueprint $table) {
            $table->dropForeign(['creator_id']);
            $table->dropForeign(['company_id']);
            $table->dropColumn(['creator_id', 'company_id']);
        });

        Schema::table('venues', function (Blueprint $table) {
            $table->dropForeign(['company_id']);
            $table->dropForeign(['creator_id']);
            $table->dropColumn(['creator_id', 'company_id']);
        });
    }
}
