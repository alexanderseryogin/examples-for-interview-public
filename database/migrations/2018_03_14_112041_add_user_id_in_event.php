<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdInEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->after('title')->nullable();
            $table->unsignedInteger('venue_id')->after('user_id')->nullable();
            $table->unsignedInteger('collection_id')->after('venue_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('venue_id');
            $table->dropColumn('collection_id');
        });
    }
}
