<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->unsignedInteger('organizer_id')->after('venue_id');
            $table->dropColumn([
                'collection_id',
                'buy_links',
                'is_online',
                'is_outlet',
                'is_archived',
                'type',
                'start_time',
                'end_time',
            ]);
            $table->renameColumn('date', 'start_date');
            $table->timestamp('end_date')->after('date')->nullable();
            $table->boolean('is_estimated_end_time')->after('end_date');
            $table->boolean('show_countdown')->after('is_estimated_end_time')->default(0);
            $table->timestamp('custom_date')->after('show_countdown')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn(['organizer_id']);
            $table->unsignedInteger('collection_id')->nullable();
            $table->string('buy_links')->nullable();
            $table->boolean('is_online')->default(0);
            $table->boolean('is_outlet')->default(0);
            $table->boolean('is_archived')->default(0);
            $table->string('type')->nullable();
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->renameColumn('start_date', 'date');
            $table->dropColumn(['end_date', 'is_estimated_end_time', 'show_countdown', 'custom_date']);
        });
    }
}
