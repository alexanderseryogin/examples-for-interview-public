<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->renameColumn('title', 'name');
            $table->renameColumn('stock', 'quantity');
            $table->dropColumn('sorting_id');
            $table->dropColumn('seat_plan_path');
            $table->dropColumn('text_color');
            $table->dropColumn('color');
            $table->dropColumn('barcode');
            $table->dropColumn('prefix');
            $table->dropColumn('suffix');
            $table->dropColumn('buy_links');
        });

        Schema::table('tickets', function (Blueprint $table) {
            $table->string('category')->nullable()->after('name');
            $table->string('tier')->nullable()->after('category');
            $table->dateTime('sales_end_date')->nullable()->after('tier');
            $table->unsignedInteger('currency_id')->nullable()->after('sales_end_date');
            $table->string('discount_type')->nullable()->after('quantity');
            $table->double('discount_amount')->nullable()->after('discount_type');
            $table->string('header_image')->nullable()->after('discount_amount');
            $table->string('seat_plan')->nullable()->after('header_image');
            $table->text('details')->nullable()->after('description');
            $table->integer('pos')->nullable()->after('details');
            $table->string('status')->nullable()->after('pos');

            $table->foreign('currency_id')->references('id')->on('currencies')
                ->onDelete('SET NULL')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropForeign(['currency_id']);
            $table->renameColumn('name', 'title');
            $table->renameColumn('quantity', 'stock');
            $table->dropColumn('pos');
            $table->dropColumn('seat_plan');
            $table->dropColumn('category');
            $table->dropColumn('tier');
            $table->dropColumn('sales_end_date');
            $table->dropColumn('currency_id');
            $table->dropColumn('discount_type');
            $table->dropColumn('discount_amount');
            $table->dropColumn('header_image');
            $table->dropColumn('details');
            $table->dropColumn('status');
        });

        Schema::table('tickets', function (Blueprint $table) {
            $table->integer('sorting_id')->after('id')->nullable();
            $table->string('color')->after('sorting_id')->nullable();
            $table->string('seat_plan_path')->after('color')->nullable();
            $table->string('text_color')->after('sorting_id')->nullable();
            $table->unsignedInteger('barcode')->after('event_id')->nullable();
            $table->string('buy_links')->after('title')->nullable();
            $table->string('prefix')->after('title')->nullable();
            $table->string('suffix')->after('prefix')->nullable();
        });
    }
}
