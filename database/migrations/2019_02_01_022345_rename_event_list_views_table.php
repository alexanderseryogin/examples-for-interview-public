<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameEventListViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_list_views', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::rename('event_list_views', 'list_views');

        Schema::table('list_views', function (Blueprint $table) {
            $table->string('type')->after('user_id');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('list_views', function (Blueprint $table) {
            $table->dropColumn(['type']);
            $table->dropForeign(['user_id']);
        });

        Schema::rename('list_views', 'event_list_views');

        Schema::table('event_list_views', function (Blueprint $table) {

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }
}
