<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameFieldsAddForeignKeysToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->renameColumn('user_id', 'creator_id');
            $table->renameColumn('title', 'name');
            $table->unsignedInteger('promoter_id')->nullable()->after('organizer_id');

            $table->foreign('promoter_id')->references('id')->on('users')
                ->onDelete('SET NULL')->onUpdate('cascade');
            $table->foreign('creator_id')->references('id')->on('users')
                ->onDelete('SET NULL')->onUpdate('cascade');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->unsignedInteger('organizer_id')->nullable()->change();
            $table->dropColumn('end_date');
            $table->dropColumn('status');
            $table->dropColumn('custom_date');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->unsignedInteger('organizer_id')->nullable()->change();
            $table->dateTime('end_date')->nullable()->after('start_date');
            $table->dateTime('custom_date')->nullable()->after('end_date');
            $table->string('status')->default('Draft')->after('image_mobile');
        });

        DB::table('events')
            ->where(['organizer_id' => 0])
            ->update(['organizer_id' => null]);

        Schema::table('events', function (Blueprint $table) {
            $table->foreign('organizer_id')->references('id')->on('users')
                ->onDelete('SET NULL')->onUpdate('cascade');
            $table->foreign('venue_id')->references('id')->on('venues')
                ->onDelete('SET NULL')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropForeign(['promoter_id']);
            $table->dropForeign(['creator_id']);
            $table->dropForeign(['organizer_id']);
            $table->dropForeign(['venue_id']);
            $table->renameColumn('creator_id', 'user_id');
            $table->renameColumn('name', 'title');
            $table->dropColumn('promoter_id');
            $table->dropColumn('end_date');
            $table->dropColumn('custom_date');
            $table->dropColumn('status');
        });

        DB::table('events')
            ->where(['organizer_id' => null])
            ->update(['organizer_id' => 0]);

        Schema::table('events', function (Blueprint $table) {
            $table->unsignedInteger('organizer_id')->change();
            $table->timestamp('end_date')->after('start_date')->nullable();
            $table->timestamp('custom_date')->after('show_countdown')->nullable();
            $table->string('status')->default('Draft')->after('category_id');
        });
    }
}
