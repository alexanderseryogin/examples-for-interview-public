<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('videos', function ($table) {
            $table->dropColumn('youtube_link');
        });

        Schema::table('videos', function ($table) {
            $table->string('youtube_id')->nullable()->after('event_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->dropColumn('youtube_id');
        });

        Schema::table('videos', function ($table) {
            $table->string('youtube_link')->nullable()->after('event_id');
        });
    }
}
