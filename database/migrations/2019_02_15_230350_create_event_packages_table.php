<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->string('packages_tab_name')->nullable()->after('info_video');
            $table->boolean('is_packages_tab_visible')->default(0)->after('packages_tab_name');
        });

        Schema::create('event_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id');
            $table->string('name');
            $table->string('card_size')->nullable();
            $table->string('card_image')->nullable();
            $table->unsignedInteger('currency_id')->nullable();
            $table->float('price');
            $table->string('call_to_action_link');
            $table->string('header_image')->nullable();
            $table->text('description')->nullable();
            $table->text('details')->nullable();
            $table->smallInteger('pos')->default(0);
            $table->boolean('is_visible')->default(1);
            $table->string('status');
            $table->timestamps();

            $table->foreign('event_id')->references('id')->on('events')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('currency_id')->references('id')->on('currencies')
                ->onDelete('SET NULL')->onUpdate('cascade');
        });

        Schema::create('event_package_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_package_id');
            $table->string('type');
            $table->string('title');
            $table->string('icon')->nullable();
            $table->text('content')->nullable();
            $table->smallInteger('pos')->default(0);
            $table->boolean('is_active')->default(1);
            $table->timestamps();

            $table->foreign('event_package_id')->references('id')->on('event_packages')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('packages_tab_name');
            $table->dropColumn('is_packages_tab_visible');
        });

        Schema::dropIfExists('event_package_blocks');
        Schema::dropIfExists('event_packages');
    }
}
