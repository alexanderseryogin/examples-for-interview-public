<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class MoveEventStatusToEventsTable
 */
class MoveEventStatusToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('status_id');
            $table->string('status')->after('collection_id')->default('Draft');
        });

        Schema::drop('statuses');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('value');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->unsignedInteger('status_id')->after('collection_id')->default(1)->nullable();
        });
    }
}
