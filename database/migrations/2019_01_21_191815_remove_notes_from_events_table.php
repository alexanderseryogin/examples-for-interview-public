<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveNotesFromEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            //additional_information
            $table->dropColumn([
                'additional_notes',
                'important_notes',
                'tickets_important_notes',
            ]);
            $table->renameColumn('additional_information', 'description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->renameColumn('description', 'additional_information');
            $table->text('additional_notes')->nullable();
            $table->text('important_notes')->nullable();
            $table->text('tickets_important_notes')->nullable();
        });
    }
}
