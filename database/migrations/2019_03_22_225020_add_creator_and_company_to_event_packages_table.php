<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddCreatorAndCompanyToEventPackagesTable
 */
class AddCreatorAndCompanyToEventPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_packages', function (Blueprint $table) {
            $table->unsignedInteger('creator_id')->nullable()->after('event_id');
            $table->unsignedInteger('company_id')->nullable()->after('creator_id');
            $table->foreign('creator_id')->references('id')->on('users')
                ->onDelete('set null')->onUpdate('cascade');
            $table->foreign('company_id')->references('id')->on('companies')
                ->onDelete('set null')->onUpdate('cascade');
        });

        $events = DB::table('events')->get();

        foreach ($events as $event) {
            DB::table('event_packages')
                ->where(['event_id' => $event->id])
                ->update(['company_id' => $event->company_id, 'creator_id' => $event->creator_id]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_packages', function (Blueprint $table) {
            $table->dropForeign(['creator_id']);
            $table->dropForeign(['company_id']);
            $table->dropColumn(['creator_id', 'company_id']);
        });
    }
}
