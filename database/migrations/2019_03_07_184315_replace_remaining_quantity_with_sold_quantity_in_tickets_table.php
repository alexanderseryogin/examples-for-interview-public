<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class ReplaceRemainingQuantityWithSoldQuantityInTicketsTable
 */
class ReplaceRemainingQuantityWithSoldQuantityInTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('remaining_quantity');
            $table->unsignedInteger('sold_quantity')->default(0)->after('quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('sold_quantity');
            $table->unsignedInteger('remaining_quantity')->default(0)->after('quantity');
        });
    }
}
