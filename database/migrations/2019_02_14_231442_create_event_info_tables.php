<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateEventInfoTables
 */
class CreateEventInfoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->string('info_tab_name')->nullable()->after('banner_mobile');
            $table->boolean('is_info_tab_visible')->default(0)->after('info_tab_name');
            $table->string('info_map_link')->nullable()->after('is_info_tab_visible');
            $table->string('info_video')->nullable()->after('info_map_link');
        });

        Schema::create('event_images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id');
            $table->string('category');
            $table->string('path');
            $table->timestamps();

            $table->foreign('event_id')->references('id')->on('events')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('event_info_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id');
            $table->string('type');
            $table->string('title');
            $table->unsignedInteger('pos')->default(0);
            $table->boolean('is_visible')->default(1);
            $table->timestamps();

            $table->foreign('event_id')->references('id')->on('events')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('event_quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id');
            $table->string('content');
            $table->timestamps();

            $table->foreign('event_id')->references('id')->on('events')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_images');
        Schema::dropIfExists('event_info_blocks');
        Schema::dropIfExists('event_quotes');

        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('info_tab_name');
            $table->dropColumn('is_info_tab_visible');
            $table->dropColumn('info_map_link');
            $table->dropColumn('info_video');
        });
    }
}
