<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class DropOrganizersTable
 */
class DropOrganizersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropForeign(['organizer_id']);
            $table->dropColumn('organizer_id');
        });

        Schema::dropIfExists('organizers');

        Schema::table('events', function (Blueprint $table) {
            $table->unsignedInteger('organizer_id')->nullable()->after('venue_id');
            $table->foreign('organizer_id')->references('id')->on('companies')
                ->onDelete('SET NULL')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropForeign(['organizer_id']);
            $table->dropColumn('organizer_id');
        });

        Schema::create('organizers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('logo')->nullable();
        });

        Schema::table('events', function (Blueprint $table) {
            $table->unsignedInteger('organizer_id')->nullable()->after('venue_id');
            $table->foreign('organizer_id')->references('id')->on('organizers')
                ->onDelete('SET NULL')->onUpdate('cascade');
        });
    }
}
