<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class UpdateDiscountsAddCoutdownFieldToTicketsTable
 */
class UpdateDiscountsAddCoutdownFieldToTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('discount_type');
            $table->renameColumn('discount_amount', 'discount_price');
            $table->integer('quantity')->nullable(true)->change();
            $table->boolean('has_discount')->default(0)->after('sold_quantity');
            $table->boolean('show_countdown')->default(0)->after('sales_end_date');
        });

        DB::table('tickets')->update(['discount_price' => null]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('has_discount');
            $table->dropColumn('show_countdown');
            $table->string('discount_type')->nullable()->after('quantity');
            $table->integer('quantity')->nullable(false)->change();
            $table->renameColumn('discount_price', 'discount_amount');
        });
    }
}
