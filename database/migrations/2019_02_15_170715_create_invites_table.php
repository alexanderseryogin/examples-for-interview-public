<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('token')->unique();
            $table->unsignedInteger('company_id')->nullable();
            $table->unsignedInteger('team_id')->nullable();
            $table->timestamps();
        });

        Schema::table('invites', function (Blueprint $table) {
            $table->foreign('company_id')->references('id')->on('companies')
                ->onDelete('set null');
            $table->foreign('team_id')->references('id')->on('teams')
                ->onDelete('set null');
        });

        Schema::create('event_invite', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id');
            $table->unsignedInteger('invite_id');
        });

        Schema::table('event_invite', function (Blueprint $table) {
            $table->foreign('event_id')->references('id')->on('events')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('invite_id')->references('id')->on('invites')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_invite');
        Schema::dropIfExists('invites');
    }
}
