<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddMaxPurchaseTicketsQuantityFieldToEventsTable
 */
class AddMaxPurchaseTicketsQuantityFieldToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->integer('max_purchase_tickets_quantity')->nullable(true)->after('show_countdown');
        });

        DB::table('events')->update(['max_purchase_tickets_quantity' => 8]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('max_purchase_tickets_quantity');
        });
    }
}
