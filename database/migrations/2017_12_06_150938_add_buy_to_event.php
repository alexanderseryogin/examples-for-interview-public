<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBuyToEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->string('buy_links')->after('title')->nullable();
            $table->boolean('is_online')->after('buy_links')->default(0)->nullable();
            $table->boolean('is_outlet')->after('is_online')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('buy_links');
            $table->dropColumn('is_online');
            $table->dropColumn('is_outlet');
        });
    }
}
