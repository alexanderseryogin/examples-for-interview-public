<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class MoveCompanyFromAccountsToUser
 */
class MoveCompanyFromAccountsToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('company_id')->nullable()->after('id');
            $table->foreign('company_id')->references('id')->on('companies')
                ->onDelete('set null');
        });

        $accounts = DB::table('accounts')->get();

        foreach ($accounts as $account) {
            DB::table('users')
                ->where(['id' => $account->user_id])
                ->update(['company_id' => $account->company_id]);
        }

        Schema::table('accounts', function (Blueprint $table) {
            $table->dropForeign(['company_id']);
            $table->dropColumn(['company_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->unsignedInteger('company_id')->nullable()->after('user_id');
            $table->foreign('company_id')->references('id')->on('companies')
                ->onDelete('set null');
        });

        $users = DB::table('users')->get();

        foreach ($users as $user) {
            DB::table('accounts')
                ->where(['user_id' => $user->id])
                ->update(['company_id' => $user->company_id]);
        }

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['company_id']);
            $table->dropColumn(['company_id']);
        });
    }
}
