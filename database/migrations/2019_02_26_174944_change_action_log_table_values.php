<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ActionLog;

class ChangeActionLogTableValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ActionLog::where('item_type', 'event')
            ->update(['item_type' => ActionLog::ITEM_EVENT]);
        ActionLog::where('item_type', 'venue')
            ->update(['item_type' => ActionLog::ITEM_VENUE]);
        ActionLog::where('item_type', 'role')
            ->update(['item_type' => ActionLog::ITEM_ROLE]);
        ActionLog::where('item_type', 'user')
            ->update(['item_type' => ActionLog::ITEM_USER]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        ActionLog::where('item_type', ActionLog::ITEM_EVENT)
            ->update(['item_type' => 'event']);
        ActionLog::where('item_type', ActionLog::ITEM_VENUE)
            ->update(['item_type' => 'venue']);
        ActionLog::where('item_type', ActionLog::ITEM_ROLE)
            ->update(['item_type' => 'role']);
        ActionLog::where('item_type', ActionLog::ITEM_USER)
            ->update(['item_type' => 'user']);
    }
}
