<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEventTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->time('start_time')->after('date')->default(0)->nullable();
            $table->time('end_time')->after('start_time')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('buy_links');
            $table->dropColumn('is_online');
            $table->dropColumn('is_outlet');
            $table->dropColumn('start_time');
            $table->dropColumn('end_time');
        });
    }
}
