<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeFieldsNullableInEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->boolean('is_estimated_end_time')->default(false)->after('custom_date')->change();
            $table->dateTime('start_date')->default(null)->after('slug')->nullable()->change();
            $table->text('description')->after('show_countdown')->nullable()->change();
            $table->string('image_desktop')->after('description')->nullable()->change();
            $table->string('image_mobile')->after('image_desktop')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->boolean('is_estimated_end_time')->after('custom_date')->change();
            $table->dateTime('start_date')->after('slug')->change();
            $table->text('description')->after('show_countdown')->change();
            $table->string('image_desktop')->after('description')->change();
            $table->string('image_mobile')->after('image_desktop')->change();
        });
    }
}
