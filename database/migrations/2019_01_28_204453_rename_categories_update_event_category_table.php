<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class RenameCategoriesUpdateEventCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('categories', 'event_categories');

        Schema::table('events', function (Blueprint $table) {
            $table->unsignedInteger('category_id')->nullable()->change();
        });

        DB::table('events')
            ->where(['category_id' => 0])
            ->update(['category_id' => null]);

        Schema::table('events', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('event_categories')
                ->onDelete('SET NULL')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('event_categories', 'categories');

        Schema::table('events', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
        });

        DB::table('events')
            ->where(['category_id' => null])
            ->update(['category_id' => 0]);

        Schema::table('events', function (Blueprint $table) {
            $table->unsignedInteger('category_id')->nullable(false)->change();
        });
    }
}
