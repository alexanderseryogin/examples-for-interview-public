<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->unsignedInteger('company_id')->nullable()->after('user_id');
            $table->unsignedInteger('team_id')->nullable()->after('company_id');
            $table->foreign('company_id')->references('id')->on('companies')
                ->onDelete('set null');
            $table->foreign('team_id')->references('id')->on('teams')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->dropForeign(['company_id']);
            $table->dropForeign(['team_id']);
            $table->dropColumn(['company_id', 'team_id']);
        });
    }
}
