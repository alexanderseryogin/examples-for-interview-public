@extends('layouts.email.index')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">DropCoffee Invoice</div>

                    <div class="panel-body">
                        Receipt email
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
