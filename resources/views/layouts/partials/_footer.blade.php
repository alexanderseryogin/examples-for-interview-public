<!-- Footer -->
<footer class="mainFooter">
    <div class="container">
    <div class="col-sm-7 col-xs-12">
        <div class="row footer-desc">
        <img class="rtLogo" src="/images/rugby2019/logo.png" alt="">
        <p>RedTix.com is the hottest, smartest new way to discover, discuss, review and book tickets to an international line-up of concerts, sporting events, musicals, theatre performances and more.</p>
        </div>
    </div>
    <div class="col-sm-offset-2 col-sm-3">
        <div class="row footer-desc">
        <p><strong>Quick Links</strong></p>
        <ul class="others-link list-unstyled">
            {{-- <li><a href="/ultrasingapore18">Register Ultra Flight</a></li> --}}
            <li><a href="/eventregistration">Register Your Details</a></li>
            <li><a href="/about">About RedTix</a></li>
            <li><a href="/faq">FAQ</a></li>
            <li><a href="/websiteterms">Website Terms &amp; Conditions</a></li>
            <li><a href="/purchaseterms">Purchase Terms &amp; Conditions</a></li>
            <li><a href="/privacy">Privacy</a></li>
        </ul>
        </div>
    </div>
    <div class="aaArea">
        <div class="footerBanner">
        <!-- <a target="_blank" href="http://www.airasia.com/"><img class="aaLogo" src="images/airasia_logo.png" alt=""></a>
        <p>©2007-2017 AirAsia Berhad. All rights reserved.</p> , a subsidiary of <a target="_blank" href="http://www.airasia.com/">AirAsia</a> Berhad.-->
        <p>RedTix Sdn. Bhd. (Co. No. 1096870 M). Phone +60 327-101-461 &copy;2007-2018 All rights reserved.<br/>Wisma Tune, Unit 19-05-04/05 Level 5, 19 Lorong Dungun Bukit Damansara, 50490 Kuala Lumpur, Malaysia</p>
        <div class="socialFooter">
            <a target="_blank" href="https://www.facebook.com/RedTix"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
            <a target="_blank" href="https://twitter.com/redtix"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
            <!-- <a target="_blank" href="https://www.youtube.com/user/airasiaredtix"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> -->
            <a target="_blank" href="https://www.youtube.com/channel/UCGs2OxKX4WQzmDGuwwNd9-g"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
            <a target="_blank" href="https://www.instagram.com/AirAsiaRedTix/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
        </div>
        </div>
    </div>
    </div>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


<!-- Featherlight lightbox -->
<script src="https://cdn.rawgit.com/noelboss/featherlight/1.7.7/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdn.rawgit.com/noelboss/featherlight/1.7.7/release/featherlight.gallery.min.js" type="text/javascript" charset="utf-8"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
{{-- {!! Html::script('js/app.js') !!} --}}
<script type="text/javascript" src="js/app.min.js"></script>
<script type="text/javascript" src="/js/scrollToFixed.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vue"></script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<!-- <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-587468fe5d4abf72"></script> -->

<script type="text/javascript">
    var register_newsletter = function(){
        var user_email = document.getElementById('mce-EMAIL').value;
        var user_name = user_email.split("@")[0];
        mixpanel.alias(user_email);
        mixpanel.people.set({
          "$name": user_name,
          "$email": user_email
        });
        mixpanel.identify(user_email);
        mixpanel.track('register_newsletter',
        {
            Email: user_email
        });
    }
</script>

@if (App::environment('production'))
<!-- Mixpanel last touch attributes -->
<script type="text/javascript">
    function getQueryParam(url, param) {
        // Expects a raw URL
        param = param.replace(/[[]/, "\[").replace(/[]]/, "\]");
        var regexS = "[\?&]" + param + "=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec(url);
        if (results === null || (results && typeof(results[1]) !== 'string' && results[1].length)) {
            return '';
        } else {
            return decodeURIComponent(results[1]).replace(/\W/gi, ' ');
        }
    };

    function campaignParams() {
        var campaign_keywords = 'utm_source utm_medium utm_campaign utm_content utm_term'.split(' ')
            , kw = ''
            , params = {}
            , first_params = {};

        var index;
        for (index = 0; index < campaign_keywords.length; ++index) {
            kw = getQueryParam(document.URL, campaign_keywords[index]);
            if (kw.length) {
                params[campaign_keywords[index] + ' [last touch]'] = kw;
            }
        }
        for (index = 0; index < campaign_keywords.length; ++index) {
            kw = getQueryParam(document.URL, campaign_keywords[index]);
            if (kw.length) {
                first_params[campaign_keywords[index] + ' [first touch]'] = kw;
            }
        }

        mixpanel.people.set(params);
        mixpanel.people.set_once(first_params);
        mixpanel.register(params);
        mixpanel.identify()
    }

    campaignParams();
@endif
</script>