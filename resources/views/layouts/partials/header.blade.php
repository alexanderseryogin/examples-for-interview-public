<!-- Header & Navigation -->
<header class="mainHeader">
   <div class="container">
        <nav class="navbar navbar-empty" role="navigation">
            <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
                <span class="sr-only">Toggle navigation</span>
            </button>
            <a class="navbar-brand" href="{{ route('homepage') }}"><img src="{{ asset('images/redtix_logo.png') }}" alt=""></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse-01">
            <ul class="nav navbar-nav pull-right">
                <li><a href="https://lifestylehub.airasiaredtix.com">Lifestyle Hub</a></li>
                <li><a href="/about">About Us</a></li>
                <li><a href="/faq">FAQ</a></li>
                <li><a class="newsletter-nav" data-toggle="modal" data-target="#modalSubscribeNewsletter">Newsletter</a></li>
                <li><a href="mailto:support_redtix@airasia.com">Contact Us</a></li>
            </ul>
            </div><!-- /.navbar-collapse -->
        </nav><!-- /navbar -->
   </div>

   @isset($showAdminNavigation)
        <div class="navbar-custom-menu">
            <ul class="nav">

                @if (Auth::check())
                    @php
                        $user = Auth::user();
                        $account = $user->account;
                    @endphp
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                           aria-expanded="false">
                            @if($account->avatar)
                                <img src="{{ $account->avatarUrl }}" class="user-image" alt="{{ $account->full_name }}">
                            @else
                                <img src="{{ asset('images/login/user-circle.svg') }}" class="user-image" alt="avatar">
                            @endif
                            <span class="hidden-xs">{{$user->email}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <a href="{{ \App\Helpers\AuthHelper::hasCmsAccess() ? route('admin.users.profile.show') : '' }}">
                                    <img src="{{ asset('images/login/user-circle.svg') }}" class="user-icon" alt="">My Account
                                    <span class="sub-text">{{$user->roleList}}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.events.index') }}">Event Management System</a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}">Logout<img src="{{ asset('images/login/logout.svg') }}" class="logout-icon"
                                                                           alt=""></a>
                            </li>
                        </ul>
                    </li>

                    @if(App\Helpers\AuthHelper::hasCmsAccess())
                        <li><a href="/admin">Admin</a></li>
                    @else
                        <li><a href="/home">Profile</a></li>
                    @endif
                @else
                    <li><a data-toggle="modal" data-target="#modalLogin" href="">Sign In</a></li>
                @endif

                @if(Auth::guest())
                    <li><a href="/register">Register</a></li>
                @endif
            </ul>
        </div>
    @endisset
</header>
