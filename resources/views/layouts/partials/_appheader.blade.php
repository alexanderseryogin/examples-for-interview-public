<div class="header">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<!-- Logo -->
				<div class="logo">
					@if(auth()->check())
						@if(\App\Helpers\AuthHelper::hasCmsAccess())
							<h1><a href="/admin">Redtix</a></h1>
						@else
							<h1><a href="/home">Redtix</a></h1>
						@endif
					@else
						<h1><a href="/">Redtix</a></h1>
					@endif
				</div>
			</div>
			{{-- <div class="col-md-5">
				<div class="row">
					<div class="col-lg-12">
						<div class="input-group form">
							<input type="text" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button">Search</button>
							</span>
						</div>
					</div>
				</div>
			</div> --}}
			<div class="col-md-2 pull-right">
				<div class="navbar navbar-inverse" role="banner">
					@if(auth()->check())
						<nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
									<ul class="dropdown-menu animated fadeInUp">
										<li><a href="/profile/{{ Auth::user()->digest }}">Profile</a></li>
										<li><a href="/profile/{{ Auth::user()->digest }}/password/change">Change Password</a></li>
										<li><a href="/logout">Logout</a></li>
									</ul>
								</li>
							</ul>
						</nav>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>