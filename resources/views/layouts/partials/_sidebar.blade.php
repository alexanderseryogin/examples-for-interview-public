<aside class="col-sm-3 mainSidebarContent hidden-xs">
    <!--Upcoming Events-->
    <section>
        <h1 class="mainSecTitle">Upcoming <span>Events</span></h1>
        <div class="table-responsive">
        <table class="table table-hover incomingEvent">
            <tbody>
            <tr>
                <td><a href="Fantasy_Rainforest" class="sideEventTitle">Fantasy Rainforest</a><span class="date">Daily</span></td>
            </tr>
            <tr>
                <td><a href="k_fun_stage_so_fresh" class="sideEventTitle">K Fun Stage – So Fresh</a><span class="date">15 July 2017</span></td>
            </tr>
            <tr>
                <td><a href="svara_asean" class="sideEventTitle">SVARA ASEAN</a><span class="date">28 - 29 July 2017</span></td>
            </tr>
            <tr>
                <td><a href="livin_la_vida_imelda" class="sideEventTitle">LIVIN’ LA VIDA IMELDA, Carlos Celdran’s One-Man Show</a><span class="date">28 July 2017</span></td>
            </tr>
            <tr>
                <td><a href="history_con_malaysia" class="sideEventTitle">History Con Malaysia 2017</a><span class="date">4 - 6 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="the_tribute_concert_by_ustad_rahat_fateh_ali_khan" class="sideEventTitle">The Tribute Concert 2017 by Ustad Rahat Fateh Ali Khan</a><span class="date">6 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="cell" class="sideEventTitle">CELL by Smoking Apples & Dogfish</a><span class="date">5 - 6 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="manganiyar_classroom" class="sideEventTitle">THE MANGANIYAR CLASSROOM By Roysten Abel</a><span class="date">5 - 6 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="ikat" class="sideEventTitle">IKAT by Artism</a><span class="date">11 - 12 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="a_simple_space" class="sideEventTitle">A SIMPLE SPACE by Gravity & Other Myths</a><span class="date">11 - 12 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="one_quest_for_greatness" class="sideEventTitle">ONE: QUEST FOR GREATNESS</a><span class="date">18 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="hakanai" class="sideEventTitle">HAKANAÏ by Adrien M & Claire B</a><span class="date">18 - 19 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="teng" class="sideEventTitle">TENG @ GEORGETOWN by The TENG Ensemble</a><span class="date">18 - 19 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="gala_by_jerome_bel" class="sideEventTitle">GALA by Jerome Bel</a><span class="date">19 - 20 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="tao_6&8" class="sideEventTitle">6 & 8 by TAO Dance Theatre</a><span class="date">25 - 26 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="the_italian_restaurant" class="sideEventTitle">THE ITALIAN RESTAURANT</a><span class="date">26 - 27 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="nosferatu" class="sideEventTitle">NOSFERATU – A SYMPHONY OF HORROR by Tess Said So </a><span class="date">26 - 27 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="a_feast_of_korean_classics" class="sideEventTitle">A FEAST OF KOREAN CLASSICS by IMBANGUL</a><span class="date">27 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="anthar_agni" class="sideEventTitle">ANTHAR AGNI by Temple of Fine Arts</a><span class="date">30 - 31 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="alvin_sputnik" class="sideEventTitle">THE ADVENTURES OF ALVIN SPUTNIK: DEEP SEA EXPLORER by Tim Watts</a><span class="date">30 - 31 August 2017</span></td>
            </tr>
            <tr>
                <td><a href="the_human_voice" class="sideEventTitle">THE HUMAN VOICE</a><span class="date">2 - 3 September 2017</span></td>
            </tr>
            <tr>
                <td><a href="malaysian_independent_live_fusion_festival" class="sideEventTitle">Malaysian Independent Live Fusion Festival 2017</a><span class="date">9 September 2017</span></td>
            </tr>
            <tr>
                <td><a href="FlashRun2017" class="sideEventTitle">FLASH RUN 2017</a><span class="date">23 September 2017</span></td>
            </tr>
            <tr>
                <td><a href="2017_formula1_petronas_malaysia_grandprix" class="sideEventTitle">2017 FORMULA 1 PETRONAS MALAYSIA GRANDPRIX</a><span class="date">29 September 2017</span></td>
            </tr>
            <tr>
                <td><a href="2017_shell_malaysia_motorcycle_grandprix" class="sideEventTitle">2017 SHELL MALAYSIA MOTORCYLE GRANDPRIX</a><span class="date">27 October 2017</span></td>
            </tr>
            <tr>
                <td><a href="titanland_festival_2017" class="sideEventTitle">Titanland Festival 2017</a><span class="date">25 November 2017</span></td>
            </tr>
            <tr>
                <td><a href="all_star_jukebox_in_concert" class="sideEventTitle">All Star Jukebox in Concert</a><span class="date">5th August 2017</span></td>
            </tr>
            <tr>
                <td><a href="shila_amzah" class="sideEventTitle">Shila Amzah “My Journey” Concert in Malaysia 2017</a><span class="date">26th August 2017</span></td>
            </tr>
            <tr>
                <td><a href="gary_chaw" class="sideEventTitle">Gary Chaw “A Friend” Live Concert in Genting</a><span class="date">31st August 2017</span></td>
            </tr>
            <tr>
                <td><a href="richie_jen_live_in_genting_2017" class="sideEventTitle">Richie Jen Live in Genting 2017</a><span class="date">11th November 2017</span></td>
            </tr>
            <tr>
                <td><a href="18th_malaysia_national_lion_dance_championship_2017"class="sideEventTitle">18th Malaysia National Lion Dance Championship 2017</a><span class="date">2nd December 2017</span></td>
            </tr>
            <tr>
                <td><a href="da_bang" class="sideEventTitle">Da Bang Malaysia Tour Live in Malaysia</a><span class="date">Postponed</span></td>
            </tr>
            </tbody>
        </table>
        </div>
    </section>
    <!-- /Upcoming Events-->
    <!--Need Help-->
    <section>
        <h1 class="mainSecTitle">Need <span>Help?</span></h1>
        <p><strong>AirAsiaRedTix.com</strong></p>
        <p>We will be offering an international line-up of concerts, sporting events, musicals and theatre performances in the coming months. More updates coming your way!</p>
        <a href="mailto:support@airasiaredtix.com">Contact us <i class="fa fa-question-circle" aria-hidden="true"></i></a>
    </section>
    <!-- /Need Help-->
</aside>