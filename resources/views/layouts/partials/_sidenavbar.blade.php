<div class="sidebar content-box" style="display: block;">
    <ul class="nav">
        <!-- Main menu -->
        <div class="nav-header text-center mtm mbm">
            <a class="nav-brand" href="/admin"><img class="img-responsive center-block" src="{!! asset('images/redtix_logo.png') !!}" alt=""></a>
        </div>
        <li class="@if(Request::path() == 'admin')current @endif"><a href="/admin"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
        <li class="@if(request()->segment(2) == 'events')current @endif"><a href="/admin/events"><i class="glyphicon glyphicon-gift"></i> Events</a></li>
        {{-- <li class="@if(request()->segment(2) == 'events')current open @endif submenu" role="menu">
            <a href="#">
                <i class="glyphicon glyphicon-gift"></i>Events<span class="caret pull-right"></span>
             </a>
             <!-- Sub menu -->
             <ul class="@if(request()->segment(2) == 'events')active @endif">
                <li class="@if(request()->segment(count(Request::segments())) == 'index')current @endif"><a href="/admin/events/index"><i class="glyphicon glyphicon-list"></i> Ongoing</a></li>
                <li class="@if(request()->segment(count(Request::segments())) == 'archive')current @endif"><a href="/admin/events/archive"><i class="glyphicon glyphicon-folder-open"></i> Archive</a></li>
            </ul>
        </li>
 --}}
        @if(App\Helpers\AuthHelper::hasCmsAccess())
            <li class="@if(request()->segment(2) == 'users')current @endif"><a href="/admin/users"><i class="glyphicon glyphicon-user"></i>Users</a></li>
        @endif
        <li class="@if(Request::path() == 'admin/settings')current @endif"><a href="/admin/settings"><i class="glyphicon glyphicon-cog"></i>Settings</a></li>
        <li><a href="/"><i class="glyphicon glyphicon-th-large"></i>Go to homepage</a></li>
    </ul>
</div>


{{-- <div class="well sidebar-nav" style="margin-top: 30px; height: 100%">
    <div class="nav-header text-center mtm mbm">
    	<a class="nav-brand" href="/admin"><img src="{!! asset('images/redtix_logo.png') !!}" alt=""></a>
    </div>
    <ul class="nav nav-pills nav-stacked">
        <li><a href="/admin">Dashboard</a></li>
        <li><a href="/admin/events">Events</a></li>
        @if(Auth::user()->is_admin)
            <li><a href="/admin/users">Users</a></li>
        @endif
        <li><a href="/admin/settings">Settings</a></li>
        <li><a href="/">Go to homepage</a></li>
    </ul>
    <br>
</div> --}}