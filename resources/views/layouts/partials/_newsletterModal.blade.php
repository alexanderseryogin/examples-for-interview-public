
<div class="modal newsletter-modal" id="modalSubscribeNewsletter">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                
                <div id="mc_embed_signup_scroll">
                    <h3>Newsletter</h3>
                    <p>Be the first to find out about exciting upcoming events<br>and exclusive offers!</p>
                    <div class="row">

                        <div class="col-md-12">
                            <form action="/subscribe" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate">
                                {{ csrf_field() }}
                                <div id="mc_embed_signup_scroll">
                                    <div class="col-sm-8 col-xs-12">
                                        <input type="email" name="email" class="email form-control" id="email" placeholder="email address" required>
                                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_563fc1bcdefdc4fca4efb1dbe_719d8ddc42" tabindex="-1" value=""></div>
                                    </div>
                                    <div class="clearfix visible-xs">&nbsp;</div>
                                    <div class="col-sm-4 col-xs-12">
                                        <input type="submit" id="mc-embedded-subscribe" class="button btn btn-danger btn-block">

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
