<nav class="navbar navbar-inverse visible-xs">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>         
            </button>
            <a class="navbar-brand" href="/"><img class="img-responsive" src="{!! asset('images/redtix_logo.png') !!}" alt=""></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/admin">Dashboard</a></li>
                <li><a href="/admin/events">Events</a></li>
                <li><a href="/admin/settings">Settings</a></li>
            </ul>
        </div>
    </div>
</nav>