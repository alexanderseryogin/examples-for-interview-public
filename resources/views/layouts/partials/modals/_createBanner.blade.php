<div class="modal fade" id="createBanner" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-default">
            <div class="panel-heading">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Upload Banner</h4>
                <div class="clearfix"></div>
            </div>
            <form action="{{ URL::current() }}/banners/store" method="post" enctype="multipart/form-data">
            	{{ csrf_field() }}
            	<div class="modal-body" style="padding: 5px;">
                    <div class="form-group">
		                <input id="title" type="text" class="form-control" name="title" placeholder='Banner title' autofocus required="required">
		            </div>
		            <div class="form-group">
		                <input id="links" type="text" class="form-control" name="links" placeholder='Link(navigate if clicked)' autofocus>
		            </div>

                    @if($banners->count() >= 1)
    		            <div class="form-group">
    						<select class="form-control form-control--select" name="number_placed" id="number_placed" required="required">
    							<option value="">Select position number</option>
    							@for( $i = 1; $i <= 6; $i++)
    								@if(!in_array($i, $banners_num_placed))
    									<option value="{{ $i }}">Position {{ $i }}</option>
    								@endif
    							@endfor
    						</select> 
    					</div>
                    @endif
                  
                    <div class="form-group">
                    	<div class="input-group input-file" name="website" required="required">
                            <span class="input-group-btn">
                                <button class="btn btn-danger btn-choose" name="website" type="button">Browse</button>
                            </span>
                            <input type="text" name="website" class="form-control" placeholder='Browse website version' />
                        </div>
                    </div>
                    <div class="form-group">
                    	<div class="input-group input-file" name="mobile" required="required">
                            <span class="input-group-btn">
                                <button class="btn btn-danger btn-choose" name="mobile" type="button">Browse</button>
                            </span>
                            <input type="text" name="mobile" class="form-control" placeholder='Browse mobile version' />
                        </div>
                    </div>
                </div>  
                <div class="panel-footer">
                    <input type="submit" class="btn btn-danger" value="Upload"/>
                    <button type="button" class="btn btn-default btn-close pull-right" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>