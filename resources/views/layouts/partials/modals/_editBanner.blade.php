@foreach($banners as $banner)
	<div class="modal fade" id="editBanner{{ $banner->id }}" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                <h4>Edit Banner</h4>
	                <div class="clearfix"></div>
	            </div>
	            <form action="{{ URL::current() }}/banners/{{ $banner->id }}/edit" method="post" enctype="multipart/form-data">
	            	{{ csrf_field() }}
	            	<div class="modal-body" style="padding: 5px;">
	                    <div class="form-group">
			                <input id="title" type="text" class="form-control" name="title" value={{ $banner->title }} autofocus required="required">
			            </div>
			            <div class="form-group">
			                <input id="links" type="text" class="form-control" name="links"  @if(isset($banner->links)) value="{{ $banner->links }}" @else placeholder="Navigate if clicked"@endif autofocus>
			            </div>
			            <div class="form-group">
							<select class="form-control form-control--select" name="number_placed" id="edit_number_placed" required>
								<option value="{{ null }}">Select position number</option>
								@for( $i = 1; $i <= 6; $i++)
									<option value="{{ $i }}" {{ $banner->num_placed == $i ? 'selected="selected"':'' }}>Position {{ $i }}</option>
								@endfor
							</select> 
						</div>
	                  
	                    <div class="form-group">
	                    	<div class="input-group input-file" name="website" required="required">
	                            <span class="input-group-btn">
	                                <button class="btn btn-danger btn-choose" name="website" type="button">Browse</button>
	                            </span>
	                            <input type="text" name="website" class="form-control" value='{{ $banner->web_path }}' />
	                        </div>
	                    </div>

	                    {{-- <div class="form-group">
							<div class="panel panel-default" id="gallery">
							    <div class="panel-body">
						            <div class="img-thumbnail text-center center-block" style="display: inline-block; width: 100%;">
						                <a href="{{ $banner->web_path }}" data-featherlight="image"><img class="img-responsive" src="{{ $banner->web_path }}" alt="" style="height: 135px; width: 100%"></a>
						            </div>
							    </div>
							    <div class="panel-footer">
							        <div class="input-group input-file" name="mobile" required="required">
							            <span class="input-group-btn">
							                <button class="btn btn-danger btn-choose" name="mobile" type="button">Browse</button>
							            </span>
							            <input type="text" name="mobile" class="form-control" value='{{ $banner->web_path }}' />
							        </div>
							    </div>
							</div>
						</div> --}}

	                    <div class="form-group">
	                    	<div class="input-group input-file" name="mobile" required="required">
	                            <span class="input-group-btn">
	                                <button class="btn btn-danger btn-choose" name="mobile" type="button">Browse</button>
	                            </span>
	                            <input type="text" name="mobile" class="form-control" value='{{ $banner->mobile_path }}' />
	                        </div>
	                    </div>
	                </div>  
	                <div class="panel-footer">
	                    <input type="submit" class="btn btn-danger" value="Edit"/>
	                    <button type="button" class="btn btn-default btn-close pull-right" data-dismiss="modal">Close</button>
	                </div>
	            </form>
	        </div>
	    </div>
	</div>
@endforeach





