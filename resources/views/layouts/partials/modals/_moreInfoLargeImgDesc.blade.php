
@foreach($ticket_info as $ticket)

{{-- ticket info modal --}}
<div id="infoModal{{ $ticket["Sorting Id"] }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow info-box">

                <div class="details">
                    <h6 class="type">{{$ticket["Ticket Type Desc"]}}</h6>
                    <h6 class="days">{{$ticket["Ticket Name"]}}</h6>
                </div>
                <div class="details">
                    <ul>
                        <li><span class="fa fa-calendar"></span>{{$ticket["More Info Date"]}}</li>
                        <li><span class="fa fa-map-marker"></span>{{$ticket["More Info Location"]}}</li>
                        <li><span class="fa fa-tag"></span>{{$ticket["More Info Type"]}}</li>
                    </ul>   
                </div>
                <div class="copy_text2">
                    <img src="{{ $ticket["More Info Img"] }}" class="img-responsive">
                </div>
                <div class="copy_text2">
                    <h2>{{$ticket["More Info Title"]}}</h2>
                    <p>{{$ticket["More Info Desc1"]}}</p>
                    <p>{{$ticket["More Info Desc2"]}}</p>
                </div>

                <div class="copy_text2 text-center">
                    *Ticketing &amp; Transaction Fee charges will be added at the checkout basket.<br/>
                    <a href="purchaseterms">Terms &amp; Conditions</a> apply.
                </div>    

            </div> 
        </div>
      </div>
    </div>
</div>


@endforeach