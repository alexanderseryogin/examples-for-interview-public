 <!--Modal Announcement-->
    <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content" style="width: 100%">
            <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="clearfix">&nbsp;</div>
                <div class="modcustom">
                    @if($announcement->img_link)
                       <img class="img-responsive center-block" src="{{ $announcement->img_link }}" align="middle">
                    @endif
                    @if($announcement->content)
                        <div class="well">
                            {!! $announcement->content !!}
                            @if($announcement->btn_link)
                                <a class="btn btn-danger enabled" id="buyButton" target="_blank" href="{{ $announcement->btn_link }}" role="button">CLICK THIS</a>
                            @endif
                        </div>
                    @endif
                </div>
            </div>
          </div>
        </div>
    </div>