<!-- Header & Navigation -->
<header class="innerHeader">
    <div class="container">
        <nav class="navbar navbar-empty" role="navigation">
            <div class="navbar-header" style="width: 100%;">
            	<a class="navbar-brand" href="/"><img class="img-responsive" src="{!! asset('images/redtix_logo.png') !!}" alt=""></a>
            	{{-- <a class="search-btn pull-right" href="/events">Search <i class="fa fa-search" aria-hidden="true"></i></a> --}}
            	<div class="collapse navbar-collapse" id="navbar-collapse-01">
			        <ul class="nav navbar-nav pull-right">
			            <li><a href="/about">About Us</a></li>
			            <li><a href="/faq">FAQ</a></li>
			            <li><a class="newsletter-nav" data-toggle="modal" data-target="#modalSubscribeNewsletter">Newsletter</a></li>
			            <li><a href="mailto:support@airasiaredtix.com">Contact Us</a></li>
			            @if(auth()->check())
			                <li><a href="/logout">Logout</a></li>
			            @else
			                <li><a href="/login">Login</a></li>
			            @endif
			            {{-- <li><a href="/register">Register</a></li> --}}
			        </ul>
			    </div>
        	</div>
        </nav>
    </div>
</header>