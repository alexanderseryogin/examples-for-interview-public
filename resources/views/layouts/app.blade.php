<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="shortcut icon" href="{{ asset('images/redtix_logo.png') }}">

    <!-- Styles -->
    <link rel="stylesheet" href="/css/app.min.css"> 

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet" type="text/css">

    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" type="text/css" rel="stylesheet" media="screen">
    <!-- Bootstrap -->
    <link href="/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <!-- styles -->
    <link href="/css/app.css" type="text/css" rel="stylesheet">

    <link href="//cdn.rawgit.com/noelboss/featherlight/1.7.12/release/featherlight.min.css" type="text/css" rel="stylesheet" />

    <!-- Scripts -->
    <script>
        window.Laravel = @json(['csrfToken' => csrf_token()]);
    </script>
</head>
<body>

    @yield('header')

    <div class="page-content">
        {{--<div class="col-md-12">--}}
            {{--@if(auth()->check())--}}
                {{--<div class="col-md-2">--}}
                    {{--@include('layouts.partials._sidenavbar')--}}
                {{--</div>--}}
            {{--@endif--}}
            {{--<div class="col-md-12">--}}
                @include('layouts.components.alert')
                {{--@if (count($errors) > 0)--}}
                    {{--@include('layouts.components.errors')--}}
                {{--@endif--}}
                {{-- Comment becuse errors dublicate on password reset page --}}
                @yield('content')
            {{--</div>--}}
        {{--</div>--}}
    </div>
    @yield('modal')
    {{--<link type="text/css" href="{{ asset('datatables/dataTables.bootstrap.css') }}" rel="stylesheet" media="screen">--}}
    {{-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vue"></script> --}}
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="https://code.jquery.com/jquery.js"></script>
    <!-- jQuery UI -->
    <script type="text/javascript" src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
    @stack('scripts')
    {{--<script type="text/javascript" src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ asset('datatables/dataTables.bootstrap.js') }}"></script>--}}
    <script type="text/javascript" src="/js/custom.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('.form-group-animated .form-control')
                .focus(function(){
                    $(this).parents('.form-group-animated').addClass('focused');
                })
                .blur(function(){
                        let inputValue = $(this).val();
                        if ( !inputValue ) {
                            $(this).removeClass('filled');
                            $(this).parents('.form-group-animated').removeClass('focused');
                        } else {
                            $(this).addClass('filled');
                        }
                    }
                );
        });
    </script>
</body>
</html>
