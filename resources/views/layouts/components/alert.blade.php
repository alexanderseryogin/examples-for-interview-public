@if (session('successMessage'))
    <div class="alert alert-success fade in alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="no-overflow">
            {{ session('successMessage') }}
        </div>
    </div>
@endif
@if (session('warningMessage'))
    <div class="alert alert-warning fade in alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="no-overflow">
            {{ session('warningMessage') }}
        </div>
    </div>
@endif
@if (session('errorMessage'))
    <div class="alert alert-danger fade in alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="no-overflow">
            {{ session('errorMessage') }}
        </div>
    </div>
@endif