<select class="form-control form-control--select" name="category_id" id="category_id">
    <option value="">No category</option>
    @foreach($categories as $category)
        <option value="{{$category->id}}" {{ $selected == $category->id ? 'selected="selected"':'' }}>{{$category->text}}</option>
    @endforeach
</select>