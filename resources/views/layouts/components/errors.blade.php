<div class="alert alert-warning fade in alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <i class="glyphicon glyphicon-warning-sign pull-left"></i>
    <div class="no-overflow">
        <ul style="list-style: none; margin: 0; padding: 0px;">
            @foreach ($errors->all() as $error)
            	{{-- {{ dd($errors) }} --}}
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>