<option value="Africa/Abidjan" {{ $user->timezone == "Africa/Abidjan" ? 'selected="selected"':'' }}>(GMT+00:00) Africa/Abidjan </option>
<option  value="Africa/Abidjan" {{ $user->timezone == "Africa/Abidjan" ? 'selected="selected"':''}}>(GMT+00:00) Africa/Abidjan  </option>
<option  value="Africa/Accra" {{ $user->timezone == "Africa/Accra" ? 'selected="selected"':''}}>(GMT+00:00) Africa/Accra    </option>
<option  value="Africa/Bamako" {{ $user->timezone == "Africa/Bamako" ? 'selected="selected"':''}}>(GMT+00:00) Africa/Bamako     </option>
<option  value="Africa/Banjul" {{ $user->timezone == "Africa/Banjul" ? 'selected="selected"':''}}>(GMT+00:00) Africa/Banjul     </option>
<option  value="Africa/Bissau" {{ $user->timezone == "Africa/Bissau" ? 'selected="selected"':''}}>(GMT+00:00) Africa/Bissau     </option>
<option  value="Africa/Casablanca" {{ $user->timezone == "Africa/Casablanca" ? 'selected="selected"':''}}>(GMT+00:00) Africa/Casablanca     </option>
<option  value="Africa/Conakry" {{ $user->timezone == "Africa/Conakry" ? 'selected="selected"':''}}>(GMT+00:00) Africa/Conakry  </option>
<option  value="Africa/Dakar" {{ $user->timezone == "Africa/Dakar" ? 'selected="selected"':''}}>(GMT+00:00) Africa/Dakar    </option>
<option  value="Africa/El_Aaiun" {{ $user->timezone == "Africa/El_Aaiun" ? 'selected="selected"':''}}>(GMT+00:00) Africa/El Aaiun   </option>
<option  value="Africa/Freetown" {{ $user->timezone == "Africa/Freetown" ? 'selected="selected"':''}}>(GMT+00:00) Africa/Freetown   </option>
<option  value="Africa/Lome" {{ $user->timezone == "Africa/Lome" ? 'selected="selected"':''}}>(GMT+00:00) Africa/Lome   </option>
<option  value="Africa/Monrovia" {{ $user->timezone == "Africa/Monrovia" ? 'selected="selected"':''}}>(GMT+00:00) Africa/Monrovia   </option>
<option  value="Africa/Nouakchott" {{ $user->timezone == "Africa/Nouakchott" ? 'selected="selected"':''}}>(GMT+00:00) Africa/Nouakchott     </option>
<option  value="Africa/Ouagadougou" {{ $user->timezone == "Africa/Ouagadougou" ? 'selected="selected"':''}}>(GMT+00:00) Africa/Ouagadougou  </option>
<option  value="Africa/Sao_Tome" {{ $user->timezone == "Africa/Sao_Tome" ? 'selected="selected"':''}}>(GMT+00:00) Africa/Sao Tome   </option>
<option  value="Africa/Algiers" {{ $user->timezone == "Africa/Algiers" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Algiers  </option>
<option  value="Africa/Bangui" {{ $user->timezone == "Africa/Bangui" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Bangui     </option>
<option  value="Africa/Brazzaville" {{ $user->timezone == "Africa/Brazzaville" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Brazzaville  </option>
<option  value="Africa/Ceuta" {{ $user->timezone == "Africa/Ceuta" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Ceuta    </option>
<option  value="Africa/Douala" {{ $user->timezone == "Africa/Douala" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Douala     </option>
<option  value="Africa/Kinshasa" {{ $user->timezone == "Africa/Kinshasa" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Kinshasa   </option>
<option  value="Africa/Lagos" {{ $user->timezone == "Africa/Lagos" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Lagos    </option>
<option  value="Africa/Libreville" {{ $user->timezone == "Africa/Libreville" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Libreville     </option>
<option  value="Africa/Luanda" {{ $user->timezone == "Africa/Luanda" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Luanda     </option>
<option  value="Africa/Malabo" {{ $user->timezone == "Africa/Malabo" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Malabo     </option>
<option  value="Africa/Ndjamena" {{ $user->timezone == "Africa/Ndjamena" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Ndjamena   </option>
<option  value="Africa/Niamey" {{ $user->timezone == "Africa/Niamey" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Niamey     </option>
<option  value="Africa/Porto-Novo" {{ $user->timezone == "Africa/Porto-Novo" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Porto-Novo     </option>
<option  value="Africa/Tunis" {{ $user->timezone == "Africa/Tunis" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Tunis    </option>
<option  value="Africa/Windhoek" {{ $user->timezone == "Africa/Windhoek" ? 'selected="selected"':''}}>(GMT+01:00) Africa/Windhoek   </option>
<option  value="Africa/Blantyre" {{ $user->timezone == "Africa/Blantyre" ? 'selected="selected"':''}}>(GMT+02:00) Africa/Blantyre   </option>
<option  value="Africa/Bujumbura" {{ $user->timezone == "Africa/Bujumbura" ? 'selected="selected"':''}}>(GMT+02:00) Africa/Bujumbura    </option>
<option  value="Africa/Cairo" {{ $user->timezone == "Africa/Cairo" ? 'selected="selected"':''}}>(GMT+02:00) Africa/Cairo    </option>
<option  value="Africa/Gaborone" {{ $user->timezone == "Africa/Gaborone" ? 'selected="selected"':''}}>(GMT+02:00) Africa/Gaborone   </option>
<option  value="Africa/Harare" {{ $user->timezone == "Africa/Harare" ? 'selected="selected"':''}}>(GMT+02:00) Africa/Harare     </option>
<option  value="Africa/Johannesburg" {{ $user->timezone == "Africa/Johannesburg" ? 'selected="selected"':''}}>(GMT+02:00) Africa/Johannesburg   </option>
<option  value="Africa/Kigali" {{ $user->timezone == "Africa/Kigali" ? 'selected="selected"':''}}>(GMT+02:00) Africa/Kigali     </option>
<option  value="Africa/Lubumbashi" {{ $user->timezone == "Africa/Lubumbashi" ? 'selected="selected"':''}}>(GMT+02:00) Africa/Lubumbashi     </option>
<option  value="Africa/Lusaka" {{ $user->timezone == "Africa/Lusaka" ? 'selected="selected"':''}}>(GMT+02:00) Africa/Lusaka     </option>
<option  value="Africa/Maputo" {{ $user->timezone == "Africa/Maputo" ? 'selected="selected"':''}}>(GMT+02:00) Africa/Maputo     </option>
<option  value="Africa/Maseru" {{ $user->timezone == "Africa/Maseru" ? 'selected="selected"':''}}>(GMT+02:00) Africa/Maseru     </option>
<option  value="Africa/Mbabane" {{ $user->timezone == "Africa/Mbabane" ? 'selected="selected"':''}}>(GMT+02:00) Africa/Mbabane  </option>
<option  value="Africa/Tripoli" {{ $user->timezone == "Africa/Tripoli" ? 'selected="selected"':''}}>(GMT+02:00) Africa/Tripoli  </option>
<option  value="Africa/Addis_Ababa" {{ $user->timezone == "Africa/Addis_Ababa" ? 'selected="selected"':''}}>(GMT+03:00) Africa/Addis Ababa  </option>
<option  value="Africa/Asmara" {{ $user->timezone == "Africa/Asmara" ? 'selected="selected"':''}}>(GMT+03:00) Africa/Asmara     </option>
<option  value="Africa/Dar_es_Salaam" {{ $user->timezone == "Africa/Dar_es_Salaam" ? 'selected="selected"':''}}>(GMT+03:00) Africa/Dar es Salaam    </option>
<option  value="Africa/Djibouti" {{ $user->timezone == "Africa/Djibouti" ? 'selected="selected"':''}}>(GMT+03:00) Africa/Djibouti   </option>
<option  value="Africa/Kampala" {{ $user->timezone == "Africa/Kampala" ? 'selected="selected"':''}}>(GMT+03:00) Africa/Kampala  </option>
<option  value="Africa/Khartoum" {{ $user->timezone == "Africa/Khartoum" ? 'selected="selected"':''}}>(GMT+03:00) Africa/Khartoum   </option>
<option  value="Africa/Mogadishu" {{ $user->timezone == "Africa/Mogadishu" ? 'selected="selected"':''}}>(GMT+03:00) Africa/Mogadishu    </option>
<option  value="Africa/Nairobi" {{ $user->timezone == "Africa/Nairobi" ? 'selected="selected"':''}}>(GMT+03:00) Africa/Nairobi  </option>
<option  value="America/Adak" {{ $user->timezone == "America/Adak" ? 'selected="selected"':''}}>(GMT-10:00) America/Adak    </option>
<option  value="America/Anchorage" {{ $user->timezone == "America/Anchorage" ? 'selected="selected"':''}}>(GMT-09:00) America/Anchorage     </option>
<option  value="America/Juneau" {{ $user->timezone == "America/Juneau" ? 'selected="selected"':''}}>(GMT-09:00) America/Juneau  </option>
<option  value="America/Nome" {{ $user->timezone == "America/Nome" ? 'selected="selected"':''}}>(GMT-09:00) America/Nome    </option>
<option  value="America/Sitka" {{ $user->timezone == "America/Sitka" ? 'selected="selected"':''}}>(GMT-09:00) America/Sitka     </option>
<option  value="America/Yakutat" {{ $user->timezone == "America/Yakutat" ? 'selected="selected"':''}}>(GMT-09:00) America/Yakutat   </option>
<option  value="America/Dawson" {{ $user->timezone == "America/Dawson" ? 'selected="selected"':''}}>(GMT-08:00) America/Dawson  </option>
<option  value="America/Los_Angeles" {{ $user->timezone == "America/Los_Angeles" ? 'selected="selected"':''}}>(GMT-08:00) America/Los Angeles   </option>
<option  value="America/Metlakatla" {{ $user->timezone == "America/Metlakatla" ? 'selected="selected"':''}}>(GMT-08:00) America/Metlakatla  </option>
<option  value="America/Santa_Isabel" {{ $user->timezone == "America/Santa_Isabel" ? 'selected="selected"':''}}>(GMT-08:00) America/Santa Isabel    </option>
<option  value="America/Tijuana" {{ $user->timezone == "America/Tijuana" ? 'selected="selected"':''}}>(GMT-08:00) America/Tijuana   </option>
<option  value="America/Vancouver" {{ $user->timezone == "America/Vancouver" ? 'selected="selected"':''}}>(GMT-08:00) America/Vancouver     </option>
<option  value="America/Whitehorse" {{ $user->timezone == "America/Whitehorse" ? 'selected="selected"':''}}>(GMT-08:00) America/Whitehorse  </option>
<option  value="America/Boise" {{ $user->timezone == "America/Boise" ? 'selected="selected"':''}}>(GMT-07:00) America/Boise     </option>
<option  value="America/Cambridge_Bay" {{ $user->timezone == "America/Cambridge_Bay" ? 'selected="selected"':''}}>(GMT-07:00) America/Cambridge Bay     </option>
<option  value="America/Chihuahua" {{ $user->timezone == "America/Chihuahua" ? 'selected="selected"':''}}>(GMT-07:00) America/Chihuahua     </option>
<option  value="America/Dawson_Creek" {{ $user->timezone == "America/Dawson_Creek" ? 'selected="selected"':''}}>(GMT-07:00) America/Dawson Creek    </option>
<option  value="America/Denver" {{ $user->timezone == "America/Denver" ? 'selected="selected"':''}}>(GMT-07:00) America/Denver  </option>
<option  value="America/Edmonton" {{ $user->timezone == "America/Edmonton" ? 'selected="selected"':''}}>(GMT-07:00) America/Edmonton    </option>
<option  value="America/Hermosillo" {{ $user->timezone == "America/Hermosillo" ? 'selected="selected"':''}}>(GMT-07:00) America/Hermosillo  </option>
<option  value="America/Inuvik" {{ $user->timezone == "America/Inuvik" ? 'selected="selected"':''}}>(GMT-07:00) America/Inuvik  </option>
<option  value="America/Mazatlan" {{ $user->timezone == "America/Mazatlan" ? 'selected="selected"':''}}>(GMT-07:00) America/Mazatlan    </option>
<option  value="America/Ojinaga" {{ $user->timezone == "America/Ojinaga" ? 'selected="selected"':''}}>(GMT-07:00) America/Ojinaga   </option>
<option  value="America/Phoenix" {{ $user->timezone == "America/Phoenix" ? 'selected="selected"':''}}>(GMT-07:00) America/Phoenix   </option>
<option  value="America/Yellowknife" {{ $user->timezone == "America/Yellowknife" ? 'selected="selected"':''}}>(GMT-07:00) America/Yellowknife   </option>
<option  value="America/Bahia_Banderas" {{ $user->timezone == "America/Bahia_Banderas" ? 'selected="selected"':''}}>(GMT-06:00) America/Bahia Banderas  </option>
<option  value="America/Belize" {{ $user->timezone == "America/Belize" ? 'selected="selected"':''}}>(GMT-06:00) America/Belize  </option>
<option  value="America/Cancun" {{ $user->timezone == "America/Cancun" ? 'selected="selected"':''}}>(GMT-06:00) America/Cancun  </option>
<option  value="America/Chicago" {{ $user->timezone == "America/Chicago" ? 'selected="selected"':''}}>(GMT-06:00) America/Chicago   </option>
<option  value="America/Costa_Rica" {{ $user->timezone == "America/Costa_Rica" ? 'selected="selected"':''}}>(GMT-06:00) America/Costa Rica  </option>
<option  value="America/El_Salvador" {{ $user->timezone == "America/El_Salvador" ? 'selected="selected"':''}}>(GMT-06:00) America/El Salvador   </option>
<option  value="America/Guatemala" {{ $user->timezone == "America/Guatemala" ? 'selected="selected"':''}}>(GMT-06:00) America/Guatemala     </option>
<option  value="America/Indiana/Knox" {{ $user->timezone == "America/Indiana/Knox" ? 'selected="selected"':''}}>(GMT-06:00) America/Indiana / Knox  </option>
<option  value="America/Indiana/Tell_City" {{ $user->timezone == "America/Indiana/Tell_City" ? 'selected="selected"':''}}>(GMT-06:00) America/Indiana / Tell City   </option>
<option  value="America/Managua" {{ $user->timezone == "America/Managua" ? 'selected="selected"':''}}>(GMT-06:00) America/Managua   </option>
<option  value="America/Matamoros" {{ $user->timezone == "America/Matamoros" ? 'selected="selected"':''}}>(GMT-06:00) America/Matamoros     </option>
<option  value="America/Menominee" {{ $user->timezone == "America/Menominee" ? 'selected="selected"':''}}>(GMT-06:00) America/Menominee     </option>
<option  value="America/Merida" {{ $user->timezone == "America/Merida" ? 'selected="selected"':''}}>(GMT-06:00) America/Merida  </option>
<option  value="America/Mexico_City" {{ $user->timezone == "America/Mexico_City" ? 'selected="selected"':''}}>(GMT-06:00) America/Mexico City   </option>
<option  value="America/Monterrey" {{ $user->timezone == "America/Monterrey" ? 'selected="selected"':''}}>(GMT-06:00) America/Monterrey     </option>
<option  value="America/North_Dakota/Beulah" {{ $user->timezone == "America/North_Dakota/Beulah" ? 'selected="selected"':''}}>(GMT-06:00) America/North Dakota / Beulah     </option>
<option  value="America/North_Dakota/Center" {{ $user->timezone == "America/North_Dakota/Center" ? 'selected="selected"':''}}>(GMT-06:00) America/North Dakota / Center     </option>
<option  value="America/North_Dakota/New_Salem" {{ $user->timezone == "America/North_Dakota/New_Salem" ? 'selected="selected"':''}}>(GMT-06:00) America/North Dakota / New Salem    </option>
<option  value="America/Rainy_River" {{ $user->timezone == "America/Rainy_River" ? 'selected="selected"':''}}>(GMT-06:00) America/Rainy River   </option>
<option  value="America/Rankin_Inlet" {{ $user->timezone == "America/Rankin_Inlet" ? 'selected="selected"':''}}>(GMT-06:00) America/Rankin Inlet    </option>
<option  value="America/Regina" {{ $user->timezone == "America/Regina" ? 'selected="selected"':''}}>(GMT-06:00) America/Regina  </option>
<option  value="America/Swift_Current" {{ $user->timezone == "America/Swift_Current" ? 'selected="selected"':''}}>(GMT-06:00) America/Swift Current     </option>
<option  value="America/Tegucigalpa" {{ $user->timezone == "America/Tegucigalpa" ? 'selected="selected"':''}}>(GMT-06:00) America/Tegucigalpa   </option>
<option  value="America/Winnipeg" {{ $user->timezone == "America/Winnipeg" ? 'selected="selected"':''}}>(GMT-06:00) America/Winnipeg    </option>
<option  value="America/Atikokan" {{ $user->timezone == "America/Atikokan" ? 'selected="selected"':''}}>(GMT-05:00) America/Atikokan    </option>
<option  value="America/Bogota" {{ $user->timezone == "America/Bogota" ? 'selected="selected"':''}}>(GMT-05:00) America/Bogota  </option>
<option  value="America/Cayman" {{ $user->timezone == "America/Cayman" ? 'selected="selected"':''}}>(GMT-05:00) America/Cayman  </option>
<option  value="America/Detroit" {{ $user->timezone == "America/Detroit" ? 'selected="selected"':''}}>(GMT-05:00) America/Detroit   </option>
<option  value="America/Grand_Turk" {{ $user->timezone == "America/Grand_Turk" ? 'selected="selected"':''}}>(GMT-05:00) America/Grand Turk  </option>
<option  value="America/Guayaquil" {{ $user->timezone == "America/Guayaquil" ? 'selected="selected"':''}}>(GMT-05:00) America/Guayaquil     </option>
<option  value="America/Havana" {{ $user->timezone == "America/Havana" ? 'selected="selected"':''}}>(GMT-05:00) America/Havana  </option>
<option  value="America/Indiana/Indianapolis" {{ $user->timezone == "America/Indiana/Indianapolis" ? 'selected="selected"':''}}>(GMT-05:00) America/Indiana / Indianapolis  </option>
<option  value="America/Indiana/Marengo" {{ $user->timezone == "America/Indiana/Marengo" ? 'selected="selected"':''}}>(GMT-05:00) America/Indiana / Marengo     </option>
<option  value="America/Indiana/Petersburg" {{ $user->timezone == "America/Indiana/Petersburg" ? 'selected="selected"':''}}>(GMT-05:00) America/Indiana / Petersburg    </option>
<option  value="America/Indiana/Vevay" {{ $user->timezone == "America/Indiana/Vevay" ? 'selected="selected"':''}}>(GMT-05:00) America/Indiana / Vevay   </option>
<option  value="America/Indiana/Vincennes" {{ $user->timezone == "America/Indiana/Vincennes" ? 'selected="selected"':''}}>(GMT-05:00) America/Indiana / Vincennes   </option>
<option  value="America/Indiana/Winamac" {{ $user->timezone == "America/Indiana/Winamac" ? 'selected="selected"':''}}>(GMT-05:00) America/Indiana / Winamac     </option>
<option  value="America/Iqaluit" {{ $user->timezone == "America/Iqaluit" ? 'selected="selected"':''}}>(GMT-05:00) America/Iqaluit   </option>
<option  value="America/Jamaica" {{ $user->timezone == "America/Jamaica" ? 'selected="selected"':''}}>(GMT-05:00) America/Jamaica   </option>
<option  value="America/Kentucky/Louisville" {{ $user->timezone == "America/Kentucky/Louisville" ? 'selected="selected"':''}}>(GMT-05:00) America/Kentucky / Louisville     </option>
<option  value="America/Kentucky/Monticello" {{ $user->timezone == "America/Kentucky/Monticello" ? 'selected="selected"':''}}>(GMT-05:00) America/Kentucky / Monticello     </option>
<option  value="America/Lima" {{ $user->timezone == "America/Lima" ? 'selected="selected"':''}}>(GMT-05:00) America/Lima    </option>
<option  value="America/Montreal" {{ $user->timezone == "America/Montreal" ? 'selected="selected"':''}}>(GMT-05:00) America/Montreal    </option>
<option  value="America/Nassau" {{ $user->timezone == "" ? 'selected="selected"':''}}>(GMT-05:00) America/Nassau    </option>
<option  value="America/New_York" {{ $user->timezone == "America/New_York" ? 'selected="selected"':''}}>(GMT-05:00) America/New York    </option>
<option  value="America/Nipigon" {{ $user->timezone == "America/Nipigon" ? 'selected="selected"':''}}>(GMT-05:00) America/Nipigon   </option>
<option  value="America/Panama" {{ $user->timezone == "America/Panama" ? 'selected="selected"':''}}>(GMT-05:00) America/Panama  </option>
<option  value="America/Pangnirtung" {{ $user->timezone == "America/Pangnirtung" ? 'selected="selected"':''}}>(GMT-05:00) America/Pangnirtung   </option>
<option  value="America/Port-au-Prince" {{ $user->timezone == "America/Port-au-Prince" ? 'selected="selected"':''}}>(GMT-05:00) America/Port-au-Prince  </option>
<option  value="America/Resolute" {{ $user->timezone == "America/Resolute" ? 'selected="selected"':''}}>(GMT-05:00) America/Resolute    </option>
<option  value="America/Thunder_Bay" {{ $user->timezone == "America/Thunder_Bay" ? 'selected="selected"':''}}>(GMT-05:00) America/Thunder Bay   </option>
<option  value="America/Toronto" {{ $user->timezone == "America/Toronto" ? 'selected="selected"':''}}>(GMT-05:00) America/Toronto   </option>
<option  value="America/Caracas" {{ $user->timezone == "America/Caracas" ? 'selected="selected"':''}}>(GMT-04:30) America/Caracas   </option>
<option  value="America/Anguilla" {{ $user->timezone == "America/Anguilla" ? 'selected="selected"':''}}>(GMT-04:00) America/Anguilla    </option>
<option  value="America/Antigua" {{ $user->timezone == "America/Antigua" ? 'selected="selected"':''}}>(GMT-04:00) America/Antigua   </option>
<option  value="America/Argentina/San_Luis" {{ $user->timezone == "America/Argentina/San_Luis" ? 'selected="selected"':''}}>(GMT-04:00) America/Argentina / San Luis    </option>
<option  value="America/Aruba" {{ $user->timezone == "America/Aruba" ? 'selected="selected"':''}}>(GMT-04:00) America/Aruba     </option>
<option  value="America/Asuncion" {{ $user->timezone == "America/Asuncion" ? 'selected="selected"':''}}>(GMT-04:00) America/Asuncion    </option>
<option  value="America/Barbados" {{ $user->timezone == "America/Barbados" ? 'selected="selected"':''}}>(GMT-04:00) America/Barbados    </option>
<option  value="America/Blanc-Sablon" {{ $user->timezone == "America/Blanc-Sablon" ? 'selected="selected"':''}}>(GMT-04:00) America/Blanc-Sablon    </option>
<option  value="America/Boa_Vista" {{ $user->timezone == "America/Boa_Vista" ? 'selected="selected"':''}}>(GMT-04:00) America/Boa Vista     </option>
<option  value="America/Campo_Grande" {{ $user->timezone == "America/Campo_Grande" ? 'selected="selected"':''}}>(GMT-04:00) America/Campo Grande    </option>
<option  value="America/Cuiaba" {{ $user->timezone == "America/Cuiaba" ? 'selected="selected"':''}}>(GMT-04:00) America/Cuiaba  </option>
<option  value="America/Curacao" {{ $user->timezone == "America/Curacao" ? 'selected="selected"':''}}>(GMT-04:00) America/Curacao   </option>
<option  value="America/Dominica" {{ $user->timezone == "America/Dominica" ? 'selected="selected"':''}}>(GMT-04:00) America/Dominica    </option>
<option  value="America/Eirunepe" {{ $user->timezone == "America/Eirunepe" ? 'selected="selected"':''}}>(GMT-04:00) America/Eirunepe    </option>
<option  value="America/Glace_Bay" {{ $user->timezone == "America/Glace_Bay" ? 'selected="selected"':''}}>(GMT-04:00) America/Glace Bay     </option>
<option  value="America/Goose_Bay" {{ $user->timezone == "America/Goose_Bay" ? 'selected="selected"':''}}>(GMT-04:00) America/Goose Bay     </option>
<option  value="America/Grenada" {{ $user->timezone == "America/Grenada" ? 'selected="selected"':''}}>(GMT-04:00) America/Grenada   </option>
<option  value="America/Guadeloupe" {{ $user->timezone == "America/Guadeloupe" ? 'selected="selected"':''}}>(GMT-04:00) America/Guadeloupe  </option>
<option  value="America/Guyana" {{ $user->timezone == "America/Guyana" ? 'selected="selected"':''}}>(GMT-04:00) America/Guyana  </option>
<option  value="America/Halifax" {{ $user->timezone == "America/Halifax" ? 'selected="selected"':''}}>(GMT-04:00) America/Halifax   </option>
<option  value="America/La_Paz" {{ $user->timezone == "America/La_Paz" ? 'selected="selected"':''}}>(GMT-04:00) America/La Paz  </option>
<option  value="America/Manaus" {{ $user->timezone == "America/Manaus" ? 'selected="selected"':''}}>(GMT-04:00) America/Manaus  </option>
<option  value="America/Martinique" {{ $user->timezone == "America/Martinique" ? 'selected="selected"':''}}>(GMT-04:00) America/Martinique  </option>
<option  value="America/Moncton" {{ $user->timezone == "America/Moncton" ? 'selected="selected"':''}}>(GMT-04:00) America/Moncton   </option>
<option  value="America/Montserrat" {{ $user->timezone == "America/Montserrat" ? 'selected="selected"':''}}>(GMT-04:00) America/Montserrat  </option>
<option  value="America/Port_of_Spain" {{ $user->timezone == "America/Port_of_Spain" ? 'selected="selected"':''}}>(GMT-04:00) America/Port of Spain     </option>
<option  value="America/Porto_Velho" {{ $user->timezone == "America/Porto_Velho" ? 'selected="selected"':''}}>(GMT-04:00) America/Porto Velho   </option>
<option  value="America/Puerto_Rico" {{ $user->timezone == "America/Puerto_Rico" ? 'selected="selected"':''}}>(GMT-04:00) America/Puerto Rico   </option>
<option  value="America/Rio_Branco" {{ $user->timezone == "America/Rio_Branco" ? 'selected="selected"':''}}>(GMT-04:00) America/Rio Branco  </option>
<option  value="America/Santiago" {{ $user->timezone == "America/Santiago" ? 'selected="selected"':''}}>(GMT-04:00) America/Santiago    </option>
<option  value="America/Santo_Domingo" {{ $user->timezone == "America/Santo_Domingo" ? 'selected="selected"':''}}>(GMT-04:00) America/Santo Domingo     </option>
<option  value="America/St_Kitts" {{ $user->timezone == "America/St_Kitts" ? 'selected="selected"':''}}>(GMT-04:00) America/St Kitts    </option>
<option  value="America/St_Lucia" {{ $user->timezone == "America/St_Lucia" ? 'selected="selected"':''}}>(GMT-04:00) America/St Lucia    </option>
<option  value="America/St_Thomas" {{ $user->timezone == "America/St_Thomas" ? 'selected="selected"':''}}>(GMT-04:00) America/St Thomas     </option>
<option  value="America/St_Vincent" {{ $user->timezone == "America/St_Vincent" ? 'selected="selected"':''}}>(GMT-04:00) America/St Vincent  </option>
<option  value="America/Thule" {{ $user->timezone == "America/Thule" ? 'selected="selected"':''}}>(GMT-04:00) America/Thule     </option>
<option  value="America/Tortola" {{ $user->timezone == "America/Tortola" ? 'selected="selected"':''}}>(GMT-04:00) America/Tortola   </option>
<option  value="America/St_Johns" {{ $user->timezone == "America/St_Johns" ? 'selected="selected"':''}}>(GMT-03:30) America/St Johns    </option>
<option  value="America/Araguaina" {{ $user->timezone == "America/Araguaina" ? 'selected="selected"':''}}>(GMT-03:00) America/Araguaina     </option>
<option  value="America/Argentina/Buenos_Aires" {{ $user->timezone == "America/Argentina/Buenos_Aires" ? 'selected="selected"':''}}>(GMT-03:00) America/Argentina / Buenos Aires    </option>
<option  value="America/Argentina/Catamarca" {{ $user->timezone == "America/Argentina/Catamarca" ? 'selected="selected"':''}}>(GMT-03:00) America/Argentina / Catamarca     </option>
<option  value="America/Argentina/Cordoba" {{ $user->timezone == "America/Argentina/Cordoba" ? 'selected="selected"':''}}>(GMT-03:00) America/Argentina / Cordoba   </option>
<option  value="America/Argentina/Jujuy" {{ $user->timezone == "America/Argentina/Jujuy" ? 'selected="selected"':''}}>(GMT-03:00) America/Argentina / Jujuy     </option>
<option  value="America/Argentina/La_Rioja" {{ $user->timezone == "America/Argentina/La_Rioja" ? 'selected="selected"':''}}>(GMT-03:00) America/Argentina / La Rioja    </option>
<option  value="America/Argentina/Mendoza" {{ $user->timezone == "America/Argentina/Mendoza" ? 'selected="selected"':''}}>(GMT-03:00) America/Argentina / Mendoza   </option>
<option  value="America/Argentina/Rio_Gallegos" {{ $user->timezone == "America/Argentina/Rio_Gallegos" ? 'selected="selected"':''}}>(GMT-03:00) America/Argentina / Rio Gallegos    </option>
<option  value="America/Argentina/Salta" {{ $user->timezone == "America/Argentina/Salta" ? 'selected="selected"':''}}>(GMT-03:00) America/Argentina / Salta     </option>
<option  value="America/Argentina/San_Juan" {{ $user->timezone == "America/Argentina/San_Juan" ? 'selected="selected"':''}}>(GMT-03:00) America/Argentina / San Juan    </option>
<option  value="America/Argentina/Tucuman" {{ $user->timezone == "America/Argentina/Tucuman" ? 'selected="selected"':''}}>(GMT-03:00) America/Argentina / Tucuman   </option>
<option  value="America/Argentina/Ushuaia" {{ $user->timezone == "America/Argentina/Ushuaia" ? 'selected="selected"':''}}>(GMT-03:00) America/Argentina / Ushuaia   </option>
<option  value="America/Bahia" {{ $user->timezone == "America/Bahia" ? 'selected="selected"':''}}>(GMT-03:00) America/Bahia     </option>
<option  value="America/Belem" {{ $user->timezone == "America/Belem" ? 'selected="selected"':''}}>(GMT-03:00) America/Belem     </option>
<option  value="America/Cayenne" {{ $user->timezone == "America/Cayenne" ? 'selected="selected"':''}}>(GMT-03:00) America/Cayenne   </option>
<option  value="America/Fortaleza" {{ $user->timezone == "America/Fortaleza" ? 'selected="selected"':''}}>(GMT-03:00) America/Fortaleza     </option>
<option  value="America/Godthab" {{ $user->timezone == "America/Godthab" ? 'selected="selected"':''}}>(GMT-03:00) America/Godthab   </option>
<option  value="America/Maceio" {{ $user->timezone == "America/Maceio" ? 'selected="selected"':''}}>(GMT-03:00) America/Maceio  </option>
<option  value="America/Miquelon" {{ $user->timezone == "America/Miquelon" ? 'selected="selected"':''}}>(GMT-03:00) America/Miquelon    </option>
<option  value="America/Montevideo" {{ $user->timezone == "America/Montevideo" ? 'selected="selected"':''}}>(GMT-03:00) America/Montevideo  </option>
<option  value="America/Paramaribo" {{ $user->timezone == "America/Paramaribo" ? 'selected="selected"':''}}>(GMT-03:00) America/Paramaribo  </option>
<option  value="America/Recife" {{ $user->timezone == "America/Recife" ? 'selected="selected"':''}}>(GMT-03:00) America/Recife  </option>
<option  value="America/Santarem" {{ $user->timezone == "America/Santarem" ? 'selected="selected"':''}}>(GMT-03:00) America/Santarem    </option>
<option  value="America/Sao_Paulo" {{ $user->timezone == "America/Sao_Paulo" ? 'selected="selected"':''}}>(GMT-03:00) America/Sao Paulo     </option>
<option  value="America/Noronha" {{ $user->timezone == "America/Noronha" ? 'selected="selected"':''}}>(GMT-02:00) America/Noronha   </option>
<option  value="America/Scoresbysund" {{ $user->timezone == "America/Scoresbysund" ? 'selected="selected"':''}}>(GMT-01:00) America/Scoresbysund    </option>
<option  value="America/Danmarkshavn" {{ $user->timezone == "America/Danmarkshavn" ? 'selected="selected"':''}}>(GMT+00:00) America/Danmarkshavn    </option>
<option  value="Antarctica/Palmer" {{ $user->timezone == "Antarctica/Palmer" ? 'selected="selected"':''}}>(GMT-04:00) Antarctica/Palmer     </option>
<option  value="Antarctica/Rothera" {{ $user->timezone == "Antarctica/Rothera" ? 'selected="selected"':''}}>(GMT-03:00) Antarctica/Rothera  </option>
<option  value="Antarctica/Syowa" {{ $user->timezone == "Antarctica/Syowa" ? 'selected="selected"':''}}>(GMT+03:00) Antarctica/Syowa    </option>
<option  value="Antarctica/Mawson" {{ $user->timezone == "Antarctica/Mawson" ? 'selected="selected"':''}}>(GMT+05:00) Antarctica/Mawson     </option>
<option  value="Antarctica/Vostok" {{ $user->timezone == "Antarctica/Vostok" ? 'selected="selected"':''}}>(GMT+06:00) Antarctica/Vostok     </option>
<option  value="Antarctica/Davis" {{ $user->timezone == "Antarctica/Davis" ? 'selected="selected"':''}}>(GMT+07:00) Antarctica/Davis    </option>
<option  value="Antarctica/Casey" {{ $user->timezone == "Antarctica/Casey" ? 'selected="selected"':''}}>(GMT+08:00) Antarctica/Casey    </option>
<option  value="Antarctica/DumontDUrville" {{ $user->timezone == "Antarctica/DumontDUrville" ? 'selected="selected"':''}}>(GMT+10:00) Antarctica/DumontDUrville     </option>
<option  value="Antarctica/Macquarie" {{ $user->timezone == "Antarctica/Macquarie" ? 'selected="selected"':''}}>(GMT+11:00) Antarctica/Macquarie    </option>
<option  value="Antarctica/McMurdo" {{ $user->timezone == "Antarctica/McMurdo" ? 'selected="selected"':''}}>(GMT+12:00) Antarctica/McMurdo  </option>
<option  value="Asia/Amman" {{ $user->timezone == "Asia/Amman" ? 'selected="selected"':''}}>(GMT+02:00) Asia/Amman  </option>
<option  value="Asia/Beirut" {{ $user->timezone == "Asia/Beirut" ? 'selected="selected"':''}}>(GMT+02:00) Asia/Beirut   </option>
<option  value="Asia/Damascus" {{ $user->timezone == "Asia/Damascus" ? 'selected="selected"':''}}>(GMT+02:00) Asia/Damascus     </option>
<option  value="Asia/Gaza" {{ $user->timezone == "Asia/Gaza" ? 'selected="selected"':''}}>(GMT+02:00) Asia/Gaza     </option>
<option  value="Asia/Jerusalem" {{ $user->timezone == "Asia/Jerusalem" ? 'selected="selected"':''}}>(GMT+02:00) Asia/Jerusalem  </option>
<option  value="Asia/Nicosia" {{ $user->timezone == "Asia/Nicosia" ? 'selected="selected"':''}}>(GMT+02:00) Asia/Nicosia    </option>
<option  value="Asia/Aden" {{ $user->timezone == "Asia/Aden" ? 'selected="selected"':''}}>(GMT+03:00) Asia/Aden     </option>
<option  value="Asia/Baghdad" {{ $user->timezone == "Asia/Baghdad" ? 'selected="selected"':''}}>(GMT+03:00) Asia/Baghdad    </option>
<option  value="Asia/Bahrain" {{ $user->timezone == "Asia/Bahrain" ? 'selected="selected"':''}}>(GMT+03:00) Asia/Bahrain    </option>
<option  value="Asia/Kuwait" {{ $user->timezone == "Asia/Kuwait" ? 'selected="selected"':''}}>(GMT+03:00) Asia/Kuwait   </option>
<option  value="Asia/Qatar" {{ $user->timezone == "Asia/Qatar" ? 'selected="selected"':''}}>(GMT+03:00) Asia/Qatar  </option>
<option  value="Asia/Riyadh" {{ $user->timezone == "Asia/Riyadh" ? 'selected="selected"':''}}>(GMT+03:00) Asia/Riyadh   </option>
<option  value="Asia/Tehran" {{ $user->timezone == "Asia/Tehran" ? 'selected="selected"':''}}>(GMT+03:30) Asia/Tehran   </option>
<option  value="Asia/Baku" {{ $user->timezone == "Asia/Baku" ? 'selected="selected"':''}}>(GMT+04:00) Asia/Baku     </option>
<option  value="Asia/Dubai" {{ $user->timezone == "Asia/Dubai" ? 'selected="selected"':''}}>(GMT+04:00) Asia/Dubai  </option>
<option  value="Asia/Muscat" {{ $user->timezone == "Asia/Muscat" ? 'selected="selected"':''}}>(GMT+04:00) Asia/Muscat   </option>
<option  value="Asia/Tbilisi" {{ $user->timezone == "Asia/Tbilisi" ? 'selected="selected"':''}}>(GMT+04:00) Asia/Tbilisi    </option>
<option  value="Asia/Yerevan" {{ $user->timezone == "Asia/Yerevan" ? 'selected="selected"':''}}>(GMT+04:00) Asia/Yerevan    </option>
<option  value="Asia/Kabul" {{ $user->timezone == "Asia/Kabul" ? 'selected="selected"':''}}>(GMT+04:30) Asia/Kabul  </option>
<option  value="Asia/Aqtau" {{ $user->timezone == "Asia/Aqtau" ? 'selected="selected"':''}}>(GMT+05:00) Asia/Aqtau  </option>
<option  value="Asia/Aqtobe" {{ $user->timezone == "Asia/Aqtobe" ? 'selected="selected"':''}}>(GMT+05:00) Asia/Aqtobe   </option>
<option  value="Asia/Ashgabat" {{ $user->timezone == "Asia/Ashgabat" ? 'selected="selected"':''}}>(GMT+05:00) Asia/Ashgabat     </option>
<option  value="Asia/Dushanbe" {{ $user->timezone == "Asia/Dushanbe" ? 'selected="selected"':''}}>(GMT+05:00) Asia/Dushanbe     </option>
<option  value="Asia/Karachi" {{ $user->timezone == "Asia/Karachi" ? 'selected="selected"':''}}>(GMT+05:00) Asia/Karachi    </option>
<option  value="Asia/Oral" {{ $user->timezone == "Asia/Oral" ? 'selected="selected"':''}}>(GMT+05:00) Asia/Oral     </option>
<option  value="Asia/Samarkand" {{ $user->timezone == "Asia/Samarkand" ? 'selected="selected"':''}}>(GMT+05:00) Asia/Samarkand  </option>
<option  value="Asia/Tashkent" {{ $user->timezone == "Asia/Tashkent" ? 'selected="selected"':''}}>(GMT+05:00) Asia/Tashkent     </option>
<option  value="Asia/Yekaterinburg" {{ $user->timezone == "Asia/Yekaterinburg" ? 'selected="selected"':''}}>(GMT+05:00) Asia/Yekaterinburg  </option>
<option  value="Asia/Colombo" {{ $user->timezone == "Asia/Colombo" ? 'selected="selected"':''}}>(GMT+05:30) Asia/Colombo    </option>
<option  value="Asia/Kolkata" {{ $user->timezone == "Asia/Kolkata" ? 'selected="selected"':''}}>(GMT+05:30) Asia/Kolkata    </option>
<option  value="Asia/Kathmandu" {{ $user->timezone == "Asia/Kathmandu" ? 'selected="selected"':''}}>(GMT+05:45) Asia/Kathmandu  </option>
<option  value="Asia/Almaty" {{ $user->timezone == "Asia/Almaty" ? 'selected="selected"':''}}>(GMT+06:00) Asia/Almaty   </option>
<option  value="Asia/Bishkek" {{ $user->timezone == "Asia/Bishkek" ? 'selected="selected"':''}}>(GMT+06:00) Asia/Bishkek    </option>
<option  value="Asia/Dhaka" {{ $user->timezone == "Asia/Dhaka" ? 'selected="selected"':''}}>(GMT+06:00) Asia/Dhaka  </option>
<option  value="Asia/Novokuznetsk" {{ $user->timezone == "Asia/Novokuznetsk" ? 'selected="selected"':''}}>(GMT+06:00) Asia/Novokuznetsk     </option>
<option  value="Asia/Novosibirsk" {{ $user->timezone == "Asia/Novosibirsk" ? 'selected="selected"':''}}>(GMT+06:00) Asia/Novosibirsk    </option>
<option  value="Asia/Omsk" {{ $user->timezone == "Asia/Omsk" ? 'selected="selected"':''}}>(GMT+06:00) Asia/Omsk     </option>
<option  value="Asia/Qyzylorda" {{ $user->timezone == "Asia/Qyzylorda" ? 'selected="selected"':''}}>(GMT+06:00) Asia/Qyzylorda  </option>
<option  value="Asia/Thimphu" {{ $user->timezone == "Asia/Thimphu" ? 'selected="selected"':''}}>(GMT+06:00) Asia/Thimphu    </option>
<option  value="Asia/Rangoon" {{ $user->timezone == "Asia/Rangoon" ? 'selected="selected"':''}}>(GMT+06:30) Asia/Rangoon    </option>
<option  value="Asia/Bangkok" {{ $user->timezone == "Asia/Bangkok" ? 'selected="selected"':''}}>(GMT+07:00) Asia/Bangkok    </option>
<option  value="Asia/Ho_Chi_Minh" {{ $user->timezone == "Asia/Ho_Chi_Minh" ? 'selected="selected"':''}}>(GMT+07:00) Asia/Ho Chi Minh    </option>
<option  value="Asia/Hovd" {{ $user->timezone == "Asia/Hovd" ? 'selected="selected"':''}}>(GMT+07:00) Asia/Hovd     </option>
<option  value="Asia/Jakarta" {{ $user->timezone == "Asia/Jakarta" ? 'selected="selected"':''}}>(GMT+07:00) Asia/Jakarta    </option>
<option  value="Asia/Krasnoyarsk" {{ $user->timezone == "Asia/Krasnoyarsk" ? 'selected="selected"':''}}>(GMT+07:00) Asia/Krasnoyarsk    </option>
<option  value="Asia/Phnom_Penh" {{ $user->timezone == "Asia/Phnom_Penh" ? 'selected="selected"':''}}>(GMT+07:00) Asia/Phnom Penh   </option>
<option  value="Asia/Pontianak" {{ $user->timezone == "Asia/Pontianak" ? 'selected="selected"':''}}>(GMT+07:00) Asia/Pontianak  </option>
<option  value="Asia/Vientiane" {{ $user->timezone == "Asia/Vientiane" ? 'selected="selected"':''}}>(GMT+07:00) Asia/Vientiane  </option>
<option  value="Asia/Brunei" {{ $user->timezone == "Asia/Brunei" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Brunei   </option>
<option  value="Asia/Choibalsan" {{ $user->timezone == "Asia/Choibalsan" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Choibalsan   </option>
<option  value="Asia/Chongqing" {{ $user->timezone == "Asia/Chongqing" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Chongqing  </option>
<option  value="Asia/Harbin" {{ $user->timezone == "Asia/Harbin" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Harbin   </option>
<option  value="Asia/Hong_Kong" {{ $user->timezone == "Asia/Hong_Kong" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Hong Kong  </option>
<option  value="Asia/Irkutsk" {{ $user->timezone == "Asia/Irkutsk" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Irkutsk    </option>
<option  value="Asia/Kashgar" {{ $user->timezone == "Asia/Kashgar" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Kashgar    </option>
<option  value="Asia/Kuala_Lumpur" {{ $user->timezone == "Asia/Kuala_Lumpur" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Kuala Lumpur     </option>
<option  value="Asia/Kuching" {{ $user->timezone == "Asia/Kuching" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Kuching    </option>
<option  value="Asia/Macau" {{ $user->timezone == "Asia/Macau" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Macau  </option>
<option  value="Asia/Makassar" {{ $user->timezone == "Asia/Makassar" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Makassar     </option>
<option  value="Asia/Manila" {{ $user->timezone == "Asia/Manila" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Manila   </option>
<option  value="Asia/Shanghai" {{ $user->timezone == "Asia/Shanghai" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Shanghai     </option>
<option  value="Asia/Singapore" {{ $user->timezone == "Asia/Singapore" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Singapore  </option>
<option  value="Asia/Taipei" {{ $user->timezone == "Asia/Taipei" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Taipei   </option>
<option  value="Asia/Ulaanbaatar" {{ $user->timezone == "Asia/Ulaanbaatar" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Ulaanbaatar    </option>
<option  value="Asia/Urumqi" {{ $user->timezone == "Asia/Urumqi" ? 'selected="selected"':''}}>(GMT+08:00) Asia/Urumqi   </option>
<option  value="Asia/Dili" {{ $user->timezone == "Asia/Dili" ? 'selected="selected"':''}}>(GMT+09:00) Asia/Dili     </option>
<option  value="Asia/Jayapura" {{ $user->timezone == "Asia/Jayapura" ? 'selected="selected"':''}}>(GMT+09:00) Asia/Jayapura     </option>
<option  value="Asia/Pyongyang" {{ $user->timezone == "Asia/Pyongyang" ? 'selected="selected"':''}}>(GMT+09:00) Asia/Pyongyang  </option>
<option  value="Asia/Seoul" {{ $user->timezone == "Asia/Seoul" ? 'selected="selected"':''}}>(GMT+09:00) Asia/Seoul  </option>
<option  value="Asia/Tokyo" {{ $user->timezone == "Asia/Tokyo" ? 'selected="selected"':''}}>(GMT+09:00) Asia/Tokyo  </option>
<option  value="Asia/Yakutsk" {{ $user->timezone == "Asia/Yakutsk" ? 'selected="selected"':''}}>(GMT+09:00) Asia/Yakutsk    </option>
<option  value="Asia/Sakhalin" {{ $user->timezone == "Asia/Sakhalin" ? 'selected="selected"':''}}>(GMT+10:00) Asia/Sakhalin     </option>
<option  value="Asia/Vladivostok" {{ $user->timezone == "Asia/Vladivostok" ? 'selected="selected"':''}}>(GMT+10:00) Asia/Vladivostok    </option>
<option  value="Asia/Anadyr" {{ $user->timezone == "Asia/Anadyr" ? 'selected="selected"':''}}>(GMT+11:00) Asia/Anadyr   </option>
<option  value="Asia/Kamchatka" {{ $user->timezone == "Asia/Kamchatka" ? 'selected="selected"':''}}>(GMT+11:00) Asia/Kamchatka  </option>
<option  value="Asia/Magadan" {{ $user->timezone == "Asia/Magadan" ? 'selected="selected"':''}}>(GMT+11:00) Asia/Magadan    </option>
<option  value="Atlantic/Bermuda" {{ $user->timezone == "Atlantic/Bermuda" ? 'selected="selected"':''}}>(GMT-04:00) Atlantic/Bermuda    </option>
<option  value="Atlantic/Stanley" {{ $user->timezone == "Atlantic/Stanley" ? 'selected="selected"':''}}>(GMT-04:00) Atlantic/Stanley    </option>
<option  value="Atlantic/South_Georgia" {{ $user->timezone == "Atlantic/South_Georgia" ? 'selected="selected"':''}}>(GMT-02:00) Atlantic/South Georgia  </option>
<option  value="Atlantic/Azores" {{ $user->timezone == "Atlantic/Azores" ? 'selected="selected"':''}}>(GMT-01:00) Atlantic/Azores   </option>
<option  value="Atlantic/Cape_Verde" {{ $user->timezone == "Atlantic/Cape_Verde" ? 'selected="selected"':''}}>(GMT-01:00) Atlantic/Cape Verde   </option>
<option  value="Atlantic/Canary" {{ $user->timezone == "Atlantic/Canary" ? 'selected="selected"':''}}>(GMT+00:00) Atlantic/Canary   </option>
<option  value="Atlantic/Faroe" {{ $user->timezone == "Atlantic/Faroe" ? 'selected="selected"':''}}>(GMT+00:00) Atlantic/Faroe  </option>
<option  value="Atlantic/Madeira" {{ $user->timezone == "Atlantic/Madeira" ? 'selected="selected"':''}}>(GMT+00:00) Atlantic/Madeira    </option>
<option  value="Atlantic/Reykjavik" {{ $user->timezone == "Atlantic/Reykjavik" ? 'selected="selected"':''}}>(GMT+00:00) Atlantic/Reykjavik  </option>
<option  value="Atlantic/St_Helena" {{ $user->timezone == "Atlantic/St_Helena" ? 'selected="selected"':''}}>(GMT+00:00) Atlantic/St Helena  </option>
<option  value="Australia/Perth" {{ $user->timezone == "Australia/Perth" ? 'selected="selected"':''}}>(GMT+08:00) Australia/Perth   </option>
<option  value="Australia/Eucla" {{ $user->timezone == "Australia/Eucla" ? 'selected="selected"':''}}>(GMT+08:45) Australia/Eucla   </option>
<option  value="Australia/Adelaide" {{ $user->timezone == "Australia/Adelaide" ? 'selected="selected"':''}}>(GMT+09:30) Australia/Adelaide  </option>
<option  value="Australia/Broken_Hill" {{ $user->timezone == "Australia/Broken_Hill" ? 'selected="selected"':''}}>(GMT+09:30) Australia/Broken Hill     </option>
<option  value="Australia/Darwin" {{ $user->timezone == "Australia/Darwin" ? 'selected="selected"':''}}>(GMT+09:30) Australia/Darwin    </option>
<option  value="Australia/Brisbane" {{ $user->timezone == "Australia/Brisbane" ? 'selected="selected"':''}}>(GMT+10:00) Australia/Brisbane  </option>
<option  value="Australia/Currie" {{ $user->timezone == "Australia/Currie" ? 'selected="selected"':''}}>(GMT+10:00) Australia/Currie    </option>
<option  value="Australia/Hobart" {{ $user->timezone == "Australia/Hobart" ? 'selected="selected"':''}}>(GMT+10:00) Australia/Hobart    </option>
<option  value="Australia/Lindeman" {{ $user->timezone == "Australia/Lindeman" ? 'selected="selected"':''}}>(GMT+10:00) Australia/Lindeman  </option>
<option  value="Australia/Melbourne" {{ $user->timezone == "Australia/Melbourne" ? 'selected="selected"':''}}>(GMT+10:00) Australia/Melbourne   </option>
<option  value="Australia/Sydney" {{ $user->timezone == "Australia/Sydney" ? 'selected="selected"':''}}>(GMT+10:00) Australia/Sydney    </option>
<option  value="Australia/Lord_Howe" {{ $user->timezone == "Australia/Lord_Howe" ? 'selected="selected"':''}}>(GMT+10:30) Australia/Lord Howe   </option>
<option  value="Europe/Dublin" {{ $user->timezone == "Europe/Dublin" ? 'selected="selected"':''}}>(GMT+00:00) Europe/Dublin     </option>
<option  value="Europe/Lisbon" {{ $user->timezone == "Europe/Lisbon" ? 'selected="selected"':''}}>(GMT+00:00) Europe/Lisbon     </option>
<option  value="Europe/London" {{ $user->timezone == "Europe/London" ? 'selected="selected"':''}}>(GMT+00:00) Europe/London     </option>
<option  value="Europe/Amsterdam" {{ $user->timezone == "Europe/Amsterdam" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Amsterdam    </option>
<option  value="Europe/Andorra" {{ $user->timezone == "Europe/Andorra" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Andorra  </option>
<option  value="Europe/Belgrade" {{ $user->timezone == "Europe/Belgrade" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Belgrade   </option>
<option  value="Europe/Berlin" {{ $user->timezone == "Europe/Berlin" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Berlin     </option>
<option  value="Europe/Brussels" {{ $user->timezone == "Europe/Brussels" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Brussels   </option>
<option  value="Europe/Budapest" {{ $user->timezone == "Europe/Budapest" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Budapest   </option>
<option  value="Europe/Copenhagen" {{ $user->timezone == "Europe/Copenhagen" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Copenhagen     </option>
<option  value="Europe/Gibraltar" {{ $user->timezone == "Europe/Gibraltar" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Gibraltar    </option>
<option  value="Europe/Luxembourg" {{ $user->timezone == "Europe/Luxembourg" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Luxembourg     </option>
<option  value="Europe/Madrid" {{ $user->timezone == "Europe/Madrid" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Madrid     </option>
<option  value="Europe/Malta" {{ $user->timezone == "Europe/Malta" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Malta    </option>
<option  value="Europe/Monaco" {{ $user->timezone == "Europe/Monaco" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Monaco     </option>
<option  value="Europe/Oslo" {{ $user->timezone == "Europe/Oslo" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Oslo   </option>
<option  value="Europe/Paris" {{ $user->timezone == "Europe/Paris" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Paris    </option>
<option  value="Europe/Prague" {{ $user->timezone == "Europe/Prague" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Prague     </option>
<option  value="Europe/Rome" {{ $user->timezone == "Europe/Rome" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Rome   </option>
<option  value="Europe/Stockholm" {{ $user->timezone == "Europe/Stockholm" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Stockholm    </option>
<option  value="Europe/Tirane" {{ $user->timezone == "Europe/Tirane" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Tirane     </option>
<option  value="Europe/Vaduz" {{ $user->timezone == "Europe/Vaduz" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Vaduz    </option>
<option  value="Europe/Vienna" {{ $user->timezone == "Europe/Vienna" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Vienna     </option>
<option  value="Europe/Warsaw" {{ $user->timezone == "Europe/Warsaw" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Warsaw     </option>
<option  value="Europe/Zurich" {{ $user->timezone == "Europe/Zurich" ? 'selected="selected"':''}}>(GMT+01:00) Europe/Zurich     </option>
<option  value="Europe/Athens" {{ $user->timezone == "Europe/Athens" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Athens     </option>
<option  value="Europe/Bucharest" {{ $user->timezone == "Europe/Bucharest" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Bucharest    </option>
<option  value="Europe/Chisinau" {{ $user->timezone == "Europe/Chisinau" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Chisinau   </option>
<option  value="Europe/Helsinki" {{ $user->timezone == "Europe/Helsinki" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Helsinki   </option>
<option  value="Europe/Istanbul" {{ $user->timezone == "Europe/Istanbul" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Istanbul   </option>
<option  value="Europe/Kaliningrad" {{ $user->timezone == "Europe/Kaliningrad" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Kaliningrad  </option>
<option  value="Europe/Kiev" {{ $user->timezone == "Europe/Kiev" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Kiev   </option>
<option  value="Europe/Minsk" {{ $user->timezone == "Europe/Minsk" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Minsk    </option>
<option  value="Europe/Riga" {{ $user->timezone == "Europe/Riga" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Riga   </option>
<option  value="Europe/Simferopol" {{ $user->timezone == "Europe/Simferopol" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Simferopol     </option>
<option  value="Europe/Sofia" {{ $user->timezone == "Europe/Sofia" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Sofia    </option>
<option  value="Europe/Tallinn" {{ $user->timezone == "Europe/Tallinn" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Tallinn  </option>
<option  value="Europe/Uzhgorod" {{ $user->timezone == "Europe/Uzhgorod" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Uzhgorod   </option>
<option  value="Europe/Vilnius" {{ $user->timezone == "Europe/Vilnius" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Vilnius  </option>
<option  value="Europe/Zaporozhye" {{ $user->timezone == "Europe/Zaporozhye" ? 'selected="selected"':''}}>(GMT+02:00) Europe/Zaporozhye     </option>
<option  value="Europe/Moscow" {{ $user->timezone == "Europe/Moscow" ? 'selected="selected"':''}}>(GMT+03:00) Europe/Moscow     </option>
<option  value="Europe/Samara" {{ $user->timezone == "Europe/Samara" ? 'selected="selected"':''}}>(GMT+03:00) Europe/Samara     </option>
<option  value="Europe/Volgograd" {{ $user->timezone == "Europe/Volgograd" ? 'selected="selected"':''}}>(GMT+03:00) Europe/Volgograd    </option>
<option  value="Indian/Antananarivo" {{ $user->timezone == "Indian/Antananarivo" ? 'selected="selected"':''}}>(GMT+03:00) Indian/Antananarivo   </option>
<option  value="Indian/Comoro" {{ $user->timezone == "Indian/Comoro" ? 'selected="selected"':''}}>(GMT+03:00) Indian/Comoro     </option>
<option  value="Indian/Mayotte" {{ $user->timezone == "Indian/Mayotte" ? 'selected="selected"':''}}>(GMT+03:00) Indian/Mayotte  </option>
<option  value="Indian/Mahe" {{ $user->timezone == "Indian/Mahe" ? 'selected="selected"':''}}>(GMT+04:00) Indian/Mahe   </option>
<option  value="Indian/Mauritius" {{ $user->timezone == "Indian/Mauritius" ? 'selected="selected"':''}}>(GMT+04:00) Indian/Mauritius    </option>
<option  value="Indian/Reunion" {{ $user->timezone == "Indian/Reunion" ? 'selected="selected"':''}}>(GMT+04:00) Indian/Reunion  </option>
<option  value="Indian/Kerguelen" {{ $user->timezone == "Indian/Kerguelen" ? 'selected="selected"':''}}>(GMT+05:00) Indian/Kerguelen    </option>
<option  value="Indian/Maldives" {{ $user->timezone == "Indian/Maldives" ? 'selected="selected"':''}}>(GMT+05:00) Indian/Maldives   </option>
<option  value="Indian/Chagos" {{ $user->timezone == "Indian/Chagos" ? 'selected="selected"':''}}>(GMT+06:00) Indian/Chagos     </option>
<option  value="Indian/Cocos" {{ $user->timezone == "Indian/Cocos" ? 'selected="selected"':''}}>(GMT+06:30) Indian/Cocos    </option>
<option  value="Indian/Christmas" {{ $user->timezone == "Indian/Christmas" ? 'selected="selected"':''}}>(GMT+07:00) Indian/Christmas    </option>
<option  value="Pacific/Apia" {{ $user->timezone == "Pacific/Apia" ? 'selected="selected"':''}}>(GMT-11:00) Pacific/Apia    </option>
<option  value="Pacific/Midway" {{ $user->timezone == "Pacific/Midway" ? 'selected="selected"':''}}>(GMT-11:00) Pacific/Midway  </option>
<option  value="Pacific/Niue" {{ $user->timezone == "Pacific/Niue" ? 'selected="selected"':''}}>(GMT-11:00) Pacific/Niue    </option>
<option  value="Pacific/Pago_Pago" {{ $user->timezone == "Pacific/Pago_Pago" ? 'selected="selected"':''}}>(GMT-11:00) Pacific/Pago Pago     </option>
<option  value="Pacific/Fakaofo" {{ $user->timezone == "Pacific/Fakaofo" ? 'selected="selected"':''}}>(GMT-10:00) Pacific/Fakaofo   </option>
<option  value="Pacific/Honolulu" {{ $user->timezone == "Pacific/Honolulu" ? 'selected="selected"':''}}>(GMT-10:00) Pacific/Honolulu    </option>
<option  value="Pacific/Johnston" {{ $user->timezone == "Pacific/Johnston" ? 'selected="selected"':''}}>(GMT-10:00) Pacific/Johnston    </option>
<option  value="Pacific/Rarotonga" {{ $user->timezone == "Pacific/Rarotonga" ? 'selected="selected"':''}}>(GMT-10:00) Pacific/Rarotonga     </option>
<option  value="Pacific/Tahiti" {{ $user->timezone == "Pacific/Tahiti" ? 'selected="selected"':''}}>(GMT-10:00) Pacific/Tahiti  </option>
<option  value="Pacific/Marquesas" {{ $user->timezone == "Pacific/Marquesas" ? 'selected="selected"':''}}>(GMT-09:30) Pacific/Marquesas     </option>
<option  value="Pacific/Gambier" {{ $user->timezone == "Pacific/Gambier" ? 'selected="selected"':''}}>(GMT-09:00) Pacific/Gambier   </option>
<option  value="Pacific/Pitcairn" {{ $user->timezone == "Pacific/Pitcairn" ? 'selected="selected"':''}}>(GMT-08:00) Pacific/Pitcairn    </option>
<option  value="Pacific/Easter" {{ $user->timezone == "Pacific/Easter" ? 'selected="selected"':''}}>(GMT-06:00) Pacific/Easter  </option>
<option  value="Pacific/Galapagos" {{ $user->timezone == "Pacific/Galapagos" ? 'selected="selected"':''}}>(GMT-06:00) Pacific/Galapagos     </option>
<option  value="Pacific/Palau" {{ $user->timezone == "Pacific/Palau" ? 'selected="selected"':''}}>(GMT+09:00) Pacific/Palau     </option>
<option  value="Pacific/Chuuk" {{ $user->timezone == "Pacific/Chuuk" ? 'selected="selected"':''}}>(GMT+10:00) Pacific/Chuuk     </option>
<option  value="Pacific/Guam" {{ $user->timezone == "Pacific/Guam" ? 'selected="selected"':''}}>(GMT+10:00) Pacific/Guam    </option>
<option  value="Pacific/Port_Moresby" {{ $user->timezone == "Pacific/Port_Moresby" ? 'selected="selected"':''}}>(GMT+10:00) Pacific/Port Moresby    </option>
<option  value="Pacific/Saipan" {{ $user->timezone == "Pacific/Saipan" ? 'selected="selected"':''}}>(GMT+10:00) Pacific/Saipan  </option>
<option  value="Pacific/Efate" {{ $user->timezone == "Pacific/Efate" ? 'selected="selected"':''}}>(GMT+11:00) Pacific/Efate     </option>
<option  value="Pacific/Guadalcanal" {{ $user->timezone == "Pacific/Guadalcanal" ? 'selected="selected"':''}}>(GMT+11:00) Pacific/Guadalcanal   </option>
<option  value="Pacific/Kosrae" {{ $user->timezone == "Pacific/Kosrae" ? 'selected="selected"':''}}>(GMT+11:00) Pacific/Kosrae  </option>
<option  value="Pacific/Noumea" {{ $user->timezone == "Pacific/Noumea" ? 'selected="selected"':''}}>(GMT+11:00) Pacific/Noumea  </option>
<option  value="Pacific/Pohnpei" {{ $user->timezone == "Pacific/Pohnpei" ? 'selected="selected"':''}}>(GMT+11:00) Pacific/Pohnpei   </option>
<option  value="Pacific/Norfolk" {{ $user->timezone == "Pacific/Norfolk" ? 'selected="selected"':''}}>(GMT+11:30) Pacific/Norfolk   </option>
<option  value="Pacific/Auckland" {{ $user->timezone == "Pacific/Auckland" ? 'selected="selected"':''}}>(GMT+12:00) Pacific/Auckland    </option>
<option  value="Pacific/Fiji" {{ $user->timezone == "Pacific/Fiji" ? 'selected="selected"':''}}>(GMT+12:00) Pacific/Fiji    </option>
<option  value="Pacific/Funafuti" {{ $user->timezone == "Pacific/Funafuti" ? 'selected="selected"':''}}>(GMT+12:00) Pacific/Funafuti    </option>
<option  value="Pacific/Kwajalein" {{ $user->timezone == "Pacific/Kwajalein" ? 'selected="selected"':''}}>(GMT+12:00) Pacific/Kwajalein     </option>
<option  value="Pacific/Majuro" {{ $user->timezone == "Pacific/Majuro" ? 'selected="selected"':''}}>(GMT+12:00) Pacific/Majuro  </option>
<option  value="Pacific/Nauru" {{ $user->timezone == "Pacific/Nauru" ? 'selected="selected"':''}}>(GMT+12:00) Pacific/Nauru     </option>
<option  value="Pacific/Tarawa" {{ $user->timezone == "Pacific/Tarawa" ? 'selected="selected"':''}}>(GMT+12:00) Pacific/Tarawa  </option>
<option  value="Pacific/Wake" {{ $user->timezone == "Pacific/Wake" ? 'selected="selected"':''}}>(GMT+12:00) Pacific/Wake    </option>
<option  value="Pacific/Wallis" {{ $user->timezone == "Pacific/Wallis" ? 'selected="selected"':''}}>(GMT+12:00) Pacific/Wallis  </option>
<option  value="Pacific/Chatham" {{ $user->timezone == "Pacific/Chatham" ? 'selected="selected"':''}}>(GMT+12:45) Pacific/Chatham   </option>
<option  value="Pacific/Enderbury" {{ $user->timezone == "Pacific/Enderbury" ? 'selected="selected"':''}}>(GMT+13:00) Pacific/Enderbury     </option>
<option  value="Pacific/Tongatapu" {{ $user->timezone == "Pacific/Tongatapu" ? 'selected="selected"':''}}>(GMT+13:00) Pacific/Tongatapu     </option>
<option  value="Pacific/Kiritimati" {{ $user->timezone == "Pacific/Kiritimati" ? 'selected="selected"':''}}>(GMT+14:00) Pacific/Kiritimati</option> 