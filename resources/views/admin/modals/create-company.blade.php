<div class="modal fade" id="create-new-company-modal" tabindex="-1" role="dialog" aria-labelledby="create-new-company-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create new company</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="company_name" class="control-label">Company Name</label>
                        <input id="company_name"
                               type="text"
                               class="form-control"
                               name="company_name"
                               placeholder="Type your company name">
                    </div>
                    <div class="form-group">
                        {{ Form::label('company_type', 'Type of company', ['class' => 'control-label']) }}
                        <div class="select-wrap">
                            @if (!empty($type))
                                {{ Form::select('company_type', $types, $type, ['class'=>'form-control', 'placeholder'=>'Select your company type', 'disabled' => true]) }}
                            @else
                                {{ Form::select('company_type', $types, null, ['class'=>'form-control', 'placeholder'=>'Select your company type']) }}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="company_logo" class="control-label">Company logo</label>
                        <div class="control-sublabel">Company image need to be <span class="bold">width: 300px</span> and <span
                                class="bold">height: 300px</span></div>
                        <div class="drop-zone drop-croppie-company drop-zone-organizer vertical">
                            <input id="company_logo"
                                   type="hidden"
                                   class="form-control"
                                   name="company_logo">
                        </div>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-primary-blur btn-xl" id="company-save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>