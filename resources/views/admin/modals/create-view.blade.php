<div class="modal fade" id="modalAddView" tabindex="-1" role="dialog" aria-labelledby="modalAddView" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add view</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="top-checkbox-wrap">
                            <input class="custom-checkbox" name="is_default" type="checkbox" id="defaultViewName">
                            <label for="defaultViewName" class="custom-checkbox-label">Set as default</label>
                        </div>
                        <label for="view_name">View name</label>
                        <input type="text" class="form-control" id="viewName" placeholder="Type your view name">
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-primary-blur btn-xl" id="saveListViewBtn">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>