<div class="modal fade" id="notifyModal" tabindex="-1" role="dialog" aria-labelledby="notifyModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-with-logo" role="document">
        <div class="modal-content text-center">
            <div class="modal-body">
                <h5 class="content-title"></h5>
                <div class="content-sub-title"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-lg" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>