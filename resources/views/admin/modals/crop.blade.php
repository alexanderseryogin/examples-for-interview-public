<div class="modal fade drop-croppie" id="drop-croppie-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="image-container">
                    <div id="drop-croppie-block" class="center-block text-center"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="drop-croppie-submit-btn" class="btn btn-primary btn-lg">Save</button>
                <button type="button" class="btn btn-outline-primary btn-lg" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
