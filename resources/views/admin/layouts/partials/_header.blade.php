<header class="header-noauth main-header">
    <div class="nav-brand">
        <a href="{{route('homepage')}}">
            <img src="{{ asset('images/redtix_logo.png') }}" alt="Redtix">
        </a>
    </div>

    @isset($showAdminNavigation)
        <div class="navbar-custom-menu">
            <ul class="nav">

                @if (Auth::check())
                    @php
                        $user = Auth::user();
                        $account = $user->account;
                    @endphp
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                           aria-expanded="false">
                            @if($account->avatar)
                                <img src="{{ $account->avatarUrl }}" class="user-image" alt="{{ $account->full_name }}">
                            @else
                                <img src="{{ asset('images/login/user-circle.svg') }}" class="user-image" alt="avatar">
                            @endif
                            <span class="hidden-xs">{{$user->email}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <a href="{{ \App\Helpers\AuthHelper::hasCmsAccess() ? route('admin.users.profile.show') : '' }}">
                                    <img src="{{ asset('images/login/user-circle.svg') }}" class="user-icon" alt="">My Account
                                    <span class="sub-text">{{$user->roleList}}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.events.index') }}">Event Management System</a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}">Logout<img src="{{ asset('images/login/logout.svg') }}" class="logout-icon"
                                                                           alt=""></a>
                            </li>
                        </ul>
                    </li>
                @else
                    <li><a data-toggle="modal" data-target="#modalLogin" href="">Sign In</a></li>
                @endif
            </ul>
        </div>
    @endisset
</header>