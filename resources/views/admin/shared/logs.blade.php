<div class="tab-content-bottom">
    <div class="revisions-wrap">
        @foreach($logs as $log)
            <div class="single-revision">{{$log->userFullName}} {{$log->description}}. {{$log->created_at->format('M d, Y')}}</div>
        @endforeach
    </div>
</div>