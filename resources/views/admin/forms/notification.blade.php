<div id="notification"></div>

<script>
    $(document).ready(function () {
        let notification = $('#notification').kendoNotification({
            position: {
                pinned: true,
                top: 30,
                right: 90
            },
            autoHideAfter: 2000,
            templates: [
                {
                    type: 'success',
                    template: $('#successTemplate').html()
                },
                {
                    type: 'error',
                    template: $('#errorTemplate').html()
                }
            ]

        }).data('kendoNotification');

        @if (session()->has('success'))
        notification.show({}, 'success');
        @endif

        @if (session()->has('error'))
        notification.show({}, 'error');
        @endif

    });
</script>

<script id="errorTemplate" type="text/x-kendo-template">
    @if (session()->has('error'))
    <div class="error">
        <h4>{{session()->get('error')}}</h4>
    </div>

    @php
        session()->forget('error');
    @endphp
    @endif
</script>

<script id="successTemplate" type="text/x-kendo-template">
    @if (session()->has('success'))

    <div class="success">
        <h4>{{session()->get('success')}}</h4>
    </div>

    @php
        session()->forget('success');
    @endphp
    @endif
</script>






