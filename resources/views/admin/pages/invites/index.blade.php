@extends('adminlte::page')

@section('title', 'Invitations')

@section('content_header')

@stop

@section('content')
    <div class="content-box-wrap">
        <div class="content-box content-box-large">
            <div class="panel-heading">
                <h1 class="panel-heading-title">Invitations</h1>
                <form action="{{ route('admin.invites.index') }}" method="get">
                    <div class="panel-heading-search input-group">
                        <input type="text" name="q" value="{{ $q }}" class="form-control" placeholder="Search" id="invites-search">
                        <span class="input-group-addon" id="invites-search-btn">
                        <i class="k-icon k-i-search"></i>
                    </span>
                    </div>
                </form>
                <div class="panel-heading-row">
                    <a class="btn btn-lg-small btn-outline-primary getTix-btn" href="{{ route('admin.invites.create') }}">Invite User</a>
                    <input id="select-view" class="select-input" placeholder="Type and save this view">
                </div>
            </div>
            <div class="panel-body table-responsive no-grid-toolbar">

                {{-- Invitations grid --}}
                <div id="grid-invites"></div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    @include('admin.modals.create-view')
@endsection

@push('scripts')
@endpush

@section('js')
    <script id="noDataTemplate" type="text/x-kendo-tmpl">
        <button class="js-add-new-view add-button" onclick="addNewView('#: instance.text() #')">#: instance.text() #<span>(Create new view)</span></button>
    </script>

    <script src="{{asset('js/components/admin/invites.js')}}"></script>
@stop

