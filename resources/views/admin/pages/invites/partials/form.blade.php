<div class="content-box">
    <div class="box box-solid">

        <div class="box-body">

            {{-- Email --}}

            <div class="form-group{{ $errors->has('emails') || $errors->has('emails.*')? ' has-error' : '' }}">
                {{Form::label('emails', 'Email', ['class' => 'control-label with-sublabel'])}}
                <div class="control-sublabel">Separate multiple email addresses with a comma</div>
                {{Form::select('emails[]', [], old('emails'),
                    ['class'=>'form-control', 'placeholder'=>'Type user email', 'id' => 'emails'])
                }}
                <div class="error-block">
                    {{ $errors->first('emails') }}
                    @foreach ($errors->get('emails.*') as $errorMessages)
                        @foreach ($errorMessages as $errorMessage)
                            <p>{{ $errorMessage }}</p>
                        @endforeach
                    @endforeach
                </div>
            </div>

            {{-- Role --}}

            <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                {{ Form::label('role', 'Role', ['class' => 'control-label']) }}
                <div class="select-wrap">
                    {{ Form::select('role', $roles->prepend('Select role', '') , null, ['class' => 'form-control form-control--select']) }}
                </div>
                <div class="error-block">
                    {{ $errors->first('role') }}
                </div>
            </div>

            {{-- Company --}}

            <div class="form-group{{ $errors->has('company_id') ? ' has-error' : '' }}">
                {{ Form::label('company_id', 'Company', ['class' => 'control-label']) }}
                {{Form::text('company_id', null, ['class'=>'form-control', 'placeholder'=>'Type and search company name'])}}
                <div class="error-block">
                    {{ $errors->first('company_id') }}
                </div>
            </div>

            {{-- Team --}}
            {{-- DONT DELETE, TEMPORARY COMMENTED --}}

            {{--<div class="form-group{{ $errors->has('team_id') ? ' has-error' : '' }}">
                {{ Form::label('team_id', 'Team', ['class' => 'control-label']) }}
                {{Form::text('team_id', null, ['class'=>'form-control', 'placeholder'=>'Type and search team'])}}
                <div class="error-block">
                    {{ $errors->first('team_id') }}
                </div>
            </div>--}}

            {{-- Event accsess --}}

            <div class="form-group{{ $errors->has('events') ? ' has-error' : '' }}">
                {{ Form::label('events', 'EVENT ACCESS', ['class' => 'control-label with-sublabel']) }}
                <div class="control-sublabel">Optional: Grant access to events outside the user’s company or team</div>
                {{ Form::select('events[]', $events , null,
                    ['multiple' => 'multiple', 'data-placeholder' => 'Type and select event genre', 'class' => 'form-control', 'id' => 'events'])
                }}
                <div class="error-block">
                    {{ $errors->first('events') }}
                </div>
            </div>

            <div class="tab-submit-wrap text-center">
                <div class="remark">User will receive an invitation email</div>
                <button type="submit" class="btn btn-primary-blur btn-lg-small">Send invite</button>
            </div>

        </div>
    </div>
</div>