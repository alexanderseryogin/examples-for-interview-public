@extends('adminlte::page')

@section('title', 'Invite User')

@section('content_header')

@endsection

@section('content')
    <div class="content-header">
        <div class="content-header-inner-wrap">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row content-header-bottom-row">
                        <div class="col-lg-8">
                            <h1>Invite User</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content-wrap mt-20">
        <div class="content-box-wrap">
            {{ Form::model(
                $invite,
                [
                    'method' => 'POST',
                    'url' => route('admin.invites.store')
                ]
            ) }}
            @csrf

            @include('admin.pages.invites.partials.form')

            {{ Form::close() }}
        </div>
    </div>
@endsection

@section('modal')
    @include('admin.modals.crop')
    @include('admin.modals.create-company')
@endsection

@push('scripts')

@endpush

@section('js')
    <style>
        #emails-list {
            display: none !important;
        }
    </style>
    <script id="no-data-template" type="text/x-kendo-tmpl">
        <div>
            <button class="js-add-new-company add-button" onclick="addNewCompany('#: instance.text() #')">
                #: instance.text() #<span>(Create new Company)</span>
            </button>
        </div>
    </script>
    <script src="{{asset('js/components/admin/invites.js')}}"></script>
@stop