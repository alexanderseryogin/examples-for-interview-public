@extends('adminlte::page')

@section('title', 'Roles')

@section('content_header')

@stop

@section('content')
    <div class="content-box-wrap">
        <div class="content-box content-box-large">
            <form action="{{ route('admin.roles.batchUpdate') }}" method="POST">
                @csrf
                <div class="panel-heading">
                    <h1 class="panel-heading-title">Roles</h1>
                    <div class="panel-heading-row">
                        <a class="btn btn-lg-small btn-outline-primary getTix-btn" href="{{ route('admin.roles.create') }}">Add</a>
                    </div>
                </div>
                <div class="panel-body table-responsive no-grid-toolbar">
                    <div class="grid-wrap">
                        <div id="grid" class="k-grid table-light">
                            <table>
                                <thead class="thead k-grid-header">
                                    <tr>
                                        <th>Dynamic Permission</th>
                                        @foreach($roles as $role)
                                            <th class="k-header-bordered k-header-with-menu">
                                                {{$role->name}}
                                                @if($role->name !== \App\Models\Role::SUPER_ADMIN)
                                                    <div class="role-menu" data-role-id="{{$role->id}}"></div>
                                                @endif
                                            </th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody class="tbody">
                                    <tr>
                                        <td>Users</td>
                                        @foreach($roles as $role)
                                            <td class="text-center">
                                                {{
                                                    $role->users->count()
                                                        ? $role->users->count() . ' ' . str_plural('user', $role->users->count())
                                                        : 'No users'
                                                }}
                                            </td>
                                        @endforeach
                                    </tr>
                                    @foreach($permissions as $permission)
                                        <tr>
                                            <td>{{$permission->description}}</td>
                                            @foreach($roles as $role)
                                                @php
                                                    $name = 'roles[' . $role->id . '][' . $permission->id . ']';
                                                    $isDisabled =
                                                        ! \App\Helpers\AuthHelper::can(\App\Models\Permission::ROLE_ASSIGNMENT_REMOVAL)
                                                        || ($role->name === \App\Models\Role::SUPER_ADMIN);
                                                @endphp
                                                <td>
                                                    {{Form::checkbox(
                                                        $name,
                                                        $permission->id,
                                                        $role->hasPermissionTo($permission->name),
                                                        [
                                                            'class' => 'k-checkbox k-checkbox-circle' . ($isDisabled ? ' disabled' : ''),
                                                            'id' => $name,
                                                            'data-role-id' => $role->id,
                                                            'data-permission-id' => $permission->id,
                                                        ]
                                                    )}}
                                                    {{Form::label(
                                                        $name,
                                                        ' ',
                                                        ['class' => 'k-checkbox-label k-checkbox-label-circle k-no-text' . ($isDisabled ? ' disabled' : '')]
                                                    )}}
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }}">
                        <div class="error-block">
                            {{ $errors->first('roles') }}
                        </div>
                    </div>
                    <div class="tab-submit-wrap">
                        <input type="submit" name="action" value="Save" class="btn btn-primary btn-primary-blur btn-lg-small">
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('modal')

    @include('admin.modals.confirm-modal')

@endsection

@push('scripts')
@endpush

@section('js')
    <script id="buttonMenuTemplate" type="text/x-kendo-tmpl">
        <ul class="button-menu">
            <li><i class="fa fa-fw fa-cog"></i>
                <ul>
                    <li><a href="/admin/roles/#= id #/edit"><i class="k-icon k-i-edit"></i>Edit</a></li>
                    <li><a href onclick="deleteRole(#= id #, event)"><i class="k-icon k-i-minus"></i>Delete</a></li>
                </ul>
            </li>
        </ul>


    </script>

    <script src="{{asset('js/components/admin/roles.js')}}"></script>
@stop