@extends('adminlte::page')

@section('title', 'Create Role')

@section('content_header')

@endsection

@section('content')

    <div class="content-header">
        <div class="content-header-inner-wrap">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row content-header-bottom-row">
                        <div class="col-lg-8">
                            <h1>Create Role</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content-wrap mt-20">
        <div class="content-box-wrap">

            {{-- Role create form--}}

            {{ Form::model($role, [
                    'id' => 'form-create-role',
                    'method' => 'POST',
                    'url' => route('admin.roles.store')
                ]) }}

            @include('admin.pages.roles.partials.form')

            {{ Form::close() }}
        </div>
    </div>

@endsection

@section('modal')

@endsection

@push('scripts')

@endpush

@section('js')

    <script src="{{asset('js/components/admin/roles.js')}}"></script>

@stop