<div class="content-box">
    <div class="box box-solid">

        <div class="box-body">

            {{-- Role name --}}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {{Form::label('name', 'Role Name', ['class' => 'control-label'])}}
                {{Form::text('name', $role->pretty_name, ['class'=>'form-control', 'placeholder'=>'Type your role name'])}}
                <div class="error-block">
                    {{ $errors->first('name') }}
                </div>
            </div>

            {{-- Permissions --}}

            <div class="form-group{{ $errors->has('permissions') ? ' has-error' : '' }}">
                <p class="h2">Dynamic Permission</p>
                <div class="custom-checkboxes-list">
                    @foreach($permissions as $permission)
                        @php
                            $name = 'permissions[' . $permission->id . ']';
                        @endphp
                        <div class="custom-checkbox-wrap">

                            {{Form::checkbox(
                                $name,
                                $permission->id,
                                $role->exists ? $role->hasPermissionTo($permission->name) : false,
                                [
                                    'class' => 'custom-checkbox',
                                    'id' => $name,
                                    'data-permission-id' => $permission->id
                                ]
                            )}}
                            {{Form::label(
                                $name,
                                $permission->description,
                                ['class' => 'custom-checkbox-label']
                            )}}
                        </div>
                    @endforeach
                </div>

                <div class="error-block">
                    {{ $errors->first('permissions') }}
                </div>
            </div>

            {{-- SAVE --}}

            <div class="tab-submit-wrap">
                <input type="submit" name="action" value="Save" class="btn btn-primary btn-primary-blur btn-lg-small">
            </div>

            {{-- DELETE --}}

            @if($role->exists)
                <div class="form-group">
                    <button class="del-btn btn-link text-primary" onclick="deleteRole({{$role->id}}, event)">Delete</button>
                </div>
            @endif
        </div>

    </div>
</div>