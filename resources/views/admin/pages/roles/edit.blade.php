@extends('adminlte::page')

@section('title', 'Edit Role')

@section('content_header')

@endsection

@section('content')

    <div class="content-header">
        <div class="content-header-inner-wrap">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row content-header-bottom-row">
                        <div class="col-lg-8">
                            <h1>Edit Role</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content-wrap mt-20">
        <div class="content-box-wrap">

            {{-- Role edit form--}}

            {{ Form::model($role, [
                    'id' => 'form-create-role',
                    'method' => 'PUT',
                    'url' => route('admin.roles.update', ['role' => $role->id])
                ]) }}

            @include('admin.pages.roles.partials.form')

            {{ Form::close() }}
        </div>
    </div>

@endsection

@section('modal')

    @include('admin.modals.confirm-modal')

@endsection

@push('scripts')

@endpush

@section('js')

    <script src="{{asset('js/components/admin/roles.js')}}"></script>

@stop