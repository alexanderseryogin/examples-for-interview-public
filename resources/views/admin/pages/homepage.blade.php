@extends('layouts.app')

@section('title')
    The Passport to Entertainment
@endsection

@section('header')
    @include('admin.layouts.partials._header')
@endsection

@section('content')
<link type="text/css" rel="stylesheet" href="{{asset('css/maincustom.css')}}">

    <!-- Main Page Banner Section -->
    <section id="pageSlider2">
        <div id="pageSlider-slide" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                @if($banners->count() < 1)
                    <li data-target="#pageSlider-slide" data-slide-to="1" class="active"></li>
                @else
                    @foreach($banners as $banner)
                        @if($loop->first)
                            <li data-target="#pageSlider-slide" data-slide-to="{{ $loop->iteration }}" class="active"></li>
                        @else
                            <li data-target="#pageSlider-slide" data-slide-to="{{ $loop->iteration }}"></li>
                        @endif
                    @endforeach
                @endif
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                @if($banners->count() == 0)
                    <div class="item active">
                        <picture>
                            <source media="(max-width: 479px)" srcset="{{ asset('images/default/homepage/home-thumb.jpg') }}"></a>
                            <img class="bannerPoster img-responsive" src="{{ asset('images/default/homepage/home-banner.jpg') }}"/>
                        </picture>
                    </div>
                @else
                    @foreach($banners as $banner)
                        @if($loop->first)
                            <div class="item active">
                                @if(!is_null($banner->links))
                                    <a href="{{ $banner->links }}">
                                        <picture>
                                            <source media="(max-width: 479px)" srcset="{{ $banner->mobile_path }}">
                                            <img class="bannerPoster img-responsive" src="{{ $banner->web_path }}"/>
                                        </picture>
                                    </a>
                                @else
                                    <picture>
                                        <source media="(max-width: 479px)" srcset="{{ $banner->mobile_path }}">
                                        <img class="bannerPoster img-responsive" src="{{ $banner->web_path }}"/>
                                    </picture>
                                @endif
                            </div>
                        @else
                            <div class="item">
                                @if(!is_null($banner->links))
                                    <a href="{{ $banner->links }}">
                                        <picture>
                                            <source media="(max-width: 479px)" srcset="{{ $banner->mobile_path }}">
                                            <img class="bannerPoster img-responsive" src="{{ $banner->web_path }}"/>
                                        </picture>
                                    </a>
                                @else
                                    <picture>
                                        <source media="(max-width: 479px)" srcset="{{ $banner->mobile_path }}">
                                        <img class="bannerPoster img-responsive" src="{{ $banner->web_path }}"/>
                                    </picture>
                                @endif
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
            <!-- Controls -->
            <a class="left carousel-control" data-target="#pageSlider-slide" role="button" data-slide="prev">
              <span class="fa fa-angle-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" data-target="#pageSlider-slide" role="button" data-slide="next">
              <span class="fa fa-angle-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
        </div>
    </section><!-- /Main Page Banner Section -->

    <!-- Event Cards -->
    <section class="pageContent-Home">
        <div class="container">
            <div class="row">
                <!-- Main Body -->
                <div class="mainBodyContent col-xs-12">

                    @foreach($eventcollections as $eventcollection)
                        @if($eventcollection->events->where('status', 1)->count())
                            <section @if($loop->index -0) class="homeCategory-section last"@else class="homeCategory-section"@endif id="{{ $eventcollection->name }}">
                                <h1 class="mainSecTitle">{{ ucfirst($eventcollection->name) }} Events <span>({{ $eventcollection->events->where('status', 1)->count() }})</span></h1>
                                <span class="seeAllBtn"><a href="events?title=&eventcollection={{ $eventcollection->name }}&duration=">See All <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                                <div class="row">
                                    <div class="eventList-Home">

                                        @foreach($eventcollection->events->where('status', 1)->sortBy('start_date') as $event)
                                            <div class="col-sm-4 col-xs-12">
                                                <div class="eventCard">
                                                    <div class="top">
                                                        <a href="{{ URL::to('/events/'.$event->slug) }}">
                                                            <div class="thumb" @if($event->images->where('type', 'thumbnail')->first()) style="background-image: url({{ $event->images->where('type', 'thumbnail')->first()->path }})@endif"></div>
                                                            <div class="dateEvent concert">
                                                                @if(date('d', strtotime($event->start_date)) == date('d', strtotime($event->end_date)))
                                                                    {{ date('D', strtotime($event->start_date)) }}, {{ date('j M', strtotime($event->start_date)) }} '{{ date(' y', strtotime($event->start_date)) }}
                                                                @endif
                                                                @if(date('d', strtotime($event->start_date)) != date('d', strtotime($event->end_date)) && date('M', strtotime($event->start_date)) == date('M', strtotime($event->end_date)))

                                                                    {{ date('j', strtotime($event->start_date)) }} - {{ date('j', strtotime($event->end_date)) }} {{ date('M', strtotime($event->end_date)) }} '{{ date(' y', strtotime($event->start_date)) }}
                                                                @endif
                                                                @if(date('d', strtotime($event->start_date)) != date('d', strtotime($event->end_date)) && date('M', strtotime($event->start_date)) != date('M', strtotime($event->end_date)) && date('y', strtotime($event->start_date)) == date('y', strtotime($event->end_date)))

                                                                    {{ date('j M', strtotime($event->start_date)) }}  - {{ date('j M', strtotime($event->end_date)) }} '{{ date(' y', strtotime($event->start_date)) }}
                                                                @endif
                                                                @if(date('d', strtotime($event->start_date)) != date('d', strtotime($event->end_date)) && date('M', strtotime($event->start_date)) != date('M', strtotime($event->end_date)) && date('y', strtotime($event->start_date)) != date('y', strtotime($event->end_date)))

                                                                    {{ date('j M', strtotime($event->start_date)) }} '{{ date(' y', strtotime($event->start_date)) }}   - {{ date('j M', strtotime($event->end_date)) }} '{{ date(' y', strtotime($event->start_date)) }}
                                                                @endif
                                                            </div>
                                                            <div class="viewEvent"><span>View Event</span></div>
                                                        </a>
                                                    </div>
                                                    <div class="content">
                                                        <div class="category">@if($event->category){{ $event->category }}@endif</div>
                                                        <div class="titleEvent"><a href="{{ URL::to('/events/'.$event->slug) }}"><h2>{{ $event->title }}</h2></a></div>
                                                        <div class="location">@if($event->venue){{ $event->venue->name }}, {{ $event->venue->city }}@endif</div>
                                                        {{-- if many-to-many --}}
                                                        {{-- @if($event->venues->count() > 1)
                                                            @foreach($event->venues as $venue)
                                                                <div class="location">@if($venue){{ $venue->name }}@endif</div>
                                                                @if($loop->index -0)
                                                                    <div class="location">@if($venue),{{ $venue->name }}@endif</div>
                                                                @endif
                                                            @endforeach
                                                        @endif --}}
                                                    </div>
                                                    <div class="bottom">
                                                        {{-- @if( $event->promotions->isset())
    	                                                    <div class="discount">Redtix Exclusive</div>
    	                                                @endif --}}
    	                                               <div class="price">@if($event->tickets->count())From RM{{ $event->tickets->pluck('price')->min() }} @else TBD @endif</div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </section>
                        @endif
                    @endforeach

                </div>
            </div>
        </div>
    </section>

@endsection

@push('scripts')
    <script type="text/javascript" src="{{asset('js/login.js')}}"></script>
    <script type="text/javascript" src="/js/homepage.js"></script>
    @if(isset($showLoginModal))
        <script>
            $('#modalLogin').modal('show');
        </script>
    @endif
@endpush


@section('modal')
    @include('auth.login.login')
    @include('auth.login.reset-password')
    @include('admin.modals.notify-modal')
@endsection

