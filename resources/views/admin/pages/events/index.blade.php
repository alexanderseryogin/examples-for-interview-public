@extends('adminlte::page')

@section('title', 'Event Management')

@section('content_header')

@stop

@section('content')
    <div class="content-box-wrap">
        <div class="content-box-large">
            <div class="panel-heading">
                <h1 class="panel-heading-title">Event Management</h1>
                <form action="{{ route('admin.events.index') }}" method="get" class="autosave">
                    <div class="panel-heading-search input-group">
                        <input type="text" name="q" value="{{ $q }}" class="form-control" placeholder="Search" id="events-search">
                        <span class="input-group-addon" id="events-search-btn">
                        <i class="k-icon k-i-search"></i>
                    </span>
                    </div>
                </form>
                <div class="panel-heading-row">
                    <a class="btn btn-lg-small btn-outline-primary getTix-btn" href="{{ route('admin.events.create') }}">Add</a>
                    <input id="select-view" class="select-input" placeholder="Type and save this view">
                </div>
            </div>
            <div class="panel-body table-responsive no-grid-toolbar">

                <div id="grid"></div>

                <ul class="table-functionality">
                    <li><a href="javascript:;" onclick="batchUpdateEventStatusModal('private')">Private</a></li>
                    <li><a href="javascript:;" onclick="batchUpdateEventStatusModal('delete')">Delete</a></li>
                    <li><a href="javascript:;" onclick="batchUpdateEventStatusModal('suspend')">Suspend</a></li>
                    <li><a href="javascript:;" onclick="batchUpdateEventStatusModal('cancel')">Cancel</a></li>
                    <li><a href="javascript:;" onclick="batchUpdateEventStatusModal('publish')">Publish</a></li>
                </ul>
            </div>
        </div>
    </div>
    {{--<button data-target="#modalResetPassword" data-toggle="modal">greg</button>--}}
@endsection

@section('modal')

    @include('admin.modals.create-view')
    @include('admin.pages.events.modals.events')

@endsection

@push('scripts')
@endpush

@section('js')
    <script>
        var eventStatuses = JSON.parse('{!! json_encode($statuses) !!}');
    </script>
    <script id="noDataTemplate" type="text/x-kendo-tmpl">
        <button class="js-add-new-view add-button" onclick="addNewView('#: instance.text() #')">#: instance.text() #<span>(Create new view)</span></button>
    </script>

    <script id="buttonMenuTemplate" type="text/x-kendo-tmpl">
        <ul class="button-menu">
            <li><i class="fa fa-fw fa-cog"></i>
                <ul>
                    <li class="bordered-item">
                        <a href="javascript:;"><i class="k-icon k-i-plus"></i>Add</a>
                    </li>
                    <li>
                        <a href="javascript:;" onclick="updateEventStatusModal(#= id #, 'draft')">
                            <i class="k-icon k-i-track-changes-enable"></i>Draft
                        </a>
                    </li>
                    <li>
                        <a href="/admin/events/#= id #"><i class="k-icon k-i-edit"></i>Edit</a>
                    </li>
                    <li>
                        <a href="javascript:;" onclick="updateEventStatusModal(#= id #, 'delete')">
                            <i class="k-icon k-i-minus"></i>Delete
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" onclick="updateEventStatusModal(#= id #, 'suspend')">
                            <i class="k-icon k-i-cancel-outline"></i>Suspend
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" onclick="updateEventStatusModal(#= id #, 'cancel')">
                            <i class="k-icon k-i-close"></i>Cancel
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" onclick="updateEventStatusModal(#= id #, 'publish')">
                            <i class="k-icon k-i-check"></i>Publish
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" onclick="updateEventStatusModal(#= id #, 'private')">
                            <i class="k-icon k-i-preview"></i>Private Sale
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </script>

    <script src="{{asset('js/components/admin/events.js')}}"></script>
@stop