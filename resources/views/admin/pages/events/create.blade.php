@extends('adminlte::page')

@section('title', 'Create Event')

@section('content_header')

@endsection

@section('content')
    <div class="content-header">
        <div class="content-header-inner-wrap">
            <a href="{{ route('admin.events.index') }}" class="link-back">&lt; Back</a>
            <div class="row">
                <div class="col-lg-8">
                    <div class="row content-header-bottom-row">
                        <div class="col-lg-8">
                            <h1>Create Event</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content-wrap">
        <div class="nav-tabs-custom" id="tabstrip">
            <ul class="nav nav-tabs nav-tabs_general nav-tabs_with-link">
                <li class="k-state-active">General Info</li>
            </ul>

            <div class="tab-content">
                {{ Form::open([
                        'method' => 'POST',
                        'url' => route('admin.events.store'),
                        'enctype' => 'multipart/form-data',
                        'id' => 'event-general-info-form'
                    ]) }}

                @include('admin.pages.events.partials.tabs.general-info')

                {{ Form::close() }}
            </div>
        </div>
        {{--End Event create/edit form--}}
    </div>

@endsection

@section('modal')
    @include('admin.modals.crop')
    @include('admin.modals.create-company', ['type' => \App\Models\Company::TYPE_EVENT_ORGANIZER])
@endsection

@section('js')
    <script id="no-data-template" type="text/x-kendo-tmpl">
        <div>
            <button class="js-add-new-organizer add-button" onclick='addNewEventOrganizer("#: instance.text() #")'>
                #: instance.text() #<span>(Create new organizer)</span>
            </button>
        </div>
    </script>

    <script src="{{asset('js/components/admin/events.js')}}"></script>
@stop