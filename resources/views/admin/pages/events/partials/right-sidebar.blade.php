<div class="right-sidebar closed">
    <button class="info-sidebar-toggle js-right-sidebar-toggle" type="button"></button>
    <div class="box box-solid">
        <dl>
            <dt>Created By:</dt>
            <dd>{{ $event->creatorName ?? '...' }}</dd>
        </dl>
        <dl>
            <dt>Created on:</dt>
            <dd>{{ $event->created_at ? \App\Helpers\DateHelper::date($event->created_at) : '...' }}</dd>
        </dl>
        <dl>
            <dt>Saved on:</dt>
            <dd class="autosave-time">{{ $event->updated_at ? \App\Helpers\DateHelper::dateTime($event->updated_at) : '...' }}</dd>
        </dl>
        <dl>
            <dt>Status:</dt>
            <dd>
                <div class="label label-{{ strtolower($event->status) }}">{{ $event->status }}</div>
            </dd>
        </dl>
        <dl>
            <dt>Person in charge (BD):</dt>
            <dd>
                <div class="select-wrap">
                    {{ Form::select('select-name', $users, null, ['id' => 'select-name', 'class' => 'form-control']) }}
                </div>
            </dd>
        </dl>
    </div>
</div>
