{{ Form::model($event, [
        'method' => 'POST',
        'id' => 'event-faqs-form',
        'url' => route('admin.events.faqs.update', ['event' => $event->id])
    ]) }}
<div class="content-box">
    <div class="box box-solid">
        <div class="box-body">
            <div class="js-sortable-list sortable-list" id="event-faqs-list"
                 data-url-all="{{ route('admin.events.faq.all', ['event' => $event->id]) }}">
                @foreach($event->faqs as $key => $faq)
                    @include('admin.pages.events.partials.tabs.faq.index')
                @endforeach
                @if($event->faqs->isEmpty())
                    @include('admin.pages.events.partials.tabs.faq.index', ['key' => 0, 'faq' => \App\Models\EventFaq::newModelInstance()])
                @endif
            </div>
            <button class="btn-link text-sky-blue lead-sm text-uppercase" id="event-add-faq"
                    data-url="{{ route('admin.events.faq.get-empty-block', ['event' => $event->id]) }}">
                + add faq
            </button>
            <div class="tab-submit-wrap">
                <input type="submit" name="action" value="Save" class="btn btn-primary-blur btn-lg-small">
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}