{{ Form::model($event, [
        'method' => 'POST',
        'class' => 'autosave',
        'url' => route('admin.events.info.update', ['event' => $event->id])
    ]) }}
<div class="content-box">
    <div class="box box-solid">
        <div class="box-body">
            <div class="accordion-wrap js-sortable-list sortable-list">
                <div class="form-group{{ $errors->has('info_tab_name') ? ' has-error' : '' }}">
                    {{ Form::label('info_tab_name', 'Tab name', ['class' => 'control-label with-sublabel']) }}
                    <div class="control-sublabel">This tab name will reflect in event page</div>
                    {{ Form::text('info_tab_name', null, ['class' => 'form-control', 'placeholder' => 'Tab name']) }}
                    <div class="top-checkbox-wrap">
                        <label class="toggle-switch">
                            {{Form::checkbox('is_info_tab_visible', 1, $event->is_info_tab_visible, [
                                    'class' => 'checkbox-recommend'
                              ])}}
                            <span class="slider">
                            <span class="labels">Show</span>
                            <span class="labels">Hide</span>
                        </span>
                        </label>
                    </div>
                    @if($errors->has('info_tab_name'))
                        <div class="error-block">
                            {{ $errors->first('info_tab_name') }}
                        </div>
                    @endif
                </div>

                @foreach($event->infoBlocks as $infoBlock)
                    <div class="js-sortable sortable accordion-block">
                        <div class="right-handler">
                            <i class="k-icon k-i-more-vertical js-handler handler"></i>
                        </div>
                        <div class="js-accordion-panel accordion-panel">
                            <div class="accordion-header">
                                <h4 class="accordion-title js-handler handler">{{ $infoBlock->title }}</h4>
                                {{Form::hidden('infoBlocks[' . $infoBlock->type . '][id]', $infoBlock->id)}}
                                {{Form::hidden('infoBlocks[' . $infoBlock->type . '][pos]', $infoBlock->pos, [
                                    'class' => 'info-block-pos'
                                ])}}
                                <div class="top-checkbox-wrap">
                                    <label class="toggle-switch round">
                                        {{Form::checkbox('infoBlocks[' . $infoBlock->type . '][is_visible]', 1, $infoBlock->is_visible, [
                                            'class' => 'checkbox-recommend'
                                      ])}}
                                        <span class="slider">
                                        <span class="labels">on</span>
                                        <span class="labels">off</span>
                                    </span>
                                    </label>
                                </div>
                            </div>

                            <div class="accordion-panel-content js-accordion-panel-content js-sortable-panel">
                                @include('admin.pages.events.partials.tabs.info-blocks.' . $infoBlock->type)
                            </div>
                        </div>
                        <button class="accordion-btn btn-link text-sky-blue js-accordion-btn" type="button">Show more</button>
                    </div>
                @endforeach

            </div>
            <div class="tab-submit-wrap">
                <input type="submit" name="action" value="Save" class="btn btn-primary-blur btn-lg-small">
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}