<div class="js-sortable sortable event-faq">
    <div class="right-handler">
        <i class="k-icon k-i-more-vertical js-handler handler"></i>
    </div>
    <div class="js-sortable-panel sortable-panel">
        <div class="form-group form-group-with-top-checkbox{{ $errors->has('faqs.'. $key . '.title') ? ' has-error' : '' }}">
            {{ Form::hidden('faqs['. $key . '][id]', null, ['id' => 'faqs_'. $key . '_id']) }}
            {{ Form::hidden('faqs['. $key . '][pos]', null, ['class' => 'event-faq-pos', 'id' => 'faqs_'. $key . '_pos']) }}
            {{ Form::label('faqs['. $key . '][title]', 'Faq title', ['class' => 'control-label']) }}
            {{ Form::text('faqs['. $key . '][title]', null, ['class'=>'form-control event-faq-title', 'placeholder'=>'Type your FAQ title']) }}
            <div class="top-checkbox-wrap">
                <label class="toggle-switch">
                    {{Form::checkbox('faqs['. $key . '][is_visible]', 1, $faq->is_visible, [
                            'class' => 'checkbox-recommend'
                      ])}}
                    <span class="slider">
                        <span class="labels">Show</span>
                        <span class="labels">Hide</span>
                    </span>
                </label>
            </div>
            @if($errors->has('faqs.'. $key . '.title'))
                <div class="error-block">
                    {{ $errors->first('faqs.'. $key . '.title') }}
                </div>
            @endif
        </div>

        <div class="form-group{{ $errors->has('faqs.'. $key . '.body') ? ' has-error' : '' }}">
            {{ Form::label('faqs_'. $key . '_body', 'Faq body', ['class' => 'control-label']) }}
            {!! Form::textarea('faqs['. $key . '][body]', null, [
                  'class' => 'js-editor form-control js-event-faq-body',
                  'placeholder'=> 'Type your FAQ body',
                  'id'=> 'faqs_'. $key . '_body'
            ]) !!}
            <div class="top-checkbox-wrap">
                {{Form::checkbox('faqs['. $key . '][is_template]', 1, $faq->is_template, [
                    'class' => 'custom-checkbox',
                    'id' => 'faqs_'. $key . '_is_template'
                ])}}
                {{ Form::label('faqs_'. $key . '_is_template', 'Save as template', ['class' => 'custom-checkbox-label']) }}
            </div>
            @if($errors->has('faqs.'. $key . '.body'))
                <div class="error-block">
                    {{ $errors->first('faqs.'. $key . '.body') }}
                </div>
            @endif
        </div>
    </div>
    <button class="del-btn btn-link text-primary event-remove-faq">Delete</button>
</div>