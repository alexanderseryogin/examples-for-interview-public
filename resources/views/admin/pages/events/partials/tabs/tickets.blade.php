<div class="content-box">
    <div class="box box-solid">
        <div class="box-body">
            <h4 class="box-title">Add tickets to be display in event page</h4>
            <div class="box-body-btn-wrap">
                <input type="button" value="Add" id="event-tickets-grid-add" class="btn btn-outline-primary btn-lg-small">
            </div>
            <div id="event-tickets-grid" data-base-url="{{ route('admin.events.ticket.index', ['event' => $event->id]) }}"
                 data-event-start-date="{{ $event->start_date }}"></div>

            {{ Form::model($event, [
                'method' => 'POST',
                'id' => 'event-tickets-form',
                 'class' => 'autosave',
                'url' => route('admin.events.ticket.update-info', ['event' => $event->id])
            ]) }}

                <div class="mt-30 form-group{{ $errors->has('max_purchase_tickets_quantity') ? ' has-error' : '' }}">
                    {{ Form::label('max_purchase_tickets_quantity', 'Max. Ticket Quantity Per Purchase', ['class' => 'control-label']) }}
                    <div class="d-flex align-items-center number-increment-input-wrap">
                        <span class="inc button">+</span>
                        {{ Form::number('max_purchase_tickets_quantity', null, ['class' => 'form-control number-increment-input']) }}
                        <span class="dec button">&ndash;</span>
                    </div>
                    <div class="error-block">
                        {{ $errors->first('max_purchase_tickets_quantity') }}
                    </div>
                </div>

                <div class="tab-submit-wrap">
                    <input type="submit" name="action" value="Save" class="btn btn-primary-blur btn-lg-small">
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

<script id="ticket-button-menu-template" type="text/x-kendo-tmpl">
    <ul class="button-menu">
        <li>
            <span class="button-menu_more-btn">
                <i class="fa fa-circle"></i>
                <i class="fa fa-circle"></i>
                <i class="fa fa-circle"></i>
            </span>
            <ul>
                <li class="save-event-ticket-wrap" style="display: none">
                    <a href="javascript:;" data-uid="#= uid #" class="save-event-ticket">Save</a>
                </li>
                <li class="edit-event-ticket-wrap">
                    <a href="javascript:;" data-uid="#= uid #" class="edit-event-ticket">Edit</a>
                </li>
                <li class="cancel-event-ticket-wrap" style="display: none">
                    <a href="javascript:;" data-uid="#= uid #" class="cancel-event-ticket">Cancel</a>
                </li>
                <li class="edit-details-event-ticket-wrap">
                    <a href="javascript:;" data-uid="#= uid #" class="edit-details-event-ticket">Edit Details</a>
                </li>
                <li class="delete-event-ticket-wrap">
                    <a href="javascript:;" data-uid="#= uid #" class="delete-event-ticket">Delete</a>
                </li>
            </ul>
        </li>
    </ul>
</script>