
<div class="content-box">
    <div class="box box-solid">

        <div class="box-body">

            {{-- Event name --}}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {{Form::label('name', 'Event Name', ['class' => 'control-label'])}}
                {{Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Type Event Name'])}}
                @if($errors->has('name'))
                    <div class="error-block">
                        {{ $errors->first('name') }}
                    </div>
                @endif
            </div>

            {{-- URL --}}

            <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                {{Form::label('url', 'Event URL', ['class' => 'control-label with-sublabel'])}}
                <div class="control-sublabel">Event URL will be generate based on Event Name</div>
                <div class="input-constant-value-wrap">
                    <span class="constant-value-wrap" id="js-event-url-start">{{ \Illuminate\Support\Str::finish(url('tickets'), '/')  }}</span>
                    {{Form::text('url', null, ['class'=>'form-control', 'placeholder'=>'event name', 'id' => 'js-event-url-end'])}}
                    <i class="fa fa-copy copy-event-url" id="js-copy-event-url" title="Copy to clipboard"></i>
                </div>@if($errors->has('url'))
                    <div class="error-block">
                        {{ $errors->first('url') }}
                    </div>
                @endif
            </div>

            {{-- Banners --}}

            <div class="form-group">
                {{Form::label('banner_desktop', 'Hero banner for desktop', ['class' => 'control-label'])}}
                <div class="control-sublabel">
                    Event hero banner image need to be <span class="bold">width: 1440px</span> and
                    <span class="bold">height: 537px</span>
                </div>
                <div class="drop-zone drop-zone-desktop horizontal"
                     data-url="{{ old('banner_desktop') ? '/storage/' . old('banner_desktop') : optional($event)->bannerDesktopUrl }}">
                    {{Form::hidden('banner_desktop', null, ['id' => 'banner_desktop'])}}
                </div>
            </div>

            <div class="form-group">
                {{Form::label('banner_mobile', 'Hero banner for mobile', ['class' => 'control-label'])}}
                <div class="control-sublabel">
                    Event hero banner image need to be <span class="bold">width: 414px</span> and
                    <span class="bold">height: 670px</span>
                </div>
                <div class="drop-zone drop-zone-mobile vertical"
                     data-url="{{ old('banner_mobile') ? '/storage/' . old('banner_mobile') : optional($event)->bannerMobileUrl }}">
                    {{Form::hidden('banner_mobile', null, ['id' => 'banner_mobile'])}}
                </div>
            </div>

            {{-- DATES --}}

            <div class="form-group d-flex datetime-input-wrap">
                <div class="date-input-wrap">
                    {{Form::label('start_date', 'Event Start: Date', ['class' => 'control-label'])}}
                    {{Form::text('start_date', old('start_date', $event->start_date), ['class' => 'form-control', 'placeholder' => '8 June 2019'])}}
                </div>
                <div class="time-input-wrap">
                    {{ Form::label('start_date_hour', 'Hour', ['class' => 'control-label']) }}
                    <div class="select-wrap">
                        {{ Form::select('start_date_hour',
                            \App\Helpers\DateHelper::hoursList(),
                            optional($event->start_date)->format('G'), [
                                'class' => 'form-control form-control--select',
                            ]) }}
                    </div>
                </div>
                <div class="time-input-wrap">
                    {{ Form::label('start_date_minute', 'Minute', ['class' => 'control-label']) }}
                    <div class="select-wrap">
                        {{ Form::select('start_date_minute',
                            \App\Helpers\DateHelper::minutesList(),
                            optional($event->start_date)->format('i'), [
                                'class' => 'form-control form-control--select',
                            ]) }}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="d-flex justify-content-between datetime-chechbox-wrap">
                    <div class="d-flex datetime-input-wrap">
                        <div class="date-input-wrap">
                            {{Form::label('end_date', 'Event End: Date', ['class' => 'control-label'])}}
                            {{Form::text('end_date', $event->end_date, ['class' => 'form-control', 'placeholder' => '8 June 2019'])}}
                        </div>
                        <div class="time-input-wrap">
                            {{ Form::label('end_date_hour', 'Hour', ['class' => 'control-label']) }}
                            <div class="select-wrap">
                                {{ Form::select('end_date_hour',
                                    \App\Helpers\DateHelper::hoursList(),
                                    optional($event->end_date)->format('G'), [
                                        'class' => 'form-control form-control--select',
                                    ]) }}
                            </div>
                        </div>
                        <div class="time-input-wrap">
                            {{ Form::label('end_date_minute', 'Minute', ['class' => 'control-label']) }}
                            <div class="select-wrap">
                                {{ Form::select('end_date_minute',
                                    \App\Helpers\DateHelper::minutesList(),
                                    optional($event->end_date)->format('i'), [
                                        'class' => 'form-control form-control--select',
                                    ]) }}
                            </div>
                        </div>
                    </div>

                    <div class="custom-checkbox-wrap">
                        {{ Form::checkbox('is_estimated_end_time', 1, null, ['class' => 'custom-checkbox', 'id' => 'is_estimated_end_time']) }}
                        {{ Form::label('is_estimated_end_time', 'Set as estimated end time', ['class' => 'custom-checkbox-label mb-0']) }}
                    </div>
                </div>
            </div>

            {{-- Venue, Genre, Organizators, Categories select --}}

            <div class="form-group">
                {{ Form::label('venue_id', 'event venue', ['class' => 'control-label']) }}
                <div>
                    {{Form::text('venue_id', null, ['class'=>'form-control', 'placeholder'=>'Type event venue'])}}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('organizer_id', 'Event organizer', ['class' => 'control-label']) }}
                <div>
                    {{Form::number('organizer_id', null, ['class'=>'form-control form-control--select', 'placeholder'=>'Type event venue'])}}
                </div>
            </div>

            <div class="form-group">
                <label class="control-label">RedTix Guarantee</label>

                <div class="form-bottom-inputs">
                    <label class="toggle-switch round" for="has_redtix_guarantee">
                        {{ Form::checkbox('has_redtix_guarantee', 1, null, ['class' => 'checkbox-recommend', 'id' => 'has_redtix_guarantee']) }}
                        <span class="slider">
                                <span class="labels">off</span>
                                <span class="labels">on</span>
                            </span>
                        Show on event detail page
                    </label>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('category_id', 'Event Category', ['class' => 'control-label']) }}
                <div class="select-wrap">
                    {{ Form::select('category_id', $categories->prepend('No category', '') , null, ['class' => 'form-control form-control--select']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('genres', 'Event genre', ['class' => 'control-label']) }}
                {{ Form::select('genres[]', $genres , null,
                    ['multiple' => 'multiple', 'data-placeholder' => 'Type and select event genre', 'class' => 'form-control', 'id' => 'genres'])
                }}
            </div>

            {{-- Description --}}

            <div class="form-group">
                {{ Form::label('description', 'Event Description', ['class' => 'control-label']) }}
                {!! Form::textarea('description', null, [
                      'class' => 'form-control',
                      'placeholder'=> 'Inform people more about the event',
                      'data-provide' => 'markdown-editable'
                ]) !!}
            </div>

            {{-- Countdown and custom date --}}

            <div class="form-group">
                <label class="control-label">Countdown</label>

                <div class="form-bottom-inputs">
                    <label class="toggle-switch round" for="show_countdown">
                        {{ Form::checkbox('show_countdown', 1, null, ['class' => 'checkbox-recommend', 'id' => 'show_countdown']) }}
                        <span class="slider">
                                <span class="labels">off</span>
                                <span class="labels">on</span>
                            </span>
                        Show on event detail page
                    </label>

                    <div class="custom-checkbox-wrap">
                        {{ Form::checkbox('add_custom_datetime', 1, !empty($event->custom_date), [
                            'class' => 'custom-checkbox',
                            'id' => 'add_custom_datetime'
                        ]) }}
                        {{ Form::label('add_custom_datetime', 'Add custom date & time', ['class' => 'custom-checkbox-label']) }}
                    </div>
                </div>

                <div id="add_custom_datetime-wrap" class="countdown-datetime-inputs datetime-input-wrap @if(!empty($event->custom_date)) open @endif">
                    <div class="date-input-wrap">
                        {{Form::label('custom_date', 'Countdown: Date', ['class' => 'control-label'])}}
                        {{Form::text('custom_date', $event->custom_date, ['class' => 'form-control', 'placeholder' => ''])}}
                    </div>
                    <div class="time-input-wrap">
                        {{ Form::label('custom_date_hour', null, ['class' => 'control-label', 'aria-label' => 'Hour']) }}
                        <div class="select-wrap">
                            {{ Form::select('custom_date_hour',
                                \App\Helpers\DateHelper::hoursList(),
                                optional($event->custom_date)->format('G'), [
                                    'class' => 'form-control form-control--select',
                                ])
                            }}
                        </div>
                    </div>
                    <div class="time-input-wrap">
                        {{ Form::label('custom_date_minute', null, ['class' => 'control-label', 'aria-label' => 'Minute']) }}
                        <div class="select-wrap">
                            {{ Form::select('custom_date_minute',
                                \App\Helpers\DateHelper::minutesList(),
                                optional($event->custom_date)->format('i'), [
                                    'class' => 'form-control form-control--select',
                                ])
                            }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-submit-wrap">
                <input type="submit" name="action" value="Save" class="btn btn-primary-blur btn-lg-small">
            </div>
        </div>
    </div>
</div>