<div id="event-info-quotes">
    @foreach($event->quotes as $key => $quote)
        <div class="form-group" data-number="{{$key}}">
            {{Form::label('quotes[' . $key . ']', 'Quote ' . ($key + 1), ['class' => 'control-label'])}}
            {{Form::hidden('quotes[' . $key . '][id]', $quote->if)}}
            {{Form::textarea('quotes[' . $key . '][content]', $quote->content, [
                'class' => 'js-editor',
                'placeholder' => 'Please type your quote. Please state the spokesperson name'
            ])}}
            <button class="btn-link text-primary event-info-remove-quote">- Remove quote</button>
        </div>
    @endforeach
    @if($event->quotes->isEmpty())
        <div class="form-group">
            <label for="quotes[0]" class="control-label" data-number="1">Quote 1</label>
            <textarea id="quotes[0]" name="quotes[0][content]" class="js-editor"
                      placeholder="Please type your quote. Please state the spokesperson name"></textarea>
            <div class="error-block"></div>
        </div>
    @endif
</div>
{{--<button class="btn-link text-sky-blue" id="event-info-add-quote" type="button">+ Add quote</button>--}}