<div class="form-group">
    {{Form::label('artist_line_up[]', 'Artists', ['class' => 'control-label'])}}
    <div class="control-sublabel">
        Images will be cropped to square (500px by 500px) and displayed in rows of 3
    </div>
    <div id="event-info-artist-line-up">
        @foreach($event->artistLineUp  as $image)
            <div class="drop-zone drop-zone-artist horizontal" data-url="{{ $image->url }}">
                {{Form::hidden('artist_line_up[]', $image->path, ['class' => 'artist-line-up-image'])}}
            </div>
        @endforeach
        @if($event->artistLineUp->isEmpty())
            <div class="drop-zone drop-zone-artist horizontal">
                <input class="artist-line-up-image" name="artist_line_up[]" type="hidden" value="">
            </div>
        @endif
    </div>
    <div class="text-center">
        <button class="btn btn-primary-blur btn-lg-small" id="info-add-artist-line-up-image" type="button">+ Add files</button>
    </div>
</div>