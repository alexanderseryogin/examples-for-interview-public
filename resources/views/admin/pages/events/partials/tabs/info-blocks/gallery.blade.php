<div class="form-group">
    {{Form::label('gallery[]', 'Images', ['class' => 'control-label'])}}
    <div class="control-sublabel">
        Images will be cropped to square (500px by 500px) and displayed in rows of 3
    </div>
    <div id="event-info-gallery">
        @foreach($event->gallery  as $image)
            <div class="drop-zone drop-zone-gallery horizontal" data-url="{{ $image->url }}">
                {{Form::hidden('gallery[]', $image->path, ['class' => 'gallery-image'])}}
            </div>
        @endforeach
        @if($event->gallery->isEmpty())
            <div class="drop-zone drop-zone-gallery horizontal">
                <input class="gallery-image" name="gallery[]" type="hidden" value="">
            </div>
        @endif
    </div>
    <div class="text-center">
        <button class="btn btn-primary" id="info-add-gallery-image" type="button">+ Add files</button>
    </div>
</div>