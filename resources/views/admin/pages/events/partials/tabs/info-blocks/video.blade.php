<div class="form-group">
    <label for="info_video" class="control-label with-sublabel">Video url</label>
    <div class="control-sublabel">Video url can be retrieve from YouTube</div>
    {{Form::text('info_video', null, [
            'class' => 'form-control',
            'placeholder' => 'Add video Url',
            'id' => 'info_video'
      ])}}
    <div class="error-block">
        {{ $errors->first('info_video') }}
    </div>
</div>