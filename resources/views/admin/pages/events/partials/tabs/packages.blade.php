{{ Form::model($event, [
        'method' => 'POST',
        'id' => 'event-packages-form',
        'url' => route('admin.events.package.update-info', ['event' => $event->id])
    ]) }}
<div class="content-box">
    <div class="box box-solid">
        <div class="box-body">
            <div class="form-group">
                {{ Form::label('packages_tab_name', 'Tab name', ['class' => 'control-label with-sublabel']) }}
                <div class="control-sublabel">This tab name will reflect in event page</div>
                {{ Form::text('packages_tab_name', null, ['class' => 'form-control', 'placeholder' => 'Packages']) }}
                <div class="top-checkbox-wrap">
                    <label class="toggle-switch">
                        {{Form::checkbox('is_packages_tab_visible', 1, $event->is_packages_tab_visible, [
                                'class' => 'checkbox-recommend'
                          ])}}
                        <span class="slider">
                            <span class="labels">Show</span>
                            <span class="labels">Hide</span>
                        </span>
                    </label>
                </div>
                <div class="error-block">
                    {{ $errors->first('packages_tab_name') }}
                </div>
            </div>
            <div class="box-body-btn-wrap">
                <input type="button" value="Add" id="event-packages-grid-add" class="btn btn-primary-blur btn-lg-small">
            </div>

            <div id="event-packages-grid" data-base-url="{{ route('admin.events.package.index', ['event' => $event->id]) }}"></div>

            <div class="tab-submit-wrap">
                <input type="submit" name="action" value="Save" class="btn btn-primary-blur btn-lg-small">
            </div>
        </div>

    </div>
</div>

<script id="package-button-menu-template" type="text/x-kendo-tmpl">
    <ul class="button-menu">
        <li>
            <span class="button-menu_more-btn">
                <i class="fa fa-circle"></i>
                <i class="fa fa-circle"></i>
                <i class="fa fa-circle"></i>
            </span>
            <ul>
                <li class="save-event-package-wrap" style="display: none">
                    <a href="javascript:;" data-uid="#= uid #" class="save-event-package">Save</a>
                </li>
                <li class="edit-event-package-wrap">
                    <a href="javascript:;" data-uid="#= uid #" class="edit-event-package">Edit</a>
                </li>
                <li class="cancel-event-package-wrap" style="display: none">
                    <a href="javascript:;" data-uid="#= uid #" class="cancel-event-package">Cancel</a>
                </li>
                <li class="edit-details-event-package-wrap">
                    <a href="javascript:;" data-uid="#= uid #" class="edit-details-event-package">Edit Details</a>
                </li>
                <li class="delete-event-package-wrap">
                    <a href="javascript:;" data-uid="#= uid #" class="delete-event-package">Delete</a>
                </li>
            </ul>
        </li>
    </ul>
</script>
{{ Form::close() }}