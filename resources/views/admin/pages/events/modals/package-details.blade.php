<div class="modal fade package-details" id="edit-event-package-modal" tabindex="-1" role="dialog" aria-labelledby="packageDetails" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="" id="edit-event-package-form">
                <div class="modal-header">
                    <button type="button" class="close" aria-label="Close" data-bind="events: { click: cancel }">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <fieldset>
                        <legend>Package details</legend>

                        <div class="form-group">
                            <label for="package_name" class="control-label">Package name</label>
                            <input id="package_name"
                                   type="text"
                                   class="form-control"
                                   name="package_name"
                                   data-bind="value: selected.name"
                                   placeholder="Type your ticket name">
                            <div class="top-checkbox-wrap">
                                <input class="custom-checkbox" name="package_is_hidden" type="checkbox" id="package_is_hidden" value="1"
                                       data-bind="checked: selected.is_hidden, events: { change: isHiddenChecked }" data-type="number">
                                <label for="package_is_hidden" class="custom-checkbox-label">Hide package on your website</label>
                            </div>
                            <div class="error-block"></div>
                        </div>

                        <div class="form-group">
                            <label for="package_card_size" class="control-label">Card size</label>
                            <div class="select-wrap">
                                <select class="form-control form-control--select"
                                        name="package_card_size"
                                        id="package_card_size"
                                        data-bind="value: selected.card_size">
                                    @foreach($packageCardSizes as $packageCardSize)
                                        <option value="{{ $packageCardSize }}">{{ $packageCardSize }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="package_card_image" class="control-label">Image in package card</label>
                            <div class="control-sublabel">
                                Image in package card need to be <span class="bold">width: 1110px</span> and
                                <span class="bold">height: 295px</span>
                            </div>
                            <div class="drop-zone drop-croppie-package-card drop-zone-ticket horizontal" data-url="">
                                <input id="package_card_image"
                                       type="hidden"
                                       class="form-control"
                                       name="card_image"
                                       data-bind="value: selected.card_image">
                            </div>
                            <div class="bottom-checkbox-wrap">
                                <input class="custom-checkbox" name="package_use_same_more_info" type="checkbox" id="package_use_same_more_info" value="1"
                                       data-bind="checked: selected.use_same_more_info" data-type="number">
                                <label for="package_use_same_more_info" class="custom-checkbox-label">Use the same image for "More info"</label>
                            </div>
                        </div>

                        <div class="form-group js-sortable-list sortable-list">
                            <div class="control-label">Type of package benefit</div>
                            <div class="control-sublabel">Maximum benefit can be add up front of package details will be 4 type of benefit</div>
                            <div id="js-event-package-sortable-list" data-bind="source: selected.blocks"
                                 data-template="event-package-blocks-template"></div>
                        </div>

                        <script id="event-package-blocks-template" type="text/x-kendo-template">
                             # if(get("type") === 'ticket') { #
                             <div class="js-event-package-sortable sortable sortable-block">
                                    <div class="right-handler">
                                        <i class="k-icon k-i-more-vertical js-event-package-handler handler"></i>
                                    </div>
                                    <div class="sortable-block-wrap">
                                        <div class="sortable-block-header">
                                            <h4 class="handler title text-uppercase">Ticket</h4>
                                            <input type="hidden"
                                                   class="form-control"
                                                   name="package_ticket_block_id"
                                                   data-bind="value: id">
                                            <input type="hidden"
                                                   class="event-package-block-pos"
                                                   name="package_ticket_block_pos"
                                                   data-bind="value: pos">
                                            <div class="top-checkbox-wrap">
                                                <label class="toggle-switch round">
                                                <input id="package_ticket_block_is_active"
                                                       type="checkbox"
                                                       class="checkbox-recommend"
                                                       name="package_ticket_block_is_active"
                                                       data-bind="value: is_active"
                                                       data-type="number">
                                                <span class="slider">
                                                    <span class="labels">on</span>
                                                    <span class="labels">off</span>
                                                </span>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="js-event-package-sortable-panel">
                                            <div class="form-group">
                                                <label for="package_ticket_block_content" class="control-label">
                                                    Ticket name
                                                </label>
                                                <input id="package_ticket_block_content"
                                                       type="text"
                                                       class="form-control"
                                                       name="package_ticket_block_content"
                                                       data-bind="value: content">
                                                <div class="error-block">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             # } else if (get("type") === 'flight') { #
                             <div class="js-event-package-sortable sortable sortable-block">
                                    <div class="right-handler">
                                        <i class="k-icon k-i-more-vertical js-event-package-handler handler"></i>
                                    </div>
                                    <div class="sortable-block-wrap">
                                        <div class="sortable-block-header">
                                            <h4 class="handler title text-uppercase">Flight</h4>
                                            <input type="hidden"
                                                   class="form-control"
                                                   name="package_flight_block_id"
                                                   data-bind="value: id">
                                            <input type="hidden"
                                                   class="event-package-block-pos"
                                                   name="package_flight_block_pos"
                                                   data-bind="value: pos">
                                            <div class="top-checkbox-wrap">
                                                <label class="toggle-switch round">
                                                <input id="package_flight_block_is_active"
                                                       type="checkbox"
                                                       class="checkbox-recommend"
                                                       name="package_flight_block_is_active"
                                                       value="1"
                                                       data-bind="checked: is_active"
                                                       data-type="number">
                                                <span class="slider">
                                                    <span class="labels">on</span>
                                                    <span class="labels">off</span>
                                                </span>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="js-event-package-sortable-panel">
                                            <div class="form-group">
                                                <label for="package_flight_block_content" class="control-label">
                                                    Flight summary info
                                                </label>
                                                <input id="package_flight_block_content"
                                                       type="text"
                                                       class="form-control"
                                                       name="package_flight_block_content"
                                                       data-bind="value: content">
                                                <div class="error-block">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             # } else if (get("type") === 'hotel') { #
                             <div class="js-event-package-sortable sortable sortable-block">
                                    <div class="right-handler">
                                        <i class="k-icon k-i-more-vertical js-event-package-handler handler"></i>
                                    </div>
                                    <div class="sortable-block-wrap">
                                        <div class="sortable-block-header">
                                            <h4 class="handler title text-uppercase">Hotel</h4>
                                            <input type="hidden"
                                                   class="form-control"
                                                   name="package_hotel_block_id"
                                                   data-bind="value: id">
                                            <input type="hidden"
                                                   class="event-package-block-pos"
                                                   name="package_hotel_block_pos"
                                                   data-bind="value: pos">
                                            <div class="top-checkbox-wrap">
                                                <label class="toggle-switch round">
                                                <input id="package_hotel_block_is_active"
                                                       type="checkbox"
                                                       class="checkbox-recommend"
                                                       name="package_hotel_block_is_active"
                                                       value="1"
                                                       data-bind="checked: is_active"
                                                       data-type="number">
                                                <span class="slider">
                                                    <span class="labels">on</span>
                                                    <span class="labels">off</span>
                                                </span>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="js-event-package-sortable-panel">
                                            <div class="form-group">
                                                <label for="package_hotel_block_content" class="control-label">
                                                    Hotel summary info
                                                </label>
                                                <input id="package_hotel_block_content"
                                                       type="text"
                                                       class="form-control"
                                                       name="package_hotel_block_content"
                                                       data-bind="value: content">
                                                <div class="error-block">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             # } else if (get("type") === 'merchandise') { #
                             <div class="js-event-package-sortable sortable sortable-block">
                                    <div class="right-handler">
                                        <i class="k-icon k-i-more-vertical js-event-package-handler handler"></i>
                                    </div>
                                    <div class="sortable-block-wrap">
                                        <div class="sortable-block-header">
                                            <h4 class="handler title text-uppercase">Merchandise</h4>
                                            <input type="hidden"
                                                   class="form-control"
                                                   name="package_merchandise_block_id"
                                                   data-bind="value: id">
                                            <input type="hidden"
                                                   class="event-package-block-pos"
                                                   name="package_merchandise_block_pos"
                                                   data-bind="value: pos">
                                            <div class="top-checkbox-wrap">
                                                <label class="toggle-switch round">
                                                <input id="package_merchandise_block_is_active"
                                                       type="checkbox"
                                                       class="checkbox-recommend"
                                                       name="package_merchandise_block_is_active"
                                                       value="1"
                                                       data-bind="checked: is_active"
                                                       data-type="number">
                                                <span class="slider">
                                                    <span class="labels">on</span>
                                                    <span class="labels">off</span>
                                                </span>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="js-event-package-sortable-panel">
                                            <div class="form-group">
                                                <label for="package_merchandise_block_content" class="control-label">
                                                    Merchandise summary info
                                                </label>
                                                <input id="package_merchandise_block_content"
                                                       type="text"
                                                       class="form-control"
                                                       name="package_merchandise_block_content"
                                                       data-bind="value: content">
                                                <div class="error-block">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             # } else { #
                             <div class="js-event-package-sortable sortable sortable-block">
                                    <div class="right-handler">
                                        <i class="k-icon k-i-more-vertical js-event-package-handler handler"></i>
                                    </div>
                                    <div class="sortable-block-wrap">
                                        <div class="sortable-block-header">
                                            <h4 class="handler title text-uppercase">Other Perks</h4>
                                            <input type="hidden"
                                                   class="form-control"
                                                   name="package_perks_block_id"
                                                   data-bind="value: id">
                                            <input type="hidden"
                                                   class="event-package-block-pos"
                                                   name="package_perks_block_pos"
                                                   data-bind="value: pos">
                                            <div class="top-checkbox-wrap">
                                                <label class="toggle-switch round">
                                                <input id="package_perks_block_is_active"
                                                       type="checkbox"
                                                       class="checkbox-recommend"
                                                       name="package_perks_block_is_active"
                                                       value="1"
                                                       data-bind="checked: is_active"
                                                       data-type="number">
                                                <span class="slider">
                                                    <span class="labels">on</span>
                                                    <span class="labels">off</span>
                                                </span>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="js-event-package-sortable-panel">
                                            <div class="form-group">
                                                <label for="package_perks_block_icon" class="control-label with-sublabel">Icon</label>
                                                <div class="control-sublabel">
                                                    Icon need to be <span
                                                            class="bold">width: 30px</span> and
                                                    <span class="bold">height: 30px</span> and in
                                                    <span class="bold">PNG format</span>
                                                </div>
                                                <div class="drop-zone drop-croppie-perks-icon drop-zone-ticket horizontal"
                                                     data-bind="attr: {data-url : iconUrl}">
                                                    <input id="package_perks_block_icon"
                                                           type="hidden"
                                                           class="form-control"
                                                           name="package_perks_block_icon"
                                                           data-bind="value: icon">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="package_perks_block_content" class="control-label">
                                                    Perks summary info
                                                </label>
                                                <input id="package_perks_block_content"
                                                       type="text"
                                                       class="form-control"
                                                       name="package_perks_block_content"
                                                       data-bind="value: content">
                                                <div class="error-block">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             # } #
                        </script>


                        <div class="form-group">
                            <label for="package_price_rating" class="control-label">Package price</label>
                            <div class="price-inputs-wrap">
                                <div class="rating-input-wrap">
                                    <div class="select-wrap">
                                        <select name="package_price_rating"
                                                id="package_price_rating"
                                                class="form-control form-control--select"
                                                data-bind="value: selected.currency_id">
                                            @foreach($currencies as $currency)
                                                <option value="{{ $currency->id }}">{{ $currency->code }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="error-block" data-for="selected.currency_id"></div>
                                </div>
                                <div class="number-input-wrap d-flex align-items-center">
                                    <label for="package_price_number" class="control-label-horizontal">From</label>
                                    <input id="package_price_number"
                                           name="package_price_number"
                                           type="number"
                                           class="form-control"
                                           min="0"
                                           step="0.01"
                                           data-bind="value: selected.price">
                                    <div class="error-block" data-for="selected.price"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="package_call_to_action_link" class="control-label with-sublabel">Call to action URL</label>
                            <div class="control-sublabel">Package URL will be generate based on Package Name</div>
                            <div class="input-constant-value-wrap">
                                <span class="constant-value-wrap">{{ str_finish(url('/'), '/') }}</span>
                                <input id="package_call_to_action_link"
                                       name="call_to_action_link"
                                       type="text"
                                       class="form-control"
                                       placeholder="type your link here"
                                       data-bind="value: selected.call_to_action_link">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="package_status" class="control-label">Package status</label>
                            <div class="select-wrap">
                                <select class="form-control form-control--select"
                                        name="package_status"
                                        id="package_status"
                                        data-bind="value: selected.status">
                                    @foreach($packageStatuses as $packageStatus)
                                        <option value="{{ $packageStatus }}">{{ $packageStatus }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>More info pop-up</legend>

                        <div class="modal-title-small">Mor info pop-up</div>

                        <div class="form-group">
                            <label for="package_header_image" class="control-label with-sublabel">Pop up header image</label>
                            <div class="control-sublabel">
                                Pop-up header image image need to be <span class="bold">width: 788px</span> and
                                <span class="bold">height: 300px</span>
                            </div>
                            <div class="drop-zone drop-croppie-package-header drop-zone-ticket horizontal" data-url="">
                                <input id="package_header_image"
                                       type="hidden"
                                       class="form-control"
                                       name="header_image"
                                       data-bind="value: selected.header_image">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="package_description" class="control-label">Package description</label>
                            <div class="control-sublabel">Describe what the ticket includes and benefits</div>
                            <textarea data-provide="markdown-editable"
                                      id="package_description"
                                      class="form-control"
                                      name="package_content"
                                      data-bind="value: selected.description"
                                      placeholder="Brief package description">
                        </textarea>
                        </div>

                        <div class="form-group">
                            <label for="package_details" class="control-label">Package detail information</label>
                            <div class="control-sublabel">Include terms and conditions of the ticket or important notes</div>
                            <textarea id="package_details"
                                      class="form-control"
                                      name="details"
                                      aria-label="editor"
                                      data-bind="value: selected.details">
                        </textarea>
                            <div class="error-block" data-for="selected.details"></div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-primary-blur btn-xl" data-bind="events: { click: sync }">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>