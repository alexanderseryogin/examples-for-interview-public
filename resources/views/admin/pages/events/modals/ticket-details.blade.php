<div class="modal fade ticket-details" id="edit-event-ticket-modal" tabindex="-1" role="dialog" aria-labelledby="edit-event-ticket-modal"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form action="" id="edit-event-ticket-form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-label="Close" data-bind="events: { click: cancel }">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group-block">
                        <div class="modal-title-small">Ticket details</div>

                        <div class="form-group">
                            <label class="control-label">Event name</label>
                            <div class="lead">{{ $event->name }}</div>
                        </div>

                        <div class="form-group">
                            <label for="ticket_name" class="control-label">Ticket name</label>
                            <input id="ticket_name"
                                   type="text"
                                   class="form-control"
                                   name="ticket_name"
                                   required
                                   data-bind="value: selected.name"
                                   placeholder="Type your ticket name">
                            <div class="error-block" data-for="selected.name"></div>
                        </div>

                        <div class="form-group">
                            <label for="ticket_category" class="control-label">Ticket category</label>
                            <input id="ticket_category"
                                   type="text"
                                   class="form-control"
                                   name="ticket_category"
                                   data-bind="value: selected.category"
                                   placeholder="Type your ticket category">
                            <div class="error-block" data-for="selected.category"></div>
                        </div>

                        <div class="form-group">
                            <label for="ticket_price_currency" class="control-label">Ticket price</label>
                            <div class="price-inputs-wrap">
                                <div class="rating-input-wrap">
                                    <div class="select-wrap">
                                        <select name="ticket_price_currency"
                                                id="ticket_price_currency"
                                                class="form-control form-control--select"
                                                data-bind="value: selected.currency_id">
                                            @foreach($currencies as $currency)
                                                <option value="{{ $currency->id }}">{{ $currency->code }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="error-block" data-for="selected.currency_id"></div>
                                </div>
                                <div class="number-input-wrap">
                                    <input id="ticket_price"
                                           name="ticket_price"
                                           type="number"
                                           min="0"
                                           step="0.01"
                                           class="form-control"
                                           data-bind="value: selected.price">
                                    <div class="error-block" data-for="selected.price"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="price-inputs-wrap">
                                <div class="rating-input-wrap">
                                    <label class="control-label">Ticket Discount</label>
                                    <div class="form-bottom-inputs">
                                        <label class="toggle-switch round" for="ticket_has_discount">
                                            <input id="ticket_has_discount"
                                                   type="checkbox"
                                                   class="checkbox-recommend"
                                                   name="ticket_has_discount"
                                                   data-bind="checked: selected.has_discount, events: {change: changeHasDiscount}"
                                                   data-type="number">
                                            <span class="slider">
                                            <span class="labels">off</span>
                                            <span class="labels">on</span>
                                        </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="rating-input-wrap">
                                    <label for="ticket_discount_price" class="control-label with-sublabel">Discount price</label>
                                    <div class="control-sublabel">
                                        Enter in the total for the final discount price
                                    </div>
                                    <input id="ticket_discount_price"
                                           name="ticket_discount_price"
                                           type="number"
                                           min="0"
                                           step="0.01"
                                           class="form-control"
                                           data-bind="value: selected.discount_price">
                                    <div class="error-block" data-for="selected.discount_price"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="checkout_url" class="control-label">Checkout URL</label>
                            <input id="checkout_url"
                                   type="url"
                                   class="form-control"
                                   name="checkout_url"
                                   required
                                   data-bind="value: selected.checkout_url"
                                   placeholder="Type checkout url">
                            <div class="error-block" data-for="selected.checkout_url"></div>
                        </div>

                        <div class="form-group">
                            <label for="ticket_status" class="control-label">Ticket status</label>
                            <div class="select-wrap">
                                <select class="form-control form-control--select"
                                        name="ticket_status"
                                        id="ticket_status"
                                        data-bind="value: selected.status">
                                    @foreach($ticketStatuses as $ticketStatus)
                                        <option value="{{ $ticketStatus }}">{{ $ticketStatus }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ticket_tier" class="control-label">Ticket tier (optional)</label>
                            <input id="ticket_tier"
                                   type="text"
                                   class="form-control"
                                   name="ticket_tier"
                                   data-bind="value: selected.tier"
                                   placeholder="Type your ticket tier">
                            <div class="error-block" data-for="selected.tier"></div>
                        </div>

                        <div class="form-group d-flex datetime-input-wrap">
                            <div class="date-input-wrap">
                                <div>
                                    <label for="ticket_sales_end_date" class="control-label">Sales End: Date</label>
                                </div>
                                <input id="ticket_sales_end_date"
                                       type="text"
                                       class="form-control"
                                       name="sales_end_date"
                                       data-bind="value: selected.sales_end_date"
                                       placeholder="13 November 2019">
                                <div class="error-block" data-for="selected.sales_end_date"></div>
                            </div>
                            <div class="time-input-wrap">
                                <label for="sales_end_date_hour" class="control-label">Hour</label>
                                <div class="select-wrap">
                                    <select class="form-control form-control--select"
                                            id="sales_end_date_hour"
                                            name="sales_end_date_hour"
                                            data-bind="value: selected.sales_end_date_hour">
                                        @foreach(\App\Helpers\DateHelper::hoursList() as $hour)
                                            <option value="{{ $hour }}">{{ $hour }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="error-block" data-for="selected.sales_end_date_hour"></div>
                            </div>
                            <div class="time-input-wrap">
                                <label for="sales_end_date_minute" class="control-label">Minute</label>
                                <div class="select-wrap">
                                    <select class="form-control form-control--select"
                                            id="sales_end_date_minute"
                                            name="sales_end_date_minute"
                                            data-bind="value: selected.sales_end_date_minute">
                                        @foreach(\App\Helpers\DateHelper::minutesList() as $minute)
                                            <option value="{{ $minute }}">{{ $minute }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="error-block" data-for="selected.sales_end_date_minute"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Ticket Tier End - Countdown Timer</label>
                            <div class="form-bottom-inputs">
                                <label class="toggle-switch round" for="ticket_show_countdown">
                                    <input id="ticket_show_countdown"
                                           type="checkbox"
                                           class="checkbox-recommend"
                                           name="ticket_show_countdown"
                                           data-bind="checked: selected.show_countdown"
                                           data-type="number">
                                    <span class="slider">
                                        <span class="labels">off</span>
                                        <span class="labels">on</span>
                                    </span>
                                    Show on event detail page
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="modal-title-small">More info pop-up</div>

                    <div class="form-group">
                        <label for="ticket_header_image" class="control-label with-sublabel">Header Image</label>
                        <div class="control-sublabel">
                            Pop-up header image need to be <span class="bold">width: 788px</span> and
                            <span class="bold">height: 300px</span>
                        </div>
                        <div class="drop-zone drop-croppie-header drop-zone-ticket horizontal" data-url="">
                            <input id="ticket_header_image"
                                   type="hidden"
                                   class="form-control"
                                   name="header_image"
                                   data-bind="value: selected.header_image">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ticket_description" class="control-label">Ticket Description</label>
                        <div class="control-sublabel">Describe what the ticket includes and benefits</div>
                        <textarea data-provide="markdown-editable"
                                  id="ticket_description"
                                  class="form-control js-ticket-editor"
                                  name="ticket_description"
                                  aria-label="editor"
                                  data-bind="value: selected.description"
                                  placeholder="Brief ticket description">
                        </textarea>
                    </div>

                    <div class="form-group">
                        <label for="ticket_details" class="control-label">Ticket detail information</label>
                        <div class="control-sublabel">Include terms and conditions of the ticket or important notes</div>
                        <textarea id="ticket_details"
                                  class="form-control js-ticket-editor"
                                  name="details"
                                  aria-label="editor"
                                  data-bind="value: selected.details">
                        </textarea>
                        <div class="error-block" data-for="selected.details"></div>
                    </div>

                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-primary-blur btn-xl" data-bind="events: { click: sync }">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>