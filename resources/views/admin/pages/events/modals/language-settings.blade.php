<div class="modal fade language-setting" id="languageSetting" tabindex="-1" role="dialog" aria-labelledby="languageSetting" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Language Setting</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="language1">Language 1</label>
                        <div class="d-flex input-group-with-del">
                            <div class="select-wrap select-wrap-arrow">
                                <select class="form-control form-control--select"
                                        name="language1"
                                        id="language1">
                                    <option value="0" selected>English</option>
                                    <option value="1">Chinese</option>
                                    <option value="2">Thai</option>
                                    <option value="3">Tagalog</option>
                                    <option value="4">Tamil</option>
                                </select>
                            </div>
                            <button class="btn-link btn-link-grey">Delete</button>
                        </div>
                        <div class="bottom-checkbox-wrap">
                            <input class="custom-checkbox" type="checkbox" id="defaultViewName" checked name="checkAll">
                            <label for="defaultViewName" class="custom-checkbox-label">Set as default</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="language2">Language 2</label>
                        <div class="d-flex input-group-with-del">
                            <div class="select-wrap select-wrap-arrow">
                                <select class="form-control form-control--select"
                                        name="language2"
                                        id="language2">
                                    <option value="0">English</option>
                                    <option value="1" selected>Chinese</option>
                                    <option value="2">Thai</option>
                                    <option value="3">Tagalog</option>
                                    <option value="4">Tamil</option>
                                </select>
                            </div>
                            <button class="btn-link btn-link-grey">Delete</button>
                        </div>
                        <div class="bottom-checkbox-wrap">
                            <input class="custom-checkbox" type="checkbox" id="defaultViewName" name="checkAll">
                            <label for="defaultViewName" class="custom-checkbox-label">Set as default</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="language3">Language 3</label>
                        <div class="d-flex input-group-with-del">
                            <div class="select-wrap select-wrap-arrow">
                                <select class="form-control form-control--select"
                                        name="language3"
                                        id="language3">
                                    <option value="0">English</option>
                                    <option value="1">Chinese</option>
                                    <option value="2" selected>Thai</option>
                                    <option value="3">Tagalog</option>
                                    <option value="4">Tamil</option>
                                </select>
                            </div>
                            <button class="btn-link btn-link-grey">Delete</button>
                        </div>
                        <div class="bottom-checkbox-wrap">
                            <input class="custom-checkbox" type="checkbox" id="defaultViewName" name="checkAll">
                            <label for="defaultViewName" class="custom-checkbox-label">Set as default</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="language4">Language 4</label>
                        <div class="d-flex input-group-with-del">
                            <div class="select-wrap select-wrap-arrow">
                                <select class="form-control form-control--select"
                                        name="language4"
                                        id="language4">
                                    <option value="0">English</option>
                                    <option value="1">Chinese</option>
                                    <option value="2">Thai</option>
                                    <option value="3" selected>Tagalog</option>
                                    <option value="4">Tamil</option>
                                </select>
                            </div>
                            <button class="btn-link btn-link-grey">Delete</button>
                        </div>
                        <div class="bottom-checkbox-wrap">
                            <input class="custom-checkbox" type="checkbox" id="defaultViewName" name="checkAll">
                            <label for="defaultViewName" class="custom-checkbox-label">Set as default</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="language5">Language 5</label>
                        <div class="d-flex input-group-with-del">
                            <div class="select-wrap select-wrap-arrow">
                                <select class="form-control form-control--select"
                                        name="language5"
                                        id="language5">
                                    <option value="0">English</option>
                                    <option value="1">Chinese</option>
                                    <option value="2">Thai</option>
                                    <option value="3">Tagalog</option>
                                    <option value="4" selected>Tamil</option>
                                </select>
                            </div>
                            <button class="btn-link btn-link-grey">Delete</button>
                        </div>
                        <div class="bottom-checkbox-wrap">
                            <input class="custom-checkbox" type="checkbox" id="defaultViewName" name="checkAll">
                            <label for="defaultViewName" class="custom-checkbox-label">Set as default</label>
                        </div>
                    </div>

                    <button class="text-primary add-link">
                        <span class="k-icon k-i-globe-outline"></span>Add another language
                    </button>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-primary-blur btn-xl">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>