@extends('adminlte::page')

@section('title', 'Edit Event')

@section('content_header')

@endsection

@section('content')
    <div class="content-header">
        <div class="content-header-inner-wrap">
            <a href="{{ route('admin.events.index') }}" class="link-back">&lt; Back</a>
            <div class="row">
                <div class="col-lg-8">
                    <div class="row content-header-bottom-row">
                        <div class="col-lg-8">
                            <h1>Edit Event</h1>
                        </div>
                        <div class="col-lg-4">
                            <form action="{{route('admin.events.publish', $event->id)}}" method="post">
                                @csrf
                                <input type="submit"
                                       name="action"
                                       value="Publish"
                                       class="btn btn-primary_gradient btn-lg-small pull-right">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content-wrap">
        {{-- Event create/edit form--}}
        <div class="nav-tabs-custom" id="tabstrip">
            <ul class="nav nav-tabs nav-tabs_general nav-tabs_with-link">
                <li class="k-state-active">General Info</li>
                <li>Tickets</li>
                <li>Event Info</li>
                <li>FAQ</li>
                <li>Packages</li>
                {{--<a href=""><i class="k-icon k-i-plus"></i> Add tab</a>--}}
            </ul>

            <div class="tab-content">
                {{ Form::model($event, [
                    'method' => 'POST',
                    'id' => 'event-general-info-form',
                    'class' => 'autosave',
                    'url' => route('admin.events.general-info.update', ['event' => $event->id])
                ]) }}
                    @include('admin.pages.events.partials.tabs.general-info')
                {{ Form::close() }}
            </div>

            <div class="tab-content">
                @include('admin.pages.events.partials.tabs.tickets')
            </div>

            <div class="tab-content" id="event-info">
                @include('admin.pages.events.partials.tabs.event-info')
            </div>

            <div class="tab-content" id="event-faqs">
                @include('admin.pages.events.partials.tabs.faq')
            </div>

            <div class="tab-content">
                @include('admin.pages.events.partials.tabs.packages')
            </div>
        </div>

        @include('admin.pages.events.partials.right-sidebar')
    </div>

    {{--@include('admin.shared.logs', ['logs' => $event->logsLatest])--}}
@endsection

@section('modal')
    @include('admin.modals.crop')
    @include('admin.modals.create-company', ['type' => \App\Models\Company::TYPE_EVENT_ORGANIZER])
    @include('admin.pages.events.modals.ticket-details')
    @include('admin.pages.events.modals.package-details')
@endsection

@section('js')
    <script>
        var currencies = JSON.parse('{!! json_encode($currencies) !!}');
        var ticketStatuses = JSON.parse('{!! json_encode($ticketStatuses) !!}');
        var eventPackageStatuses = JSON.parse('{!! json_encode($packageStatuses ) !!}');
        var eventPackageCardSizes = JSON.parse('{!! json_encode($packageCardSizes ) !!}');
    </script>
    <script id="no-data-template" type="text/x-kendo-tmpl">
        <div>
            <button class="js-add-new-organizer add-button" onclick='addNewEventOrganizer("#: instance.text() #")'>
                #: instance.text() #<span>(Create new company)</span>
            </button>
        </div>
    </script>

    <script src="{{asset('js/components/admin/events.js')}}"></script>
@stop