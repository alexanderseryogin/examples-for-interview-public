<div class="content-box">
    <div class="box box-solid">
        <div class="box-body">

            <div class="form-group{{ $errors->has('event_id') ? ' has-error' : '' }}">
                {{Form::label('event_id', 'Event Name', ['class' => 'control-label'])}}
                <div>
                    {{ Form::text('event_id') }}
                </div>
                <div class="error-block">
                    {{ $errors->first('event_id') }}
                </div>
            </div>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {{Form::label('name', 'Ticket name', ['class' => 'control-label'])}}
                {{Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Type your ticket name'])}}
                <div class="error-block">
                    {{ $errors->first('name') }}
                </div>
            </div>

            <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                {{Form::label('category', 'Ticket category', ['class' => 'control-label'])}}
                {{Form::text('category', null, ['class'=>'form-control', 'placeholder'=>'Type your ticket category'])}}
                <div class="error-block">
                    {{ $errors->first('category') }}
                </div>
            </div>

            <div class="form-group {{ $errors->has('currency_id') || $errors->has('price') ? ' has-error' : '' }}">
                {{Form::label('currency_id', 'Ticket price', ['class' => 'control-label'])}}
                <div class="price-inputs-wrap">
                    <div class="rating-input-wrap">
                        <div class="select-wrap">
                            {{Form::select('currency_id', $currencies, null, ['class'=>'form-control', 'placeholder'=>'Select currency'])}}
                        </div>
                        <div class="error-block">
                            {{ $errors->first('currency_id') }}
                        </div>
                    </div>
                    <div class="number-input-wrap">
                        {{Form::number('price', null, ['class'=>'form-control', 'placeholder' => '0', 'min' => 0, 'step' => 0.01])}}
                        <div class="error-block">
                            {{ $errors->first('price') }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
                <div class="price-inputs-wrap">
                    <div class="rating-input-wrap">
                        <label class="control-label">Ticket Discount</label>
                        <div class="form-bottom-inputs">
                            <label class="toggle-switch round" for="has_discount">
                                {{ Form::checkbox('has_discount', 1, null, ['class' => 'checkbox-recommend', 'id' => 'has_discount']) }}
                                <span class="slider">
                                <span class="labels">off</span>
                                <span class="labels">on</span>
                            </span>
                            </label>
                        </div>
                    </div>
                    <div class="rating-input-wrap">
                        {{Form::label('discount_price', 'Discount Price', ['class' => 'control-label'])}}
                        <div class="control-sublabel">
                            Enter in the total for the final discount price
                        </div>
                        {{Form::number('discount_price', null, [
                            'class'=>'form-control',
                            'placeholder' => '0',
                            'min' => 0,
                            'step' => 0.01,
                            'disabled' => !old('has_discount', $ticket->has_discount)
                           ])}}
                        <div class="error-block">
                            {{ $errors->first('discount_price') }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group {{ $errors->has('checkout_url') ? ' has-error' : '' }}">
                {{Form::label('checkout_url', 'Checkout URL', ['class' => 'control-label'])}}
                {{Form::text('checkout_url', null, ['class'=>'form-control', 'placeholder'=>'Type checkout url'])}}
                <div class="error-block">
                    {{ $errors->first('checkout_url') }}
                </div>
            </div>

            <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                {{Form::label('status', 'Ticket status', ['class' => 'control-label'])}}
                <div class="select-wrap">
                    {{Form::select('status', $statuses, null, ['class'=>'form-control', 'placeholder'=>'Select your ticket status'])}}
                </div>
                <div class="error-block">
                    {{ $errors->first('status') }}
                </div>
            </div>

            <div class="form-group{{ $errors->has('tier') ? ' has-error' : '' }}">
                {{Form::label('tier', 'Ticket tier (optional)', ['class' => 'control-label'])}}
                {{Form::text('tier', null, ['class'=>'form-control', 'placeholder'=>'Type your ticket tier'])}}
                <div class="error-block">
                    {{ $errors->first('tier') }}
                </div>
            </div>

            <div class="form-group d-flex datetime-input-wrap">
                <div class="date-input-wrap">
                    {{Form::label('sales_end_date', 'Sales End: Date', ['class' => 'control-label'])}}
                    {{Form::text('sales_end_date', $ticket->sales_end_date, ['class' => 'form-control', 'placeholder' => '13 November 2019'])}}
                </div>
                <div class="time-input-wrap">
                    {{ Form::label('sales_end_date_hour', 'Hour', ['class' => 'control-label']) }}
                    <div class="select-wrap">
                        {{ Form::select('sales_end_date_hour',
                            \App\Helpers\DateHelper::hoursList(),
                            optional($ticket->sales_end_date)->format('G'), [
                                'class' => 'form-control form-control--select',
                            ]) }}
                    </div>
                </div>
                <div class="time-input-wrap">
                    {{ Form::label('sales_end_date_minute', 'Minute', ['class' => 'control-label']) }}
                    <div class="select-wrap">
                        {{ Form::select('sales_end_date_minute',
                            \App\Helpers\DateHelper::minutesList(),
                            optional($ticket->sales_end_date)->format('i'), [
                                'class' => 'form-control form-control--select',
                            ]) }}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label">Ticket Tier End - Countdown Timer</label>

                <div class="form-bottom-inputs">
                    <label class="toggle-switch round" for="show_countdown">
                        {{ Form::checkbox('show_countdown', 1, null, ['class' => 'checkbox-recommend', 'id' => 'show_countdown']) }}
                        <span class="slider">
                                <span class="labels">off</span>
                                <span class="labels">on</span>
                            </span>
                        Show on event detail page
                    </label>
                </div>
            </div>

            <div class="modal-title-small">More info pop-up</div>

            <div class="form-group">
                {{Form::label('header_image', 'Header image', ['class' => 'control-label'])}}
                <div class="control-sublabel">
                    Pop-up header image need to be <span class="bold">width: 788px</span> and
                    <span class="bold">height: 300px</span>
                </div>
                <div class="drop-zone drop-croppie-header vertical"
                     style="width: 602px;height: 222px;"
                     data-url="{{ old('header_image') ? '/storage/' . old('header_image') : optional($ticket)->headerImageUrl }}">
                    {{Form::hidden('header_image', null, ['id' => 'header_image'])}}
                </div>
            </div>

            <div class="form-group">
                {{Form::label('ticket_description', 'Ticket Description', ['class' => 'control-label with-sublabel'])}}
                <div class="control-sublabel">Describe what the ticket includes and benefits</div>
                {{Form::textarea('description', null, [
                        'id' => 'ticket_description',
                        'class' => 'form-control js-editor',
                        'placeholder' => 'Brief ticket description'
                   ])}}
                <div class="error-block">
                    {{ $errors->first('description') }}
                </div>
            </div>

            <div class="form-group">
                {{Form::label('ticket_details', 'Ticket detail information', ['class' => 'control-label'])}}
                <div class="control-sublabel">Include terms and conditions of the ticket or important notes</div>
                {{Form::textarea('details', null, [
                        'id' => 'ticket_details',
                        'class' => 'form-control js-editor',
                        'aria-label' => 'editor',
                   ])}}
                <div class="error-block">
                    {{ $errors->first('details') }}
                </div>
            </div>

            <div class="tab-submit-wrap">
                <input type="submit" name="action" value="Save" class="btn btn-primary-blur btn-lg-small">
            </div>
        </div>
    </div>
</div>