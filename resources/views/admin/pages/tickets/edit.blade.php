@extends('adminlte::page')

@section('title', 'Edit Ticket')

@section('content_header')

@endsection

@section('content')
    <div class="content-header">
        <div class="content-header-inner-wrap">
            <a href="{{ route('admin.tickets.index') }}" class="link-back">&lt; Back</a>
            <div class="row">
                <div class="col-lg-8">
                    <div class="row content-header-bottom-row">
                        <div class="col-lg-8">
                            <h1>Edit Ticket</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content-wrap mt-20">
        <div class="content-box-wrap">
            {{ Form::model($ticket, [
                    'method' => 'POST',
                    'url' => route('admin.tickets.update', ['id' => $ticket->id])
                ]) }}

            @include('admin.pages.tickets.partials.form')

            {{ Form::close() }}
        </div>
    </div>

@endsection

@section('modal')
    @include('admin.modals.crop')
@endsection

@push('scripts')

@endpush

@section('js')
    <script src="{{asset('js/components/admin/tickets.js')}}"></script>
@stop