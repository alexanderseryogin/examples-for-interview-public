@extends('adminlte::page')

@section('title', 'Venue Management')

@section('content_header')

@stop

@section('content')
    <div class="content-box-wrap">
        <div class="content-box-large">
            <div class="panel-heading">
                <h1 class="panel-heading-title">Venue Management</h1>
                <form action="{{ route('admin.venues.index') }}" method="get">
                    <div class="panel-heading-search input-group">
                        <input type="text" name="q" value="{{ $q }}" class="form-control" placeholder="Search" id="venues-search">
                        <span class="input-group-addon" id="venues-search-btn">
                        <i class="k-icon k-i-search"></i>
                    </span>
                    </div>
                </form>
                <div class="panel-heading-row">
                    <a class="btn btn-lg-small btn-outline-primary getTix-btn" href="{{ route('admin.venues.create') }}">Add</a>
                    <input id="select-view" class="select-input" placeholder="Type and save this view">
                </div>
            </div>
            <div class="panel-body table-responsive no-grid-toolbar">

                <div id="grid"></div>

                <ul class="table-functionality">
                    <li><a href onclick="batchDeleteVenue('delete', event)">Delete</a></li>
                </ul>
            </div>
        </div>
    </div>

@endsection

@section('modal')

    @include('admin.modals.create-view')
    @include('admin.modals.confirm-modal')

@endsection

@push('scripts')
@endpush

@section('js')
    <script id="noDataTemplate" type="text/x-kendo-tmpl">
        <button class="js-add-new-view add-button" onclick="addNewView('#: instance.text() #')">#: instance.text() #<span>(Create new view)</span></button>
    </script>

    <script id="buttonMenuTemplate" type="text/x-kendo-tmpl">
        <ul class="button-menu">
            <li><i class="fa fa-fw fa-cog"></i>
                <ul>
                    <li class="bordered-item">
                        <a href="{{ route('admin.venues.create') }}"><i class="k-icon k-i-plus"></i>Add</a>
                    </li>
                    <li>
                        <a href="/admin/venues/#= id #"><i class="k-icon k-i-edit"></i>Edit</a>
                    </li>
                    <li>
                        <a href onclick="deleteVenue(#= id #, 'delete', event)"><i class="k-icon k-i-minus"></i>Delete</a>
                    </li>
                </ul>
            </li>
        </ul>
    </script>

    <script src="{{asset('js/components/admin/venues.js')}}"></script>
@stop