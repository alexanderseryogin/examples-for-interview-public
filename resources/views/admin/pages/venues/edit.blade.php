@extends('adminlte::page')

@section('title', 'Edit Venue')

@section('content_header')

@endsection

@section('content')

    <div class="content-header">
        <div class="content-header-inner-wrap">
            <a href="{{ route('admin.venues.index') }}" class="link-back">&lt; Back</a>
            <div class="row">
                <div class="col-lg-8">
                    <div class="row content-header-bottom-row">
                        <div class="col-lg-8">
                            <h1>Edit Venue</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content-wrap mt-20">
        <div class="content-box-wrap">
            {{ Form::model($venue, [
                    'method' => 'PUT',
                    'url' => route('admin.venues.update', ['venue' => $venue->id])
                ]) }}

            @include('admin.pages.venues.partials.form')

            {{ Form::close() }}
        </div>
    </div>

    {{--@include('admin.shared.logs', ['logs' => $venue->logsLatest])--}}
@endsection

@section('modal')
    @include('admin.modals.crop')
@endsection

@push('scripts')

@endpush

@section('js')
    <script>
        const countries = JSON.parse('{!! json_encode($countries, JSON_HEX_APOS ) !!}');
    </script>
    <script src="{{asset('js/components/admin/venues.js')}}"></script>
@stop