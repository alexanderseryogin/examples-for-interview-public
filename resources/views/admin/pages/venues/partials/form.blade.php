<div class="content-box">
    <div class="box box-solid">
        <div class="box-body">

            {{-- Venue name --}}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {{Form::label('name', 'Venue Name', ['class' => 'control-label'])}}
                {{Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Type venue name'])}}
                <div class="error-block">
                    {{ $errors->first('name') }}
                </div>
            </div>

            {{-- Address --}}
            <div class="form-group form-group-low{{ $errors->has('address1') ? ' has-error' : '' }}">
                {{Form::label('name', 'Venue address', ['class' => 'control-label with-sublabel'])}}
                <div class="control-sublabel">Street name</div>
                {{Form::text('address1', null, ['class'=>'form-control', 'placeholder'=>'Address 1'])}}
                <div class="error-block">
                    {{ $errors->first('address1') }}
                </div>
            </div>
            <div class="form-group{{ $errors->has('address2') ? ' has-error' : '' }}">
                {{Form::text('address2', null, ['class'=>'form-control', 'placeholder'=>'Address 2'])}}
                <div class="error-block">
                    {{ $errors->first('address2') }}
                </div>
            </div>

            {{-- City and State --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-5 col-lg-4">
                        <div class="form-group-low{{ $errors->has('city') ? ' has-error' : '' }}">
                            {{Form::label('city', 'City', ['class' => 'control-sublabel'])}}
                            {{Form::text('city', null, ['class'=>'form-control', 'placeholder'=>'Select your city'])}}
                            <div class="error-block">
                                {{ $errors->first('city') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-lg-8">
                        <div class="{{ $errors->has('state') ? ' has-error' : '' }}">
                            {{ Form::label('state', 'State', ['class' => 'control-sublabel']) }}
                            {{ Form::text('state', null, ['class' => 'form-control form-control--select', 'placeholder' => 'Select your state']) }}
                            <div class="error-block">
                                {{ $errors->first('state') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Postcode and Country --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-5 col-lg-4">
                        <div class="form-group-low{{ $errors->has('postcode') ? ' has-error' : '' }}">
                            {{Form::label('postcode', 'Postcode', ['class' => 'control-sublabel'])}}
                            {{Form::text('postcode', null, ['class'=>'form-control', 'placeholder'=>'0'])}}
                            <div class="error-block">
                                {{ $errors->first('postcode') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-lg-8">
                        <div class="{{ $errors->has('country_id') ? ' has-error' : '' }}">
                            {{ Form::label('country_id', 'Country', ['class' => 'control-sublabel']) }}
                            <div class="select-wrap">
                                {{ Form::select('country_id', $countries->prepend('Select your country', '') , null, ['class' => 'form-control form-control--select']) }}
                            </div>
                            <div class="error-block">
                                {{ $errors->first('country_id') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Link --}}
            <div class="form-group{{ $errors->has('map_link') ? ' has-error' : '' }}">
                {{Form::label('map_link', 'Google Map', ['class' => 'control-label with-sublabel'])}}
                <div class="control-sublabel">Search venue in Google Map, hit share button and paste the embed HTML link here</div>
                {{Form::text('map_link', null, ['class'=>'form-control', 'placeholder'=>'Paste embed HTML link'])}}
                <div class="error-block">
                    {{ $errors->first('map_link') }}
                </div>
                <div class="map-zone" id="google-map" data-placeholder="Your Google Map will appear here">
                    Your Google Map will appear here
                </div>
            </div>

            {{-- Venue capacity --}}
            <div class="form-group{{ $errors->has('capacity') ? ' has-error' : '' }}">
                {{Form::label('capacity', 'Venue CAPACITY', ['class' => 'control-label'])}}
                {{Form::number('capacity', null, ['class'=>'form-control', 'placeholder'=>'0', 'min' => '0','max' => '99999', 'step' => 1])}}
                <div class="error-block">
                    {{ $errors->first('capacity') }}
                </div>
                drop-croppie-controls
            </div>

            {{-- SEATING LAYOUT --}}
            <div class="form-group {{ $errors->has('seating_layout_image') ? ' has-error' : '' }}">
                {{Form::label('seat_plan', 'Seating layout', ['class' => 'control-label'])}}
                <div class="drop-zone drop-zone-seating-layout horizontal dz-clickable">
                    @php
                        $seatingLayoutImageUrl = old('seating_layout_image') ? '/storage/' . old('seating_layout_image') : optional($venue)->seatingLayoutImageUrl;
                    @endphp
                    <div id="drop-zone-seating-layout-uploaded"
                         @if($seatingLayoutImageUrl) style="display:block;" @else style="display:none;" @endif>
                        <img src="{{ $seatingLayoutImageUrl }}" alt=""/>
                        <span class="drop-zone-controls">
                            <span class="drop-zone-delete"><i class="k-icon k-i-delete"></i></span>
                        </span>
                    </div>
                    <div id="drop-zone-seating-layout-upload"
                         @if(!$seatingLayoutImageUrl) style="display:block;" @else style="display:none;" @endif>
                            <span class="drop-zone-upload-block">
                                <i class="fa fa-cloud-download"></i>Drag & Drop your files or
                                <button class="drop-zone-button drop-zone-seating-layout-button">Browse</button>
                            </span>
                    </div>
                </div>
                {{ Form::hidden('seating_layout_image', null, ['id' => 'seating_layout_image']) }}
                {{ Form::file('seating_layout_file', ['class' => 'form-control', 'id' => 'seating_layout_file']) }}
            </div>

            <div class="tab-submit-wrap">
                <input type="submit" name="action" value="Save" class="btn btn-primary-blur btn-lg-small">
            </div>
        </div>
    </div>
</div>