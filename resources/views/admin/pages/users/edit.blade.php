@extends('adminlte::page')

@section('title', 'Edit User Details')

@section('content_header')

@stop

@section('content')
    <div class="content-header">
        <div class="content-header-inner-wrap">
            <a href="{{ route('admin.users.show', $user->id) }}" class="link-back">&lt; Back</a>
            <div class="row">
                <div class="col-lg-8">
                    <div class="row content-header-bottom-row">
                        <div class="col-lg-8">
                            <h1>Edit User Details</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content-wrap mt-20">
        <div class="content-box-wrap">
            {{ Form::model($user, [
                'method' => 'PUT',
                'url' => route('admin.users.update', ['user' => $user->id])
            ]) }}

            @include('admin.pages.users.partials.form')

            {{ Form::close() }}
        </div>
    </div>
@endsection

@section('modal')
    @include('admin.modals.crop')
@endsection

@push('scripts')

@endpush

@section('js')
    <script src="{{asset('js/components/admin/users.js')}}"></script>
@stop
