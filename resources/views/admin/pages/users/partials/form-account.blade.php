{{-- Invite token --}}

{{ Form::hidden('token', $invite->token ) }}

<div class="container-sm">

    <div class="form-group-double">

        {{-- First name --}}

        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
            {{ Form::label('first_name', 'First name', ['class' => 'control-label']) }}
            {{ Form::text('first_name', optional($user->account)->first_name, ['class'=>'form-control', 'placeholder'=>'Type your first name']) }}
            <div class="error-block">
                {{ $errors->first('first_name') }}
            </div>
        </div>

        {{-- Last name --}}

        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
            {{ Form::label('last_name', 'Last name', ['class' => 'control-label']) }}
            {{ Form::text('last_name', optional($user->account)->last_name, ['class'=>'form-control', 'placeholder'=>'Type your last name']) }}
            <div class="error-block">
                {{ $errors->first('last_name') }}
            </div>
        </div>
    </div>

    {{-- Password --}}

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        {{ Form::label('password', 'Password', ['class' => 'control-label']) }}
        {{ Form::password('password', ['class'=>'form-control', 'placeholder'=>'Type your password']) }}
        <div class="error-block">
            {{ $errors->first('password') }}
        </div>
    </div>

    {{-- Password --}}

    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
        {{ Form::label('password_confirmation', 'Reconfirm password', ['class' => 'control-label']) }}
        {{ Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>'Reconfirm your password']) }}
        <div class="error-block">
            {{ $errors->first('password_confirmation') }}
        </div>
    </div>
</div>

<div class="text-center">
    <button type="submit" class="btn btn-primary-blur btn-lg">Submit</button>
</div>
