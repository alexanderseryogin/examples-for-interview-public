@php
    use App\Models\Permission;
    use App\Helpers\AuthHelper;
@endphp

<div class="content-box">
    <div class="box box-solid">

        <div class="box-body">

            {{-- Account status --}}

            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                {{Form::label('status', 'Account status', ['class' => 'control-label'])}}
                <div class="select-wrap">
                    {{Form::select('status', $statuses, $account->status, [
                        'class'     =>'form-control',
                        'disabled'  => AuthHelper::user()->cannot(Permission::CHANGE_USER_ACCOUNT_STATUSES)
                    ])}}
                </div>
                <div class="error-block">
                    {{ $errors->first('status') }}
                </div>
            </div>

            <h2>User Information</h2>

            {{-- Profile photo--}}

            <div class="form-group {{ $errors->has('avatar') ? ' has-error' : '' }}">
                {{Form::label('avatar', 'Profile photo', ['class' => 'control-label'])}}
                <div class="control-sublabel">
                    Photo need to be <span class="bold">width: 400px</span> and
                    <span class="bold">height: 400px</span>
                </div>
                <div class="drop-zone drop-croppie-avatar vertical"
                     style="width: 332px;height: 332px;"
                     data-url="{{ optional($account)->avatarUrl }}">
                    {{Form::hidden('avatar', optional($account)->avatar, ['id' => 'avatar'])}}
                </div>
            </div>

            {{-- First and Last name --}}

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6 col-lg-6">
                        <div class="form-group-low{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            {{Form::label('first_name', 'First Name', ['class' => 'control-label'])}}
                            {{Form::text('first_name', optional($user->account)->first_name, ['class'=>'form-control', 'placeholder'=>'Type your first name'])}}
                            <div class="error-block">
                                {{ $errors->first('first_name') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            {{ Form::label('last_name', 'Last Name', ['class' => 'control-label']) }}
                            {{ Form::text('last_name', optional($user->account)->last_name, ['class' => 'form-control form-control--select', 'placeholder' => 'Type your last name']) }}
                            <div class="error-block">
                                {{ $errors->first('last_name') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Email --}}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {{Form::label('email', 'Email', ['class' => 'control-label'])}}
                {{Form::text('email', null,
                    ['class'=>'form-control', 'placeholder'=>'Type your email', 'id' => 'email'])
                }}
                <div class="error-block">
                    {{ $errors->first('email') }}
                </div>
            </div>

            @role(\App\Models\Role::SUPER_ADMIN)

            <h2>Team</h2>

            {{-- Company --}}

            <div class="form-group{{ $errors->has('company_id') ? ' has-error' : '' }}">
                {{ Form::label('company_id', 'Company', ['class' => 'control-label']) }}
                {{Form::text('company_id', $user->company_id, ['class'=>'form-control', 'placeholder'=>'Type and search company name'])}}
                <div class="error-block">
                    {{ $errors->first('company_id') }}
                </div>
            </div>

            {{-- Team --}}
            {{-- No delete, for future --}}

            {{--<div class="form-group{{ $errors->has('team_id') ? ' has-error' : '' }}">
                {{ Form::label('team_id', 'Team', ['class' => 'control-label']) }}
                {{Form::text('team_id', optional($account->team)->id, ['class'=>'form-control', 'placeholder'=>'Type and search team'])}}
                <div class="error-block">
                    {{ $errors->first('team_id') }}
                </div>
            </div>--}}

            {{-- Role --}}

            <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                {{ Form::label('role', 'Roles', ['class' => 'control-label']) }}
                <div class="select-wrap">
                    {{ Form::select('role', $roles->prepend('Select role', ''), $user->roles->first()->id ?? null, ['class' => 'form-control form-control--select']) }}
                </div>
                <div class="error-block">
                    {{ $errors->first('role') }}
                </div>
            </div>

            @else

                {{-- Passwords --}}

                <h2>Security</h2>

                <div class="form-group{{ $errors->has('password_old') ? ' has-error' : '' }}">
                    {{Form::label('password_old', 'Old password', ['class' => 'control-label'])}}
                    {{Form::password('password_old',
                        ['class'=>'form-control', 'placeholder'=>'Type your old password', 'id' => 'password_old', 'autocomplete' => 'off'])
                    }}
                    <div class="error-block">
                        {{ $errors->first('password_old') }}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    {{Form::label('password', 'New password', ['class' => 'control-label'])}}
                    {{Form::password('password',
                        ['class'=>'form-control', 'placeholder'=>'Type your new password', 'id' => 'password'])
                    }}
                    <div class="error-block">
                        {{ $errors->first('password') }}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    {{Form::label('password_confirmation', 'Confirm password', ['class' => 'control-label'])}}
                    {{Form::password('password_confirmation',
                        ['class'=>'form-control', 'placeholder'=>'Reconfirm your password', 'id' => 'password_confirmation'])
                    }}
                    <div class="error-block">
                        {{ $errors->first('password_confirmation') }}
                    </div>
                </div>

                <h2>Team</h2>

                <dl class="custom-dl">
                    {{--Company--}}
                    <dt>Company</dt>
                    <dd>{{optional($user->company)->name ?? 'No company'}}</dd>

                    {{--Team--}}
                    {{-- No delete, for future --}}

                    {{--<dt>Team</dt>
                    <dd>{{optional($user->team)->name ?? 'No team'}}</dd>--}}

                    {{--Role--}}
                    <dt>Roles</dt>
                    <dd>{{$user->roleList}}</dd>
                </dl>

            @endrole

            <div class="btns-wrap text-center">
                <input type="submit" name="action" value="Save" class="btn btn-primary-blur btn-lg-small">
                <a href="{{route('admin.users.show', $user->id)}}" class="btn btn-outline-primary btn-lg-small">Cancel</a>
            </div>
        </div>

    </div>
</div>