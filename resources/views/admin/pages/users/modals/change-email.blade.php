<div class="modal fade" id="change-email-modal" tabindex="-1" role="dialog" aria-labelledby="change-email-modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change email address</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <div class="modal-body">
                    <div class="form-group">
                        <p class="control-label">Current email</p>
                        <p type="text" id="current-email"></p>
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label">New EMAIL</label>
                        <input id="email"
                               type="text"
                               class="form-control"
                               name="email"
                               placeholder="Type new email">
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-primary-blur btn-xl" id="change-email-btn">Change</button>
                    <button type="button" class="btn btn-xl btn-outline-primary" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>