<div class="modal fade" id="inviteSentModal" tabindex="-1" role="dialog" aria-labelledby="inviteSentModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-with-logo" role="document">
        <div class="modal-content text-center">
            <div class="modal-header">
                <h5 class="modal-title">We have sent an email(s) to:</h5>
                <div class="modal-subtitle">{{ session()->get('emails') }}</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modal-body-sm">
                <div class="content-text">
                    <div class="content-sub-title">
                        <p>Included in the email is a link for password creation in order to
                            complete account setup.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="{{route('admin.invites.create')}}" class="btn btn-primary btn-lg">Invite another user</a>
                <a href="{{route('admin.users.index')}}" class="btn btn-link-grey btn-lg" data-dismiss="modal">Back to user listing</a>
            </div>
        </div>
    </div>
</div>