@extends('adminlte::page-noauth')

@section('title', 'Your account has been created')

@section('content_header')

@endsection


@section('content')
    <div class="main-content-wrap d-flex align-items-center justify-content-center h-100">
        <div class="text-center">
            <img class="success-page-img" src="{{ asset('images/login/user-success.png') }}" alt="">
            <h1 class="page-title-big">Success!</h1>
            <h2 class="auth-page-h1 m-0">Your account has been created.</h2>
            <a href="{{URL::to('login')}}" class="btn btn-primary-blur btn-lg">Login to your account</a>
        </div>
    </div>
@endsection

@section('modal')

@endsection

@push('scripts')

@endpush

@section('js')
@stop
