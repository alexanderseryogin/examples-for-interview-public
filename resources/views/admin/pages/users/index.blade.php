@extends('adminlte::page')

@section('title', 'User Management')

@section('content_header')

@stop

@section('content')
    <div class="content-box-wrap">
        <div class="content-box content-box-large">
            <div class="panel-heading">
                <h1 class="panel-heading-title">Users Management</h1>
                <form action="{{ route('admin.users.index') }}" method="get">
                    <div class="panel-heading-search input-group">
                        <input type="text" name="q" value="{{ $q }}" class="form-control" placeholder="Search" id="users-search">
                        <span class="input-group-addon" id="users-search-btn">
                        <i class="k-icon k-i-search"></i>
                    </span>
                    </div>
                </form>
                <div class="panel-heading-row">
                    <a class="btn btn-lg-small btn-outline-primary getTix-btn" href="{{ route('admin.invites.create') }}">Invite user</a>
                    <input id="select-view" class="select-input" placeholder="Type and save this view">
                </div>
            </div>
            <div class="panel-body table-responsive no-grid-toolbar">


                {{-- Users grid --}}
                <div id="grid" class="grid-users"></div>

                <ul class="table-functionality">
                    <li>
                        <button onclick="batchDeleteUsers('Deleted', event)" @cannot(\App\Models\Permission::CHANGE_USER_ACCOUNT_STATUSES) disabled @endcannot>
                            Delete
                        </button>
                    </li>
                    <li>
                        <button onclick="batchUpdateUsers('Suspend', event)" @cannot(\App\Models\Permission::CHANGE_USER_ACCOUNT_STATUSES) disabled @endcannot>
                            Suspend
                        </button>
                    </li>
                    <li>
                        <button onclick="batchDeleteUsers('Pending', event)" @cannot(\App\Models\Permission::CHANGE_USER_ACCOUNT_STATUSES) disabled @endcannot>
                            Unsuspend
                        </button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('modal')

    @include('admin.modals.create-view')
    @include('admin.modals.confirm-modal')
    @include('admin.modals.notify-modal')
    @include('admin.pages.users.modals.change-email')
    @if (session()->has('emails'))
        @include('admin.pages.users.modals.invite-sent-modal')
    @endif
@endsection

@push('scripts')
@endpush

@section('js')
    <script>
        var userStatuses = @json($statuses);
        var userRoles = @json($roles);
    </script>
    <script id="noDataTemplate" type="text/x-kendo-tmpl">
        <button class="js-add-new-view add-button" onclick="addNewView('#: instance.text() #')">#: instance.text() #<span>(Create new view)</span></button>
    </script>

    <script id="buttonMenuTemplate" type="text/x-kendo-tmpl">
        <ul class="button-menu">
            <li><i class="fa fa-fw fa-cog"></i>
                <ul>
                    <li class="bordered-item">
                        <a href="/admin/users/#= id #"><i class="k-icon k-i-preview"></i>View</a>
                    </li>
                    @can('update-users', \App\Helpers\AuthHelper::user())
                    <li>
                        <a href="/admin/users/#= id #/edit"><i class="k-icon k-i-edit"></i>Edit</a>
                        <ul>
                            <li><a href="/admin/users/#= id #/edit">Edit user profile</a></li>
                            <li><a href onclick="changeUserEmail(#= id #, '#= email #', event)">Change user email address</a></li>
                            <li><a href onclick="resetUserPassword('#= email #', event)">Reset user password</a></li>
                            {{--<li><a href onclick="resendRegistrationEmail(#= id #, event)">Resend registration email</a></li>--}}
                        </ul>
                    </li>
                    @endcan
                    @can(\App\Models\Permission::CHANGE_USER_ACCOUNT_STATUSES)
                    <li>
                        <a href onclick="updateUser(#= id #, 'Active', event)">
                            <i class="k-icon k-i-check"></i>Activate
                        </a>
                    </li>
                    <li>
                        <a href onclick="updateUser(#= id #, 'Suspend', event)">
                            <i class="k-icon k-i-cancel-outline"></i>Suspend
                        </a>
                    </li>
                    @endcan
                    @can('delete-users', \App\Helpers\AuthHelper::user())
                    <li>
                        <a href onclick="updateUser(#= id #, 'Deleted', event)"><i class="k-icon k-i-close"></i>Delete</a>
                    </li>
                    @endcan
                </ul>
            </li>
        </ul>
    </script>

    <script src="{{asset('js/components/admin/users.js')}}"></script>
@stop

