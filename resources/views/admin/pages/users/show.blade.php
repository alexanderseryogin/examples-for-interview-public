@extends('adminlte::page')

@section('title', 'User Profile')

@section('content_header')

@stop

@section('content')
    <div class="content-header">
        <div class="content-header-inner-wrap">
            <div class="row">
                <div class="col-lg-8">
                    <h1>User Profile</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="main-content-wrap mt-20">
        <div class="content-box-wrap">
            <div class="content-box">
                <div class="box box-solid">
                    <div class="user-profile-wrap divided-block d-flex">
                        <div class="left-col d-flex">
                            <div class="avatar-wrap">
                                @if($account->avatar)
                                    <img src="{{ $account->avatarUrl }}" class="avatar" alt="{{ $account->full_name }}">
                                @else
                                    <img src="{{ asset('images/avatars/user-image.svg') }}" class="avatar" alt="avatar">
                                @endif
                            </div>
                            <div>
                                <div class="name">{{$account->full_name}}</div>
                                <div class="position">{{ $user->roles->implode('name', ',') }}</div>
                                <dl class="custom-dl">
                                    <dt>Account status:</dt>
                                    <dd><span class="round-status disactive {{strtolower($user->status)}}"></span>{{$user->status}}</dd>
                                    <dt>Last active:</dt>
                                    <dd>{{$user->last_active_at}}</dd>
                                </dl>
                            </div>
                        </div>
                        <div class="right-col">
                            <div class="info-title">User details</div>
                            <dl class="custom-dl">
                                <dt>Email:</dt>
                                <dd>{{ $user->email }}</dd>
                                @if($user->company)
                                    <dt>Company:</dt>
                                    <dd>{{ $user->company->name }}</dd>
                                @endif
                                @if($account->team)
                                    <dt>Team</dt>
                                    <dd>{{ $account->team->name }}</dd>
                                @endif
                            </dl>
                        </div>
                        <div class="positioned-top-btn">
                            @can('update-user', $user)
                                @if( $user->id === \App\Helpers\AuthHelper::id() )
                                    <a href="{{route('admin.users.profile.edit')}}" class="edit-btn">
                                        <i class="k-icon k-i-edit"></i>
                                    </a>
                                @else
                                    <a href="{{route('admin.users.edit', $user->id)}}" class="edit-btn">
                                        <i class="k-icon k-i-edit"></i>
                                    </a>
                                @endif
                            @endcan
                        </div>
                        <div class="positioned-bottom-btn">
                            @can('delete-user', $user)
                                <button class="btn btn-primary-blur btn-lg-small"
                                        onclick="deleteUserProfile({{$user->id}}, event)">
                                    Delete user
                                </button>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="box box-solid mt-20">
                    <div class="box-body">
                        <h2>Events Summary</h2>
                        <div class="btns-wrap">
                            <button class="btn btn-light-grey active btn-lg-small js-get-user-event-btn">Created by user</button>
                            <button class="btn btn-light-grey btn-lg-small js-get-company-event-btn">Associated with user</button>
                        </div>

                        {{-- User Events grid --}}
                        <div id="grid" class="grid-events no-scroll-x-table"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    @include('admin.modals.confirm-modal')
@endsection

@section('js')
    <script>
        var eventStatuses = @json($eventStatuses);
        var userId = @json($user->id);
        var companyId = @json($user->company_id);
    </script>

    <script src="{{asset('js/components/admin/users.js')}}"></script>
@stop

