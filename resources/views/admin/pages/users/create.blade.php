@extends('adminlte::page-noauth')

@section('title', 'Create your account')

@section('content_header')

@endsection


@section('content')
    <div class="account-page-wrap">
        <section class="section">
            <h1 class="auth-page-h1">Create your account</h1>
            <p>Please confirm your details & fill in the new information needed. </p>
        </section>

        <section class="section section-highlight">
            <h2 class="auth-page-h2">User details</h2>

            <dl class="custom-dl">
                 {{--Email--}}
                <dt>Email</dt>
                <dd>{{$invite->email}}</dd>

                 {{--Role--}}
                <dt>Role</dt>
                <dd>{{$invite->roleList}}</dd>

                 {{--Company--}}
                <dt>Company</dt>
                <dd>{{$invite->company->name}}</dd>

                 {{--Team--  Tempopary hide}}
                {{--<dt>Team</dt>
                <dd>{{optional($invite->team)->name ?? 'No team'}}</dd>--}}

                 {{--Event accsess--}}
                <dt>Event access</dt>
                @if ($invite->events->isNotEmpty())
                    @foreach($invite->events as $event)
                        <dd>{{$loop->iteration . '. ' . $event->name}}</dd>
                    @endforeach
                @else
                    <dd>All events created by users under your company</dd>
                @endif
            </dl>
        </section>
        <section class="section">
            <h2 class="auth-page-h2">Information needed</h2>

            {{ Form::model(
                $user,
                [
                    'method' => 'POST',
                    'url' => route('admin.users.store')
                ]
            ) }}
            @csrf

            @include('admin.pages.users.partials.form-account')

            {{ Form::close() }}
        </section>
    </div>
@endsection

@section('modal')

@endsection

@push('scripts')

@endpush

@section('js')

@stop
