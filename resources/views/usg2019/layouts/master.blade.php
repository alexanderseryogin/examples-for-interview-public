<!doctype html>
<html class="no-js" lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>RedTix - @yield('title')</title>
    <meta name="description" content="Ultra Singapore returns to Ultra Park for its 4th edition on Saturday, June 8th and Sunday, June 9th – 2019!">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    @include('usg2019.layouts.partials.meta')

    <link rel="stylesheet" href="/css/ultra2019.css?v=2019-03-05-2">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">

    @if (App::environment('production'))
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-W7WS5LM');
        </script>
        <!-- End Google Tag Manager -->

        <!-- Google Tag Manager Ultra Vayner -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-TV8VDXS');
        </script>
        <!-- End Google Tag Manager -->

        <!-- Google Tag Manager ADA Asia -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-56GR95T');
        </script>
        <!-- End Google Tag Manager -->

        <!-- Google Tag Manager ADA Asia 2 -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-KCWM3ZR');
        </script>
        <!-- End Google Tag Manager -->

        <!-- Start Heap Analytics -->
        <script type="text/javascript">
            window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
            heap.load("2662097889");
        </script>
        <!-- End Heap Analytics -->

        <!-- Google Tag Manager Ultra -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-N687ZV8');</script>
        <!-- End Google Tag Manager Ultra -->

        <!-- Global site tag (gtag.js) - Google Ultra Ads: 801130855 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-801130855"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-801130855');
        </script>

        <!-- Event snippet for tracker - Adwords Conversion Vayner-->
        <script>
            function gtag_report_conversion(url) {
                var callback = function () {
                    if (typeof(url) != 'undefined') {
                        window.location = url;
                    }
                };
                gtag('event', 'conversion', {
                    'send_to': 'AW-801130855/jOecCOub9YMBEOeSgf4C',
                    'value': 1.0,
                    'currency': 'USD',
                    'transaction_id': '',
                    'event_callback': callback
                });
                return false;
            }
        </script>

        <!-- Facebook Pixel Code Ultra - Vayner-->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '677816742600949'); 
        fbq('track', 'PageView');
        </script>
        <!-- End Facebook Pixel Code -->
        
    @endif
</head>
<body>

@if (App::environment('production'))
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W7WS5LM" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Google Tag Manager (noscript) Ultra Vayner -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TV8VDXS" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Google Tag Manager (noscript) ADA Asia-->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-56GR95T" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Google Analytics Cross Domain Tracking -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-114034753-1', 'auto', {allowLinker: true});

    // Loads the Linker plugin
    ga('require', 'linker');
    // Set domains for source and destination
    ga('linker:autoLink', ['airasiaredtix.com', 'redtix-tickets.airasia.com']);

    ga(function(tracker) {
      var linkerParam = tracker.get('linkerParam');
    });

    </script>
    <!-- End Google Analytics Cross Domain Tracking -->

    <!-- Facebook Pixel - Vayner -->
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=677816742600949&ev=PageView&noscript=1"/>
    </noscript>
@endif

<div class="layout">

    @yield('header')

    @yield('content')

    @yield('info-modal')

    @yield('sale-modal')

    @yield('newsletter-modal')

    @yield('footer')

</div>
<script src="{{asset('js/ultra-app.js')}}"></script>

@yield('customjs')

</body>
</html>