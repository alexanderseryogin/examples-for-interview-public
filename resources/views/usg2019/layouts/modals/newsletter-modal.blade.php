<div class="modal newsletter-modal" id="newsletter-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"><i class="icon icon-close"></i></button>
            <div class="modal-body text-center">
                {{-- <h3>Newsletter</h3>
                <p>Be the first to find out about exciting upcoming events<br>and exclusive offers!</p> --}}
                <form action="https://AirAsiaRedtix.us14.list-manage.com/subscribe/post?u=563fc1bcdefdc4fca4efb1dbe&id=56231f06ce" method="post"
                      id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <h3>Newsletter</h3>
                        <p>Be the first to find out about exciting upcoming events<br>and exclusive offers!</p>
                        <div class="row">
                            <div class="col-md-8">
                                <input type="email" value="" name="EMAIL" class="email form-control" id="mce-EMAIL" placeholder="email address"
                                       required="">
                                <input type="hidden" name="b_563fc1bcdefdc4fca4efb1dbe_56231f06ce" value="">
                            </div>
                            <div class="col-md-4">
                                <input type="submit" onclick="register_newsletter()" value="Subscribe" name="subscribe" id="mc-embedded-subscribe"
                                       class="button btn btn-danger btn-block">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
