<div class="modal info-modal" id="info-modal-pga">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"><i class="icon icon-close"></i></button>
            <div class="modal-header">
                <div class="image-block" style="background-image: url(/images/ultrasingapore2019/modal/modal-img1.jpg);"></div>
            </div>
            {{-- <div class="modal-body">
                <h3>PGA - WEEKEND TICKet</h3>
                <p>At <b>Ultra Singapore 2019</b>, there will be a brand new elevated and holistic PGA experience for our festival-goers. There is a limited quantity of PGA tickets available.</p>
                <h4 class="title">What You Will Get:</h4>
                <ol>
                    <li>Entry to the festival through a separate entrance</li>
                    <li>Expedited pick up for wristbands</li>
                    <li>Limited edition wristbands</li>
                    <li>Access to the PGA-only washrooms</li>
                    <li>Dedicated queues at all bars and top-up stations</li>
                    <li>An air-conditioned lounge and a shaded rest area</li>
                </ol>
                <h4 class="title">T&C:</h4>
                <ol>
                    <li>Booking fee SGD 3</li>
                    <li>Excludes Event Protect fee SGD 6</li>
                    <li>Strictly no replacement for missing tickets and cancellation</li>
                    <li>Online tickets selling will close 1 day prior to event day, subject to availability</li>
                    <li>Age limit 18+</li>
                </ol>
            </div> --}}

            {{-- <div class="modal-body">
                <h3>PGA 2-DAY COMBO TICKet</h3>
                <h4 class="title">Includes:</h4>
                <ul>
                    <li>Premium general admission entrance to the festival, including access to each stage and the VIP Village with dedicated bars & food stalls.</li>
                </ul>
                <h4 class="title">Important Information:</h4>
                <ol>
                    <li>Booking fee SGD 3.</li>
                    <li>Excludes Event Protect fee SGD 6</li>
                    <li>Must be 18 years of age or older in order to attend.</li>
                    <li>All prices are subject to availability.</li>
                    <li>All sales are final.</li>
                </ol>
            </div> --}}

            <div class="modal-body">
                <h3>PGA 2-DAY COMBO TICKET</h3>
                <h4 class="title">Includes:</h4>
                <ol>
                    <li>Entry to the festival through a separate entrance.</li>
                    <li>Expedited pick up for wristbands.</li>
                    <li>Limited edition wristbands.</li>
                    <li>Access to the PGA-only washrooms.</li>
                    <li>Dedicated queues at all bars and top-up stations.</li>
                </ol>
                <h4 class="title">Important Information:</h4>
                <ol>
                    <li>Booking fee SGD 3.</li>
                    <li>Excludes Event Protect fee SGD 6</li>
                    <li>Must be 18 years of age or older in order to attend.</li>
                    <li>All prices are subject to availability.</li>
                    <li>All sales are final.</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="modal info-modal" id="info-modal-pga-1-day">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"><i class="icon icon-close"></i></button>
            <div class="modal-header">
                <div class="image-block" style="background-image: url(/images/ultrasingapore2019/modal/modal-img1.jpg);"></div>
            </div>            
            <div class="modal-body">
                <h3>PGA 1-DAY TICKET</h3>
                <h4 class="title">Includes:</h4>
                <ol>
                    <li>Entry to the festival through a separate entrance.</li>
                    <li>Expedited pick up for wristbands.</li>
                    <li>Limited edition wristbands.</li>
                    <li>Access to the PGA-only washrooms.</li>
                    <li>Dedicated queues at all bars and top-up stations.</li>
                </ol>
                <h4 class="title">Important Information:</h4>
                <ol>
                    <li>Booking fee SGD 3.</li>
                    <li>Excludes Event Protect fee SGD 4</li>
                    <li>Must be 18 years of age or older in order to attend.</li>
                    <li>All prices are subject to availability.</li>
                    <li>All sales are final.</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="modal info-modal" id="info-modal-ga">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"><i class="icon icon-close"></i></button>
            <div class="modal-header">
                <div class="image-block" style="background-image: url(images/ultrasingapore2019/modal/modal-img2.jpg);"></div>
            </div>
            <div class="modal-body">
                <h3>GA 2-DAY COMBO TICKET</h3>
                <h4 class="title">Includes:</h4>
                <ul>
                    <li>General admission entrance to the festival for both days.</li>
                </ul>
                <h4 class="title">Important Information:</h4>
                <ol>
                    <li>Booking fee SGD 3.</li>
                    <li>Excludes Event Protect fee SGD 5</li>
                    <li>Must be 18 years of age or older in order to attend.</li>
                    <li>All prices are subject to availability.</li>
                    <li>All sales are final.</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="modal info-modal" id="info-modal-ga-1-day">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"><i class="icon icon-close"></i></button>
            <div class="modal-header">
                <div class="image-block" style="background-image: url(images/ultrasingapore2019/modal/modal-img2.jpg);"></div>
            </div>
            <div class="modal-body">
                <h3>GA 1-DAY TICKET</h3>
                <h4 class="title">Includes:</h4>
                <ul>
                    <li>General admission entrance to the festival for 1 selected day.</li>
                </ul>
                <h4 class="title">Important Information:</h4>
                <ol>
                    <li>Booking fee SGD 3.</li>
                    <li>Excludes Event Protect fee SGD 4</li>
                    <li>Must be 18 years of age or older in order to attend.</li>
                    <li>All prices are subject to availability.</li>
                    <li>All sales are final.</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="modal info-modal" id="info-modal-ga-passport">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"><i class="icon icon-close"></i></button>
            <div class="modal-header">
                <div class="image-block" style="background-image: url(images/ultrasingapore2019/modal/modal-img4.jpg);"></div>
            </div>
            <div class="modal-body">
                <h3>GA ULTRA PASSPORT PACKS</h3>
                <p>Pick your squad. Get your tickets. GO!</p>
                <h4 class="title">How Does It Work?</h4>
                <ol>
                    {{-- <li>Click this <a href="https://events.liveit.io/ultra-singapore-2019/ultra-singapore-2019-group/group/?scheme=799">link</a> to register as a Ultra Passport Holder</li> --}}
                    <li>Click this <a href="http://ultrasingapore.com/passport-pack">link</a> to register as a Ultra Passport Holder</li>
                    <li>Once 5 purchases are made, a 6th ticket will be unlocked!</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="modal info-modal" id="info-modal-pga-passport">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"><i class="icon icon-close"></i></button>
            <div class="modal-header">
                <div class="image-block" style="background-image: url(images/ultrasingapore2019/modal/modal-img5.jpg);"></div>
            </div>
            <div class="modal-body">
                <h3>PGA ULTRA PASSPORT PACKS</h3>
                <p>Pick your squad. Get your tickets. GO!</p>
                <h4 class="title">How Does It Work?</h4>
                <ol>
                    {{-- <li>Click this <a href="https://events.liveit.io/ultra-singapore-2019/ultra-singapore-2019-group/group/?scheme=800">link</a> to register as a Ultra Passport Holder</li> --}}
                    <li>Click this <a href="http://ultrasingapore.com/passport-pack-pga">link</a> to register as a Ultra Passport Holder</li>
                    <li>Once 5 purchases are made, a 6th ticket will be unlocked!</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="modal info-modal" id="info-modal-flight-hotel-package">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"><i class="icon icon-close"></i></button>
            <div class="modal-header">
                <picture>
                    <source media="(max-width: 370px)" srcset="/images/ultrasingapore2019/modal/modal-img10-mobile.jpg">
                    <source media="(min-width: 371px)" srcset="/images/ultrasingapore2019/modal/modal-img10.jpg">
                    <img src="/images/ultrasingapore2019/modal/modal-img10.jpg">
                </picture>
            </div>
            <div class="modal-body">
                <h3>FLIGHT + HOTEL PACKAGE</h3>
                <p>The full monty. Event ticket, selected return flight and hotel stay. We’ve got you covered with one convenient package that covers all bases for the seasoned raver.</p>
                <h4 class="title">Includes:</h4>
                <ul>
                    <li>PGA 2-day ticket</li>
                    <li>International return flight from Kuala Lumpur</li>
                    <li>4 Days 3 Nights stay at Hotel G Singapore, Singapore</li>
                </ul>
                <h4 class="title">Excludes:</h4>
                <ul>
                    <li>Booking/services fee SGD 3</li>
                    <li>Event Protect fee per ticket SGD 6</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="modal info-modal" id="info-modal-comfort-package">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"><i class="icon icon-close"></i></button>
            <div class="modal-header">
                <picture>
                    <source media="(max-width: 370px)" srcset="/images/ultrasingapore2019/modal/modal-img11-mobile.jpg">
                    <source media="(min-width: 371px)" srcset="/images/ultrasingapore2019/modal/modal-img11.jpg">
                    <img src="/images/ultrasingapore2019/modal/modal-img11.jpg">
                </picture>
            </div>
            <div class="modal-body">
                <h3>COMFORT PACKAGE</h3>
                <p>Sleep easy with your event ticket and hotel bundled into one convenient package. Clean, fresh and vibrant hotel only 10 minutes from Ultra festival.</p>
                <h4 class="title">Includes:</h4>
                <ul>
                    <li>PGA 2-day ticket</li>
                    <li>4 Days 3 Nights stay at Hotel G Singapore, Singapore</li>
                </ul>
                <h4 class="title">Excludes:</h4>
                <ul>
                    <li>Booking/services fee SGD 3</li>
                    <li>Event Protect fee per ticket SGD 6</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="modal info-modal" id="info-modal-flight-package">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"><i class="icon icon-close"></i></button>
            <div class="modal-header">
                <picture>
                    <source media="(max-width: 370px)" srcset="/images/ultrasingapore2019/modal/modal-img12-mobile.jpg">
                    <source media="(min-width: 371px)" srcset="/images/ultrasingapore2019/modal/modal-img12.jpg">
                    <img src="/images/ultrasingapore2019/modal/modal-img12.jpg">
                </picture>
            </div>
            <div class="modal-body">
                <h3>FLIGHT PACKAGE</h3>
                <p>Fly with AirAsia RedTix. Let RedTix get you to Singapore with our affordable selected flight and event ticket package.</p>
                <h4 class="title">Includes:</h4>
                <ul>
                    <li>PGA 2-day ticket</li>
                    <li>International return flight from Kuala Lumpur</li>
                </ul>
                <h4 class="title">Excludes:</h4>
                <ul>
                    <li>Booking/services fee SGD 3</li>
                    <li>Event Protect fee per ticket SGD 6</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="modal info-modal" id="info-modal-vvip">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"><i class="icon icon-close"></i></button>
            <div class="modal-header">
                <div class="image-block" style="background-image: url(images/ultrasingapore2019/modal/modal-img6.jpg);"></div>
            </div>
            <div class="modal-body">
                <h3>VVIP TABLE SERVICE</h3>
                <p>VVIP tables are available to those seeking greater prestige and service. It will be the ultimate luxury experience for our esteemed guests.</p>
                <h4 class="title">VVIP-Only Benefits Including: </h4>
                <ol>
                    <li>VVIP-only entrance & exit.</li>
                    <li>Re-entry to festival grounds.</li>
                    <li>Executive air-conditioned restrooms.</li>
                    <li>Breathtaking view of the main stage, and many more.</li>
                </ol>
                <h4 class="title text-center">Coming Soon</h4>
            </div>
        </div>
    </div>
</div>

<div class="modal info-modal" id="info-modal-party-flight-plus">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"><i class="icon icon-close"></i></button>
            <div class="modal-header">
                <div class="image-block" style="background-image: url(images/ultrasingapore2019/modal/modal-img7.jpg);"></div>
            </div>
            <div class="modal-body">
                <h3>THE PARTY FLIGHT + </h3>
                <h4 class="title">Coming Soon!</h4>
            </div>
        </div>
    </div>
</div>