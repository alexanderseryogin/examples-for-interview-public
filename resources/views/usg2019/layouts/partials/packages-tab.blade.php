<section class="packages-section">
    <div class="container">
        <div class="packages-section-holder">
            <h2 class="section-title">Travel Packages</h2>
            <div class="row">
                <div class="col-md-12 custom-col">
                    <div class="item{{$FlightHotelSoldout?'-soldout':''}} pga">
                        <div class="image-block">
                            <picture>
                                <source media="(max-width: 360px)" srcset="/images/ultrasingapore2019/packages/img10-mobile.jpg">
                                <source media="(min-width: 361px)" srcset="/images/ultrasingapore2019/packages/img10.jpg">
                                <img src="/images/ultrasingapore2019/packages/img10.jpg" alt="ultra-singapore">
                            </picture>

                            <div class="title-block">
                                <h3 class="title"><span>flight + hotel</span>package</h3>
                            </div>
                        </div>
                        <div class="content-holder">
                            <button type="button" class="info-button" data-toggle="modal" data-target="#info-modal-flight-hotel-package"><i class="icon icon-info"></i></button>
                            <ul class="info-list">
                                <li>
                                    <div class="icon-holder">
                                        <i class="icon icon-coupon"></i>
                                    </div>
                                    PGA | 2-Day ticket
                                </li>
                                <li>
                                    <div class="icon-holder">
                                        <i class="icon icon-plane"></i>
                                    </div>
                                    International Return Flight
                                </li>
                                <li>
                                    <div class="icon-holder">
                                        <span class="iconart iconart-hotel"></span>
                                    </div>
                                    4 Days 3 Nights stay at Singapore 
                                </li>
                            </ul>
                            <div class="content-bottom">
                                <div class="price-block">
                                    <div>
                                        <div class="fromstr desktop-hide">from</div>
                                        <div class="discount">SGD 903</div>
                                        <div class="price"><span class="fromstr mobile-hide">From </span> SGD 851</div>
                                    </div>
                                    <div><br><a href="{{ url('ultrasingapore2019/packages/flight-hotel') }}" class="btn btn-primary btn-select">Select</a>
                                        <span class="soldout-label">SOLD OUT</span>
                                    </div>
                                </div>
                                {{--<ul class="price-block">
                                    <li><span class="fromstr">From</span><br>
                                        <span class="price">SGD 804</span></li>
                                    <li><br><a href="{{ url('ultrasingapore2019/packages/flight-hotel') }}" class="btn btn-primary">Select</a>
                                        <span class="soldout-label">SOLD OUT</span>
                                    </li>
                                </ul>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 custom-col">
                    <div class="item{{$ComfortSoldout?'-soldout':''}} ga">
                        <div class="image-block">
                            <img src="/images/ultrasingapore2019/packages/img11.jpg" alt="ultra-singapore">
                            <div class="title-block">
                                <h3 class="title">Comfort<br>Package</h3>
                            </div>
                        </div>
                        <div class="content-holder">
                            <button type="button" class="info-button" data-toggle="modal" data-target="#info-modal-comfort-package"><i class="icon icon-info"></i></button>
                            <ul class="info-list">
                                <li>
                                    <div class="icon-holder">
                                        <i class="icon icon-coupon"></i>
                                    </div>
                                    PGA | 2-Day ticket
                                </li>
                                <li>
                                    <div class="icon-holder">
                                        <span class="iconart iconart-hotel"></span>
                                    </div>
                                    4 Days 3 Nights stay at Singapore 
                                </li>
                            </ul>
                            <div class="content-bottom">
                                <div class="price-block">
                                    <div>
                                        <div class="fromstr desktop-hide">from</div>
                                        <div class="discount">SGD 712</div>
                                        <div class="price"><span class="fromstr mobile-hide">From </span> SGD 667</div>
                                    </div>
                                    <div><br><a href="{{ url('ultrasingapore2019/packages/hotel') }}" class="btn btn-primary btn-select">Select</a>
                                        <div class="soldout-label">SOLD OUT</div>
                                    </div>
                                </div>

                                {{--<ul class="price-block">
                                    <li><span class="fromstr">From</span><br>
                                        <span class="price">SGD 687</span></li>
                                    <li><br><a href="{{ url('ultrasingapore2019/packages/hotel') }}" class="btn btn-primary">Select</a>
                                        <span class="soldout-label">SOLD OUT</span>
                                    </li>
                                </ul>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 custom-col">
                    <div class="item{{$FlightSoldout?'-soldout':''}} pga">
                        <div class="image-block">
                            <picture>
                                <source media="(max-width: 360px)" srcset="/images/ultrasingapore2019/packages/img12-mobile.jpg">
                                <source media="(min-width: 361px)" srcset="/images/ultrasingapore2019/packages/img12.jpg">
                                <img src="/images/ultrasingapore2019/packages/img12.jpg" alt="ultra-singapore">
                            </picture>
                            <div class="title-block">
                                <h3 class="title">Flight<br>Package</h3>
                            </div>
                        </div>
                        <div class="content-holder">
                            <button type="button" class="info-button" data-toggle="modal" data-target="#info-modal-flight-package"><i class="icon icon-info"></i></button>
                            <ul class="info-list">
                                <li>
                                    <div class="icon-holder">
                                        <i class="icon icon-coupon"></i>
                                    </div>
                                    PGA | 2-Day ticket
                                </li>
                                <li>
                                    <div class="icon-holder">
                                        <i class="icon icon-plane"></i>
                                    </div>
                                    International Return Flight
                                </li>
                            </ul>
                            <div class="content-bottom">
                                <div class="price-block">
                                    <div>
                                        <div class="fromstr desktop-hide">from</div>
                                        <div class="discount">SGD 465</div>
                                        <div class="price"><span class="fromstr mobile-hide">From </span> SGD 438</div>
                                    </div>
                                    <div><br><a href="{{ url('ultrasingapore2019/packages/flight') }}" class="btn btn-primary btn-select">Select</a>
                                        <div class="soldout-label">SOLD OUT</div>
                                    </div>
                                </div>
                                {{--<ul class="price-block">
                                    <li><span class="fromstr">From</span><br>
                                        <span class="price">SGD 465</span></li>
                                    <li><br><a href="{{ url('ultrasingapore2019/packages/flight') }}" class="btn btn-primary">Select</a>
                                        <span class="soldout-label">SOLD OUT</span>
                                    </li>
                                </ul>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    {{--        <div class="line-separate"></div>
            <div class="item vip">
                <div class="image-block">
                    <img src="images/ultrasingapore2019/packages/img2.jpg" alt="">
                    <div class="title-block">
                        <h3 class="title"><span>VVIP</span>Table<br>Service</h3>
                    </div>
                </div>
                <div class="content-holder">
                    <button type="button" class="info-button" data-toggle="modal" data-target="#info-modal-vvip"><i class="icon icon-info"></i></button>
                    <ul class="info-list">
                        <li>
                            <div class="icon-holder">
                                <i class="icon icon-coupon"></i>
                            </div>
                            10 VVIP Festival tickets
                        </li>
                        <li>
                            <div class="icon-holder">
                                <i class="icon icon-disk"></i>
                            </div>
                            Exclusive perks
                        </li>
                        <li>
                            <div class="icon-holder">
                                <i class="icon icon-shop"></i>
                            </div>
                            Drinks package
                        </li>
                    </ul>
                    <div class="content-bottom">
                        <span class="coming-soon">Coming Soon</span>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image-block">
                    <img src="images/ultrasingapore2019/packages/img3.jpg" alt="">
                    <div class="title-block">
                        <h3 class="title"><span>The</span>Party FLight +</h3>
                    </div>
                </div>
                <div class="content-holder">
                    <button type="button" class="info-button" data-toggle="modal" data-target="#info-modal-party-flight-plus"><i class="icon icon-info"></i></button>
                    <ul class="info-list">
                        <li>
                            <div class="icon-holder">
                                <i class="icon icon-coupon"></i>
                            </div>
                            Festival ticket
                        </li>
                        <li>
                            <div class="icon-holder">
                                <i class="icon icon-disk"></i>
                            </div>
                            Malaysia party flight
                        </li>
                        <li>
                            <div class="icon-holder">
                                <i class="icon icon-build"></i>
                            </div>
                            4 Days 3 Nights Hotel stay at Singapore
                        </li>
                    </ul>
                    <div class="content-bottom">
                        <span class="coming-soon">Coming Soon</span>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
</section>