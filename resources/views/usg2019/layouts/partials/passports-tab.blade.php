<section class="packages-section">
    <div class="container">
        <div class="packages-section-holder">
            <h2 class="section-title">Ultra Passport Packs</h2>
            <div class="row">
                <div class="col-md-8 custom-col">
                    <div class="item pga">
                        <div class="image-block">
                            <img src="/images/ultrasingapore2019/packages/img9.jpg" alt="">
                            <div class="title-block">
                                <h3 class="title"><span>PGA</span>Ultra passport<br>packs</h3>
                            </div>
                            <img src="/images/ultrasingapore2019/singtel.png" alt="" class="package-logo">
                        </div>
                        <div class="content-holder">
                            <button type="button" class="info-button" data-toggle="modal" data-target="#info-modal-pga-passport"><i class="icon icon-info"></i></button>
                            <ul class="info-list">
                                <li>
                                    <div class="icon-holder">
                                        <i class="icon icon-coupon"></i>
                                    </div>
                                    5 + 1 PGA 2-day tickets
                                </li>
                            </ul>
                            <div class="content-bottom price-block">
                                <a href="http://ultrasingapore.com/passport-pack-pga" class="btn btn-primary">Select</a>
                                <span class="soldout-label">SOLD OUT</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 custom-col">
                    <div class="item ga">
                        <div class="image-block">
                            <img src="/images/ultrasingapore2019/packages/img1.jpg" alt="">
                            <div class="title-block">
                                <h3 class="title"><span>Ga</span>Ultra passport<br>packs</h3>
                            </div>
                            <img src="/images/ultrasingapore2019/singtel.png" alt="" class="package-logo">
                        </div>
                        <div class="content-holder">
                            <button type="button" class="info-button" data-toggle="modal" data-target="#info-modal-ga-passport"><i class="icon icon-info"></i></button>
                            <ul class="info-list">
                                <li>
                                    <div class="icon-holder">
                                        <i class="icon icon-coupon"></i>
                                    </div>
                                    5 + 1 GA 2-day tickets
                                </li>
                            </ul>
                            <div class="content-bottom price-block">
                                <a href="http://ultrasingapore.com/passport-pack" class="btn btn-primary">Select</a>
                                <span class="soldout-label">SOLD OUT</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>