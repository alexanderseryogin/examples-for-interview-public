<header id="header">
    <div class="container">
        <div class="logo">
            <a href="/">
                <img src="/images/ultrasingapore2019/logo.svg" alt="Red Tix">
            </a>
        </div>
        <button type="button" class="j-toggle-nav burger-btn"><span></span></button>
        <div class="main-menu-holder">
            <nav class="main-menu">
                <div class="drop">
                    <ul>
                        <li><a href="https://lifestylehub.airasiaredtix.com">LIFESTYLE HUB</a></li>
                        <li><a href="https://airasiaredtix.com/about">ABOUT us</a></li>
                        <li><a href="https://airasiaredtix.com/faq">FAQ</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#newsletter-modal">NEWSLETTER</a></li>
                        <li><a href="mailto:support@airasiaredtix.com">CONTACT US</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>