<link rel="apple-touch-icon" sizes="57x57" href="/images/ultrasingapore2019/favicons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/images/ultrasingapore2019/favicons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/images/ultrasingapore2019/favicons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/images/ultrasingapore2019/favicons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/images/ultrasingapore2019/favicons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/images/ultrasingapore2019/favicons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/images/ultrasingapore2019/favicons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/images/ultrasingapore2019/favicons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/images/ultrasingapore2019/favicons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/images/ultrasingapore2019/favicons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/ultrasingapore2019/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/images/ultrasingapore2019/favicons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/ultrasingapore2019/favicons/favicon-16x16.png">
<link rel="manifest" href="/images/ultrasingapore2019/favicons/manifest.json">
<meta property="og:url" content="{{ Request::fullUrl() }}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Ultra Singapore 2019" />
<meta property="og:description" content="Ultra Singapore returns to Ultra Park for its 4th edition on Saturday, June 8th and Sunday, June 9th – 2019!"/>
<meta property="og:image" content="{{ Request::Url().'images/usg2019/thumbnail.jpg' }}" />
<meta name="msapplication-TileImage" content="/images/ultrasingapore2019/favicons/ms-icon-144x144.png">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">
