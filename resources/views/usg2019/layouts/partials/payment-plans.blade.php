<div class="container">
   <section class="payment-plans-section">
       <h4 class="title">Payment Plans</h4>
       <p>Take advantage of GA and PGA Payment Plans with 4 easy payments.</p>
       <b class="list-title">General Admission Payment Schedule</b>
       <ol>
           <li>Initial Deposit - <b>SGD 74.50</b></li>
           <li>2nd Payment  - <b>SGD 74.50</b></li>
           <li>3rd Payment - <b>SGD 74.50</b></li>
           <li>4th Payment - <b>SGD 74.50</b></li>
       </ol>
       <b class="list-title">Premium General Admission Payment Schedule</b>
       <ol>
           <li>Initial Deposit - <b>SGD 82.00</b></li>
           <li>2nd Payment  - <b>SGD 82.00</b></li>
           <li>3rd Payment - <b>SGD 82.00</b></li>
           <li>4th Payment - <b>SGD 82.00</b></li>
       </ol>
       <p>Following the initial deposit, each additional payment will be due on the 5th day of the month.</p>
       <div class="btn-holder">
           <a href="https://events.liveit.io/ultra-singapore-2019/ultra-singapore-2019-payment-plan-ga/" class="btn btn-primary btn-lg">Buy GA Tickets</a>
           <a href="https://events.liveit.io/ultra-singapore-2019/ultra-singapore-2019-payment-plan-pga/" class="btn btn-primary btn-lg">Buy PGA Tickets</a>
       </div>
   </section>
</div>