<section class="faq-section">
    <div class="container">
        <div id="accordion" class="faq-accordion" role="tablist">
            <div class="card">
                <div class="card-header" role="tab">
                    <a data-toggle="collapse" href="#collapse8" aria-expanded="true" aria-controls="collapse8" class="collapsed">
                        Prohibited Items
                    </a>
                </div>
                <div id="collapse8" class="collapse" role="tabpanel" data-parent="#accordion">
                    <div class="card-body">
                        <ul class="custom-list">
                            <li>No illegal or illicit substances, drugs, or drug paraphernalia</li>
                            <li>No weapons of any kind including, but not limited to, pocket knives and self defense sprays</li>
                            <li>No flammable items or liquids including aerosols and fireworks</li>
                            <li>No markers, pens or spray paint</li>
                            <li>No backpacks, purses, or bags allowed except bags made of clear plastic, clear vinyl, or clear PVC not exceeding 13”x17”</li>
                            <li>No stuffed animals</li>
                            <li>No facial masks</li>
                            <li>No outside food or beverage including alcohol</li>
                            <li>No bottles, cans, canteens, flasks, or coolers</li>
                            <li>No opened over-the-counter medication or eye drops</li>
                            <li>No opened packs of cigarettes or tampons</li>
                            <li>No pacifiers or glow sticks</li>
                            <li>No balloons, balls, inflatable balls, frisbees or flying disks</li>
                            <li>No umbrellas, chairs, blankets, sleeping bags or tents</li>
                            <li>No bicycles, skateboards, hoverboards, scooters, or personal motorized vehicles</li>
                            <li>No drones or unmanned aerial vehicles</li>
                            <li>No animals except for service animals assisting an individual with a disability. “Comfort,” “therapy” or “emotional support” animals do not meet the definition of a service animal and are not permitted entry to the Event.</li>
                            <li>No large chains, chained wallets or spiked jewelry</li>
                            <li>No laser pens, laser pointers, or similar focused light devices</li>
                            <li>No water guns, squirt guns, spray bottles or misters</li>
                            <li>No musical instruments, noisemakers or air horns</li>
                            <li>No state / country flags</li>
                            <li>No professional cameras, flash cameras or video and audio recording equipment, or camera poles</li>
                            <li>No poles, sticks, or “totems”</li>
                            <li>Additional items may be prohibited at the discretion of law enforcement or security officials.</li>
                        </ul>
                        <p class="title">ACCEPTABLE:</p>
                        <ul class="custom-list">
                            <li>Yes to hydration packs that are not backpacks (empty upon entry)</li>
                            <li>Yes to bags made of clear plastic, clear vinyl, or clear PVC (not exceeding 13”x17”)</li>
                            <li>Yes to one-gallon clear, plastic zip-top bag</li>
                            <li>Yes to fanny packs</li>
                            <li>Yes to small clutch bags, approximately the size of a hand, with or without a handle or strap is allowed</li>
                            <li>Yes to cell phones</li>
                            <li>Yes to sunglasses and hats</li>
                            <li>Yes to illuminated or glowing jewelry or costumes</li>
                            <li>Yes to sealed packs of cigarettes upon entry</li>
                            <li>Yes to non-professional cameras</li>
                            <li>Yes to action cameras, such as GoPro’s ( Regarding action cameras – yes to strap headwear or non-extendable handles )</li>
                            <li>Yes to earplugs</li>
                            <li>Yes to powdered make-up</li>
                            <li>Yes to feminine products (sealed upon entry)</li>
                            <li>Yes to prescription medication (you must have the prescription/label in your name with you)</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab">
                    <a data-toggle="collapse" href="#collapse9" aria-expanded="true" aria-controls="collapse9" class="collapsed">
                        Forms of Identification
                    </a>
                </div>
                <div id="collapse9" class="collapse" role="tabpanel" data-parent="#accordion">
                    <div class="card-body">
                        <p><b>Age Requirement: <i>18+ for entrance, 18+ to purchase alcohol, ID required.</i></b></p>
                        <p><b>ACCEPTABLE FORMS OF IDENTIFICATION FOR ENTRY FOR SINGAPORE RESIDENTS:</b></p>
                        <ul class="custom-list">
                            <li><i>Valid Singapore Government-issued identification card or driver’s license.</i></li>
                        </ul>
                        <p><b>ACCEPTABLE FORMS OF IDENTIFICATION FOR ENTRY FOR NON-RESIDENTS:</b></p>
                        <ul class="custom-list">
                            <li><i>Valid foreign government-issued passport (must have photo and date of birth).</i></li>
                            <li><i>Valid foreign government-issued identification card or drivers license (must have photo and date of birth) along with a photocopy of a valid passport from the same country as the identification card or drivers license provided</i></li>
                        </ul>
                        <p><b>UNACCEPTABLE FORMS OF IDENTIFICATION FOR ENTRY:</b></p>
                        <ul class="custom-list">
                            <li><i>School identifications</i></li>
                            <li><i>Expired identifications of any kind</i></li>
                            <li><i>Birth Certificates</i></li>
                            <li><i>Social Security Cards</i></li>
                            <li><i>Voter Card</i></li>
                            <li><i>Photo copies of any identifications</i></li>
                            <li><i>Consular identification</i></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab">
                    <a data-toggle="collapse" href="#collapse10" aria-expanded="true" aria-controls="collapse10" class="collapsed">
                        Clear Bag Policy
                    </a>
                </div>
                <div id="collapse10" class="collapse" role="tabpanel" data-parent="#accordion">
                    <div class="card-body">
                        <p>In order to expedite entry into the Event and to continue to promote the safety of employees, crew, festival goers and others, there shall be a limit on the size and type of bag that may be brought into the venue.</p>
                        <p>Although festival goers are encouraged not to bring bags to the Event, the items outlined below are permissible:</p>
                        <ul class="custom-list">
                            <li><i>Bags made of clear plastic, clear vinyl or clear PVC, which do not exceed 13”x 17” in size.</i></li>
                            <li><i>One-gallon clear zip-top bag.</i></li>
                            <li><i>Small clutch bags, approximately the size of a hand either with or without a handle or strap is allowed.</i></li>
                            <li><i>Hydration packs, which are NOT backpacks, will be allowed.</i></li>
                            <li><i>“Fanny-pack(s)” or similar waist-packs will be allowed.</i></li>
                            <li><i>Exceptions will be made on a case-by-case basis for medically required items after proper. inspection</i></li>
                        </ul>
                        <p class="danger"><i>Please note, we are not in the position to make any final/definitive rulings on bags. All we can do is provide advice, but ultimately all final decisions will be made by security at the gates.</i></p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab">
                    <a data-toggle="collapse" href="#collapse11" aria-expanded="true" aria-controls="collapse11" class="collapsed">
                        What is included with Event Protect
                    </a>
                </div>
                <div id="collapse11" class="collapse" role="tabpanel" data-parent="#accordion">
                    <div class="card-body">
                        <ul class="custom-list">
                            <li><i>Adverse weather conditions for indoor events.</i></li>
                            <li><i>Adverse weather conditions for outdoor events.</i></li>
                            <li><i>Cover for transport delays.</i></li>
                            <li><i>Key supplier failure.</i></li>
                            <li><i>Any major incident deemed a health & safety risk.</i></li>
                            <li><i>Mechanical failure or breakdown of key equipment</i></li>
                            <li><i>Unknown works being carried out by builders or contractors at the venue which renders it or its facilities unusable.</i></li>
                            <li><i>Cancellation or revocation of Licence or closure of event for health & safety by local authority.</i></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>