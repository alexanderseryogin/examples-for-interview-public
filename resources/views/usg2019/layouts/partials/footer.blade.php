<footer id="footer">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-6 col-lg-5 d-flex">
                <div class="left-column">
                    <div class="content-holder">
                        <div class="top-block">
                            <div class="row">
                                <div class="col-md-7 custom-col">
                                    <div class="logo">
                                        <a href="/">
                                            <img src="/images/ultrasingapore2019/logo.svg" alt="Red Tix">
                                        </a>
                                    </div>
                                    <div class="info d-none d-md-block">
                                        <p>AirAsiaRedTix.com is the hottest, <br>
                                            smartest new way to discover, discuss, review <br>
                                            and book tickets to an international line-up <br>
                                            of concerts, sporting events, musicals, <br>
                                            theatre performances and more.</p>
                                    </div>
                                </div>
                                <div class="col-md-5 custom-col">
                                    <nav class="menu menu-primary">
                                        <ul>
                                            <li class="active"><a href="/">Home</a></li>
                                            <li><a href="https://lifestylehub.airasiaredtix.com">LIFESTYLE HUB</a></li>
                                            <li><a href="https://airasiaredtix.com/about">ABOUT US</a></li>
                                            <li><a href="https://airasiaredtix.com/faq">FAQ</a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#newsletter-modal">NEWSLETTER</a></li>
                                            <li><a href="mailto:support@airasiaredtix.com">CONTACT US</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7 custom-col">
                                <div class="social-holder">
                                    <span class="title">FOLLOW</span>
                                    <ul class="social">
                                        <li>
                                            <a href="https://www.instagram.com/AirAsiaRedTix/"><i class="icon icon-instagram"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.youtube.com/channel/UCGs2OxKX4WQzmDGuwwNd9-g"><i class="icon icon-youtube"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/redtix"><i class="icon icon-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/RedTix"><i class="icon icon-facebook"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-5 custom-col">
                                <nav class="menu menu-secondary">
                                    <ul>
                                        <li><a href="/websiteterms">Website Terms & Conditions</a></li>
                                        <li><a href="/eventregistration">Register Your Details</a></li>
                                        <li><a href="/privacy">Privacy</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <p class="copy d-none d-md-block">Redtix Sdn. Bhd., a subsidiary of AirAsia Berhad. ©2007-2018 All rights reserved.</p>
                </div>
            </div>
            <div class="col-md-6 d-flex justify-content-between">
                <div class="image-block">
                    <img src="/images/ultrasingapore2019/footer-img.png" alt="Image" width="400">
                </div>
                <div class="info-holder d-block d-md-none">
                    <div class="info">
                        <p>AirAsiaRedTix.com is the hottest,
                            smartest new way to discover, discuss, review
                            and book tickets to an international line-up
                            of concerts, sporting events, musicals,
                            theatre performances and more.
                        </p>
                    </div>
                    <p class="copy d-block d-md-none">Redtix Sdn. Bhd., a subsidiary of AirAsia Berhad. ©2007-2018 All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>