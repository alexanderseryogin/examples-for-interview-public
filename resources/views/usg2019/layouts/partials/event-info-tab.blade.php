<div class="event-info-tab">
    <div class="container">
        <section class="info-section">
            <div class="section-title">
                <h4 class="title">ULTRA SINGAPORE 2019</h4>
                <span class="number">01</span>
            </div>
            <ul>
                <li>8<sup>th</sup> - 9<sup>th</sup> June 2019</li>
                <li>Ultra Park - One Bayfront Avenue, - <a href="https://www.google.com/maps/place/Singapore+Ultra+Park/@1.2816365,103.8576372,17z/data=!3m1!4b1!4m5!3m4!1s0x31da1904fbceae3f:0x822f11e9050f4767!8m2!3d1.2816365!4d103.8598259?shorturl=1" target="_blank">View Map</a></li>
                <li>Singapore</li>
            </ul>
            <p>Ultra Singapore returns to Ultra Park for its 4th edition on Saturday, June 8th and Sunday, June 9th – 2019!</p>
        </section>
        <section class="artists-section">
            <div class="header">PHASE 1 ANNOUNCEMENT</div>
            <div class="hr-sect">
                <span class="title">HEADLINERS</span>
            </div>
            <img class="headliners" src="/images/ultrasingapore2019/artists/Ultra_Singapore_Artist_Announcement-Martin-Garrix.jpg">
            <img class="headliners" src="/images/ultrasingapore2019/artists/Ultra_Singapore_Artist_Announcement-Porter-Robinson.jpg">
            <img class="headliners" src="/images/ultrasingapore2019/artists/Ultra_Singapore_Artist_Announcement-Skrillex.jpg">
            <div class="hr-sect">
                <span class="title2">RESISTANCE</span>
            </div>
            <img class="resistance" src="/images/ultrasingapore2019/artists/Ultra_Singapore_Artist_Announcement-Jamie-Jones.jpg">
            <img class="resistance" src="/images/ultrasingapore2019/artists/Ultra_Singapore_Artist_Announcement-Art-Department.jpg">
            <img class="resistance" src="/images/ultrasingapore2019/artists/Ultra_Singapore_Artist_Announcement-Eats-Everything.jpg">
            <img class="resistance" src="/images/ultrasingapore2019/artists/Ultra_Singapore_Artist_Announcement-Josh-Wink.jpg">
        </section>
        <section class="video-section">
            <div class="section-title">
                <h4 class="title">ULTRA SINGAPORE 2018 AFTERMOVIE</h4>
                <span class="number">02</span>
            </div>
            <div class="video">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/Ox1zn3WS4mg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </section>
    </div>
    <section class="gallery-section">
        <div class="container">
            <div class="section-title">
                <h4 class="title">ULTRA SINGAPORE GALLERY</h4>
                <span class="number">03</span>
            </div>
        </div>
        <div class="gallery">
            <a data-fancybox="gallery" href="/images/ultrasingapore2019/gallery/img1-xl.jpg" class="item"><img src="/images/ultrasingapore2019/gallery/img1.jpg"></a>
            <a data-fancybox="gallery" href="/images/ultrasingapore2019/gallery/img2-xl.jpg" class="item"><img src="/images/ultrasingapore2019/gallery/img2.jpg"></a>
            <a data-fancybox="gallery" href="/images/ultrasingapore2019/gallery/img3-xl.jpg" class="item"><img src="/images/ultrasingapore2019/gallery/img3.jpg"></a>
            <a data-fancybox="gallery" href="/images/ultrasingapore2019/gallery/img4-xl.jpg" class="item"><img src="/images/ultrasingapore2019/gallery/img4.jpg"></a>
            <a data-fancybox="gallery" href="/images/ultrasingapore2019/gallery/img5-xl.jpg" class="item"><img src="/images/ultrasingapore2019/gallery/img5.jpg"></a>
            <a data-fancybox="gallery" href="/images/ultrasingapore2019/gallery/img6-xl.jpg" class="item"><img src="/images/ultrasingapore2019/gallery/img6.jpg"></a>
            <a data-fancybox="gallery" href="/images/ultrasingapore2019/gallery/img7-xl.jpg" class="item"><img src="/images/ultrasingapore2019/gallery/img7.jpg"></a>
        </div>
    </section>
</div>