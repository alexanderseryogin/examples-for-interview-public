<div class="container">
    <div class="description-block">
        <a href="#cta-get-tickets-anchor" id="cta-get-tickets-anchor"></a>
        <ul class="custom-list">
           {{-- <li>GA and PGA Tier 1 & 2 tickets are available exclusively to those who registered. Registration does not guarantee availability to these ticket tiers. </li>
           <li>GA and PGA Tier 3 tickets are available to everyone.</li>
           <li>GA Tier 1 and 2 tickets are limited to 2 tickets per order. PGA Tier 1 and 2 tickets are limited to 2 tickets per order. GA and PGA Tier 3 tickets are limited to 4 tickets per order. </li> --}}
           <li>GA and PGA Payment Plan tickets are limited to 2 tickets per order. </li>
           <li>GA and PGA tickets are limited to 4 tickets per order.</li>
           {{-- <li>There is an overall limit of 4 tickets per order.</li> --}}
           <li>Credit cards from Visa and MasterCard are the only form of payment accepted.</li>
           <li>Must be 18 years of age or older in order to attend. </li>
           <li>All prices are subject to availability.</li>
           <li>All sales are final.</li>
       </ul>

    </div>

    <section class="ticket-section">
        <div class="section-title-block">
            
            <h2 class="section-title">General Admission</h2>
        </div>
        <div class="items-holder">

            <div class="item">
                <div class="category-block">
                    <div class="text-holder">
                        <span class="title">TIER 3</span>
                    </div>
                </div>
                <div class="title-block">
                    <div class="title-block-holder">
                        <h3 class="title">2-DAY TICKET</h3>
                    </div>
                </div>
                <div class="price-block">
                    <div class="price-block-holder">
                        <span class="price">SGD 238</span>
                        <div class="btn-holder">
                            <a href="https://tickets.airasiaredtix.com/airasia-redtix/ultrasingapore2019/booking" class="btn btn-primary" onClick="gtag_report_conversion();">Buy Ticket</a>
                        </div>
                        <span class="soldout-label">SOLD OUT</span>
                    </div>
                </div>
                <button type="button" class="info-botton" data-toggle="modal" data-target="#info-modal-ga"><i class="icon icon-info"></i></button>
            </div>            

            <div class="item">
                <div class="category-block">
                    <div class="text-holder">
                        <span class="title">TIER 3</span>
                    </div>
                </div>
                <div class="title-block">
                    <div class="title-block-holder">
                        <span class="subtitle">Sunday</span>
                        <h3 class="title">1-DAY TICKET</h3>
                    </div>
                </div>
                <div class="price-block">
                    <div class="price-block-holder">
                        <span class="price">SGD 158</span>
                        <div class="btn-holder">
                            <a href="https://tickets.airasiaredtix.com/airasia-redtix/ultrasingapore2019/booking" class="btn btn-primary" onClick="gtag_report_conversion();">Buy Ticket</a>
                        </div>
                        <span class="soldout-label">SOLD OUT</span>
                    </div>
                </div>
                <button type="button" class="info-botton" data-toggle="modal" data-target="#info-modal-ga-1-day"><i class="icon icon-info"></i></button>
            </div>

            <div class="item-soldout">
                <div class="category-block">
                    <div class="text-holder">
                        <span class="title">TIER 3</span>
                    </div>
                </div>
                <div class="title-block">
                    <div class="title-block-holder">
                        <span class="subtitle">Saturday</span>
                        <h3 class="title">1-DAY TICKET</h3>
                    </div>
                </div>
                <div class="price-block">
                    <div class="price-block-holder">
                        <span class="price">SGD 158</span>
                        <div class="btn-holder">
                            <a href="https://tickets.airasiaredtix.com/airasia-redtix/ultrasingapore2019/booking" class="btn btn-primary" onClick="gtag_report_conversion();">Buy Ticket</a>
                        </div>
                        <span class="soldout-label">SOLD OUT</span>
                    </div>
                </div>
                <button type="button" class="info-botton" data-toggle="modal" data-target="#info-modal-ga-1-day"><i class="icon icon-info"></i></button>
            </div>

            <div class="item-soldout">
                <div class="category-block">
                    <div class="text-holder">
                        <span class="title">TIER 2</span>
                    </div>
                </div>
                <div class="title-block">
                    <div class="title-block-holder">
                        <span class="subtitle">By registration only</span>
                        <h3 class="title">2-DAY TICKET</h3>
                    </div>
                </div>
                <div class="price-block">
                    <div class="price-block-holder">
                        <span class="price">SGD 218</span>
                        <buy-button type="ga-t2"></buy-button>
                        <span class="soldout-label">SOLD OUT</span>
                    </div>
                </div>
                <button type="button" class="info-botton" data-toggle="modal" data-target="#info-modal-ga"><i class="icon icon-info"></i></button>
            </div>

            <div class="item-soldout">
                <div class="category-block">
                    <div class="text-holder">
                        <span class="title">TIER 1</span>
                    </div>
                </div>
                <div class="title-block">
                    <div class="title-block-holder">
                        <span class="subtitle">By registration only</span>
                        <h3 class="title">2-DAY TICKET</h3>
                    </div>
                </div>
                <div class="price-block">
                    <div class="price-block-holder">
                        <span class="price">SGD 198</span>
                        <buy-button type="ga-t1"></buy-button>
                        <span class="soldout-label">SOLD OUT</span>
                    </div>
                </div>
                <button type="button" class="info-botton" data-toggle="modal" data-target="#info-modal-ga"><i class="icon icon-info"></i></button>
            </div>
            
        </div>
        <div class="line-separate"></div>
        <div class="section-title-block">
            <h2 class="section-title">Premium General Admission</h2>
        </div>
        <div class="items-holder">
            
            <div class="item premium">
                <div class="category-block">
                    <div class="text-holder">
                        <span class="title">TIER 3</span>
                    </div>
                </div>
                <div class="title-block">
                    <div class="title-block-holder">
                        <h3 class="title">2-DAY TICKET</h3>
                    </div>
                </div>
                <div class="price-block">
                    <div class="price-block-holder">
                        <span class="price">SGD 268</span>
                        <div class="btn-holder">
                            {{-- <a href="https://redtix-tickets.airasia.com/sgp/en-AU/shows/ultra singapore 2019 pga 2-day ticket (sgd t3)/events" class="btn btn-primary" onClick="gtag_report_conversion();">Buy Ticket</a> --}}
                            <a href="https://tickets.airasiaredtix.com/airasia-redtix/ultrasingapore2019/booking" class="btn btn-primary" onClick="gtag_report_conversion();">Buy Ticket</a>
                        </div>
                        <span class="soldout-label">SOLD OUT</span>
                    </div>
                </div>
                <button type="button" class="info-botton" data-toggle="modal" data-target="#info-modal-pga"><i class="icon icon-info"></i></button>
            </div>            

            <div class="item premium">
                <div class="category-block">
                    <div class="text-holder">
                        <span class="title">TIER 3</span>
                    </div>
                </div>
                <div class="title-block">
                    <div class="title-block-holder">
                        <span class="subtitle">Sunday</span>
                        <h3 class="title">1-DAY TICKET</h3>
                    </div>
                </div>
                <div class="price-block">
                    <div class="price-block-holder">
                        <span class="price">SGD 178</span>
                        <div class="btn-holder">                            
                            <a href="https://tickets.airasiaredtix.com/airasia-redtix/ultrasingapore2019/booking" class="btn btn-primary" onClick="gtag_report_conversion();">Buy Ticket</a>
                        </div>
                        <span class="soldout-label">SOLD OUT</span>
                    </div>
                </div>
                <button type="button" class="info-botton" data-toggle="modal" data-target="#info-modal-pga-1-day"><i class="icon icon-info"></i></button>
            </div>

            <div class="item-soldout premium">
                <div class="category-block">
                    <div class="text-holder">
                        <span class="title">TIER 3</span>
                    </div>
                </div>
                <div class="title-block">
                    <div class="title-block-holder">
                        <span class="subtitle">Saturday</span>
                        <h3 class="title">1-DAY TICKET</h3>
                    </div>
                </div>
                <div class="price-block">
                    <div class="price-block-holder">
                        <span class="price">SGD 178</span>
                        <div class="btn-holder">                            
                            <a href="https://tickets.airasiaredtix.com/airasia-redtix/ultrasingapore2019/booking" class="btn btn-primary" onClick="gtag_report_conversion();">Buy Ticket</a>
                        </div>
                        <span class="soldout-label">SOLD OUT</span>
                    </div>
                </div>
                <button type="button" class="info-botton" data-toggle="modal" data-target="#info-modal-pga-1-day"><i class="icon icon-info"></i></button>
            </div>

            <div class="item-soldout premium">
                <div class="category-block">
                    <div class="text-holder">
                        <span class="title">TIER 2</span>
                    </div>
                </div>
                <div class="title-block">
                    <div class="title-block-holder">
                        <span class="subtitle">By registration only</span>
                        <h3 class="title">2-DAY TICKET</h3>
                    </div>
                </div>
                <div class="price-block">
                    <div class="price-block-holder">
                        <span class="price">SGD 248</span>
                        <buy-button type="pga-t2"></buy-button>
                        <span class="soldout-label">SOLD OUT</span>
                    </div>
                </div>
                <button type="button" class="info-botton" data-toggle="modal" data-target="#info-modal-pga"><i class="icon icon-info"></i></button>
            </div>
            
            <div class="item-soldout premium">
                <div class="category-block">
                    <div class="text-holder">
                        <span class="title">TIER 1</span>
                    </div>
                </div>
                <div class="title-block">
                    <div class="title-block-holder">
                        <span class="subtitle">By registration only</span>
                        <h3 class="title">2-DAY TICKET</h3>
                    </div>
                </div>
                <div class="price-block">
                    <div class="price-block-holder">
                        <span class="price">SGD 228</span>
                        <buy-button type="pga-t1"></buy-button>
                        <span class="soldout-label">SOLD OUT</span>
                    </div>
                </div>
                <button type="button" class="info-botton" data-toggle="modal" data-target="#info-modal-pga"><i class="icon icon-info"></i></button>
            </div>

        </div>
    </section>
</div>