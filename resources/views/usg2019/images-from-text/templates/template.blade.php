

    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>ticket</title>
        <style>
            * {
                margin: 0;
                padding: 0;
            }

            body {
                font-family: sans-serif;
                font-size: 16px;
            }

            .ticket {
                margin: 20px auto;
                background-image: url("./graphic2.png");
                background-repeat: no-repeat;
                width: 918px;
                height: 343px;
                display: flex;
            }

            .event-info {
                width: 270px;
                margin: 25px 0 25px 328px;
                padding-right: 14px;
                padding-top: 45px;
                position: relative;
                /*border-right: 1px dashed rgba(29, 29, 29, 0.2);*/
            }

            .ticket-info {
                width: 246px;
                margin: 44px 20px 20px 40px;
            }

            .event-info h4 {
                color: rgba(29, 29, 29, 0.65);
                letter-spacing: 0.16em;
                font-weight: normal;
                font-size: 13px;
                line-height: 15px;
                margin-bottom: 7px;
            }

            .event-info p {
                font-size: 16px;
                line-height: 19px;
                font-weight: bold;
                color: rgba(29, 29, 29, 0.75);
                margin-bottom: 18px;
            }

            .event-info .line {
                position: absolute;
                top: 0;
                right: -1px;
                width: 1px;
                height: 100%;
                background-image: linear-gradient(to bottom, rgba(29, 29, 29, 0.2) 50%, transparent 50%);
                background-size: 1px 20px;
            }

            .ticket-info h5 {
                font-size: 16px;
                line-height: 19px;
                letter-spacing: 0.02em;
                font-weight: normal;
                margin-bottom: 19px;
            }

            .ticket-info p {
                font-weight: normal;
                font-size: 13px;
                line-height: 15px;
                letter-spacing: 0.02em;
                color: rgba(29, 29, 29, 0.45);
                margin-bottom: 6px;
            }

            .ticket-info p b {
                color: rgba(29, 29, 29, 0.75);
            }

            .qr-box {
                display: flex;
                padding-top: 27px;
            }

            .qr-box .code {
                margin-left: auto;
            }
        </style>
    </head>

    <div class="ticket">
        <div class="event-info">
            <h4>DATE & TIME</h4>
            <p>8 - 9 June 2019 | 2:00 PM</p>
            <h4>VENUE</h4>
            <p>Ultra Park - One Bayfront Avenue, Singapore</p>
            <h4>TICKET CATEGORY</h4>
            <p>PGA 2-DAY TICKET</p>
            <div class="line"></div>
        </div>
        <div class="ticket-info">
            <h5>Barcode number:<br><b>841157905392</b></h5>
            <p>Name: <b>John Doe {{ 1 }}</b></p>
            <p>Email: <b>johndoe@gmail.com</b></p>
            <p>Passport/ID: <b>A123456</b></p>
            <div class="qr-box">
                <img class="code" src="/images/qrcode/{{$fullBarcode}}.png" alt="qr-code"
                     width="{{env('QR_CODE_SIZE')}}" height="{{env('QR_CODE_SIZE')}}">
            </div>
        </div>
    </div>



