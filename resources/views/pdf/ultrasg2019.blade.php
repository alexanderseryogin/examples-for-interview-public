<!DOCTYPE html>
<html>
<head>
	<title>{{$data['full_barcode']}}</title>
	<style>
		
	</style>
</head>
<body>
	<div align="center" style="padding-bottom:25px">
		<table align="center" style="text-align:center">
			<tr>
				<td><img src="{{$logo_url}}" height="50px" width="50px"/></td>
				<td style="padding-left:25px"></td>
			</tr>
		</table>
	</div>
	<hr>
	<div style="position:relative;">
		<img src="{{$ticket_url}}" width="100%"/>
		<div style="position:absolute; top:50; left:370; font-size:15px; width:650px; ">{{$data['full_barcode']}}</div>
		<div style="position:absolute; top:70; left:400; font-size:13px; text-align:left; width:650px; ">{{$data['fullname']}}</div>
		<div style="position:absolute; top:81; left:400; font-size:13px; width:650px; text-align:left">{{$data['email']}}</div>
		<div style="position:absolute; top:93; left:420; font-size:13px; text-align:left; width:650px; ">{{$data['passportID']}}</div>
		<div style="position:absolute; top:155; left:195; font-size:15px; text-align:left; width:550px; ">{{$data['category']}}</div>
		<img src="{{$qr_url}}" style="position:absolute; bottom:-135; right:35" width="80px"/>
	</div>
	<hr>
	<h2>Terms & Conditions:</h2>
	<ol style="font-size:15px">
		<li style="padding-bottom:20px">This ticket is NOT valid for resale. All tickets are tagged to your name and ID checks will be performed at the door. Any resold tickets will be turned away at the gates.</li>
		<li style="padding-bottom:20px">This ticket is not valid for entry into the festival grounds. You or the purchaser of your ticket will need to print this ticket, bring it to Ultra, along with a valid ID (18+) matching the name on the ticket or the purchaser’s ticket, and exchange it for a wristband for entry.</li>
		<li style="padding-bottom:20px">No refund or exchange of e-vouchers will be made under any circumstances except pursuant to event. Postponement or cancellation subject to the event terms and conditions.</li>
		<li style="padding-bottom:20px">Entry will be refused if e-vouchers have not been purchased from AirAsiaRedTix or other authorized points of sale.</li>
		<li style="padding-bottom:20px">The resale of tickets at the same or any price in excess of the initial purchase price is prohibited.</li>
		<li style="padding-bottom:20px">Any complaints regarding the event will be directed to and dealt with by the Promoter.</li>
		<li style="padding-bottom:20px">Other terms and conditions stated in AirAsiaRedTix's website www.airasiaredtix.com and displays at all our authorized agents and box office are also applicable.</li>
		<li style="padding-bottom:20px">For other terms and conditions, please refer to the venue and promoter website.</li>		
	</ol>
	<h2>Conditions of Sales</h2>
	<p style="font-size:15px">This e-vouchers is sold by AirAsiaRedTix as agent for and on behalf of the venue management or Owner ("Venue OWNER") and/or promoter ("Promoter") responsible for the event or which it is sold. AirAsiaRedTix, Owner, their agents and employees shall not be liable for any death, personal injury (unless such death or personal injury was caused by the negligence of AirAsiaRedTix or its agents and employees), loss or damage however caused while in the venue nor are they liable for any complaints, claims, refunds, or exchange for any reason whatsoever, including without limitation, cancellation or postponement of the event. Booking fees and all handling fees shall not be refundable. Any other refunds shall be made only at the Promoter's discretion and on the Promoter's account.</p>
</body>
</html>
