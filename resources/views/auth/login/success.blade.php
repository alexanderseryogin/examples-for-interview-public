<div class="modal fade" id="modalSuccess" tabindex="-1" role="dialog" aria-labelledby="modalSuccess" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-with-logo" role="document">
        <div class="modal-content text-center">
            <div class="modal-body">
                <h5 class="content-title">Password retrieval instructions have been sent</h5>
                <div class="content-sub-title">Note that the link to reset your password will expire in 24 hours.<br>
                Don't forget to check your spam folder if you can't find the email in your inbox.</div>
            </div>
            <div class="modal-footer">
                <a href="/login" class="btn btn-primary_gradient btn-lg">Back to login</a>
            </div>
        </div>
    </div>
</div>