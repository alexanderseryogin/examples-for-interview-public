<div class="modal fade" id="modalResetPassword" tabindex="-1" role="dialog" aria-labelledby="modalResetPassword" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-with-logo" role="document">
        <div class="modal-content text-center">
            <div class="modal-header">
                <h5 class="content-title">Reset your password</h5>
                <div class="content-sub-title">It happens to the best of us! Give us the email tied to you account & wee'll send
                    you an email with instructions to help you get a new password
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('password.email') }}" method="POST" class="form-group-small-wrap" id="resetPasswordForm">
                    @csrf
                    <div class="form-group">
                        <div class="error-block text-left">Sorry, some error.</div>
                        <div class="form-group-animated">
                            {{ Form::label('email', 'Email', ['class' => 'form-label']) }}
                            {{ Form::email('email', null, ['class' => 'form-control', 'id' => 'resetPasswordEmail']) }}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary_gradient btn-lg disabled">Done</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>