<div class="modal fade login-modal" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="modalLogin" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="d-flex">
                <div class="modal-left-col">
                    <div class="logo">
                        <img src="{{asset('images/logo.png')}}" alt="">
                    </div>

                    <h3 class="modal-title">Welcome to <span class="highlighted">RedTix</span></h3>
                    <div class="modal-subtitle">Please login.</div>

                    {{-- Temporary hide --}}
                    {{--<div class="login-with-soc">
                        <div class="regular-title">Login with Facebook or AirAsia Big account:</div>
                        <ul class="login-with-soc-list d-flex align-items-center">
                            <li>
                                <a href="">
                                    <img src="{{asset('images/login/airasiabiglogo.png')}}" alt="airasiabig">
                                </a>
                            </li>
                            <li>
                                <a href="{{route('login.facebook')}}">
                                    <img src="{{asset('images/login/facebook.png')}}" alt="facebook">
                                </a>
                            </li>
                        </ul>
                    </div>--}}

                    <div class="email-login">
                        <div class="top-block d-flex justify-content-between">
                            <div class="regular-title">Login with email address:</div>

                            {{--<div class="d-flex flex-column align-items-end text-right">
                                <span>Don't have an account?</span>
                                <a href="{{route('register')}}" class="btn-link text-primary">Register here</a>
                            </div>--}}
                        </div>

                        <form action="{{route('login')}}" method="POST" id="loginForm">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <div class="error-block">The email and/or password appears to be invalid. Please try again.</div>
                                <div class="coupled-form-control-wrap">
                                    <div class="form-group-animated">
                                        {{ Form::label('email', 'Email', ['class' => 'form-label']) }}
                                        {{ Form::email('email', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}
                                    </div>
                                    <div class="form-group-animated">
                                        {{ Form::label('password', 'Password', ['class' => 'form-label']) }}
                                        {{ Form::password('password', ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>

                            <a href="{{route('homepage')}}" class="bottom-link btn-link btn-link-dark-grey" id="forgotPassword">Forgot your password?</a>

                            <div>
                                <button type="submit" class="btn btn-primary_gradient btn-lg disabled">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-right-col" style="background-image: url({{asset('images/login/login-event.png')}}"></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="k-icon k-i-close">&#x2715; </span> Close
                </button>
            </div>
        </div>
    </div>
</div>