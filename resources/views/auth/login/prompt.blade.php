<div class="modal fade" id="modalPrompt" tabindex="-1" role="dialog" aria-labelledby="modalPrompt" aria-hidden="true">
    <div class="modal-dialog modal-dialog-xs modal-dialog-centered" role="document">
        <div class="modal-content text-center">
            <div class="modal-body">
                <p>Are you sure you want to log out?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary btn-lg-small">Yes</button>
                <button type="button" class="btn btn-primary btn-lg-small" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>