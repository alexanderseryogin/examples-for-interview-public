@extends('layouts.app')

@section('header')
    @include('layouts.partials.header')
@endsection

@section('content')
    <div class="container">
        <div class="text-center">
            <h1 class="auth-page-title">Reset your AirAsia Redtix Password</h1>
            <div class="auth-page-subtitle">Reset your password by filing in the form below.</div>

            <form class="form-group-small-wrap" role="form" method="POST" action="{{ route('password.request') }}">
                {{ csrf_field() }}

                {{Form::hidden('token', $token)}}
                {{Form::hidden('email', $email)}}

                <div class="form-group{{ $errors->any() ? ' has-error' : '' }}">
                    @foreach ($errors->all() as $error)
                        <div class="error-block text-left">
                            {{$error}}
                        </div>
                    @endforeach
                    <div class="coupled-form-control-wrap">
                        <div class="form-group-animated">
                            {{ Form::label('password', 'New password', ['class' => 'form-label']) }}
                            {{ Form::password('password', ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group-animated">
                            {{ Form::label('password_confirmation', 'Reconfirm new password', ['class' => 'form-label']) }}
                            {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary_gradient btn-lg">Done</button>
            </form>
        </div>
    </div>
@endsection
