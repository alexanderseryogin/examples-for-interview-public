@extends('adminlte::master')

@section('adminlte_css')

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
    @stack('css')
    @yield('css')
@stop

@section('body')

    <header class="header-noauth">
        <div class="nav-brand">
            <img src="{{ asset('images/redtix_logo.png') }}" alt="">
        </div>
    </header>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper-noauth h-100" id="adminApp">
        <div class="content-wrapper-full h-100">
            @yield('content')
        </div>
    </div>
    <!-- /.content-wrapper -->
    <!-- ./wrapper -->
@stop

@section('adminlte_js')
    @stack('js')
    @yield('js')
@stop
