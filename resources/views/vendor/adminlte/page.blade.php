@php
    use App\Models\Permission
@endphp

@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}}">
    <!-- Common Kendo UI CSS for web widgets and widgets for data visualization. -->
    <link rel="stylesheet" href="{{ asset('kendoui.for.jquery/styles/kendo.common-material.min.css')}}">
    <!-- Default Kendo UI theme CSS for web widgets and widgets for data visualization. -->
    <link rel="stylesheet" href="{{ asset('kendoui.for.jquery/styles/kendo.material.min.css')}}">
    <!-- (Optional) Kendo UI Hybrid CSS. Include only if you will use the mobile devices features. -->
    <link rel="stylesheet" href="{{ asset('kendoui.for.jquery/styles/kendo.material.mobile.min.css')}}">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link href="{{ asset('css/croppie.css') }}" type="text/css" rel="stylesheet"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
    @stack('css')
    @yield('css')
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper" id="adminApp">

        <!-- Main Header -->
        <header class="main-header">
            @if(config('adminlte.layout') == 'top-nav')
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="navbar-brand">
                                {!! config('adminlte.logo', '<b>Red</b>Tix') !!}
                            </a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                            <ul class="nav navbar-nav">
                                @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item')
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    @endif
                    <!-- Navbar Right Menu -->
                        <div class="navbar-custom-menu">
                            <ul class="nav">
                                @php
                                    $user = Auth::user();
                                    $account = $user->account;
                                @endphp
                                <li class="dropdown user user-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                       aria-expanded="false">
                                        @if($account->avatar)
                                            <img src="{{ $account->avatarUrl }}" class="user-image" alt="{{ $account->full_name }}">
                                        @else
                                            <img src="{{ asset('images/login/user-circle.svg') }}" class="user-image" alt="avatar">
                                        @endif
                                        <span class="hidden-xs">{{$user->email}}</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="user-header">
                                            <a href="{{ route('admin.users.profile.show') }}">
                                                <img src="{{ asset('images/login/user-circle.svg') }}" class="user-icon" alt="">My Account
                                                <span class="sub-text">{{$user->roleList}}</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.users.profile.edit') }}">Profile</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}">Logout<img src="{{ asset('images/login/logout.svg') }}"
                                                                                       class="logout-icon" alt=""></a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        @if(config('adminlte.layout') == 'top-nav')
                    </div>
                    @endif
                </nav>
        </header>


    @if(config('adminlte.layout') != 'top-nav')
        <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">

                <a href="#" class="sidebar-toggle" role="button">
                    <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                </a>

                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">

                    <!-- Logo -->
                    <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="logo">
                        <!-- mini logo for sidebar mini 50x50 pixels -->
                        <span class="logo-mini">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</span>
                        <!-- logo for regular state and mobile devices -->
                        <span class="logo-lg">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</span>
                    </a>

                    <!-- Sidebar Menu -->
                    <ul class="sidebar-menu" data-widget="tree">
                        @each('adminlte::partials.menu-item', $adminlte->menu(), 'item')
                    </ul>
                    <!-- /.sidebar-menu -->
                </section>
                <!-- /.sidebar -->
            </aside>
    @endif

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @if(config('adminlte.layout') == 'top-nav')
                <div class="container">
                @endif

                <!-- Content Header (Page header) -->
                {{--<section class="content-header">--}}
                {{--@yield('content_header')--}}
                {{--</section>--}}

                <!-- Main content -->
                    <section>

                        @yield('content')

                    </section>
                    <!-- /.content -->
                    @if(config('adminlte.layout') == 'top-nav')
                </div>
                <!-- /.container -->
            @endif
        </div>
        <!-- /.content-wrapper -->

    </div>
    <!-- ./wrapper -->
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/nicescroll/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('js/admin.js') }}"></script>
    <script src="{{ asset('js/global.js') }}"></script>
    <!-- Kendo UI combined JavaScript -->
    <script src="{{ asset('kendoui.for.jquery/js/kendo.all.min.js')}}"></script>
    @stack('js')
    @yield('js')
    @include('admin.forms.notification')
@stop
