@extends('ultrasg18.default')
@section('title','Details')

@section('content')
    <section class="home-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4 px-0">
                    <div class="home-box-home">
                    </div>
                </div>
                <div class="col-sm-8 bg-white main-right-container">
                    <div class="main-home">

                        <div class="number-bar">
                            <div><img src="images/ultrapassengerbooking2018/bar.png"></div>
                            <p class="code-text">{{ Auth::guard('ultrasingapore')->user()->formattedTransactionNumber }}</p>
                        </div>

                        <div class="clearfix"></div>
                        <div class="text-only-xs">
                            <div class="row">
                                <div class="col-8"><p class="Order-summary">Order summary:</p></div>
                                <div class="col-4"><p class="QTY">QTY</p></div>
                                <div class="col-8">
                                    <p class="DAY-COMBO-Flight">{{ $purchase->show }}</p> 
                                    {{-- <p class="DAY-COMBO-Flight"> + Flight from KUALA LUMPUR</p> --}}
                                </div>
                                <div class="col-4"><p class="number-xs">{{ $passengerCount }}</p></div>
                                {{-- <div class="col-8"><p class="Sed-diam-ex-imperdi">Sed diam ex, imperdiet id nisi ac, tincidunt lacinia odio. Ut sit amet lectus non justo volutpat dignissim.</p></div> --}}
                            </div>
                        </div>
                        @if (!$isAlreadyCompleted)
                        <p class="Details-required-for">Details required for passengers who are flying:</p>
                        <form id="passenger" action="{{route('ultrasingapore.passenger.store')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="error-container form-group text-center">
                                @if($errors->any())
                                    <div class="passenger-error alert alert-danger">Please check the fields highlighted in red</div>
                                @endif
                            </div>

                            <div id="passenger-container">
                                @for ($i = 0; $i < $passengerCount; $i++)
                                    @include('ultrasg18.passengerspartial',['p_no' => $i+1])
                                @endfor
                            </div>
                            <?php /* <div class="addmore">
                                <a data-url="{{route('ultrasingapore.add.passenger')}}" href="javascript:void(0)" id="add-passenger">Add another passenger<img class="ml-2" src="images/ultrapassengerbooking2018/add.png"></a>
                            </div> */ ?>
                            <div class="submit-home">
                                <div class="row">
                                    <div class="col-lg-3 col-md-1"></div>
                                    <div class="col-lg-9 col-md-11" >
                                        <p class="text-bottom-form" style="font-size: 20px;">
                                        <br /><br />The personal information provided above will be reflected in your flight booking once your click 'Confirm info and submit'.
                                        <br /><br />You'll be denied check-in/boarding if flight booking information given does not match passenger's identification document.</p>
                                        {{-- <p class="text-bottom-form">Please ensure that the personal information provided above is identical to the information in your identification document, the failure of with may result in you being denied entry to the aircraft.</p> --}}
                                    </div>

                                </div>
                                <div class="text-center">
                                    <input style="font-size:20px;" type="submit" value="Confirm Info and Submit" class="btn-form-home">
                                </div>
                            </div>
                        </form>
                        @else
                            <div style="height: 500px"></div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    @component('ultrasg18.modalcomponent', ['modalId' => 'alreadyCompleted'])
     <p>You have already provided all of the necessary passenger details</p>
     <p>&nbsp;</p>
     <div class="text-center">
         <button id="close" class="btn-modal close-modal-logout" >DONE</button>
     </div>
    @endcomponent

@endsection


@section('js')
    <script>
    @if ($isAlreadyCompleted)
        $('#alreadyCompleted').modal({backdrop: 'static', keyboard: false});
    @endif

    @if (session('bookingStatus') == 'success')
        $('#thanks').modal({backdrop: 'static', keyboard: false});
    @endif
    </script>
@endsection