<div class="home-form">
    <div class="row">
        <div class="col-sm-4 col-lg-3 px-0">
            <div class="mobile-line">
                <p class="Passenger-1">Passenger <span class="p-no">{{$p_no ?? "1"}}</span></p>
            </div>
        </div>
        <div class="col-sm-8 col-lg-9">
            <hr class="hr-pass" data-content="AND">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-sm-4 px-0 col-form-label home-lable">Full Name as per passport</label>
        <div class="col-lg-9 col-sm-8 pad-zero">
            <input class="form-control home-input {{$errors->has("passenger.$p_no.fullname") ? 'has-error' : ""}}" type="text" name="passenger[{{$p_no}}][fullname]" maxlength="50" value="{{old("passenger.$p_no.fullname")}}" required>
            @if ($errors->has("passenger.$p_no.fullname"))
                <div class="error-message">{{ $errors->get("passenger.$p_no.fullname")[0] }}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-sm-4 px-0 col-form-label home-lable">Email Address</label>
        <div class="col-lg-9 col-sm-8 pad-zero">
            <input class="form-control home-input {{$errors->has("passenger.$p_no.email") ? 'has-error' : ""}}" type="email" name="passenger[{{$p_no}}][email]" maxlength="50" value="{{old("passenger.$p_no.email")}}" required>
            @if ($errors->has("passenger.$p_no.email"))
                <div class="error-message">{{ $errors->get("passenger.$p_no.email")[0] }}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-sm-4 px-0 col-form-label home-lable">Date of Birth</label>
        <div class="col-lg-9 col-sm-8 pad-zero">
            <input class="form-control home-input {{$errors->has("passenger.$p_no.date_of_birth") ? 'has-error' : ""}}" type="date" name="passenger[{{$p_no}}][date_of_birth]"  value="{{old("passenger.$p_no.date_of_birth")}}" required>
            @if ($errors->has("passenger.$p_no.date_of_birth"))
                <div class="error-message">{{ $errors->get("passenger.$p_no.date_of_birth")[0] }}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-sm-4 px-0 col-form-label home-lable">Passport No</label>
        <div class="col-lg-9 col-sm-8 pad-zero">
            <input class="form-control home-input {{$errors->has("passenger.$p_no.passport_no_nric") ? 'has-error' : ""}}"  type="text" name="passenger[{{$p_no}}][passport_no_nric]" maxlength="50" value="{{old("passenger.$p_no.passport_no_nric")}}" required>
            @if ($errors->has("passenger.$p_no.passport_no_nric"))
                <div class="error-message">{{ $errors->get("passenger.$p_no.passport_no_nric")[0] }}</div>
            @endif

        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-sm-4 px-0 col-form-label home-lable">Passport Expiry Date</label>
        <div class="col-lg-9 col-sm-8 pad-zero">
            <input class="form-control home-input {{$errors->has("passenger.$p_no.passport_expiry_date") ? 'has-error' : ""}}" type="date" name="passenger[{{$p_no}}][passport_expiry_date]" value="{{old("passenger.$p_no.passport_expiry_date")}}" required>
            @if ($errors->has("passenger.$p_no.passport_expiry_date"))
                <div class="error-message">{{ $errors->get("passenger.$p_no.passport_expiry_date")[0] }}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-sm-4 px-0 col-form-label home-lable">Country of Issue</label>
        <div class="col-lg-9 col-sm-8 pad-zero">
            <input class="form-control home-input {{$errors->has("passenger.$p_no.country_of_issue") ? 'has-error' : ""}}" type="text" name="passenger[{{$p_no}}][country_of_issue]" maxlength="50" value="{{old("passenger.$p_no.country_of_issue")}}" required>
            @if ($errors->has("passenger.$p_no.country_of_issue"))
                <div class="error-message">{{ $errors->get("passenger.$p_no.country_of_issue")[0] }}</div>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-sm-4 px-0 col-form-label home-lable">Nationality</label>
        <div class="col-lg-9 col-sm-8 pad-zero">
            <input class="form-control home-input {{$errors->has("passenger.$p_no.nationality") ? 'has-error' : ""}}" type="text" name="passenger[{{$p_no}}][nationality]" maxlength="50" value="{{old("passenger.$p_no.nationality")}}" required>
            @if ($errors->has("passenger.$p_no.nationality"))
                <div class="error-message">{{ $errors->get("passenger.$p_no.nationality")[0] }}</div>
            @endif
        </div>
    </div>
</div>