@extends('layouts.app')

@section('title')
    Test
@endsection

@section('header')
    @include('layouts.partials._appheader')
@endsection

@section('content')
    @include('layouts.components.alert')

    <div class="row">
        <div class="col-md-12">
            <div id="project-label">Select a project (type "j" for a start):</div>

            <input id="project">

            <input type="text" id="project-id">
        </div>
    </div>

@endsection
@push('scripts')
<!-- jQuery (necessary for Bootstrap's JavaScript plugins  and Typeahead) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
  $( function() {
    var venues = JSON.parse({{ $venues }});
    console.log(venues);
    $( "#project" ).autocomplete({
        minLength: 0,
        source: venue,
        focus: function( event, ui ) {
            $( "#project" ).val( ui.item.label );
            return false;
        },
        select: function( event, ui ) {
            $( "#project" ).val( ui.item.label );
            $( "#project-id" ).val( ui.item.value );
            return false;
        }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<div>" + item.label + "</div>" )
        .appendTo( ul );
    };
  } );
</script>
@endpush