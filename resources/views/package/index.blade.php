@extends('usg2019.layouts.master')
@section('title')
    Ultra Singapore 2019
@endsection

@section('header')
    @include('usg2019.layouts.partials.header')
@endsection

@section('content')
    <main class="main">
    <div class="image-block-top">
        <picture>
            <source media="(max-width: 414px)" srcset="/images/ultrasingapore2019/packages/{{$imgtop}}-mobile.jpg">
            <source media="(min-width: 415px)" srcset="/images/ultrasingapore2019/packages/{{$imgtop}}.jpg">
            <img src="/images/ultrasingapore2019/packages/{{$imgtop}}.jpg" alt="ultra-singapore">
        </picture>
        <div class="title-block">
            <h1 class="title">{{$title}}</h1>
        </div>
    </div>
    <div class="main-content-holder">
        <search-panel type="{{$type}}"></search-panel>
            <div class="main-content-wrapp" id="packages-details" style="display: none">
                <div class="row">
                    <div class="col-lg-0 col-sm-0 col-xl-1"></div>
                <div class="container col-md-11 col-sm-8 col-xl-7 col-lg-7">
                    <section class="package-section">
                        @if ($type!="hotel")
                        <div class="section-title-block">
                            <h2 class="section-title">Flights</h2>
                        </div>
                        <div class="items-holder">
                            <div class="item item-flight">
                                <div class="category-block category-block-flight">
                                    <img src="/images/ultrasingapore2019/packages/flight-departure.png">
                                        <span class="subtitle">Kuala Lumpur International Airport 2</span>
                                        <span class="subtitle2">FRI, June 7, 2019</span>
                                        <span class="subtitle2">14:25</span>
                                    <div class="text-holder">
                                    </div>
                                </div>
                                <div class="category-block category-block-flight category-block-return">
                                        <img src="/images/ultrasingapore2019/packages/flight-return.png">
                                        <span class="subtitle">Changi International Airport</span>
                                        <span class="subtitle2">MON, June 10, 2019</span>
                                        <span class="subtitle2">11:40</span>
                                </div>
                                <div class="price-block price-block-flight"></div>
                            </div>
                        </div>
                        @endif
                        @if ($type!="flight")

                        <div class="section-title-block">
                            <h2 class="section-title">Hotels</h2>
                        </div>
                        <div class="items-holder">
                            <div class="item">
                                <div class="category-block category-block-hotel">
                                    <div class="text-holder">
                                        <span class="title"></span>
                                    </div>
                                </div>
                                <div class="title-block">
                                    <div class="title-block-holder">
                                        <h3 class="title-hotel">Hotel G Singapore, Singapore</h3>
                                        <span class="subtitle">200 Middle Road, Bugis, Singapore, Singapore</span>
                                        <span class="subtitle2">07 June - 10 June 2019</span>
                                        <div class="subtitle4">4-Days 3-Nights</div>

                                        <div class="row subtitle3">
                                            <div class="col-1"><img style="min-height: 14px;min-width: 14px;" src="/images/ultrasingapore2019/double-bed.svg"></div>
                                            <div class="col-11 subtitle3-align">One double bed per room for 1 or 2 pax only</div>
                                         </div>
                                        <div class="row subtitle3">
                                            <div class="col-1"><i class="iconart iconart-aircondition"></i></div>
                                            <div class="col-6 subtitle3-align">Air conditioner</div>
                                        </div>
                                        <div class="row subtitle3">
                                            <div class="col-1"><i class="iconart iconart-wifi"></i></div>
                                             <div class="col-6 subtitle3-align">Free Wi-fi</div>
                                        </div>
                                    </div>

                                </div>
                                <div class="price-block">
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="section-title-block">
                            <h2 class="section-title">Tickets</h2>
                        </div>
                        <div class="items-holder">
                            <div class="item premium">
                                <div class="category-block">
                                    <div class="text-holder">
                                        <span class="title">TIER 3</span>
                                    </div>
                                </div>
                                <div class="title-block">
                                    <div class="title-block-holder">
                                        <h3 class="title-ticket">2-DAY TICKET</h3>
                                    </div>
                                </div>
                                <div class="price-block">
                                </div>
                                <button type="button" class="info-botton" data-toggle="modal" data-target="#info-modal-pga"><i class="icon icon-info"></i></button>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-sm-12 col-xl-3 col-lg-4">
                    <package-order-summary type="{{$type}}" title="{{$title}}"></package-order-summary>
                </div>
                <div class="col-0 col-sm-0 col-xl-1">
                </div>
            </div>
            </div>
        </div>
    </main>
@endsection

@section('info-modal')
    @include('usg2019.layouts.modals.info-modal')
@endsection

@section('footer')
    @include('usg2019.layouts.partials.footer')
@endsection