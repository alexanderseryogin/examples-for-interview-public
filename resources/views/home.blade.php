@extends('master')
@section('title')
    The Passport to Entertainment
@endsection

@section('header')
    @include('layouts.partials._innerheader')
@endsection
@section('content')
<section class="pageContent" style="margin: auto auto;  height: 100vh;">
    <div class="mainBodyContent no-btm-mar section-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Dashboard</div>
                        <div class="panel-body">
                            @if(Auth::check())
                                @if(\App\Helpers\AuthHelper::hasCmsAccess()))
                                    <a href="{{url('admin/routes')}}">Admin</a>
                                @endif
                            <a href="{{url('/logout')}}">Logout</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
