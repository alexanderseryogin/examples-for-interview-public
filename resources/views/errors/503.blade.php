@extends('errorpage')
@section('title')
    Under Maintenance
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Content Section -->
    <section class="missingPage h-100 d-flex align-items-center justify-content-center">
        <div class="container text-center">
            <h1>Be right back</h1>
            <p>We are performing some important maintenance work to serve you better</p>
            <div class="clearfix">&nbsp;</div>
        </div>
    </section><!-- /Content Section -->

@endsection