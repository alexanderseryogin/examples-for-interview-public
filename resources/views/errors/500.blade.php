@extends('errorpage')
@section('title')
    500
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Content Section -->
    <section class="missingPage h-100 d-flex align-items-center justify-content-center">
        <div class="container text-center">
            <img class="img-responsive center-block" src="images/error500.png" alt="">
            <h1><span class="text-danger">500</span> - Internal Server Error</h1>
            <p>Error, Something Went Wrong. Please Try Again Later Or</p>
            <div class="clearfix">&nbsp;</div>
            <!-- <a class="btn btn-lg btn-black" href="mailto:support_redtix@airasia.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> Let Us Know</a> -->
            <a class="btn btn-primary-blur btn-lg-small" href="mailto:support@airasiaredtix.com">Let Us Know</a>
        </div>
    </section><!-- /Content Section -->

@endsection