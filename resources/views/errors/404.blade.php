@extends('errorpage')

@section('title')
    404
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Content Section -->
    <section class="missingPage h-100 d-flex align-items-center justify-content-center">
        <div class="container text-center">
            <img class="img-responsive center-block" src="{!! asset('images/error404.png') !!}" alt="">
            <h1><span class="text-danger">404</span> - Page Not Found</h1>
            <p>For Some Reason The Page You Requested Could Not Be Found On Our Server</p>
            <div class="clearfix">&nbsp;</div>
            <a class="btn btn-primary-blur btn-lg-small" href="/">Go Home</a>
        </div>
    </section><!-- /Content Section -->

@endsection