<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title></title>
    <!--[if !mso]><!-- -->
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <!--<![endif]-->
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <style type="text/css">
        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass * {
            line-height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            font-family: 'Roboto';
        }

        table,
        td {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        p {
            display: block;
            margin: 13px 0;
        }
    </style>
    <!--[if !mso]><!-->
    <style type="text/css">
        @media only screen and (max-width: 480px) {
            @-ms-viewport {
                width: 320px;
            }
            @viewport {
                width: 320px;
            }
        }
    </style>
    <!--<![endif]-->
    <!--[if mso]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if lte mso 11]>
    <style type="text/css">
        .outlook-group-fix {
            width: 100% !important;
        }
    </style>
    <![endif]-->
    <style type="text/css">
        @media only screen and (min-width: 480px) {
            .mj-column-per-100 {
                width: 100% !important;
                max-width: 100%;
            }
        }
    </style>
    <style type="text/css">
        @media only screen and (max-width: 480px) {
            table.full-width-mobile {
                width: 100% !important;
            }

            td.full-width-mobile {
                width: auto !important;
            }
        }
    </style>
</head>

<body>
<div style="">
    <!--[if mso | IE]>
    <table
        align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <div style="margin:0px auto;max-width:1040px;background-color:#fcfcfc;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
                <tr>
                    <td style="direction:ltr;font-size:0px;padding:37px 0 190px;text-align:center;vertical-align:top;">
                        <!--[if mso | IE]>
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                            <tr>

                                <td
                                    class="" style="vertical-align:top;width:600px;"
                                >
                        <![endif]-->
                        <div class="mj-column-per-100 outlook-group-fix"
                             style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                <tr>
                                    <td style="font-size:0px;padding:10px 48px;word-break:break-word;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                               style="border-collapse:collapse;border-spacing:0px;">
                                            <tbody>
                                                <tr>
                                                    <td style="width:100px;"><img height="auto" src="{{asset('images/logo.png')}}"
                                                                                  style="border:0;display:block;outline:none;text-decoration:none;height:auto;"
                                                                                  width="111"/></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-size:0px;padding:52px 73px 19px;word-break:break-word;">
                                        <div style="font-size:24px;line-height:1;text-align:left;color:#aeaeae;">Requested a password change.
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-size:0px;padding:10px 73px 0;word-break:break-word;">
                                        <div style="font-size:18px;line-height:1;text-align:left;color:#6B6B6B;">Please click button below to reset your password.
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-size:0px;padding:8px 73px 22px;word-break:break-word;">
                                        <div style="font-size:18px;line-height:1;text-align:left;color:#6B6B6B;">If you did not request a reset & believe this is an error, please contact us immediately
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-size:0px;padding:10px 73px 52px;word-break:break-word;">
                                        <a href="{{ route('password.reset', ['token' => $token, 'email' => $email]) }}"
                                           style="min-width: 190px; padding: 19px 10px; text-align: center; display: inline-block; border-radius: 3px; background-color: #F36175; font-size: 18px; color: #fff; text-decoration: none; letter-spacing: 0.022em;">Reset password</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-size:0px;padding:10px 73px 7px;word-break:break-word;">
                                        <div style="font-size:14px;line-height:1;text-align:left;color:#6B6B6B;">If the button doesn't work, copy &
                                            paste this URL into your browser
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-size:0px;padding:10px 73px;word-break:break-word;">
                                        <a href="{{ route('password.reset', ['token' => $token, 'email' => $email]) }}"
                                           style="font-size:18px;line-height:1.3;text-align:left;color:#F57E8F; font-weight: 500;letter-spacing: 0.02em; text-decoration: none;">{{ route('password.reset', ['token' => $token, 'email' => $email]) }}</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if mso | IE]>
                        </td>

                        </tr>

                        </table>
                        <![endif]-->
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->
</div>
</body>

</html>