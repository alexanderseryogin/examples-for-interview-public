@extends('master')
@section('title')
    {{ $event->title }}
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        @if(!is_null($banner))
            <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{ $banner->path }}" style="width: 100%" class="img-responsive" alt="{{ $event->title }}"></div>
        @else
            <div class="jumbotron eventBanner hidden-xs"><h2 class="text-center" style="color: #fff">Banner Missing</h2></div>
        @endif
        @if(!is_null($thumbnail))
            <div class="widewrapper main hidden-lg hidden-md hidden-sm">
                <img src="{{ $thumbnail->path }}" style="width: 100%" class="img-responsive" alt="{{ $event->title }}">
            </div>
        @else
            @if($event->status == 0)
                <div class="jumbotron eventBanner hidden-lg hidden-md hidden-sm"><h2 class="text-center" style="color: #fff">Banner Missing</h2></div>
            @else
                <div class="jumbotron eventBanner hidden-lg hidden-md hidden-sm"></div>
            @endif
        @endif
    </section>

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            @if($event->title)
                                <h6>{{ $event->title }}</h6>
                            @else
                                <h6>{ Event Title }</h6>
                            @endif
                            @if( $event->tickets()->count())
                                Tickets from <span>RM{{ $event->tickets()->pluck('price')->min()}}</span>
                            @else
                                @if($event->status == 0)
                                    { Tickets }
                                @endif
                            @endif
                        </div>
                        @if($event->tickets()->count())
                            <div class="col-sm-3 text-center">
                                <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
            <div class="container intro">
                <div class="row">
                    <div class="col-sm-offset-1 col-sm-10 leftBar">
                        @if(!is_null($event->start_date))
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>
                                @if(isset($event->start_date)){{ date('jS F Y', strtotime($event->start_date)) }}@endif @if(isset($event->end_time))- {{ date('jS F Y', strtotime($event->end_date)) }}@endif
                            </div>
                        @else
                            @if($event->status == 0)
                                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>
                                    { Event Date Missing }
                                </div>
                            @endif
                        @endif
                        @if($event->venue)
                            <div class="vanue">
                                <i class="fa fa-map-pin" aria-hidden="true"></i> {{ $event->venue->name }}, {{ $event->venue->address1 }} <a target="_blank" href="https://maps.google.com/?q={{ $event->venue->name }},{{ $event->venue->address1 }}">View Map</a>
                            </div>
                        @else
                            @if($event->status == 0)
                                <div class="vanue">
                                    Event venue missing
                                </div>
                            @endif
                        @endif
                        @if(!is_null($event->start_time))
                            <div><i class="fa fa-clock-o" aria-hidden="true"></i> @if(isset($event->start_time)){{ date('h:i a', strtotime($event->start_date)) }}@endif @if(isset($event->end_time))- {{ date('h:i a', strtotime($event->end_date)) }}@endif</div>
                        @endif
                        {{-- <div id="countdown"></div> --}}
                          {{-- <countdown date="August 15, 2016"></countdown> --}}
                        <div class="clearfix">&nbsp;</div>
                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <div class="addthis_inline_share_toolbox"></div>
                        <!-- /sharing -->
                        <hr>
                        @if($event->additional_information)
                            <p>{!! $event->additional_information !!}</p>
                        @else
                            @if($event->status == 0)
                                { Please Add Event's Informations }
                            @endif
                        @endif
                        @if($event->videos)
                            @if($event->videos->count() == 1)
                                <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $videos->first()->youtube_id }}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                                </div>
                            @endif
                        @endif
                    </div>
                    {{-- <example></example> --}}
                </div>
            </div>
            <div class="clearfix"></div>
            </section>
            @if($event->talents->count())
                <section class="pageCategory-section last section-grey">
                    <div class="container">
                        <div class="lineup text-center">
                            <h1 class="subSecTitle"><strong>THE LINEUP</strong></h1>
                            @if($event->category == "Concert")
                                <p>Artist that will be performed</p>
                            @endif
                            <ul class="list-unstyled list-inline lineupList">
                                @foreach($event->talents as $talent)
                                    <li><img src="{{ $talent->img_path }}" alt="{{ $talent->name }}">
                                        @if($talent->name)
                                            {{ $talent->name }}<br>
                                        @endif
                                        @if($talent->company)
                                            {{ $talent->company }}<br>
                                        @endif
                                        @if($talent->position)
                                            {{ $talent->position }}<br>
                                        @endif
                                        @if($talent->field)
                                            {{ $talent->field }}
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </section>
            @endif
            @if($gallery->count() !== 0)
                <section class="pageCategory-section last section-grey">
                    <div class="container">
                        <div class="gallery text-center">
                            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                            <!-- Swiper -->
                            <div class="swiper-container">
                                @if(!is_null($gallery))                       
                                    <div class="swiper-wrapper">
                                        @foreach($gallery as $image)
                                            <div class="swiper-slide">
                                                <a href="{{ $image->path }}" data-featherlight="image"><img class="" src="{{ $image->path }}" alt=""></a>
                                            </div>
                                        @endforeach
                                        @if(!is_null($videos) && ($videos->count() > 1) == true)
                                            @foreach($videos as $video)
                                                <div class="swiper-slide">
                                                    <iframe width="560" height="100%" src="https://www.youtube.com/embed/{{ $video->youtube_id }}" frameborder="0" allowfullscreen></iframe>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>                   
                                @endif
                                <!-- Add Pagination -->
                                <div class="swiper-pagination"></div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next swiper-button-white"></div>
                                <div class="swiper-button-prev swiper-button-white"></div>
                            </div><!-- /Swiper -->
                        </div>
                    </div>
                </section>
            @else
                @if($event->status == 0)
                    <section class="pageCategory-section last section-grey">
                        <div class="container">
                            <div class="gallery text-center">
                                { Please Add Gallery Images }<br>
                            </div>
                        </div>
                    </section>
                @endif
            @endif

            @if($seatplan->count())
                <section class="pageCategory-section last section-grey">
                    <div class="container">
                        <div class="gallery text-center">
                            <h1 class="subSecTitle"><strong>SEATING PLAN</strong></h1>
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    @foreach($seatplan as $img)
                                        <div class="swiper-slide">
                                            <a href="{{ $img->path }}" data-featherlight="image"><img class="" src="{{ $img->path }}" alt=""></a>
                                        </div>
                                    @endforeach
                                </div>  
                                <!-- Add Pagination -->
                                <div class="swiper-pagination"></div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next swiper-button-white"></div>
                                <div class="swiper-button-prev swiper-button-white"></div>
                            </div><!-- /Swiper -->
                        </div>
                    </div>
                </section>
            @endif

            <section class="pageCategory-section last" id="anchorPrice">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            @if($event->tickets->count())
                                <div class="text-center">
                                    <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                    <p>Select ticket</p>
                                </div>
                                <div class="clearfix">&nbsp;</div>
                                <div class="table-responsive">
                                    <table class="table infoTable-D table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Category</th>
                                                <th>Normal</th>
                                               {{--  @foreach($event->tickets as $ticket)
                                                    @if($ticket->coupons)
                                                        @foreach($ticket->coupons as $coupon)
                                                            <th>{{ $coupon->description }}</th>
                                                        @endforeach
                                                    @endif
                                                @endforeach --}}
                                                <th>Remarks</th>
                                                {{-- @if(!is_null($seatplan))
                                                    <th>Seating Plan</th>
                                                @endif --}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($event->tickets->sortBy('sorting_id') as $ticket)
                                                <tr>
                                                    <td style="background-color: {{ $ticket->color }}; color: {{ $ticket->text_color }}">{{ $ticket->title }}</td>
                                                    <td>RM {{ number_format($ticket->price, 2) }}</td>
                                                    {{-- @if($ticket->coupons)
                                                        @foreach($ticket->coupons as $coupon)
                                                            <td>
                                                                {{ $coupon->amount }}
                                                            </td>
                                                        @endforeach
                                                    @endif --}}
                                                    <td>
                                                        @if($ticket->description)
                                                            {{ $ticket->description }}
                                                        @endif
                                                    </td>
                                                    {{-- @if(!is_null($seatplan))
                                                        @if($ticket->seat_plan_path)
                                                            <td><img class="img-responsive seatPlanImg" src="{{ $ticket->seat_plan_path }}" alt=""></td>
                                                        @else
                                                            <td></td>
                                                        @endif
                                                    @endif --}}
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @if($event->is_online || $event->is_outlet)
                                    <div class="buyAlert-bar">
                                        @if($event->buy_links && $event->is_online)
                                            <a class="btn btn-danger" id="buyButton" target="_blank" href="/" role="button">BUY TICKETS</a>
                                        @endif
                                        @if($event->buy_links && $event->is_online && $event->is_outlet)
                                            <span class="or">/</span>
                                        @endif
                                        @if($event->is_outlet)
                                            <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                                        @endif 
                                    </div>
                                @endif
                                @if(!is_null($event->tickets_important_notes))
                                    <span class="importantNote">{!! $event->tickets_important_notes !!}</span>
                                @endif
                            @else
                                @if($event->status == 0)
                                    { Please Setup This Event Tickets }<br>
                                @endif
                            @endif
                            @if($event->event_collection)
                                @if($event->event_collection->name == 'genting')
                                    <div class="note text-left alert alert-warning">
                                        <div class="media">
                                            <div class="media-left media-middle">
                                                <a href="#">
                                                <img class="media-object" src="images/oku.png" alt="">
                                            </a>
                                            </div>
                                            <div class="media-body">
                                                <h2>OKU Info</h2>
                                                <p>Seats for those with special needs are located in the easily accessible zone (marked with the wheelchair sign on the floor plan). For ticket reservations, please call: 03-2718 1118.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <p>3rd party collection will require authorisation letter and photocopy of the NRIC of ticket holder.</p>
                                    <p>Please be informed that all the tickets which issued by Ticketing Agent no longer to redeem at Genting Box Office. Guest may directly enter to the concert with using their original purchased ticket. However, guest who holding the Confirmation Letter/E-Ticket for any concert/show shall remain to exchange their ticket at Arena of Star counter, Genting.</p>
                                @endif
                            @else
                                @if($event->status == 0)
                                    { Please Setup This Event Collection Section }<br>
                                @endif
                            @endif
                            <div class="note text-left">
                                @if(!is_null($event->additional_notes))
                                    <h2>EVENT SPECIFIC NOTES</h2>
                                    {!! $event->additional_notes !!}
                                @endif
                                @if(!is_null($event->important_notes) )
                                    <h2>IMPORTANT NOTE</h2>
                                    {!! $event->important_notes !!}
                                @else
                                    @if($event->status == 0)
                                        { Please Setup This Event Important Note }<br>
                                    @endif
                                @endif
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._eventannoucement')
    @include('layouts.partials.modals._getTix')
@endsection

@push('scripts')
    <script type="text/javascript" src="/js/eventswiper.js"></script>
    <script type="text/javascript" src="/js/seatplan.js"></script>
    {{-- <script type="text/javascript" src="/js/hidebanner.js"></script> --}}
    <script type="text/javascript" src="/js/smoothscroll.js"></script>
    <script type="text/javascript" src="/js/scrollToFixed.js"></script>

    @if($event->announcement_is_published)
        <script type="text/javascript" src="/js/eventannouncement.js"></script>
    @endif
   {{--  <script>
        var end = new Date('02/19/2018 10:1 AM');

        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;

        function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {

                clearInterval(timer);
                document.getElementById('countdown').innerHTML = 'EXPIRED!';

                return;
            }
            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);

            document.getElementById('countdown').innerHTML = days + ' days left ';
            // document.getElementById('countdown').innerHTML += hours + 'hrs ';
            // document.getElementById('countdown').innerHTML += minutes + 'mins ';
            // document.getElementById('countdown').innerHTML += seconds + 'secs';
        }

        timer = setInterval(showRemaining, 1000);
    </script> --}}
    
@endpush