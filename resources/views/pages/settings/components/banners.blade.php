<div class="content-box-large">
	<h4>Hero-Banner</h4>
	<div class="panel panel-default">
		<form action="{{ URL::current() }}/banners/update" method="post">
			{{ csrf_field() }}
            <div class="panel-body table-responsive">
                <table class="table table-hover" cellpadding="0" cellspacing="0" border="0">
					<thead>
						<tr>
							<th>Number Placed</th>
							<th>Title</th>
							<th>Website</th>
							<th>Mobile</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
						@if($banners->count())
							@foreach($banners as $banner)
								<tr>
									{{-- <td>{{ $banner->num_placed }}</td> --}}
									<td>
			    						<select name="number_placed[{{$banner->id}}]" id="number_placed" required="required">
			    							<option value="">Select position number</option>
			    							@for( $i = 1; $i <= 6; $i++)
		    									<option value="{{ $i }}" {{ $banner->num_placed == $i ? 'selected="selected"':'' }}>Position {{ $i }}</option>
			    							@endfor
			    						</select> 
									</td>
									<td>{{ $banner->title }}</td>
									<td>
										<a href="{{ $banner->web_path }}" data-featherlight="image">{{ $banner->web_path }}</a>
									</td>
									<td>
										<a href="{{ $banner->mobile_path }}" data-featherlight="image">{{ $banner->mobile_path }}</a>
									</td>
									<td>
										<a href="#" data-toggle="modal" data-target="#editBanner{{ $banner->id }}"><span class="glyphicon glyphicon-cog"></span></a>
										<a href="{{URL::current()}}/banners/{{ $banner->id }}/delete" onclick="return confirm('Are you sure you want to delete?');"><span class="glyphicon glyphicon-trash"></span></a>
									</td>
								</tr>
							@endforeach
						@else
							<tr>
								<td colspan="5" class="text-center">No Banner</td>
							</tr>
						@endif
					</tbody>
	            </table>
            </div>
            @if($banners->count() < 6)
	            <div class="panel-footer">
	            	<input type="submit" class="btn btn-danger pull-left" value="Update"/>
                    <a class="btn btn-danger pull-right" data-toggle="modal" data-target="#createBanner">Upload Banner</a>
					<div class="clearfix"></div>
	            </div>
	        @endif
        </form>
    </div>
</div>