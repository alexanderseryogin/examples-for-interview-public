<div class="content-box-large">
	<h3 class="text-center">Venues</h3>
    <div class="panel panel-default">
    	<div class="panel-body table-responsive">
            <table class="table table-hover" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Venue Name</th>
						<th>City</th>
						<th>Edit/Delete</th>
					</tr>
				</thead>
				<tbody>
					@if($venues->count())
						@foreach($venues as $venue)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $venue->name }}</td>
								<td>{{ $venue->city }}</td>
								<td>
									<a href="#" data-toggle="modal" data-target="#editvenue{{ $venue->id }}"><span class="glyphicon glyphicon-cog"></span></a>
									<a href="/admin/venues/{{ $venue->id }}/delete" onclick="return confirm('Are you sure you want to delete?');"><span class="glyphicon glyphicon-trash"></span></a>
								</td>
							</tr>
						@endforeach
					@else
						<tr>
							<td colspan="5" class="text-center">No venue</td>
						</tr>
					@endif
				</tbody>
            </table>
        </div>
        <div class="panel-footer">
            <a class="btn btn-danger center-block" data-toggle="modal" data-target="#venuecreate">Create a new venue</a>
			<div class="clearfix"></div>
        </div>
    </div>
</div>