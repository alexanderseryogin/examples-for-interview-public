<div id="venuecreate" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
       <div class="panel panel-default">
            <div class="panel-heading">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Create new venue</h4>
                <div class="clearfix"></div>
            </div>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/settings/venues/store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="col-md-2 control-label">Venue Name</label>
                    <div class="col-md-10">
                        <input id="name" type="text" class="form-control" name="name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="address1" class="col-md-2 control-label">Address 1</label>
                    <div class="col-md-10">
                        <input id="address1" type="text" class="form-control" name="address1">
                    </div>
                </div>
                <div class="form-group">
                    <label for="address2" class="col-md-2 control-label">Address 1</label>
                    <div class="col-md-10">
                        <input id="address2" type="text" class="form-control" name="address2">
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-md-2 control-label">City</label>
                    <div class="col-md-10">
                        <input id="city" type="text" class="form-control" name="city">
                    </div>
                </div>
                <div class="form-group">
                    <label for="state" class="col-md-2 control-label">State</label>
                    <div class="col-md-10">
                        <input id="state" type="text" class="form-control" name="state">
                    </div>
                </div>
                <div class="form-group">
                    <label for="postcode" class="col-md-2 control-label">Postal Code</label>
                    <div class="col-md-10">
                        <input id="postcode" type="number" class="form-control" name="postcode">
                    </div>
                </div>
                <div class="form-group">
                    <label for="country_id" class="col-md-2 control-label">Country</label>
                    <div class="col-md-10">
                        <select class="form-control form-control--select" name="country_id" id="country_id">
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}">{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-danger center-block">
                        Save new venue
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>