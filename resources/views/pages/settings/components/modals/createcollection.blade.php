<div id="createCollection" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
       <div class="panel panel-default">
            <div class="panel-heading">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Create New Collection Section</h4>
                <div class="clearfix"></div>
            </div>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/settings/event_collection/store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="col-md-2 control-label">Collection Name</label>
                    <div class="col-md-10">
                        <input id="name" type="text" class="form-control" name="name">
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-danger center-block">
                        Save new collection
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>