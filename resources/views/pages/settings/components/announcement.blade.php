<div class="content-box-large">
	<h3 class="text-center">Homepage Annoucement</h3>
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/settings/announcement/store') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="content" class="col-md-2 control-label">Content</label>
            <div class="col-md-10">
                <textarea data-provide="markdown-editable" id="content" type="text" class="form-control" name="content">@if($announcement->content){{ $announcement->content }}@endif</textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="btn_link" class="col-md-2 control-label">Button Link</label>
            <div class="col-md-10">
                <input id="btn_link" type="text" class="form-control" name="btn_link" @if($announcement->btn_link)value="{{ $announcement->btn_link }}"@endif>
            </div>
        </div>
        <div class="form-group">
        	<label for="is_published" class="col-md-2 control-label">Publish Announcement</label>
            <div class="col-md-6">
                <label class="radio-inline">
			      <input type="radio" name="is_published" @if($announcement->is_published) checked @endif value="1">Yes
			    </label>
			    <label class="radio-inline">
			      <input type="radio" name="is_published" @if(!$announcement->is_published) checked @endif value="0">No
			    </label>
            </div>
        </div>
        <div class="form-group">
        	<label for="img_link" class="col-md-2 control-label">Announcement Image</label>
        	<div class="col-md-10">
            	<div class="input-group input-file" name="img_link">
                    <span class="input-group-btn">
                        <button class="btn btn-danger btn-choose" name="img_link" type="button">Browse</button>
                    </span>
                    <input type="text" name="img_link" class="form-control" placeholder='Browse website version' @if($announcement->btn_link)value='{{ $announcement->img_link }}'"@endif />
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-danger center-block">
                Store Announcement
            </button>
        </div>
    </form>
</div>