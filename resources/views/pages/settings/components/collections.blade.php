<div class="content-box-large">
	<h4>Event Collection Section</h4>
	<div class="panel panel-default">
		<form action="{{ URL::current() }}/banners/update" method="post">
			{{ csrf_field() }}
            <div class="panel-body table-responsive">
                <table class="table table-hover" cellpadding="0" cellspacing="0" border="0">
					<thead>
						<tr>
							<th>#</th>
							<th>Section Title</th>
						</tr>
					</thead>
					<tbody>
						@foreach($eventcollections as $collection)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $collection->name }}</td>
								<td>
									<a href="{{URL::current()}}/event_collection/{{ $collection->id }}/delete" onclick="return confirm('Are you sure you want to delete?');"><span class="glyphicon glyphicon-trash"></span></a>
								</td>
							</tr>
						@endforeach
					</tbody>
	            </table>
            </div>
            <div class="panel-footer">
                <a class="btn btn-danger pull-right" data-toggle="modal" data-target="#createCollection">Add new event collection section</a>
				<div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>