@extends('layouts.app')

@section('title')
	The Passport to Entertainment
@endsection

@section('header')
    @include('layouts.partials._appheader')
@endsection

@section('content')

	<div class="content-box-large">
        @include('layouts.components.breadcrumbs')
		<h4>Settings</h4>
		<p>Setting for hero-banner set up/annoucement on homepage</p>
	</div>

	@include('pages.settings.components.banners')
	@include('pages.settings.components.venues')
	@include('pages.settings.components.announcement')
	@include('pages.settings.components.collections')
	
	{{-- <div class="content-box-large">
		<h4>Buy Location</h4>
	</div>

	<div class="content-box-large">
		<h4>Default important notes</h4>
	</div>    --}}
@endsection


@section('modal')
	@include('layouts.partials.modals._createBanner')
	@include('layouts.partials.modals._editBanner')
	@include('pages.settings.components.modals.venuecreate')
	@include('pages.settings.components.modals.editvenue')
	@include('pages.settings.components.modals.createcollection')
@endsection

@push('scripts')
    <script src="//code.jquery.com/jquery-latest.js"></script>
    <script src="//cdn.rawgit.com/noelboss/featherlight/1.7.12/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="/js/smoothscroll.js"></script>
    <script type="text/javascript" src="/js/btn_upload.js"></script>
    <script type="text/javascript" src="/js/tables.js"></script>
    {{-- swap placed if banner number is already placed --}}
	<script type="text/javascript">
		var array = {{ json_encode($banners_num_placed) }};
		$('#edit_number_placed').change(function(){
		    if(jQuery.inArray($(this).val(), array))
		        alert("Are you sure you want to swap?");
		});
	</script>
	<script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/tinymce_init.js') }}"></script>
@endpush