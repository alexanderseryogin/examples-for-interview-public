@extends('adminlte::page')

@section('title', 'Profile')

@section('content_header')

@stop

@section('content')
    <div class="content-header">
        <div class="content-header-inner-wrap">
            <div class="row">
                <div class="col-lg-8">
                    <h1>Edit User Details</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content-wrap mt-20">
        <div class="content-box-wrap">
            <div class="content-box">
                <div class="box box-solid">
                    <div class="box-body">
                        <form action="">

                            <div class="form-group">
                                <label for="name" class="control-label"></label>
                                <input class="form-control" type="text">
                                <div class="error-block">

                                </div>
                            </div>

                            <div class="btns-wrap text-center">
                                <button class="btn btn-primary btn-lg-small">Save</button>
                                <button class="btn btn-outline-primary btn-lg-small">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
