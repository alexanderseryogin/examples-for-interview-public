@extends('adminlte::page')

@section('title', 'Profile')

@section('content_header')

@stop

@section('content')
    <div class="content-header">
        <div class="content-header-inner-wrap">
            <div class="row">
                <div class="col-lg-8">
                    <h1>User Profile</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content-wrap mt-20">
        <div class="content-box-wrap">
            <div class="content-box">
                <div class="box box-solid">
                    <div class="user-profile-wrap divided-block d-flex">
                        <div class="left-col d-flex">
                            <div class="avatar-wrap">
                                <img src="<?php echo asset('images/user_image.png'); ?>" class="avatar" alt="avatar">
                            </div>
                            <div>
                                <div class="name">John Doe</div>
                                <div class="position">Admin</div>
                                <dl class="custom-dl">
                                    <dt>Account status:</dt>
                                    <dd><span class="round-status active"></span>Active</dd>
                                    <dt>Last active:</dt>
                                    <dd>23/8/2018, 14:00</dd>
                                </dl>
                            </div>
                        </div>
                        <div class="right-col">
                            <div class="info-title">User details</div>
                            <dl class="custom-dl">
                                <dt>Email:</dt>
                                <dd>john@gmail.com</dd>
                                <dt>Company:</dt>
                                <dd>AirAsia RedTix</dd>
                                <dt>Team</dt>
                                <dd>Ultra Music Festival Team</dd>
                            </dl>
                        </div>
                        <div class="positioned-top-btn">
                            <a href="" class="edit-btn"><i class="k-icon k-i-edit"></i></a>
                        </div>
                        <div class="positioned-bottom-btn">
                            <a href="" class="btn btn-primary-blur btn-lg-small">Delete user</a>
                        </div>
                    </div>
                </div>

                <div class="box box-solid mt-20">
                    <div class="box-body">
                        <h2>Events Summary</h2>
                        <div class="btns-wrap">
                            <a href="" class="btn btn-grey btn-lg-small">Created by user</a>
                            <a href="" class="btn btn-light-grey btn-lg-small">Associated with user</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
