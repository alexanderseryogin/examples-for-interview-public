@extends('layouts.app')

@section('title')
    The Passport to Entertainment
@endsection

@section('header')
    @include('layouts.partials._appheader')
@endsection

@section('content')
    <div class="content-box-large">
        @include('layouts.components.breadcrumbs')
		<h6>Edit Profile</h6> 
	</div>
	<div class="content-box-large">
		<form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/'.$user->digest.'/update') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name" class="col-md-4 control-label">Name</label>
                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-md-4 control-label">Email</label>
                <div class="col-md-6">
                    <input id="email" type="text" class="form-control" name="email" value="{{ $user->email }}">
                </div>
            </div>
            {{-- <div class="form-group">
                <label for="Password" class="col-md-4 control-label">Password</label>
                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password" }}">
                </div>
            </div> --}}
           {{--  <div class="form-group">
                <label class="col-md-2 control-label">Position</label>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                      <input name="is_admin" type="checkbox" id="is_admin" value="1" {{ $event->is_admin ? 'checked="checked"':'' }}>Admin
                    </label>
                </div>
            </div> --}}
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Update Profile
                    </button>
                </div>
            </div>
        </form>
	</div> 
        
       
@endsection