@extends('layouts.app')

@section('title')
    The Passport to Entertainment
@endsection

@section('header')
    @include('layouts.partials._appheader')
@endsection

@section('content')
	<div class="col-md-12">
		<div class="content-box-large">
	        @include('layouts.components.breadcrumbs')
		</div>
	</div>
	<div class="col-md-4">
		<div class="content-box-large">
			<div class="panel-heading text-center">
				{{-- <div class="panel-title">New vs Returning Visitors</div> --}}
				@if(is_null($user->account->avatar))
				    <a href="/images/default/profile/profile.png" data-featherlight="image">
				        <img class="center" src="/images/default/profile/profile.png" alt="Profile Image" style="width:150px; height:150px; border-radius:50%; margin: 0 auto;">
				    </a>
				@else
					<a href="{{ $account->avatar }}" data-featherlight="image">
				        <img src="{{ $account->avatar }}" alt="{{ $user->name }}" style="width:150px; height:150px; border-radius:50%; margin: 0 auto;">
				    </a>
				@endif
	            <h2>{{ $user->name }}'s Profile</h2>
	            <form action="{{ URL::current() }}/avatar" method="post" enctype="multipart/form-data">
                    <label>Upload new profile picture:</label>
                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                    @if ($errors->has('avatar'))
                        <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                    @endif
                    <div class="form-group">
                        <div class="input-group input-file" name="avatar">
                            <span class="input-group-btn">
                                <button class="btn btn-danger btn-choose" name="avatar" type="button">Browse</button>
                            </span>
                            <input type="text" name="avatar" class="form-control" placeholder='Choose a file...' />
                            <span class="input-group-btn">
                                 <button type="submit" class="btn btn-danger" type="button">Upload</button>
                            </span>
                        </div>
                    </div>
                </form>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-2 text-right">
						Name:
					</div>
					<div class="col-md-4">
						{{ $user->name }}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 text-right">
						Email:
					</div>
					<div class="col-md-4">
						{{ $user->email }}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 text-right">
						Position:
					</div>
					@if($user->isSuperAdmin())
						<div class="col-md-4">
							<span class="text-success">Admin</span>
						</div>
					@else
						<div class="col-md-4">
							<span class="text-success">User</span>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="content-box-large">
			<div class="panel-heading">
				<div class="panel-title"><h4>Edit Profile</h4></div>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/'.$user->digest.'/update') }}">
		            {{ csrf_field() }}
		            <div class="form-group">
		                <label for="name" class="col-md-2 control-label">Display Name</label>
		                <div class="col-md-6">
		                    <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" autofocus>
		                </div>
		            </div>
		            <div class="form-group">
		                <label for="first_name" class="col-md-2 control-label">First Name</label>
		                <div class="col-md-6">
		                    <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $user->account->first_name }}">
		                </div>
		            </div>
		            <div class="form-group">
		                <label for="last_name" class="col-md-2 control-label">Last Name</label>
		                <div class="col-md-6">
		                    <input id="last_name" type="text" class="form-control" name="last_name" value="{{ $user->account->last_name }}">
		                </div>
		            </div>
		            <div class="form-group">
		                <label for="address1" class="col-md-2 control-label">Address 1</label>
		                <div class="col-md-6">
		                    <input id="address1" type="text" class="form-control" name="address1" value="{{ $user->account->address1 }}">
		                </div>
		            </div>
		            <div class="form-group">
		                <label for="address2" class="col-md-2 control-label">Address 2</label>
		                <div class="col-md-6">
		                    <input id="address2" type="text" class="form-control" name="address2" value="{{ $user->account->address2 }}">
		                </div>
		            </div>
		            <div class="form-group">
		                <label for="city" class="col-md-2 control-label">City</label>
		                <div class="col-md-6">
		                    <input id="city" type="text" class="form-control" name="city" value="{{ $user->account->city }}">
		                </div>
		            </div>
		            <div class="form-group">
		                <label for="state" class="col-md-2 control-label">State</label>
		                <div class="col-md-6">
		                    <input id="state" type="text" class="form-control" name="state" value="{{ $user->account->state }}">
		                </div>
		            </div>
		            <div class="form-group">
		                <label for="postal_code" class="col-md-2 control-label">Postal Code</label>
		                <div class="col-md-6">
		                    <input id="postal_code" type="text" class="form-control" name="postal_code" value="{{ $user->account->postal_code }}">
		                </div>
		            </div>
		            <div class="form-group">
		                <label for="country" class="col-md-2 control-label">Country</label>
		                <div class="col-md-6">
		                    <select class="form-control form-control--select" name="country" id="country">
		                        @foreach($countries as $country)
		                        	<option value="{{ $country->id }}" {{ $user->account->country_id ==  $country->id ? 'selected="selected"':'' }}>{{ $country->name }}</option>
		                        @endforeach
		                    </select>
		                </div>
		            </div>

		            <div class="form-group">
                        <label class="col-md-2 control-label" for="users_timezone" >Timezone</label>
                        <div class="col-md-6">
	                        <select class="form-control form-control--select" name="timezone" id="users_timezone">
	                            <option value="-">Select your timezone</option>
	                            @include('layouts.components.timezone_option')
	                        </select>
                        </div>
                    </div>

		            <div class="form-group">
		                <label for="email" class="col-md-2 control-label">Email</label>
		                <div class="col-md-6">
		                    <input id="email" type="text" class="form-control" name="email" value="{{ $user->email }}">
		                </div>
		            </div>
		            {{-- <div class="form-group">
		                <label for="Password" class="col-md-4 control-label">Password</label>
		                <div class="col-md-6">
		                    <input id="password" type="password" class="form-control" name="password" }}">
		                </div>
		            </div> --}}
		           {{--  <div class="form-group">
		                <label class="col-md-2 control-label">Position</label>
		                <div class="col-md-6">
		                    <label class="checkbox-inline">
		                      <input name="is_admin" type="checkbox" id="is_admin" value="1" {{ $event->is_admin ? 'checked="checked"':'' }}>Admin
		                    </label>
		                </div>
		            </div> --}}
		            <div class="form-group">
		                <div class="col-md-6 col-md-offset-2">
		                    <button type="submit" class="btn btn-danger">
		                        <i class="glyphicon glyphicon-refresh"></i> Update Profile
		                    </button>
		                </div>
		            </div>
		        </form>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
<script src="//code.jquery.com/jquery-latest.js"></script>
<script src="//cdn.rawgit.com/noelboss/featherlight/1.7.12/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/tinymce_init.js') }}"></script>


<script>
    // function thisFileUpload() {
    //     document.getElementById("file").click();
    // };
</script>

<script type="text/javascript" src="/js/smoothscroll.js"></script>
<script type="text/javascript" src="/js/btn_upload.js"></script>
@endpush