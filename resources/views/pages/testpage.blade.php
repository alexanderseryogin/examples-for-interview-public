@extends('master')
@section('title')
    Purchase Terms & Condition
    @endsection

    @section('header')
    @include('layouts.partials._header')
    @endsection

    @section('content')

            <!-- Banner Section -->
    <section class="pageBanner">
        <div class="jumbotron" style="background-image: url(/images/bigmainbanner.jpg);">
            <div class="bigBanner-overlay"></div>
            <div class="container banner-body">
                <div class="col-sm-offset-1 col-sm-10 col-xs-12">
                    <div class="bannerText text-center">
                        <h1>Purchase Terms & Condition</h1>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /Banner Section -->

    <!-- Content Section -->
    <section class="pageContent">
        <div class="container">

            <div class="row">

                <!-- Main Body -->
                <div class="mainBodyContent col-sm-9">

                    <h1>Hello</h1>
                    {{$ticket}}

                </div><!-- /Main Body -->

                <!-- Sidebar -->
                @include('layouts.partials._innersidebar')
                        <!-- /Sidebar -->

            </div><!-- /row -->

        </div>
    </section><!-- /Content Section -->

@endsection