@extends('master')
@section('title')
    Events
@endsection

@section('header')
    @include('layouts.partials._innerheader')
@endsection

@section('content')

    <!-- Search Section -->
    <section class="search-nav">
      <div class="filter-navHolder">
        <div class="container">
          <div class="bannerText text-center">
            <h1>Find Events</h1>
          </div>
          <form method="GET">
            <input type="hidden" name="category" value="{{ Request::input('category[]') }}">
            <input type="hidden" name="duration" value="{{ Request::input('duration') }}">
            <div class="input-group input-group-hg input-group-rounded">
              <span class="input-group-btn">
                <button type="submit" class="btn"><span class="fui-search"></span></button>
              </span>
              <input type="text" class="form-control" placeholder="Search in AirAsia Redtix" id="" name="title" value="{{ Request::input('title') }}">
            </div>
          </form>
        </div>
      </div>
    </section><!-- /Search Section -->

    <!-- Content Section -->
    <section class="pageContent">
      <div class="container">
        
        <div class="row">
          <!-- Filter Sidebar -->
          <aside class="col-sm-3 filterSidebar">
            <div class="filter-navHolder boxPanel">
              <form method="GET">
                <input type="hidden" name="title" value="{{ Request::input('title') }}">
                <div class="boxPanelHead">
                  <h6><i class="fa fa-sliders" aria-hidden="true"></i> Filter your search</h6>
                </div>
                <div>
                  {{-- {{ dd(Request::input('category[]')) }} --}}
                  <p><strong>Categories</strong></p>
                  <label class="radio" for="concert">
                    <input type="radio" value="concerts" name="category" @if(Request::input('category') == "concerts") checked="checked" @endif id="concert" data-toggle="checkbox">Concerts
                  </label>
                  <label class="radio" for="theatre">
                    <input type="radio" value="theatre" name="category" @if(Request::input('category') == "theatre") checked="checked" @endif id="theatre" data-toggle="checkbox">Theatre
                  </label>
                  <label class="radio" for="sports">
                    <input type="radio" value="sports" name="category" @if(Request::input('category') == "sports") checked="checked" @endif id="sports" data-toggle="checkbox">Sports
                  </label>
                  <label class="radio" for="family">
                    <input type="radio" value="family" name="category" @if(Request::input('category') == "family") checked="checked" @endif id="family" data-toggle="checkbox">Family
                  </label>
                  <label class="radio" for="festivals">
                    <input type="radio" value="festivals" name="category" @if(Request::input('category') == "festivals") checked="checked" @endif id="festivals" data-toggle="checkbox">Festivals
                  </label>
                  <label class="radio" for="attractions">
                    <input type="radio" value="attractions" name="category" @if(Request::input('category') == "attractions") checked="checked" @endif id="attractions" data-toggle="checkbox">Attractions
                  </label>
                  <label class="radio" for="promotional">
                    <input type="radio" value="promotional" name="category" @if(Request::input('category') == "promotional") checked="checked" @endif id="promotional" data-toggle="checkbox">Promotional
                  </label>
                  {{-- <label class="radio" for="others">
                    <input type="radio" value="" name="category" @if(Request::input('category') == "others") checked="checked" @endif id="others" data-toggle="checkbox">Others
                  </label> --}}
                </div>
                <hr>
                <div>
                  <p><strong>Events Collections</strong></p>
                  <label class="radio" for="all">
                    <input type="radio" value="all" name="eventcollection" @if(Request::input('eventcollection') === 'all' ) checked="checked" @endif id="all" data-toggle="checkbox">All
                  </label>
                  @foreach($eventcollections as $eventcollection)
                    <label class="radio" for="{{ $eventcollection->name }}">
                      <input type="radio" value="{{ $eventcollection->name }}" name="eventcollection" @if(Request::input('eventcollection') === $eventcollection->name ) checked="checked" @endif id="{{ $eventcollection->name }}" data-toggle="checkbox">{{ ucfirst($eventcollection->name) }}
                    </label>
                  @endforeach
                </div>
                <div class="">
                    <div class="form-group has-feedback">
                        <input placeholder="Select Date" type="date" class="form-control date form_date col-xs-12" name="duration" value="{{ strlen(Request::input('duration')) ? date('Y-m-d',strtotime(Request::input('duration'))) : 'MM/DD/YYY' }}" data-link-format="yyyy-mm-dd" />
                        <span class="form-control-feedback fa fa-calendar"></span>
                    </div>
                </div>
                <hr>
                <div class="">
                  <div class="form-group">
                    <button type="submit" class="btn btn-danger center-block">
                        Search <span class="fui-search"></span>
                    </button>
                  </div>
                </div>
                <hr>
              </form>
            </div>
          </aside>
          <!-- /Filter Sidebar -->

          <!-- Main Body -->
          <div class="mainBodyContent col-sm-9 pad-L-reset15">

            <section class="eventCategory-section last">
              @if(Request::input('title'))
                <h1 class="mainSecTitle">Search results for @if(Request::input('title')) {{ Request::input('title') }} @endif</h1>
              @else
                <h1 style="margin-bottom: 0px;">&nbsp</h1>
              @endif
              @if(Request::input('title'))
                <p>Found {{ $events->count() }} results</p>
              @else
                <p style="margin-bottom: 0px;">&nbsp</p>
              @endif
              <div class="eventList boxPanel">
              @foreach($events as $event)
                <!--Ticket List-->
                <ul class="list-unstyled">
                  <li>
                    <div class="media">
                        <div class="media-left media-middle">
                          @if($event->images->where('type', 'thumbnail')->count())
                            <a href="{{ URL::to('/events/'.$event->slug) }}">
                                <div class="evThumb" style="background-image: url({{ $event->images->where('type', 'thumbnail')->first()->path }})"></div>
                            </a>
                          @else
                            <a href="{{ URL::to('/events/'.$event->slug) }}">
                                <div class="evThumb" style="background-image: url('') }})"></div>
                            </a>
                          @endif
                        </div>
                        <div class="media-body">
                          <div class="eventText">
                            <div class="cat">{{ $event->category }}</div>
                            <h4 class="m-heading"><a href="{{ URL::to('/events/'.$event->slug) }}">{{ $event->title }}</a></h4>
                            <p><strong>Date :</strong>
                              @if($event->date)
                               {{ date('F d, Y', strtotime($event->date)) }}
                              @else
                                {{ date('F d, Y', strtotime($event->start_date))}} 
                              @endif
                            </p>
                            <p><strong>Time :</strong>
                              @if($event->start_time)
                                {{ date('h:i A',strtotime($event->start_time)) }}
                              @else
                                Unavailable
                              @endif
                            </p>
                            @if($event->venue)
                              <p><strong>Location :</strong> {{ $event->venue->name }}, {{ $event->venue->city }}</p>
                            @endif
                          </div>
                          <div class="priceNbtn">
                            <p>From<span>RM {{ $event->tickets->pluck('price')->min() }} - RM {{ $event->tickets->pluck('price')->max() }}</span></p>
                            <a class="getTix-btn" href="{{ URL::to('/events/'.$event->slug.'#anchorPrice') }}">Get Ticket</a>
                          </div>                            
                        </div>
                    </div>
                  </li>
                </ul>
              @endforeach
              
              </div>
              <div class="clearfix text-center">
                {{ $events->links() }}
              </div>
            </section>

          </div><!-- /Main Body -->

        </div><!-- /row -->

      </div>
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <!-- Initialize Swiper -->
    <script type="text/javascript">
    // Date Picker for Filter
    $('.form_date').datetimepicker({
        fontAwesome: true,
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    </script>
    
@endsection