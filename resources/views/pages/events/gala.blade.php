@extends('master')
@section('title')
    GALA by Jerome Bel
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/gala/gala-banner.png')">
      </div>
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>GALA by Jerome Bel</h6>
                  Tickets from <span>RM25</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 19 - 20th August 2017, (Saturday - Sunday)</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Dewan Sri Pinang Auditorium, George Town, Penang <a target="_blank" href="https://goo.gl/maps/qNKZwx9yoFv">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8.30pm (90 minutes without intermission)</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <h2>GALA by Jerome Bel</h2>
                <p>- Photo by: Josefina Tommasi, Museo de Arte Moderno de Buenos Aires (Argentina, August 2015)</p>
                <p>Where is the art in dance when it includes no hint of individuality? Where is the art in dance when the rawness and sincere emotion fails to shine through the movements of one’s body? A usual dance stage would fill its floor with a wide range of styles, flawless techniques and yearlong experiences. But what happens when you put together ordinary people with the same passion – to dance?</p>
                <p>Varying from different generations, backgrounds and life-stories, twenty ordinary people take the stage – whether they are professionals, first timers or simply the type to let loose; changing the expectations of the public and instead, embraces the art of dance, or much rather, the art of trying to dance, on a different take.</p>
                <p>French choreographer Jérôme Bel introduces Gala – showcasing unique and exclusive hand-picked George Town individuals taking turns in executing different styles and genre of dances; allowing the audience to be a part of the imaginative world the performers slip into whilst on stage.</p>
                <p><b>QUOTES</b><br>
                    “Funny, moving and entertaining. An outrageously entertaining show” - The Times<br>
                    "It bears the stamp of Bel's special genius, both in eliciting so much for his performers' trust and in winning over his audience" - The Guardian, 2016<br>
                    “Gala places the audience&#39;s expectations in the foreground, blurring the boundary between failure and success in the show, suggesting that the theater is a community, both on and off stage. It is a tour de force, fiercely entertaining and deeply radical”. - Roslyn Sulcas, The New York Times, May 13, 2015<br>
                    “Funny, moving and entertaining. An outrageously entertaining show.” - The Times</p>
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/QuOna16GLOQ" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/gala/gala1.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/gala/gala2.jpg" alt="">
                  </div>
                  
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Category</th>
                          <th>Normal Price</th>
                          <th>Early Bird Price <br>(24 May - 23 June 2017)</th>
                          <th>Senior Citizen,<br> Child (5 - 12 years),<br> OKU (Disabled)</th>
                          <th>Seating Plan</th> 
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td><span class="box midblue">STUDENT</td>
                              <td>RM 25</td>
                              <td>Not Applicable</td>
                              <td>Not Applicable</td>
                              <td rowspan="4"><img class="img-responsive seatPlanImg" src="images/manganiyar/seatplan.png" alt="">
                          </tr>
                          <tr>
                              <td><span class="box pink">FLOOR 1</td>
                              <td>RM 65</td>
                              <td>RM 46</td>
                              <td>RM 33</td>
                          </tr>
                          <tr>
                              <td><span class="box midgreen">FLOOR 2</td>
                              <td>RM 85</td>
                              <td>RM 60</td>
                              <td>RM 43</td>
                          </tr>
                          <tr>
                              <td><span class="box yellow">GALLERY</td>
                              <td>RM 125</td>
                              <td>RM 88</td>
                              <td>RM 63</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/gtf 2017- gala by jerome bel/events" role="button">BUY TICKETS</a>
                  <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>
                <span class="importantNote">* Additional charges will be applied for credit/debit card transaction, optionally customer may pay cash for outlet purchase to avoid the charges.</span>
                <div class="note text-left">
                    <h2>Admission Rules</h2>
                    <ol>
                        <li>Students and senior citizen ticket holders are obliged to present proof of identification at the door.</li>
                        <li>Out of consideration for both the audience and artists, <strong class="text-danger">children under the age of 5 are not allowed admission.</strong></li>
                    </ol>
                    <h2>Late Seating Policy</h2>
                    <ol>
                        <li>Latecomers will not be seated once the performance has begun. No refunds will be made for patrons arriving late.</li>
                        <li>If you must leave the performance at any time once it has begun, you will not be re-admitted until an appropriate break.</li>
                    </ol>
                    <h2>Photography Rules</h2>
                    <ol>
                        <li>No photography, video recording and audio recording is allowed for this event.</li>
                    </ol>
                </div>
                <div class="note text-left">
                    <h2>Important Notes</h2>
                    <ol>
                      <li>Prices shown include RM4.00 AirAsiaRedTix fee.</li>
                      <li>Subject to 6% GST and credit card charges.</li>
                      <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                      <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                      <li>Strictly no replacement for missing tickets and cancellation.</li>
                      <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                    </ol>
                    <h2>For enquiry only:</h2>
                    <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        {{-- <section class="pageCategory-section last">
          <div class="buyNowSec" style="background-image: url('images/event2.jpg')">
            <div class="bannerOverlay"></div>
            <div class="container">
              <div class="col-sm-offset-1 col-sm-10 quoteArea">
                <h1>You’ve heard him on the radio, you’ve seen him on TV. It’s about time. See him live!</h1>
              </div>
              <div class="btnArea">
                <a class="btn btn-black btn-lg" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/konsert%20gegar%20pop%20yeh%20yeh/events" role="button">GET TICKET ONLINE NOW <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <div class="clearfix">&nbsp;</div>
                <span data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets</span>
              </div>
            </div>
          </div>
        </section> --}}

        {{-- <section class="pageCategory-section last">
          <div class="container">
            <div class="partner text-center">
                <h1 class="subSecTitle"><strong>PARTNERS</strong></h1>
                <div class="clearfix">&nbsp;</div>
                <ul class="list-unstyled list-inline partnerList">
                    <li><img src="images/neon/aabig_logo (offical partner).png" alt=""></li>
                    <li><img src="images/neon/BAC.png" alt=""></li>
                    <li><img src="images/neon/durex (official partner).png" alt=""></li>
                    <li><img src="images/neon/fac3 (co-organiser).jpg" alt=""></li>
                    <li><img src="images/neon/seven friday (official watch).jpeg" alt=""></li>
                    <li><img src="images/neon/sunway lagoon (official venue).png" alt=""></li>
                </ul>
            </div>
          </div>
        </section> --}}

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                <dd>Bangsar Village (TEL: 03-22021139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
                <dd>Tropicana</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
                <dd>Artist Gallery, Gurney Plaza (04-228 5648)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-637 0191)</dd>
            </dl>
            </div>
        </div>
        </div>
    </div>

@endsection