@extends('master')
@section('title')
    Origin Fields
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Origin Fields" />
    <meta property="og:description" content="Origin Fields takes the 12th year of Origin NYE to a new apex with the largest music and cultural festival of 2018."/>
    <meta property="og:image" content="{{ Request::Url().'images/originfields2018/thumbnail.jpg' }}" />
@endsection

@section('content')
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="css/custom2.css">
    <link type="text/css" rel="stylesheet" href="css/clock.css">
    <link type="text/css" rel="stylesheet" href="/css/custom/redtix.css">
    <link type="text/css" rel="stylesheet" href="css/timeTo.css">

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        {{-- <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/originfields2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Origin Fields"></div> --}}
        <div class="jumbotron embed-responsive embed-responsive-16by9 hidden-xs">
            <iframe height="500" style="padding-top:20px;" class="embed-responsive-item" src="https://www.youtube.com/embed/5NAXh192wKs?rel=0&showinfo=0&autoplay=1&controls=0&loop=1&modestbranding=0" allow="autoplay" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            {{-- <img src="{{a5sset('images/originfields2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Origin Fields"> --}}
            <iframe height="25%" width="100%" class="embed-responsive-item" src="https://www.youtube.com/embed/5NAXh192wKs?rel=0&showinfo=0&autoplay=1&controls=0&loop=1&modestbranding=0" allow="autoplay" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Origin Fields</h6> Tickets from <span>RM930</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  30th - 31st December 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Langley Park, Plain St & Hay Street, East Perth WA 6004, Australia <a target="_blank" href="https://goo.gl/maps/BV2CWyopfFz">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Sun, 30th December - Mon, 31st December </div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-8">
                                <p><h2>ORIGIN FIELDS ANNOUNCE ALL-STAR LINE UP WITH HEADLINERS CARDI B, KHALID, MIGOS, TASH SULTANA, THE KOOKS AND MORE</h2></p>
                                <p>Western Australia’s largest New Year’s celebration is evolving.</p>
                                <p><b>Origin Fields</b> take the 12th year of Origin NYE to a new apex with the largest music and cultural festival of 2018. Now two days of music over December 30 & 31, Origin Fields delivers a diverse line-up of the biggest acts from Australia and around the world.</p>
                                <p>This year will see Origin Fields arriving at the scenic new foreshore location of <b>Langley Park</b> on Perth’s iconic foreshore, and with it two days of the most varied and impressive Origin experience to date.</p>
                                <p>With the change in format, support from <b>triple j</b> and a line-up of the world’s hottest artists - including female rap superstar <b>Cardi B</b>, super-group <b>Migos, Khalid, The Kooks, Pendulum (Live), Tash Sultana, Duke Dumont, PNAU</b> and many more festival favorites - the new direction of Origin Fields aspires to cement Perth’s reputation as a world-class festival destination.</p>
                                <p><br />Origin Fields 2018, First Round Announcement below in alphabetical order:<br />
                                <font-type style="font-family:futura; font-size:12px;">AJ TRACEY &middot;
                                BONOBO [DJ SET] &middot;
                                CARDI B (One of two Australian performances) &middot;
                                DUKE DUMONT &middot;
                                FLAVA D &middot;
                                HABSTRAKT &middot;
                                HAYDEN JAMES &middot;
                                HYPE & HAZARD &middot;
                                KEYS N KRATES &middot;
                                KHALID (AUSTRALIAN EXCLUSIVE PERFORMANCE) &middot;
                                THE KOOKS &middot;
                                MIGOS (One of two Australian performances) &middot;
                                MIJA &middot;
                                MR CARMACK &middot;
                                PENDULUM [LIVE] (AUSTRALIAN EXCLUSIVE PERFORMANCE) &middot;
                                PNAU &middot;
                                ROLLING BLACKOUTS COSTAL FEVER &middot;
                                SAFIA &middot;
                                SAMPA THE GREAT &middot;
                                SASASAS &middot;
                                SIGMA [DJ SET] &middot;
                                SUPERDUPER &middot;
                                KYLE &middot;
                                SPACE LACES &middot;
                                TASH SULTANA &middot;
                                TCHAMI X MALAA [NO REDEMPTION] &middot;
                                VIRTUAL RIOT &middot;
                                WHETHAN &middot;
                                WINSTON SURFSHIRT
                                </font-type>
                                </p>
                            </div>
                            <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/user/originnye/playlist/4QhEw1GhsylPssqtX0zVjW" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/originfields2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/originfields2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/originfields2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/originfields2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/originfields2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/originfields2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/originfields2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/originfields2018/gallery-4.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            {{-- <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>MY KAD Price</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>First Release: 2 Day Pass</td>
                                            <td>AUD 319 / RM 970.00</td>
                                            <td>RM 930.00</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td>5 Night Stay for 2 People - Bundle A<br/>Kuala Lumpur - Perth</td>
                                            <td>RM 7,400.00</td>
                                            <td>-</td>
                                            <td>Package includes: 2 Return flights on AirAsia, 5 Night stay at Duxton Hotel (5*) & (2) 2 Day Passes at Origin Fields</td>
                                        </tr>
                                        <tr>
                                            <td>5 Night Stay for 2 People - Bundle B<br/>Kuala Lumpur - Perth</td>
                                            <td>RM 6,220.00</td>
                                            <td>-</td>
                                            <td>Package includes: 2 Return flights on AirAsia, 5 Night stay at IbisHotel (3*) & (2) 2 Day Passes at Origin Fields</td>
                                        </tr>
                                        <tr>
                                            <td>5 Night Stay for 2 People - Bundle C<br/>Bali - Perth</td>
                                            <td>RM 8,032.00</td>
                                            <td>-</td>
                                            <td>Package includes: 2 Return flights on AirAsia, 5 Night stay at Duxton Hotel (5*) & (2) 2 Day Passes at Origin Fields</td>
                                        </tr>
                                        <tr>
                                            <td>5 Night Stay for 2 People - Bundle D<br/>Bali - Perth</td>
                                            <td>RM 7,161.00</td>
                                            <td>-</td>
                                            <td>Package includes: 2 Return flights on AirAsia, 5 Night stay at Ibis Hotel (3*) & (2) 2 Day Passes at Origin Fields</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Dec 29 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/origin fields (30th - 31st december 2018)/events" role="button">BUY TICKETS</a>
                                <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                            </div> --}}

                            @include('layouts.partials._showtickets')

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude RM4.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Australian Dollar prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in AUD may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close a day prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
    @include('layouts.partials.modals._moreInfo')
@endsection