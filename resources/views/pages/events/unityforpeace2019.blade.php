@extends('master')
@section('title')
    Unity4Peace Walk A Hunt Challenge 2019
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Unity4Peace Walk a Hunt" />
    <meta property="og:description" content="The UNITY4PEACE Walk A Hunt Challenge 2019 has been conceptualized to promote and showcase the amazing heritage of Kuala Lumpur"/>
    <meta property="og:image" content="{{ Request::Url().'images/unityforpeace2019/thumbnail2.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/unityforpeace2019/web-banner3.jpg')}}" style="width: 100%" class="img-responsive" alt="Unity4Peace Walk a Hunt"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/unityforpeace2019/thumbnail2.jpg')}}" style="width: 100%" class="img-responsive" alt="Unity4Peace Walk a Hunt">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Unity4Peace Walk A Hunt Challenge 2019</h6> Tickets from <span>RM83</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            {{-- <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 7th April 2019</div> --}}
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Level 6 Carpark, Terminal Bersepadu Selatan, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/pXbn7nEYuZK2">View Map</a></div>
                            {{-- <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Sunday, 7 April (8.00 am - 5.00pm)</div> --}}
                            <div class="time"><i class="fa fa-calendar" aria-hidden="true"></i> Sunday, 7th April 2019</div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Registrations opens at 6.00am</div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Event starts at 8.00am to 5.00pm</div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Prize Presentation at 5.30pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <p><h2>Unity4Peace Walk A Hunt Challenge 2019</h2><br/>
                                The Unity4Peace Walk A Hunt Challenge 2019 which is produced & organized by Perdana Global Peace Foundation (PGPF) & Define International will see an estimated 2000 to 4000 participants trying their best to win our total prize money of RM 73,000.00! With the historical journey of our beloved capital Kuala Lumpur as our guide, this journey using MRT, LRT & Monorail will take all participants on a hunting trail like no other before. Come join & be part of this record breaking event that caters to all participants above 18 years old!</p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/6J9LZY3x1kU?&autoplay=1&loop=1&rel=0&showinfo=0&color=white&iv_load_policy=3&playlist=6J9LZY3x1kU" frameborder="0" allow="autoplay" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/unityforpeace2019/gallery-dj-1.jpg" data-featherlight="image"><img class="" src="images/unityforpeace2019/gallery-dj-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>EVENT INFORMATION</strong></h1>
                            </div>
                            {{-- <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Place</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1<sup>st</sup> Place</td>
                                            <td>RM 30,0000 + Medals</td>
                                        </tr>
                                        <tr style="background-color: #953734; color: #FFFFFF;">
                                            <td>2<sup>nd</sup> Place</td>
                                            <td>RM 20,000.00 + Medals</td>
                                        </tr>
                                        <tr style="background-color: #FFC001; color: #FFFFFF;">
                                            <td>3<sup>rd</sup> Place</td>
                                            <td>RM 10,000.00 + Medals</td>
                                        </tr>
                                        <tr style="background-color: #17375F; color: #FFFFFF;">
                                            <td>4<sup>th</sup> Place</td>
                                            <td>RM 5,000.00 + Medals</td>
                                        </tr>
                                        <tr style="background-color: #77933B; color: #FFFFFF;">
                                            <td>5<sup>th</sup> Place</td>
                                            <td>RM 3,000.00 + Medals</td>
                                        </tr>
                                        <tr>
                                            <td>6<sup>th</sup> to 10<sup>th</sup> Place</td>
                                            <td>RM 1,000.00 + Medals</td>
                                        </tr>
                                        <tr style="background-color: #953734; color: #FFFFFF;">
                                            <td>Hunt Completion</td>
                                            <td>Medals</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> --}}

                            <div class="text-center">
                                <img class="img-responsive center-block" src="images/unityforpeace2019/event-info.png" alt="">
                            </div>

                        </div>
                    </div>
                </div>
            </section>


            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                {{-- <p>Select ticket</p> --}}
                            </div>
                            {{-- <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Early Bird</td>
                                            <td>RM 83.00</td>
                                        </tr>
                                        <tr>
                                            <td>Normal</td>
                                            <td>RM 99.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="buyAlert-bar">  --}}

                            <div class="text-center">
                                <img class="img-responsive center-block" src="images/unityforpeace2019/ticket-info.png" alt="">
                            </div>

                            <div class="text-center">
                                <a class="btn btn-danger disabled" id="buyButton" datetime="Mar 31 2019 00:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/unity4peace/booking" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Price inclusive of Ticket Fee and Payment Charges.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });


    // Modal Announcement
	//$(document).ready(function(){
    //  $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. Special Limited offer at AirAsia RedTix only</p>
                        <p>2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection