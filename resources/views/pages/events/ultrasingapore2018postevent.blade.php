{{-- route(@extends('master') --}}
@extends('masterforultra')
@section('title')
    Ultra Singapore 2018
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="css/custom2.css">
    <link type="text/css" rel="stylesheet" href="css/clock.css">
    <link type="text/css" rel="stylesheet" href="/css/custom/ultra2018.css">
    <link type="text/css" rel="stylesheet" href="css/timeTo.css">

    <!-- Banner Section -->
    {{-- <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/ultrasingapore2018/ultra-banner-mock.jpg')}}" style="width: 100%" class="img-responsive" alt="ultrasingapore2018"><a href="#usgIntroduction"><img src="images/ultrasingapore2018/arrow.png" class="center-block arrow img-responsive"></a></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/ultrasingapore2018/mobile-splash-3.jpg')}}" style="width: 100%" class="img-responsive" alt="ultrasingapore2018">
            <a href="#usgIntroduction"><img src="images/ultrasingapore2018/arrow.png" class="center-block arrow-mobile img-responsive"></a>
        </div>
    </section> --}}

	<section class="pageCategory-section section-black background-ultra">
        <div class="col-md-12 col-lg-12 sect-logo hidden-xs hidden-sm">
            <div class=logo-block>
                <div class="col-md-12 col-lg-12 ultra-logo">
                    <img src="/images/ultrasingapore2018/ultra-logo-white.png" class="img-responsive">
                </div>
                <div class="col-md-12 col-lg-12 ultra-date">
                    <img src="/images/ultrasingapore2018/ultra-event-date-2.png" class="img-responsive">
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 sect-count hidden-xs hidden-sm">
			<div class="col-md-6 col-lg-6"> 
                <div class="countdown-block">
                    <h2 class="text-center text-white open-sans h2-count">Coming up Ultra Singapore 2019 | Event Starts in :</h2>
                    <div class="text-center" id="countdown"></div>
                </div>
			</div>
			<div class="col-md-6 col-lg-6 text-center btn-wrapper"> 
                <h2 class="text-center text-white open-sans h2-count">TIER 3 TICKETS : <font style="color:red;">SELLING FAST</font></h2>
				<a class="btn ultraBuyBtn center-block" href="/ultrasingapore2019" role="button">Buy Ultra SG 2019 Tickets</a>
			</div>
        </div>
        
        <div class="col-xs-12 col-sm-12 sect-count hidden-md hidden-lg">
			<div class="col-xs-12 col-sm-12"> 
                <div class="countdown-block">
                    <h2 class="text-center text-white open-sans h2-count">Coming up Ultra Singapore 2019 Event Starts in :</h2>
				    <div class="text-center" id="countdown2"></div>
                </div>
            </div>
			<div class="col-xs-12 col-sm-12 text-center btn-wrapper">
                <h2 class="text-center text-white open-sans h2-count-1">TIER 3 TICKETS : <font style="color:red;">SELLING FAST</font></h2>
				<a class="btn ultraBuyBtn center-block" href="/ultrasingapore2019" role="button">Buy Ultra SG 2019 Tickets</a>
			</div>
		</div>

	</section>
    <!-- /Banner Section -->


    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white" style="padding-top: 0px;">





            <section class="pageCategory-section section-black last">
                <div class="container tixPrice" id="usgIntroduction">
                <div class="row">
                       {{--  <div class="col-sm-offset-1 col-sm-10" style="text-align:center;">
                            <h6 style="color:#fff;">Ultra Singapore 2018</h6>
                            <p>Friday, 15 June 2018 - Saturday, 16 June 2018</p>
                            <p>Ultra Park, 1 Bayfront Avenue, Singapore</p>
                            <br>
                        </div> --}}
                    </div>
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10">
                            <!-- <img class="whiteLogo center-block img-responsive" src="images/ultrasingapore2018/logo-3.png"> -->
                            <h6 class="text-center" style="margin-top: 40px; color: #fff;">ESCAPE THE ORDINARY. BE ULTRA.</h6>
                            <p>The music festival that has been taking the whole world by storm is landing at Singapore. This will be Ultra’s 3rd consecutive year partying at the Lion City. Hordes of ‘Ultranauts’ will travel from all over the globe to enjoy some world-class entertainment and soak up the ever stunning destination while they’re at it. Be ready for 2 days of explosive energy from the people, to the DJs on stage.</p>
                            <p>Wave your national flag and tell 45,000 revellers which part of the earth are you from. Enjoy the best of EDM from the melting pot of culture for Ultra. Leave all your worries behind when the beat drops, and enjoy the unbelievable energy from the Ultra family.</p>
                            <p>Ultra is the biggest party in the world, and you Ultranauts are the stars. Lights will guide you, music will revive you and fireworks will captivate you at beautiful Singapore with Ultra.</p>
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/oN9uCVPBB3Q" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pageCategory-section-alt text-center section-black-alt last">
                <div class="col-sm-12 text-center section-black-alt">
                <div class="row text-center section-black-alt">
                        {{-- desktop --}}
                    <div class="col-xs-12 col-md-12 col-sm-12 hidden-xs hidden-md hidden-sm">
                        <h1><strong>Ultra Singapore 2018 Lineup</strong></h1>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-above-beyond.jpg" style="width:auto; height: auto;">
                        <p>Above &amp; Beyond</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-afrojack.jpg" style="width:auto; height: auto;">
                        <p>Afrojack</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-aly-fila.jpg" style="width:auto; height: auto;">
                        <p>Aly &amp; Fila</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-andrewrayel.jpg" style="width:auto; height: auto;">
                        <p>Andrew Rayel</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-cesqeaux.jpg" style="width:auto; height: auto;">
                        <p>Cesqeaux</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-djsnake.jpg" style="width:auto; height: auto;">
                        <p>DJ Snake</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-feddelegrand.jpg" style="width:auto; height: auto;">
                        <p>Fedde Le Grand</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-ggmagree.jpg" style="width:auto; height: auto;">
                        <p>GG Magree</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-illenium.jpg" style="width:auto; height: auto;">
                        <p>Illenium</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-joyride.jpg" style="width:auto; height: auto;">
                        <p>JOYRYDE</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-kriskrosamsterdam.jpg" style="width:auto; height: auto;">
                        <p>Kris Kross Amsterdam</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-mykris.jpg" style="width:auto; height: auto;">
                        <p>Mykris</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-myrne.jpg" style="width:auto; height: auto;">
                        <p>MYRNE</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-nervo.jpg" style="width:auto; height: auto;">
                        <p>NERVO</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-gudvibrations.jpg" style="width:auto; height: auto;">
                        <p>NGHTMRE + SLANDER present Gud Vibrations</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-nickyromero.jpg" style="width:auto; height: auto;">
                        <p>Nicky Romero</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-raverepublic.jpg" style="width:auto; height: auto;">
                        <p>Rave Republic</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-rlgrime.jpg" style="width:auto; height: auto;">
                        <p>RL Grime</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-steveangello.jpg" style="width:auto; height: auto;">
                        <p>Steve Angello</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-sunneryjames-ryanmarciano.jpg" style="width:auto; height: auto;">
                        <p>Sunnery James &amp; Ryan Marciano</p>
                    </div>
                    <div class="col-xs-2 col-md-2 col-sm-2 hidden-xs hidden-md hidden-sm">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-ummetozcan.jpg" style="width:auto; height: auto;">
                        <p>Ummet Ozcan</p>
                    </div>
                </div>
                    {{-- Mobile --}}
                <div class="row text-center section-black-alt">
                    <div class="col-xs-12 col-sm-12 hidden-lg hidden-md">
                        <h1><strong>DJ Line Ups</strong></h1>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-above-beyond.jpg" style="width:auto; height: auto;">
                        <p>Above &amp; Beyond</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-afrojack.jpg" style="width:auto; height: auto;">
                        <p>Afrojack</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-aly-fila.jpg" style="width:auto; height: auto;">
                        <p>Aly &amp; Fila</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-andrewrayel.jpg" style="width:auto; height: auto;">
                        <p>Andrew Rayel</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-cesqeaux.jpg" style="width:auto; height: auto;">
                        <p>Cesqeaux</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-djsnake.jpg" style="width:auto; height: auto;">
                        <p>DJ Snake</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-feddelegrand.jpg" style="width:auto; height: auto;">
                        <p>Fedde Le Grand</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-ggmagree.jpg" style="width:auto; height: auto;">
                        <p>GG Magree</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-illenium.jpg" style="width:auto; height: auto;">
                        <p>Illenium</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-joyride.jpg" style="width:auto; height: auto;">
                        <p>JOYRYDE</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-kriskrosamsterdam.jpg" style="width:auto; height: auto;">
                        <p>Kris Kross Amsterdam</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-mykris.jpg" style="width:auto; height: auto;">
                        <p>Mykris</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-myrne.jpg" style="width:auto; height: auto;">
                        <p>MYRNE</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-nervo.jpg" style="width:auto; height: auto;">
                        <p>NERVO</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-gudvibrations.jpg" style="width:auto; height: auto;">
                        <p>NGHTMRE + SLANDER present Gud Vibrations</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-nickyromero.jpg" style="width:auto; height: auto;">
                        <p>Nicky Romero</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-raverepublic.jpg" style="width:auto; height: auto;">
                        <p>Rave Republic</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-rlgrime.jpg" style="width:auto; height: auto;">
                        <p>RL Grime</p>
                    </div>
                </div>
                <div class="row text-center section-black-alt">
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-steveangello.jpg" style="width:auto; height: auto;">
                        <p>Steve Angello</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-sunneryjames-ryanmarciano.jpg" style="width:auto; height: auto;">
                        <p>Sunnery James &amp; Ryan Marciano</p>
                    </div>
                    <div class="col-xs-4 col-sm-4 hidden-lg hidden-md">
                        <img class="img-responsive" src="images/ultrasingapore2018/dj/dj-ummetozcan.jpg" style="width:auto; height: auto;">
                        <p>Ummet Ozcan</p>
                    </div>
                </div>
                </div>
            </section>



            {{-- <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                           <img class="longLogo center-block img-responsive" src="images/ultrasingapore2018/logo-1.png">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>LATEST NEWS</strong></h1>    
                            </div>

                            <div class="col-sm-12 col-xs-12">
                                
                                <div class="col-sm-4 col-xs-12 cardWrapper">
                                    <div class="card center-block">
                                        <img class="card-img-top img-responsive" src="images/ultrasingapore2018/card1.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <h6 class="card-title">ULTRA SINGAPORE: REMIXED INTO THE WEEKEND</h6>
                                            <p class="card-text">The second edition of Ultra Singapore takes place this weekend! Prepare for the event with these official …</p>
                                            <a href="https://ultramusicfestival.com/worldwide/prepare-ultra-singapore-weekend-apple-music-playlists/" class="readMore">Read More <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                                   
                                <div class="col-sm-4 col-xs-12 cardWrapper">
                                    <div class="card center-block">
                                        <img class="card-img-top img-responsive" src="images/ultrasingapore2018/card2.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <h6 class="card-title">ULTRA MUSIC FESTIVAL’S TWENTIETH ANNIVERSARY</h6>
                                            <p class="card-text">Having welcomed over one million fans to forty-five events internationally in 2017, the world’s largest …</p>
                                            <a href="https://ultrasingapore.com/worldwide/ultra-music-festivals-twentieth-anniversary/" class="readMore">Read More <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-xs-12 cardWrapper">
                                    <div class="card center-block">
                                        <img class="card-img-top img-responsive" src="images/ultrasingapore2018/card3.jpg" alt="Card image cap">
                                        <div class="card-body">
                                            <h6 class="card-title">ULTRA SINGAPORE: OFFICIAL AFTER MOVIE 2016</h6>
                                            <p class="card-text">A 2 day event boasting the world's top EDM DJ's with unparalleled stage designs and top tier production …</p>
                                            <a href="https://umfworldwide.com/media/umf-films/" class="readMore">Read More <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="col-sm-12 col-xs-12" style="padding-top: 40px;">
                                <a class="btn ultraBtn center-block" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="https://ultrasingapore.com/news/">MORE NEWS</a>   
                            </div>

                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section> --}}
           {{--  <section class="pageCategory-section last">
                <img class="img-responsive" src="images/ultrasingapore2018/sponsor.jpg" style="width: 100%; height: auto;">
            </section> --}}

            <section class="pageCategory-section last">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                           {{-- <img class="longLogo center-block img-responsive" src="images/ultrasingapore2018/logo-1.png" style="margin-top: 70px;"> --}}
                            <div class="text-center col-sm-12 col-xs-12">
                                <h1 class="subSecTitle"><strong>CONCERT GALLERY</strong></h1>
                            </div>
                            <div class="row hidden-xs">
                                <div class="col-sm-12 text-center pd-0">
                                    <div class="col-sm-8 pd-0">
                                        <div class="col-sm-6 collageImg">
                                            <a href="images/ultrasingapore2018/gallery1-1.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery1-1.png" alt=""></a>
                                        </div>
                                        <div class="col-sm-6 collageImg">
                                            <a href="images/ultrasingapore2018/gallery1-2.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery1-2.png" alt=""></a>
                                        </div>
                                        <div class="col-sm-12 collageImg">
                                            <a href="images/ultrasingapore2018/gallery4.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery4.png" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 pd-0">
                                        <div class="col-sm-12 collageImg">
                                            <a href="images/ultrasingapore2018/gallery3.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery3.png" alt=""></a>
                                        </div>  
                                        <div class="col-sm-12 collageImg">
                                            <a href="images/ultrasingapore2018/gallery5.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery5.png" alt=""></a>
                                        </div>  
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center pd-0">
                                    <div class="col-sm-12 pd-0">
                                        <div class="col-sm-4 collageImg">
                                            <a href="images/ultrasingapore2018/gallery6.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery6.png" alt=""></a>
                                        </div>
                                        <div class="col-sm-8 collageImg">
                                            <a href="images/ultrasingapore2018/gallery7.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery7.png" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="gallery text-center hidden-lg hidden-md hidden-sm">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery1-1.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery1-1.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery1-2.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery1-2.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery4.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery4.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery3.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery3.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery5.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery5.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery6.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery6.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery7.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery7.png" alt=""></a>
                                        </div>
                                    </div>

                                    <div class="swiper-pagination"></div>

                                    <div class="swiper-button-next swiper-button-white"></div>
                                    <div class="swiper-button-prev swiper-button-white"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>


            
             <section class="pageCategory-section last bottom">
                <img src="images/ultrasingapore2018/white-logo1.png" class="img-responsive logoBottom hidden-xs"> 
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="note text-left">
                                {{-- <h4>please contact us at <a href="mailto:ultra@airasiaredtix.com">ultra@airasiaredtix.com</a> if you have any further questions for custom packages</h4> --}}
                                {{-- <h4>Don't have the Card? Get your DBS Live Fresh Card here: <a href="http://dbs.com.sg/livefresh">dbs.com.sg/livefresh</h4>
                                <h4>Tickets go on sale on Tues 20th Feb. Don't miss out, <a href="https://ultrasingapore.com/">register</a> now to get access to tier 1 tickets!</h4> --}}
                                {{-- <h4>Frequently Asked Questions:</h4>
                                <p><a href="https://intercom.help/airasia-redtix/your-guide-to-ultra-singapore-2018">Click here for more information</a></p> --}}
                                +                                <h4>please contact us at <a href="mailto:ultra@airasiaredtix.com">ultra@airasiaredtix.com</a><br> if you have any further questions for custom packages</h4>
                            </div>
                            <div class="note text-left">
                                <h4>For enquiry only:</h4>
                                <p>Email to <a href="mailto:ultra@airasiaredtix.com">ultra@airasiaredtix.com</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="center-block text-center">
                        <div class="faq center-block">
                            {{-- FAQ button --}}
                            <img src="images/ultrasingapore2018/question.svg" style="width: 30px;"> Need more info? <a class="tixOptBtn" href="https://intercom.help/airasia-redtix/your-guide-to-ultra-singapore-2018">FAQ</a>
                        </div>
                    </div>
                </div>
            </section>
 
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')
    <script type="text/javascript">
        $(document).ready(function() {

        //Get hash from URL. Ex. index.html#Chapter1
        var hash = location.hash;

        if (hash == '#getflight') {
            //Do stuff here
            //alert("Hello");
            location.href = "#myticket";
            $(function() {
                $('[href="#ticket_flight"]').tab('show');
            });
        }
    });
    </script>

	{{-- countdown --}}
	<script src="js/jquery.time-to.js"></script>
	<script type="text/javascript">
		$('#countdown').timeTo({
		    timeTo: new Date(new Date('Saturday June 8 2019 12:00:00 GMT+0800 (+08)')),
		    displayDays: 2,
		    theme: "white",
		    displayCaptions: true,
		    fontSize: 36,
		    captionSize: 12
		}); 
		$('#countdown2').timeTo({
		    timeTo: new Date(new Date('Saturday June 8 2019 12:00:00 GMT+0800 (+08)')),
		    displayDays: 2,
		    theme: "white",
		    displayCaptions: true,
		    fontSize: 36,
		    captionSize: 12
		}); 
	</script>
	{{-- /countdown --}}

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });
    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for anchor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

/*    $(function() {
        $(".buy_form").submit(function(){
            alert("am here");
            return false;
        });
    })
*/
    </script>


    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">
        $('.flightTab').hide();
        //show the first tab content
        $('#country-1').show();

        $('#select-country').change(function () {
           dropdown = $('#select-country').val();
          //first hide all tabs again when a new option is selected
          $('.flightTab').hide();
          //then show the tab content of whatever option value was selected
          $('#' + "country-" + dropdown).show();                                    
        });
    </script>
    @if(!empty(Session::get('error_code')) && Session::get('error_code') == 11)
        <script>
        $(function() {
            $('#errorModalRegistered').modal('show');
        });
        </script>
    @endif

    @if(!empty(Session::get('error_code')) && Session::get('error_code') == 12)
        <script>
        $(function() {
            $('#failModalRegistered').modal('show');
        });
        </script>
    @endif

    @if(!empty(Session::get('error_code')) && Session::get('error_code') == 13)
        <script>
        $(function() {
            $('#errorModalUsedPassword').modal('show');
        });
        </script>
    @endif

    @if(!empty(Session::get('error_code')) && Session::get('error_code') == 5)
        <script>
        $(function() {
            $('#errorModal').modal('show');
        });
        </script>
    @endif
    @if(!empty(Session::get('error_code')) && Session::get('error_code') == 6)
        <script>
        $(function() {
            $('#errorModal2').modal('show');
        });
        </script>
    @endif
    @if(!empty(Session::get('success_lf')))
        <script>
        $(function() {
            $('#successlfModal{{ session()->get('ticket_id') }}').modal('show');
        });
        </script>
    @endif

    @if(!empty(Session::get('success_dbs')))
        <script>
        $(function() {
            $('#successdbsModal{{ session()->get('ticket_id') }}').modal('show');
        });
        </script>
    @endif
    
    @if(!empty(Session::get('success_registered')))
        <script>
        $(function() {
            $('#successModalRegistered{{ session()->get('ticket_id') }}').modal('show');
        });
        </script>
    @endif

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')

@endsection
