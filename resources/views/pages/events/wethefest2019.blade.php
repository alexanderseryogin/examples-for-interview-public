@extends('master')
@section('title')
    We The Fest 2019
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="We The Fest 2019" />
    <meta property="og:description" content="We The Fest 2019" />
    <meta property="og:image" content="{{ Request::Url().'images/wethefest2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/wethefest2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="We The Fest 2019"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/wethefest2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="We The Fest 2019">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>We The Fest 2019</h6>Tickets from <span>IDR 1,750,000</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                        <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  19 - 21 July 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> JIEXPO, Jakarta <a target="_blank" href="https://goo.gl/maps/enb6vjQa43LyLfda8">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 3.00pm - 1.00am</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <p><h2>We The Fest 2019</h2><br/>
                                <p>
                                We The Fest is an annual summer festival of music, arts, fashion and food taking place in Indonesia's capital of Jakarta. Since its inaugural edition in 2014, the festival has seen incredible performances from globally-known acts of different genres including The Kooks, Dua Lipa, James Bay, SZA, Miguel, Ellie Goulding, Big Sean, The 1975, G-Eazy, Mark Ronson, Phoenix, Macklemore & Ryan Lewis, CL, The Temper Trap, Purity Ring, Flight Facilities, Jessie Ware and many more. Indonesia's most exciting musical acts have also performed at the festival including Potret, NAIF, Scaller, Barasuara, Sheila on 7, Raisa, The Trees and the Wild, Ramengvrl, Elephant Kind, and Stars & Rabbit amongst many.
                                </p>
                                <p>
                                In 2018, the fifth edition of We The Fest took place on the grounds of JiExpo Kemayoran at the heart of Jakarta, and was a wonderful home to over 60,000 punters from over 30 countries. Described as ""a classy festival"" by Vice for its friendly vibe and how slickly it has been organized, the festival is a pioneer of its kind in the Southeast Asian festival scene with elements such as arts, fashion and food presented through various whimsical activations and zones festival-goers can experience and explore.
                                </p>
                                
                                <p>We The Fest is returning for its sixth edition on 19, 20 & 21 July 2019.</p>

                            </div>
                            {{-- <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/artist/4BxCuXFJrSWGi1KHcVqaU4" width="300" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div>--}}
                        </div>
                        {{-- <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/qWT_W3MDFmk?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <img class="image-responsive" src="images/wethefest2019/gallery-1x.jpg" style="width:100%; height:auto;" alt="">
                        </div>
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/wethefest2019/poster.jpg" data-featherlight="image"><img class="" src="images/wethefest2019/poster.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/wethefest2019/gallery1.jpg" data-featherlight="image"><img class="" src="images/wethefest2019/gallery1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            {{-- <div class="text-center">
                                <h1 class="subSecTitle"><strong>Buy 2 tickets for a special discounted price. Choices available for match 1,2 or 3 in CAT 2, 3, 4 and 5. Limited time only.</strong></h1>
                            </div> --}}
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Day</th>                                            
                                            <th>GA</th>
                                            <th>VIB</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>                                        
                                        <tr>
                                            <td>3 Days Pass</td>
                                            <td>IDR 1,750,000</td>
                                            <td>IDR 4,000,000</td>                                            
                                       	</tr>           
                                </table>
                            </div>

                             <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Jul 14 2019 00:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/wethefest2019/booking" role="button">BUY TICKETS</a>
                                <br/><br/><i style="font-size:11px">*Purchased tickets cannot be exchanged, refunded or cancelled.</i>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>  --}}
                            </div>


                            {{-- <span class="importantNote">*Additional charges will be applied for credit/debit card transaction, optionally customer may pay cash for outlet purchase to avoid the charges.</span> --}}
                            <div class="note text-left">
                                <h2>Age Restrictions</h2>
                                <ul>
                                    <li>This is an all-ages festival. Children under the age of 5 are free of admission.</li>
                                    <li>Guests must show valid photo ID (National Identification Card or KTP/Driving License/Passport/Student ID) to enter the festival.</li>
                                    <li>Guests without valid photo ID will be denied admission without refund.</li>
                                    <li>We strongly advise guests under the age of 12 to be accompanied by a responsible adult aged 18 or older. </li>
                                </ul>

                                <h2>General Policies</h2>
                                <ul>
                                    <li>No unauthorized vending allowed.</li>
                                    <li>No unauthorized solicitations, handbills, sampling or giveaways.</li>
                                    <li>No camping or overnight attendance.</li>
                                    <li>Alcohol purchase is strictly for festival-goers who are at least 21 years old.</li>
                                    <li>All customers must exchange their e-vouchers to wristbands before entering the venue.</li>
                                    <li>You must wear your wristband at all time on the venue. Don’t lose it! Those who lose their wristband for any reason will not be allowed entry into the event.</li>
                                    <li>Any person within the event not in possession of a valid wristband, will be removed. There are no exceptions.</li>
                                    <li>Always bring your valid ID while at the event.</li>
                                    <li>The organizers and promoters are not responsible for tickets that are purchased from any third-party entities/platforms/places that are not registered as an official ticket box partner of We The Fest 2018.</li>
                                    <li>Artists are subject to change or withdrawal. In the event of such change or withdrawal, refunds will not be given.</li>
                                    <li>In certain circumstances some areas within the venue may become restricted.</li>
                                    <li>The organizers, promoters, and artists are not liable for any compensation and/or cancellation charges for travel expenses resulting from the event being cancelled or postponed.</li>
                                    <li>Guests who are behaving in a disorderly, offensive or inappropriate manner, and guests who refuse to obey instructions/warnings from event staff will be removed from the festival immediately. No refunds will be given.</li>
                                    <li>Any attempt to enter the event without a valid ticket or valid credentials will result in the immediate removal from event grounds. In certain cases we will contact police and press charges against the violators.</li>
                                    <li>The event operators, organizers, promoters, and artists cannot be held responsible for lost, stolen or damaged possessions or accidents resulting in injury occurring anywhere in the venue during the event regardless of fault or reason.</li>
                                    <li>We will refuse entry to guests who are severely intoxicated, clearly affected by drugs or deemed un t for entry. Anyone found carrying these items will be reported to the police immediately. No refunds will be given. Guests found to possess illegal substances, weapons and/or other prohibited items will also be refused entry.</li>
                                    <li>Any outside food or beverage is strictly prohibited inside the event.</li>
                                    <li>The organizer will not use or disclose guests’ personal information collected at the time of ticket purchase without consent.</li>
                                    <li>We do not appreciate shirtless people and visitors who take their shirts off will be asked to put them back on.</li>
                                    <li>Guests who are removed from the venue for any reasons will be banned from re-entering the event.</li>
                                    <li>The event is intended to be a friendly and enjoyable event.</li>
                                </ul>
                                <h2>Security Entry</h2>
                                <p>All attendees are subject to being searched prior to entry into the event, including members of the media. By requesting entry into the event, all attendees shall be subject to a thorough airport-style pat down search, including emptying pockets and bags, and having your items examined. Electronic drug detector and K-9 search will be used. Event organizer shall, at all times, reserve the right to either deny any person entry into or to eject any person from the event, who in the sole and absolute discretion of event organizer is in violation of event rules, which rules may be updated from time to time, with or without notice.</p>
                                <h2>Recording Rights</h2>
                                <p>Ismaya Live and its affiliations/association reserves the right to film, record, and/or photograph the visitor, participant or wristband holder's image, likeness, action, and/or statement of this event for television, moving picture, webcast and/or other public broadcast in any medium for any purpose. Any visitor, participant and/or wristband holder of this show hereby fully releases and discharges Ismaya Live (including its employees and subsidiaries) and its affiliations/association from any rights, claims, damages, losses, liabilities, fines and causes of action whatsoever arising directly or indirectly from the filming, recording and/or photography of this event.</p>

                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown include RM4.00 AirAsiaRedTix fee & other taxes.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM30.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li> --}}
                                    <li>Prices shown exclude IDR90,000 AirAsiaRedTix fee.</li>
                                    <!-- <li>Strictly no replacement for missing tickets, torn tickets and cancellation.</li> -->
                                    <li>Online ticket selling will close 5 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection