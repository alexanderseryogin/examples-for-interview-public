@extends('master')
@section('title')
    BARNEY'S GREATEST HITS LIVE IN MALAYSIA
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/barney/barney-banner.jpg')">
      </div>  
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>BARNEY'S GREATEST HITS LIVE IN MALAYSIA</h6>
                  Tickets from <span>RM78.80</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 7 - 9 July 2017 <a class="label label-warning" data-toggle="modal" data-target="#announcementModal">Read Announcement</a></div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Putrajaya International Convention Centre<a target="_blank" href="https://goo.gl/maps/GCdSDeSntsJ2"> View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i>  5.00 PM</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <p>Hey Barney fans! It’s time to sing and dance with the purple dinosaur, Barney and his friends Baby Bop, BJ and Riff, in this high-energy song and dance live on stage. To celebrate Barney’s love and care over 25 years, a brand new musical with many of Barney’s popular and favorite songs all in one show. Kids, let’s raise up your hands, shake your body, move your feet and sing-along with Barney in this super-dee-duper musical show.</p>
                <p>Barney leads the little audiences to the imaginative adventure tour. They go to the Musical Castle and meet the King; then they travel to outer space to visit the purple planet. Finally Barney, BJ, Baby Bop and Riff present the top hits and there will be plenty of fun for everyone!</p>
                <p>Barney's Greatest Hits will start performing at Putrajaya International Convention Centre in Malaysia on July 7, 2017 until July 9, 2017</p>
                <p>See you soon.</p>
                </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/barney/barney-poster.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/barney/barney-banner.jpg" alt="">
                  </div>
                  
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Ticket Category</th>
                          <th>Normal Price</th>
                          <th>15% Off Per Ticket</th>
                          <th>Buy 2 Free 1 Promotion<br>(Please select 3 tickets)</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody> 
                          <tr>
                              <td><span class="box purple">VIP / MOSH PIT</span>(Mosh Pit: Strictly for kids below 12 only)</td>
                              <td>RM 328</td>
                              <td>RM 282.80</td>
                              <td>RM 668</td>
                              <td rowspan="4"><img class="img-responsive seatPlanImg" src="images/barney/barney-seatplan.jpg" alt=""></td>
                          </tr>
                          <tr>
                              <td><span class="box yellow">GOLD</span></td>
                              <td>RM 268</td>
                              <td>RM 231.80</td>
                              <td>RM 548</td>
                          </tr>
                          <tr>
                              <td><span class="box green">SILVER</span></td>
                              <td>RM 168</td>
                              <td>RM 146.80</td>
                              <td>RM 348</td>
                          </tr>
                          <tr>
                              <td><span class="box orange">BRONZE</span></td>
                              <td>RM 88</td>
                              <td>RM 78.80</td>
                              <td>RM 188</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/barney's greatest hit live in malaysia/events" role="button">BUY TICKETS</a>
                  <!--<span class="or">/</span>                 
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>-->
                </div>                  
                <span class="importantNote">* Prices shown include RM4.00 AirAsiaRedTix fee.</span>
                <div class="note text-left">
                  <h2>Important Notes</h2>
                  <ol>
                    <li>Prices shown inclusive RM4.00 AirAsiaRedTix fee.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                  </ol>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('modal')

  <!--Modal Announcement-->
  <div id="announcementModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
            <h4>CANCELLATION OF FRIDAY'S SHOWS ANNOUNCEMENT</h4>
            <div class="clearfix">&nbsp;</div>
            <div class="well">
            <p>We are regret announce that due to unforeseen technical difficulties, our Friday's 5pm & 8pm shows have to be cancelled. Kindly be informed however that all tickets purchased for the said shows will be automatically upgraded to Sat/Sun show under a better seat category. Our Customer Service personnel will contact the respective customers soonest possible to arrange for the said upgrade or refund (if necessary). Whereas, All shows scheduled for Saturday and Sunday remains unchanged. We apologize for any inconvenience caused.</p>
            </div>
        </div>
      </div>
    </div>
  </div>
  
  <!--Modal Seat Plan image-->
  <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <div class="clearfix">&nbsp;</div>
          <img src="" class="enlargeImageModalSource" style="width: 100%;">
          </div>
      </div>
      </div>
  </div>
  
  <!-- Modal Where to Get Tix Location-->
  <div id="modalGetTixLoc" class="modal fade" role="dialog">
      <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <h2>GET YOUR TICKETS FROM:</h2>
          <dl class="dl-horizontal">
              <dt>Online:</dt>
              <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Rock Corner outlets:</dt>
              <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
              <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
              <dd>Bangsar Village (TEL: 03-22021139)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Victoria Music outlets:</dt>
              <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
              <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
              <dd>Tropicana</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Penang outlets:</dt>
              <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
              <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Istana Budaya Jalan<br>Tun Razak:</dt>
              <dd>03 40265558</dd>
          </dl>
          </div>
      </div>
      </div>
  </div>

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
    $(document).ready(function(){
      $("#announcementModal").modal('show');
    });
    </script>
    
@endsection