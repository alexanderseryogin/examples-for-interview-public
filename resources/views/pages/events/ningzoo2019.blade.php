@extends('master')
@section('title')
    Ning LIVE @ ZOO NEGARA
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Ning LIVE @ ZOO NEGARA" />
    <meta property="og:description" content="Malaysia's Queen of Soul and multi award-winning pop singer and actress Ning Baizura, will be performing in this intimate concert at Zoo Negara."/>
    <meta property="og:image" content="{{ Request::Url().'images/ningzoo2019/thumbnail1.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/ningzoo2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Ning LIVE @ ZOO NEGARA"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/ningzoo2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Ning LIVE @ ZOO NEGARA">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Ning LIVE @ ZOO NEGARA</h6> Tickets <span>RM100</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  20th April 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Show Amphitheatre - Zoo Negara, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/FZYKo9225dE2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Saturday (12.00 pm - 1.00pm)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>Ning LIVE @ ZOO NEGARA </h2><br/>
                            Malaysia's Queen of Soul and multi award-winning pop singer and actress Ning Baizura, will be performing in this intimate concert at Zoo Negara. Ning LIVE! is a friendship project in aid of MYCAT (Malaysian Conservation Alliance For Tigers), joining Malaysia in the race against time to save the last remaining Malayan Tigers from extinction. This very special concert features an exclusive collaboration with the zoo's very colorful animal show! Ning is well-loved and famous for her live stage performances so don't miss this rare public performance. As a treat for her fans, there will be an autograph and photography session with Ning at the Elephant and Tiger sections after the concert. Jom, bermesra dengan Ning!</p>
                            <p>Album:<br/>
                            1993 – Dekat Padamu (Sony Music)<br/>
                            1994 – Ning (Sony Music)<br/>
                            1995 – Teguh (BMG)<br/>
                            1997 – Ke Sayup Bintang (BMG)<br/>
                            1997 – Always (BMG)<br/>
                            1999 – Pujaan Ku (BMG)<br/>
                            2001 – Natural Woman (AMS Records,Japan)<br/>
                            2003 – Selagi Ada... (Warner)<br/>
                            2004 – Erti Petermuan (Warner)<br/>
                            2006 – 3113 (Greatest Hits Compilation)(Warner & SonyBMG)<br/>
                            2006 – Drama (Featuring Nikki & Yanie) CD Single (Warner Music)<br/>
                            2008 – EastToWest<br/>
                            2011 – 3 Suara (with Jaclyn Victor & Shila Amzah)<br/>
                            2011 – Dewa<br/>
                            2013 – Kekal</p>

                            <p>Awards:<br/>
                            1991 – Voice of Asia<br/>
                            1991 – Best Artiste Development Award<br/>
                            1993 – AIM Awards: Best New Artiste<br/>
                            1993 — AIM Awards: Album of the Year (Dekat Padamu)<br/>
                            1993 — Anugerah Juara Lagu: Best Song – Ballad Category (Curiga)<br/>
                            1994 — AIM Awards: Best Pop Album (Ning)<br/>
                            1994 — Anugerah Juara Lagu: Best Song – Pop/Rock Category (Kau & Aku), award as lyricist<br/>
                            2003 — Anugerah ERA: Choice Female Vocalist<br/>
                            2004 — AIM Awards: Song of The Year (Selagi Ada Cinta)<br/>
                            2005 — AIM Awards: Best Album Cover (Erti Pertemuan)<br/>
                            2005 — AIM Awards: Best Music Video (Awan Yang Terpilu)<br/>
                            2005 — AIM Awards: Song of The Year (Awan Yang Terpilu)<br/>
                            2005 — AIM Awards: Best Pop Album (Erti Pertemuan)<br/>
                            2008 — VOIZE Favourite Local Act Award<br/>
                            2008 — Cosmopolitan Malaysia magazine’s Fun, Fearless and Fabulous (FFF) Award 2008 – Singer category<br/>
                            2011 — Anugerah Planet Muzik: Best Duo/Group (award for '3 Suara' with Jaclyn Victor and Shila Hamzah)<br/>
                            2011 — AIM Awards: Best Vocal Performance In A Song Duo/Group – (Beribu Sesalan, award for '3 Suara' with Jaclyn Victor and Shila Hamzah)<br/>
                            2011 — Anugerah Juara Lagu: Best Song (Runner-up) – (Beribu Sesalan, award for '3 Suara' with Jaclyn Victor and Shila Hamzah)<br/>
                            2012 — Anugerah Bintang Popular Berita Harian: Most Popular Duo/Group (award for '3 Suara' with Jaclyn Victor and Shila Hamzah)<br/>
                            2015 — Brandlaureate Awards: Country Branding award<br/>
                            2015 — Global Leadership Awards: Excellence is Entertainment award<br/>
                            2015 — Global Branding Awards: Global Fashion Icon<br/>
                            2017 — Anugerah Personaliti Industri & Usahawan Malaysia: Malaysia Music Icon award<br/>
                            2017 — McMillan Woods Global Awards Night: Jazz Diva of the Year</p>
                        </div>
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/-I2y3co1JKU?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 text-center leftBar">
                        <div class="row">
                        <div class="col-sm-6 leftBox">
                            <img src="{{asset('images/ningzoo2019/my-cat.jpg')}}" style="width: 100%" class="img-responsive" alt="MY CAT">
                        </div>
                        <div class="col-sm-6 text-center">
                            <img src="{{asset('images/ningzoo2019/zoo-negara.jpg')}}" style="width: 100%" class="img-responsive" alt="Ning LIVE @ ZOO NEGARA">
                        </div>
                        </div>
                    </div>

                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/ningzoo2019/gallery-.jpg" data-featherlight="image"><img class="" src="images/ningzoo2019/gallery-.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Adult</td>
                                            <td>RM175</td>
                                        </tr>
                                        <tr>
                                            <td>Kids<br/>(3-12 years old)</td>
                                            <td>RM100</td>
                                        </tr>
                                        <tr>
                                            <td>Family Package<br/>(2 Adults &amp; 2 Kids)</td>
                                            <td>RM505</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Apr 19 2019 00:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/ningliveatzoonegara/booking" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Ticket including Zoo Negara entrance fees (allowed to enter the zoo by 9.30am).</li>
                                    <li>Kids below 3 years old are free of charge (FOC).</li>
                                    <li>You are required to present MYKid identity card before enter to the zoo.</li>
                                    <li>This event is not number seated, seat availability will be based on first come first serve basis.</li>
                                    <li>Prices shown include AirAsiaRedTix ticket fee and processing fees.  </li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	//$(document).ready(function(){
    //    $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

    <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Temporarily down for Maintenance</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>We are performing scheduled maintenance. We should be back online shortly</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection