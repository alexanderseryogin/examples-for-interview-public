@extends('master')
@section('title')
    Rainforest World Music Festival 2018
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Rainforest World Music Festival 2018" />
    <meta property="og:description" content="Rainforest World Music Festival 2018" />
    <meta property="og:image" content="{{ Request::Url().'images/rainforest2018/thumbnail2.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/rainforest2018/web-banner1.jpg')}}" style="width: 100%" class="img-responsive" alt="Aladdin & The Magic Lamp"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/rainforest2018/thumbnail2.jpg')}}" style="width: 100%" class="img-responsive" alt="Aladdin & The Magic Lamp">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Rainforest World Music Festival 2018</h6>Tickets from <span>RM110</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  13th - 15th July 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Sarawak Cultural Village <a target="_blank" href="https://goo.gl/maps/DKH7ooeAAwo"> View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 10.00 am - 12.30 am </div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p>Established in 1998 with an audience of only 300 people, the Rainforest World Music Festival is now a major annual event in the State’s music calendar, eagerly anticipated by a crowd of over 20,000 people who flock from overseas, interstate as well as locals. It is a family oriented event with afternoon sessions especially for children and tickets sold, also incorporates a family package.</p>
                            <p>The Festival consists of a richly diverse programme with performers from around the world at nightly shows on three outdoor stages, the well-known Jungle and Tree Stage as well as a new Big Tent Stage. Afternoon shows take place on an indoor stage, mini sessions spread over 3 afternoons, daily drum circles in the evenings. There is also a food and village mart selling an array of local, international and fusion food as well as festival memorabilia.</p>
                            <p>It also incorporates health and wellness programmes as part of the festival, with “parades” and surprise pop-up performances throughout the festival.
                            Even when the Festival stops, the party doesn’t! This year’s RWMF features a DJ Session at Escobar in Damai Central as soon as the last concert performance wraps up.</p>
                            <p>The Festival also has a record of environmental awareness as evidenced by its attention to recycling, waste management and the use of shuttle buses to reduce carbon emission.</p>
                            <p>The objective of having the event is to create an international event in support of Sarawak Tourism Board’s objectives of promoting Sarawak as the region’s premier destination which emphasize on performing arts and the preservation of culture as well as to delivering a world-class event that will generate generous revenue and profit to the business community, sponsors and individuals in and around Kuching.</p>
                            {{-- <ol>
                                <li>CUATRO MINIMAL (Mexico / Japan / Korea)</li>
                                <li>YALLAH BYE (Tunisia)</li>
                                <li>VOLOSI (Poland)</li>
                                <li>NAEDRUM (Korea)</li>
                                <li>ELISOUMA (Comoros Islands)</li>
                                <li>COMBO GINEBRA (Chile)</li>
                                <li>BHUNGAR KHAN COMPANY (India)</li>
                                <li>GAYAGAYO (Indonesia)</li>
                                <li>NITEWORKS (Isle of Skye)</li>
                                <li>GUO GAN & ALY KEITA (China + Mali)</li>
                                <li>AT ADAU (Sarawak)</li>
                                <li>ALBERTO MARIN (Spain)</li>
                                <li>AZIZA BRAHIM (Saharawi Refugee Camp)</li>
                                <li>OYME (Mordovian Republic)</li>
                                <li>KEVIN LOCKE (USA)</li>
                                <li>BALKANOPOLIS (Serbia)</li>
                                <li>SLOBODAN TRKULKA (Serbia)</li>
                                <li>GRACE NONO (Philippines)</li>
                                <li>DJELI MOUSSA CONDE (Guinea)</li>
                                <li>NARASIRATO (Solomon Islands)</li>
                                <li>THE RAGHU DIXIT PROJECT  (India)</li>
                                <li>SWARASIA MALAYSIA (Malaysia )</li>
                                <li>DONA ONETE (Brazil)</li>
                                <li>SHANREN (China)</li>
                                <li>DRUM CIRCLES (Malaysia)</li>
                                <li>CHINGAY (Penang)</li>
                                <li>OKA  (Australia)</li>
                                <li>ADRIAN  (Sarawak)</li>
                                <li>Sarawak Cultural Village (Sarawak)</li>
                            </ol>
                            <i>*Bands’ list subject to change</i> --}}
                            <p>Visit our <a href="http://rwmf.net/">Rainforest World Music Festival</a> for more information on the event. Grab your tickets before it is sold off.</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/rainforest2018/poster1.jpg" data-featherlight="image"><img class="" src="images/rainforest2018/poster1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/rainforest2018/web-banner1.jpg" data-featherlight="image"><img class="" src="images/rainforest2018/web-banner1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            {{-- <th>Early Bird<br>(till 30<sup>th</sup> April 2018)</th>
                                            <th>Pre-Sale<br>(1<sup>st</sup> May - 12<sup>th</sup> July 2018)</th> --}}
                                            <th>Door Sale<br>(13<sup>th</sup> - 15<sup>th</sup> July 2018)</th>
                                            {{-- <th>Site Map</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Adult (1 Day Pass)</td>
                                            {{-- <td>RM 110.00</td>
                                            <td>RM 123.80</td> --}}
                                            <td>RM 146.00</td>
                                            {{-- <td rowspan="6"><img class="img-responsive seatPlanImg" src="images/rainforest2018/seat-plan.jpg" alt=""></td> --}}
                                        </tr>
                                        <tr>
                                            <td>Adult (3 Day Pass)</td>
                                            {{-- <td>RM 290.20</td>
                                            <td>RM 323.80</td> --}}
                                            <td>RM 387.00</td>
                                        </tr>
                                        <tr>
                                            <td>Child (1 Day Pass)</td>
                                            {{-- <td>RM 57.00</td>
                                            <td>RM 63.80</td> --}}
                                            <td>RM 76.00</td>
                                        </tr>
                                        <tr>
                                            <td>Child (3 Day Pass)</td>
                                            {{-- <td>RM 110.00</td>
                                            <td>RM 144.00</td> --}}
                                            <td>RM 189.00</td>
                                        </tr>
                                        <tr>
                                            <td>Family Package (2 Adult + 2 Child Tickets)</td>
                                            {{-- <td>RM 228.00</td>
                                            <td>RM 255.10</td> --}}
                                            <td>RM 302.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="July 16 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/rainforest%20world%20music%20festival%202018/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            {{-- <span class="importantNote">*All the above tickets ranges does NOT include F&amp;B</span> --}}
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown include RM4.00 AirAsiaRedTix fee & other taxes.</li> --}}
                                    <li>Prices shown include RM4.00 AirAsiaRedTix fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 1 days prior to event day, subject to availability.</li>
                                    <li>Adult: 13 years old and above.</li>
                                    <li>Child: 7 - 12 years old.</li>
                                    <li>Child below age 6 entitled free entry.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection