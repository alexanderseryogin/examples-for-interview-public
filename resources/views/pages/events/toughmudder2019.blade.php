@extends('master')
@section('title')
    Tough Mudder 2019 - Philippines
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Tough Mudder 2019 - Philippines" />
    <meta property="og:description" content="Following the much-supported and succesful launch of Tough Mudder Philippines, we bring you TOUGH MUDDER PHILIPPINES 2019, an obstacle-laden event where toughness and teamworkare top the mind to overcome challenges and fear."/>
    <meta property="og:image" content="{{ Request::Url().'images/toughmudder2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/toughmudder2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Tough Mudder 2019 - Philippines"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/toughmudder2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Tough Mudder 2019 - Philippines">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Tough Mudder 2019 - Philippines</h6> Tickets <span>RM265</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  4th August 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Amore at Portofino, Alabang, Philippines <a target="_blank" href="https://goo.gl/maps/WPbqUDsFMpR2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 6.00 am - 12.00pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>Tough Mudder 2019 - Philippines </h2><br/>
                            Following the much-supported and successful launch of Tough Mudder Philippines, we bring you TOUGH MUDDER PHILIPPINES 2019, an obstacle-laden event where toughness and teamwork are top the mind to overcome challenges and fear. This time, we're bringing more and tougher obstacles to the table!</p>
                            <p>Whether it’s a 5K full of fun or the new 8-10 mile Classic, Tough Mudder has the best obstacle-packed courses on the planet - and they're waiting for you. Get ready to go all-in on getting tougher, muddier and more team oriented. Let's get tougher together at TOUGH MUDDER PHILIPPINES 2019.</p>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                {{-- <div class="swiper-slide">
                                    <a href="images/toughmudder2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/toughmudder2019/gallery-1.jpg" alt=""></a>
                                </div> --}}
                                <div class="swiper-slide">
                                    <a href="images/toughmudder2019/gallery-2.jpg" data-featherlight="image"><img class="" src="images/toughmudder2019/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/toughmudder2019/gallery-3.jpg" data-featherlight="image"><img class="" src="images/toughmudder2019/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/toughmudder2019/web-banner.jpg" data-featherlight="image"><img class="" src="images/toughmudder2019/web-banner.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Tier</th>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="2">Early Bird</td>
                                            <td>5K Premium</td>
                                            <td><strike>RM265</strike><p style="color:red;">SOLD OUT</p></td>
                                        </tr>
                                        <tr>
                                            <td>Classic Premium</td>
                                            <td><strike>RM365</strike><p style="color:red;">SOLD OUT</p></td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2">Regular</td>
                                            <td>5K Premium</td>
                                            <td>RM299</td>
                                        </tr>
                                        <tr>
                                            <td>Classic Premium</td>
                                            <td>RM398</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Aug 4 2019 18:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/tough-mudder-2019/booking" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>

                            <div class="note text-left">
                                <h2>Event Notes</h2>
                                <ul>
                                    <li>Admission Age Limit: 18 years old and above</li>
                                    <li>Registration fees include the following: Event Singlet, Bib Number Tattoo, Wrist tag, Finisher's Headband, Finisher's Shirt, Drawstring Bag, Tough Mudder Gear, Sunglasses.</li>
                                    <li>Kit Claiming will be on August 1 - 3, 2019 11:00 AM - 7:00 PM at Evia Lifestyle Center, Alabang.</li>
                                    <li>Waving starts at 6:00 AM. Waves will be released every 15 minutes. Participant must be at the venue 45 minutes before their desired wave time.</li>
                                    <li>All other information can be found at <a href="www.toughmudder.ph">www.toughmudder.ph</a>.</li>
                                </ul>
                            </div>

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude RM8.00 AirAsiaRedTix fee.</li>
                                    {{-- <li>Transaction fee of RM8.00 per ticket applicable for Internet purchase.</li> --}}
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection