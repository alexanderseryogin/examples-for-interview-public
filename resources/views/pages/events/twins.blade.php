@extends('master')
@section('title')
    #TWINS#LOL#LIVE IN GENTING
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/twins/twins-banner.jpg')">
      </div>
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>#TWINS#LOL#LIVE IN GENTING</h6>
                  Tickets from <span>RM132</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 3rd June 2017 (Saturday)</div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8.30pm</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Arena Of Stars, Genting Highland <a target="_blank" href="https://www.google.com/maps/place/Genting+Arena+of+Stars/@3.423828,101.7910333,17z/data=!3m1!4b1!4m5!3m4!1s0x31cc14072aa30697:0xb4fadf09a74b1b30!8m2!3d3.423828!4d101.793222">View Map</a></div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <h2>#TWINS#LOL#世界巡迴演唱會#云顶站</h2>
                <p>《#TWINS#LOL#世界巡迴演唱會#雲頂站》，將為歌迷帶來經典、魅力四射及驚喜十足的動感演唱會。</p>
                <p>由蔡卓妍、钟欣潼组成的香港乐坛最受欢迎女子组合——Twins，自2001年出道以来，一直在音乐和演出上，为歌迷制造无数欢乐回忆，踏入2017，Twins出道16年，两位成员希望在舞台上，与歌迷重温过去一幕又一幕的快乐经典，跟大家尽情狂欢，一起踢走负能量，并以最兴奋心情，继续自己的人生！所以这次演唱会特别以网上流行用语“LOL"作为主题，务求让每位入场观众，都会放声大笑“Laugh Out Loud”！</p>
                <p>这次演唱会的歌曲选择上，除了配合主题的欢快歌曲，则都是以“初恋”和“友谊”为主，歌中唱尽成长中的必经话题，充满了美好、冲动、暗恋、妒忌，各种小情绪和大情怀，绝对能够给观众获得共鸣。</p>
                <p>Twins将会为全场歌迷带来一个最热闹派对，誓必让歌迷一起放开束缚，由《明爱暗恋补习社》，《风筝与风》、《恋爱大过天》、《朋友仔》还是近期的《虚龄时代》、《LOL》， JJ為她們作曲的國語新歌《就說我們都是華麗的單身族》，彻底忘形放声大笑Laugh Out Loud!</p>
                <p>Twins are a Hong Kong Cantopop duo that was created in the summer of 2001, comprising Charlene Choi (蔡卓妍) and Gillian Chung(鍾欣潼).They are a popular group in Hong Kong. Their music was generally targeted at young teens, but in 2004 they began to attract a wider audience by venturing into different styles of music. Both have shed their image as innocent schoolgirls and evolved into two elegant and mature women.</p>
                <p>Over the years, Twins have released 12 Canto-pop albums and four in Mandarin, as well as an array of EPs, compilations and live recordings. Their chart-topping hits, mostly related to young girls’ attitude towards love and romance, play a major role in the shared memories of Hong Kong youth in the 2000s.</p>
                <p>Their early hits include and A Boy in a Girls’ School 女校男生, Love Is Bigger than the Sky 恋爱大过天， Kite & Wind風箏與風 and more.</p>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
              <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/twins/twins-poster.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/twins/twins-banner.jpg" alt="">
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Normal</th>
                          <th>GRC 10% discount</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td><span class="box green">PS1</span></td>
                            <td>RM 432.00</td>
                            <td>RM 389.20</td>
                              <td rowspan="11"><img class="img-responsive seatPlanImg" src="images/twins/seatplan.jpg" alt=""></td>
                          </tr>
                          <tr>
                            <td><span class="box yellow">PS2</span></td>
                            <td>RM 302.00</td>
                            <td>RM 272.20</td>
                          </tr>
                          <tr>
                            <td><span class="box grey">PS3</span></td>
                            <td>RM 202.00</td>
                            <td>RM 182.20</td>
                          </tr>
                          <tr>
                            <td><span class="box midblue">PS4</span></td>
                            <td>RM 132.00</td>
                            <td>RM 119.20</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/twinslollive in genting/events" role="button">BUY TICKETS</a>
                  <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>
                <span class="importantNote">* Ticket pricing include RM4 processing fee.</span>
                <span class="importantNote">* GWC/GRC discount is applicable via outlets only.</span><br>
                <div class="note text-left alert alert-warning">
                  <div class="media">
                    <div class="media-left media-middle">
                      <a href="#">
                        <img class="media-object" src="images/oku.png" alt="">
                      </a>
                    </div>
                    <div class="media-body">
                      <h2>OKU Info</h2>
                      <p>Seats for those with special needs are located in the easily accessible zone (marked with the wheelchair sign on the floor plan). For ticket reservations, please call: 03-2718 1118.</p>
                    </div>
                  </div>
                </div>
                <p>3rd party collection will require authorisation letter and photocopy of the NRIC of ticket holder.</p>
                <p>Please be informed that all the tickets which issued by Ticketing Agent no longer to redeem at Genting Box Office. Guest may directly enter to the concert with using their original purchased ticket. However, guest who holding the Confirmation Letter/E-Ticket for any concert/show shall remain to exchange their ticket at Arena of Star counter, Genting.</p>
                <div class="note text-left">
                  <h2>CONCERT TERMS & CONDITIONS:</h2>
                  <ul>
                    <li>Each ticket is valid for one-time admission only.</li>
                    <li>Child must be of 95cm height and below to be eligible for child ticket priced at RM50 without seat, while child at height above 95cm must purchase tickets according to price scale. Terms & Conditions applies.</li>
                    <li>Strictly no video recording and photography during the show.</li>
                    <li>Video cameras, cameras and tablet computers such as iPad are not allowed in the venue.</li>
                    <li>Lost or damaged ticket(s) will not be entertained.</li>
                    <li>每张门票只限入场一次。</li>
                    <li>身高95公分及以下的儿童需购买价值50令吉只限入场不附席位的儿童票；而身高超过95公分以上的儿童则需依据票价购票。须符合条规。</li>
                    <li>演出进行期间，一律不允许录影及摄影。</li>
                    <li>录影机、相机及智能性平板电脑，如：iPad等，一律不允许带进表演现场。</li>
                    <li>门票若遗失或损坏，恕不受理。</li>
                </ul>
                 <h2>IMPORTANT NOTE:</h2>
                  <ol>
                    <li>Prices shown includes RM4.00 AirAsiaRedTix fee & 6% GST.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 1 week prior to event day, subject to availability.</li>
                  </ol>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        {{-- <section class="pageCategory-section last">
          <div class="buyNowSec" style="background-image: url('images/event2.jpg')">
            <div class="bannerOverlay"></div>
            <div class="container">
              <div class="col-sm-offset-1 col-sm-10 quoteArea">
                <h1>You’ve heard him on the radio, you’ve seen him on TV. It’s about time. See him live!</h1>
              </div>
              <div class="btnArea">
                <a class="btn btn-black btn-lg" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/adam%20cheng%20-%20liza%20wang%20legends%20in%20genting/events" role="button">GET TICKET ONLINE NOW <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <div class="clearfix">&nbsp;</div>
                <span data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets</span>
              </div>
            </div>
          </div>
        </section> --}}

        {{-- <section class="pageCategory-section last">
          <div class="container">
            <div class="partner text-center">
                <h1 class="subSecTitle"><strong>PARTNERS</strong></h1>
                <div class="clearfix">&nbsp;</div>
                <ul class="list-unstyled list-inline partnerList">
                    <li><img src="images/neon/aabig_logo (offical partner).png" alt=""></li>
                    <li><img src="images/neon/BAC.png" alt=""></li>
                    <li><img src="images/neon/durex (official partner).png" alt=""></li>
                    <li><img src="images/neon/fac3 (co-organiser).jpg" alt=""></li>
                    <li><img src="images/neon/seven friday (official watch).jpeg" alt=""></li>
                    <li><img src="images/neon/sunway lagoon (official venue).png" alt=""></li>
                </ul>
            </div>
          </div>
        </section> --}}

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                <h2>GET YOUR TICKETS FROM:</h2>
                <dl class="dl-horizontal">
                    <dt>Online:</dt>
                    <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Rock Corner outlets:</dt>
                    <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                    <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                    <dd>Bangsar Village (TEL: 03-22021139)</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Victoria Music outlets:</dt>
                    <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                    <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
                    <dd>Tropicana</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Penang outlets:</dt>
                    <dd>Artist Gallery, Gurney Plaza (04-228 5648)</dd>
                    <dd>Artist Gallery, Queensbay Mall (04-637 0191)</dd>
                </dl>
            </div>
        </div>
        </div>
    </div>

@endsection