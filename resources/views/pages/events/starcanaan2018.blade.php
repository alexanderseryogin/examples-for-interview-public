@extends('master')
@section('title')
    Star of Canaan Dance International Ballet Competition 2018 Malaysia
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Star of Canaan Dance International Ballet Competition 2018 Malaysia" />
    <meta property="og:description" content="Canaan is a beautiful place which is so resourceful that it becomes an endless provider. It is a blessed land that nurtures beauty and kindness."/>
    <meta property="og:image" content="{{ Request::Url().'images/starcanaan2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/starcanaan2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Star of Canaan Dance International Ballet Competition 2018 Malaysia"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/starcanaan2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Star of Canaan Dance International Ballet Competition 2018 Malaysia">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Star of Canaan Dance International Ballet Competition 2018 Malaysia</h6> Tickets <span>RM80</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  19 December 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> The Temple of Fine Arts, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/tVSbzTiJYGJ2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 2 Shows - 2.00pm & 7.30pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>Star of Canaan Dance International Ballet Competition 2018 Malaysia</h2><br/>
                            Canaan is a beautiful place which is so resourceful that it becomes an endless provider. It is a blessed land that nurtures beauty and kindness. I hope I can contribute to the society through hosting international competitions in Hong Kong with the aim to discover talents in dance and nurture them into great dancers by which building a strong foundation for nurturing more outstanding world class dancers in the long run.</p>
                            <p>Star of Canaan, “Star” means the horizons that all dancers pursue and dream of egardless of their age or ability. Star is literally the shining star up above the sky that is beyond our reach. “Star” therefore resembles the ultimate goal all dancers are pursuing which is the “state of perfection” in dance. Despite all hardship and difficulties, we still persist on and strive to be closer to such “Star”, trying our best to pursue the “state of perfection”. This “Star” leads us to our way of success and guides us to feel the true essence of dance; it also symbolizes the kind of motivation we need.</p>
                            <p>Star of Canaan Dance Competition is a stage for all future great dancers, may all young dance talents showcase your knowledge and skills in dance. On this stage, each competitor is a winner, as you all dance for the “state of perfection” that you envision! Only those who surpass themselves and showcase the true essence they learn from dance can win the audience cheer and applaud. “Challenge yourself, inspire others” is the aim of SCD IBC let each dancer to showcase their talents and attain the horizons which is hard to reach. The tough training you go through, the great effort that you pay and the pain that you tolerate; may you all share your feeling and experience with us here. Do challenge yourself, surpass yourself and dance with confidence; that is what we as audience what to see.</p>
                            <p>May all of us feel the passion you have for dance! May today become the starting point for tomorrow! At this moment, may all dancers enjoy the joy comes from dance.</p>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/starcanaan2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/starcanaan2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/starcanaan2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/starcanaan2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/starcanaan2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/starcanaan2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/starcanaan2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/starcanaan2018/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/starcanaan2018/gallery-5.jpg" data-featherlight="image"><img class="" src="images/starcanaan2018/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/starcanaan2018/web-banner1.jpg" data-featherlight="image"><img class="" src="images/starcanaan2018/web-banner1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Show Time</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>2.00pm <b>Ensemble, Junior A and B Final Competition</b></td>
                                            <td>RM80</td>
                                        </tr>
                                        <tr>
                                            <td>7.30pm <b>Closing Ceremony</b></td>
                                            <td>RM120</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Dec 18 2019 18:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/star of canaan dance international ballet competition 2018 malaysia/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown include RM4.00 ticket fee.</li>
                                    <li>Transaction fee of RM8.00 per ticket applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	$(document).ready(function(){
        $("#announcementModal").modal('show');
  	});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>For competitors discount, please present with your confirmation letter with competition’s entry number. Failure to do so will result in your ticket being canceled and will have to pay a full price for entry on the day of the show. Please read the NOTES before purchase.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection