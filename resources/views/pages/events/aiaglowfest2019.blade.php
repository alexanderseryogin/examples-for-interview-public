@extends('master')
@section('title')
    AIA Glow Festival
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="AIA Glow Festival" />
    <meta property="og:description" content="Minds will be blown at OMNI, Malaysia’s first-ever and largest indoor live music festival!"/>
    <meta property="og:image" content="{{ Request::Url().'images/glowfest2019/thumbnail.jpg' }}" />

    <!-- Google Tag Manager ADA Asia -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-56GR95T');
    </script>
    <!-- End Google Tag Manager -->

    <!-- Google Tag Manager ADA Asia 2 -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KCWM3ZR');
    </script>
    <!-- End Google Tag Manager -->

@endsection

@section('content')
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="css/custom2.css">
    <link type="text/css" rel="stylesheet" href="css/clock.css">
    <link type="text/css" rel="stylesheet" href="/css/custom/redtix.css">
    <link type="text/css" rel="stylesheet" href="css/timeTo.css">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-56GR95T"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/glowfest2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="AIA Glow Festival"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/glowfest2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="AIA Glow Festival">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>AIA Glow Festival</h6> Tickets from <span>SGD15.20</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  25th May 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Palawan Beach, Sentosa, Singapore <a target="_blank" href="https://goo.gl/maps/vCCgkBHGicT2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 7.30am - 11.00pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <p><h2>AIA Glow Festival</h2><br/>
                                Featuring Festival Favourites: Lost Frequencies, Rudimental, Nina Nesbitt and The Sam Willows & Celebrity Yogis: Laura Kasperzak and Marysia Do.<br/><br/>
                                The Ultimate Festival to Unwind. Life is hectic enough for any more madness. And we hear you.</p>
                                <p>Experience a new state of mind with us as you hit the reset button on life. Sometimes all we need is a little "Me Time", and we've got you covered.</p>
                                <p>Reset - Refresh - Recharge<br/>
                                Unwind your way. From YOGA for all levels, amazing live MUSIC, a 5km charity fun RUN and mouthwatering festival FEASTS  - do one, pick some or try them all.</p>
                                <p>It's all about the feel good vibes at Glow. Because when you feel good, everything else will follow.</p>
                                {{-- <p>Website: <a href="https://glowfestival.sg">https://glowfestival.sg</a><br/>
                                Facebook Page: <a href="https://facebook.com/glowfestivalsg">https://facebook.com/glowfestivalsg</a><br/>
                                Instagram: <a href="https://facebook.com/glowfestivalsg">https://instagram.com/glowfestival.sg</a></p> --}}
                            </div>
                            {{-- <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/artist/6deZN1bslXzeGvOLaLMOIF" width="300" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div> --}}
                        </div>
                        {{-- <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/PhrY3yep1OQ?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-10.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-10.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-3.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-4.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-17.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-17.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-16.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-16.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-2.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-5.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-6.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-7.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-8.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-9.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-9.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-11.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-11.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-12.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-12.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-13.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-13.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-14.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-14.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/glowfest2019/gallery-15.jpg" data-featherlight="image"><img class="" src="images/glowfest2019/gallery-15.jpg" alt=""></a>
                                </div>

                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">

                            @include('layouts.partials._showticketssimpledisc')

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown excludes SGD3.00 AirAsiaRedTix ticket fee.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>For AIA Glow Festival Terms and Conditions & Waiver, view <a href="https://airasiaredtix.com/document/aiaglowfestival-termsconditions-waiver.pdf">full details</a></li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
    
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
    @include('layouts.partials.modals._moreInfoLargeImgDesc')
@endsection