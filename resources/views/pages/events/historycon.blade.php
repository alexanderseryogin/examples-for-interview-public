@extends('master')
@section('title')
    History Con Malaysia 2017
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/historycon/historycon-banner.jpg')">
      </div>  
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>History Con Malaysia 2017</h6>
                  Tickets from <span>RM20</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 4 - 6 August 2017, (Friday - Sunday)<div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> MAEPS, Serdang<a target="_blank" href="https://goo.gl/maps/PHKBgxyFZYM2"> View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 10.00 AM to 9.00 PM</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <h6>History Con Malaysia 2017</h6>
                <p>The mega entertainment convention is coming to Malaysia for the first time!</p>
                <p>HISTORY Con brings history to life. Get up-close with celebrities from your favourite shows, and experience interactive, educational activities that the whole family can enjoy.</p>
                <p>HIGHLIGHTS</p>
                <ul>
                    <li>Learn to airbrush cars with Mike Henry from Counting Cars</li>
                    <li>Exclusive cooking demonstrations by MasterChef Australia judge Matt Preston</li>
                    <li>Appearances by local favourites Faizal Tahir and Elizabeth Tan</li>
                    <li>Classic car displays</li>
                    <li>Immersive VR experiences</li>
                    <li>Plus, much more!</li>
                </ul>
                </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/historycon/historycon-banner.jpg" alt="">
                  </div>

                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Ticket Category</th>
                          <th>Price</th>
                          <th>Remarks</th>
                        </tr>
                      </thead>
                      <tbody>

                        <tr>
                        <td><span class="box purple">Basic Pass</span></td>
                        <td>RM 20</td>
                        <td>
                            <ul style="text-align:left">
                                <li>1 day pass</li>
                                <li>1 event string bag</li>
                                <li>1 lucky draw coupon</li>
                                <li>Discount coupons to use at event</li>
                            </ul>
                        </td>
                        </tr>
                        <tr>
                            <td><span class="box yellow">History Maker Pass</span></td>
                            <td>RM 50</td>
                            <td>
                                <ul style="text-align:left">
                                    <li>All-access pass (all 3 days)</li>
                                    <li>1 event string bag</li>
                                    <li>1 event t-shirt</li>
                                    <li>3 lucky draw coupons</li>
                                    <li>Discount coupons to use at event</li>
                                </ul>
                            </td>
                          </tr>
                          <tr>
                              <td><span class="box red">Family Pass</span>(Please select 4 tickets)</td>
                              <td>RM 60</td>
                              <td>
                                <ul style="text-align:left">
                                    <li>4 x 1-day passes</li>
                                    <li>4 x event string bags</li>
                                    <li>4 x lucky draw coupons</li>
                                    <li>Discount coupons to use at event</li>
                                </ul>
                            </td>
                          </tr>
                          <tr>
                              <td><span class="box midblue">VIP Pass</span></td>
                              <td>RM 1000</td>
                              <td>
                                <ul style="text-align:left">
                                    <li>Exclusive 8 course dinner by Matt Preston</li>
                                    <li>All-access pass to History Con</li>
                                    <li>1 event string bag</li>
                                    <li>1 event t-shirt</li>
                                    <li>10 lucky draw coupons</li>
                                    <li>Discount coupons to use at event</li>
                                    <li>Sponsor gift pack</li>
                                    <li>Limited 50 tickets only</li>
                                </ul>
                            </td>
                          </tr>
                          
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                    <a class="btn btn-danger disabled" data-toggle="modal" data-target="#announcementModal">BUY TICKETS</a> 
                  <!--<a class="btn btn-danger disabled" id="buyButton" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/history con malaysia (4-6 august 2017)/events " role="button">BUY TICKETS</a>-->
                  <!-- <span class="or">/</span>                 
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> -->
                </div>
                <div class="alert alert-info" style="text-align:center"><i class="fa fa-info-circle" aria-hidden="true"></i> Online purchase has closed. Tickets are available at the venue from <strong>10 AM to 8 PM.</strong></div>                  
                <span class="importantNote">* Prices stated are exclusive RM4.00 ticketing fee.</span>
                <div class="note text-left">
                  <h2>Important Notes</h2>
                  <ol>
                    <li>Prices shown exclusive RM4.00 AirAsiaRedTix fee.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                    <li>Flash photography is not allowed​​.</li>
                  </ol>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('modal')

<!--Modal Announcement-->
  <div id="announcementModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center" style="max-height: 100%">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <!--<h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>-->
            <h4>PERSONAL DATA PROTECTION NOTICE</h4>
            <div class="clearfix">&nbsp;</div>
            <div class="well" style="max-height: calc(80vh - 210px); overflow-y: auto;">
            <ul style="text-align:left">
                <li>GENERAL</li>
                <p>We value your privacy and strive to safeguard your personal data in compliance with the laws of Malaysia. We use our best efforts to ensure that the information we receive remains private and is used solely for the purposes outlined. This personal data protection notice is prepared in accordance and compliance with the Personal Data Protection Act 2010 (&quot;PDP Notice&quot;). Please read this PDP Notice carefully as it is important and applicable to you when you purchase and use the ticket for History Con Malaysia 2017 (“History Con”) and participation in any activities (“Activities”) held at History Con (“Ticket”).</p>
                <li>COLLECTION, USE AND PROCESSING OF PERSONAL DATA</li>
                <p><u>Collection</u></p>
                <p>In order to carry out History Con and the Activities, we may collect a variety of your personal data including (but not limited to) personal details (such as name, photographs, age, gender, national registration identity card number, passport number or other identification document number, date of birth, race, ethnic origin, nationality, physical and mental health etc.); contact details (such as correspondence address, electronic mail address and phone numbers); payment details such as credit or debit card information, including the name of cardholder, card number, card issuing bank, card issuing country, expiry date and other banking account details; and information about your computer (including mobile device) such as your internet protocol address, geographical location, browser type, browser version, operating system, length of visit, page viewed and website navigation paths and other information that relates directly or indirectly to you, when you purchase the Tickets online through our website.</p>
                <p>All information requested for in the relevant forms is obligatory to be provided by you unless stated otherwise. Should you fail to provide the obligatory information, we would be unable to process your request to us for any of the purposes set out in this PDP Notice and/or provide you with relevant services.</p>
                <p><u>Manner of collection and processing</u></p>
                <p>We may collect and process your personal data (including sensitive personal data) when you purchase the Ticket online through our website or that of our agents, business partners/affiliates (including through the use of cookies on certain pages of the website); directly from you or indirectly from our agents, business partners/affiliates when you purchase the Ticket on-site or at their retail outlets; and when you communicate with us via e-mail or any other modes of communication.</p>
                <p><u>Purpose</u></p>
                <p>We may use and process your personal data (including sensitive personal data) to verify your identity, to administer and manage History Con and the Activities, to facilitate your participation in any Activities or contests, to process any payments related to History Con and the Activities, for internal investigations, audit or security purposes, to conduct internal marketing and customer patterns and choices analysis, to contact you regarding products, services, upcoming events, promotions, marketing and commercial materials which we may feel interest you, to attend to your requests, feedback, enquiries or complaints in relation to History Con and the Activities, to communicate with you for any of the purposes listed in this PDP Notice and/or for any other purposes that are required or permitted by any law, regulations, guidelines and/or relevant regulatory authorities.</p>
                <li>ACCESS TO AND CORRECTION OF PERSONAL DATA </li>
                <p>Subject to certain exceptions (for example, if it would affect the privacy rights of other persons or it breaches any confidentiality that attaches to that information), you may write to us at <strong class="text-danger">support_redtix@airasia.com</strong> to request access to and correct your personal data held by us. We will use reasonable efforts to comply with your requests in a timely manner. If we are unable to accede to your requests, we will notify you of the reasons.</p>
                <li>DATA INTEGRITY</li>
                <p>We use our best efforts to ensure that the personal data we maintain about you is accurate, complete, up-to-date and not misleading. Therefore, the personal data which you provide us should, as far as it is reasonably practicable, be accurate, complete, up-to-date and not misleading. Should you wish to update or rectify your personal data or limit the usage or processing of your personal data held by us, you may write to us at <strong class="text-danger">support_redtix@airasia.com</strong>. Upon request, we may update your personal data accordingly or if the information provided to us is incomplete or misleading, we shall be entitled to request for further information and supporting documents before proceeding to update your personal data. We will use reasonable efforts to comply with your requests in a timely manner. If we are unable to accede to your requests, we will notify you of the reasons.</p>
                <li>DISCLOSURE OF PERSONAL DATA</li>
                <p>We generally keep your personal data confidential. However, we may be required to disclose them to certain parties in certain circumstances such as persons/companies/organisations whom you have consented to share your personal data; our business partners/affiliates; any of our employees, officers or professional advisers insofar as is reasonably necessary for the purposes set out in this PDP Notice; any governmental, public or regulatory authority when required by such authority; and/or our prospective or new holding or subsidiary companies in case of a merger, acquisition, reorganisation of our company or any corporate proposal undertaken by us.</p>
                <li>SECURITY OF PERSONAL DATA</li>
                <p>We take commercially reasonable steps to safeguard your personal data from loss, misuse, modification, unauthorised or accidental access, disclosure, alteration, or destruction. However, we cannot ensure or warrant the security of any information which you provide to us and you do so at your own risk.</p>
                <li>RETENTION OF PERSONAL DATA</li>
                <p>Your personal data will be stored either in hard copies in our office or in the servers located in or outside Malaysia and operated by us, our service providers, our business partners/affiliates or their service provider. We will retain your personal data in compliance with this PDP Notice for such period as may be necessary for the fulfilment of the purposes described in this PDP Notice and/or where otherwise required by the law. We shall take all reasonable steps to ensure that your personal data is destroyed or permanently deleted when no longer required for the purposes described in this PDP Notice. We do not offer any online facilities for you to delete your personal data held by us.</p>
                <li>CHILDREN PERSONAL DATA PROTECTION</li>
                <p>We may collect, use and reveal the personal data of a child below 18 years of age (“Minor”) publicly, for example the Minor may be photographed and/or recorded at History Con and/or during any of the Activities for any and all purposes including the production of any and all content, vignettes, long form and/or short form, creative and/or promotional materials in connection with History Con and/or the Activities in any and all media. As a parent or legal guardian of a Minor, you have the right to refuse to permit any collection or use of the Minor’s information. To do so, you may write to us at <strong class="text-danger">support_redtix@airasia.com</strong> . However, we will not establish conditions that will require or encourage Minor’s to disclose personal information over and above what is reasonably necessary for History Con and/or the Activities.</p>
                <li>AMENDMENTS OR UPDATES TO THIS PDP NOTICE</li>
                <p>We may periodically amend or otherwise update this PDP Notice at our discretion, without prior notification to you. Such amendments or updates shall have effect immediately upon posting to our website. We suggest that you visit our website regularly to keep up to date with any changes.</p>
                <li>ACKNOWLEDGEMENT AND CONSENT</li>
                <p>By purchasing the Ticket, you acknowledge that you have read and understood this PDP Notice and agree and consent to the use, processing and transfer of your personal data as described in this PDP Notice.</p>
                <li>CONTACT US</li>
                <p>Should you have any queries or complaints relating to this PDP Notice or otherwise relating to any misuse or suspected misuse of your personal data, you may write to us at <strong class="text-danger">support_redtix@airasia.com</strong></p>
            </ul>
            </div>
        </div>
        <div class="modal-footer">
            <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/history con malaysia (4-6 august 2017)/events " role="button">PROCEED TO BUY TICKETS</a>
      </div>

    </div>
      </div>
  </div>

  <!--Modal Seat Plan image-->
  <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <div class="clearfix">&nbsp;</div>
          <img src="" class="enlargeImageModalSource" style="width: 100%;">
          </div>
      </div>
      </div>
  </div>
  
  <!-- Modal Where to Get Tix Location-->
  <div id="modalGetTixLoc" class="modal fade" role="dialog">
      <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <h2>GET YOUR TICKETS FROM:</h2>
          <dl class="dl-horizontal">
              <dt>Online:</dt>
              <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Rock Corner outlets:</dt>
              <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
              <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
              <dd>Bangsar Village (TEL: 03-22021139)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Victoria Music outlets:</dt>
              <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
              <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Penang outlets:</dt>
              <dd>Artist Gallery, Gurney Plaza (04-228 5648)</dd>
              <dd>Artist Gallery, Queensbay Mall (04-637 0191)</dd>
          </dl>
          <!--<dl class="dl-horizontal">
              <dt>Istana Budaya Jalan<br>Tun Razak:</dt>
              <dd>03 40265558</dd>-->
          </dl>
          </div>
      </div>
      </div>
  </div>

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // // Modal Announcement
    // $(document).ready(function(){
    //   $("#announcementModal").modal('show');
    // });
    // </script>
    
@endsection