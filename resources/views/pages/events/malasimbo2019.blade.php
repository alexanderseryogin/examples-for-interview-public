@extends('master')
@section('title')
    Malasimbo Music and Arts Festival
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Malasimbo Music and Arts Festival" />
    <meta property="og:description" content="The festival blends traditional and contemporary arts and culture together with world class music whilst working towards the protection and sustainability of the environment."/>
    <meta property="og:image" content="{{ Request::Url().'images/malasimbo2019/thumbnail1.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/malasimbo2019/web-banner1.jpg')}}" style="width: 100%" class="img-responsive" alt="Malasimbo Music and Arts Festival"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/malasimbo2019/thumbnail1.jpg')}}" style="width: 100%" class="img-responsive" alt="Malasimbo Music and Arts Festival">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Malasimbo Music and Arts Festival</h6> Tickets <span>RM162</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  1st & 2nd March 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Puerto Galera, Philippines <a target="_blank" href="https://goo.gl/maps/6rp58T19BVP2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Friday & Saturday (1.00 pm - 2.00am)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>Malasimbo Music and Arts Festival </h2><br/>
                            The festival blends traditional and contemporary arts and culture together with world class music whilst working towards the protection and sustainability of the environment.</p>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/malasimbo2019/gallery-flyer.jpg" data-featherlight="image"><img class="" src="images/malasimbo2019/gallery-flyer.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/malasimbo2019/gallery-1x.jpg" data-featherlight="image"><img class="" src="images/malasimbo2019/gallery-1x.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/malasimbo2019/gallery-2.jpg" data-featherlight="image"><img class="" src="images/malasimbo2019/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/malasimbo2019/gallery-3.jpg" data-featherlight="image"><img class="" src="images/malasimbo2019/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/malasimbo2019/gallery-4.jpg" data-featherlight="image"><img class="" src="images/malasimbo2019/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/malasimbo2019/gallery-5.jpg" data-featherlight="image"><img class="" src="images/malasimbo2019/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/malasimbo2019/web-banner1.jpg" data-featherlight="image"><img class="" src="images/malasimbo2019/web-banner1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Friday Pass</td>
                                            <td>RM190</td>
                                        </tr>
                                        <tr>
                                            <td>Saturday Pass</td>
                                            <td>RM240</td>
                                        </tr>
                                        <tr>
                                            <td>2 Day Pass</td>
                                            <td>RM380</td>
                                        </tr>
                                        {{-- <tr>
                                            <td>Group Pass<br/>(5 Tickets)</td>
                                            <td>RM1,296</td>
                                        </tr> --}}
                                    </tbody>
                                    {{-- <thead>
                                        <tr>
                                            <th colspan="2">DOOR PRICE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr style="color:#999999;">
                                            <td>Friday Pass</td>
                                            <td>RM203</td>
                                        </tr>
                                        <tr style="color:#999999;">
                                            <td>Saturday Pass</td>
                                            <td>RM243</td>
                                        </tr>
                                        <tr style="color:#999999;">
                                            <td>2 Day Pass</td>
                                            <td>RM405</td>
                                        </tr>
                                    </tbody> --}}
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Feb 28 2019 18:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/malasimbo music and arts festival (01st - 02nd march 2019)/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <div class="note text-left">
                                <h2>Terms &amp; Conditions</h2>
                                <ul>
                                    <li>Only tickets purchased from RedTix and other ticketing providers authorized by event organiser will be accepted.</li>
                                    <li>Tickets purchased through unofficial online sellers, reselling websites or any other supplier or means possible WILL NOT BE HONORED, unless they are stated here and authorized by Event Organizers.</li>
                                    <li>No exchange of tickets will be made under any circumstances and tickets are not transferable.</li>
                                    <li>No refund on tickets will be made under any circumstances except if the event is canceled or postponed.</li>
                                </ul>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude ticket fee.</li>
                                    <li>Transaction fee of RM8.00 per ticket applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	//$(document).ready(function(){
    //    $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

    <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Temporarily down for Maintenance</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>We are performing scheduled maintenance. We should be back online shortly</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection