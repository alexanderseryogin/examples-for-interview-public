{{-- @extends('master') --}}
@extends('masterforfairydoll')
@section('title')
    The Fairy Doll
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="The Fairy Doll" />
    <meta property="og:description" content="The Fairy Doll, or Die Puppenfee, was premiered at the Vienna Court Opera on the 4th October 1888, a ballet which owes its inspiration to E.T.A Hoffman’s 1815 story the Sandman in which a mechanical doll comes to life."/>
    <meta property="og:image" content="{{ Request::Url().'images/thefairydoll2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/thefairydoll2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="The Fairy Doll"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/thefairydoll2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="The Fairy Doll">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>The Fairy Doll</h6> Tickets <span>RM88</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  18th - 19th January 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Istana Budaya, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/xDVBXk3rECr">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8.30 pm - 10.00 pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <div class="col-sm-12">
                                <hr>
                                <p><h2>The Fairy Doll</h2><br/><br/>
                                First Act</p>
                                <p>The story occurs in a toy shop with distinctive dolls from different countries. The store owner and his young assistant happily show customers the dolls, and the children dance joyfully with them. After a while, parents become impatient and quarrelled with their children.</p>
                                <p>Finally, the store owner brings the most precious doll of all - The Fairy Doll. The stunning and charming doll dances in front of all customers. With her magic powers, she puts the adults to sleep.</p>
                                <p>Second Act</p>
                                <p>The Fairy Doll takes the young assistance and children around the world. She takes her guests feel the local visual culture in France, dance with Flamenco in Spain, spent Mid-Autumn Festival in China, appreciated the religious Japanese fan dance, elegant and lively Russian dance, enjoyed Korean dance which shows character of endurance and strength ,traditional candle dance of Malaysia that represents the balance of force and mercy in life. During the journey, all dolls in the toy shop, including the tin soldier, Bunny Doll, Music Box Dolls, Harlequin Doll, come to life and travel the world with the children. The exciting journey comes to an end as two Pierrots dances with Fairy Doll.</p>
                                <p>As children go back to the store, the adults awake from a dream of the very same journey in which they found the origin of joy. The Fairy Doll reminds everyone to that simple and positive attitude will bring innocence. To be young at heart is to love life, wake up every day and enjoy that day as a gift.</p>
                                <hr>
                                <p>第一幕</p>
                                <p>故事發生在一間容有很多各式各樣和不同國家風格娃娃的玩具店。助理店主和店主很高興地將心愛的玩具娃娃展示給他的大小客戶。小孩們都很開心和娃娃們共舞。過了一會，父母有點不耐煩，並和孩子們發生了小爭執。</p>
                                <p>後來店主取出了全店最珍貴的仙女娃娃。她在眾人面前翩翩起舞，漂亮的舞姿震驚全場。最後，仙女娃娃施法術令成人們進入夢中。</p>
                                <p>第二幕</p>
                                <p>仙女娃娃與她兩位同伴和小孩們漫遊世界。他們到了法國感受當地的視覺文化，在西班牙與佛朗明哥一起共舞，在中國渡過人月團圓中秋之夜、欣賞了日本扇舞的敬神風俗、俄羅斯舞中優雅和熱鬧的風格、體驗韓國堅忍與自强的民族性格以及傳統馬來西亞蠟燭舞的剛柔並重和平衡的人生態度。在遊歷過程中玩具店中，所有娃娃都活起來，和小孩們結伴在一起遊歷世界，例如錫兵、拉丁、賓利免、音樂盒、小丑娃娃等......最後旅程終結於仙女娃娃和她兩位同伴的舞蹈中，大家都沉醉了！</p>
                                <p>當所有小孩子返回玩具店後，成人們都夢醒了。他們在夢與小孩們共同經歷了一切，感受到小孩真正歡樂的來源。仙女娃娃提醒大家 - 簡單和正面的人生觀和童真帶給大家返璞歸真的生活，會讓我們身心永遠年輕。譲我們每天懷着感恩的心，迎接每一天。</p>
                                <br/><br/>
                            </div>
                            <div class="clear-fix"></div>
                            <div class="col-sm-12">
                                <h2>Biography</h2>
                                {{-- <p>Guest are principle dancer of Arts Ballet Theatre of Florida.</p> --}}
                            </div>
                            <div class="col-sm-12">
                                <hr>
                                <div class="col-sm-2">
                                    <p><img class="img-responsive text-center" src="images/thefairydoll2018/ivy-chung.png" alt=""></p>
                                </div>
                                <div class="col-sm-10">
                                    <p><b>Ivy Chung</b> - Artistic Director / President</p>
                                    <p>Ivy started her professional ballet training at the Hong Kong Academy for Performing Arts (HKAPA) in 1989. In 1993, Ivy graduated as an honorary student with Highest Distinction in her class, and she was awarded the Overseas Scholarship by Hong Kong Jockey Club Music and Dance Fund. She has earned a Master's degree of Business Administration and she was appointed as Associate Executive Director of the Kirov Academy of Ballet Washington DC.</p>
                                    <p>She began her career as a professional dancer in the Universal Ballet Company of Korea, Ontario Ballet Theater and the Hong Kong Ballet. She worked with various famous ballet artists: Galina Kekisheva, Roy Tobias, Stephen Jefferies, Wayne Eagling, Galina Ulanova etc. She was also awarded the Favorite Dancer Award thrice by the Friends of Hong Kong Ballet. She has been invited as a ballet teacher and choreographer internationally.</p>
                                    <p>In 2002, she founded her own school, Ivy Chung School of Ballet and Ivy’s Ballet Company in Hong Kong. Then in 2017, she founded Asia Ballet Academy in Malaysia which focuses on the Vaganova method of classical ballet training. Her productions were invited to perform at the "Hong Kong Original Dance Works Spectacle" of 2010 Hong Kong Dance Festival, the Hong Kong Ballet Group's "Young Ballet Stars Gala Performance", and the Hong Kong Dance Expo 2011. She also obtained the Registered Teacher Status of the Royal Academy of Dance and The Commonwealth Society Teachers of Dancing. She is also a mentor of the Certificate of Classical Ballet Teaching Studies. She has served on the jury for many ballet competitions, National Ballet Competition of Taiwan, Malaysia Grand Prix, Miami International Ballet Competition etc. In 2018, she was awarded Outstanding Dance Artist by Hong Kong Dance Federation in recognition of her contribution to the dance industry of Hong Kong.</p>
                                    <p>With Ivy's dedication to her students, different recognitions were awarded to her, including: 'Gold Award' in "2009 National Outstanding Arts Teacher Award", 'Elite Society Award' and 'Silver Award' in "The Second Youth Arts Festival - Arts Education", 'Silver Award' in "The Second Toddler Arts Festival - Arts Education", 'Gold Award' in Arts Education and 'Excellent Instructor Award' in the "6th Dé‧Yì‧Shuāng‧Xīn Hong Kong Selection", 'Outstanding Instructor Award' in the "4th China Youth Arts Festival – Hong Kong Selection" awarded by the China Artist Association. Ivy was presented with the Best Choreography Award for Modern Dance Section in the 39th and 40th Hong Kong School Dance Festival. In 2013, Ivy appointed Honorary President of Hong Kong Youth Art Appreciation Association and Adjudicator of Hong Kong Dance Federation.</p>
                                    <p>Ivy founded Canaan Dance in 2014, which is an international dance education organization. Under the direction of Ivy, Canaan Dance organizes workshops, auditions and international ballet competition to nurture young dancers in Asia, and provide them with scholarships and opportunities to study professionally abroad. She founded the Star of Canaan Dance International Ballet Competition, which is supported by the American Ballet Theater , Arts Ballet Theater of Florida , Royal Ballet School, the English National Ballet School, Elmhurst Ballet School, Orlando Ballet School, Kirov Academy of Ballet Washington DC, the Universal Ballet Company of Korea, the Shanghai Dance School , Houston Ballet, Elmhurst Ballet School UK, the Vassiliev Academy of Classical Ballet , the Ballet Centre of Fort Worth , the School of Cadence Ballet in Canada , Cheng Ballet in Singapore and Soki Ballet School in Japan.</p>
                                    <p>On behalf of Canaan Dance, Ivy is the Audition Director (Asia) of the School of Cadence Ballet, the Vassiliev Academy of Classical Ballet , Ballet Centre of Fort Worth and The Kirov Academy of Ballet (2014 -2017).</p>
                                    <p>Ivy’s participate in charity functions. Her students has taken part in performances for charity shows such as "Dancing from our Hearts" and "Si Chuan Earthquake Donation" supported by "the World's Vision", "Dance for Light" organized by "Lifeline Express", "Ginger Bread Dance" organized by "Children's Heart Foundation", "Joyful (Mental Health) Foundation", ballet performance organized by "Against Child Abuse Ltd", and many more.</p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <hr>
                                <div class="col-sm-2">
                                    <p><img class="img-responsive text-center" src="images/thefairydoll2018/vladimir-issaev.png" alt=""></p>
                                </div>
                                <div class="col-sm-10">
                                    <p><b>Vladimir Issaev</b> - Artistic Director / Choreographer</p>
                                    <p>VLADIMIR ISSAEV is currently Ballet Master, Choreographer & Artistic Director of Arts Ballet Theatre of Florida and former Ballet Master of Ballet Nacional de Caracas - Teresa Carreño and Miami City Ballet School. He was born in Siberia, Russia, in 1954. At the age of 11 he began his ballet studies at the Choreographic School of Voronezh, obtaining the degree of “Ballet Artist” in 1973.</p>
                                    <p>
                                        <ul>
                                            <li>Dancer at the Opera House of Ufa and the Opera House of Odessa</li>
                                            <li>Master’s Degree from the State Institute of Arts Lunacharsky G.I.T.I.S. in Moscow</li>
                                            <li>He has been Academic and Artistic Director at the Choreographic School of Frounze, and Ballet Master for the disciplines of Classical Ballet, Character Dance, Court Dances and Pas de Deux</li>
                                            <li>Ballet Master for the Ballet Metropolitano de Caracas</li>
                                            <li>Ballet Master at the Ballet Nacional de Caracas - Teresa Carreño and Artistic Director of II Company</li>
                                            <li>Ballet Master at Ballet Concierto de Puerto Rico</li>
                                            <li>Ballet Master Miami City Ballet School</li>
                                        </ul>
                                    </p>
                                    <p>As a Choreographer he has staged several ballets in Russia, United States, Puerto Rico and Venezuela for the Choreographer School of Voronezh and Frounze, the Ballet School Teresa Carreño, Ballet Metropolitano de Caracas, Ballet Nacional de Caracas Teresa Carreño – Second Company and for the Ballet Nacional de Caracas Teresa Carreño itself, Ballet Concerto de Puerto Rico, Arts Ballet Theatre of Florida, Miami City Ballet School, The Georgia Youth Ballet, The Vaganova Ballet Academy (Kirov) in Saint Petersburg, Russia, Ballet Concierto de Puerto Rico, Ballet Juvenil de Venezuela, South African Ballet and Georgia Youth Ballet, Matsumoto Ballet in Nagoya, Japan, Caadi, Queretaro Mexico.</p>
                                    <p>
                                        <ul>
                                            <li>Artistic Director of the Russian Ballet Seminar of Russian Technique since 1998</li>
                                            <li>Ballet Master for the Florida Dance Festival and the Cecchetti Seminar at the Harid Conservatory</li>
                                            <li>Seven CD’s of Music for Ballet Technique Classes and Character Dances have been released. Every CD has received the best reviews</li>
                                            <li>Guest Ballet Master for: Le Jeune Ballet de France, Ballet Nacional de España, Les Ballets Jazz  de Montreal, Orlando Ballet, Indonesia Ballet, ENSB Peru, CAADI Ballet, South African Ballet, Michiko Matsumoto Ballet</li>
                                            <li>Judge at the First International Competition of Choreographers held in Russia, International  Ballet Competition, International Ballet Competition of Lima, Nagoya international Ballet  Competition in Japan, International Ballet Competition Korea, International Ballet Competition "Golden Pointe" in Poland</li>
                                        </ul>
                                    </p>
                                    <p>[Awards]<br/>
                                        <ul>
                                            <li>2015 Best Choreographer for best production – Miami Life Awards</li>
                                            <li>2009 Best Arts Educator of the State of Florida by the Florida Alliance for Arts Education</li>
                                            <li>2002 “Best New Choreographer” at the Dance Under the Stars Choreographic Competition in Palm Springs, California</li>
                                            <li>2002 Honorable Merit Diploma from the Terpsihora Foundation for the Advancement of the Arts of Dance of Saint Petersburg, Russia, in recognition for his outstanding performance enhancing the art of classical ballet on the Vaganova Style in the United States</li>
                                            <li>2000 Most influential Ballet teacher by the National Foundation of the Advancement of the Arts</li>
                                        </ul>
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <hr>
                                <div class="col-sm-2">
                                    <p><img class="img-responsive text-center" src="images/thefairydoll2018/janis-liu.png" alt=""></p>
                                </div>
                                <div class="col-sm-10">
                                    <p><b>Janis Liu Man Ting</b> – Principal</p>
                                    <p>She was born in Hong Kong and began her ballet training under Ivy Chung School of Ballet in Hong Kong. After being accepted into the pre-professional program at Pittsburgh Ballet Theatre School in 2012, she joined Coastal City Ballet of Vancouver, Canada in 2013 and until 2016 she was a soloist at Columbia Ballet. In the summer of 2013 she was asked to perform in Swan Lake with the Hong Kong Ballet Company at the Guangzhou Theatre and in 2014 she was a guest artist for Charleston Ballet.</p>
                                    <p>Ms. Liu has won numerous international and important awards competing in many significant competitions including the prestigious Varna International Ballet Competition held in Varna, Bulgaria, USA IBC in Jackson, and the Asian Grand Prix.  Most recently she was awarded the Gold Medal at the Miami International Ballet Competition in January of 2018.Janies joined Arts Ballet Theatre of Florida in 2016 since then she has performed as soloist in Stone Flower, The Nutcracker, Chipollino, Paquita, Our Waltzes, Harlequinade, Le Corsaire, Pentimento and Firebird. She has traveled to Mexico and Poland as a guest dancer with Vladimir Issaev.</p>
                                </div>
                            </div>
                            {{-- <div class="col-sm-12">
                                <hr>
                                <div class="col-sm-2">
                                    <p><img class="img-responsive text-center" src="images/thefairydoll2018/taiyu-he.png" alt=""></p>
                                </div>
                                <div class="col-sm-10">
                                    <p><b>Taiyu He</b><br/>
                                    Taiyu He was born in Hunan, China. His early ballet training started at the Liaoning Ballet School where he also graduated.  Mr. He has won a gold medal at Osaka International Ballet Competition in 2010, gold medal at Korea International Ballet Competition in 2011, gold medal at Helsinki International Ballet Competition, gold medal at Seoul International Dance competition (Ballet Category) in 2012, gold medal at USA International Ballet Competition in 2014 and a silver medal at the Moscow International Ballet Competition in 2013.</p>
                                    <p>He was also granted a Special Distinction at the Varna International Ballet Competition in 2012. Prior to joining Arts Ballet Theatre of Florida Mr. He danced with Joffrey Ballet Studio Company and Tulsa Ballet Second Company.</p>
                                </div>
                            </div> --}}
                            <div class="col-sm-12">
                                <hr>
                                <div class="col-sm-2">
                                    <p><img class="img-responsive text-center" src="images/thefairydoll2018/kevin-zong.png" alt=""></p>
                                </div>
                                <div class="col-sm-10">
                                    <p><b>Kevin Zong</b> - Principal</p>
                                    <p>HE is a graduate from the Ellison Ballet of New York where he studied for the last three years trained by Edward Ellison himself and under full scholarship. He started his ballet training at the First State Ballet Theater in Wilmington, Delaware under the tutelage of Pasha and Kristina Kambalov. He also attended the Shanghai Dance School, and summer programs at the American Ballet Theater, Next Generation Ballet in Florida, the School of American Ballet and s short –term apprenticeship at the Royal Danish Ballet in Copenhagen, Denmark in 2015.</p>
                                    <p>His repertoire includes Swan Lake Pas de Trois, Napoli, Villanelle, Carmen, Flams of Paris, Le Corsair among many others. He has been awarded Silver Medal at the Indianapolis International Ballet Competition in 2015, and First and Second prize awards at the Youth American Grand Prix Semi-finals in 2014 and 2015 along with several full merit scholarships.</p>
                                    <p>Kevin began his professional career last season at Arts Ballet Theatre of Florida. Since then, he has performed in Mexico, Poland, and Peru with Principal dancers Mary Carmen Catoya and Janis Liu.</p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <hr>
                                <div class="col-sm-2">
                                    <p><img class="img-responsive text-center" src="images/thefairydoll2018/ito-masanao.png" alt=""></p>
                                </div>
                                <div class="col-sm-10">
                                    <p><b>Ito Masanao</b> - Principal</p>
                                    <p>Masanao Ito was born in Ibaraki, Japan. He started his ballet training at Sion Ballet. Later, he attended the Pittsburgh Ballet Theatre School's Graduate Program on full scholarship for two years where he danced roles in Serenade, Etudes, Don Quixote, La Sylphide and Tribute. Additionally, he was invited to perform with the main company where he danced roles in The Nutcracker, Peter Pan, Le Corsaire and Alice in Wonderland among others.</p>
                                    <p>He participated in Japan Grand Prix 2013 and received a full scholarship at Pittsburgh Ballet Theatre School Summer Intensive. He was awarded Bronze Medal in Pas de Deux at the Miami International Ballet Competition 2018. He also performs as a guest dancer all over Japan. Masanao has danced soloists roles with Arts Ballet Theatre of Florida, among them: Vicente Nebrada’s Our Walter and Pentimento and Sugarplum prince in The Nutcracker and Harlequinade. This is his second season with Arts Ballet Theatre of Florida.</p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <hr>
                                <div class="col-sm-2">
                                    <p><img class="img-responsive text-center" src="images/thefairydoll2018/arnulfo-andrade-jr.png" alt=""></p>
                                </div>
                                <div class="col-sm-10">
                                    <p><b>Arnulfo Andrade Jr</b> - 1st Soloist</p>
                                    <p>Former Soloist of Ballet Manila since 2012 and 2009 respectively, Arnulfo Andrade, Jr. has danced many lead roles such as Ali and Birbanto in Le Corsaire, Prince Siegfried in Swan Lake, Tybalt in Paul Vasterling’s Romeo and Juliet, and more. Before joining the company, he was trained at Ballet Manila School under the tutelage of Tatiana Alexandrovna Udalenkova from Vaganova Ballet Academy and many more masters from Mariinsky Ballet and Ballet Manila. Andrade has taken part in many prestigious competitions, and was awarded 4th Place of Senior division and Most Promising Male Dancer at Asian Grand Prix 2012 in Hong Kong. Since 2016, he has been a Freelance Artist.</p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <hr>
                                <div class="col-sm-2">
                                    <p><img class="img-responsive text-center" src="images/thefairydoll2018/alfren-salgado.png" alt=""></p>
                                </div>
                                <div class="col-sm-10">
                                    <p><b>Alfren Salgado</b> - 1st Soloist</p>
                                    <p>Soloist of Ballet Manila.</p>
                                    <p>Joined Ballet Manila in June 2005.</p>
                                    <p>Repertoire :<br/>
                                    Role of Solor in La Bayadere; Swan Lake (Price Siegfried); Don Quixote(Basilio); Carmen(Donjose); Romeo and Juliet( Mercuito); Nutcracker; Pinocchio; Halo-Halo2; La Fille Mal Gardae; Tatlong kuwento ni Lola Basyan;, The Swan,the Fairy and the Princess; Alamat: Si SIbol at Sin Gunaw; Sayaw Sa Pamlang; Night; Arnis; Dancing to Czerny; Pas de Trios; Ballet Blanc; Dulce; Pista; Solo Tango; M.A.Z.N; Reconfigured; Berlioz Overture; Less Sylphides; Flux; Love Beyond Goodbye; Reverie; Diane et Acteon; Nais Ko; Alon Ng Buwanand Buenos Aires; The Legend And the Classic; Le Corsaire(Lankadam); Giselle;, Snow White (Huntsman);, Cinderella (The Prince);, El Adwa (Soloist); Ibong Ardana; Aria .</p>

                                    <p>Competiton :<br/>
                                    Finalist 2008 and 2010 National Music Competition for Young Artists (NAMCYA), Senior Division.<br/>
                                    Bronze Medalist 2013 in Pas de Deux division, 3rd Asian Grand Prix Hong Kong.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/thefairydoll2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/thefairydoll2018/gallery-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>--}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                            <hr>
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Normal Price</th>
                                            <th>Senior Citizen / Student / OKU </th>
                                            <th>Seat Map</p>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="background-color: #FF30C7; color: #ffffff;">Cat 1</td>
                                            <td>RM198.00</td>
                                            <td>RM159.25</td>
                                            <td rowspan="5"><img class="img-responsive seatPlanImg" src="images/thefairydoll2018/seat-plan.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #FF9700; color: #ffffff;">Cat 2</td>
                                            <td>RM158.00</td>
                                            <td>RM134.95</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #9802C6; color: #ffffff;">Cat 3</td>
                                            <td>RM128.00</td>
                                            <td>RM115.60</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #94D14C; color: #ffffff;">Cat 4</td>
                                            <td>RM88.00</td>
                                            <td>N/A</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #FF7791; color: #ffffff;">Gold</td>
                                            <td>TBA</td>
                                            <td>TBA</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Jan 20 2019 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/The Fairy Doll/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>

                            <div class="note text-left">
                                <h2>Terms & Conditions for Discount</h2>
                                <ol>
                                    <li>Senior Citizens / Student / OKU discounts can be purchased online. You must present the relevant documents during the redemption of the ticket. Failure to do so will result in your ticket being canceled and will have to pay a full price for entry on the day of the show. Please read the NOTES before purchase.</li>
                                </ol>
                            </div>

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown includes ticket fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                    <li>The discount applicable for ticket bearer only.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	$(document).ready(function(){
        $("#announcementModal").modal('show');
  	});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>Senior Citizens / Student / OKU discounts can be purchased online. You must present the relevant documents during the redemption of the ticket. Failure to do so will result in your ticket being canceled and will have to pay a full price for entry on the day of the show. Please read the NOTES before purchase.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection