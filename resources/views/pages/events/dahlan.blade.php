@extends('master')
@section('title')
    Superstars of the 80’s Live at Hard Rock Café KL
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/dahlan/dahlan-banner.jpg')">
        {{-- <video id="video-background" preload muted autoplay loop>
          <source src="images/neon/top-banner2000x450.mp4" type="video/mp4">
        </video> --}}
      </div>
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>Superstars of the 80’s Live at Hard Rock Café KL</h6>
                  Tickets from <span>RM54</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 19 February 2017 (Sunday)</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Hard Rock Café, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/9ZLpaezMUrQ2">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 2:30 pm</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <h2>ft. Jay Jay (Carefree), Jatt (Black Dog Bone) & Fredo (Flybaits)</h2>
                <p>Datuk Dahlan Zainuddin Fundraising Charity Concert at Hard Rock Café, Kuala Lumpur on February 19, 2017 will feature three Superstars from 80s band era, namely:</p>
                <ul>
                    <li>Jay Jay - Carefree</li>
                    <li>Fredo - Flybaits</li>
                    <li>Jatt - Black Dog Bone</li>
                </ul>
                <p>Jay Jay is the lead vocalist of the group Carefree. He has won many awards such “Penyanyi Popular Lelaki” pada Anugerah Bintang Popular Berita Harian (1988), “Vokal Terbaik Lelaki” pada Anugerah Industri Muzik (1998) dan “Juara Kategori Pop Rock” pada Juara Lagu (1998) with the song "Joget Angan Tak Sudah".</p>
                <p>Fredo, the lead vocalist of group Flybaits , a band formed in 1969. The band had 4 successful albums, which garnered popular hit songs titled Kenangan Lalu, Kasih Berubah, Andai Kata and Mengapa Perpisahan. In 1984, Fredo began to move as a soloist and had 4 albums to his name charting popular hits as Gurisan Terakhir, Coretan Wajahmu, Rinduku Meneruskan Percintaaan and many more, some of which was composed by Fredo himself. Fredo is still active in singing and performing till to date.</p>
                <p>Jatt is the lead vocalist of the group Black Dog Bone. A band formed in 1973 and was actively performing in Singapore. Black Dog Bone began as backup band for popular local artists of Singapore, Malaysia, Hong Kong and also Taiwan. The band became a hit among the Malaysian domestic music industry after having released their album in 1976 and producing hit songs as Si Gadis Ayu, Bila Rindu, Releks, Joget Hati Jujur , Jangan Lukakan Hatiku . Thus winning fans from all over Asia and even faraway Holand! Black Dig Bone was disbanded in 1981 after having released 7 albums. Jatt is still singing whenever there is a request and in 2009, Black Dog Bone had a reunion concert in Istana Budaya which brings back good memories to the band members.</p>
                <p>The opening act for the concert will be Melody United Music’s Own Recording Artist Kris Patt, Wafi & The Patriot Band and Alfi Alwi.</p>
                <p>Expect more artist to give a cameo performance for this charity fundraising event.</p>
                <p>Moved by his plight as a result of a stroke last July 2016, Persatuan Karyawan Malaysia (KARYAWAN) and Melody United Music presents This Charity Fund Raising Concert for the Benefit of one of the Super Star of the 70s era Datuk Dahlan Zainuddin.</p>
                <p>Dato Dahlan suffered a stroke on June 29 while attending a Breaking of Fast Event with some orphans in Perlis.</p>
                <p>The Concert hopes to raise funds to take care some of his medical expenses. Dato' Dahlan has been undergoing physiotherapy for six months at the Cheras Rehabilitation Hospital.</p>
                <p>Dato’ Dahlan Zainuddin (or stage name, Dell) 75, was a Superstar Singer and Actor in the 70;s from Malaysia who is best known for the song “Kisah Seorang Biduan”. </p>
                <p>Hailing from Ipoh, Dahlan began his career as a singer after he won best overall performance in the 1975 edition of Bintang RTM.</p>
                <p>He dabbled in acting after being given the lead role in the film “Kisah Seorang Biduan” alongside Gaya Zakry.</p>

                <p>He created history by becoming the first local singer to hold a concert at Stadium Negara in 1978. He was host of the television show “Hiburan Minggu Ini” in the 1970’s.</p>

                <p>Dahlan was also formerly a staff member of the New Straits Times and a talented skilful soccer player having played alongside the likes of Malaysian soccer legend Mokhtar Dahari.</p>

                <p>He is married with Effa Rizan who is also a famous singer in the 70s. In 2014 he received the Darjah Indera Mahkota Pahang (DIMP) from the Sultan of Pahang which carries the title Dato’.</p>
                <p>Some of his top hits from his numerous songs are:</p>
                <ul>
                    <li>Kisah Seorang Biduan</li>
                    <li>Lagu Untukmu</li>
                    <li>Wajah-Wajah Cinta</li>
                    <li>Bersama Lagu Ini</li>
                    <li>Joget Kaki Lima</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/dahlan/poster.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/dahlan/dahlan-banner.jpg" alt="">
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Normal Price <br>(include ONE soft drink)</th>
                          <th>Remarks</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td><span class="box yellow">VVIP</span></td>
                              <td>RM 254.00</td>
                              <td>Meet & Greet Buffet Package</td>
                              <td rowspan="11"><img class="img-responsive seatPlanImg" src="images/dahlan/seatplan.jpg" alt=""></td>
                          </tr>
                          <tr>
                              <td><span class="box green">Platinum</span></td>
                              <td>RM 204.00</td>
                              <td rowspan="3">Seated</td>
                          </tr>
                          <tr>
                              <td><span class="box orange">Gold</span></td>
                              <td>RM 154.00</td>
                          </tr>
                          <tr>
                              <td><span class="box red">Silver</span></td>
                              <td>RM 104.00</td>
                          </tr>
                          <tr>
                              <td><span class="box purple">Bronze</span></td>
                              <td>RM 54.00</td>
                              <td>Free Standing</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/events/dato%E2%80%99%20dahlan%20zainuddin%20charity%20fundraising%20concert/2017-2-19_14.30/hard%20rock%20caf%C3%A9%20kuala%20lumpur?hallmap" role="button">BUY TICKETS</a>
                  <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>
                
                <div class="note text-left">
                    <h2>Note</h2>
                    <ol>
                        <li>Prices shown include RM4.00 AirAsiaRedTix fee</li>
                        <li>Cover Charge Price includes One Soft Drink</li>
                        <li>The organiser reserves the right to change the flow of the agenda and times of performances as they may be additional guest artist invited to perform. The showcase is from 2.30pm-7.00pm</li>
                    </ol>
                    <h2>Reminder</h2>
                    <p><strong>Dress code</strong><br>Neat and decent. For Males shorts, slippers, and sandals are STRICTLY NOT ALLOWED.</p>
                    <p><strong>Minimum age</strong><br>6 years & above</p>
                    <p><strong>Outside F&B</strong><br>Outside Food and drinks are not allowed.</p>
                    <span class="importantNote">Each ticket is valid for one-time admission only.</span>
                    <span class="importantNote">Child must be of 95cm height and below to be eligible for child ticket priced at RM50 without seat, while child at height above 95cm must purchase tickets according to price scale.</span>
                    <h2>Important Notes</h2>
                    <ol>
                      <li>Prices shown include RM4.00 AirAsiaRedTix fee & 6% GST.</li>
                      <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                      <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                      <li>Strictly no replacement for missing tickets and cancellation.</li>
                      <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                    </ol>
                    <h2>For enquiry only:</h2>
                    <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                <dd>The Curve (TEL: 03 - 7733 1139)</dd>
                <dd>Bangsar Village (TEL: 03-22021139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
                <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
            </dl>
            </div>
        </div>
        </div>
    </div>

@endsection