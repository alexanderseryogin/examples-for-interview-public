{{-- route(@extends('master') --}}
@extends('master-event-registration')
@section('title')
    Find Order
@endsection

@section('header')
<!-- Header & Navigation -->
<header class="mainHeader">
    <div class="container-fluid">
    <nav class="navbar navbar-expand-lg" role="navigation">
        <div class="navbar-header">
            <!-- TODO -->
            <!-- <button type="button" id="search" style="display: none;">
                <span><i class="fa fa-search pull-right"></i></span>
            </button> -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
                <span class="sr-only">Toggle navigation</span>
            </button>
            <a class="navbar-brand" href="/"><img src="{!! asset('images/redtix_logo.png') !!}" alt="no nav img"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-01">
            <ul class="nav navbar-nav pull-right">
                <li><a href="/about">About Us</a></li>
                <li><a href="/faq">FAQ</a></li>
                <li><a class="newsletter-nav" data-toggle="modal" data-target="#modalSubscribeNewsletter">Newsletter</a></li>
                <li><a href="mailto:support@airasiaredtix.com">Contact Us</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav><!-- /navbar -->
    </div>
</header>
@endsection

@section('content')
<div class="container">
    <div class="vertical-center-row">
        <div  class='text-grey' align="center">
            <h4>Order Retrieval</h4>
            <span class='p-text-font'>Enter details below to register your tickets:</span><br>
            <small>Refer to your Payment Confirmation Email to view the transaction number.</small><br><br>

            <!-- Order Retrieval Form -->
            <form action="/order-retrieval" method="post" enctype="multipart/form-data" id="order-retrieval-form">
                {{ csrf_field() }}

                <input type="text" class="form-control form-input" id="transaction_number" name="transactionNumber" placeholder="Transaction number" required><br>
                <input type="email" class="form-control form-input" id="email" name="email" placeholder="Email" required>
                @if(isset($error))
                    <span style="text-align:center; color:red; display: block">{{$error}}</span>
                @endif  
                <button type="submit" class="btn btn-submit-order" id="submit">Submit</button>
                <!-- use (.btn-submit .btn-pink-shadow) once your form gets valid inputs & remove (.btn-submit-order)-->
                <!-- add .has-error if you get any error for your input -->
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $("#transaction_number").focusout(function() {
            validationCheck("transaction_number");
        })
        $("#email").focusout(function() {
            validationCheck("email");
        })

        $("#order-retrieval-form").submit(function(){
            return validate();            
        })

    });

    function validationCheck (input) {
        var obj = {};
        obj[input] = $("#"+input).val();
        $.ajax({
            url: "/validate-order",
            type: "GET",
            dataType: 'json',
            data: obj,
            success: function(data){
                if(data.success == false){
                    $("#"+input).addClass('has-error'); 
                    $("#"+input+"-error").remove();
                    $( "<span id='"+input+"-error' class='error-message'>"+data.message+"</span>" ).insertBefore( "#"+input );
                    validate();
                    return;
                }
                $("#"+input).removeClass('has-error');
                $("#"+input+"-error").remove();
                validate();
            }
        });        
    }
    function validate(){
        if ($(".has-error").length == 0 && $("#email").val() !== "" && $("#transaction_number").val() !== "" ) {
            $("#submit").removeClass('btn-submit-order');
            $("#submit").addClass('btn-submit');
            return true;
        } else {
            $("#submit").addClass('btn-submit-order');
            $("#submit").removeClass('btn-submit');
            return false;
        }
    }
</script>


@endsection

