@extends('master')
@section('title')
    Pulp Summer Slam XIX
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Pulp Summer Slam XIX" />
    <meta property="og:description" content="PULP SUMMER SLAM is the longest running metal festival in Southeast Asia, now on its 19th year”"/>
    <meta property="og:image" content="{{ Request::Url().'images/pulpslam2019/thumbnail1.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/pulpslam2019/web-banner1.jpg')}}" style="width: 100%" class="img-responsive" alt="Pulp Summer Slam XIX"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/pulpslam2019/thumbnail1.jpg')}}" style="width: 100%" class="img-responsive" alt="Pulp Summer Slam XIX">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Pulp Summer Slam XIX</h6> Tickets from <span>RM95</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  23rd Mar 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Amoranto Stadium, Manila  <a target="_blank" href="https://goo.gl/maps/k6fRtGik4pz">View Map</a></div>
                            {{-- <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Saturday, 23 Mac </div> --}}
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>Pulp Summer Slam XIX</h2><br/>
                            PULP SUMMER SLAM is the longest running metal festival in Southeast Asia, now on its 19th year. It is also the only music festival ranked by the prestigious global listing known as FESTIVAL 250. It was ranked 243 in 2016 and in 2017 it rose to 229. This upcoming 2019 the festival is the home of the only Southeast Asia date for the legendary thrash metal band SLAYER's FINAL WORLD TOUR. The award winning Metal band will be supported by a number of other Metal bands from around the world as well as top Philippine bands.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/xNcBApgKwSg?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/pulpslam2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/pulpslam2019/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pulpslam2019/gallery-2.jpg" data-featherlight="image"><img class="" src="images/pulpslam2019/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pulpslam2019/gallery-3.jpg" data-featherlight="image"><img class="" src="images/pulpslam2019/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pulpslam2019/gallery-4.jpg" data-featherlight="image"><img class="" src="images/pulpslam2019/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pulpslam2019/gallery-5.jpg" data-featherlight="image"><img class="" src="images/pulpslam2019/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pulpslam2019/gallery-6.jpg" data-featherlight="image"><img class="" src="images/pulpslam2019/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pulpslam2019/gallery-6.jpg" data-featherlight="image"><img class="" src="images/pulpslam2019/gallery-7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pulpslam2019/gallery-6.jpg" data-featherlight="image"><img class="" src="images/pulpslam2019/gallery-8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/pulpslam2019/gallery-6.jpg" data-featherlight="image"><img class="" src="images/pulpslam2019/gallery-9.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">

                            @include('layouts.partials._showticketsnodisc')

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude IDR30,000 AirAsiaRedTix fee.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close mid day prior to the event, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Type</th>
                                            <th>Ticket Price</th>
                                            <th>Seat Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size:10px;">
                                        <tr>
                                            <td rowspan="7">Pulp Royalty</td>
                                            <td>Ticket Only</td>
                                            <td rowspan="7">RM900</td>
                                            <td rowspan="18"><img class="img-responsive seatPlanImg" src="images/pulpslam2019/seat-plan.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td>Ticket Only + Official Shirt (S)</td>
                                        </tr>
                                        <tr>
                                            <td>Ticket Only + Official Shirt (M)</td>
                                        </tr>
                                        <tr>
                                            <td>Ticket Only + Official Shirt (L)</td>
                                        </tr>
                                        <tr>
                                            <td>Ticket Only + Official Shirt (XL)</td>
                                        </tr>
                                        <tr>
                                            <td>Ticket Only + Official Shirt (XXL)</td>
                                        </tr>
                                        <tr>
                                            <td>Ticket Only + Official Shirt (XXXL)</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td rowspan="6">VIP</td>
                                            <td>Ticket Only</td>
                                            <td>RM230</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>Ticket Only + Official Shirt (S)</td>
                                            <td rowspan="3">RM275</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>Ticket Only + Official Shirt (M)</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>Ticket Only + Official Shirt (L)</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>Ticket Only + Official Shirt (XL)</td>
                                            <td rowspan="2">RM294</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>Ticket Only + Official Shirt (XXL)</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="7">GA</td>
                                            <td>Ticket Only</td>
                                            <td>RM95</td>
                                        </tr>
                                        <tr>
                                            <td>Ticket Only + Official Shirt (S)</td>
                                            <td rowspan="3">RM140</td>
                                        </tr>
                                        <tr>
                                            <td>Ticket Only + Official Shirt (M)</td>
                                        </tr>
                                        <tr>
                                            <td>Ticket Only + Official Shirt (L)</td>
                                        </tr>
                                        <tr>
                                            <td>Ticket Only + Official Shirt (XL)</td>
                                            <td>RM160</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Mar 22 2019 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/pulp summer slam xix/events" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown includes AirAsiaRedTix fee and Credit card fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 1 day prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	//$(document).ready(function(){
    //    $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

    <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Temporarily down for Maintenance</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>We are performing scheduled maintenance. We should be back online shortly</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection