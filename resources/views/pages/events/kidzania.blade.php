@extends('master')
@section('title')
    Kidzania Kuala Lumpur
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Kidzania Kuala Lumpur" />
    <meta property="og:description" content="Kidzania Kuala Lumpur" />
    <meta property="og:image" content="{{ Request::Url().'images/kidzania/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/kidzania/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Kidzania Kuala Lumpur"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/kidzania/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Kidzania Kuala Lumpur">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Kidzania Kuala Lumpur</h6>Tickets from <span>RM 35.10</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                        <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  Ongoing</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Selangor, Malaysia <a target="_blank" href="https://goo.gl/maps/CTNtTPWkwdhXWx7K7">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Sun - Fri (10.00am - 5.00pm) / Saturday, Public Holiday & School Holiday (10.00am - 7.00pm) / Monday - Closed</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <p><h2>Kidzania Kuala Lumpur</h2><br/>
                                <p>KidZania Kuala Lumpur is an award-winning indoor family edutainment centre that offers interactive learning and role-play experienes for kids. The first of its kind in providing edutainment fun, KidZania empowers and inspires kids to pursue their life’s dreams by providing them an avenue to explore what it’s like to live and work in a real functioning city. KidZania’s kid-sized city experience is targeted at kids aged between 4 to 14 years which allows them to choose from over 100 job related role-plays spread across 60 various city establishments. As in the real world, children perform ""jobs"" and are either paid for their work or pay to shop or to be entertained. KidZania is built as a replica of a real functioning child-sized city, complete with buildings, paved streets, vehicles, a functioning economy, and recognizable destinations in the form of "establishments" sponsored and branded by leading multi-national and local brands.</p>

                            </div>
                            {{-- <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/artist/4BxCuXFJrSWGi1KHcVqaU4" width="300" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div>--}}
                        </div>
                        {{-- <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/qWT_W3MDFmk?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>Kidzania Kuala Lumpur</strong></h1>                                
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Seating Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img class="image-responsive" src="images/kidzania/seat-plan.jpg" style="width:100%; height:auto;" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/kidzania/gallery-1.jpg" data-featherlight="image"><img class="" src="images/kidzania/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/kidzania/gallery-2.jpg" data-featherlight="image"><img class="" src="images/kidzania/gallery-2.jpg" alt=""></a>
                                </div>                                
                                <div class="swiper-slide">
                                    <a href="images/kidzania/gallery-3.jpg" data-featherlight="image"><img class="" src="images/kidzania/gallery-3.jpg" alt=""></a>
                                </div>                                
                                <div class="swiper-slide">
                                    <a href="images/kidzania/gallery-4.jpg" data-featherlight="image"><img class="" src="images/kidzania/gallery-4.jpg" alt=""></a>
                                </div>                                
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            {{-- <div class="text-center">
                                <h1 class="subSecTitle"><strong>Buy 2 tickets for a special discounted price. Choices available for match 1,2 or 3 in CAT 2, 3, 4 and 5. Limited time only.</strong></h1>
                            </div> --}}
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>                                            
                                            <th>Type</th>                                        
                                            <th>Price</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                                            
                                            <td>Adult</td>
                                            <td><strike style="color:red">RM 50.00</strike> RM 38.50</td>                                            
                                        </tr>                                        
                                        <tr>
                                            <td>Kid</td>
                                            <td><strike style="color:red">RM 95.00</strike> RM 76.75</td>                                            
                                        </tr>
                                        <tr>
                                            <td>Toddler</td>
                                            <td><strike style="color:red">RM 46.00</strike> RM 35.10</td>                                            
                                        </tr>
                                </table>
                            </div>


                             <div class="buyAlert-bar">
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#coupon-modal">BUY TICKETS</button>
                                
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>  --}}
                            </div>


                            {{-- <span class="importantNote">*Additional charges will be applied for credit/debit card transaction, optionally customer may pay cash for outlet purchase to avoid the charges.</span> --}}
                            {{-- <div class="note text-left">
                                <h2>Reminder</h2>
                                <ol>
                                    <li><b>Dress code</b> : Neat and decent. Worn out / faded / shabby jeans, t-shirt without collar, shorts, slippers, and sandals are STRICTLY NOT ALLOWED.</li>
                                    <li><b>Age limit to show</b> : 4 years old and above. Every person/kid needs ticket to own seat.</li>
                                    <li>Food and drinks are not allowed in the auditorium.</li>
                                </ol>
                            </div> --}}
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown include RM4.00 AirAsiaRedTix fee & other taxes.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM30.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li> --}}
                                    <li>Prices shown exclude RM4 AirAsiaRedTix fee.</li>
                                    <li>Age admission limit: Toddler - 2-3 Years Old | Kid - 4-17 Years Old | Adult 18 Years Old and Above</li>
                                    <li>E-Ticket Valid for 3 Months from Date of Purchase</li>
                                    <li>Subject to available slots on date of visit.</li>
                                    {{-- <li>Online ticket selling will close 8 days prior to event day, subject to availability.</li> --}}
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

    <div class="modal coupon-modal" id="coupon-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><i class="icon icon-close"></i></button>
                <div class="modal-header" align="center">
                    <h6>DO YOU HAVE A COUPON?</h6>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{route('kidzania.coupon')}}">
                        <div style="margin-bottom:1rem" align="center">Key in your coupon here or click skip if you do not have a coupon:</div>
                        <div style="margin-bottom:1rem" class="@if($errors->any()) has-error @endif">
                            <input type="text" placeholder="Your coupon here..." class="form-control" name="code" />    
                            @if($errors->any())
                            <div align="center"><i style="color:red">Invalid code, please try again.</i></div>
                            @endif                                            
                        </div>
                        <div class="form-group" align="center">
                            <button class="btn btn-danger coupon-btn">REDEEM</button>
                        </div>
                        {{csrf_field()}}
                    </form>
                </div>
                <div class="modal-footer">                    
                    <a class="btn btn-primary coupon-btn" id="buyButton" datetime="May 01 2021 00:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/kidzaniakl/booking" role="button">NO, SKIP</a>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('customjs')

    <script type="text/javascript">
    
    @if($errors->any())
        $('#coupon-modal').modal('show');
    @endif

    $('.coupon-btn').click(function(){
        $('#coupon-modal').modal('hide');
    });

    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection