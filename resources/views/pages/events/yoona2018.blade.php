@extends('master')
@section('title')
    Yoona Fan Meeting Tour
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Yoona Fan Meeting Tour" />
    <meta property="og:description" content="Popular Girls’ Generation member and award-winning Korean drama actress YOONA will meet her fans in Singapore."/>
    <meta property="og:image" content="{{ Request::Url().'images/yoona2018/thumbnail.jpg' }}" />
@endsection

@section('content')
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="css/custom2.css">
    <link type="text/css" rel="stylesheet" href="css/clock.css">
    <link type="text/css" rel="stylesheet" href="/css/custom/redtix.css">
    <link type="text/css" rel="stylesheet" href="css/timeTo.css">

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/yoona2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Yoona Fan Meeting Tour"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/yoona2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Yoona Fan Meeting Tour">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>YOONA FAN MEETING TOUR, So Wonderful Day #Story_1 in SINGAPORE</h6> Tickets from <span>RM249</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  28th September 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Zepp@Bigbox, Level 3, Big Box 1 Venture Avenue, 608521 <a target="_blank" href="https://goo.gl/maps/NcUVPEJKmm32">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i>  8.00pm - 10.00pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-8">
                                <p><h2>HALLYU GODDESS YOONA OF GIRLS' GENERATION WILL BE IN SINGAPORE FOR HER FIRST OFFICIAL FAN MEET :<br /><b>“YOONA FANMEETING TOUR, So Wonderful Day #Story_1 in SINGAPORE”</b></h2></p>
                                <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/whAPGbHjUpM?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                                </div>
                                <p><img class="img-responsive" src="{{asset('images/yoona2018/seat-plan.jpg')}}" alt="Yoona"></p>
                                <p>Popular Girls’ Generation member and award-winning Korean drama actress YOONA will meet her fans in Singapore in “YOONA FANMEETING TOUR, So Wonderful Day #Story_1 in SINGAPORE” - her first solo fan meeting tour in Asia. The highly-anticipated Singapore fan meet, promoted by IMC Live Group, will take place on Friday, 28 September 2018, 8PM at Zepp@BigBox.</p>
                                <p>IM YOONA, professionally known as YOONA, is a South Korean pop idol active under popular girl group Girls’ Generation under SM Entertainment, and this 'YOONA FANMEETING TOUR - So Wonderful Day #Story_1' marks YOONA’s first official solo tour. The tour started in May 2018 and has since taken place in Seoul, Bangkok, Tokyo, Osaka and will continue to Hong Kong in August. As proof of her immense popularity in Asia, the tour’s Bangkok leg was sold out within minutes of launch, making her the first-ever Korean solo artist to sell out Thailand’s Thunder Dome for a fan meet.</p>
                                <p>Famed for her pure beauty and innocent charm, YOONA is an iconic Korean pop artiste often referred to as a Hallyu (read: Korean Wave) goddess in the Korean entertainment industry. One of Girls’ Generation’s most popular members, she has gone on to release several successful solo singles such as ‘To You’, ‘Deoksugung Stonewall Walkway’ and ‘When The Wind Blows’.</p>
                                <p>Outside of her musical career, YOONA has received tremendous recognition in her work as an actress. Her work as a leading actress includes ‘Confidential Assignment’, ‘You Are My Destiny’, ‘Cinderella Man’, ‘Love Rain’, ‘Prime Minister and I’, ‘THE K2’ and ‘The King in Love’. She also participated in Chinese mega project drama ‘God of War Zhao Yun’ which garnered over ten billion online views. YOONA recently completed her variety show stint in Lee Hyori’s ‘Hyori’s Homestay’.</p>
                                <p>A popular icon that has garnered lots of love and support by fans worldwide, YOONA along with her girl group Girls’ Generation was among the few groups of artistes named by the Korean Cultural Contents Agency in 2015 as the most influential artistes to impact the Hallyu Wave over the past 20 years. In 2016, YOONA has created a record of the highest online live viewers in Weibo Live Chat for a Korean artiste with a total of 20.41 million viewers last year.</p>
                                <p>YOONA, who will be performing and interacting with fans, is excited to be bringing a different fan experience for ‘YOONA FANMEETING TOUR, So Wonderful Day #Story_1 in SINGAPORE’.</p>
                                <p><img class="img-responsive" src="{{asset('images/yoona2018/fan-benefits-1.jpg')}}" alt="Yoona Fan Benefits"></p>
                                <p>Official Hashtag: #YOONA_SoWonderfulDayinSG</p>
                                <p>For latest updates, follow IMC Live Global's Official Channels:<br />
                                [FB] <a href="http://www.facebook.com/IMCLiveGlobal">http://www.facebook.com/IMCLiveGlobal</a><br />
                                [IG] <a href="http://www.instagram.com/IMCLiveGlobal">http://www.instagram.com/IMCLiveGlobal</a><br />
                                </p>
                            </div>
                            <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/artist/6LCX99hubn8CejiUtMCyyk" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/yoona2018/gallery-0x.jpg" data-featherlight="image"><img class="" src="images/yoona2018/gallery-0x.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/yoona2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/yoona2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/yoona2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/yoona2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/yoona2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/yoona2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/yoona2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/yoona2018/gallery-4.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="ticketpackages"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">

                            <span class="importantNote"><br />* First 100 ticket purchasers will get 1 x NETS Card with credit of SGD10. First come first serve basis. NETS Card to be collected at ticket redemption counter on event day.</span>
                            {{-- <span class="importantNote">* First 10 TICKETS + HOTEL BUNDLE purchasers will be entitled to a free stay in Yotel Singapore on 28th Sept 2018. Please select "MyKad (min 2 tix + Free Hotel) when purchase the tickets.</span> --}}
                            
                            
                            {{-- @include('layouts.partials._showtickets') --}}

                            <!-- Custom Tickets and Packages -->

                            <div class="text-center">
                                <h1 class="subSecTitle">Ticket & Package Prices</h1>
                            </div>
                            <!-- Hotel Tickets Sale -->
                            {{-- <div class="text-center">
                                <h1 class="subSecTitle2">Tickets + Hotel Bundle Package</h1>
                                <span class="importantNote">First 10 TICKET + HOTEL BUNDLE PACKAGE purchasers will be entitled to a free stay in Yotel Singapore (worth RM650/night), first come first serve basis. Please select "MyKad (min 2 tix) + Free Hotel" when purchase the tickets.</span>
                            </div>
                            
                            <div class="col-sm-12" id="singleticket">
                                @foreach($ticket_info as $ticket)
                                    @if($ticket["Display Type"] == "Hotel")
                                        <div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
                                            <div class="col-sm-2 text-center ticketTitle">
                                                <div class="row">
                                                    <p class="tier">{{$ticket["Ticket Tier"]}}</p>
                                                    <h1>{{$ticket["Ticket Type"]}}</h1>  
                                                </div> 
                                            </div>
                                            <div class="col-sm-3 text-center ticketDay border-left">
                                                <h6>{{ $ticket["Ticket Name"] }}</h6>
                                            </div>
                                            <div class="col-sm-4 ticketAdmission border-left">
                                                <div class="col-sm-11">
                                                    <p style="float: left;">
                                                        <span class="ticketdesc">{{ $ticket["Ticket Description"] }}</span><br><span class="smallnote">Very limited tickets. First come, first served.</span>
                                                    </p>
                                                </div>
                                                <div class="col-sm-1" style="position: absolute;right: 10px;cursor: pointer;" data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting Id"] }}">
                                                    <img src="images/assets/i.png" width="20">
                                                </div>
                                            </div>
                                            <div class="col-sm-3 ticketBuy border-left">
                                                <div class="col-sm-12 text-center">
                                                    <div class="prices">
                                                        @if(strlen($ticket["Normal Price"]) !== 0)
                                                            @if($ticket["Has Forex"] == "Yes")
                                                                <span class="forexPrice">{{$ticket["Normal Price"]}}</span>
                                                                <span class="discountPrice">{{$ticket["Discount Price"]}}</span>
                                                            @else
                                                                <span class="strikethroughPrice">{{$ticket["Normal Price"]}}</span>
                                                                <span class="discountPrice">{{$ticket["Discount Price"]}}</span>
                                                            @endif
                                                        @else
                                                        <span class="standardPrice">{{$ticket["Discount Price"]}}</span>
                                                        @endif
                                                        
                                                    </div>
                                                    <a class="btn redtixBuyBtn center-block" id="buyButton" datetime="{{ $ticket["Ticket Expiry"] }}" target="_blank" href="{{ $ticket["Ticket URL"] }}">BUY NOW</a>
                                                    <small>{{$ticket["Ticket Price Terms"]}}</small>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Mobile Tickets Sales -->
                                        <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard">
                                            <small class="discountTag"> 40% OFF</small>
                                            <div class="col-xs-12 card-body pd-0">
                                                <div class="col-xs-12 cardTop">
                                                    <div class="col-xs-6 pd-0 ticketTitle">
                                                        <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                                                        <p class="ticketTier">{{$ticket["Ticket Tier"]}} <span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
                                                        <a data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting Id"] }}"><img src="images/assets/i.png" width="20"></a>
                                                    </div>
                                                    <div class="col-xs-6 ticketPrice">
                                                        @if(strlen($ticket["Normal Price"]) !== 0)
                                                            @if($ticket["Has Forex"] == "Yes")
                                                                <p class="pull-right text-right forexPrice">{{ $ticket["Normal Price"] }}</p>
                                                                <p class="pull-right text-right">{{ $ticket["Discount Price"] }}</p>
                                                            @else
                                                                <p class="pull-right text-right strikethroughPrice">{{ $ticket["Normal Price"] }}</p>
                                                                <p class="pull-right text-right">{{ $ticket["Discount Price"] }}</p>
                                                            @endif
                                                        @else
                                                            <p class="pull-right text-right">{{ $ticket["Discount Price"] }}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="cardBottom col-xs-12 text-center">
                                                    <p>{{ $ticket["Ticket Description"] }}</p>
                                                    <a class="btn redtixBuyBtn center-block" id="buyButton" datetime="{{ $ticket["Ticket Expiry"] }}" target="_blank" href="{{ $ticket["Ticket URL"] }}">BUY NOW</a>
                                                    <small>{{$ticket["Ticket Price Terms"]}}</small>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div> --}}
                            <!-- End Hotel Ticket Sale -->


                            <!-- Bundle Tickets Sale -->
                            <div class="clear-fix"></div>
                            <div class="text-center">
                                <h1 class="subSecTitle2">Tickets Bundle Package</h1>
                                <span class="importantNote">20% less for MyKad holders when you purchase 2 tickets. Please select "MyKad (min 2 tix)" when you purchase the tickets.</span>
                            </div>
                            
                            <div class="col-sm-12" id="singleticket">
                                @foreach($ticket_info as $ticket)
                                    @if($ticket["Display Type"] == "Normal")
                                        <div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
                                            <div class="col-sm-2 text-center ticketTitle">
                                                <div class="row">
                                                    <p class="tier">{{$ticket["Ticket Tier"]}}</p>
                                                    <h1>{{$ticket["Ticket Type"]}}</h1>  
                                                </div> 
                                            </div>
                                            <div class="col-sm-3 text-center ticketDay border-left">
                                                <h6>{{ $ticket["Ticket Name"] }}</h6>
                                            </div>
                                            <div class="col-sm-4 ticketAdmission border-left">
                                                <div class="col-sm-11">
                                                    <p style="float: left;">
                                                        <span class="ticketdesc">{{ $ticket["Ticket Description"] }}</span><br><span class="smallnote">Very limited tickets. First come, first served.</span>
                                                    </p>
                                                </div>
                                                <div class="col-sm-1" style="position: absolute;right: 10px;cursor: pointer;" data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting Id"] }}">
                                                    <img src="images/assets/i.png" width="20">
                                                </div>
                                            </div>
                                            <div class="col-sm-3 ticketBuy border-left">
                                                <div class="col-sm-12 text-center">
                                                    <div class="prices">
                                                        @if(strlen($ticket["Normal Price"]) !== 0)
                                                            @if($ticket["Has Forex"] == "Yes")
                                                                <span class="forexPrice">{{$ticket["Normal Price"]}}</span>
                                                                <span class="discountPrice">{{$ticket["Discount Price"]}}</span>
                                                            @else
                                                                <span class="strikethroughPrice">{{$ticket["Normal Price"]}}</span>
                                                                <span class="discountPrice">{{$ticket["Discount Price"]}}</span>
                                                            @endif
                                                        @else
                                                        <span class="standardPrice">{{$ticket["Discount Price"]}}</span>
                                                        @endif
                                                        
                                                    </div>
                                                    <a class="btn redtixBuyBtn center-block" id="buyButton" datetime="{{ $ticket["Ticket Expiry"] }}" target="_blank" href="{{ $ticket["Ticket URL"] }}">BUY NOW</a>
                                                    <small>{{$ticket["Ticket Price Terms"]}}</small>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Mobile Tickets Sales -->
                                        <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard">
                                            {{-- <small class="discountTag"> 40% OFF</small> --}}
                                            <div class="col-xs-12 card-body pd-0">
                                                <div class="col-xs-12 cardTop">
                                                    <div class="col-xs-6 pd-0 ticketTitle">
                                                        <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                                                        <p class="ticketTier">{{$ticket["Ticket Tier"]}} <span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
                                                        <a data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting Id"] }}"><img src="images/assets/i.png" width="20"></a>
                                                    </div>
                                                    <div class="col-xs-6 ticketPrice">
                                                        @if(strlen($ticket["Normal Price"]) !== 0)
                                                            @if($ticket["Has Forex"] == "Yes")
                                                                <p class="pull-right text-right forexPrice">{{ $ticket["Normal Price"] }}</p>
                                                                <p class="pull-right text-right">{{ $ticket["Discount Price"] }}</p>
                                                            @else
                                                                <p class="pull-right text-right strikethroughPrice">{{ $ticket["Normal Price"] }}</p>
                                                                <p class="pull-right text-right">{{ $ticket["Discount Price"] }}</p>
                                                            @endif
                                                        @else
                                                            <p class="pull-right text-right">{{ $ticket["Discount Price"] }}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="cardBottom col-xs-12 text-center">
                                                    <p>{{ $ticket["Ticket Description"] }}</p>
                                                    <a class="btn redtixBuyBtn center-block" id="buyButton" datetime="{{ $ticket["Ticket Expiry"] }}" target="_blank" href="{{ $ticket["Ticket URL"] }}">BUY NOW</a>
                                                    <small>{{$ticket["Ticket Price Terms"]}}</small>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <!-- End Bundle Ticket Sale -->



                            <!-- Bundle Tickets Sale -->
                            <div class="clear-fix"></div>
                            <div class="text-center">
                                <h1 class="subSecTitle2">Single Ticket</h1>
                                <span class="importantNote">15% less for all, special limited offer at AirAsia RedTix only. Please select "LESS 15%" when you purchase the tickets.</span>
                            </div>
                            
                            <div class="col-sm-12" id="singleticket">
                                @foreach($ticket_info as $ticket)
                                    @if($ticket["Display Type"] == "Single")
                                        <div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
                                            <div class="col-sm-2 text-center ticketTitle">
                                                <div class="row">
                                                    <p class="tier">{{$ticket["Ticket Tier"]}}</p>
                                                    <h1>{{$ticket["Ticket Type"]}}</h1>  
                                                </div> 
                                            </div>
                                            <div class="col-sm-3 text-center ticketDay border-left">
                                                <h6>{{ $ticket["Ticket Name"] }}</h6>
                                            </div>
                                            <div class="col-sm-4 ticketAdmission border-left">
                                                <div class="col-sm-11">
                                                    <p style="float: left;">
                                                        <span class="ticketdesc">{{ $ticket["Ticket Description"] }}</span><br><span class="smallnote">Very limited tickets. First come, first served.</span>
                                                    </p>
                                                </div>
                                                <div class="col-sm-1" style="position: absolute;right: 10px;cursor: pointer;" data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting Id"] }}">
                                                    <img src="images/assets/i.png" width="20">
                                                </div>
                                            </div>
                                            <div class="col-sm-3 ticketBuy border-left">
                                                <div class="col-sm-12 text-center">
                                                    <div class="prices">
                                                        @if(strlen($ticket["Normal Price"]) !== 0)
                                                            @if($ticket["Has Forex"] == "Yes")
                                                                <span class="forexPrice">{{$ticket["Normal Price"]}}</span>
                                                                <span class="discountPrice">{{$ticket["Discount Price"]}}</span>
                                                            @else
                                                                <span class="strikethroughPrice">{{$ticket["Normal Price"]}}</span>
                                                                <span class="discountPrice">{{$ticket["Discount Price"]}}</span>
                                                            @endif
                                                        @else
                                                        <span class="standardPrice">{{$ticket["Discount Price"]}}</span>
                                                        @endif
                                                        
                                                    </div>
                                                    <a class="btn redtixBuyBtn center-block" id="buyButton" datetime="{{ $ticket["Ticket Expiry"] }}" target="_blank" href="{{ $ticket["Ticket URL"] }}">BUY NOW</a>
                                                    <small>{{$ticket["Ticket Price Terms"]}}</small>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Mobile Tickets Sales -->
                                        <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard">
                                            {{-- <small class="discountTag"> 40% OFF</small> --}}
                                            <div class="col-xs-12 card-body pd-0">
                                                <div class="col-xs-12 cardTop">
                                                    <div class="col-xs-6 pd-0 ticketTitle">
                                                        <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                                                        <p class="ticketTier">{{$ticket["Ticket Tier"]}} <span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
                                                        <a data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting Id"] }}"><img src="images/assets/i.png" width="20"></a>
                                                    </div>
                                                    <div class="col-xs-6 ticketPrice">
                                                        @if(strlen($ticket["Normal Price"]) !== 0)
                                                            @if($ticket["Has Forex"] == "Yes")
                                                                <p class="pull-right text-right forexPrice">{{ $ticket["Normal Price"] }}</p>
                                                                <p class="pull-right text-right">{{ $ticket["Discount Price"] }}</p>
                                                            @else
                                                                <p class="pull-right text-right strikethroughPrice">{{ $ticket["Normal Price"] }}</p>
                                                                <p class="pull-right text-right">{{ $ticket["Discount Price"] }}</p>
                                                            @endif
                                                        @else
                                                            <p class="pull-right text-right">{{ $ticket["Discount Price"] }}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="cardBottom col-xs-12 text-center">
                                                    <p>{{ $ticket["Ticket Description"] }}</p>
                                                    <a class="btn redtixBuyBtn center-block" id="buyButton" datetime="{{ $ticket["Ticket Expiry"] }}" target="_blank" href="{{ $ticket["Ticket URL"] }}">BUY NOW</a>
                                                    <small>{{$ticket["Ticket Price Terms"]}}</small>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <!-- End Bundle Ticket Sale -->


                            <!-- /End Custom Tickets and Packages -->

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Special Limited offer at AirAsia RedTix only</li>
                                    <li>Malaysian MyKad required at ticket redemption and entry into event for MyKad package</li>
                                    <li>Prices shown exclude RM4.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    {{-- <li>All ticket prices will be paid in Malaysian Ringgit. Australian Dollar prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in AUD may differ due to currency fluctuations.</li> --}}
                                    <li>All ticket prices will be paid in Malaysian Ringgit.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close a day prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	$(document).ready(function(){
      $("#announcementModal").modal('show');
  	});	

    </script>


    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
    @include('layouts.partials.modals._moreInfo')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. Special Limited offer at AirAsia RedTix only</p>
                        <p>2. Malaysian MyKad required at ticket redemption and entry into event for MyKad package</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection