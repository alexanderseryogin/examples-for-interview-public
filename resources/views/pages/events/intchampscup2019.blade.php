@extends('master')
@section('title')
    2019 International Champions Cup
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="2019 International Champions Cup" />
    <meta property="og:description" content="2019 International Champions Cup" />
    <meta property="og:image" content="{{ Request::Url().'images/intchampionscup2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/intchampionscup2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="2019 International Champions Cup"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/intchampionscup2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="2019 International Champions Cup">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>2019 International Champions Cup Presented by AIA</h6>Tickets from <span>SGD42</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                        <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  20th - 21st July 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> National Stadium, Singapore <a target="_blank" href="https://goo.gl/maps/FUXXiGGjWZo">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 7.30pm - 9.30pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <p><h2>2019 International Champions Cup Presented by AIA</h2><br/>
                                The 2019 International Champions Cup Singapore presented by AIA will see four of Europe’s top clubs play at the National Stadium in July 2019!</p>
                                <p>Catch English giants Manchester United play against Italian Serie A side Inter on Saturday, 20 July, followed by Italian powerhouse Juventus taking on English Premier League side Tottenham Hotspur on Sunday, 21 July!</p>
                                <p>The 2019 International Champions Cup tournament format is consistent with last year’s, where each team will play three matches and the club with the most points at the end of the tournament will win the championship. This year’s chase for the championship features 12 of the best clubs in the world playing 18 matches across the United States, Europe and Asia.</p>
                                <p>For more information, please visit www.internationalchampionscup.sg.</p>
                                <p>Fixtures for the 2019 International Champions Cup Singapore presented by AIA:<br/><br/>
                                Saturday, 20 July 2019<br/>
                                Manchester United vs Inter<br/><br/>
                                Sunday, 21 July 2019<br/>
                                Juventus vs Tottenham Hotspur</p>
                            </div>
                            {{-- <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/artist/4BxCuXFJrSWGi1KHcVqaU4" width="300" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div>--}}
                        </div>
                        {{-- <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/qWT_W3MDFmk?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>2019 International Champions Cup Presented by AIA</strong></h1>
                                {{-- <p>Select ticket</p> --}}
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Seating Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img class="image-responsive" src="images/intchampionscup2019/seat-plan.jpg" style="width:100%; height:auto;" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/intchampionscup2019/poster.jpg" data-featherlight="image"><img class="" src="images/intchampionscup2019/poster.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/intchampionscup2019/gallery1.jpg" data-featherlight="image"><img class="" src="images/intchampionscup2019/gallery1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            {{-- <div class="text-center">
                                <h1 class="subSecTitle"><strong>Buy 2 tickets for a special discounted price. Choices available for match 1,2 or 3 in CAT 2, 3, 4 and 5. Limited time only.</strong></h1>
                            </div> --}}
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>MATCH</th>
                                            <th>CAT 1</th>
                                            <th>CAT 2</th>
                                            <th>CAT 3</th>
                                            <th>CAT 4</th>
                                            <th>CAT 5</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Match 1</td>
                                            <td>{{-- SGD 222 --}}<font style="color:red">Sold Out</font></td>
                                            <td><font style="color:red">Sold Out</font></td>
                                            <td><font style="color:red">Sold Out</font></td>
                                            <td>{{-- SGD 72 --}}<font style="color:red">Sold Out</font></td>
                                            <td>{{-- SGD 42 --}}<font style="color:red">Sold Out</font></td>
                                        </tr>
                                        <tr>
                                            <td>Match 2</td>
                                            <td>SGD 222</td>
                                            <td>SGD 162</td>
                                            <td>SGD 102</td>
                                            <td>SGD 72</td>
                                            <td><font style="color:red">Sold Out</font></td>
                                        </tr>
                                        <tr>
                                            <td>2 DAY Package</td>
                                            <td><font style="color:red">Sold Out</font></td>
                                            <td><font style="color:red">Sold Out</font></td>
                                            <td><font style="color:red">Sold Out</font></td>
                                            <td><font style="color:red">Sold Out</font></td>
                                            <td>{{-- SGD 72.10 --}}<font style="color:red">Sold Out</font></td>
                                </table>
                            </div>

                             <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="July 19 2019 00:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/internationalchampionscup/booking" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>  --}}
                            </div>


                            {{-- <span class="importantNote">*Additional charges will be applied for credit/debit card transaction, optionally customer may pay cash for outlet purchase to avoid the charges.</span> --}}
                            {{-- <div class="note text-left">
                                <h2>Reminder</h2>
                                <ol>
                                    <li><b>Dress code</b> : Neat and decent. Worn out / faded / shabby jeans, t-shirt without collar, shorts, slippers, and sandals are STRICTLY NOT ALLOWED.</li>
                                    <li><b>Age limit to show</b> : 4 years old and above. Every person/kid needs ticket to own seat.</li>
                                    <li>Food and drinks are not allowed in the auditorium.</li>
                                </ol>
                            </div> --}}
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown include RM4.00 AirAsiaRedTix fee & other taxes.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM30.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li> --}}
                                    <li>Prices shown exclude SGD3.00 AirAsiaRedTix fee.</li>
                                    <!-- <li>Strictly no replacement for missing tickets, torn tickets and cancellation.</li> -->
                                    <li>Online ticket selling will close 1 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection