@extends('master')
@section('title')
    2017 FORMULA 1 PETRONAS MALAYSIA GRANDPRIX
@endsection

@section('header')
    @include('layouts.partials._header')
    <link rel="stylesheet" href="css/custom.css">
  <!-- Slick Slider css -->
  <link rel="stylesheet" type="text/css" href="slick/slick.css">
  <link rel="stylesheet" type="text/css" href="slick/slick-theme.css">
    @endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/formula1/formula1-banner.jpg')">
      </div>  
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>2017 FORMULA 1 PETRONAS MALAYSIA GRANDPRIX</h6>
                  Tickets from <span>RM226.24</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent gradient_red black_bg" style="margin-top:-27px">
        <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-black">

            <section class="pageCategory-section last">
{{-- After 19 years --}}
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar hidden-xs">
                            <p class="text-center after-19-years-histo">
                                After 19 years historical run,<br>the curtains are coming down for
                            </p>
                            <p class="first-created-in-199">First created in 1999, the Sepang International circuit has wowed Formula 1 fans who watched it in person, and the millions who watched at home throughout the 19-year. The circuit’s speedy straights, and magnificent corners, received universal praise. Not many has changed after its completion in 1999, but the circuit still remains one of the best, and challenging tracks for drivers. This is your final chance to wave goodbye to Malaysia’s favourite racing event, Formula 1.
                            </p>
                        </div>
                        <div class="col-sm-offset-1 col-sm-10 leftBar hidden-lg hidden-md hidden-sm">
                            <p class="after-19-years-histo" style="text-align: left">
                                After 19 years historical run,<br>the curtains are coming down for
                            </p>
                            <p class="first-created-in-199" style="text-align: left">
                                First created in 1999, the Sepang International circuit has wowed Formula 1 fans who watched it in person, and the millions who watched at home throughout the 19-year. The circuit’s speedy straights, and magnificent corners, received universal praise. Not many has changed after its completion in 1999, but the circuit still remains one of the best, and challenging tracks for drivers. This is your final chance to wave goodbye to Malaysia’s favourite racing event, Formula 1.
                            </p>
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>
{{--  --}}
                <div class="malaysian-s-reaction">
                    MALAYSIAN’S REACTIONS
                </div>
{{--  --}}
                <img src="images/formula1/car_2017_3x.png" class="car-2017 hidden-xs">
                <img src="images/formula1/car_2017_3x.png" class="car-2017_xs hidden-lg hidden-md hidden-sm">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
{{-- F1 Video Promo --}}
                            <div class="row text-center hidden-xs">
                                <iframe width="100%" height="450" src="https://www.youtube.com/embed/gsxQcHaBbGo" frameborder="0" allowfullscreen style="margin-bottom: 69px"></iframe>
                            </div>
                            <div class="row text-center hidden-lg hidden-md hidden-sm">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/gsxQcHaBbGo" frameborder="0" allowfullscreen style="margin-bottom: 69px"></iframe>
                            </div>
{{-- Video Endz --}}
                            <div class="the-beginning">
                                THE BEGINNING
                            </div>
                            <div class="hidden-xs">
                                <p class="the-sepang-international">
                                    The Sepang International. Designed by a German German engineer and circuit designer,  Hermann Tilke, it first opened its doors to FORMULA 1 in the year 1999 and has been in the FORMULA 1 calendar ever since. The first FORMULA 1 PETRONAS MALAYSIA GRAND PRIX had Eddie Irvine in pole position, while Michael Schumacher and Mika Häkkinen finished behind him respectively.
                                </p>
                                <br>
                                <p class="the-sepang-international">
                                    Legendary FORMULA 1 racers and rivalries from Michael Schumacher and Mika Häkkinen, to Sebastian Vettel and Lewis Hamilton, have graced the circuit and made those who attended and those who watched at home at their edge of their seats the whole time. Making it one of the most exciting events for Malaysians to mark on their calendar.
                                </p>
                            </div>
                            <div class="hidden-lg hidden-md hidden-sm">
                                <p class="the-sepang-international_xs">
                                    The Sepang International. Designed by a German German engineer and circuit designer,  Hermann Tilke, it first opened its doors to FORMULA 1 in the year 1999 and has been in the FORMULA 1 calendar ever since. The first FORMULA 1 PETRONAS MALAYSIA GRAND PRIX had Eddie Irvine in pole position, while Michael Schumacher and Mika Häkkinen finished behind him respectively.
                                </p>
                                <br>
                                <p class="the-sepang-international_xs">
                                    Legendary FORMULA 1 racers and rivalries from Michael Schumacher and Mika Häkkinen, to Sebastian Vettel and Lewis Hamilton, have graced the circuit and made those who attended and those who watched at home at their edge of their seats the whole time. Making it one of the most exciting events for Malaysians to mark on their calendar.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <img src="{{asset('images/formula1/graphic_car_1_01_3x.png')}}" class="graphic-car-1-01 hidden-xs">
                <img src="{{asset('images/formula1/graphic_car_1_01_3x.png')}}" class="graphic-car-1-01_xs hidden-lg hidden-md hidden-sm">

                <div class="clearfix"></div>
                <br>

                <div class="the-beginning hidden-xs" style="">
                    GALLERY
                </div>
                <div class="the-beginning_xs hidden-lg hidden-md hidden-sm" style="">
                    GALLERY
                </div>

                <br>
            </section>
{{-- Gallery images --}}
            <section class="regular slider hidden-xs" style="margin-top: 1px;z-index: 99">
                <div>
                    <img src="{{asset('images/formula1/gallery_images/sepang-international-circuit-2016-3.jpg')}}"><br>
                    <img src="{{asset('images/formula1/gallery_images/1412555856388_wps_1_Formula_One_Japan_Grand_P.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/Public-pit-lane-walk.jpg')}}"><br>
                    <img src="{{asset('images/formula1/gallery_images/F1-Fansite.com HD Wallpaper 2010 Malaysia F1 GP_25.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/8af0a564735a23a0534ef98961eb0bf6.jpg')}}"><br>
                    <img src="{{asset('images/formula1/gallery_images/best-place-to-watch-sepang-f1.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/Gp-Malesia-2013-Grid-girls-6.jpg')}}"><br>
                    <img src="{{asset('images/formula1/gallery_images/Lewis_Hamilton_leads_2012_Malaysian_GP.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/malaysiangrandprix.jpg')}}"><br>
                    <img src="{{asset('images/formula1/gallery_images/maxresdefault.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/MST-approved-F1-Header-Photo2.jpg')}}"><br>
                    <img src="{{asset('images/formula1/gallery_images/p2.jpg')}}">
                </div>
            </section>
            <section class="regular_xs slider_xs hidden-lg hidden-md hidden-sm" style="z-index:33;border: 0px solid red;margin-top: -31px">
                <div>
                    <img src="{{asset('images/formula1/gallery_images/sepang-international-circuit-2016-3.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/1412555856388_wps_1_Formula_One_Japan_Grand_P.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/Public-pit-lane-walk.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/F1-Fansite.com HD Wallpaper 2010 Malaysia F1 GP_25.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/8af0a564735a23a0534ef98961eb0bf6.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/best-place-to-watch-sepang-f1.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/Gp-Malesia-2013-Grid-girls-6.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/Lewis_Hamilton_leads_2012_Malaysian_GP.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/malaysiangrandprix.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/maxresdefault.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/MST-approved-F1-Header-Photo2.jpg')}}">
                </div>
                <div>
                    <img src="{{asset('images/formula1/gallery_images/p2.jpg')}}">
                </div>
            </section>
{{-- Gallery ends --}}
            <br><br><br>
            <img src="{{asset('images/formula1/Graphic-Car-2-01.svg')}}" class="car-2017_graphic hidden-xs">
            <img src="{{asset('images/formula1/graphic_car_2_01_3x.png')}}" class="car-2017_graphic_xs hidden-lg hidden-md hidden-sm">
{{-- after concert --}}
            <div class="container intro hidden-xs" style="">
                <div class="row">
                    <div class="col-sm-offset-1 col-sm-10 leftBar artist_div">
                        <div class="the-beginning">
                            AFTER RACE CONCERT
                        </div>
                        <div class="desktop-regular-font">
                            In celebration of the final F1 race in Malaysia,  SIC in collaboration with Mediacorp Singapore
                            will be organising the APM 2017 Prelude as the after-race concert and closing to the final race on Sunday
                        </div>
                        <div class="col-md-5 text-center" style="">
                            <br><br>
                            <b style="font-weight: 700">Indonesia</b><br>
                            <p class="desktop-regular-font">Dato' Rossa <br> Anji</p> <br>
                            <b style="font-weight: 700">Singapore</b><br>
                            <p class="desktop-regular-font">Aisyah Aziz <br>Harris Baba</p> <br>
                            <b style="font-weight: 700">Invited artist</b><br>
                            <p class="desktop-regular-font">Ayda Jebat <br>Hael Husaini</p>
                        </div>
                        <div class="col-md-7">
                            <img src="{{asset('images/formula1/artist/F1_artist_Desktop.png')}}" style="width: 100%" alt="">
                        </div>
                    </div>

                </div>
            </div>

            <div class="container intro hidden-lg hidden-md hidden-sm">
                <div class="row text-center font-white">
                    <div class="the-beginning">
                        AFTER RACE CONCERT
                    </div>
                    <div class="desktop-regular-font" style="margin: 11px;text-align: left">
                        In celebration of the final F1 race in Malaysia,  SIC in collaboration with Mediacorp Singapore
                        will be organising the APM 2017 Prelude as the after-race concert and closing to the final race on Sunday
                        <br><br>
                    </div>
                    <div class="artist_div_xs">
                        <b style="font-weight: 700">Indonesia</b><br>
                        <p class="desktop-regular-font">Dato' Rossa <br> Anji</p> <br>
                        <b style="font-weight: 700">Singapore</b><br>
                        <p class="desktop-regular-font">Aisyah Aziz <br>Harris Baba</p> <br>
                        <b style="font-weight: 700">Invited artist</b><br>
                        <p class="desktop-regular-font">Ayda Jebat <br>Hael Husaini</p>
                    </div>
                </div>
                <div class="row">
                    <img src="{{asset('images/formula1/artist/F1_artist_Mobile.png')}}" style="width: 100%" alt="">
                </div>
            </div>
{{-- after concert ends --}}

{{-- Event Schedule --}}
            <div class="container intro" style="">
                <div class="row">
                    <div class="col-sm-offset-1 col-sm-10 leftBar">
                        <div class="the-beginning">
                            <br>EVENT SCHEDULE
                        </div>
                        <div class="wrapper hidden-xs">
                            <div class="tab_schedule" id="tab_schedule1" data-no="1">
                                <input id="tab-1" type="checkbox" name="tabs">
                                <label for="tab-1">Friday, Sept 29 <b id="tab_i_1"><i style="color: #e2e2e2" class="fa fa-angle-down i_schedule_icon" aria-hidden="true"></i></b></label>
                                <div class="tab_schedule_content">
                                    <hr>
                                    <table class="table-striped">
                                        <tr>
                                            <td style="width: 7%">08:00</td>
                                            <td style="width: 7%">08:40</td>
                                            <td>PADDOCK CLUB</td>
                                            <td>PIT LANE</td>
                                            <td style="width: 50%" class="lowercase capitalize">PADDOCK CLUB PIT LANE WALK</td>
                                        </tr>
                                        <tr>
                                            <td>08:05</td>
                                            <td></td>
                                            <td>FIA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">MEDICAL INSPECTION</td>
                                        </tr>
                                        <tr>
                                            <td>08:15</td>
                                            <td>08:25</td>
                                            <td>FIA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">TRACK INSPECTION AND TRACK TEST</td>
                                        </tr>
                                        <tr>
                                            <td>08:45</td>
                                            <td>09:10<sup>1</sup></td>
                                            <td>FIA FORMULA 4 SEA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">PRACTICE SESSION</td>
                                        </tr>
                                        <tr>
                                            <td>09:30</td>
                                            <td>10:15<sup>1</sup></td>
                                            <td>PORSCHE CARRERA CUP</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">PRACTICE SESSION</td>
                                        </tr>
                                        <tr>
                                            <td>10:20</td>
                                            <td></td>
                                            <td>FIA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">MEDICAL INSPECTION</td>
                                        </tr>
                                        <tr>
                                            <td>10:30</td>
                                            <td>10:45</td>
                                            <td>FIA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">TRACK INSPECTION AND TRACK TEST</td>
                                        </tr>
                                        <tr>
                                            <td>11:00</td>
                                            <td>12:30<sup>1</sup></td>
                                            <td>FORMULA ONE</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">FIRST PRACTICE SESSION</td>
                                        </tr>
                                        <tr>
                                            <td>12:50</td>
                                            <td>13:15</td>
                                            <td>FIA FORMULA 4 SEA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">QUALIFYING SESSION</td>
                                        </tr>
                                        <tr>
                                            <td>13:30</td>
                                            <td>14:00</td>
                                            <td>PADDOCK CLUB</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">TRUCK TOUR</td>
                                        </tr>
                                        <tr>
                                            <td>13:30</td>
                                            <td>14:00</td>
                                            <td>PADDOCK CLUB</td>
                                            <td>PIT LANE</td>
                                            <td class="lowercase capitalize">PIT LANE WALK</td>
                                        </tr>
                                        <tr>
                                            <td>14:30</td>
                                            <td>14:40</td>
                                            <td>FIA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">TRACK INSPECTION</td>
                                        </tr>
                                        <tr>
                                            <td>15:00</td>
                                            <td>16:30<sup>1</sup></td>
                                            <td>FORMULA ONE</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">SECOND PRACTICE SESSION</td>
                                        </tr>
                                        <tr>
                                            <td>17:00<sup>*</sup></td>
                                            <td>17:30<sup>2</sup></td>
                                            <td>FIA FORMULA 4 SEA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">FIRST RACE (8 LAPS OR 25 MINS)</td>
                                        </tr>
                                        <tr>
                                            <td>17:00</td>
                                            <td>18:00</td>
                                            <td>FORMULA ONE</td>
                                            <td>PRESS ROOM</td>
                                            <td class="lowercase capitalize">PRESS CONFERENCE</td>
                                        </tr>
                                        <tr>
                                            <td>17:50</td>
                                            <td>18:20<sup>1</sup></td>
                                            <td>MALAYSIA CHAMPIONSHIP SERIES</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">PRACTICE SESSION</td>
                                        </tr>
                                        <tr>
                                            <td>18:00</td>
                                            <td></td>
                                            <td>FORMULA ONE</td>
                                            <td>DRIVERS’ BRIEFING ROOM</td>
                                            <td class="lowercase capitalize">DRIVERS’ MEETING</td>
                                        </tr>
                                        <tr>
                                            <td>18:30</td>
                                            <td>19:30</td>
                                            <td>PROMOTER ACTIVITY</td>
                                            <td>PIT LANE</td>
                                            <td class="lowercase capitalize">MARSHALL & PUBLIC PIT LANE WALK</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab_schedule" id="tab_schedule2" data-no="2">
                                <input id="tab-2" type="checkbox" name="tabs">
                                <label for="tab-2">Saturday, Sept 30 <b id="tab_i_2"><i style="color: #e2e2e2" class="fa fa-angle-down i_schedule_icon" aria-hidden="true"></i></b></label>
                                <div class="tab_schedule_content">
                                    <hr>
                                    <table class="table-striped">
                                        <tr>
                                            <td style="width: 7%">09:15</td>
                                            <td style="width: 7%"></td>
                                            <td>FIA</td>
                                            <td>TRACK</td>
                                            <td style="width: 50%" class="lowercase capitalize">MEDICAL INSPECTION</td>
                                        </tr>
                                        <tr>
                                            <td>09:30</td>
                                            <td>09:45</td>
                                            <td>FIA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">TRACK INSPECTION AND TRACK TEST</td>
                                        </tr>
                                        <tr>
                                            <td>10:05<sup>*</sup></td>
                                            <td>10:35<sup>2</sup></td>
                                            <td>FIA FORMULA 4 SEA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">SECOND RACE (8 LAPS OR 25 MINS)</td>
                                        </tr>
                                        <tr>
                                            <td>10:55</td>
                                            <td>11:25</td>
                                            <td>MALAYSIA CHAMPIONSHIP SERIES</td>
                                            <td>TRACK</td>
                                            <td>QUALIFYING SESSION</td>
                                        </tr>
                                        <tr>
                                            <td>11:00</td>
                                            <td>11:30</td>
                                            <td>FORMULA ONE</td>
                                            <td>PIT LANE</td>
                                            <td class="lowercase capitalize">TEAM PIT STOP PRACTICE</td>
                                        </tr>
                                        <tr>
                                            <td>11:45<sup>*</sup></td>
                                            <td>12:15<sup>2</sup></td>
                                            <td>FIA FORMULA 4 SEA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">THIRD RACE (8 LAPS OR 25 MINS)</td>
                                        </tr>
                                        <tr>
                                            <td>12:00</td>
                                            <td>13:00</td>
                                            <td>PROMOTER ACTIVITY</td>
                                            <td></td>
                                            <td class="lowercase capitalize">DRIVERS’ FAN FORUM</td>
                                        </tr>
                                        <tr>
                                            <td>12:30</td>
                                            <td>13:30</td>
                                            <td>PADDOCK CLUB</td>
                                            <td>PIT LANE</td>
                                            <td class="lowercase capitalize">PIT LANE WALK</td>
                                        </tr>
                                        <tr>
                                            <td>12:35<sup>*</sup></td>
                                            <td>13:05<sup>2</sup></td>
                                            <td>FIA FORMULA 4 SEA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">FOURTH RACE (8 LAPS OR 25 MINS)</td>
                                        </tr>
                                        <tr>
                                            <td>13:30</td>
                                            <td>13:45</td>
                                            <td>FIA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">TRACK INSPECTION AND SAFETY CAR TEST</td>
                                        </tr>
                                        <tr>
                                            <td>14:00</td>
                                            <td>15:00<sup>1</sup></td>
                                            <td>FORMULA ONE</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">THIRD PRACTICE SESSION</td>
                                        </tr>
                                        <tr>
                                            <td>15:20</td>
                                            <td>15:50</td>
                                            <td>PORSCHE CARRERA CUP</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">QUALIFYING SESSION</td>
                                        </tr>
                                        <tr>
                                            <td>15:55</td>
                                            <td>16:30</td>
                                            <td>PADDOCK CLUB</td>
                                            <td>PIT LANE</td>
                                            <td class="lowercase capitalize">PIT LANE WALK</td>
                                        </tr>
                                        <tr>
                                            <td>16:30</td>
                                            <td>16:40</td>
                                            <td>FIA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">TRACK INSPECTION</td>
                                        </tr>
                                        <tr>
                                            <td>17:00</td>
                                            <td>18:00</td>
                                            <td>FORMULA ONE</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">QUALIFYING SESSION</td>
                                        </tr>
                                        <tr>
                                            <td>18:30</td>
                                            <td>19:00</td>
                                            <td>MALAYSIA CHAMPIONSHIP SERIES</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">FIRST RACE (.... LAPS OR 25 MINS)</td>
                                        </tr>
                                        <tr>
                                            <td>19:10</td>
                                            <td>19:40</td>
                                            <td>PADDOCK CLUB</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">TRUCK TOUR</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab_schedule" id="tab_schedule3" data-no="3">
                                <input id="tab-3" type="checkbox" name="tabs">
                                <label for="tab-3">Sunday, Oct 1 <b id="tab_i_3"><i style="color: #e2e2e2" class="fa fa-angle-down i_schedule_icon" aria-hidden="true"></i></b></label>
                                <div class="tab_schedule_content">
                                    <hr>
                                    <table class="table-striped">
                                        <tr>
                                            <td style="width: 9%">09:00</td>
                                            <td style="width: 9%"></td>
                                            <td>FIA</td>
                                            <td>TRACK</td>
                                            <td style="width: 48%" class="lowercase capitalize">MEDICAL INSPECTION</td>
                                        </tr>
                                        <tr>
                                            <td>09:15</td>
                                            <td>09:30</td>
                                            <td>FIA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">TRACK INSPECTION AND TRACK TEST</td>
                                        </tr>
                                        <tr>
                                            <td>10:00<sup>*</sup></td>
                                            <td>10:30<sup>2</sup></td>
                                            <td>FIA FORMULA 4 SEA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">FIFTH RACE (8 LAPS OR 25 MINS)</td>
                                        </tr>
                                        <tr>
                                            <td>10:35</td>
                                            <td>11:20</td>
                                            <td>PADDOCK CLUB</td>
                                            <td>PIT LANE</td>
                                            <td class="lowercase capitalize">PIT LANE WALK</td>
                                        </tr>
                                        <tr>
                                            <td>10:50<sup>*</sup></td>
                                            <td>11:20<sup>2</sup></td>
                                            <td>MALAYSIA CHAMPIONSHIP SERIES</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">SECOND RACE (... LAPS OR 25 MINS)</td>
                                        </tr>
                                        <tr>
                                            <td>11:00</td>
                                            <td>12:00</td>
                                            <td>PROMOTER ACTIVITY</td>
                                            <td></td>
                                            <td class="lowercase capitalize">F1 DRIVERS’ AUTOGRAPH SESSION TBC</td>
                                        </tr>
                                        <tr>
                                            <td>11:40<sup>*</sup></td>
                                            <td>12:10<sup>2</sup></td>
                                            <td>FIA FORMULA 4 SEA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">SIXTH RACE (8 LAPS OR 25 MINS)</td>
                                        </tr>
                                        <tr>
                                            <td>12:30<sup>*</sup></td>
                                            <td>13:05<sup>2</sup></td>
                                            <td>PORSCHE CARRERA CUP</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">RACE (14 LAPS OR 30 MINS)</td>
                                        </tr>
                                        <tr>
                                            <td>13:05</td>
                                            <td>14:10</td>
                                            <td>PADDOCK CLUB</td>
                                            <td>PIT LANE</td>
                                            <td class="lowercase capitalize">PIT LANE WALK</td>
                                        </tr>
                                        <tr>
                                            <td>13:15</td>
                                            <td>13:45</td>
                                            <td>PADDOCK CLUB</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">TRUCK TOUR</td>
                                        </tr>
                                        <tr>
                                            <td>13:30</td>
                                            <td></td>
                                            <td>PADDOCK CLUB</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">DRIVERS’ TRACK PARADE</td>
                                        </tr>
                                        <tr>
                                            <td>13:45</td>
                                            <td>14:15</td>
                                            <td>FORMULA ONE</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">STARTING GRID CEREMONY & OPENING CEREMONY</td>
                                        </tr>
                                        <tr>
                                            <td>14:00</td>
                                            <td></td>
                                            <td>FIA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">MEDICAL INSPECTION</td>
                                        </tr>
                                        <tr>
                                            <td>14:10</td>
                                            <td>14:20</td>
                                            <td>FIA</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">TRACK INSPECTION</td>
                                        </tr>
                                        <tr>
                                            <td>14:30</td>
                                            <td></td>
                                            <td>FORMULA ONE</td>
                                            <td>PIT LANE</td>
                                            <td class="lowercase capitalize">PIT LANE OPEN</td>
                                        </tr>
                                        <tr>
                                            <td>14:40</td>
                                            <td></td>
                                            <td>FORMULA ONE</td>
                                            <td>PIT LANE</td>
                                            <td class="lowercase capitalize">PIT LANE CLOSED GRID FORMATION</td>
                                        </tr>
                                        <tr>
                                            <td>14:46</td>
                                            <td></td>
                                            <td>FORMULA ONE</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">NATIONAL ANTHEM</td>
                                        </tr>
                                        <tr>
                                            <td>15:00<sup>*</sup></td>
                                            <td>17:00<sup>2</sup></td>
                                            <td>FORMULA ONE</td>
                                            <td>TRACK</td>
                                            <td class="lowercase capitalize">GRAND PRIX (56 LAPS OR 120 MINS)</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper hidden-lg hidden-md hidden-sm">
                            <div class="tab_schedule tab_schedule_m" id="tab_schedule_m1" data-no="1">
                                <input id="tab-m1" type="checkbox" name="tabs">
                                <label for="tab-m1">Friday, Sept 29 <b id="tab_i_m1"><i style="color: #e2e2e2" class="fa fa-angle-down i_schedule_icon" aria-hidden="true"></i></b></label>
                                <div class="tab_schedule_content">
                                    <div class="row">
                                        <p class="date_m">08:00 - 08:40</p>
                                        <p class="schedule_desc_m">
                                            PADDOCK CLUB <br>
                                            PIT LANE <br>
                                            <?=ucfirst(strtolower('PADDOCK CLUB PIT LANE WALK'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">08:05</p>
                                        <p class="schedule_desc_m">
                                            FIA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('MEDICAL INSPECTION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">08:15 - 08:25</p>
                                        <p class="schedule_desc_m">
                                            FIA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('TRACK INSPECTION AND TRACK TEST'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">08:45 - 09:10<sup>1</sup></p>
                                        <p class="schedule_desc_m">
                                            FIA FORMULA 4 SEA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('PRACTICE SESSION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">09:30 - 10:15<sup>1</sup></p>
                                        <p class="schedule_desc_m">
                                            PORSCHE CARRERA CUP<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('PRACTICE SESSION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">10:20</p>
                                        <p class="schedule_desc_m">
                                            FIA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('MEDICAL INSPECTION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">10:30 - 10:45</p>
                                        <p class="schedule_desc_m">
                                            FIA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('TRACK INSPECTION AND TRACK TEST'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">11:00 - 12:30<sup>1</sup></p>
                                        <p class="schedule_desc_m">
                                            FORMULA ONE<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('FIRST PRACTICE SESSION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">12:50 - 13:15</p>
                                        <p class="schedule_desc_m">
                                            FIA FORMULA 4 SEA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('QUALIFYING SESSION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">13:30 - 14:00</p>
                                        <p class="schedule_desc_m">
                                            PADDOCK CLUB<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('TRUCK TOUR'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">13:30 - 14:00</p>
                                        <p class="schedule_desc_m">
                                            PADDOCK CLUB<br>
                                            PIT LANE<br>
                                            <?=ucfirst(strtolower('PIT LANE WALK'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">14:30 - 14:40</p>
                                        <p class="schedule_desc_m">
                                            FIA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('TRACK INSPECTION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">15:00 - 16:30<sup>1</sup></p>
                                        <p class="schedule_desc_m">
                                            FORMULA ONE<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('SECOND PRACTICE SESSION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">17:00<sup>*</sup> - 17:30<sup>2</sup></p>
                                        <p class="schedule_desc_m">
                                            FIA FORMULA 4 SEA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('FIRST RACE (8 LAPS OR 25 MINS)'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">17:00 - 18:00</p>
                                        <p class="schedule_desc_m">
                                            FORMULA ONE<br>
                                            PRESS ROOM<br>
                                            <?=ucfirst(strtolower('PRESS CONFERENCE'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">17:50 - 18:20<sup>1</sup></p>
                                        <p class="schedule_desc_m">
                                            MALAYSIA CHAMPIONSHIP SERIES<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('PRACTICE SESSION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">18:00</p>
                                        <p class="schedule_desc_m">
                                            FORMULA ON<br>
                                            DRIVERS’ BRIEFING ROOM<br>
                                            <?=ucfirst(strtolower('DRIVERS’ MEETING'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">18:30 - 19:30</p>
                                        <p class="schedule_desc_m">
                                            PROMOTER ACTIVITY<br>
                                            PIT LANE<br>
                                            <?=ucfirst(strtolower('MARSHALL & PUBLIC PIT LANE WALK'))?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab_schedule tab_schedule_m" id="tab_schedule_m2" data-no="2">
                                <input id="tab-m2" type="checkbox" name="tabs">
                                <label for="tab-m2">Saturday, Sept 30 <b id="tab_i_m2"><i style="color: #e2e2e2" class="fa fa-angle-down i_schedule_icon" aria-hidden="true"></i></b></label>
                                <div class="tab_schedule_content">
                                    <div class="row">
                                        <p class="date_m">09:15</p>
                                        <p class="schedule_desc_m">
                                            FIA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('MEDICAL INSPECTION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">09:30 - 09:45</p>
                                        <p class="schedule_desc_m">
                                            FIA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('TRACK INSPECTION AND TRACK TEST'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">10:05<sup>*</sup> - 10:35<sup>2</sup></p>
                                        <p class="schedule_desc_m">
                                            FIA FORMULA 4 SEA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('SECOND RACE (8 LAPS OR 25 MINS)'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">10:55 - 11:25</p>
                                        <p class="schedule_desc_m">
                                            MALAYSIA CHAMPIONSHIP SERIES<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('QUALIFYING SESSION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">11:00 - 11:30</p>
                                        <p class="schedule_desc_m">
                                            FORMULA ONE<br>
                                            PIT LANE<br>
                                            <?=ucfirst(strtolower('TEAM PIT STOP PRACTICE'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">11:45<sup>*</sup> - 12:15<sup>2</sup></p>
                                        <p class="schedule_desc_m">
                                            FIA FORMULA 4 SEA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('THIRD RACE (8 LAPS OR 25 MINS)'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">12:00 - 13:00</p>
                                        <p class="schedule_desc_m">
                                            PROMOTER ACTIVITY<br>
                                            &nbsp;<br>
                                            <?=ucfirst(strtolower('DRIVERS’ FAN FORUM'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">12:30 - 13:30</p>
                                        <p class="schedule_desc_m">
                                            PADDOCK CLUB<br>
                                            PIT LANE<br>
                                            <?=ucfirst(strtolower('PIT LANE WALK'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">12:35<sup>*</sup> - 13:05<sup>2</sup></p>
                                        <p class="schedule_desc_m">
                                            FIA FORMULA 4 SEA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('FOURTH RACE (8 LAPS OR 25 MINS)'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">13:30 - 13:45</p>
                                        <p class="schedule_desc_m">
                                            FIA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('TRACK INSPECTION AND SAFETY CAR TEST'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">14:00 - 15:00<sup>1</sup></p>
                                        <p class="schedule_desc_m">
                                            FORMULA ONE<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('THIRD PRACTICE SESSION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">15:20 - 15:50</p>
                                        <p class="schedule_desc_m">
                                            PORSCHE CARRERA CUP<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('QUALIFYING SESSION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">15:55 - 16:30</p>
                                        <p class="schedule_desc_m">
                                            PADDOCK CLUB<br>
                                            PIT LANE<br>
                                            <?=ucfirst(strtolower('PIT LANE WALK'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">16:30 - 16:40</p>
                                        <p class="schedule_desc_m">
                                            FIA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('TRACK INSPECTION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">17:00 - 18:00</p>
                                        <p class="schedule_desc_m">
                                            FORMULA ONE<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('QUALIFYING SESSION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">18:30 - 19:00</p>
                                        <p class="schedule_desc_m">
                                            MALAYSIA CHAMPIONSHIP SERIES<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('FIRST RACE (.... LAPS OR 25 MINS)'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">19:10 - 19:40</p>
                                        <p class="schedule_desc_m">
                                            PADDOCK CLUB<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('TRUCK TOUR'))?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab_schedule tab_schedule_m" id="tab_schedule_m3" data-no="3">
                                <input id="tab-m3" type="checkbox" name="tabs">
                                <label for="tab-m3">Sunday, Oct 1 <b id="tab_i_m3"><i style="color: #e2e2e2" class="fa fa-angle-down i_schedule_icon" aria-hidden="true"></i></b></label>
                                <div class="tab_schedule_content">
                                    <div class="row">
                                        <p class="date_m">09:00</p>
                                        <p class="schedule_desc_m">
                                            FIA <br>
                                            TRACK <br>
                                            <?=ucfirst(strtolower('MEDICAL INSPECTION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">09:15 - 09:30</p>
                                        <p class="schedule_desc_m">
                                            FIA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('TRACK INSPECTION AND TRACK TEST'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">10:00<sup>*</sup> - 10:30<sup>2</sup></p>
                                        <p class="schedule_desc_m">
                                            FIA FORMULA 4 SEA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('FIFTH RACE (8 LAPS OR 25 MINS)'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">10:35 - 11:20</p>
                                        <p class="schedule_desc_m">
                                            PADDOCK CLUB <br>
                                            PIT LANE<br>
                                            <?=ucfirst(strtolower('PIT LANE WALK'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">10:50<sup>*</sup> - 11:20<sup>2</sup></p>
                                        <p class="schedule_desc_m">
                                            MALAYSIA CHAMPIONSHIP SERIES <br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('SECOND RACE (... LAPS OR 25 MINS)'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">11:00 - 12:00</p>
                                        <p class="schedule_desc_m">
                                            PROMOTER ACTIVITY<br>
                                            &nbsp;<br>
                                            <?=ucfirst(strtolower('F1 DRIVERS’ AUTOGRAPH SESSION TBC'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">11:40<sup>*</sup> - 12:10<sup>2</sup></p>
                                        <p class="schedule_desc_m">
                                            FIA FORMULA 4 SEA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('SIXTH RACE (8 LAPS OR 25 MINS)'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">12:30<sup>*</sup> - 13:05<sup>2</sup></p>
                                        <p class="schedule_desc_m">
                                            PORSCHE CARRERA CUP<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('RACE (14 LAPS OR 30 MINS)'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">13:05 - 14:10</p>
                                        <p class="schedule_desc_m">
                                            PADDOCK CLUB<br>
                                            PIT LANE<br>
                                            <?=ucfirst(strtolower('PIT LANE WALK'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">13:15 - 13:45</p>
                                        <p class="schedule_desc_m">
                                            PADDOCK CLUB<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('TRUCK TOUR'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">13:30</p>
                                        <p class="schedule_desc_m">
                                            PADDOCK CLUB<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('DRIVERS’ TRACK PARADE'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">13:45 - 14:15</p>
                                        <p class="schedule_desc_m">
                                            FORMULA ONE<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('STARTING GRID CEREMONY & OPENING CEREMONY'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">14:00</p>
                                        <p class="schedule_desc_m">
                                            FIA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('MEDICAL INSPECTION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">14:10 - 14:20</p>
                                        <p class="schedule_desc_m">
                                            FIA<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('TRACK INSPECTION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">14:30</p>
                                        <p class="schedule_desc_m">
                                            FORMULA ONE<br>
                                            PIT LANE<br>
                                            <?=ucfirst(strtolower('PIT LANE OPEN'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">14:40</p>
                                        <p class="schedule_desc_m">
                                            FORMULA ONE<br>
                                            PIT LANE<br>
                                            <?=ucfirst(strtolower('PIT LANE CLOSED GRID FORMATION'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">14:46</p>
                                        <p class="schedule_desc_m">
                                            FORMULA ONE<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('NATIONAL ANTHEM'))?>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <p class="date_m">15:00<sup>*</sup> - 17:00<sup>2</sup></p>
                                        <p class="schedule_desc_m">
                                            FORMULA ONE<br>
                                            TRACK<br>
                                            <?=ucfirst(strtolower('GRAND PRIX (56 LAPS OR 120 MINS)'))?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="font-white">
                            <small>*This times refers to the start formation lap</small><br>
                            <sup>1</sup> Fixed end session <br>
                            <sup>2</sup> Approximate finishing time
                        </div>

                    </div>


                </div>
            </div>

        

                                </div>
                                <div class="col-md-4">
                                    <span class="event_description_header">Features</span>
                                    <p class="event_description">
                                    <ul>
                                        <li>Covered</li>
                                        <li>Seated Viewing</li>
                                        <li>Unreserved Position</li>
                                        <li>TV Screen (subject to change)</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="tab" data-no="5">
                            <input class="input_checkbox_tab" id="tab5" type="checkbox" name="tabs">
                            <label for="tab5">K1 Grandstand <p class="hidden-lg hidden-md hidden-sm" style="margin-bottom: -41px">RM 391.68</p><span class="pull-right price hidden-xs">RM 391.68</span> <br><small>Free seating</small>
                                <br><span id="tab-more5">MORE <i style="color: #fdfdfd" class="fa fa-angle-down" aria-hidden="true"></i></span></label>
                            <div class="tab-content">
                                <p><img src="{{asset('images/formula1/f1_gallery/K1_Grandstand.jpg')}}" style="width: 100%" alt=""></p>
                                <div class="col-md-8">
                                    <span class="event_description_header">Description</span>
                                    <p class="event_description" style="margin-left: auto">
                                        You’ll get the opportunity to have a grand view of the main straight (its like the car coming right at you!). Plus, you have the best seats to overlook the first 2 corners of the circuit which is a tight right hand corner, followed by an immediate left turn.
                                    </p>
                                    <p class="event_description" style="margin-left: auto">
                                        <b>
                                            *Earn 10x BIG Points when you purchase normal price K1 Grandstand tickets (1,750 BIG Points)
                                        </b>
                                    </p>
                                    <br>
                                    <span class="event_description_header">Parking</span>
                                    <p class="event_description">
                                        <ul>
                                            <li>Bay 1 : 550 m to Welcome centre</li>
                                            <li>Bay 2 : 700 m to Welcome centre</li>
                                            <li>Bay 3 : 550 m to Welcome centre</li>
                                        </ul>
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <span class="event_description_header">Features</span>
                                    <p class="event_description">
                                    <ul>
                                        <li>Covered</li>
                                        <li>Seated Viewing</li>
                                        <li>Unreserved Position</li>
                                        <li>TV Screen (subject to change)</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="tab" data-no="6">
                            <input class="input_checkbox_tab" id="tab6" type="checkbox" name="tabs">
                            <label for="tab6">F Grandstand <p class="hidden-lg hidden-md hidden-sm" style="margin-bottom: -41px">RM 280.68</p><span class="pull-right price hidden-xs">RM 280.68</span> <br><small>Free seating</small>
                                <br><span id="tab-more6">MORE <i style="color: #fdfdfd" class="fa fa-angle-down" aria-hidden="true"></i></span></label>
                            <div class="tab-content">
                                <p><img src="{{asset('images/formula1/f1_gallery/F_Grandstand.jpg')}}" style="width: 100%" alt=""></p>
                                <div class="col-md-8">
                                    <span class="event_description_header">Description</span>
                                    <p class="event_description" style="margin-left: auto">
                                        This seat is a bang for your buck. This section will allow you to view both North and South Main Grandstand (best of both worlds!). Plus, you get a panoramic view of the whole circuit.
                                    </p>
                                    <p class="event_description" style="margin-left: auto">
                                        This includes a view of turn 5,6,7,8, and a clear sight of the hairpin turn 15 (hopefully some last-minute overtakes on the final lap *fingers crossed*)
                                    </p>
                                    <p class="event_description" style="margin-left: auto">
                                        <b>
                                            *Earn 10x BIG Points when you purchase normal price F Grandstand tickets (1,250 BIG Points)
                                        </b>
                                    </p>
                                    <br>
                                    <span class="event_description_header">Parking</span>
                                    <p class="event_description">
                                        <ul>
                                            <li>Bay 14 : 600 m to Gate F</li>
                                            <li>Bay 15 : 650 m to Gate F</li>
                                            <li>Bay 17 : 1,050 m to Gate F</li>
                                        </ul>
                                    </p>

                                </div>
                                <div class="col-md-4">
                                    <span class="event_description_header">Features</span>
                                    <p class="event_description">
                                    <ul>
                                        <li>Covered</li>
                                        <li>Seated Viewing</li>
                                        <li>Unreserved Position</li>
                                        <li>TV Screen (subject to change)</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="tab" data-no="7">
                            <input class="input_checkbox_tab" id="tab7" type="checkbox" name="tabs">
                            <label for="tab7">Hillstand C2 Covered <p class="hidden-lg hidden-md hidden-sm" style="margin-bottom: -41px">RM 114.18</p><span class="pull-right price hidden-xs">RM 114.18</span> <br><small>Free seating</small>
                                <br><span id="tab-more7">MORE <i style="color: #fdfdfd" class="fa fa-angle-down" aria-hidden="true"></i></span></label>
                            <div class="tab-content">
                                <p><img src="{{asset('images/formula1/f1_gallery/Hillstand_C2_Covered.jpg')}}" style="width: 100%" alt=""></p>
                                <div class="col-md-8">
                                    <span class="event_description_header">Description</span>
                                    <p class="event_description" style="margin-left: auto">
                                        This section is located on the south-side of the circuit. You can enjoy some of the most challenging turns, which is turn 9 and 10.
                                        All this at the comfort of a covered roof above you. Great seats for a great price.
                                    </p>
                                    <br>
                                    <span class="event_description_header">Parking</span>
                                    <p class="event_description">
                                        <ul>
                                            <li>Bay 6 : 550 m to Gate C2</li>
                                            <li>Bay 7 : 550 m to Gate C2</li>
                                            <li>Bay 8 : 900 m to Gate C2</li>
                                            <li>Bay 9 : 1,200 m to Gate C2</li>
                                        </ul>
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <span class="event_description_header">Features</span>
                                    <p class="event_description">
                                    <ul>
                                        <li>Covered</li>
                                        <li>Banking/Ground Viewing</li>
                                        <li>Unreserved Position</li>
                                        <li>TV Screen (subject to change)</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="tab" data-no="8">
                            <input class="input_checkbox_tab" id="tab8" type="checkbox" name="tabs">
                            <label for="tab8">Hillstand K2 Uncovered <p class="hidden-lg hidden-md hidden-sm" style="margin-bottom: -41px">RM 91.98</p><span class="pull-right price hidden-xs">RM 91.98</span> <br><small>Free seating</small>
                                <br><span id="tab-more8">MORE <i style="color: #fdfdfd" class="fa fa-angle-down" aria-hidden="true"></i></span></label>
                            <div class="tab-content">
                                <p><img src="{{asset('images/formula1/f1_gallery/Hillstand_K2_Uncovered.jpg')}}" style="width: 100%" alt=""></p>
                                <div class="col-md-8">
                                    <span class="event_description_header">Description</span>
                                    <p class="event_description" style="margin-left: auto">
                                        Some fun in the sun! This section is located directly in the third corner and is beside the K1 Grandstand.
                                        You’ll be enjoying the race with a view of turn 1 until turn 4.
                                    </p>
                                    <br>
                                    <span class="event_description_header">Parking</span>
                                    <p class="event_description">
                                        <ul>
                                            <li>Bay 1 : 550 m to Welcome centre</li>
                                            <li>Bay 2 : 700 m to Welcome centre</li>
                                            <li>Bay 3 : 550 m to Welcome centre</li>
                                        </ul>
                                    </p>

                                </div>
                                <div class="col-md-4">
                                    <span class="event_description_header">Features</span>
                                    <p class="event_description">
                                    <ul>
                                        <li>Banking/Ground Viewing</li>
                                        <li>Unreserved Position</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <img class="finale" src="{{asset('images/formula1/graphic-finishing-line-mock-up-01.png')}}" alt="">
{{-- Terms & Conditions --}}
            <div class="container intro">
                <div class="row">
                    <div class="col-sm-offset-1 col-sm-10 leftBar">
                        <div class="text-center">
                            <a href="https://redtix-tickets.airasia.com/en-AU/events/2017%20formula%201%20petronas%20malaysia%20grand%20prix%20(29%20september%20-%201%20october%202017)/2017-9-29_9.00/sepang%20international%20circuit?back=2&area=f8a112b4-3523-47ff-b950-79af61947d98&type=ga" type="button" class="btn btn-default btn-round-xs btn-xs banner_btn" style="cursor: pointer">Buy Tickets</a>
                        </div>
                        <br><br>
                        <div class="prices-shown-inclu">
                            * Prices shown include RM4.00 AirAsiaRedTix fee & 6% GST
                            <br><br>
                            IMPORTANT NOTES
                            <ol class="font-white">
                                <li>Prices shown inclusive RM4.00 AirAsiaRedTix fee & 6% GST.</li>
                                <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                <li>Strictly no replacement for missing tickets and cancellation.</li>
                                <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                <li>Member can earn 1 BIG Point for every MYR2 (excluding shipping, taxes or other admin fees) spent on Formula 1 ticket purchasing. Earn 10X BIG Points for purchasing normal price tickets of Main Grandstand, K1 Grandstand and F Grandstand.</li>
                                <li>Redeem your AirAsia BIG points for 2017 FORMULA 1 PETRONAS MALAYSIA GRAND PRIX, 
                                    send us your Full Name, Transaction Number & BIG member ID to <a href="mailto:marketing@airasiaredtix.com">marketing@airasiaredtix.com</a></li>
                            </ol>
                            FOR ENQUIRY ONLY:
                            <p>
                                <span class="font-white" style="font-size: 16px">Email to</span> <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.
                            </p>

                        </div>
                    </div>
                </div>
            </div>

        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('modal')

  <!--Modal Seat Plan image-->
  <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <div class="clearfix">&nbsp;</div>
          <img src="" class="enlargeImageModalSource" style="width: 100%;">
          </div>
      </div>
      </div>
  </div>
  
  <!-- Modal Where to Get Tix Location-->
  <div id="modalGetTixLoc" class="modal fade" role="dialog">
      <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <h2>GET YOUR TICKETS FROM:</h2>
          <dl class="dl-horizontal">
              <dt>Online:</dt>
              <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Rock Corner outlets:</dt>
              <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
              <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
              <dd>Bangsar Village (TEL: 03-22021139)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Victoria Music outlets:</dt>
              <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
              <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
              <dd>Tropicana</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Penang outlets:</dt>
              <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
              <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Istana Budaya Jalan<br>Tun Razak:</dt>
              <dd>03 40265558</dd>
          </dl>
          </div>
      </div>
      </div>
  </div>

@endsection

@section('customjs')
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="slick/slick.min.js"></script>
    <!--  <script src="slick/slick.js" type="text/javascript" charset="utf-8"></script> -->

    <script type="text/javascript">
        //Initialize Swiper
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            slidesPerView: 'auto',
            spaceBetween: 10,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            freeMode: true
        });

        // Enlarge Seat Plan Image
        $(function() {
            $('.seatPlanImg').on('click', function() {
                $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
                $('#enlargeImageModal').modal('show');
            });
        });

        // Hide top Banner when page scroll
        var header = $('.eventBanner');
        var range = 450;

        $(window).on('scroll', function () {

            var scrollTop = $(this).scrollTop();
            var offset = header.offset().top;
            var height = header.outerHeight();
            offset = offset + height;
            var calc = 1 - (scrollTop - offset + range) / range;

            header.css({ 'opacity': calc });

            if ( calc > '1' ) {
                header.css({ 'opacity': 1 });
            } else if ( calc < '0' ) {
                header.css({ 'opacity': 0 });
            }
        });

        // Smooth scroll for acnhor links
        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });

        $(document).ready(function(){
            $(".regular").slick({
                dots: true,
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 2
            });

            $(".regular_xs").slick({
                dots: true,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1
            });

            $(".tab").click(function(evt){

                var tabNo = $(this).attr('data-no');

                var isChecked = $('input#tab'+tabNo).is(':checked');

                if(isChecked) {   // about to open
                    var tabs;
                    for(tabs = 1;tabs <= 8;tabs++)
                    {
                        if(tabs != tabNo)
                        {
                            $('input#tab'+tabs).removeAttr('checked');
                            $('#tab-more'+tabs).html('MORE <i style="color: #fdfdfd" class="fa fa-angle-down" aria-hidden="true"></i>');
                        }
                    }
                    $('#tab-more'+tabNo).html('LESS <i style="color: #fdfdfd" class="fa fa-angle-up" aria-hidden="true"></i>');
                }
                else {    // about to close
                    $('#tab-more'+tabNo).html('MORE <i style="color: #fdfdfd" class="fa fa-angle-down" aria-hidden="true"></i>');
                }

            });


            $(".tab_schedule").click(function() {
                var tabNo = $(this).attr('data-no');
                var isChecked = $('input#tab-'+tabNo).is(':checked');
                if(isChecked)
                {
                    var schedule_tab;
                    for(schedule_tab = 1;schedule_tab<=3;schedule_tab++)
                    {
                        if(schedule_tab != tabNo)
                        {
                            $('#tab_schedule'+schedule_tab+' label').css({'background-color':'rgba(252, 252, 252, 0.65','color':'#ffffff','padding-top':'0'});
                            $('input#tab-'+schedule_tab).removeAttr('checked');
                            $('b#tab_i_'+schedule_tab).html('<i style="color: #e2e2e2" class="fa fa-angle-down i_schedule_icon" aria-hidden="true"></i>');
                        }
                    }
                    $('#tab_schedule'+tabNo+' label').css({'background-color':'#ffffff','color':'#717171','padding-top':'13px'});
                    $('b#tab_i_'+tabNo).html('<i style="color: #717171" class="fa fa-angle-up i_schedule_icon" aria-hidden="true"></i>');
                }
                else {
                    $('#tab_schedule'+tabNo+' label').css({'background-color':'rgba(252, 252, 252, 0.65','color':'#ffffff','padding-top':'0'});
                    $('b#tab_i_'+tabNo).html('<i style="color: #e2e2e2" class="fa fa-angle-down i_schedule_icon" aria-hidden="true"></i>');
                }
            });

            $(".tab_schedule_m").click(function() {
                var tabNo = $(this).attr('data-no');
                var isChecked = $('input#tab-m'+tabNo).is(':checked');
                if(isChecked)
                {
                    var schedule_tab;
                    for(schedule_tab = 1;schedule_tab<=3;schedule_tab++)
                    {
                        if(schedule_tab != tabNo)
                        {
                            $('#tab_schedule_m'+schedule_tab+' label').css({'background-color':'rgba(252, 252, 252, 0.65','color':'#ffffff'});
                            $('input#tab-m'+schedule_tab).removeAttr('checked');
                            $('b#tab_i_m'+schedule_tab).html('<i style="color: #e2e2e2" class="fa fa-angle-down i_schedule_icon" aria-hidden="true"></i>');
                        }
                    }
                    $('#tab_schedule_m'+tabNo+' label').css({'background-color':'#ffffff','color':'#717171'});
                    $('b#tab_i_m'+tabNo).html('<i style="color: #717171" class="fa fa-angle-up i_schedule_icon" aria-hidden="true"></i>');
                }
                else {
                    $('#tab_schedule_m'+tabNo+' label').css({'background-color':'rgba(252, 252, 252, 0.65','color':'#ffffff'});
                    $('b#tab_i_m'+tabNo).html('<i style="color: #e2e2e2" class="fa fa-angle-down i_schedule_icon" aria-hidden="true"></i>');
                }
            });



        });

        // Modal Announcement
        //$(document).ready(function(){
        //    $("#announcementModal").modal('show');
        //});
    </script>
  
@endsection