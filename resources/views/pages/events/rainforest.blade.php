@extends('master')
@section('title')
    Fantasy Rainforest
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner hidden-xs" style="background-image: url('images/rainforest/rainforest-banner.jpg')"></div>
      <div class="widewrapper main hidden-lg hidden-md hidden-sm">
        <img src="{{asset('images/rainforest/rainforest-thumb.jpg')}}" width="100%" class="img-responsive" alt="Fantasy Rainforest">
      </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>Fantasy Rainforest</h6>
                  Tickets from <span>RM60</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> Daily from 1 January 2018 – 31 December 2018</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Hall A, Putrajaya International Convention Centre <a target="_blank" href="https://www.google.com/maps/place/Putrajaya+International+Convention+Centre/@2.894816,101.6751504,17z/data=!3m1!4b1!4m5!3m4!1s0x31cdb7966ecf97f7:0x9a346bf504069e0d!8m2!3d2.894816!4d101.6773391?q=Hall+A,+Putrajaya+International+Convention+Centre&um=1&ie=UTF-8&sa=X&ved=0ahUKEwiEy4Xa-obRAhWKQo8KHYnFDekQ_AUICCgB">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 12.30pm &amp; 6.00pm (daily)</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <p>Malaysia's first ever world–class live performance – "FANTASY RAINFOREST" promote the unique, and multiculturalism of the country. This is an approximately 10 million ringgit cost live performance that interspersed with acrobatics, street dance, gymnastics, bamboo dance, and magic show. Let's enjoy the Malaysia Rainforest's unique rhapsody together with Malaysia's multicultural elements, flora and fauna. The authentic Malaysian cultures will be presented to you.</p>
                <p>Malaysia is blessed with abundant natural resources. The rainforest covering two–third of its surface is known as one of the oldest in the world – as ancient as 130 million years old. Besides, it also serves as the habitat for nearly 20% species of animals in the world. By understanding Malaysia, you will fall in love with Malaysia, a place that full of vitality, a place that melt all races and religion as one. Malaysia, a homeland for Malays, Indians, Chinese and other ethnic, we respect each other, we love each other, and we live in harmony. Besides that, Malaysian had been influence by the multiculturalism, it not only made us a gastronomical paradise but also a variety of cultural festival, and this is why Malaysian love celebration and social, why Malaysian are temperament elegant, warm and friendly.</p>
                <p>"FANTASY RAINFOREST", encapsulates the concept of "love", "tolerance" and "harmony" through the role of a hunter. The storyline begins with a harmonious state in the Malaysian rainforest, in which the aboriginal people and wildlife are living together peacefully. However, the harmony is later stirred by a hunter who sets up traps to poach the endangered animals in Malaysia – Malayan tigers, monkeys, peacocks and other animals, and he even shoot the Malaysia's national bird – the hornbill with a shotgun and caused the anger among the aborigines. The hunter then suffer the consequences himself, he caught by the traps set himself and arrested by the aborigines. However, with the theme of 'Love' and 'Harmony', hornbills did not choose to hurt the hunter but to forgive him. At last, the hunter had hand his gun down and join to be part of the big family and live together peacefully.</p>
                <p>The story represents the harmony in Malaysia – people of all races live together in peace and accept each other's differences, subsequently bringing forth a beautiful Malaysia. In addition, the "FANTASY RAINFOREST" also presents the unique flora of Malaysia – hibiscus, Rafflesia, rainforest vine and others. A 60 minutes live performance, consist of amazing performance such as gymnastics, somersaults and magic show, a team of 30 performers who dressed in traditional costumes which to reveal their true strength, are ready for you. </p><br>
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/KwrmDefaPS4?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/rainforest/fantasyrainforest-02.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/rainforest/fantasyrainforest-03.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/rainforest/fantasyrainforest-04.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/rainforest/fantasyrainforest-05.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/rainforest/fantasyrainforest-06.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/rainforest/fantasyrainforest-09.jpg" alt="">
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Normal Price</th>
                          <th>RedTix Discount</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>VIP</td>
                              <td>RM 200.00</td>
                              <td>RM 153.00</td>
                              <td rowspan="3">Free Seating</td>
                          </tr>
                          <tr>
                              <td>Adult</td>
                              <td>RM 120.00</td>
                              <td>RM 93.00</td>
                          </tr>
                          <tr>
                              <td>Child (5 & 12 years)</td>
                              <td>RM 60.00</td>
                              <td>RM 48.00</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger" id="buyButton" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/fantasy%20rainforest/events" role="button">BUY TICKETS</a>
                  <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>
                <span class="importantNote">* Prices shown include RM4.00 AirAsiaRedTix fee</span>
                <span class="importantNote">* Outlet &amp; Online ticket selling close 1 Days prior to event show day, subject to availability. Ticket will not be valid within the same day of purchased. Validation of ticket shall be one (1) day after the purchased date.</span>
                <div class="note text-left">
                  <h2>Important Notes</h2>
                  <ol>
                    <li>Prices shown inclusive RM4.00 AirAsiaRedTix fee &amp; 6% GST.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                  </ol>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        {{-- <section class="pageCategory-section last">
          <div class="buyNowSec" style="background-image: url('images/event2.jpg')">
            <div class="bannerOverlay"></div>
            <div class="container">
              <div class="col-sm-offset-1 col-sm-10 quoteArea">
                <h1>You’ve heard him on the radio, you’ve seen him on TV. It’s about time. See him live!</h1>
              </div>
              <div class="btnArea">
                <a class="btn btn-black btn-lg" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/flash%20run%202017/events" role="button">GET TICKET ONLINE NOW <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <div class="clearfix">&nbsp;</div>
                <span data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets</span>
              </div>
            </div>
          </div>
        </section> --}}

        {{-- <section class="pageCategory-section last">
          <div class="container">
            <div class="partner text-center">
                <h1 class="subSecTitle"><strong>PARTNERS</strong></h1>
                <div class="clearfix">&nbsp;</div>
                <ul class="list-unstyled list-inline partnerList">
                    <li><img src="images/neon/aabig_logo (offical partner).png" alt=""></li>
                    <li><img src="images/neon/BAC.png" alt=""></li>
                    <li><img src="images/neon/durex (official partner).png" alt=""></li>
                    <li><img src="images/neon/fac3 (co-organiser).jpg" alt=""></li>
                    <li><img src="images/neon/seven friday (official watch).jpeg" alt=""></li>
                    <li><img src="images/neon/sunway lagoon (official venue).png" alt=""></li>
                </ul>
            </div>
          </div>
        </section> --}}

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                <dd>Bangsar Village (TEL: 03-22021139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
               <dd>Artist Gallery, Gurney Plaza (04-228 5648)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-637 0191)</dd>
            </dl>
            </div>
        </div>
        </div>
    </div>

@endsection