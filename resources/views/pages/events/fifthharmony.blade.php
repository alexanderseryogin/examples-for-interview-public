@extends('master')
@section('title')
    FIFTH HARMONY: The 7/27 Tour Live in Kuala Lumpur
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/fifthharmony/fifthharmony-banner.jpg')">
      </div>
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>FIFTH HARMONY: The 7/27 Tour Live in Kuala Lumpur</h6>
                  Tickets from <span>RM104</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 7 April 2017, Friday</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Sunway Lagoon @ Surf Beach <a target="_blank" href="https://www.google.com/maps/place/Sunway+Lagoon/@3.0692108,101.604838,17z/data=!3m1!4b1!4m5!3m4!1s0x31cc4c88dded3125:0xdb654cc77af0cdbc!8m2!3d3.0691643!4d101.6069192">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 7 pm</div>
                <hr>
                <p>From the stage of The X Factor to opening for Cher Lloyd, Demi Lovato and Austin Mahone to headlining tours all around the world, the beautiful and incredibly talented girl group Fifth Harmony will be touring South East Asia for the very first time.</p>
                <p>After leaving The X Factor, Fifth Harmony has gained rapid success with their debut single "Miss Movin' On", which was certified gold by the Recording Industry Association of America (RIAA). Its music video won the group the MTV Video Music Award for Artist to Watch. Their debut extended play (EP), Better Together (2013), had a first week position of number six on the US Billboard 200. The group released their debut studio album Reflection in 2015, debuting at number five on the Billboard 200 and receiving a gold certification from the RIAA. The album included the platinum singles "Boss", "Sledgehammer" and "Worth It".</p>
                <p>The latter achieved triple platinum certification in the United States and reached the top-ten in thirteen countries. "Work from Home", the lead single from their second album 7/27 (2016), became the group's first top-ten single on the US Billboard Hot 100 and the first top-five by a girl group in a decade on that chart.</p>
                <p>Fans can expect Ally Brooke, Normani Kordei, Dinah Jane and Lauren Jauregui perform their mega hits LIVE at Surf Beach, Sunway Lagoon on April 7th, 2017. Doors open at 7pm.</p>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="lineup text-center">
                <h1 class="subSecTitle"><strong>THE LINEUP</strong></h1>
                <p>Performers that will be performed</p>
                <ul class="list-unstyled list-inline lineupList">
                    <li><img src="images/fifthharmony/Ally-Brooke.jpg" alt="">Dinah Jane</li>
                    <li><img src="images/fifthharmony/Normani-Hamilton.jpg" alt="">Normani Kordei</li>
                    <li><img src="images/fifthharmony/Dinah-Jane.jpg" alt="">Ally Brooke</li>
                    <li><img src="images/fifthharmony/Lauren-Jauregui.jpg" alt="">Lauren Jauregui</li>
                </ul>
            </div>
            <div class="gallery">
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/fifthharmony/poster.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/fifthharmony/fifth-harmony-twitter.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/fifthharmony/1280_fifth_harmony_twitter.jpg" alt="">
                  </div>                  
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Categories</th>
                          <th>Phase 1<br>(20/01/17 till 5/02/17)<br>While ticket last</th>
                          <th>Phase 2<br>(6/02/17 onwards)</th>
                          <th>Seating</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td><span class="box orange">VIP</span></td>
                            <td>RM 354.00<br><strong>Sold Out</strong></td>
                            <td>RM 454.00</td>
                            <td rowspan="3">Free Standing</td>
                          </tr>
                          <tr>
                            <td><span class="box red">Harmony</span></td>
                            <td>RM 154.00<br><strong>Sold Out</strong></td>
                            <td>RM 254.00</td>
                          </tr>
                          <tr>
                            <td><span class="box blue">Party</span></td>
                            <td>RM 104.00<br><strong>Sold Out</strong></td>
                            <td>RM 154.00</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/fifth harmony - the 7-27 tour live in malaysia/events" role="button">BUY TICKETS</a>
                  <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>
                <div class="note text-left">
                    <h2>Note</h2>
                    <ol>
                        <li>Phase 1 promo period from 20 Jan till 5 Feb 2017 and while tickets last.</li>
                        <li>Prices stated are inclusive RM4.00 ticketing fee</li>
                        <li>Doors open at 7pm.</li>
                    </ol>
                    <h2>Important Notes</h2>
                    <ol>
                        <li>Prices shown include RM4.00 AirAsiaRedTix fee & 6% GST.</li>
                        <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                        <li>RM5.00 reprint fee applicable if the online ticket purchaser  fail to present the e-ticket for redemption on the event day.</li>
                        <li>Strictly no replacement for missing tickets and cancellation.</li>
                        <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                        <li>Flash photography is not allowed.</li>
                    </ol>
                    <h2>For enquiry only:</h2>
                    <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Announcement-->
    {{-- <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>In support of International Women's Day today, 08 March 2017 we would like to support our ladies by having <strong class="text-danger">10% off all tickets categories for 2 days ONLY</strong>.</p>
                    </div>

                </div>
            </div>
        </div>
    </div> --}}

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                <dd>Bangsar Village (TEL: 03-22021139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
                <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
            </dl>
            </div>
        </div>
        </div>
    </div>

@endsection