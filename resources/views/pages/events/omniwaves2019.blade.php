@extends('master')
@section('title')
    OMNI Waves 2019
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="OMNI Waves 2019" />
    <meta property="og:description" content="Minds will be blown at OMNI, Malaysia’s first-ever and largest indoor live music festival!"/>
    <meta property="og:image" content="{{ Request::Url().'images/omniwaves2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/omniwaves2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="OMNI Waves 2019"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/omniwaves2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="OMNI Waves 2019">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>OMNI Waves 2019</h6> Tickets from <span>RM299</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  23rd March 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Malaysia International Trade and Exhibition Centre, Malaysia <a target="_blank" href="https://goo.gl/maps/Shq9V5v9y3C2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 6.00pm - 1.00am</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <p><h2>OMNI Waves 2019</h2><br/>
                                Minds will be blown at OMNI, Malaysia’s first-ever and largest indoor live music festival!</p>
                                <p>Take your senses on an epic, transcendental space odyssey as OMNI whisks you across galaxies of many genres to the tune of live performances by stellar artists.</p>
                                <p>Combining psychedelic light and sound effects with an out-of-this-world stage design and captivating pyro effects, your voyage with OMNI will surely uplift your mind to a whole new soundscape.</p>
                                <p>No spacesuit required. Standby for lift-off.</p>
                            </div>
                            {{-- <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/artist/6deZN1bslXzeGvOLaLMOIF" width="300" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div> --}}
                        </div>
                        {{-- <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/PhrY3yep1OQ?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/omniwaves2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/omniwaves2019/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/omniwaves2019/gallery-2.jpg" data-featherlight="image"><img class="" src="images/omniwaves2019/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/omniwaves2019/gallery-3.jpg" data-featherlight="image"><img class="" src="images/omniwaves2019/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/omniwaves2019/gallery-4.jpg" data-featherlight="image"><img class="" src="images/omniwaves2019/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/omniwaves2019/gallery-5.jpg" data-featherlight="image"><img class="" src="images/omniwaves2019/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/omniwaves2019/gallery-6.jpg" data-featherlight="image"><img class="" src="images/omniwaves2019/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/omniwaves2019/gallery-7.jpg" data-featherlight="image"><img class="" src="images/omniwaves2019/gallery-7.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Remarks</th>
                                            <th>Seat Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>VIP</td>
                                            <td>RM 299.00</td>
                                            <td>VIP ticket comes with 1 complimentary drink<br/><br/>Promotion ticket (with promo code) drink is not included.</td>
                                            <td><img class="img-responsive seatPlanImg" src="images/omniwaves2019/seat-plan.jpg" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger disabled" id="buyButton" datetime="Mar 22 2019 00:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/omniwaves2019/booking" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Transaction fee of RM8.00 per event ticket applicable for Internet purchase.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Adult: Strictly 18 years old and above.</li>
                                    <li>For event's Terms &amp; Conditions, visit <a href="www.omni-waves.com/termsandconditions">www.omni-waves.com/termsandconditions</a></li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });


    // Modal Announcement
	//$(document).ready(function(){
    //  $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. Special Limited offer at AirAsia RedTix only</p>
                        <p>2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection