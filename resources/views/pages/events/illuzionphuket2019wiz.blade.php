@extends('master')
@section('title')
    Wiz Khalifa at Illuzion | THU 12 SEP
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Wiz Khalifa at Illuzion | THU 12 SEP" />
    <meta property="og:description" content="Wiz Khalifa at Illuzion | THU 12 SEP"/>
    <meta property="og:image" content="{{ Request::Url().'images/illuzionphuket2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/illuzionphuket2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Wiz Khalifa at Illuzion | THU 12 SEP"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/illuzionphuket2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Wiz Khalifa at Illuzion | THU 12 SEP">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Wiz Khalifa at Illuzion | THU 12 SEP</h6> Tickets from <span>THB2,000</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  12th Sep 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Illuzion Phuket, Thailand <a target="_blank" href="https://goo.gl/maps/UYT4jfgiGEdGMkhJ9">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i>  9.00pm - Late</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <h2>Wiz Khalifa at Illuzion | THU 12 SEP</h2><br/>                                

                                <p>Three-time Billboard Award winner & multi-grammy award nominated American Rap superstar Wiz Khalifa will make a stop in Phuket on Thursday September 12th for an epic performance at #IlluzionPhuket with his top chart smashing hits.</p>

                                <p>In 2011, he scored his first big hit with “Black and Yellow.” Khalifa also co-starred with Snoop Dogg in the comedy Mac and Devin Go to High School around this time. He went on to make such popular rap albums as O.N.I.F.C. (2012) and Blacc Hollywood (2014). In 2015 Khalifa enjoyed huge success with “See You Again,” a track that was nominated for 21 awards, and won 7, including Top rap song of the year.</p>

                                <p>Don’t miss your chance to see one of the biggest rap acts in the scene at the # 1 club in Thailand!</p>
                            </div>
                            {{-- <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/artist/6deZN1bslXzeGvOLaLMOIF" width="300" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div> --}}
                        </div>
                        {{-- <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/P8ZRL8FGMNw?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </section>
         
            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Tier</th>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                        
                                        <tr>
                                            <td>Early Bird</td>
                                            <td>Early Bird</td>
                                            <td><strike style="color:red">SOLD OUT</strike></td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2">Tier 1</td>
                                            <td>GA</td>
                                            <td><strike style="color:red">SOLD OUT</strike></td>
                                        </tr>
                                        <tr>                                            
                                            <td>GA + Open Bar<br/><i style="font-size:11px">Unlimited Drinks from 09.00 PM. - 01.00 AM</i></td>
                                            <td><strike style="color:red">SOLD OUT</strike></td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2">Tier 2</td>
                                            <td>GA</td>
                                            <td>THB 2,000</td>
                                        </tr>
                                        <tr>                                            
                                            <td>GA + Open Bar<br/><i style="font-size:11px">Unlimited Drinks from 09.00 PM. - 01.00 AM</i></td>
                                            <td>THB 2,700</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Aug 18 2019 00:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/wizkhalifailluzion/booking" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                                        
                                <h2>Terms & Conditions</h2>
                                <ol>
                                    <li>Open Bar Package : Unlimited Drinks from 09.00 PM. - 01.00 AM</li>
                                    <li>This Ticket is Non Refundable / Non Returnable / บัตรนี้ไม่สามารถคืนหรือเปลี่ยนเป็นเงินสดได้ </li>
                                    <li>This Ticket is Valid for Date of Show issued Only / บัตรนี้ใช้ได้เฉพาะวันแสดง และรอบการแสดงในบัตรเท่านั้น</li>
                                    <li>All admission age is 20 years and older / จำกัดอายุสำหรับ 20 ปีขึ้นไป</li>
                                </ol>

                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude AirAsiaRedTix fee</li>                                                                        
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });


    // Modal Announcement
	//$(document).ready(function(){
    //  $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. Special Limited offer at AirAsia RedTix only</p>
                        <p>2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection