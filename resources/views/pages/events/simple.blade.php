@extends('master')
@section('title')
    A SIMPLE SPACE by Gravity & Other Myths
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/simple/simple-banner.jpg')">
      </div>
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>A SIMPLE SPACE by Gravity & Other Myths</h6>
                  Tickets from <span>RM25</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 11 - 12th August 2017, (Friday - Saturday)</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Dewan Sri Pinang Auditorium, George Town, Penang <a target="_blank" href="https://goo.gl/maps/qNKZwx9yoFv">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8.30pm (11 August), 3.30pm (12 August), (60 minutes without intermission)</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <h2>A SIMPLE SPACE by Gravity & Other Myths</h2>
                <p>- Photo by: Chris Herzfeld</p>
                <p>“Cirque du Soleil with a fistful of grit” - Express UK</p>
                <p>“Mind blowing physical theatre.” - Talk Fringe, Edinburgh Fringe Festival</p>
                <p>“Stripped back and raw…an awe inspiring display of strength, skill and creativity.” - Adelaide Advertiser</p>
                <p>“This is circus in the 21st Century” - Lip Mag</p>
                <p>“A total triumph!” - The Guardian</p>
                <p>Premiered with short formats at Adelaide Fringe 2013 and Edinburgh Fringe 2013, the low-fi but high skill production, A Simple Space, is a worldwide sensation that leaves audience standing and a formidable string of 5-star reviews in its wake.</p>
                <p>A Simple Space has toured to 14 countries on six continents, wowing audiences with a universal physical language and joyful energy and comes to George Town Festival as a Southeast Asian premiere.</p>
                <p>“Falling!” can be heard from the top of a single-human-tower before trust weighs completely down to rest. Bodies are presented to be light as a feather when tossed from a grip onto another; the elasticity through the performers’ veins could be questioned from the amount of body twisting, weaving and climbing to shape human structures.</p>
                <p>Seven acrobats push their physical limits without reserve; this performance is simultaneously raw, frantic and delicate. Supported by driving live percussion and presented so intimately that you can feel the heat, hear every breath, and be immersed in every moment.</p>
                <p>A Simple Space evokes real responses in audiences, something visceral rather than cerebral. Instead of fine-tuning the performance with makeup, lighting and contrived theatrical overlay, the cast has deliberately gone the opposite way. The audience is brought in close to surround the stripped back stage. In that space the acrobats are pushed to the physical limit, breaking down their usual guards and introducing the reality of failure and weakness. With nothing left to hide behind personal narratives come through naturally. This honesty is the essence behind A Simple Space.</p>
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/Fio_KPtkjjY" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/simple/simple-1.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/simple/simple-2.jpg" alt="">
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Category</th>
                          <th>Normal Price</th>
                          <th>Early Bird Price <br>(24 May - 23 June 2017)</th>
                          <th>Senior Citizen,<br> Child (5 - 12 years),<br> OKU (Disabled)</th>
                          <th>Seating Plan</th> 
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td><span class="box midblue">STUDENT</td>
                              <td>RM 25</td>
                              <td>Not Applicable</td>
                              <td>Not Applicable</td>
                              <td rowspan="4"><img class="img-responsive seatPlanImg" src="images/simple/floorplan.png" alt="">
                          </tr>
                          <tr>
                              <td><span class="box pink">GALLERY</td>
                              <td>RM 65</td>
                              <td>RM 46</td>
                              <td>RM 33</td>
                          </tr>
                          <tr>
                              <td><span class="box yellow">STAGE SIDE</td>
                              <td>RM 100</td>
                              <td>RM 70</td>
                              <td>RM 50</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/gtf 2017- a simple space - gravity - other myths/events" role="button">BUY TICKETS</a>
                  <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>
                <span class="importantNote">* Additional charges will be applied for credit/debit card transaction, optionally customer may pay cash for outlet purchase to avoid the charges.</span>
                <div class="note text-left">
                    <h2>Admission Rules</h2>
                    <ol>
                        <li>Students and senior citizen ticket holders are obliged to present proof of identification at the door.</li>
                        <li>Out of consideration for both the audience and artists, <strong class="text-danger">children under the age of 5 are not allowed admission.</strong></li>
                    </ol>
                    <h2>Late Seating Policy</h2>
                    <ol>
                        <li>Latecomers will not be seated once the performance has begun. No refunds will be made for patrons arriving late.</li>
                        <li>If you must leave the performance at any time once it has begun, you will not be re-admitted until an appropriate break.</li>
                    </ol>
                    <h2>Photography Rules</h2>
                    <ol>
                        <li>No photography, video recording and audio recording is allowed for this event.</li>
                    </ol>
                </div>
                <div class="note text-left">
                    <h2>Important Notes</h2>
                    <ol>
                      <li>Prices shown include RM4.00 AirAsiaRedTix fee.</li>
                      <li>Subject to 6% GST and credit card charges.</li>
                      <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                      <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                      <li>Strictly no replacement for missing tickets and cancellation.</li>
                      <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                    </ol>
                    <h2>For enquiry only:</h2>
                    <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        {{-- <section class="pageCategory-section last">
          <div class="buyNowSec" style="background-image: url('images/event2.jpg')">
            <div class="bannerOverlay"></div>
            <div class="container">
              <div class="col-sm-offset-1 col-sm-10 quoteArea">
                <h1>You’ve heard him on the radio, you’ve seen him on TV. It’s about time. See him live!</h1>
              </div>
              <div class="btnArea">
                <a class="btn btn-black btn-lg" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/konsert%20gegar%20pop%20yeh%20yeh/events" role="button">GET TICKET ONLINE NOW <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <div class="clearfix">&nbsp;</div>
                <span data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets</span>
              </div>
            </div>
          </div>
        </section> --}}

        {{-- <section class="pageCategory-section last">
          <div class="container">
            <div class="partner text-center">
                <h1 class="subSecTitle"><strong>PARTNERS</strong></h1>
                <div class="clearfix">&nbsp;</div>
                <ul class="list-unstyled list-inline partnerList">
                    <li><img src="images/neon/aabig_logo (offical partner).png" alt=""></li>
                    <li><img src="images/neon/BAC.png" alt=""></li>
                    <li><img src="images/neon/durex (official partner).png" alt=""></li>
                    <li><img src="images/neon/fac3 (co-organiser).jpg" alt=""></li>
                    <li><img src="images/neon/seven friday (official watch).jpeg" alt=""></li>
                    <li><img src="images/neon/sunway lagoon (official venue).png" alt=""></li>
                </ul>
            </div>
          </div>
        </section> --}}

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                <dd>Bangsar Village (TEL: 03-22021139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
                <dd>Tropicana</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
                <dd>Artist Gallery, Gurney Plaza (04-228 5648)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-637 0191)</dd>
            </dl>
            </div>
        </div>
        </div>
    </div>

@endsection