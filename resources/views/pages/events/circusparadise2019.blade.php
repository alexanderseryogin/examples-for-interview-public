@extends('master')
@section('title')
    Circus Paradise Festival
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Circus Paradise Festival" />
    <meta property="og:description" content="From the creator of Future Music Festival & Summafieldayze proudly brings to you a new boutique festival for Queensland."/>
    <meta property="og:image" content="{{ Request::Url().'images/circusparadise2019/thumbnail.jpg' }}" />
@endsection

@section('content')
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="css/custom2.css">
    <link type="text/css" rel="stylesheet" href="css/clock.css">
    <link type="text/css" rel="stylesheet" href="/css/custom/redtix.css">
    <link type="text/css" rel="stylesheet" href="css/timeTo.css">

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/circusparadise2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Circus Paradise Festival"></div>
        {{-- <div class="jumbotron embed-responsive embed-responsive-16by9 hidden-xs">
            <iframe height="500" style="padding-top:20px;" class="embed-responsive-item" src="https://www.youtube.com/embed/5NAXh192wKs?rel=0&showinfo=0&autoplay=1&controls=0&loop=1&modestbranding=0" allow="autoplay" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </div> --}}
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/circusparadise2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Circus Paradise Festival">
            {{-- <iframe height="25%" width="100%" class="embed-responsive-item" src="https://www.youtube.com/embed/5NAXh192wKs?rel=0&showinfo=0&autoplay=1&controls=0&loop=1&modestbranding=0" allow="autoplay" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe> --}}
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Circus Paradise Festival</h6> Tickets from <span>RM260</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  10th Mar 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Broadwater Parklands, Gold Coast <a target="_blank" href="https://goo.gl/maps/LwgMsmVxX9m">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 12.00pm </div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-8">
                                <p><h2>Circus Paradise Festival</h2></p>
                                <p>From the creator of Future Music Festival & Summafieldayze proudly brings to you a new boutique festival in Queensland. Welcome to the magical world of Circus Paradise Festival, an annual one-day festival that will take place on the beautiful beach side of the Broadwater Parklands Gold Coast. Covering all spectrums of quality electronic dance and live music featuring a carefully selected line up of world-class local and International artists performing across four purpose-built stages.</p>
                                <p>This year's line-up includes:<br/>
                                Charlotte de Witte, Oliver Huntemann, Motez, Total Giovanni (Live), Leftfield (DJ Set), Manuel De La Mare, Tim Engelhardt (Live), Supernova, Noizu, CC:Disco!, Gavin Rayna Russom (LCD Soundsystem), Rob Hes, Torren Foot, Banoffee (Live), Jolyon Petch, Anthony Pappa, Made in Paris, Cassette, Ivey (Live), Peach Fur (Live), Taglo, Mark James, Penelope Two Fives (Live), Audun, Rattlesnake (DJs), Mitch Grey, Jamie Grenenger, Batchelo, Tim Fuchs, Giv, Ctrl Alt Del, Christian Kerr, Rascal, Latour, Tash Losan.</p>
                                <p>More to be announced soon.</p>
                            </div>
                            <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/user/ftw7fjm86k0nxcplnrt65sdzj/playlist/5fflyG8pWTO9NbK51e4kIa" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

        <!-- Sponsors -->
        <section class="pageCategory-section last section-grey">
                <div class="container tixPrice text-center">
                    <h1 class="subSecTitle"><strong>SPONSORS</strong></h1>
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="col-sm-offset-1 col-sm-2">
                                <img src="{{asset('images/circusparadise2019/sponsor-1.png')}}" class="img-responsive" alt="Circus Paradise Festival">
                            </div>
                            <div class="col-sm-offset-1 col-sm-2">
                                <img src="{{asset('images/circusparadise2019/sponsor-2.png')}}" class="img-responsive" alt="Circus Paradise Festival">
                            </div>
                            <div class="col-sm-offset-1 col-sm-2">
                                <img src="{{asset('images/circusparadise2019/sponsor-3.png')}}"  class="img-responsive" alt="Circus Paradise Festival">
                            </div>
                            <div class="col-sm-offset-1 col-sm-2">
                                <img src="{{asset('images/circusparadise2019/sponsor-4.png')}}" class="img-responsive" alt="Circus Paradise Festival">
                            </div>
                        </div>
                        <div class="col-sm-offset-2 col-sm-10 ">
                            <div class="col-sm-offset-1 col-sm-2">
                                <img src="{{asset('images/circusparadise2019/sponsor-5.png')}}" class="img-responsive" alt="Circus Paradise Festival">
                            </div>
                            <div class="col-sm-offset-1 col-sm-2">
                                <img src="{{asset('images/circusparadise2019/sponsor-6.png')}}" class="img-responsive" alt="Circus Paradise Festival">
                            </div>
                            <div class="col-sm-offset-1 col-sm-2">
                                <img src="{{asset('images/circusparadise2019/sponsor-7.png')}}"  class="img-responsive" alt="Circus Paradise Festival">
                            </div>
                        </div>
                        <div class="col-sm-1 clearfix">
                    </div>
                </div>
            </section>
        <!-- /Sponsors -->

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/circusparadise2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/circusparadise2019/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/circusparadise2019/gallery-2.jpg" data-featherlight="image"><img class="" src="images/circusparadise2019/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/circusparadise2019/gallery-3.jpg" data-featherlight="image"><img class="" src="images/circusparadise2019/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/circusparadise2019/gallery-4.jpg" data-featherlight="image"><img class="" src="images/circusparadise2019/gallery-4.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">

                            @include('layouts.partials._showtickets')

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown exclude RM4.00 AirAsiaRedTix fee and 3% Credit card fee.</li> --}}
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Australian Dollar prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in AUD may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close a day prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	//$(document).ready(function(){
    //    $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
    @include('layouts.partials.modals._moreInfo')

    <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Temporarily down for Maintenance</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>We are performing scheduled maintenance. We should be back online shortly</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection