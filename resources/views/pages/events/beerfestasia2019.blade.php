@extends('master')
@section('title')
    Beerfest Asia 2019
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Beerfest Asia 2019" />
    <meta property="og:description" content="Beerfest Asia 2019" />
    <meta property="og:image" content="{{ Request::Url().'images/beerfestasia2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/beerfestasia2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Beerfest Asia 2019"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/beerfestasia2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Beerfest Asia 2019">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Beerfest Asia 2019</h6>Tickets from <span>SGD19</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                        <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  27th - 30th June 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Marina Promenade (Behind Singapore Flyer & F1 Pit Building)
Downtown Core, Singapore 038975 <a target="_blank" href="https://goo.gl/maps/gpppaSTC5FNy7u3S9">View Map</a></div>
                            {{-- <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 7.30pm - 9.30pm</div> --}}
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <h2>Beerfest Asia 2019</h2><br/>
                                <p>Beerfest Asia is set to bring you the future of the beer industry this 11th edition!</p>

                                <p>Let’s look forward to the future of beer with innovation; in products, in technology. The 11th edition of Beerfest Asia will feature more than 600 local and international beers including new launches and all-time award winners, an upgraded live entertainment line-up of international tribute bands, homegrown artistes and DJs, and a wide variety of gastronomic treats.</p>

                                <p>Beer buddies can look forward to a workout of a different kind at the first-ever Beerfest Run. Participants can down a beer at each pit stop along a 2km route around the festival ground, and each finisher gets a 1-litre mug to refill at various exhibitors at a promotional price for the whole night.</p>

                                <p>Also making its debut at Beerfest Asia 2019 is “Ignition”, an extravaganza that encompasses music, lasers, lighting and video mapping within an air-conditioned tent for an experience like no other. Showcasing the latest in entertainment technology, the laserfest features “Venus Rising” performers, a roster of international and regional female DJs championing the “Venus Rising” platform of “The Future is Female”. (Separate ticket charges applies, to be ready for sale soon)</p>

                                <p>Beerfest Asia will transform into a family carnival on Sunday with inflatables and games stalls, suitable for children and pets. Beer Workshops and Comedy sessions will be back in action too. So much to look forward to so what are you waiting for? Grab your passes today!</p>
                            </div>
                            {{-- <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/artist/4BxCuXFJrSWGi1KHcVqaU4" width="300" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div>--}}
                        </div>
                        {{-- <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/qWT_W3MDFmk?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>Beerfest Asia 2019</strong></h1>                                
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Seating Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img class="image-responsive" src="images/beerfestasia2019/seat-plan.jpg" style="width:100%; height:auto;" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
 --}}
            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">                                
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-2.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-3.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-4.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-5.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-6.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-7.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-8.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-9.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-9.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-10.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-10.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-11.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-11.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-12.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-12.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-13.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-13.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-14.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-14.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-15.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-15.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-16.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-16.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-17.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-17.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/beerfestasia2019/gallery-18.jpg" data-featherlight="image"><img class="" src="images/beerfestasia2019/gallery-18.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            {{-- <div class="text-center">
                                <h1 class="subSecTitle"><strong>Buy 2 tickets for a special discounted price. Choices available for match 1,2 or 3 in CAT 2, 3, 4 and 5. Limited time only.</strong></h1>
                            </div> --}}
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>DAY</th>
                                            <th>TIME</th>
                                            <th>General<br/><i>(Includes 1 Welcome Beer)</i></th>
                                            <th>VIP<br/><i>(Includes Free Flow VIP Beer)</i></th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Day 1 (27 Jun 2019)</td>      
                                            <td>6.00pm - 1.00am</td>                                      
                                            <td><strike style="color:red">SGD 22</strike> SGD 19<br/><i style="font-size:11px">Limited Time</i></td>
                                            <td><strike style="color:red">SGD 124</strike> SGD 106<br/><i style="font-size:11px">Limited Time</i></td>                                            
                                        </tr>
                                        <tr>
                                            <td>Day 2 (28 Jun 2019)</td>                                            
                                            <td>5.00pm - 2.00am</td>                                      
                                            <td><strike style="color:red">SGD 27</strike> SGD 23.25<br/><i style="font-size:11px">Limited Time</i></td>
                                            <td><strike style="color:red">SGD 164</strike> SGD 140<br/><i style="font-size:11px">Limited Time</i></td>                                            
                                        </tr>
                                        <tr>
                                            <td>Day 3 (29 Jun 2019)</td>                                            
                                            <td>4.00pm - 2.00am</td>                                      
                                            <td><strike style="color:red">SGD 27</strike> SGD 23.25<br/><i style="font-size:11px">Limited Time</i></td>
                                            <td><strike style="color:red">SGD 164</strike> SGD 140<br/><i style="font-size:11px">Limited Time</i></td> 
                                        </tr>
                                        <tr>
                                            <td>Day 4 (30 Jun 2019)</td>                                            
                                            <td>2.00pm - 11.00pm</td>                                      
                                            <td><strike style="color:red">SGD 22</strike> SGD 19<br/><i style="font-size:11px">Limited Time</i></td>
                                            <td><strike style="color:red">SGD 124</strike> SGD 106<br/><i style="font-size:11px">Limited Time</i></td>                                            
                                        </tr>
                                </table>
                            </div>

                             <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="July 19 2019 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/sgp/en-AU/shows/beerfest asia 2019/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>  --}}
                            </div>


                            {{-- <span class="importantNote">*Additional charges will be applied for credit/debit card transaction, optionally customer may pay cash for outlet purchase to avoid the charges.</span> --}}
                            {{-- <div class="note text-left">
                                <h2>Reminder</h2>
                                <ol>
                                    <li><b>Dress code</b> : Neat and decent. Worn out / faded / shabby jeans, t-shirt without collar, shorts, slippers, and sandals are STRICTLY NOT ALLOWED.</li>
                                    <li><b>Age limit to show</b> : 4 years old and above. Every person/kid needs ticket to own seat.</li>
                                    <li>Food and drinks are not allowed in the auditorium.</li>
                                </ol>
                            </div> --}}
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown include RM4.00 AirAsiaRedTix fee & other taxes.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM30.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li> --}}
                                    <li>Prices shown exclude SGD2.00 AirAsiaRedTix fee.</li>
                                    <li>18 and above except Sunday.</li>
                                    <li>Free entry for children below 12 years of age on Sunday. Teenagers below 18 years of age requires a regular pass on Sunday.</li>
                                    <li>Ticket sales close on 20th June 2019.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection