@extends('master')
@section('title')
  Island Music Festival 2018
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

<style type="text/css">
.col-half-offset{
    margin-left:4.166666667%
}

.embed-responsive .embed-responsive-item,
.embed-responsive iframe,
.embed-responsive embed,
.embed-responsive object,
.embed-responsive video {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  height: 80%;
  width: 80%;
  border: 0;
  align: center;
}

.embed-responsive-16by9 {
  padding-bottom: 48.25%;
  align: center;
}

</style>
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/islandmusicfest2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Island Music Festival 2018"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/islandmusicfest2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Island Music Festival 2018">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>Island Music Festival 2018</h6> Tickets from <span>RM450</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white"> 
        <section class="pageCategory-section last">
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>Title</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 12th October 2018</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Long Beach, Redang Island<a target="_blank" href="https://goo.gl/maps/FT4p9xvR8Cs"> View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8.00am - 12.00am</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <p>After 5 successful years, the Island Music Festival is back for its 6th edition from 12th – 14th October 2018 on Long Beach, Redang Island. This year, with the more activites, workshops and performances.</p>
                <p><b>Activities</b><br />
                <ul>
                <li>ATV ON THE BEACH</li>
                <li>BEACH FOOTBALL</li>
                <li>BEACH VOLLEYBALL</li>
                <li>CATWALK</li>
                <li>JUNGLE TREKKING</li>
                <li>KAYAK</li>
                <li>PADDLE BOARD</li>
                <li>SNORKELING</li>
                <li>YOGA</li>
                </ul>
                </p>

                <p><b>WorkShops</b><br />
                <ul>
                <li>ARTS &amp; CRAFTS</li>
                <li>COOKING</li>
                <li>COCKTAIL MAKING</li>
                <li>FIRST AID</li>
                <li>MUSIC</li>
                <li>SAND SCULPTING</li>
                </ul>
                </p>
                
                <p><b>LineUp</b><br />
                <ul>
                <li>A-KID</li>
                <li>ANNATASHA</li>
                <li>BIHZHU</li>
                <li>CHRISTIAN THESEIRA</li>
                <li>DANI</li>
                <li>DJ BIGGIE</li>
                <li>GABRIEL LYNCH</li>
                <li>JUMERO</li>
                <li>KAYA</li>
                <li>LASKAR SONAS</li>
                <li>LAST LOGIC</li>
                <li>LENG SISTERS</li>
                <li>LIL J</li>
                <li>KIDD SANTHE</li>
                <li>MASS MUSIC</li>
                <li>NJWA</li>
                <li>ODDICON</li>
                <li>PEANUTBUTTER JELLYJAM</li>
                <li>PJ12</li>
                <li>PRISCILLA XAVIER</li>
                <li>RHYTHM REBELS</li>
                <li>SEATRAVEL</li>
                <li>SKELETOR</li>
                <li>SOUNDS OF JANE</li>
                <li>UCOPPP</li>
                <li>VITAL SIGNALS</li>
                <li>ZUPANOVA</li>
                </ul>
                </p>
                <p>The Island Music Festival gurantees a unique experience on the beautiful Redang Island, Malaysia.</p>
                <p>Come Feel It.</p>
                <p>#imfparadise #beachbum2018 #letsbebeachy</p>
                <p>Event FAQ: <a href="https://theislandmusicfestival.com/faq/">https://theislandmusicfestival.com/faq/</a></p>

                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="col-sm-1 clearfix">
              </div>
              <div class="col-sm-10 col-sm-offset-2">
              <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Zo-Q8-Hfr0A?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
              </div>
              </div>
              <div class="col-sm-1 clearfix">
              </div>
            </div>
          </div>
        </section>

        <!-- Sponsors -->
            <section class="pageCategory-section last">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>PARTNERS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img class="image-responsive" src="images/islandmusicfest2018/sponsor-2.png" style="width:70%; height:auto;" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>MAP LAYOUT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img class="image-responsive" src="images/islandmusicfest2018/layout-1.jpg" style="width:70%; height:auto;" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>EVENT FAQ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><p>Click here: <a href="https://theislandmusicfestival.com/faq/">https://theislandmusicfestival.com/faq/</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-1 clearfix">
                        </div>
                    </div>
                </div>
            </section>
        <!-- /Sponsors -->

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <a href="images/islandmusicfest2018/activity-0.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/activity-0.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/islandmusicfest2018/activity-1.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/activity-1.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/islandmusicfest2018/activity-2.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/activity-2.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/islandmusicfest2018/activity-3.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/activity-3.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/islandmusicfest2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/gallery-1.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/islandmusicfest2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/gallery-2.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/islandmusicfest2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/gallery-3.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/islandmusicfest2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/gallery-4.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/islandmusicfest2018/gallery-5.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/gallery-5.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/islandmusicfest2018/gallery-6.jpg" data-featherlight="image"><img class="" src="images/islandmusicfest2018/gallery-6.jpg" alt=""></a>
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          {{-- <th>Category</th>
                          <th>Price</th>
                          <th>Remarks</th> --}}
                          <th>Category</th>
                          <th>Single<br />Occupancy</th>
                          <th>Twin Sharing (2 Pax)</th>
                        </tr>
                      </thead>
                      <tbody>
{{--                          <tr>
                              <td>Early Bird</td>
                              <td><strike style="color:red;">RM 450.00</strike><br/>SOLD OUT</td>
                              <td></td>
                          </tr>
                          <tr>
                              <td>Twin Sharing<br/>(RM 550 x 2 Pax)</td>
                              <td>RM 1,100.00</td>
                              <td></td>
                          </tr>
                          <tr>
                              <td>Single Occupancy</td>
                              <td>RM 750.00</td>
                               <td></td>
                          </tr> --}}
                          <tr>
                            <td>Early Bird</td>
                            <td><strike style="color:red;">RM 650.00</strike><br/>SOLD OUT</td>
                            <td><strike style="color:red;">RM 900.00</strike><br/>SOLD OUT</td>
                          </tr>
                          <tr>
                            <td>Tier 1</td>
                            <td>RM 750.00</td>
                            <td>RM 1,100.00</td>
                          </tr>
                          <tr>
                            <td>Tier 2</td>
                            <td>RM 800.00</td>
                            <td>RM 1,200.00</td>
                          </tr>
                          <tr>
                            <td>Tier 3</td>
                            <td>RM 850.00</td>
                            <td>RM 1,300.00</td>
                          </tr>
                        </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger" id="buyButton" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/island music festival 2018 (12 to 14 october 2018)/events" role="button">BUY TICKETS</a>
                  <!--<span class="or">/</span>                 
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>-->
                </div>                  
                <span class="importantNote">1. Package price for Single Occupancy entitles 1 Pax only.</span>
                <span class="importantNote">2. Package price for Twin Sharing entitles 2 Pax only.</span>
                <span class="importantNote">3. Ticket price is inclusive of all Island Music Festival Activities and Entertainment.</span>
                <span class="importantNote">4. Tickets price is inclusive of 3 Days 2 Nights stay at selected partner hotels.</span>
                <span class="importantNote">5. Organizer will assign the partner hotels based on first come first serve basis.</span>
                <span class="importantNote">6. Ticket price is inclusive of return ferry transfer (Syahbandar Jetty –> Redang Island -> Syahbandar Jetty)</span>
                <div class="note text-left">
                  <h2>Important Notes</h2>
                  <ol>
                    <li>Prices shown excludes RM4.00 AirAsiaRedTix fee.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                  </ol>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('modal')

  <!--Modal Seat Plan image-->
  <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <div class="clearfix">&nbsp;</div>
          <img src="" class="enlargeImageModalSource" style="width: 100%;">
          </div>
      </div>
      </div>
  </div>
  
  <!-- Modal Where to Get Tix Location-->
  <div id="modalGetTixLoc" class="modal fade" role="dialog">
      <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <h2>GET YOUR TICKETS FROM:</h2>
          <dl class="dl-horizontal">
              <dt>Online:</dt>
              <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Rock Corner outlets:</dt>
              <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
              <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Victoria Music outlets:</dt>
              <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
              <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
              <dd>Tropicana</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Penang outlets:</dt>
              <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
              <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Istana Budaya Jalan<br>Tun Razak:</dt>
              <dd>03 40265558</dd>
          </dl>
          </div>
      </div>
      </div>
  </div>

   {{-- <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Title</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>Announce</p>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    
    //$(document).ready(function(){
      // Session based Pop up modal
    //  if(typeof(Storage) !== "undefined") {
    //    if(!sessionStorage.getItem('modal')) {                          // if the session key is not exist
    //        sessionStorage.setItem('modal', 'true');                    // create new session
    //        $("#announcementModal").modal('show')                       // show modal dialog
    //    }
    //  } 
    //});

    // Modal Announcement
	  //$(document).ready(function(){
    //  $("#announcementModal").modal('show');
  	//});	

    </script>
    
@endsection