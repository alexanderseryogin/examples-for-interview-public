@extends('master')
@section('title')
    TYLER BRYANT AND THE SHAKEDOWN
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    {{-- <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('{!! asset('images/tylerbryant/tylerbryant-banner.jpg') !!}')">
        <video id="video-background" class="hidden-xs" preload muted autoplay loop>
          <source src="{!! asset('images/tylerbryant/tylerbryant-vidbanner.mp4') !!}" type="video/mp4">
        </video>
        <video id="video-background" class="visible-xs" controls muted loop>
          <source src="{!! asset('images/tylerbryant/tylerbryant-vidbanner-m.mp4') !!}" type="video/mp4">
        </video>
      </div>
    </section> --}}<!-- /Banner Section -->

    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/tylerbryant/tylerbryant-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Aladdin & The Magic Lamp"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/tylerbryant/tylerbryant-thumb.jpg')}}" style="width: 100%" class="img-responsive">
        </div>
    </section>

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>TYLER BRYANT AND THE SHAKEDOWN</h6>
                  Tickets from <span>RM197</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 23rd February 2017 </div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> HardRock Cafe, Singapore  <a target="_blank" href="https://goo.gl/maps/5rFh9SgLE6k">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 9:00pm</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <p>This coming Thursday, February 23rd, Tyler Bryant & The Shakedown will be making their live debut in Singapore as part of LAMC's pre-show party for the local leg of the Guns N' Roses Not In This Lifetime Tour.</p>
                <p>Best known for their muscular blues rock sound, the band have famously toured with the likes of AC/DC, Aerosmith, B.B. King and Eric Clapton (just to name a few).</p>
                <p>Tyler Bryant & The Shakedown show is part of the “<a href="http://www.lamcproductions.com/rising-star">Rising Star Series</a>”. It is a new initiative that will feature up-and-coming international artistes performing alongside significant local acts in an intimate 'live' performance setting. </p>  
                <p>Taking place at 9pm at Hard Rock Café, the show will also be supported by virtuoso Singaporean guitarist Alif Putra. Known as a charismatic powerhouse on-stage, local music fans may also remember him from his high-profile stints opening for Metallica and performing with guitar god Steve Vai.</p>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <iframe width="560" height="100%" src="https://www.youtube.com/embed/HO5UNXF_6U8" frameborder="0" allowfullscreen></iframe>
                  </div>
                  <div class="swiper-slide">
                    <iframe width="560" height="100%" src="https://www.youtube.com/embed/ykJPw4880JM" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Category</th>
                          <th>Price</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td><span class="box green">Standard</span></td>
                              <td>RM 197</td>
                              <td>Free standing</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/events/tyler%20bryant%20and%20the%20%20shakedown/2017-2-23_21.00/hard%20rock%20caf%C3%A9%20singapore?back=2&area=153e12bd-948a-4829-85da-6ddeaae9795e&type=ga">BUY TICKETS</a>
                </div>
                <div class="alert alert-danger"><i class="fa fa-info-circle" aria-hidden="true"></i> Online purchase has been closed.</div>
                <div class="note text-left">
                  <h2>Rating / Age Limit</h2>
                  <ul>
                    <li>No admission for infants in arms and children below 12 years old</li>
                    <li>Children 12 years and above must purchase a ticket for entry</li>
                    <li>Admission is subject to tickets produced at the entrance</li>
                  </ul>
                  <h2>Photography / Video Recording Rules</h2>
                  <ul>
                    <li>No Photography, Video recording and Audio recording is permitted for this event</li>
                  </ul>
                  <h2>Important Notes</h2>
                  <ol>
                    <li>Prices shown include RM4.00 AirAsiaRedTix fee.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                  </ol>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                <dd>The Curve (TEL: 03 - 7733 1139)</dd>
                <dd>Bangsar Village (TEL: 03-22021139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
                <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
            </dl>
            </div>
        </div>
        </div>
    </div>

@endsection