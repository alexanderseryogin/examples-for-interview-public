@extends('master')
@section('title')
    Heart and Soul - Sid Sriram Live in KL
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Heart and Soul - Sid Sriram Live in KL" />
    <meta property="og:description" content="Sid Sriram, who has garnered a sea of fans on this side of the world, will be performing Live In KL under Woodmark Events"/>
    <meta property="og:image" content="{{ Request::Url().'images/sidsriram2019/thumbnail.jpg' }}" />

    <style type="text/css">
    .blink {
        -webkit-animation: blink 1s step-end infinite;
            animation: blink 1s step-end infinite;
    }
    @-webkit-keyframes blink { 50% { visibility: hidden; }}
        @keyframes blink { 50% { visibility: hidden; }}
    </style>
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/sidsriram2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Heart and Soul - Sid Sriram Live in KL"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/sidsriram2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Heart and Soul - Sid Sriram Live in KL">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Heart and Soul, Sid Sriram Live in KL</h6> Tickets from <span>RM99</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  6th July 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Quill City Mall Convention Centre, Level 6 <a target="_blank" href="https://goo.gl/maps/yt4HhxQJxRt">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 7.00pm - 11.30pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-8">
                                <p><h2>Heart and Soul, Sid Sriram Live in KL</h2><br/>
                                You read that right! The sensational singer returns to KL for the fourth time in his next show called ‘Heart & Soul’.</p>
                                <p>Sid Sriram, who has garnered a sea of fans on this side of the world, will be performing Live In KL under Woodmark Events.</p>
                                <p>In the last year alone, Sid has voiced numerous film songs including 2.0, Vada Chennai, Geeta Govindam, Sarkar, Visvasam and many more; won awards and released his album ‘Entropy’.</p>
                            </div>
                            <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/user/spotify/playlist/37i9dQZF1DZ06evO4ovoG0" width="300" height="300" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div>
                        </div>
                        {{-- <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/PhrY3yep1OQ?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/sidsriram2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/sidsriram2019/gallery-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr style="background-color:#666666;font-size:11px;">
                                            <th>Category</th>
                                            <th>Early Bird<br/>(Limited)</th>
                                            <th>Normal Price</th>
                                            <th>Seat Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Platinum</td>                                            
                                            <td>RM 649<br /><font style="color:red" class="blink">Selling Fast</font></td></td>
                                            <td style="color:#999999;">RM 699</td>
                                            <td rowspan="11" style="background-color: #ffffff;"><img class="img-responsive seatPlanImg" src="images/sidsriram2019/seat-plan.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="text-align:left;font-size:11px;">
                                            Platinum Packages:
                                                <ul>
                                                    <li>AFTER PARTY - MEET & GREET WITH ARTIST</li>
                                                    <li>FREESTLYE Pre-Show Cocktail & Dinner</li>
                                                    <li>Designated Dining Area</li>
                                                    <li>Free parking</li>
                                                    <li>OFFICIAL T-SHIRT Merchandise</li>
                                                    <li>OFFICIAL POSTER PRINTED WITH NAME, SIGNED BY SID.</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr style="background-color:#666666;font-size:11px;color:#ffffff;">
                                            <td>Category</td>
                                            <td>Early Bird<br/>(Limited)</td>
                                            <td>Normal Price</td>
                                        </tr>
                                        <tr style="background-color:#FCEFDC;">
                                            <td>Gold</td>
                                            <td>RM 349<br /><font style="color:red" class="blink">Selling Fast</font></td></td>                                            
                                            <td style="color:#999999;">RM 399</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="text-align:left;background-color:#FCEFDC;font-size:11px;">
                                            Gold Packages:
                                                <ul>
                                                    <li>FREESTLYE DINNER & SOFT DRINKS</li>
                                                    <li>COMPLIMENTARY Cocktail on the house</li>
                                                    <li>Designated Dining Area</li>
                                                    <li>OFFICIAL T-SHIRT Merchandise</li>
                                                    <li>OFFICIAL POSTER PRINTED WITH NAME, SIGNED BY SID.</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr style="background-color:#666666;font-size:11px;color:#ffffff;">
                                            <td>Category</td>
                                            <td>Early Bird<br/>(Limited)</td>
                                            <td>Normal Price</td>
                                        </tr>
                                        <tr>
                                            <td>Grand Stand</td>
                                            <td><strike>RM 149</strike><br /><font style="color:red">Sold Out</font></td>
                                            <td><strike>RM 149</strike><br /><font style="color:red">Sold Out</font></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="text-align:left;font-size:11px;">
                                            Grand Stand Package:
                                                <ul>
                                                    <li>Complimentary drinks on the house</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr style="background-color:#666666;font-size:11px;color:#ffffff;">
                                            <td>Category</td>
                                            <td>Early Bird<br/>(Limited)</td>
                                            <td>Normal Price</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>Silver</td>
                                            <td><strike>RM 99</strike><br /><font style="color:red">Sold Out</font></td>
                                            <td><strike>RM 199</strike><br /><font style="color:red">Sold Out</font></td>                                            
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="text-align:left;background-color: #FCEFDC;font-size:11px;">
                                            Silver Package:
                                                <ul>
                                                    <li>Complimentary drinks on the house</li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Jul 6 2019 00:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/sidsriram2019/booking" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown include RM4.00 ticketing fee</li>
                                    <li>Transaction fee of RM8.00 per basket/cart applicable for Internet purchase.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });


    // Modal Announcement
	//$(document).ready(function(){
    //  $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. Special Limited offer at AirAsia RedTix only</p>
                        <p>2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection