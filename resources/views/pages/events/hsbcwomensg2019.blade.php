@extends('master')
@section('title')
    HSBC Womens World Championship 2019
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="HSBC Womens World Championship 2019" />
    <meta property="og:description" content="Boasting one of the strongest fields in women’s golf outside of the five Major Championships, the HSBC Women’s World Championship"/>
    <meta property="og:image" content="{{ Request::Url().'images/hsbcwomensg2019/thumbnail1.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/hsbcwomensg2019/web-banner1.jpg')}}" style="width: 100%" class="img-responsive" alt="HSBC Womens World Championship 2019"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/hsbcwomensg2019/thumbnail1.jpg')}}" style="width: 100%" class="img-responsive" alt="HSBC Womens World Championship 2019">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>HSBC Womens World Championship 2019</h6> Tickets from <span>RM34</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  28th Feb - 3rd Mar 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  The New Tanjong Course, Sentosa Golf Club, Singapore <a target="_blank" href="https://goo.gl/maps/tVUGmmTKMgq">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 7.00am </div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>HSBC Womens World Championship 2019</h2><br/>
                            Boasting one of the strongest fields in women’s golf outside of the five Major Championships, the HSBC Women’s World Championship will see 63 of the top women players battle it out at Sentosa Golf Club’s New Tanjong Course.</p>
                            <p>Taking place from the 28th February to 3rd March 2019, the four-day extravaganza will be a platform to celebrate the sport of golf, the brilliant women competing in it and inspirational women from all facets of life throughout Asia.</p>
                            <p>For more information, please visit <a href="www.hsbcgolf.com/womens">www.hsbcgolf.com/womens</a></p>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/9SHApiiqQfA?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div> --}}
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/hsbcwomensg2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/hsbcwomensg2019/gallery-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            {{-- <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">

                            @include('layouts.partials._showticketsnodisc')

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude IDR30,000 AirAsiaRedTix fee.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close mid day prior to the event, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Package Tier</th>
                                            <th>Ticket</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size:10px;">
                                        <tr>
                                            <td>EARLY BIRD - FOUR DAY SEASONAL TICKET (20% discount)</td>
                                            <td>28th February - 3rd March 2019</td>
                                            <td><strike>RM203</strike><br /><font style="color:red;">SOLD OUT</font></td>
                                        </tr>

                                        <tr>
                                            <td>EARLY BIRD - WEEKEND SEASONAL TICKET (20% discount)</td>
                                            <td>2nd - 3rd March 2019</td>
                                            <td><strike>RM131</strike><br /><font style="color:red;">SOLD OUT</font></td>
                                        </tr>
                                        <tr>
                                            <td>EARLY BIRD - WEEKEND BUDDY TICKET (20% discount)</td>
                                            <td>2nd or 3rd March 2019</td>
                                            <td><strike>RM131</strike><br /><font style="color:red;">SOLD OUT</font></td>
                                        </tr>
                                        <tr>
                                            <td>EARLY BIRD - FAMILY BUNDLE (20% discount)</td>
                                            <td>2nd or 3rd March 2019</td>
                                            <td><strike>RM124</strike><br /><font style="color:red;">SOLD OUT</font></td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>FOUR DAY SEASONAL TICKET</td>
                                            <td>28th February - 3rd March 2019</td>
                                            <td>RM254</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>WEEKEND SEASONAL TICKET</td>
                                            <td>2nd - 3rd March 2019</td>
                                            <td>RM164</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>WEEKEND BUDDY TICKET</td>
                                            <td>2nd or 3rd March 2019 </td>
                                            <td>RM164</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>FAMILY BUNDLE</td>
                                            <td>2nd or 3rd March 2019 </td>
                                            <td>RM155</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Feb 27 2019 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/hsbc womens world championship 2019 (28th feb 2019-03rd march 2019)/events" role="button">BUY TICKETS</a>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Single Day Tier</th>
                                            <th>Ticket</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size:10px;">
                                        <tr>
                                            <td rowspan="4">SINGLE DAY TICKET</td>
                                            <td>28th February 2019 (Thursday)</td>
                                            <td>RM68</td>
                                        </tr>
                                        <tr>
                                            <td>1st March 2019 (Friday)</td>
                                            <td>RM68</td>
                                        </tr>
                                        <tr>
                                            <td>2nd March 2019 (Saturday)</td>
                                            <td>RM90</td>
                                        </tr>
                                        <tr>
                                            <td>3rd March 2019 (Sunday)</td>
                                            <td>RM90</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td rowspan="4">SENIOR CITIZEN 60+ SINGLE DAY TICKET</td>
                                            <td>28th February 2019 (Thursday)</td>
                                            <td>RM34</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>1st March 2019 (Friday)</td>
                                            <td>RM34</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>2nd March 2019 (Saturday)</td>
                                            <td>RM45</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>3rd March 2019 (Sunday)</td>
                                            <td>RM45</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Feb 27 2019 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/hsbc womens world championship 2019_/events" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                <h2>Terms &amp; Conditions</h2>
                                <ol>
                                    <li>Admission to event/venue by full ticket only. Printed or electronic tickets must be produced for admission.</li>
                                    <li>Juniors under 16 years old are entitled to free entry when accompanied by a ticketed adult.</li>
                                    <li>Adults and children 16 years old and above will each require a ticket for admission.</li>
                                    <li>Possession of a ticket does not reserve a specific seat position, unless otherwise stated on the ticket.</li>
                                    <li>Items that cannot brought into the venue include:
                                        <ul>
                                            <li>Outside food and drink</li>
                                            <li>Any items made of glass</li>
                                            <li>Weapons/sharp objects</li>
                                            <li>Professional cameras/recording devices</li>
                                            <li>Any large bags/luggages/bulky items</li>
                                            <li>Pets</li>
                                            <li>Laser Pointers</li>
                                            <li>Illegal Substances</li>
                                            <li>Selfie sticks</li>
                                            <li>Signboards or banners bigger than A3 size</li>
                                        </ul>
                                    </li>
                                    <li>Public parking at Sentosa Island is limited and heavy traffic is expected around the event area. Ticket holders are therefore strongly encourage and advise to go public and take the free shuttle buses provided, walk or utilise the existing mode of transport provided by Sentosa Island. Shuttle bus schedule and pick up/drop off points would be provided at a later date.</li>
                                    <li>Terms & Conditions apply. Visit <a href="https://www.hsbcgolf.com/womens/tickets-terms-and-conditions">https://www.hsbcgolf.com/womens/tickets-terms-and-conditions</a> for details.
                                </ol>
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown includes AirAsiaRedTix fee and Credit card fee.</li>
                                    <li>Transaction fee of RM10.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 1 day prior to event day, subject to availability.</li>
                                </ol>

                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	//$(document).ready(function(){
    //    $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

    <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Temporarily down for Maintenance</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>We are performing scheduled maintenance. We should be back online shortly</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection