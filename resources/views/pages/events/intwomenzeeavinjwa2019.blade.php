@extends('master')
@section('title')
    International Women's Day Zee Avi + NJWA Concert
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="International Women's Day Zee Avi + NJWA Concert" />
    <meta property="og:description" content="International Women's Day Zee Avi + Njwa Concert combines two artists with a very distinct style of music."/>
    <meta property="og:image" content="{{ Request::Url().'images/intwomenzeeavinjwa2019/thumbnail1.jpg' }}" />
@endsection

@section('content')
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="css/custom2.css">
    <link type="text/css" rel="stylesheet" href="css/clock.css">
    <link type="text/css" rel="stylesheet" href="/css/custom/redtix.css">
    <link type="text/css" rel="stylesheet" href="css/timeTo.css">

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/intwomenzeeavinjwa2019/web-banner1.jpg')}}" style="width: 100%" class="img-responsive" alt="International Women's Day Zee Avi + NJWA Concert"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/intwomenzeeavinjwa2019/thumbnail1.jpg')}}" style="width: 100%" class="img-responsive" alt="International Women's Day Zee Avi + NJWA Concert">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>International Women's Day Zee Avi + NJWA Concert</h6> Tickets from <span>RM88</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 22nd Jun 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Dewan Merdeka Putra World Trade Centre (PWTC) Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/AzWYnUsDft82">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8.45pm - 10.45pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <p><h2>International Women's Day Zee Avi + NJWA Concert</h2><br/>
                                Dewan Merdeka, Putra World Trade Centre (PWTC) Kuala Lumpur 22 June 2019<br/>
                                20:45 MYT</p>
                                <p>International Women's Day Zee Avi + NJWA Concert combines two artists with a very distinct style of music. The concert is organised by FM Showlab Ventures which will host it in celebration of the International Women’s Day 2019. It will be held at Dewan Merdeka, Putra World Trade Center (PWTC) Kuala Lumpur on 22 March, 2019.</p>
                                <p>Both artists have spread their music all over the world independently and unpretentiously and to have them doing a concert together, surely this will be a world-class sensation.</p>
                                <p>Zee Avi is an internationally recognised Malaysian singer, songwriter, musician and visual artist where she was the recipient of The International Youth Icon Award in 2011. One of her hit song, Swell Window was also featured in the international TV series including 21 Jump Street and Gossip Girl.</p>
                                <p>Najwa Mahiaddin, who now simply known as NJWA has just released a three track, self titled EP to celebrate her musical freedom and experimentation with an ethereal, neo-soul sound that she is extending to her show through out the country. Recipient of the Best New Artist and Best Local English Song during AIM 18, NJWA will surely bring something new for her fans.</p>
                                <p>The production design for the concert, with the concept of “Classy, Sophisticated and Elegant” will enhanced the duo’s status. Thus, will match their maturity in the music industry as respectful artists. They will perform their selection of songs in the 120 minutes show.</p>
                                <p>All elements in the stage design, Editing works on the background visuals will enhance every song. Technical expertise for sound system, lighting and visual are highly professional to ensure the smoothness of the concert. It is a show for their fans and to give maximum enjoyment to them. Trendy and classy with international standard.</p>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/intwomenzeeavinjwa2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/intwomenzeeavinjwa2019/gallery-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">

                            @include('layouts.partials._showticketssimple')

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown excludes AirAsiaRedTix fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Adult: 18 years old and above.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	//$(document).ready(function(){
    //    $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
    @include('layouts.partials.modals._moreInfoNoFee')

    <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Promoter's Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>Dear Esteemed Audience,<br/>
                        We regret to inform you that after thorough deliberation, we have come to a decision of postponing Zee Avi and NJWA'S concert to a new date on the 22nd of June 2019.</p>
                        <p>This decision has to be taken due to some unforeseen technical circumstances which has prevented us from moving ahead with the original date on the 22nd of March 2019.</p>
                        <p>On behalf of Showlab, we thank you for understanding this situation. Please accept our apologies for any inconveniences this may cause you and we could never thank you enough for your support towards this concert.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection