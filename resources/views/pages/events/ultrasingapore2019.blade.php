@extends('usg2019.layouts.master')
@section('title')
    Ultra Singapore 2019
@endsection

@section('header')
    @include('usg2019.layouts.partials.header')
@endsection

@section('content')
    <main class="main">

        <div class="hero">
            <picture>
                <source media="(max-width: 500px)" srcset="/images/ultrasingapore2019/hero-mobile.gif">
                <source media="(min-width: 501px)" srcset="/images/ultrasingapore2019/hero.gif">
                <img src="/images/ultrasingapore2019/hero.gif" alt="ultra-singapore">
            </picture>
        </div>

        <div class="main-content-holder">
            <div class="main-content-wrapp">
                <section class="info-section">
                    <div class="container">
                        <div class="row justify-content-between">

                            <div class="form-group d-block d-sm-none col-sm-12 col-xs-12" align="center" style="margin-bottom:4rem">
                                <div class="btn-holder cta-get-tickets" align="center">
                                    <a class="btn btn-primary" style="font-size:18px; width:75%">Get Tickets</a>
                                </div>                                                                
                            </div>

                            <div class="col-md-4 col-lg-3 d-flex col-sm-12 col-xs-12">
                                <div>
                                    <ul class="info-list">
                                        <li>
                                            <div class="icon-holder">
                                                <i class="icon icon-ticket"></i>
                                            </div>
                                            Ultra Singapore 2019
                                        </li>
                                        <li>
                                            <div class="icon-holder">
                                                <i class="icon icon-event"></i>
                                            </div>
                                            8<sup>th</sup> - 9<sup>th</sup>June 2019
                                        </li>
                                        <li>
                                            <div class="icon-holder">
                                                <i class="icon icon-pin"></i>
                                            </div>
                                            Ultra Park - One Bayfront Avenue,
                                            Singapore
                                        </li>
                                    </ul>
                                    <div class="info-label-holder category-block">
                                        <span class="label">FESTIVAL</span>
                                        <span class="label label-outline">EDM</span>
                                    </div>                                
                                </div>
                                <div style="border-right:1px solid rgba(255, 255, 255, 0.8); height:100%;padding:0 0 0 10px" class="d-none d-md-block"></div>
                            </div>
                                                                                    
                            
                            <div class="col-md-5 col-lg-4 form-group d-none d-md-block align-self-center" >
                                <div style="display:inline-block;">                                    
                                        <div style="font-size:14px; margin-bottom:12px; text-align: justify;">Don't miss your experience with ULTRA!</div>
                                        <div class="btn-holder cta-get-tickets">
                                            <a class="btn btn-primary"  style="font-size:18px; width:100%">Get Tickets</a>
                                        </div>
                                    
                                </div>
                            </div>

                            <div class="col-md-3 col-lg-5 justify-content-end d-flex">
                                <div class="logos-block">
                                    <div class="item">
                                        <span class="title">Brought to you by:</span>
                                        <img src="/images/ultrasingapore2019/ultra-singapore.png" alt="Ultra singapore" width="110">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="tab-section">
                    <div class="tabs">
                        <div class="nav-tabs-holder">
                            <div class="container">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tab1" role="tab" id="tickets-tab">
                                            <span class="title">TICKETS</span>
                                        </a>
                                    </li>
                                    {{-- <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab4" role="tab">
                                            <span class="title">PAYMENT PLANS</span>
                                        </a>
                                    </li> --}}
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab6" role="tab">
                                            <span class="title">ULTRA PASSPORT PACKS</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab5" role="tab">
                                            <span class="title">TRAVEL PACKAGES</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab2" role="tab">
                                            <span class="title">FESTIVAL INFO</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab3" role="tab">
                                            <span class="title">FAQ</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane show active fade" id="tab1" role="tabpanel">
                                @include('usg2019.layouts.partials.tickets-tab')
                            </div>
                            {{-- <div class="tab-pane fade" id="tab4" role="tabpanel">
                                @include('usg2019.layouts.partials.payment-plans')
                            </div> --}}
                            <div class="tab-pane fade" id="tab6" role="tabpanel">
                                @include('usg2019.layouts.partials.passports-tab')
                            </div>
                            <div class="tab-pane fade" id="tab5" role="tabpanel">
                                @include('usg2019.layouts.partials.packages-tab')
                            </div>
                            {{-- <div class="tab-pane fade" id="tab5" role="tabpanel">
                                @include('usg2019.layouts.partials.packages-all-tab')
                            </div>--}}
                            <div class="tab-pane fade" id="tab2" role="tabpanel">
                                @include('usg2019.layouts.partials.event-info-tab')
                            </div>
                            <div class="tab-pane fade" id="tab3" role="tabpanel">
                                @include('usg2019.layouts.partials.faq-tab')
                            </div>
                            <div class="tab-pane fade" id="tab6" role="tabpanel">

                                @include('usg2019.layouts.partials.passports-tab')

                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
@endsection

@section('footer')
    @include('usg2019.layouts.partials.footer')
@endsection

@section('info-modal')
    @include('usg2019.layouts.modals.info-modal')
@endsection

@section('sale-modal')
    @include('usg2019.layouts.modals.sale-modal')
@endsection

@section('newsletter-modal')
    @include('usg2019.layouts.modals.newsletter-modal')
@endsection

@section('customjs')
    <script type="text/javascript">

        function scrollTo($element) {
            $('html, body').animate({
              scrollTop: $element.offset().top
            }, 500);
            return false;
        }

        var register_newsletter = function () {
            var user_email = document.getElementById('mce-EMAIL').value;
            var user_name = user_email.split("@")[0];
            mixpanel.alias(user_email);
            mixpanel.people.set({
                "$name": user_name,
                "$email": user_email
            });
            mixpanel.identify(user_email);
            mixpanel.track('register_newsletter', {Email: user_email});
        };

        $('#mc-embedded-subscribe-form').on('submit', function() {
            $('#newsletter-modal').modal('hide');
        });

        $('.cta-get-tickets').on('click', function(){            
                    
            $('.nav-link').attr('aria-selected', false);
            $('#tickets-tab').attr('aria-selected',true);
            $('.nav-link').removeClass('active');
            $('#tickets-tab').addClass('active');

            $('.tab-pane').removeClass('active show');
            $('#tab1').addClass('active show');

            setTimeout(function() {                 

                $('html, body').animate({
                    scrollTop: $("#cta-get-tickets-anchor").offset().top + 70
                }, 400);

            }, 100);
                         
        });
        
    </script>
@endsection