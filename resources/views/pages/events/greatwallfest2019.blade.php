@extends('master')
@section('title')
Great Wall Festival 2019
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Great Wall Festival 2019" />
    <meta property="og:description" content="Rave & Run on the Great all of China. Summer Camping in Forest and Mountains. Let’s bring history back and enjoy an audio visual reunion with the Terra Cotta Warriors through the space-time transformation."/>
    <meta property="og:image" content="{{ Request::Url().'images/greatwallfest2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="css/custom2.css">
    <link type="text/css" rel="stylesheet" href="css/clock.css">
    <link type="text/css" rel="stylesheet" href="/css/custom/redtix.css">
    <link type="text/css" rel="stylesheet" href="css/timeTo.css">

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/greatwallfest2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Great Wall Festival 2019"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/greatwallfest2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Great Wall Festival 2019">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Great Wall Festival 2019</h6> Tickets from <span>USD103</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 18th - 19th May 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  The Great Wall of China, Beijing <a target="_blank" href="https://goo.gl/maps/MhzFYw4Vf8S2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Saturday - Sunday</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <p><h2>Great Wall Festival 2019</h2><br/>
                                Rave & Run on the Great wall of China.<br/>
                                Summer Camping in Forest and Mountains.<br/>
                                Let’s bring history back and enjoy an audio visual reunion with the Terracotta Warriors through the space-time transformation.<br/>
                                We respect ancient glory.<br/>
                                We also try to change 700 years of history.<br/>
                                Now we sprinkle a layer of different light on the magnificent Great Wall.<br/>
                                The aurora is absorbing the colors of the sky shining, cycling and dancing.</p>

                                <p>Let’s meet there in 2019.</p>

                                <p>We are very ambitious to develop and improve festival to make it even better than it was.<br/>
                                2019 we will show you that Great Wall Festival is more than you could have imagined before.<br/>
                                We are really looking forward to 18-19 May 2019 when we can welcome you at the Great Wall again or for the first time.<br/>
                                You won’t regret it! <br/>
                                3 Stages┃30 Hours techno┃After Party</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/g-dDYUdpylY?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/greatwallfest2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/greatwallfest2019/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/greatwallfest2019/gallery-2.jpg" data-featherlight="image"><img class="" src="images/greatwallfest2019/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/greatwallfest2019/gallery-3.jpg" data-featherlight="image"><img class="" src="images/greatwallfest2019/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/greatwallfest2019/gallery-4.jpg" data-featherlight="image"><img class="" src="images/greatwallfest2019/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/greatwallfest2019/gallery-5.jpg" data-featherlight="image"><img class="" src="images/greatwallfest2019/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/greatwallfest2019/gallery-6.jpg" data-featherlight="image"><img class="" src="images/greatwallfest2019/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/greatwallfest2019/gallery-7.jpg" data-featherlight="image"><img class="" src="images/greatwallfest2019/gallery-7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/greatwallfest2019/gallery-8.jpg" data-featherlight="image"><img class="" src="images/greatwallfest2019/gallery-8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/greatwallfest2019/gallery-9.jpg" data-featherlight="image"><img class="" src="images/greatwallfest2019/gallery-9.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/greatwallfest2019/gallery-10.jpg" data-featherlight="image"><img class="" src="images/greatwallfest2019/gallery-10.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/greatwallfest2019/gallery-11.jpg" data-featherlight="image"><img class="" src="images/greatwallfest2019/gallery-11.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/greatwallfest2019/gallery-12.jpg" data-featherlight="image"><img class="" src="images/greatwallfest2019/gallery-12.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/greatwallfest2019/gallery-13.jpg" data-featherlight="image"><img class="" src="images/greatwallfest2019/gallery-13.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">

                            @include('layouts.partials._showticketssimple')

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown excludes USD2.00 AirAsiaRedTix ticket fee.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
    @include('layouts.partials.modals._moreInfoLargeImgDesc')
@endsection