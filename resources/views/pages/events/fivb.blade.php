@extends('master')
@section('title')
    FIVB Beach Volleyball World Tour - Malaysia Open 2017
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    {{-- <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/fivb/fivb-banner.jpg')">
        <video id="video-background" class="hidden-xs" preload muted autoplay loop>
          <source src="images/fivb/fivb-vidbanner.mp4" type="video/mp4">
        </video>
      </div>  
    </section> --}}<!-- /Banner Section -->

    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/fivb/fivb-banner.jpg')}}" style="width: 100%" class="img-responsive"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/fivb/volleyball-thumb.jpg')}}" style="width: 100%" class="img-responsive">
        </div>
    </section>

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>FIVB Beach Volleyball World Tour - Malaysia Open 2017</h6>
                  Tickets from <span>RM34</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 15 - 16 April 2017</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Pantai Cenang, Langkawi <a target="_blank" href="https://goo.gl/maps/SrP2htzUX4G2">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8.00 AM</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <p>Malaysia will be hosting its first FIVB Beach Volleyball World Tour at Pantai Cenang on Saturday and Sunday, 15 and 16 April 2017 with 32 top male and female beach volleyball players from over 16 countries competing for the country’s first title. Malaysia will be ASEAN’s first and only nation to host the tournament. For more information, e-mail: fivbbeachvolleyballmalaysia@eplusglobal.com.</p>
                <p>The 2017 FIVB Beach Volleyball World Tour - Malaysia Open is organised by E-Plus Global Sdn Bhd; sanctioned by the Federation Internationale de Volleyball and Malaysia Volleyball Association; and supported by the Ministry of Culture and Tourism Malaysia, Malaysia Major Events, Langkawi Development Authority, Majlis Bandaraya Pelancongan Langkawi and Majlis Daerah Langkawi.</p>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/fivb/fivb-flyer.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/fivb/logo.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/fivb/men-team.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/fivb/women-team.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/fivb/logo1.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/fivb/logo2.jpg" alt="">
                  </div>
                  
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Normal Price</th>
                          <th>Remarks</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>Daily</td>
                              <td>RM 34.00</td>
                              <td rowspan="2"> Free Seating</td>
                              <td rowspan="2"><img class="img-responsive seatPlanImg" src="images/fivb/seatplan.jpg" alt=""></td>
                          </tr>
                          <tr>
                              <td>Season (2 days)</td>
                              <td>RM 54.00</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/fivb%20beach%20volleyball%20world%20tour%20%E2%80%93%20malaysia%20open%202017/events" role="button">BUY TICKETS</a>
                  <span class="or">/</span>                 
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>                  
                <div>
                <span class="importantNote">* Prices shown include RM4.00 AirAsiaRedTix fee & 6% GST</span> 
                <span class="importantNote">* Children age 7 and above would require to purchase ticket for admission. </span> 
                <span class="importantNote">*Children 6 years and below are admitted free of charge subject to being accompanied by paying guest.</span> 
                </div>
              </div>
            </div>
          </div>
        </section>

        

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('modal')

  <!--Modal Seat Plan image-->
  <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <div class="clearfix">&nbsp;</div>
          <img src="" class="enlargeImageModalSource" style="width: 100%;">
          </div>
      </div>
      </div>
  </div>
  
  <!-- Modal Where to Get Tix Location-->
  <div id="modalGetTixLoc" class="modal fade" role="dialog">
      <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <h2>GET YOUR TICKETS FROM:</h2>
          <dl class="dl-horizontal">
              <dt>Online:</dt>
              <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Rock Corner outlets:</dt>
              <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
              <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
              <dd>Bangsar Village (TEL: 03-22021139)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Victoria Music outlets:</dt>
              <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
              <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
              <dd>Tropicana</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Penang outlets:</dt>
              <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
              <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Istana Budaya Jalan<br>Tun Razak:</dt>
              <dd>03 40265558</dd>
          </dl>
          </div>
      </div>
      </div>
  </div>

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
    $(document).ready(function(){
      $("#announcementModal").modal('show');
    });
    </script>
    
@endsection