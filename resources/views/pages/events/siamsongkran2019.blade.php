@extends('master')
@section('title')
    Siam Songkran Music Festival
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Siam Songkran Music Festival" />
    <meta property="og:description" content="The debut edition of Siam Songkran Music Festival comes together as a brainchild of Thailand’s premiere festival organizers."/>
    <meta property="og:image" content="{{ Request::Url().'images/siamsongkran2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/siamsongkran2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Siam Songkran Music Festival"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/siamsongkran2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Siam Songkran Music Festival">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Siam Songkran Music Festival</h6> Tickets from <span>RM263</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  12th - 15th April 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Show DC Arena, Rama 9, Bangkok <a target="_blank" href="https://goo.gl/maps/dXTF6wTPxAu">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 5.00pm - 12.00am</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <p><h2>Siam Songkran Music Festival</h2><br/>
                                A world class Songkran festival experience with an exquisite musical line-up to match:</p>
                                <p>The debut edition of Siam Songkran Music Festival comes together as a brainchild of Thailand’s premiere festival organizers.</p>
                                <p>The four-day fiesta will be held from the 12th to 15th of April 2019 at SHOW DC in Bangkok. Siam Songkram aims to provide an all-round, interactive experience for attendees, hence a massive festival site with four respective areas.</p>
                                <p>Dance to your heart’s content across not one, but two state of the art stages with world class production that rivals many.</p>
                                <p>Partake in our many culture-based workshops that ranges from Muay Thai to cooking classes. It is not often that you’ll get the chance to learn how to cook Pad Thai at a rave!</p>
                                <p>Last but not least, is a very unique food and creative market section that will allow festival-goers the chance to try out an array of exquisite Thai delicacies while shopping for vintage trinkets.</p>
                                <p>Don’t miss out on what is yet to be a marvelous celebration of water, music and utmost joy to ring in the Thai New Year. We’ll see you there!</p>
                            </div>
                            {{-- <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/artist/6deZN1bslXzeGvOLaLMOIF" width="300" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div> --}}
                        </div>
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <img src="{{asset('images/siamsongkran2019/dj-list.jpg')}}" style="width: 100%" class="img-responsive" alt="Siam Songkran Music Festival">
                        </div>
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Q7j3tIyduNg?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/fZG1ani-DwI?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/siamsongkran2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/siamsongkran2019/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/siamsongkran2019/gallery-2.jpg" data-featherlight="image"><img class="" src="images/siamsongkran2019/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/siamsongkran2019/gallery-3.jpg" data-featherlight="image"><img class="" src="images/siamsongkran2019/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/siamsongkran2019/gallery-4.jpg" data-featherlight="image"><img class="" src="images/siamsongkran2019/gallery-4.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Description</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- <tr>
                                            <td rowspan="5">GA</td>
                                            <td>4 Day Pass Tier 2</td>
                                            <td>RM520.00</td>
                                        </tr>
                                        <tr>
                                            <td>12 April 2019 Tier 2</td>
                                            <td>RM230.00</td>
                                        </tr>
                                        <tr>
                                            <td>13 April 2019 Tier 2</td>
                                            <td>RM230.00</td>
                                        </tr>
                                        <tr>
                                            <td>14 April 2019 Tier 2</td>
                                            <td>RM230.00</td>
                                        </tr>
                                        <tr>
                                            <td>15 April 2019 Tier 2</td>
                                            <td>RM230.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td rowspan="5">VIP</td>
                                            <td>4 Day Pass Tier 2</td>
                                            <td>RM930.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>12 April 2019 Tier 2</td>
                                            <td>RM340.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>13 April 2019 Tier 2</td>
                                            <td>RM340.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>14 April 2019 Tier 2</td>
                                            <td>RM340.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>15 April 2019 Tier 2</td>
                                            <td>RM340.00</td>
                                        </tr> --}}
                                        <tr>
                                            <td rowspan="5">GA</td>
                                            <td>4 Day Pass Tier 3</td>
                                            <td>RM570.00</td>
                                        </tr>
                                        <tr>
                                            <td>12 April 2019 Tier 3</td>
                                            <td>RM255.00</td>
                                        </tr>
                                        <tr>
                                            <td>13 April 2019 Tier 3</td>
                                            <td>RM255.00</td>
                                        </tr>
                                        <tr>
                                            <td>14 April 2019 Tier 3</td>
                                            <td>RM255.00</td>
                                        </tr>
                                        <tr>
                                            <td>15 April 2019 Tier 3</td>
                                            <td>RM255.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td rowspan="5">VIP</td>
                                            <td>4 Day Pass Tier 3</td>
                                            <td>RM965.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>12 April 2019 Tier 3</td>
                                            <td>RM372.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>13 April 2019 Tier 3</td>
                                            <td>RM372.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>14 April 2019 Tier 3</td>
                                            <td>RM372.00</td>
                                        </tr>
                                        <tr style="background-color: #FCEFDC;">
                                            <td>15 April 2019 Tier 3</td>
                                            <td>RM372.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Apr 10 2019 00:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/siamsongkran2019/booking" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Transaction fee of RM8.00 per event ticket applicable for Internet purchase.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Adult: Strictly 20 years old and above.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });


    // Modal Announcement
	//$(document).ready(function(){
    //  $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. Special Limited offer at AirAsia RedTix only</p>
                        <p>2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection