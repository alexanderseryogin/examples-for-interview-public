@extends('master')
@section('title')
	 UFC FIGHT NIGHT SINGAPORE
@endsection

@section('header')
	@include('layouts.partials._header')
	
@endsection

@section('content')
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
{{-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet"> --}}
<link type="text/css" rel="stylesheet" href="css/custom2.css">
<link type="text/css" rel="stylesheet" href="css/clock.css">
<link type="text/css" rel="stylesheet" href="css/ufc221.css">
<link type="text/css" rel="stylesheet" href="css/timeTo.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<style type="text/css">
	.background-ufc {
		background-image: url('/images/ufcfnsg/background1.png');
	    background-size: 100% auto;
	    background-repeat: no-repeat;
	    height: 1000px;
	}
	.sect-count {
		top: 60%;
	}
	.open-sans {
		font-family: 'Open Sans', sans-serif;
		font-weight: 100;
	}
	.text-white {
		color: #fff;
	}
	.h2-count {
		font-size: 32px;
	}
	.btn-danger {
		background: linear-gradient(#b82941, #40151c);
	}
	.btn-wrapper {
		margin-top: 80px;
	}
	@media screen and (max-width: 767px){ 
		.background-ufc {
			background-image: url('/images/ufcfnsg/mobile-background.png');
			height: 1200px;
		}
		.sect-count {
			margin-top: 40px;
			margin-bottom: 40px;
		}
		.btn-wrapper {
			margin-top: 120px;
		}
	}
	@media screen and (min-width: 768px){ 
		.sect-count {
			top: 40%;
		}
		.background-ufc { 
			height: 910px;
		}
	}
	@media screen and (min-width: 1024px){ 
		.background-ufc { 
			height: 986px;
		}
		.sect-count {
			top: 50%;
		}
	}
	@media screen and (max-width: 1024px){ 
		.background-ufc { 
			height: 1096px;
		}
		.sect-count {
			top: 50%;
		}
	}
	@media screen and (min-width: 768px) and (max-width: 1024px){ 
		.background-ufc {
			height: 916px;
		}
		.sect-count {
			top: 50%;
		}
	}
	@media screen and (min-width: 1024px) and (max-width: 1366px){ 
		.background-ufc{
			height: 1200px;
		}
		.sect-count {
			top: 50%;
		}
	}
	@media screen and (min-width: 1400px){ 
		.background-ufc{
			height: 1400px;
		}
		.sect-count {
			top: 60%;
		}
	}
	@media screen and (max-width: 414px){ 
		.background-ufc { 
			height: 1260px;
		}
		.sect-count {
			top: 50%;
		}
	}
	
</style>
	<!-- Content Section -->
	<section class="pageContent">
	  <!-- Main Body -->
		<div class="mainBodyContent no-btm-mar" style="padding-top: 0px;">

			<section class="pageCategory-section last section-black background-ufc">
				<div class="col-md-12 col-xs-12 sect-count">
					<div class="col-md-12 col-xs-12"> 
						<h2 class="text-center text-white open-sans h2-count">Countdown to Special Packages to UFC Singapore!</h2>
						<div class="text-center" id="countdown" style="padding-top: 40px; padding-bottom: 80px;"></div>
					</div>
					<div class="col-md-12 col-xs-12 text-center btn-wrapper"> 
						<a class="btn btn-danger" href="https://docs.google.com/forms/d/e/1FAIpQLSe9tyIQ8OK4juJo_HAr-iOGnF6dTviNEAlhvE7GOX3cHxZ00A/viewform?usp=sf_link" role="button">Register Your Interest</a>
					</div>
				</div>
			</section>

		</div><!-- /Main Body -->
  
	</section><!-- /Content Section -->

@endsection

@section('customjs')
	{{-- countdown --}}
	<script src="js/jquery.time-to.js"></script>
	<script type="text/javascript">
		$('#countdown').timeTo({
		    timeTo: new Date(new Date('Saturday June 23 2018 00:00:00 GMT+0800 (+08)')),
		    displayDays: 2,
		    theme: "black",
		    displayCaptions: true,
		    fontSize: 48,
		    captionSize: 14
		}); 
	</script>
	{{-- /countdown --}}

	<script type="text/javascript">
	//Initialize Swiper
	var swiper = new Swiper('.swiper-container', {
		pagination: '.swiper-pagination',        
		paginationClickable: true,
		slidesPerView: 'auto',
		spaceBetween: 10,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		freeMode: true
	});

	// Enlarge Seat Plan Image
	$(function() {
		$('.seatPlanImg').on('click', function() {
		$('.enlargeImageModalSource').attr('src', $(this).attr('src'));
		$('#enlargeImageModal').modal('show');
		});
	});

	// Hide top Banner when page scroll
	var header = $('.eventBanner');
	var range = 450;

	$(window).on('scroll', function () {
		
		var scrollTop = $(this).scrollTop();
		var offset = header.offset().top;
		var height = header.outerHeight();
		offset = offset + height;
		var calc = 1 - (scrollTop - offset + range) / range;

		header.css({ 'opacity': calc });

		if ( calc > '1' ) {
		header.css({ 'opacity': 1 });
		} else if ( calc < '0' ) {
		header.css({ 'opacity': 0 });
		}
	});

	// Smooth scroll for acnhor links
	$('a[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		if (target.length) {
			$('html, body').animate({
			scrollTop: target.offset().top
			}, 1000);
			return false;
		}
		}
	});

	</script>

	{{-- Buy button disable --}}
	<script type="text/javascript">
		$(function() {
			$('a[id^=buyButton]').each(function() {
				var date = new Date();
				var enddate = $(this).attr('datetime'); 
				if ( Date.parse(date) >= Date.parse(enddate)) {
				  $(this).addClass('disabled');
				}
			});
		});
	</script>
	
@endsection

@section('modal')
	@include('layouts.partials.modals._seatplan')
	@include('layouts.partials.modals._getTixCustom')

@endsection