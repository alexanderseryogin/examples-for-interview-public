@extends('master')
@section('title')
    Jacky Wu & Family Live In Genting
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Jacky Wu & Family Live In Genting" />
    <meta property="og:description" content="Jacky Wu & Family Live In Genting" />
    <meta property="og:image" content="{{ Request::Url().'images/genting/jackywu2018/thumbnail.png' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/genting/jackywu2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Jacky Wu & Family Live In Genting"></div>
      <div class="widewrapper main hidden-lg hidden-md hidden-sm">
      <img src="{{asset('images/genting/jackywu2018/thumbnail.jpg')}}" width="100%" class="img-responsive" alt="Jacky Wu & Family Live In Genting">
      </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>Jacky Wu & Family Live In Genting</h6>
                  Tickets from <span>RM134</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 25 August 2018 (Saturday)</div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 08.30pm</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Arena Of Stars, Genting Highland <a target="_blank" href="https://www.google.com/maps/place/Genting+Arena+of+Stars/@3.423828,101.7910333,17z/data=!3m1!4b1!4m5!3m4!1s0x31cc14072aa30697:0xb4fadf09a74b1b30!8m2!3d3.423828!4d101.793222">View Map</a></div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <h2>憲在攻頂  開麥啦!</h2>
                <p>Jacky Wu吴宗宪 is an accomplished variety show host, singer and actor in Taiwan.  He hosts numerous variety shows and known for his quick witted humor and open fire talks. He was nominated 3 times as The Best Male Vocalist in Taiwan Golden Melody Awards and won The Best Male Vocalist Taiwanese in 1994.</p>
                <p>Now, Jacky is bringing his family from variety shows and music partners to Resorts World Genting, presenting the most humorous stand-up comedy concert with SBDW 咻比嘟華 (Will Liu劉畊宏, Jason Chung小鐘, Morrison Ni小馬), 辛隆Shin Lung, 陳斯亞 Siya, 高以馨Sun.</p>
                <p>歌手出身的台湾综艺天王吴宗宪，暌违多年推出全新风格综艺演唱会。这一次还带来了咻比嘟哗成员--刘畊宏、小马（倪子钧）、小钟（钟昀呈），还有辛龙、陈斯亚和高以馨。</p>
                <p>2个小时的节目不仅有搞笑幽默的talking，经典歌曲深情诠释，更加入了舞蹈、模仿、笑料、观众互动等，从开场至结束，家喻户晓的K歌，笑到喷泪的内容，绝不冷场。</p>
                <p>势必带给你史上最好笑的综艺演唱会。</p>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
              <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <a href="images/jackywu2018/poster.jpg" data-featherlight="image"><img class="" src="images/genting/jackywu2018/gallery-1.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/jackywu2018/web-banner.png" data-featherlight="image"><img class="" src="images/genting/jackywu2018/web-banner.jpg" alt=""></a>
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Normal</th>
                          <th>GRC 10% discount</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td><span class="box red">PS1</span></td>
                            <td>RM 465.00</td>
                            <td>RM 418.90</td>
                            <td rowspan="4"><img class="img-responsive seatPlanImg" src="images/genting/jackywu2018/seat-plan.jpg" alt=""></td>
                          </tr>
                          <tr>
                            <td><span class="box green">PS2</span></td>
                            <td>RM 342.00</td>
                            <td>RM 308.20</td>
                          </tr>
                          <tr>
                            <td><span class="box grey">PS3</span></td>
                            <td>RM 229.00</td>
                            <td>RM 206.50</td>
                          </tr>
                          <tr>
                            <td><span class="box midblue">PS4</span></td>
                            <td>RM 134.00</td>
                            <td>N/A</td>
                          </tr>        
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" datetime="Aug 25 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/jacky%20wu%20-%20family%20live%20in%20genting/events" role="button">BUY TICKETS</a>
                  {{-- <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                </div>
                <div class="alert alert-info"><i class="fa fa-info-circle" aria-hidden="true"></i> Online purchase has closed. Please proceed to venue.</div>
                <span class="importantNote">** Ticket pricing include RM 4 processing fee.</span>
                <span class="importantNote">** GWC/GRC discount is applicable via outlets only.</span><br>
                <span class="importantNote">** The GRC’s details (i.e. member’s name &amp; number) are compulsory to fill up for the purchase of GRC discounted rates. Failure which, the normal ticket rates will be charged.</span><br>
                <div class="note text-left alert alert-warning">
                  <div class="media">
                    <div class="media-left media-middle">
                      <a href="#">
                        <img class="media-object" src="images/oku.png" alt="">
                      </a>
                    </div>
                    <div class="media-body">
                      <h2>OKU Info</h2>
                      <p>Seats for those with special needs are located in the easily accessible zone (marked with the wheelchair sign on the floor plan). For ticket reservations, please call: 03-2718 1118.</p>
                    </div>
                  </div>
                </div>
                <p>3rd party collection will require authorisation letter and photocopy of the NRIC of ticket holder.</p>
                <p>Please be informed that all the tickets which issued by Ticketing Agent no longer to redeem at Genting Box Office. Guest may directly enter to the concert with using their original purchased ticket. However, guest who holding the Confirmation Letter/E-Ticket for any concert/show shall remain to exchange their ticket at Arena of Star counter, Genting.</p>
                <div class="note text-left">
                <h2>CONCERT TERMS & CONDITIONS:</h2>
                  <ul>
                    <li>Each ticket is valid for one-time admission only.</li>
                    <li>Child must be of 95cm height and below to be eligible for child ticket priced at RM50 without seat, while child at height above 95cm must purchase tickets according to price scale. Terms &amp; Conditions applies.</li>
                    <li>Strictly no video recording and photography during the show.</li>
                    <li>Video cameras, cameras and tablet computers such as iPad are not allowed in the venue.</li>
                    <li>Lost or damaged ticket(s) will not be entertained.</li>
                    <li>The concert starts at 8.30pm sharp, so please be seated by or before then.</li>
                    <li>Late arrival may result in non-admittance until a suitable break in the performance.</li>
                    <li>每张门票只限入场一次。</li>
                    <li>身高95公分及以下的儿童需购买价值50令吉只限入场不附席位的儿童票；而身高超过95公分以上的儿童则需依据票价购票。须符合条规。</li>
                    <li>演出进行期间，一律不允许录影及摄影。</li>
                    <li>录影机、相机及智能性平板电脑，如：iPad等，一律不允许带进表演现场。</li>
                    <li>门票若遗失或损坏，恕不受理。</li>
                    </li>演唱会将于晚上8点30分开始，请准时或提前入场。</li>
                    <li>迟到可能导致禁止入场，直至演出小休时间为止。</li>
                  </ul>

                  <h2>IMPORTANT NOTE:</h2>
                  <ol>
                    {{-- <li>Prices shown inclusive RM 4.00 AirAsiaRedTix fee and 6% GST.</li> --}}
                    <li>Prices shown inclusive RM 4.00 AirAsiaRedTix fee.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                  </ol>
                  <h2>Enquiries Hotline:</h2>
                  <p>Genting: 03-2718 1118 or visit website www.rwgenting.com</p>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')
<script type="text/javascript">
//Initialize Swiper
var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',        
    paginationClickable: true,
    slidesPerView: 'auto',
    spaceBetween: 10,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    freeMode: true
});

// Enlarge Seat Plan Image
$(function() {
    $('.seatPlanImg').on('click', function() {
    $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
    $('#enlargeImageModal').modal('show');
    });
});

// Hide top Banner when page scroll
var header = $('.eventBanner');
var range = 450;

$(window).on('scroll', function () {
    
    var scrollTop = $(this).scrollTop();
    var offset = header.offset().top;
    var height = header.outerHeight();
    offset = offset + height;
    var calc = 1 - (scrollTop - offset + range) / range;

    header.css({ 'opacity': calc });

    if ( calc > '1' ) {
    header.css({ 'opacity': 1 });
    } else if ( calc < '0' ) {
    header.css({ 'opacity': 0 });
    }
});

// Smooth scroll for acnhor links
$('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if (target.length) {
        $('html, body').animate({
        scrollTop: target.offset().top
        }, 1000);
        return false;
    }
    }
});
</script>

{{-- Buy button disable --}}
<script type="text/javascript">
    $(function() {
        $('a[id^=buyButton]').each(function() {
            var date = new Date();
            var enddate = $(this).attr('datetime'); 
            if ( Date.parse(date) >= Date.parse(enddate)) {
              $(this).addClass('disabled');
            }
        });
    });
</script>
@endsection

@section('modal')
@include('layouts.partials.modals._seatplan')
@include('layouts.partials.modals._getTix')
@endsection