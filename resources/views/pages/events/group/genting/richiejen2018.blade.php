@extends('master')
@section('title')
  Richie Jen Live In Genting
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Richie Jen Live In Genting" />
    <meta property="og:description" content="Richie Jen first found fame with his debut album, the Mandarin-language Ask Again in 1990, along with his role in the 1991 Taiwanese comedy film Cops &amp; Robbers." />
    <meta property="og:image" content="{{ Request::Url().'images/genting/richiejen2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/genting/richiejen2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Richie Jen Live In Genting"></div>
      <div class="widewrapper main hidden-lg hidden-md hidden-sm">
      <img src="{{asset('images/genting/richiejen2018/thumbnail.jpg')}}" width="100%" class="img-responsive" alt="Richie Jen Live In Genting">
      </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>Richie Jen Live In Genting / 任贤齐2018云顶演唱会</h6>
                  Tickets from <span>RM121</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>Title</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 19th - 20th October 2018 (Friday &amp; Saturday)</div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8.30pm</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Arena Of Stars, Genting Highland <a target="_blank" href="https://www.google.com/maps/place/Genting+Arena+of+Stars/@3.423828,101.7910333,17z/data=!3m1!4b1!4m5!3m4!1s0x31cc14072aa30697:0xb4fadf09a74b1b30!8m2!3d3.423828!4d101.793222">View Map</a></div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <h2>Richie Jen Live In Genting / 任贤齐2018云顶演唱会</h2>
                <p>Richie Jen first found fame with his debut album, the Mandarin-language Ask Again in 1990, along with his role in the 1991 Taiwanese comedy film Cops &amp; Robbers. Signing with Rock Records in 1996, Jen’s star began to shine brighter with the release of his breakthrough album Too Softhearted, which became a massive success, selling over two million copies across Asia and the title track becoming his signature song. Additional success followed with the release of 1998’s Love Like The Pacific Ocean, featuring the hit song The Sad Pacific, which helped the album sell more than a million copies in Taiwan alone. Musical success continued through the 2000s, all the way to the release of his last studio album, 2011’s Daredevil Spirit and another one of his signature songs, Fold.</p>
                <p>In addition to his musical successes, Jen has also found great fame in front of the camera. With over 40 movie roles under his belt, his roles range from comedies like Summer Holiday, romantic fantasies like Fly Me To Polaris and historical epics like Legendary Amazons. He also made his debut as a screenwriter and director in the recent All You Need Is Love. Beyond the cinema, Jen has also been a regular fixture on the small screen, acting in series such as 1998’s The Return Of Condor Heroes and 2011’s The New Adventures of Chor Lau Heung. Jen also served as presenter on Top Gear China and has been a part of the Chinese Idol judging panel since 2013.</p>
                <p>任賢齊 是影歌视三栖发展的当红艺人。他于1996年凭借个人专辑《心太软》一夜爆红，不仅被列入该年《中国最有影响力的十件大事》之一，同时也在亚洲各地区荣获多个音乐大奖，广受歌迷的欢迎。他于2001年推出专辑《飞鸟》，荣获《第八届Channel V华语音乐榜中榜》“最受欢迎歌曲奖”和“全国最佳人气男歌手奖”，风头一时无两。他推出很多脍炙人口的歌曲包括《春天花会开》、《我是一只鱼》和《浪花一朵朵》等。</p>
                <p>唱而优则演的他也曾参演多部影视作品包括《笑傲江湖》、《新楚留香》、《星愿》、《夏日的么么茶》和《大事件》等，不仅创下票房佳绩，更凭借电影《树大招风》获得《第三十六届香港电影金像奖》和《第十一届亚洲电影大奖》“最佳男主角”提名。</p>
                </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
              <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <a href="images/genting/richiejen2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/genting/richiejen2018/gallery-1.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/genting/richiejen2018/web-banner.jpg" data-featherlight="image"><img class="" src="images/genting/richiejen2018/web-banner.jpg" alt=""></a>
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th colspan="2" style="background:#666;">19th Oct 2018</th>
                          <th colspan="2">20th Oct 2018</th>
                          <th style="background:#666;">Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td></td>
                            <td>Normal Price</td>
                            <td>GRC Discount 10%</td>
                            <td>Normal Price</td>
                            <td>GRC Discount 10%</td>
                            <td rowspan="6"><img class="img-responsive seatPlanImg" src="images/genting/richiejen2018/seat-plan.jpg" alt=""></td>
                          </tr>
                          <tr>
                            <td><span class="box purple">VIP</span></td>
                            <td>RM 481.00</td>
                            <td>RM 433.30</td>
                            <td>RM 534.00</td>
                            <td>RM 481.00</td>
                          </tr>
                          <tr>
                            <td><span class="box red">PS1</span></td>
                            <td>RM 391.00</td>
                            <td>RM 352.30</td>
                            <td>RM 434.00</td>
                            <td>RM 391.00</td>
                          </tr>
                          <tr>
                            <td><span class="box green">PS2</span></td>
                            <td>RM 301.00</td>
                            <td>RM 271.30</td>
                            <td>RM 334.00</td>
                            <td>RM 301.00</td>
                          </tr>
                          <tr>
                            <td><span class="box grey">PS3</span></td>
                            <td>RM 211.00</td>
                            <td>RM 190.30</td>
                            <td>RM 234.00</td>
                            <td>RM 211.00</td>
                          </tr>
                          <tr>
                            <td><span class="box midblue">PS4</span></td>
                            <td>RM 121.00</td>
                            <td>N/A</td>
                            <td>RM 134.00</td>
                            <td>N/A</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger" id="buyButton" datetime="Oct 20 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/richie jen live in genting/events" role="button">BUY TICKETS</a>
                  {{-- <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                </div>
                {{-- <div class="alert alert-info"><i class="fa fa-info-circle" aria-hidden="true"></i> Online purchase has closed. Please proceed to venue.</div> --}}
                <span class="importantNote">** Ticket pricing include RM4 processing fee.</span>
                <span class="importantNote">** GWC/GRC discount is applicable via outlets only.</span><br>
                <span class="importantNote">** All numbered seat.</span>
                <span class="importantNote">** The GRC’s details (i.e. member’s name &amp; number) are compulsory to fill up for the purchase of GRC discounted rates. Failure which, the normal ticket rates will be charged.</span><br>
                <div class="note text-left alert alert-warning">
                  <div class="media">
                    <div class="media-left media-middle">
                      <a href="#">
                        <img class="media-object" src="images/oku.png" alt="">
                      </a>
                    </div>
                    <div class="media-body">
                      <h2>OKU Info</h2>
                      <p>Seats for those with special needs are located in the easily accessible zone (marked with the wheelchair sign on the floor plan). For ticket reservations, please call: 03-2718 1118.</p>
                    </div>
                  </div>
                </div>
                <p>3rd party collection will require authorisation letter and photocopy of the NRIC of ticket holder.</p>
                <p>Please be informed that all the tickets which issued by Ticketing Agent no longer to redeem at Genting Box Office. Guest may directly enter to the concert with using their original purchased ticket. However, guest who holding the Confirmation Letter/E-Ticket for any concert/show shall remain to exchange their ticket at Arena of Star counter, Genting.</p>
                <div class="note text-left">
                <h2>CONCERT TERMS & CONDITIONS:</h2>
                  <ul>
                    <li>Each ticket is valid for one-time admission only.</li>
                    <li>Child must be of 95cm height and below to be eligible for child ticket priced at RM50 without seat, while child at height above 95cm must purchase tickets according to price scale. Terms &amp; Conditions applies.</li>
                    <li>Strictly no video recording and photography during the show.</li>
                    <li>Video cameras, cameras and tablet computers such as iPad are not allowed in the venue.</li>
                    <li>Lost or damaged ticket(s) will not be entertained.</li>
                    <li>The concert starts at 8.30pm sharp, so please be seated by or before then.</li>
                    <li>Late arrival may result in non-admittance until a suitable break in the performance.</li>
                    <li>每张门票只限入场一次。</li>
                    <li>身高95公分及以下的儿童需购买价值50令吉只限入场不附席位的儿童票；而身高超过95公分以上的儿童则需依据票价购票。须符合条规。</li>
                    <li>演出进行期间，一律不允许录影及摄影。</li>
                    <li>录影机、相机及智能性平板电脑，如：iPad等，一律不允许带进表演现场。</li>
                    <li>门票若遗失或损坏，恕不受理。</li>
                    </li>演唱会将于晚上8点30分开始，请准时或提前入场。</li>
                    <li>迟到可能导致禁止入场，直至演出小休时间为止。</li>
                  </ul>

                 <h2>IMPORTANT NOTE:</h2>
                  <ol>
                    <li>Prices shown includes RM4.00 AirAsiaRedTix fee.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 1 week prior to event day, subject to availability.</li>
                  </ol>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')
<script type="text/javascript">
//Initialize Swiper
var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',        
    paginationClickable: true,
    slidesPerView: 'auto',
    spaceBetween: 10,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    freeMode: true
});

// Enlarge Seat Plan Image
$(function() {
    $('.seatPlanImg').on('click', function() {
    $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
    $('#enlargeImageModal').modal('show');
    });
});

// Hide top Banner when page scroll
var header = $('.eventBanner');
var range = 350;

$(window).on('scroll', function () {
    
    var scrollTop = $(this).scrollTop();
    var offset = header.offset().top;
    var height = header.outerHeight();
    offset = offset + height;
    var calc = 1 - (scrollTop - offset + range) / range;

    header.css({ 'opacity': calc });

    if ( calc > '1' ) {
    header.css({ 'opacity': 1 });
    } else if ( calc < '0' ) {
    header.css({ 'opacity': 0 });
    }
});

// Smooth scroll for acnhor links
$('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if (target.length) {
        $('html, body').animate({
        scrollTop: target.offset().top
        }, 1000);
        return false;
    }
    }
});
</script>

{{-- Buy button disable --}}
<script type="text/javascript">
    $(function() {
        $('a[id^=buyButton]').each(function() {
            var date = new Date();
            var enddate = $(this).attr('datetime'); 
            if ( Date.parse(date) >= Date.parse(enddate)) {
              $(this).addClass('disabled');
            }
        });
    });
</script>

@endsection

@section('modal')
@include('layouts.partials.modals._seatplan')
@include('layouts.partials.modals._getTix')
@endsection