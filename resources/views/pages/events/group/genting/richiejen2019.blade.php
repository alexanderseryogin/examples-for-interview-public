@extends('master')
@section('title')
  Richie Jen
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Richie Jen" />
    <meta property="og:description" content="Richie Jen / 任贤齐" />
    <meta property="og:image" content="{{ Request::Url().'images/genting/richiejen2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/genting/richiejen2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Richie Jen"></div>
      <div class="widewrapper main hidden-lg hidden-md hidden-sm">
      <img src="{{asset('images/genting/richiejen2019/thumbnail.jpg')}}" width="100%" class="img-responsive" alt="Richie Jen">
      </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>Richie Jen</h6> Tickets from <span>RM154</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>Title</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 08 - 09 November 2019 (Friday/Saturday)</div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8.30pm</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Arena Of Stars, Genting Highland <a target="_blank" href="https://goo.gl/maps/bj4CgzB5ajt">View Map</a></div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <h2>Richie Jen / 任贤齐</h2>
                <p>Fans of Richie Jen, get ready for a treat! The Taiwanese singer-actor is returning once again to the stage of Resort World Genting’s Arena of Stars after a successful concert last year. So save the dates – 8-9 November 2019 – as the heartthrob prepares for the very special Richie Jen Live in Genting 2019 concerts.</p>

                <p>Born in Changhua, Richie Jen first found fame in the musical world with his debut album, the
                Mandarin-language Ask Again in 1990. The next year, Jen branched out as a multi-talented with his role in the 1991 Taiwanese comedy film Cops &amp; Robbers. After establishing himself, Jen signed with Rock Records in 1996, when his star began to shine brighter with the release of his breakthrough album Too Softhearted. A massive success, the album sold over 2 million copies across Asia and the title track has now become one of his signature songs. Jen then continued to climb the ladder of stardom, with the release of 1998’s Love Like The Pacific Ocean, featuring the hit song The Sad Pacific, which propelled the album to sales of more than a million copies in Taiwan alone. Jen’s success continued through the 2000s, all the way to the release of his last studio album, 2011’s Daredevil Spirit and another addition to his repertoire of signature songs, Fold.</p>

                <p>In addition to his musical successes, Jen has also found great fame in front of the camera. With over 40 movie roles under his belt, his roles range from comedies like Summer Holiday, romantic fantasies like Fly Me To Polaris and historical epics like Legendary Amazons. He also made his debut as a screenwriter and director in 2015’s All You Need Is Love. Beyond the cinema, Jen is also a regular fixture on the small screen, acting in series such as 1998’s The Return Of Condor Heroes and 2011’s The New Adventures of Chor Lau Heung. Jen also served as presenter on Top Gear China and has been a part of the Chinese Idol judging panel since 2013.</p>

                </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
              <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <a href="images/genting/richiejen2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/genting/richiejen2019/gallery-1.jpg" alt=""></a>
                  </div>
                  <div class="swiper-slide">
                    <a href="images/genting/richiejen2019/web-banner.jpg" data-featherlight="image"><img class="" src="images/genting/richiejen2019/web-banner.jpg" alt=""></a>
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>08 November 2019 (Friday)</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Normal</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td><span class="box red">VIP</span></td>
                            <td>RM 586</td>
                            <td rowspan="5"><img class="img-responsive seatPlanImg" src="images/genting/richiejen2019/seat-plan.jpg" alt=""></td>
                          </tr>
                          <tr>
                            <td><span class="box green">PS1</span></td>
                            <td>RM 406</td>
                          </tr>
                          <tr>
                            <td><span class="box yellow">PS2</span></td>
                            <td>RM 316</td>
                          </tr>
                          <tr>
                            <td><span class="box grey">PS3</span></td>
                            <td>RM 226</td>
                          </tr>
                          <tr>
                            <td><span class="box midblue">PS4</span></td>
                            <td>RM 154</td>
                          </tr>
                      </tbody>
                  </table>
                </div>

                <div class="text-center">                  
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>09 November 2019 (Saturday)</p>
                </div>

                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Normal</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td><span class="box red">VIP</span></td>
                            <td>RM 650</td>
                            <td rowspan="5"><img class="img-responsive seatPlanImg" src="images/genting/richiejen2019/seat-plan.jpg" alt=""></td>
                          </tr>
                          <tr>
                            <td><span class="box green">PS1</span></td>
                            <td>RM 450</td>
                          </tr>
                          <tr>
                            <td><span class="box yellow">PS2</span></td>
                            <td>RM 350</td>
                          </tr>
                          <tr>
                            <td><span class="box grey">PS3</span></td>
                            <td>RM 250</td>
                          </tr>
                          <tr>
                            <td><span class="box midblue">PS4</span></td>
                            <td>RM 170</td>
                          </tr>
                      </tbody>
                  </table>
                </div>

                <div class="buyAlert-bar">
                  <a class="btn btn-danger" id="buyButton" datetime="Aug 08 2019 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/richie jen live in genting 2019/events" role="button">BUY TICKETS</a>
                  {{-- <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                </div>
                {{-- <div class="alert alert-info"><i class="fa fa-info-circle" aria-hidden="true"></i> Online purchase has closed. Please proceed to venue.</div> --}}
                <span class="importantNote">** Ticket pricing include RM 4 processing fee.</span><br/>
                <div class="note text-left alert alert-warning">
                  <div class="media">
                    <div class="media-left media-middle">
                      <a href="#">
                        <img class="media-object" src="images/oku.png" alt="">
                      </a>
                    </div>
                    <div class="media-body">
                      <h2>OKU Info</h2>
                      <p>Seats for those with special needs are located in the easily accessible zone (marked with the wheelchair sign on the floor plan). For ticket reservations, please call: 03-2718 1118.</p>
                    </div>
                  </div>
                </div>
                <p>3rd party collection will require authorisation letter and photocopy of the NRIC of ticket holder.</p>
                <p>Please be informed that all the tickets which issued by Ticketing Agent no longer to redeem at Genting Box Office. Guest may directly enter to the concert with using their original purchased ticket. However, guest who holding the Confirmation Letter/E-Ticket for any concert/show shall remain to exchange their ticket at Arena of Star counter, Genting.</p>
                <div class="note text-left">
                <h2>CONCERT TERMS & CONDITIONS:</h2>
                  <ul>
                    <li>The concert starts at 8.30pm sharp, so please be seated by or before then. / 演唱会将于晚上8点30分开始，请准时或提前入场。</li>
                    <li>Late arrival may result in non-admittance until a suitable break in the performance. / 迟到可能导致禁止入场，直至演出小休时间为止。</li>
                    <li>Every customer MUST enter the hall with a ticket, including children of all ages & infants. / 每位观众必需凭票入场, 包括任何岁数的儿童与婴孩.</li>
                    <li>Each ticket is valid for one-time admission only. / 每张门票只限入场一次。</li>
                    <li>Strictly no video recording and photography during the show. / 演出进行期间，一律不允许录影及摄影。</li>
                    <li>Video cameras, cameras and tablet computers such as iPad are not allowed in the venue. / 录影机、相机及智能性平板电脑，如：iPad等，一律不允许带进表演现场。</li>
                    <li>Lost or damaged ticket(s) will not be entertained. / 门票若遗失或损坏，恕不受理</li>
                  </ul>
                  <h2>IMPORTANT NOTE:</h2>
                  <ol>
                    {{-- <li>Prices shown excludes RM 4.00 AirAsiaRedTix fee.</li> --}}
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                  </ol>
                  <h2>Enquiries Hotline:</h2>
                  <p>Genting: 03-2718 1118 or visit website www.rwgenting.com</p>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                </div>
              </div>
            </div>
          </div>

        </section>
      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')
<script type="text/javascript">
//Initialize Swiper
var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',        
    paginationClickable: true,
    slidesPerView: 'auto',
    spaceBetween: 10,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    freeMode: true
});

// Enlarge Seat Plan Image
$(function() {
    $('.seatPlanImg').on('click', function() {
    $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
    $('#enlargeImageModal').modal('show');
    });
});

// Hide top Banner when page scroll
var header = $('.eventBanner');
var range = 450;

$(window).on('scroll', function () {
    
    var scrollTop = $(this).scrollTop();
    var offset = header.offset().top;
    var height = header.outerHeight();
    offset = offset + height;
    var calc = 1 - (scrollTop - offset + range) / range;

    header.css({ 'opacity': calc });

    if ( calc > '1' ) {
    header.css({ 'opacity': 1 });
    } else if ( calc < '0' ) {
    header.css({ 'opacity': 0 });
    }
});

// Smooth scroll for acnhor links
$('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if (target.length) {
        $('html, body').animate({
        scrollTop: target.offset().top
        }, 1000);
        return false;
    }
    }
});
</script>

{{-- Buy button disable --}}
<script type="text/javascript">
    $(function() {
        $('a[id^=buyButton]').each(function() {
            var date = new Date();
            var enddate = $(this).attr('datetime'); 
            if ( Date.parse(date) >= Date.parse(enddate)) {
              $(this).addClass('disabled');
            }
        });
    });
</script>
@endsection

@section('modal')
@include('layouts.partials.modals._seatplan')
@include('layouts.partials.modals._getTix')
@endsection