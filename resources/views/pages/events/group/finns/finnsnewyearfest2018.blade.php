@extends('master')
{{-- @extends('masterforpotatohead') --}}
@section('title')
    Finns Beach Club New Year's Music Festival
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Finns Beach Club New Year's Music Festival" />
    <meta property="og:description" content="Finns Beach Club New Years Festival. BIG NEWS! New Year’s in Bali just went next level."/>
    <meta property="og:image" content="{{ Request::Url().'images/finns/finnsny2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="css/custom2.css">
    <link type="text/css" rel="stylesheet" href="css/clock.css">
    <link type="text/css" rel="stylesheet" href="/css/custom/redtix.css">
    <link type="text/css" rel="stylesheet" href="css/timeTo.css">

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/finns/finnsny2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Finns Beach Club New Year's Music Festival"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/finns/finnsny2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Finns Beach Club New Year's Music Festival">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Finns Beach Club New Year's Music Festival</h6> Tickets from <span>IDR1,400,000</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  31st Dec 2018 - 1st Jan 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Finns Beach Club <a target="_blank" href="https://goo.gl/maps/VnH57acDtQJ2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Monday - Tuesday</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <div class="col-sm-8">
                                    <p><h2>Finns Beach Club New Year's Music Festival</h2><br/>
                                    Introducing the Finns Beach Club 2 day music festival featuring Martin Solveig, Robin Schulz and Roger Sanchez for NYE and our New Years Day lineup featuring BROODS, Clean Bandit, Khalid, Robinson Music, The Rubens and The Wombats. Get your tickets to the biggest New Years in Bali!</a>
                                </div>
                                <div class="col-sm-4">
                                    <iframe src="https://open.spotify.com/embed/user/21gkyxyzeeoya6r2dnok4u2oq/playlist/7vWIhCXk6HvNyVVS89Tm0x" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>
                            <div class="col-sm-12 text-center">
                                <hr>
                                <p><b>N E W   Y E A R S   E V E</b><br/><br/>
                                    3:00 – 4:10 <b>Fadi</b><br/>
                                    4:10 – 5:20 <b>Kaiser</b><br/>
                                    5:20 – 6:30 <b>Yuki</b><br/>
                                    6:30 – 8:30 <b>Roger Sanchez</b><br/>
                                    8:30 – 10:15 <b>Robin Schulz</b><br/>
                                    10:15 – 12:15 <b>Martin Solveig</b><br/>
                                    12:15 – 1:15 <b>Artist TBA</b><br/>
                                    1:15 – 2:00 <b>Koyuki</b><br/><br/><br/></p>
                                <p><b>N E W   Y E A R S   D A Y</b><br/><br/>
                                    12:00 – 12:45 <b>Silent Alarm</b><br/>
                                    1:00 – 1:45 <b>Thearosa</b><br/>
                                    2:00 – 3:00 <b>Loups</b><br/>
                                    3:30 – 4:30 <b>Robinson</b><br/>
                                    5:00 – 6:00 <b>The Rubens</b><br/>
                                    6:30 – 7:40 <b>Clean Bandit</b><br/>
                                    8:10 – 9:10 <b>Broods</b><br/>
                                    9:40 – 10:40 <b>The Wombats</b><br/>
                                    11:00 – 12:00 <b>Khalid</b></p>
                            </div>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/9SHApiiqQfA?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/finns/finnsny2018/gallery-dj-1.jpg" data-featherlight="image"><img class="" src="images/finns/finnsny2018/gallery-dj-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">

                            @include('layouts.partials._showticketsnodisc')

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude IDR30,000 AirAsiaRedTix fee.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close mid day prior to the event, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            {{-- <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>GA - NYE</td>
                                            <td>IDR1,400,000</td>
                                            <td>New Year's Eve Day Pass</td>
                                        </tr>
                                        <tr>
                                            <td>GA - NYD</td>
                                            <td>IDR1,400,000</td>
                                            <td>New Year's Day Pass</td>
                                        </tr>
                                        <tr>
                                            <td>GA - 2 Day</td>
                                            <td>IDR2,400,000</td>
                                            <td>2 Days Pass</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Dec 30 2018 12:00:00 GMT+0800" target="_blank" href="/finnsnewyearfestival-tickets" role="button">BUY TICKETS</a>
                                <a class="btn btn-danger" id="buyButton" datetime="Dec 30 2018 12:00:00 GMT+0800" target="_blank" href="https://redtix.cognitix.id/aart/finns-newyearfestival/booking" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude IDR30,000 AirAsiaRedTix fee.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close mid day prior to the event, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section> --}}
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
    @include('layouts.partials.modals._moreInfo')
@endsection