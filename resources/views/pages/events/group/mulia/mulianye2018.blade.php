@extends('master')
@section('title')
    Mulia Bali NYE Celebration
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Mulia Bali NYE Celebration" />
    <meta property="og:description" content="This festive season, live up the festive mood by bringing your family and friends to a spectacular world-class professional acrobatic circus show all the way from Russia."/>
    <meta property="og:image" content="{{ Request::Url().'images/mulia/nye2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/mulia/nye2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Mulia Bali NYE Celebration"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/mulia/nye2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Mulia Bali NYE Celebration">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>MULIA BALI NYE CELEBRATION</h6> Tickets from <span>IDR422,290</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  30th - 31st Dec 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  The Mulia, Bali  <a target="_blank" href="https://goo.gl/maps/N96zZk4cZaF2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Sunday - Monday, 30-31 December</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>MULIA BALI NYE CELEBRATION</h2></p>
                            <p><b>Mulia Circus Acrobatica</b><br/>
                            30 December 2018<br/>
                            The Grand Ballroom from 3 PM TO 6 PM</p>
                            <p>This festive season, live up the festive mood by bringing your family and friends to a spectacular world-class professional acrobatic circus show all the way from Russia.</p>
                            <p>The show promises to be thriving entertainment; from an exciting display of live acrobatic performances and graceful theatrical aerial ballet dance to magnificent clown act.<br/>
                            *Children at all ages are welcome to see the show</p>
                            <p><b>MACY KATE</b><br/>
                            31 December 2018<br/>
                            The Grand Ballroom from 10 PM TO 1 AM</p>
                            <p>Join the New Year countdown with a special performance by award-winning star, MACY KATE and a spectacular world-class professional acrobatic show with the finest Russian clown act, concluding with a Live DJ that will surely bring some magical moments to end the year.<br/>
                            *Admission only from 13 years old and above</p>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/9SHApiiqQfA?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div> --}}
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                    <a href="images/mulia/nye2018/gallery-1.jpg" data-featherlight="image"><img class="img-responsive" src="images/mulia/nye2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/mulia/nye2018/gallery-1.jpg" data-featherlight="image"><img class="img-responsive" src="images/mulia/nye2018/gallery-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">

                            @include('layouts.partials._showticketsnodisc')

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude IDR30,000 AirAsiaRedTix fee.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close mid day prior to the event, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Date</th>
                                            <th>Type</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="3">Standard GA</td>
                                            <td rowspan="2">30th Dec 2018</td>
                                            <td>Mulia Circus Acrobatica Admission for Adults</td>
                                            <td><strike>IDR966,790</strike><br/><font style="color:red;">SOLD OUT</font></td>
                                        </tr>
                                        <tr>
                                            <td>Mulia Circus Acrobatica Admission for Kids (0-8 years old)</td>
                                            <td><strike>IDR422,290</strike><br/><font style="color:red;">SOLD OUT</font></td>
                                        </tr>
                                        <tr>
                                            <td>31th Dec 2018</td>
                                            <td>New Year Countdown feat Macy Kate Admission (13 years and above)</td>
                                            <td>IDR4,235,000</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger disabled" id="buyButton" datetime="Dec 31 2018 12:00:00 GMT+0800" target="_blank" href="https://airasiaredtix.cognitix.id/airasiaredtix/celebrate-new-year-mulia-style/booking" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Dress code: 30 Dec: Smart Casual (No T-Shirt, No Short Pants, No Singlet, and No Sandals), 31 Dec: Smart Evening Wear (No T-Shirt, No Short Pants, No Singlet, and No Sandals)</li>
                                    <li>Additional charge for food and beverage consumption will be applied</li>
                                    <li>Prices shown exclude IDR30,000 AirAsiaRedTix fee.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket sales will close 12am 27th December 2018, subject to availability</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection