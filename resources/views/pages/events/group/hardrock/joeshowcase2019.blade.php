@extends('master')
@section('title')
    The Best of Joe Exclusive Showcase
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="The Best of Joe Exclusive Showcase" />
    <meta property="og:description" content="Abdul Zamin bin Abdul Kadir atau lebih dikenali sebagai Joe Wings @ Jo Branko merupakan seorang pemain gitar utama tangan kiri kumpulan Scandals, Wings, penerbit album, pemuzik undangan, pencipta lagu, penulis lirik, pengarah muzik, juri, pelakon dan penyanyi di Malaysia."/>
    <meta property="og:image" content="{{ Request::Url().'images/hardrock/joeshowcase2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/hardrock/joeshowcase2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="The Best of Joe Exclusive Showcase"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/hardrock/joeshowcase2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="The Best of Joe Exclusive Showcase">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>The Best of Joe Exclusive Showcase</h6> Tickets from <span>SGD100</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 6th April 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Hard Rock Café Singapore <a target="_blank" href="https://goo.gl/maps/2ZXAXJusRKQ2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 10.30pm - 12.00am</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <p><h2>The Best of Joe Exclusive Showcase</h2><br/>
                                Abdul Zamin bin Abdul Kadir atau lebih dikenali sebagai Joe Wings @ Jo Branko merupakan seorang pemain gitar utama tangan kiri kumpulan Scandals, Wings, penerbit album, pemuzik undangan, pencipta lagu, penulis lirik, pengarah muzik, juri, pelakon dan penyanyi di Malaysia. Beliau pernah menyertai Scandals dan kemudiannya Kumpulan Wings. Setelah 28 tahun bersama Wings kini Joe menubuhkan kumpulan terbaharunya iaitu Prime Domestic.</p>
                                <p>Lagu-lagu Nyanyian<br/>
                                <ul>
                                <li>Perhentian (Wings & Superfriends)</li>
                                <li>Lupakanlah (Orang Asing)</li>
                                <li>Asmara (Biru Mata Hitamku)</li>
                                <li>Amelia (Filem Amelia)</li>
                                <li>Ranjang Mawar (Filem Amelia)</li>
                                <li>Orang Timur (Konsert G4)</li>
                                <li>Semalam Yang Hangat (An Evening with Joe wings (akustika) in Dublin</li>
                                <li>Ibu (Acoustic Sunday Nite Live in Singapore)</li>
                                </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/hardrock/joeshowcase2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/hardrock/joeshowcase2019/gallery-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                        <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>GA Standing</td>
                                            <td>SGD100</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Apr 7 2019 00:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/joeshowcase/booking" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown includes AirAsiaRedTix fee.</li>
                                    <li>Transaction fee SGD8.00 per event applicable for Internet purchase.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Adult: 18 years old and above.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	//$(document).ready(function(){
    //    $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

    <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Temporarily down for Maintenance</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>We are performing scheduled maintenance. We should be back online shortly</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection