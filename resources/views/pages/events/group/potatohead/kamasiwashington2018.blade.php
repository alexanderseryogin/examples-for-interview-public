@extends('master')
{{-- @extends('masterforpotatohead') --}}
@section('title')
    Vice x Potato Head: Kamasi Washington
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Vice x Potato Head: Kamasi Washington" />
    <meta property="og:description" content="As part of our biannual collaboration with cutting-edge media platform Vice—which, last April took the shape of a heady weekend banger with the illustrious DJ Harvey—we’ve teamed up once again to bring you an energetic live-act affair."/>
    <meta property="og:image" content="{{ Request::Url().'images/potatohead/singles/kamasiwashington/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/potatohead/singles/kamasiwashington/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Vice x Potato Head: Kamasi Washington"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/potatohead/singles/kamasiwashington/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Vice x Potato Head: Kamasi Washington">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Vice x Potato Head: Kamasi Washington</h6> Tickets from <span>RM77</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  25th August 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Potato Head Beach Club, Bali   <a target="_blank" href="https://goo.gl/maps/ViaZE8gHKT62">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Saturday, 25 August (4.00 pm - 2.00am)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>Vice x Potato Head: Kamasi Washington</h2><br/>
                            As part of our biannual collaboration with cutting-edge media platform Vice—which, last April took the shape of a heady weekend banger with the illustrious DJ Harvey—we’ve teamed up once again to bring you an energetic live-act affair. 
                            Heading to PHBC this August is one of the most exciting acts on tour this year: Kamasi Washington and his 12-piece band! In addition to his own musical support, Kamasi will be joined by a range of highly accomplished local talent, details of which to be announced soon. The show is guaranteed to be utterly jazztastic with a whole lot of heartfelt funk, so watch this space!</p>
                            <p><h2>Biography</h2><br />
                            LA-born musician Kamasi Washington is primarily known as a jazz saxophonist, but he’s so much more. He takes contemporary improvisational jazz to the next level by mixing trip-hop, funk, latin, hip-hop, R&B and classical into a big-band format complete with horns, strings, drums and a medley of other musical odds and ends. Kamasi has collaborated with John Legend, Run the Jewels, Ibeyi—but perhaps most notable is his work on Kendrick Lamar’s last two albums, “To Pimp a Butterfly” and “Damn,” infusing each with a jazzy element that contrasts perfectly with Lamar’s gruff tone. He recently rocked the crowd at the infamous desert festival Coachella, adding to a roster of captivating performances on the main stages of Glastonbury, Bonnaroo, Primavera and Fuji Rock. His two studio releases, “The Epic” and “Heaven and Earth” have earned him critical acclaim far beyond the world of jazz.</p>
                            <p><h2>Details</h2><br />
                            Kamasi Washington is at Potato Head Beach Club on Saturday, 25 August 2018 from 7pm-late. Pre-sale tix FDC 250K. </p>
                            {{-- <p>More info: <a href="http://www.sunnysideupfest.com" target="_blank">www.sunnysideupfest.com</a> --}}
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/potatohead/singles/kamasiwashington/gallery-dj-1.jpg" data-featherlight="image"><img class="" src="images/potatohead/singles/kamasiwashington/gallery-dj-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>PRE-SALE</td>
                                            <td>IDR 250,000 / RM 77.00</td>
                                            <td>includes one drink</td>
                                        </tr>
                                        <tr>
                                            <td>VIP Day Bed</td>
                                            <td><strike>IDR 2,000,000 / RM 610.00</strike><br />SOLD OUT</td>
                                            <td>includes 4 event tickets, IDR2 Million Minimum Spend</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Aug 25 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/vice x potato head- kamasi washington/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude RM8.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Indonesian Rupiah prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in IDR may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 1 days prior to event day, subject to availability.</li>
                                    {{-- <li>Adult: 18 years old and above.</li> --}}
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection