{{-- @extends('master') --}}
@extends('masterforpotatoheadnye')
@section('title')
    Sun Down Circle #027: Mr. Scruff
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Sun Down Circle #027: Mr. Scruff" />
    <meta property="og:description" content="Who better to set the tone for our first Sun Down Circle Session this year than master selector, DJ and producer Mr. Scruff?"/>
    <meta property="og:image" content="{{ Request::Url().'images/potatohead/singles/mrscruff2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/potatohead/singles/mrscruff2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Palm Trax"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/potatohead/singles/mrscruff2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Palm Trax">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Sun Down Circle #027: Mr. Scruff</h6> Tickets from <span>IDR150,000</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  8th Apr 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Potato Head Beach Club, Bali  <a target="_blank" href="https://goo.gl/maps/ViaZE8gHKT62">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Monday, 8 April (3.00 pm to 2.00am)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>Sun Down Circle #027: Mr. Scruff</h2></p>
                            <p>Who better to set the tone for our first Sun Down Circle Session this year than master selector, DJ and producer Mr. Scruff? Famed for his marathon sets, the English turntable legend will lead us into a balmy Bali night with a seven-hour musical journey packed full of surprises but sure to be an esoteric blend of house, disco and Afro and Latin sounds.</p>
                            <p>Scruff’s talent for keeping it fresh stems from his extensive history and obsession with music, which began as a child when a friend played the Street Sounds LP “Crucial Electro Volume 1.” After years of experimenting with myriad genres and dutifully learning the ropes of mixing and editing, Scruff—whose real name is Andy Carthy—dove in deep while studying fine art at university in the mid-nineties. His all-embracing repertoire (and cheeky take on it) is reflected in his genre-blurring discography, which includes “Trouser Jazz” (2002), the epic mix CD “Keep It Solid Steel” (2004), and “Ninja Tuna” (2008) for the renowned label Ninja Tune, which signed him in 1998, cemented his name on the scene and opened the doors for collaborations with the likes of Quantic, Danny Breaks, Alice Russell, Andreya Triana, Pete Simpson, Kaidi Tatham and Roots Manuva.</p>
                            <p>His upcoming set at the Beach Club marks a third return for Scruff, who helped kick off series in 2015 before coming back for a weekend edition with fellow crate-digger extraordinaire, Gilles Peterson. Join us for another phenomenal Sun Down Circle on Monday, 8 April when he’s joined by local favourites Dea Barandana and Tantra, who will collectively soundtrack the sky’s brilliant transformation through all the hues while the waves roll in behind them.</p>
                            <p>Sun Down Circle<br/>
                            Sun Down Circle showcases the best local talent alongside globally renowned artists in a relaxed pre-sunset atmosphere. From the very first evening, which featured Amsterdam-based Young Marco, countless acts have come through, including legendary Spanish DJ Jose Padilla—the man who helped pioneer the eclectically ambient style that Sun Down Circle is synonymous with—as well as the likes of Hot Chip, Crazy P, Mr. Scruff, Gilles Peterson, DJ Harvey, Horse Meat Disco, Ivan Smagghe, Ben UFO, Andras and King Britt and more.</p>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/potatohead/singles/mrscruff2019/gallery-dj-1.jpg" data-featherlight="image"><img class="" src="images/potatohead/singles/mrscruff2019/gallery-dj-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Pre-Sale</td>
                                            <td>IDR 150,000</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                {{-- <a class="btn btn-danger" id="buyButton" datetime="Apr 8 2019 12:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/sdc027-mrscruff/booking" role="button">BUY TICKETS</a> --}}
                                <a class="btn btn-danger" id="buyButton" datetime="Apr 8 2019 12:00:00 GMT+0800" target="_blank" href="https://redtix.cognitix.id/aart/sdc027-mrscruff/booking" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude IDR30,000 AirAsiaRedTix fee.</li>
                                    {{-- <li>Price shown exclude 3.5% Card Charges and IDR5,500 Internet Processing Fee</li> --}}
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close mid day prior to the event, subject to availability.</li>
                                    {{--<li>Adult: 18 years old and above.</li>--}}
                                    <li>Corporate credit cards are not accepted</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	//$(document).ready(function(){
    //    $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

    <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
						<p>All pre-sale tickets for NYE have sold out, but luckily we have reserved a limited number of tickets, which will be on sale at Potato Head Beach Club on 31 December from 10am</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection