@extends('master')
@section('title')
    Paramore - Live in Singapore
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Paramore - Live in Singapore" />
    <meta property="og:description" content="LAMC Productions is excited to announce that iconic Grammy-Award winning rock superstars, PARAMORE, will perform in Singapore, as part of their ‘After Laughter’ tour on Tuesday, 21 August, 2018 at Zepp @ BigBox Singapore!"/>
    <meta property="og:image" content="{{ Request::Url().'images/lamc/singles/paramore/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/lamc/singles/paramore/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Paramore - Live in Singapore"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/lamc/singles/paramore/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Paramore - Live in Singapore">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Paramore - Live in Singapore</h6> Tickets from <span>RM400</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  21st August 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Zepp@Bigbox, Level 3, Big Box 1 Venture Avenue, 608521 <a target="_blank" href="https://goo.gl/maps/NcUVPEJKmm32">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Tuesday, 21 August (8.00 pm - 10.00pm)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>Paramore - Live in Singapore</h2><br/>
                            <b>LAMC Productions</b> is excited to announce that iconic Grammy-Award winning rock superstars, <b>PARAMORE</b>, will perform in Singapore, as part of their ‘After Laughter’ tour on <b>Tuesday, 21 August, 2018 at Zepp @ BigBox Singapore!</b> 
                            <br /><br />Paramore recently released their fifth studio album, ‘AFTER LAUGHTER’, which is notably the  band’s first new album in more than four years and they are excited to return to Singapore!
                            <br /><br />“AFTER LAUGHTER” was recorded at Nashville’s historic RCA Studio B – Paramore’s first time recording in their own beloved hometown.This album also marks the return of original member Zac Farro back to the band.
                            <br /><br />Paramore has long established themselves as international rock superstars. Their previous self-titled album “PARAMORE” made a chart topping debut atop the SoundScan/Billboard 200 upon its April 2013 release marking Paramore’s first #1 album to date – and also scoring an array of #1 debuts around the globe. “PARAMORE” showcased a string of blockbuster singles including two 2x RIAA platinum-certified hits, the top 10 “Still Into You” and the #1 smash, “Ain’t It Fun.” The latter track proved Paramore’s biggest single to date, ascending to #1 at Hot AC and Rock radio outlets nationwide, #2 at Top 40, as well as to the top 10 on Billboard’s “Hot 100” – the band’s first ever top 10 hit on the overall chart and their highest peaking song thus far. Paramore has had a truly remarkable run of RIAA certified albums and singles since coming together in Nashville in 2004, from 2005’s gold-certified debut, “ALL WE KNOW IS FALLING” and 2007’s 2x platinum-certified breakthrough, “RIOT!” (with its 3x platinum-certified hit, “Misery Business,” as well as the platinum-certified singles “crushcrushcrush” and “That’s What You Get”) to 2008’s worldwide platinum “Decode” (from Chop Shop/Atlantic’s chart-topping “TWILIGHT – ORIGINAL MOTION PICTURE SOUNDTRACK”) and 2009’s platinum-certified “brand new eyes” and its GRAMMY®-nominated smash, “The Only Exception.”
                            </p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/06j-SED1DZA?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>

            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/lamc/singles/paramore/gallery-dj-1.jpg" data-featherlight="image"><img class="" src="images/lamc/singles/paramore/gallery-dj-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>GA Standing</td>
                                            <td>RM 450.00</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>GA Standing MY</td>
                                            <td>RM 400.00</td>
                                            <td>For Malaysian MyKad Holder</td>
                                        </tr>
                                        <tr>
                                            <td>GA Standing MY2<br/>
                                            (includes 2 Tickets)</td>
                                            <td>RM 650.00</td>
                                            <td>For Malaysian MyKad Holder (2 Tickets)</td>
                                        </tr>
                                        {{-- <tr>
                                            <td>RedTix Package A<br />(2 Tickets + 1 Night Stay)</td>
                                            <td>RM 1150.00</td>
                                            <td>YOTEL Singapore (4 Star)</td>
                                        </tr>
                                        <tr>
                                            <td>RedTix Package B<br />(2 Tickets + 1 Night Stay)</td>
                                            <td>RM 1250.00</td>
                                            <td>Orchard Hotel Singapore (5 Star)</td>
                                        </tr>
                                        <tr>
                                            <td>RedTix Package C<br />(2 Tickets + 1 Night Stay)</td>
                                            <td>RM 750.00</td>
                                            <td>Tune Hotel Danga Bay, Johor (3 Star)</td>
                                        </tr> --}}
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Aug 19 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/paramore - live in singapore/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <span class="importantNote">1. Special Limited offer at AirAsia RedTix only</span>
                            <span class="importantNote">2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</span>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown exclude RM8.00 AirAsiaRedTix fee and 3% Credit card fee.</li> 
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Indonesian Rupiah prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in IDR may differ due to currency fluctuations.</li> --}}
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    {{-- <li>Online ticket selling will close 1 days prior to event day, subject to availability.</li>
                                    <li>Adult: 18 years old and above.</li> --}}
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });


    // Modal Announcement
	$(document).ready(function(){
      $("#announcementModal").modal('show');
  	});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. Special Limited offer at AirAsia RedTix only</p>
                        <p>2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection