@extends('master')
@section('title')
    Chance The Rapper - Live in Singapore
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Chance The Rapper - Live in Singapore" />
    <meta property="og:description" content="LAMC Productions is excited to announce that Grammy-Award winning Chicago Rap prodigy, CHANCE THE RAPPER, will perform in Singapore for the FIRST time on Saturday, 25 August, 2018 at ZEPP@BIGBOX Singapore!"/>
    <meta property="og:image" content="{{ Request::Url().'images/lamc/singles/chancetherapper/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/lamc/singles/chancetherapper/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Chance The Rapper - Live in Singapore"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/lamc/singles/chancetherapper/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Chance The Rapper - Live in Singapore">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Chance The Rapper - Live in Singapore</h6> Tickets from <span>RM350</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  25th August 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Zepp@Bigbox, Level 3, Big Box 1 Venture Avenue, 608521 <a target="_blank" href="https://goo.gl/maps/NcUVPEJKmm32">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Saturday, 25 August (8.00 pm - 10.00pm)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p><h2>Chance The Rapper - Live in Singapore</h2><br/>
                            <b>LAMC Productions</b> is excited to announce that Grammy-Award winning Chicago Rap prodigy, <b>CHANCE THE RAPPER</b>, will perform in Singapore for the FIRST time on <b>Saturday, 25 August, 2018 at ZEPP@BIGBOX Singapore!</b>
                            <br /><br />Chance the Rapper is one of the biggest breakout stars of the past few years, achieving fame and success in an unorthodox way — by avoiding the major labels that want to sign him. His 2016 mixtape, Coloring Book earned him three Grammy Awards, including Best Rap Album. Coloring Book became the first streaming-only album to receive a nomination for, and win, a Grammy.
                            <br />Fiercely independent and articulate, he’s found a way to please both the backpacker fans and a much wider audience through savvy collaborations with the likes of Lil Wayne and Kanye West. He’s also politically active, and a recent act of philanthropy, which saw him donate $1 million to Chicago public schools, even had fellow Chicagoan Michelle Obama tweeting her praise for him: “Thanks for giving back to the Chicago community, which gave us so much. You are an example of the power of arts education.”  Born Chancelor Johnathan Bennett, he begin recording his first solo mixtape, 10 Day, in his senior year at Jones College Prep High School. A 14-track project that included work with a future frequent collaborator Vic Mensa, and production from Flying Lotus, it was released as a free download on mixtape website DatPiff. Chance had already come to the attention of Complex magazine, which featured him in its "10 new Chicago Rappers to watch out for" feature in February 2012, noting that “Chance has one of the more distinct flows and voices of any new artist in the game.”
                            <br /><br />A hook-up with Childish Gambino for the track "They Don’t Like Me" on his Royalty mixtape in 2012 opened up another door. Gambino – aka the actor Donald Glover – invited Chance to open for him on his next tour. Chance was already recording his second mixtape, Acid Rap, the release that would really push him to the next level. Released again as a free download on April 30, 2013, it featured tracks with Twista and Gambino, and almost drowned in critical praise. Spin claimed that “Chance is a wide-eyed talent figuring out the world one deeply felt rap song at a time,” while the Chicago Tribune saw Acid Rap as “a springboard for his versatility as an MC, thinker, improviser and surrealist. Little wonder he’s being pursued by a gaggle of record-company suitors.”
                            <br /><br />Coloring Book, the third free mixtape from Chance, dropped in May 2016. Again, an eclectic range of guests accompanied him — among them Kanye West, Lil Wayne, 2 Chainz, T-Pain and Justin Bieber. The single "No Problem" was an instant hit. The album became the first ever to chart on the Billboard 200 just from streaming, receiving 57.3 million in its first week alone. Entertainment Weekly thought it had sequencing issues, but raved about the creativity on display, concluding that Coloring Book "affirms Chance’s place as one of hip-hop’s most promising — and most uplifting — young stars.” Blending soul, gospel, hip hop and the music of Social Experiment, it was a favorite in best-of-year polls and with other artists. Mac Miller tweeted that it was “very very very very good. I like music that makes me feel things might just be alright.” The industry agreed, with Chance winning three Grammy awards in 2017. 
                            <br /><br />Don’t miss your opportunity to catch CHANCE THE RAPPER in Singapore for the first time!
                            </p>
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/lamc/singles/chancetherapper/gallery-dj-1.jpg" data-featherlight="image"><img class="" src="images/lamc/singles/chancetherapper/gallery-dj-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>GA Standing</td>
                                            <td>RM 400.00</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>GA Standing MY</td>
                                            <td>RM 350.00</td>
                                            <td>For Malaysian MyKad Holder</td>
                                        </tr>
                                        <tr>
                                            <td>GA Standing MY2<br/>
                                            (includes 2 Tickets)</td>
                                            <td>RM 550.00</td>
                                            <td>For Malaysian MyKad Holder (2 Tickets)</td>
                                        </tr>
                                        {{--<tr>
                                            <td>RedTix Package A<br />(2 Tickets + 1 Night Stay)</td>
                                            <td>RM 1050.00</td>
                                            <td>YOTEL Singapore (4 Star)</td>
                                        </tr>
                                        <tr>
                                            <td>RedTix Package B<br />(2 Tickets + 1 Night Stay)</td>
                                            <td>RM 650.00</td>
                                            <td>Orchard Hotel Singapore (5 Star)</td>
                                        </tr> --}}
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Aug 22 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/chance the rapper - live in singapore/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <span class="importantNote">1. Special Limited offer at AirAsia RedTix only</span>
                            <span class="importantNote">2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</span>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{--<li>Prices shown exclude RM8.00 AirAsiaRedTix fee and 3% Credit card fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Indonesian Rupiah prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in IDR may differ due to currency fluctuations.</li> --}}
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    {{-- <li>Online ticket selling will close 1 days prior to event day, subject to availability.</li>
                                    <li>Adult: 18 years old and above.</li> --}}
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	$(document).ready(function(){
      $("#announcementModal").modal('show');
  	});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. Special Limited offer at AirAsia RedTix only</p>
                        <p>2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection