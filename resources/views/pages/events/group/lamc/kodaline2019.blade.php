@extends('master')
@section('title')
    Kodaline - Live in Singapore
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Kodaline - Live in Singapore" />
    <meta property="og:description" content="LAMC Productions is excited to announce that Ireland-based, UK Platinum-selling, modern rock quartet, KODALINE, will make their highly anticipated return to Singapore, on 5 March, 2019"/>
    <meta property="og:image" content="{{ Request::Url().'images/lamc/singles/kodaline2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/lamc/singles/kodaline2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Kodaline - Live in Singapore"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/lamc/singles/kodaline2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Kodaline - Live in Singapore">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Kodaline - Live in Singapore</h6> Tickets from <span>RM350</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 5th March 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Zepp@Bigbox, Level 3, Big Box 1 Venture Avenue, 608521 <a target="_blank" href="https://goo.gl/maps/NcUVPEJKmm32">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Tuesday, 5 March (8.00 pm - 10.00pm)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-8">
                                <p><h2>Kodaline - Live in Singapore</h2><br/>
                                LAMC Productions is excited to announce that  Ireland-based, UK Platinum-selling, modern rock quartet, KODALINE, will make their highly anticipated return to Singapore, on 5 March, 2019, at ZEPP@BIGBOX.</p>
                                <p><b>ARTIST PROFILE</b><br/>
                                Kodaline released their brand new album, Politics of Living, on 28 Sep 2018, to rave reviews and are ready to embark their upcoming world tour!</p>
                                <p><b>ABOUT ARTIST</b><br/>
                                Dublin based rockers, Kodaline, specialize in soaring, radio-ready guitar rock that's drawn comparisons to Coldplay, Keane, U2, and Oasis. Kodaline got their start when childhood friends Steve Garrigan and Mark Prendergast brought their guitars to a summer camp in Dublin, spending the days penning songs together. Originally performing under the name 21 Demands, the duo added Jason Boland (bass guitar) and Vinny May (drums) to the lineup, forming Kodaline in 2011.</p>
                                <p>At the end of 2012, the band was shortlisted for the BBC's Sound of 2013, but was beaten to the top spot by HAIM. Kodaline released their debut album, In a Perfect World, in June 2013, which was preceded by the singles High Hopes and Love Like This. The album was a major success in Ireland, going platinum twice and has sold over a million copies worldwide, imprinting itself on the nation’s consciousness with streaming figures well into the 100s of millions. On Vevo, the band’s videos have had over 200 million views, and they have sold over a million singles.</p>
                                <p>Kodaline's second album, Coming Up for Air, followed in early 2015. The album peaked at number four on the U.K. charts and spawned the singles "The One" and "Honest." Since then, they've played massive sold-out shows and a number of festivals worldwide, earning a cult following in their short time together.</p>
                                <p>Their music has also been featured in several television, film and ad placements including Chicago PD (NBC), Grey’s Anatomy (ABC), Modern Family (ABC), So You Think You Can Dance (FOX) American Idol (ABC) and so much more. Notably, "All I Want" was released as part of the soundtrack for The Fault in Our Stars, as well as used in the film.</p>
                                <p>On Politics of Living, Kodaline teamed up with some of the hottest production and writing talent imaginable including hitmaker supreme Steve Mac (Ed Sheeran, Liam Payne), pop guru Johnny Coffer (Rag’N’Bone Man, Beyoncé), Jonas Jeberg (Dizzee Rascal, Kylie Minogue), Stephen Harris (Miles Kane, Kaiser Chiefs) as well as long-time collaborator Johnny McDaid (P!NK). The album is a delightful treat and surrounds big choruses with digital rhythms, glimmering piano riffs, and synths.</p>
                                <p>Kodaline are – singer Steve Garrigan, guitarist Mark Prendergast, bassist Jason Boland and drummer Vinny May.</p>
                                <p><b>Reviews from Kodaline’s debut gig in Singapore in 2015:</b><br/>
                                “Kodaline’s display was an absolute powerhouse of a performance by any standard. Pro-life tip; don’t pass up an opportunity to see these guys live.”<br/>
                                - More Than Good Hooks<br/><br/>
                                “Kodaline took Singapore by storm for the first time ever!”<br/>
                                - Spin Or Bin Music</p>
                            </div>
                            <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/artist/4BxCuXFJrSWGi1KHcVqaU4" width="300" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div>
                        </div>
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/qWT_W3MDFmk?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/lamc/singles/kodaline2019/gallery-dj-1.jpg" data-featherlight="image"><img class="" src="images/lamc/singles/kodaline2019/gallery-dj-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>GA Standing</td>
                                            <td>RM 350.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Mar 2 2019 00:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/kodaline2019/booking" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });


    // Modal Announcement
	//$(document).ready(function(){
    //  $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. Special Limited offer at AirAsia RedTix only</p>
                        <p>2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection