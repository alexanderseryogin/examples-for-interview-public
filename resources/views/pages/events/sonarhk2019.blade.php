@extends('master')
@section('title')
    Sonar Hong Kong
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Sonar Hong Kong" />
    <meta property="og:description" content="Since its launch in Barcelona in 1994, Sónar festival has established itself at the cutting edge of the global electronic music landscape and gained respect among fans and critics worldwide for its eclectic lineups, high-quality production and unwavering support for electronic artists both new and established."/>
    <meta property="og:image" content="{{ Request::Url().'images/sonarhk2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/sonarhk2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Sonar Hong Kong"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/sonarhk2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Sonar Hong Kong">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Sonar Hong Kong</h6> Tickets from <span>USD90</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  13th April 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Hong Kong Science Park, Pak Shek Kok, Hong Kong <a target="_blank" href="https://goo.gl/maps/UEwzZi5Y77S2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 12.00pm - 3.00am</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <p><h2>Sonar Hong Kong</h2><br/>
                                Since its launch in Barcelona in 1994, Sónar festival has established itself at the cutting edge of the global electronic music landscape and gained respect among fans and critics worldwide for its eclectic lineups, high-quality production and unwavering support for electronic artists both new and established. Beyond the music, Sónar plays a vital role in promoting creative technology via the Sónar+D programme, providing a unique platform for exchanging ideas and exploring the spaces where creativity and technology meet in a fun and inspirational environment.</p>
                                <p>This year, Sónar Hong Kong makes its eagerly anticipated return to Hong Kong Science Park from 12pm-3am on Saturday April 13, 2019. In total, the 15-hour festival will see more than 35 dazzlingly diverse electronic acts performing across five indoor and outdoor stages at one of the city’s most unique venues, the futuristic Hong Kong Science Park, which is set on the picturesque Tolo Harbour waterfront in the New Territories.</p>
                            </div>
                            {{-- <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/artist/6deZN1bslXzeGvOLaLMOIF" width="300" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div> --}}
                        </div>
                        {{-- <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/PhrY3yep1OQ?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/sonarhk2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/sonarhk2019/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sonarhk2019/gallery-2.jpg" data-featherlight="image"><img class="" src="images/sonarhk2019/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sonarhk2019/gallery-3.jpg" data-featherlight="image"><img class="" src="images/sonarhk2019/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sonarhk2019/gallery-4.jpg" data-featherlight="image"><img class="" src="images/sonarhk2019/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sonarhk2019/gallery-5.jpg" data-featherlight="image"><img class="" src="images/sonarhk2019/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sonarhk2019/gallery-6.jpg" data-featherlight="image"><img class="" src="images/sonarhk2019/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sonarhk2019/gallery-7.jpg" data-featherlight="image"><img class="" src="images/sonarhk2019/gallery-7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sonarhk2019/gallery-8.jpg" data-featherlight="image"><img class="" src="images/sonarhk2019/gallery-8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sonarhk2019/gallery-9.jpg" data-featherlight="image"><img class="" src="images/sonarhk2019/gallery-9.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>SonarPass</td>
                                            <td>USD 90.00</td>
                                            <td>SonarPass HKD680</td>
                                        </tr>
                                        <tr>
                                            <td>SonarPass + Late Night Pass</td>
                                            <td>USD 116.00</td>
                                            <td>SonarPass + Late Night Pass HKD880</td>
                                        </tr>
                                        <tr>
                                            <td>4 SonarPasses</td>
                                            <td>USD 329.00</td>
                                            <td>Group of 4 HK$2,488 for 4 SonarPasses</td>
                                        </tr>
                                        <tr>
                                            <td>4 SonarPasses + Late Night Passes</td>
                                            <td>USD 434.00</td>
                                            <td>Group of 4 HK$3,288 for 4 SonarPasses + Late Night Passes</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Apr 12 2019 00:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/sonarhk2019/booking" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                <h2>Notes</h2>
                                <p>SonarPass + Late Night Pass provides entrance to Sónar Hong Kong and access to all activities from 12pm until 03:00am. Access to all activities is subject to capacity limitations. Workshops may require separate registration and payment.</p>
                                <p>SonarPass provides entrance to Sónar Hong Kong and access to all outdoor activities from 12pm until 10:45pm, and all indoor activities from 12pm until 10:15pm. Access to all activities is subject to capacity limitations. Workshops may require separate registration and payment.</p>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Transaction fee of USD2.00 per event ticket applicable for Internet purchase.</li>
                                    <li>All ticket prices will be paid in US Dollar. HKD prices are shown for reference purposes only.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });


    // Modal Announcement
	//$(document).ready(function(){
    //  $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. Special Limited offer at AirAsia RedTix only</p>
                        <p>2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection