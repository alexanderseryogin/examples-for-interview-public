@extends('master')
@section('title')
    Azharina Live at Hard Rock Café, Kuala Lumpur
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/azharina/azharina-banner.jpg')">
        {{-- <video id="video-background" preload muted autoplay loop>
          <source src="images/neon/top-banner2000x450.mp4" type="video/mp4">
        </video> --}}
      </div>
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>Azharina Live at Hard Rock Café, Kuala Lumpur</h6>
                  Tickets from <span>RM84</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 26 March 2017 (Sunday)</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Hard Rock Café, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/9ZLpaezMUrQ2">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 2:30 pm</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <h2>Presented By Azharina Entertainment & Melody United Music</h2>
                <p>Azharina binti Azhar atau lebih dikenali sebagai Azharina Azhar, Azharina, Nina merupakan seorang penyanyi pop dari Malaysia yang baru-baru ini telah menjadi Juara Gegar Vaganza musim ke 3, sebuah program realiti terbitan Astro yang menyaksikan 12 penyanyi professional lama atau berpengalaman dalam industri muzik di Malaysia bersaing dalam satu pertandingan nyanyian. </p>

                <p>Azharina binti Azhar dilahirkan pada 11 Mac 1985 di Kota Kinabalu, Sabah. Azharina binti Azhar merupakan anak sulung dari empat adik beradik. Azharina binti Azhar merupakan juara dalam pertandingan nyanyian Bintang RTM 2001 menewaskan peserta hebat seperti Siti Sarah , Misha Omar , dan Asmidar. </p>

                <p>Azharina merakamkan beberapa album di bawah syarikat Millenium Art Sdn. Bhd. sebelum keluar dari syarikat tersebut. Azharina berkerjasama dengan Min Yasmin bagi rakaman album berdua. Album 2 SOUL ini diterbitkan Julfekar Ahmad Shah.</p>

                <p>Azharina pernah memenangi tempat ke-2 dalam program nyanyian suara Hits1 RTM 1 tewas kepada Jacklyn Victor lagu Gemilang pada tahun 2006 mempertaruhkan lagu Elegi Sepi... Lagu Elegi Sepi merupakan lagu paling lama mengekalkan dalam carta Hits1 RTM 1 selama lebih 30 minggu bertahan dalam carta. </p>

                <p>Ini menunjukkan lagu tersebut merupakan lagu berhantu dan hits di Malaysia.. Lagu ini juga dinobatkan lagu top meletop pilihan rakyat Malaysia menewaskan artist ternama seperti Datuk Siti Nurhaliza dalam satu anugerah bernama anugerah Era FM pada tahun 2005. </p>
                <p>Album:</p>
                <ol>
                    <li>Azharina (2002)</li>
                    <li>Seandainya (2003)</li>
                    <li>Nina (2004)</li>
                </ol>
                <p>Lagu-Lagu Hit</p>
                <ol>
                    <li>Seandainya</li>
                    <li>Elergi Sepi</li>
                    <li>Rawan</li>
                </ol>
                <p>Lagu- Lagu Azharina</p>
                <ol>
                    <li>Seandainya</li>
                    <li>Masa Dulu</li>
                    <li>Di Hari Persandingan mu</li>
                    <li>Ketika Mentari Beradu</li>
                    <li>Malam ini</li>
                    <li>Teman yang ku kasih</li>
                    <li>Kembali pada-Nya</li>
                    <li>Damba</li>
                    <li>Mengenai Cinta</li>
                    <li>Bukan membenci</li>
                </ol>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/azharina/poster.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/azharina/azharina-banner.jpg" alt="">
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Normal Price <br>(include ONE soft drink)</th>
                          <th>Remarks</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td><span class="box yellow">VVIP</span></td>
                              <td>RM 304.00</td>
                              <td>Meet & Greet Buffet Package</td>
                              <td rowspan="11"><img class="img-responsive seatPlanImg" src="images/azharina/seatplan.jpg" alt=""></td>
                          </tr>
                          <tr>
                              <td><span class="box green">Platinum</span></td>
                              <td>RM 204.00</td>
                              <td rowspan="3">Seated</td>
                          </tr>
                          <tr>
                              <td><span class="box orange">Gold</span></td>
                              <td>RM 154.00</td>
                          </tr>
                          <tr>
                              <td><span class="box red">Silver</span></td>
                              <td>RM 104.00</td>
                          </tr>
                          <tr>
                              <td><span class="box purple">Bronze</span></td>
                              <td>RM 84.00</td>
                              <td>Free Standing</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/events/azharina%20-%20live%20at%20hard%20rock%20caf%C3%A9%20kuala%20lumpur/2017-3-26_14.30/hard%20rock%20caf%C3%A9%20kuala%20lumpur?hallmap" role="button">BUY TICKETS</a>
                  <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>
                <div class="note text-left">
                    <h2>Note</h2>
                    <ol>
                        <li>Prices shown include RM4.00 AirAsiaRedTix fee</li>
                        <li>Cover Charge Price includes One Soft Drink</li>
                        <li>The organiser reserves the right to change the flow of the agenda and times of performances as they may be additional guest artist invited to perform. The showcase is from 2.30pm-7.00pm</li>
                    </ol>
                    <h2>Reminder</h2>
                    <p><strong>Dress code</strong><br>Neat and decent. For Males shorts, slippers, and sandals are STRICTLY NOT ALLOWED.</p>
                    <p><strong>Minimum age</strong><br>6 years & above</p>
                    <p><strong>Outside F&B</strong><br>Outside Food and drinks are not allowed.</p>
                    <span class="importantNote">Each ticket is valid for one-time admission only.</span>
                    <span class="importantNote">Child must be of 95cm height and below to be eligible for child ticket priced at RM50 without seat, while child at height above 95cm must purchase tickets according to price scale.</span>
                    <h2>Important Notes</h2>
                    <ol>
                      <li>Prices shown include RM4.00 AirAsiaRedTix fee & 6% GST.</li>
                      <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                      <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                      <li>Strictly no replacement for missing tickets and cancellation.</li>
                      <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                    </ol>
                    <h2>For enquiry only:</h2>
                    <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        {{-- <section class="pageCategory-section last">
          <div class="buyNowSec" style="background-image: url('images/event2.jpg')">
            <div class="bannerOverlay"></div>
            <div class="container">
              <div class="col-sm-offset-1 col-sm-10 quoteArea">
                <h1>You’ve heard him on the radio, you’ve seen him on TV. It’s about time. See him live!</h1>
              </div>
              <div class="btnArea">
                <a class="btn btn-black btn-lg" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/konsert%20gegar%20pop%20yeh%20yeh/events" role="button">GET TICKET ONLINE NOW <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <div class="clearfix">&nbsp;</div>
                <span data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets</span>
              </div>
            </div>
          </div>
        </section> --}}

        {{-- <section class="pageCategory-section last">
          <div class="container">
            <div class="partner text-center">
                <h1 class="subSecTitle"><strong>PARTNERS</strong></h1>
                <div class="clearfix">&nbsp;</div>
                <ul class="list-unstyled list-inline partnerList">
                    <li><img src="images/neon/aabig_logo (offical partner).png" alt=""></li>
                    <li><img src="images/neon/BAC.png" alt=""></li>
                    <li><img src="images/neon/durex (official partner).png" alt=""></li>
                    <li><img src="images/neon/fac3 (co-organiser).jpg" alt=""></li>
                    <li><img src="images/neon/seven friday (official watch).jpeg" alt=""></li>
                    <li><img src="images/neon/sunway lagoon (official venue).png" alt=""></li>
                </ul>
            </div>
          </div>
        </section> --}}

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                <dd>The Curve (TEL: 03 - 7733 1139)</dd>
                <dd>Bangsar Village (TEL: 03-22021139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
                <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Istana Budaya Jalan<br>Tun Razak:</dt>
                <dd>03 40265520</dd>
            </dl>
            </div>
        </div>
        </div>
    </div>

@endsection