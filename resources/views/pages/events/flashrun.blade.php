@extends('master')
@section('title')
    FLASH RUN 2017
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/flashrun/flashrun-banner.jpg')">
        {{-- <video id="video-background" preload muted autoplay loop>
          <source src="images/neon/top-banner2000x450.mp4" type="video/mp4">
        </video> --}}
      </div>
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>FLASH RUN 2017</h6>
                  Tickets from <span>RM141</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 23 September 2017 <a class="label label-warning" data-toggle="modal" data-target="#announcementModal">Read Announcement</a></div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> The Oval Lawn, Setia City Mall <a target="_blank" href="https://www.google.com/maps/place/Setia+City+Mall/@3.1095822,101.4603601,17z/data=!4m8!1m2!2m1!1sThe+Oval+Lawn,+Setia+City+Mall!3m4!1s0x31cc53dc4ac4b2eb:0x3673e8fe029af867!8m2!3d3.1097273!4d101.4595181">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 6.00 PM</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <p>NOVEMBER 2016. Music, water and neon lights. These are the elements needed to create an innovative fun run. And thus Flash Run is born; bringing to live the elements of music, water and neon lights, this innovative run is coming to you in Malaysia and will covers a course of 5km within the picturesque landscape of Setia Alam.</p>
                <p>Flash Run is organized by the Eagle In D Group in collaboration with the Malaysia Major Events (MME). This fun run event is highly anticipated to be a bigger, better and bolder water and neon lights element night run with a hint of music. Flash Run is set to hit the country on the 23 September 2017 happening at The Oval Lawn in Setia Alam, Shah Alam. We are expecting this one-night-only Flash Run to attract over 15,000 runners of all ages. The runners will be dashing to the finishing line in either Team Sniper or Team Bravo. There will be two packages offered in this run namely the Sniper pack and the Bravo pack. All runners will receive a standard race kit with T-shirt, 3 LED rings, entry wristband and goodie bags. Sniper pack runner, however, will be presented with an extra medium size water gun to battle through the course to the finishing line.</p>
                <p>For its debut in Kuala Lumpur, Flash Run will be dazzled with a never before seen course lands which will highlight Malaysia in all its neon glory. Be mesmerized and enthralled by the course lands decorated in different thematic themes. As participants run, skip or dance through the 5km route, their senses will be stimulated with the various unique activations awaiting them at separate stops. We bet it is going to be a night you will never forget.</p><br>
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/5V7H4pfmq-E?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/flashrun/FlashRun2017.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/flashrun/post5.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/flashrun/FlashRun2017_event.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/flashrun/flashrun-homepage-banner.jpg" alt="">
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Early Bird Price</th>
                          <th>Normal Price</th>
                          <th>Group Registration (4 tickets)<br>*While ticket last</th>
                          <th>Remarks</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>Sniper</td>
                              <td>RM 141.00</td>
                              <td>RM 161.00</td>
                              <td>RM 488.00</td>
                              <td>With Gun</td>
                          </tr>
                          <tr>
                              <td>Bravo</td>
                              <td>RM 121.00</td>
                              <td>RM 141.00</td>
                              <td>RM 418.00</td>
                              <td>Without Gun</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger" id="buyButton" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/flash%20run%202017/events" role="button">BUY TICKETS</a>                  
                </div>
                <span class="importantNote">* Prices shown include RM3.00 AirAsiaRedTix fee.</span>
                <span class="importantNote">* Maximum of 8 tickets in total per transaction. Please perform additional transaction if more than 8 tickets require. Total tickets of 8 included Group of 4 Package and Individual tickets.</span>
                <div class="note text-left">
                  <h2>NOTE</h2>
                  <ul>
                    <li>Once tickets are sold, strictly no ticket transfers, exchanges or cancellations.</li>
                    <li>Entry is for all ages.</li>
                    <li>UNDER 18 YEARS: Children and Participants 12 years old and above will have to register and pay FULL registration fee. Children and participants under 18 years of age must be accompanied by a successfully registered parent, adult legal guardian or an eligible adult care person AT ALL TIMES.</li>
                    <li>UNDER 12 YEARS: Entry for Children and Participants under 12 years old is FREE (NO RACE KIT will be provided). ONE (1) Parent is allowed to bring ONLY ONE (1) Child/participant under 12 years old. LIMITED FREE registrations available on a FIRST COME FIRST SERVE BASIS.</li>
                  </ul>
                  <h2>T-shirt Sizes</h2>
                    <table class="table infoTable-D table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Size</th>
                            <th>Shoulder</th>
                            <th>Chest</th>
                            <th>Short Sleeve Length</th>
                            <th>Body</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>S</td>
                                <td>17"</td>
                                <td>38"</td>
                                <td>8"</td>
                                <td>27"</td>
                            </tr>
                            <tr>
                                <td>M</td>
                                <td>18"</td>
                                <td>40"</td>
                                <td>8.5"</td>
                                <td>28"</td>
                            </tr>
                            <tr>
                                <td>L</td>
                                <td>19"</td>                            
                                <td>42"</td>
                                <td>9"</td>
                                <td>29"</td>
                                
                                
                            </tr>
                            <tr>
                                <td>XL</td>
                                <td>20"</td>
                                <td>44"</td>
                                <td>9.5"</td>
                                <td>30"</td>
                                
                            </tr>
                            <tr>
                                <td>XXL</td>                            
                                <td>21"</td>
                                <td>46"</td>
                                <td>10"</td>
                                <td>31"</td>
                            </tr>
                        </tbody>
                    </table>
                  <h2>Important Notes</h2>
                  <ol>
                    <li>Prices shown inclusive RM3.00 AirAsiaRedTix fee & 6% GST.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                  </ol>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.<br />Terms and Conditions link for the race <a target="_blank" href="http://eagleind.asia/Flash_TnC.pdf">http://eagleind.asia/Flash_TnC.pdf</a></p>
                  <p></p>
                </div>
              </div>
            </div>
          </div>
        </section>

        {{-- <section class="pageCategory-section last">
          <div class="buyNowSec" style="background-image: url('images/event2.jpg')">
            <div class="bannerOverlay"></div>
            <div class="container">
              <div class="col-sm-offset-1 col-sm-10 quoteArea">
                <h1>You’ve heard him on the radio, you’ve seen him on TV. It’s about time. See him live!</h1>
              </div>
              <div class="btnArea">
                <a class="btn btn-black btn-lg" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/flash%20run%202017/events" role="button">GET TICKET ONLINE NOW <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <div class="clearfix">&nbsp;</div>
                <span data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets</span>
              </div>
            </div>
          </div>
        </section> --}}

        {{-- <section class="pageCategory-section last">
          <div class="container">
            <div class="partner text-center">
                <h1 class="subSecTitle"><strong>PARTNERS</strong></h1>
                <div class="clearfix">&nbsp;</div>
                <ul class="list-unstyled list-inline partnerList">
                    <li><img src="images/neon/aabig_logo (offical partner).png" alt=""></li>
                    <li><img src="images/neon/BAC.png" alt=""></li>
                    <li><img src="images/neon/durex (official partner).png" alt=""></li>
                    <li><img src="images/neon/fac3 (co-organiser).jpg" alt=""></li>
                    <li><img src="images/neon/seven friday (official watch).jpeg" alt=""></li>
                    <li><img src="images/neon/sunway lagoon (official venue).png" alt=""></li>
                </ul>
            </div>
          </div>
        </section> --}}

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('modal')

  <!--Modal Announcement-->
  <div id="announcementModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
            <h4>Announcement</h4>
            <div class="clearfix">&nbsp;</div>
            <div class="well">
            <p>Please be informed that Flash 2017 had been postpone from <strong>8 July 2017</strong> to <strong class="text-danger">23 September 2017</strong>. Venue and date of event remained the same. <br>Please take note</p></div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <!--Modal Seat Plan image-->
  <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <div class="clearfix">&nbsp;</div>
          <img src="" class="enlargeImageModalSource" style="width: 100%;">
          </div>
      </div>
      </div>
  </div>

  <!-- Modal Where to Get Tix Location-->
  <div id="modalGetTixLoc" class="modal fade" role="dialog">
      <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <h2>GET YOUR TICKETS FROM:</h2>
          <dl class="dl-horizontal">
              <dt>Online:</dt>
              <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Rock Corner outlets:</dt>
              <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
              <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
              <dd>Bangsar Village (TEL: 03-22021139)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Victoria Music outlets:</dt>
              <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
              <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Penang outlets:</dt>
              <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
              <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Istana Budaya Jalan<br>Tun Razak:</dt>
              <dd>03 40265558</dd>
          </dl>
          </div>
      </div>
      </div>
  </div>

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
    $(document).ready(function(){
      $("#announcementModal").modal('show');
    });
    </script>
    
@endsection