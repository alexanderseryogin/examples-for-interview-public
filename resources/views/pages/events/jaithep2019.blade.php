@extends('master')
@section('title')
    Jai Thep Festival
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Jai Thep Festival" />
    <meta property="og:description" content="Jai Thep, Northern Thailand’s leading arts and music celebration, returns for its fourth year, continuing the extended 3 days of festivities with camping!"/>
    <meta property="og:image" content="{{ Request::Url().'images/jaithep2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/jaithep2019/web-banner1.jpg')}}" style="width: 100%" class="img-responsive" alt="Jai Thep Festival"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/jaithep2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Jai Thep Festival">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Jai Thep Festival</h6> Tickets from <span>RM123</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  1st - 3rd Feb 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Lanna Rock Garden, Ban Pong, Hang Dong District, Chiang Mai 50230 <a target="_blank" href="https://goo.gl/maps/EVbqSHUaBhC2">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Friday - Saturday, 12.00 pm - 3.00am</div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Sunday, 12.00 pm - 12.00am</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <div class="col-sm-12">
                                <hr>
                                <p><h2>Jai Thep Festival</h2><br/>
                                FRIDAY - SUNDAY, 1st - 3rd FEBURUARY 2019</p>
                                <p>Jai Thep, Northern Thailand’s leading arts and music celebration, returns for its fourth year, continuing the extended 3 days of festivities with camping! Get ready to experience musical ecstasy at one of our four stages or get entranced by the many large-scale art installations.</p>
                                <p>Jai Thep takes pride in being a multi-cultural, international event, with followers returning from all corners of the world, to enjoy resonating beats deep in the Lanna countryside.</p>
                                <p>What makes Jai Thep a truly awesome experience is the variety of stages, art installations, and magical, immersive environments, workshops and classes to explore. Curious souls who wander off the festival path may encounter many weird and wondrous realms sprinkled throughout the property, each with its’ own unique offerings, music and art.</p>
                                <p>Ever experienced Laughter Yoga?<br/>
                                Curious to learn about energy healing?<br/>
                                Interested in refining your hula-hooping skills?<br/>
                                Jai Thep is as much about its workshops as it is about its performances, and this year our lineup for activities is more diverse than ever! And don’t forget to check out the Kids Zone for a list of tailored activities for our young friends!</p>
                                <p>Children and families are very welcome at Jai Thep! We have designated child-friendly spaces where adults get the chance to be childlike again, and where kids can create and play. Planned and manned by a community of teachers, the kids activities at Jai Thep are a fundamental part of the festival.</p>
                                <p>Children aged 12 and under are welcome at Jai Thep with a paying adult, and those 13 and older can take advantage of our low-cost student tickets!</p>
                                <p>Kop Khun Kha!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/jaithep2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/jaithep2019/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/jaithep2019/gallery-2.jpg" data-featherlight="image"><img class="" src="images/jaithep2019/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/jaithep2019/gallery-3.jpg" data-featherlight="image"><img class="" src="images/jaithep2019/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/jaithep2019/gallery-4.jpg" data-featherlight="image"><img class="" src="images/jaithep2019/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/jaithep2019/gallery-5.jpg" data-featherlight="image"><img class="" src="images/jaithep2019/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/jaithep2019/gallery-6.jpg" data-featherlight="image"><img class="" src="images/jaithep2019/gallery-6.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Tier</th>
                                            <th>Ticket Type</th>
                                            <th>Ticket Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="5">Standard</td>
                                            <td>3 Day Adult Pass</td>
                                            <td>RM301</td>
                                        </tr>
                                        <tr>
                                            <td>1 Day Adult Pass - Friday</td>
                                            <td>RM123</td>
                                        </tr>
                                        <tr>
                                            <td>1 Day Adult Pass - Saturday</td>
                                            <td>RM123</td>
                                        </tr>
                                        <tr>
                                            <td>1 Day Adult Pass - Sunday</td>
                                            <td>RM123</td>
                                        </tr>
                                        <tr>
                                            <td>8x 3 Day Adult Passes</td>
                                            <td>RM2,187</td>
                                        </tr>


                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Feb 3 2019 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/jai thep festival 2019 (01st to 03rd feb 2019)/events" role="button">BUY TICKETS</a>
                                
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown exclude ticket fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Indian Rupee prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in INR may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li> --}}
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket sales will close on  3rd Feb 2019, subject to availability.</li>
                                    {{-- <li>Adult: Strictly 21 years old and above.</li> --}}
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	//$(document).ready(function(){
    //    $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')


   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Temporarily down for Maintenance</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>We are performing scheduled maintenance. We should be back online shortly</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection