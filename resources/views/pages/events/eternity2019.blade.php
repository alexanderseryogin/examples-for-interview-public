@extends('master')
@section('title')
    Ong Seong Wu 1st Fan Meeting Eternity Tour in Malaysia
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Ong Seong Wu 1st Fan Meeting Eternity Tour in Malaysia" />
    <meta property="og:description" content="Ong Seong-wu is an accomplished South Korean singer and actor. He is known for his participation in the survival reality show Produce 101 Season 2, where he finished in fifth place, and is a former member of the show's derivative boy group Wanna One."/>
    <meta property="og:image" content="{{ Request::Url().'images/eternity2019/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/eternity2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Ong Seong Wu 1st Fan Meeting Eternity Tour in Malaysia"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/eternity2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Ong Seong Wu 1st Fan Meeting Eternity Tour in Malaysia">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Ong Seong Wu 1st Fan Meeting Eternity Tour in Malaysia</h6> Tickets from <span>RM280</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  23rd March 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Axiata Arena, Bukit Jalil, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/AhQs6UgSMjp">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i>  6.00pm - 10.00pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <div class="col-sm-12">
                                <hr>
                                <p><h2>Ong Seong Wu 1st Fan Meeting Eternity Tour in Malaysia</h2><br/>
                                Ong Seong-wu is an accomplished South Korean singer and actor. He is known for his participation in the survival reality show Produce 101 Season 2, where he finished in fifth place, and is a former member of the show's derivative boy group Wanna One.</p>
                                <p>The former Wanna One member will be holding his first ever fan meeting event at Axiata Arena as part of his Ong Seong Wu First Fan Meeting – Eternity tour. The event will showcase Seong Wu’s charms as a solo artist.</p>
                                <p>Hi-Touch privilege will see lucky fans having an up-close opportunity encounter with Ong Seong-wu. All ticket purchasers who attend the event are entitled to the Enternity Tour official poster.</p>
                                <p>There is more for fans as Ong Seong-wu, will be taking group photos with 350 ticketholders from the Apple Zone, 160 from Lychee Zone and 50 from Mango Zone. The participants will be randomly chosen.</p>
                                <p>Fans are advised to visit Hatchery Co Ltd’s Facebook page for more details.</p>
                                <p><br/><h2>About Hatchery</h2><br/>
                                Hatchery Co., Ltd. is an entertainment agency based in Bangkok, Thailand. As your partner, we provide all around services including creating, planning, coordinating and managing. Our well defined products and processes are specially customised to meet your specific needs and goals.<br/><br/>
                                At Hatchery, we offer a wider range of contents in the Media and Entertainment Industry which cover regional and global markets.</p>
                            </div>
                            <div class="clearfix"></div>
                            {{-- <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2FNcP-fiI5M?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div> --}}
                        </div>
                    </div>
                </div>
                {{-- <div class="clearfix"></div> --}}
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/eternity2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/eternity2019/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/eternity2019/web-banner.jpg" data-featherlight="image"><img class="" src="images/eternity2019/web-banner.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>EVENT INFORMATION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img class="image-responsive" src="images/eternity2019/package-info.jpg" style="width:100%; height:auto;" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Seat Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="background-color:#CBD635; color:#ffffff;">Apple</td>
                                            <td>RM580</td>
                                            <td rowspan="5"><img class="img-responsive seatPlanImg" src="images/eternity2019/seat-plan.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:#EE5265; color:#ffffff;">LYCHEE</td>
                                            <td>RM480</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:#FDC728; color:#ffffff;">MANGO</td>
                                            <td>RM380</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:#FF901E; color:#ffffff;">ORANGE</td>
                                            <td>RM280</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Mar 24 2019 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/ong seong wu meet eternity tour 2019/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>

                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Persons under the age of 14 must be accompanied by an ticket paying adult</li>
                                    <li>Each transaction is limited to a maximum of 4 tickets only</li>
                                    <li>Prices shown excludes RM4.00 ticket fee and credit card fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
	//$(document).ready(function(){
    //    $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>Tickets for the event will go on sale this February 16 starting 10.00 AM (GMT+8 KUL/MY) local time</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection