@extends('master')
@section('title')
	 UFC Fight Night Adelaide
@endsection

@section('header')
	@include('layouts.partials._header')
	
@endsection

@section('content')
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link type="text/css" rel="stylesheet" href="css/custom2.css">
<link type="text/css" rel="stylesheet" href="css/clock.css">
<link type="text/css" rel="stylesheet" href="css/custom/ufc221.css">
<link type="text/css" rel="stylesheet" href="css/timeTo.css">
<style type="text/css">
	.background-ufc {
		background-image: url('/images/ufcadelaide2018v2/background.png');
	    background-size: 100% auto;
	    background-repeat: no-repeat;
	    height: 1000px;
	}
	.sect-count {
		top: 60%;
	}
	.open-sans {
		font-family: 'Open Sans', sans-serif;
		font-weight: 100;
	}
	.text-white {
		color: #fff;
	}
	.h2-count {
		font-size: 32px;
	}
	.btn-danger {
		background: linear-gradient(#EA3051, #EA3051);
	}
	.btn-wrapper {
		margin-top: 80px;
	}
	@media screen and (max-width: 767px){ 
		.background-ufc {
			background-image: url('/images/ufcadelaide2018v2/mobile-background.png');
			height: 1200px;
		}
		.sect-count {
			margin-top: 40px;
			margin-bottom: 40px;
		}
		.btn-wrapper {
			margin-top: 120px;
		}
	}
	@media screen and (min-width: 768px){ 
		.sect-count {
			top: 40%;
		}
		.background-ufc { 
			height: 910px;
		}
	}
	@media screen and (min-width: 1024px){ 
		.background-ufc { 
			height: 986px;
		}
		.sect-count {
			top: 50%;
		}
	}
	@media screen and (max-width: 1024px){ 
		.background-ufc { 
			height: 1096px;
		}
		.sect-count {
			top: 50%;
		}
	}
	@media screen and (min-width: 768px) and (max-width: 1024px){ 
		.background-ufc {
			height: 916px;
		}
		.sect-count {
			top: 50%;
		}
	}
	@media screen and (min-width: 1024px) and (max-width: 1366px){ 
		.background-ufc{
			height: 1200px;
		}
		.sect-count {
			top: 50%;
		}
	}
	@media screen and (min-width: 1400px){ 
		.background-ufc{
			height: 1400px;
		}
		.sect-count {
			top: 60%;
		}
	}
	@media screen and (max-width: 414px){ 
		.background-ufc { 
			height: 1260px;
		}
		.sect-count {
			top: 50%;
		}
	}
	#darkgreen {
		background-image: url('/images/ufcadelaide2018v2/bg-content.png');
		background-size: cover;
		background-repeat: no-repeat;
	}
	.sect-count {
		position: absolute;
	}	
	.title-green {
		color: #27917A;
	}
	.text-green {
		color: #27917A;
	}
	.title-red {
		color: #EA3051;
	}
	.text-red {
		color: #EA3051;
	}
	.fighterRow {
		padding-bottom: 40px;
	}
	.fighterTitle h1 {
		text-align: left;
		text-transform: uppercase;
		font-size: 18px;
		margin-bottom: 5px !important;
	}
    .fighterName {
        text-align: left;
	}
	.fighterName h1 { 
		/* background-color: #27917A; */
		background-color: #000000;
		border: 1.5px #EA3051 solid;
		text-align: left;
		font-size: 22px;
	}
	.fighterName h6 { 
		background-color: #27917A;
        text-align: left;
	}
	.fighterDetails {
		margin-top: 50px;
		margin-left: 15px;
	}
	.fighterVs {
		margin-top: 35px;
		margin-left: 15px;
	}
	.getTixBtn {
		border: 3px #fff solid;
    	color: #fff;
	}
	section.pageContent .intro {
    	padding-bottom: 30px;
    }
    section.pageContent .intro p{
    	font-size: 19px;
    	padding-bottom: 22px;
    }
    .fw-100 {
    	font-weight: 100;
    }

#priceFixed .priceNbtn .leftBox {
    color: #ffffff;
}

#priceFixed .priceNbtn2 {
	padding: 80px;
    color: #ffffff;
}

.ticketRow {
    font-family: Lato;
    height: 162px;
    color: #fff;
    padding-top: 40px;
    margin-top: 10px;
    margin-bottom: 10px;
    overflow: hidden;
    /*border: #fff solid 2px;*/
    background-color: #99999926;
}
.ticketRow div {
    height: 80px;
}
.ticketRow .border-left{
    border-left: solid 1px #cbd5e2;
}
.ticketRow .ticketTitle h1{
    font-size: 30px;
    font-weight: 900;
    text-align: center;
    margin: 0px;
}
.ticketRow .ticketTitle p{
    font-family: Lato;
    font-size: 14px;
    font-weight: 100;
    text-align: center;
    margin: 0px;
}
.ticketRow .ticketTitle hr{
	height: 5px !important;
	width: 100px !important;
	border: none;
}
.ticketRow .ticketDesc h6{
    font-size: 18px;
    font-weight: 200;
	text-align: center;
    margin: 0px;
}
.ticketRow .ticketInfo {
    font-size: 12px;
    font-weight: 200;
	text-align: left;
    margin: 0px;
}
.ticketRow .ticketDay {
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0px;
    text-align: left;
    overflow-y: scroll;
}
.ticketRow .ticketDay h6{
    font-size: 18px;
    font-weight: 100;
    padding: 0px;
    margin: 0px;
}
.ticketRow .ticketAdmission {
    display: flex;
    align-items: left;
    justify-content: left;
}
.ticketRow .ticketAdmission p {
    margin-bottom: 0px;
    line-height: 19px;
    font-weight: 100;
}
.ticketRow .ticketBuy h6 {
    margin-top: 3px;
    margin-bottom: 3px;
    font-size: 20px;
    font-weight: 100;
}
.ticketRow .ticketBuy small {
    font-size: 70% !important;
}
.ticketRow .ticketBuy .prices .discountPrice {
    font-size: 14px;
    text-align: center;
	color: #fff;
}
.ticketRow .ticketPrice {
    display: flex;
    justify-content: center;
    flex-direction: column;
}
.ticketRow .ticketPrice .priceRow{
    height: 30%;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    text-align: right;
    flex-direction: row; 
}
.ticketRow .ticketPrice .priceRow small {
    line-height: 1;
}
.ticketRow .ticketPrice .priceRow .discountLabel {
    padding: 4px 10px;
    font-family: Lato;
    font-size: 10px;
    font-weight: 100;
    text-align: center;
    border-radius: 8px;
}
.ticketRow .ticketPrice h5 {
    text-align: left;
    text-decoration: line-through;
    font-size: 10px;
    padding-right: 5px;
}
.ticketRow .ticketPrice h6 {
    font-weight: 100;
    text-align: left;
    font-size: 21px;
    padding: 0px;
}
.ticketRow .ticketPrice .ultraBuyBtn a {
   padding-top:5px;
}
.ticketRow .ticketPrice .textLeft {
    margin-left: 0px;
}
.ticketRow .ticketPrice .textRight {
    margin-right: 0px;
}
.ufcBuyBtnRed {
	width: 90px;
    height: 33px;
    border-radius: 8px;
    background-color: #DE2724;
    font-family: Lato;
    font-size: 12px;
    font-weight: 100;
    text-align: center;
    line-height: 1;
    color: #ffffff;
}
.btn.hover, .btn:hover, .open>.dropdown-toggle.btn {
    outline: 0;
    color: #999;
}
.btn.active, .btn:active, .btn:focus {
    color: #fff;
    background-color: #666666;
    border-color:  #666666;
}
.ticketRowFlight {
    font-family: Lato;
    height: 200px;
    padding-top: 10px;
    margin-top: 10px;
    margin-bottom: 10px;
    overflow: hidden;
    /*border: #fff solid 2px;*/
    background-color: #99999926;
}
.ticketRowFlight div {
    height: 80px;
}
.ticketRowFlight a {
    cursor: pointer;
}
.ticketRowFlight .infobtn {
    color: #fff;
}
.ticketRowFlight .ultraBuyBtn {
    color: #fff;
}
.ticketRowFlight .border-left{
    border-left: solid 1px #fff;
}
.ticketRowFlight .flightTitle .infobtn{
    color: #fff;
    margin-left: 24px;
}
.ticketRowFlight .ticketTitle h1{
    font-size: 35px;
    font-weight: 900;
    text-align: center;
    color: #fff;
    margin: 0px;
}
.ticketRowFlight .ticketTitle hr{
	height: 5px !important;
	width: 100px !important;
	border: none;
}
.ticketRowFlight .ticketTitle p{
    font-family: Lato;
    font-size: 14px;
    font-weight: bold;
    text-align: center;
    color: #fff;
    margin: 0px;
}
.ticketRowFlight .ticketDay {
    margin: 0px;
    text-align: left;
    overflow-y: scroll;
}
.ticketRowFlight .ticketDay h6{
    font-size: 18px;
    font-weight: bold;
    padding: 0px;
    margin: 0px;
    color: #fff;
}
.ticketRowFlight .ticketAdmission {
    display: flex;
    align-items: center;
    justify-content: center;
}
.ticketRowFlight .ticketBuy h6 {
    margin-top: 3px;
    margin-bottom: 3px;
    font-size: 20px;
    font-weight: 600;
}
.ticketRowFlight .ticketBuy small {
    font-size: 70% !important;
}
.ticketRowFlight .ticketBuy .prices {
	height: auto;
	padding-bottom: 10px;
}
.ticketRowFlight .ticketBuy .prices .strikethrougPrice {
    text-decoration: line-through;
    color: red;
    font-weight: bold;
    font-size: 10px;
    float: left;
    position: absolute;
    left: 0;
    top: 10px;
}
.ticketRowFlight .ticketBuy .prices .discountPrice {
    font-size: 20px;
    text-align: right;
    margin-left: 60px;
	color: #fff;
	padding-bottom: 10px;
}
.ticketRowFlight .ticketPrice {
    display: flex;
    justify-content: center;
    flex-direction: column;
}
.ticketRowFlight .ticketPrice .priceRow{
    height: 30%;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    text-align: right;
    flex-direction: row; 
}
.ticketRowFlight .ticketPrice .priceRow small {
    line-height: 1;
}
.ticketRowFlight .ticketPrice .priceRow .discountLabel {
    padding: 4px 10px;
    font-family: Lato;
    font-size: 10px;
    font-weight: 400;
    text-align: center;
    color: #ffffff;
    border-radius: 8px;
    background-color: #b30000;
}
.ticketRowFlight .ticketPrice h5 {
    text-align: left;
    text-decoration: line-through;
    color: #fff;
    font-size: 10px;
    padding-right: 5px;
}
.ticketRowFlight .flightTitle {
    height: 50px;
}
.ticketRowFlight .flightTitle h6 {
    text-align: center;
    font-size: 22px;
    color: #fff;
}
.ticketRowFlight .ticketPrice h6 {
    font-weight: bold;
    text-align: left;
    color: #fff;
    font-size: 21px;
    padding: 0px;
}
.ticketRowFlight .ticketPrice .ultraBuyBtn a {
   padding-top:5px;
}
.ticketRowFlight .ticketPrice .textLeft {
    margin-left: 0px;
}
.ticketRowFlight .ticketPrice .textRight {
    margin-right: 0px;
}
.ticketRowFlight .flightDescription p {
    margin-bottom: 0px;
    line-height: 17px;
}
.ticketRowFlight .flightDescription .plane {
    font-family: Lato;
    font-size: 12px;
    font-style: italic;
    color: #fff;
}
.ticketRowFlight .flightDescription .schedule {
    font-family: Lato;
    font-size: 12px;
    font-weight: 200;
    color: #fff;
}
.ticketRowFlight .flightDescription .destination {
    font-family: Lato;
    color: #fff;
    font-size: 13px;
}
.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
    width: 100%;
    margin-bottom: 10px;
}
.bootstrap-select .btn-default {
    color: #333;
    background-color: #fff;
}
.bootstrap-select .btn {
    border: solid 1px #333;
    font-size: 15px;
    font-weight: 400;
    line-height: 1.4;
    border-radius: 0px;
    padding: 10px 15px;
    /*-webkit-font-smoothing: subpixel-antialiased;
    transition: border .25s linear,color .25s linear,background-color .25s linear;*/
}
.dropdown-menu>li:first-child>a:first-child {
    border-top-right-radius: 0px;
    border-top-left-radius: 0px;
}
.dropdown-menu>li>a {
    padding: 8px 16px;
    line-height: 1.429;
    color: #333;
}
.btn-group.open .dropdown-toggle {
    color: #333;
    box-shadow: none;
}
.btn-default.active, .btn-default.hover, .btn-default:active, .btn-default:focus, .btn-default:hover, .open>.dropdown-toggle.btn-default {
    color: #333;
    background-color: #fff;
    border-color: #333;
}
.btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default:active.focus, .btn-default:active:focus, .btn-default:active:hover, .open>.btn-default.dropdown-toggle.focus, .open>.btn-default.dropdown-toggle:focus, .open>.btn-default.dropdown-toggle:hover {
    color: #333;
    background-color: #fff;
    border-color: #333;
}
.dropdown-menu, .select2-drop {
    z-index: 1000;
    background-color: #fff;
    min-width: 220px;
    border: solid 1px #333;
    margin-top: 9px;
    padding: 0;
    font-size: 14px;
     border-radius: 0px; 
    box-shadow: none;
}
.ticketCard {
    width: 100%;
    transition: all 0.3s cubic-bezier(.25,.8,.25,1);
    margin-top: 10px;
    margin-bottom: 10px;
    color: #fff;
    padding: 20px; 
    overflow: hidden;
    /*border: #fff solid 2px;*/
    background-color: #99999926;
}
/*.ticketCard:hover {
   box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22); 
}*/
.ticketCard .card-body{
    
}
.ticketCard .card-body hr{
	height: 5px !important;
	width: 150px !important;
	border: none;
}
.ticketCard .card-body .cardTop {
    padding-top: 20px;
    padding-bottom: 15px;
    border-bottom: solid 0.8px #cbd5e2;
}
.ticketCard .card-body .cardTop p{
    margin-bottom: 0px;
}
.ticketCard .card-body .cardTop .ticketTier {
    font-size: 12px;
    font-weight: 400;
    color: #cbd5e2;
}
.ticketCard .card-body .cardTop .ticketTier .ticketCategory {
    font-family: Lato;
    font-size: 16px;
    font-weight: 900;
    color: #cbd5e2;
}
.ticketCard .card-body .cardTop .ticketFlight {
    font-family: Lato;
    font-size: 20px;
    font-weight: bold;
    line-height: 1.0;
    letter-spacing: 0.3px;
    text-align: left;
    color: #333333;
}
.ticketCard .card-body .cardTop .ticketPrice {
    padding-top: 50px;
}
.ticketCard .card-body .cardTop .ticketPrice p{
    font-family: Lato;
    font-size: 16px;
    font-weight: bold;
    color: #333333;
    line-height: 13px;
}
.ticketCard .card-body .cardTop .ticketPrice small{
    font-family: Lato;
    font-size: 9.6px;
    font-weight: 300;
    letter-spacing: 0.2px;
    color: #676a6a;
}
.ticketCard .card-body .cardBottom {
    padding-top: 15px;
    padding-bottom: 15px;
}
.ticketCard .card-body .cardBottom .ticketPrice {
    font-weight: bold;
    text-align: center;
    color: #0d6bc4;
    font-size: 16px;
    height: 20px;
}
.ticketCard .card-body .cardBottom .ticketDiscount {
    text-decoration: line-through;
    text-align: center;
    color: #b30000;
    font-size: 12px;
}
.border {
	border-top: #FFFFFF;

}
#getTicket {
    padding-bottom: 0px;
}
.info-box {
    margin-top: 3px;
	margin-bottom: 3px;
	padding-top: 10px;
	padding-left: 15px;
	padding-right: 15px;
}
.info-box .type {
    color: #CBD5E2;
    font-size: 30px;
    margin: 0;
    margin-top: 30px;
	text-align: center;
	padding-top: 15px;
}
.info-box .days {
    margin: 0;
    margin-top: 10px;
	text-align: center;
	padding-bottom: 15px;
}
.info-box .infotitle {
    background-color: #F7F9FB;
	text-align: center;
}
.info-box .infotitle .type {
    color: #CBD5E2;
    font-size: 30px;
    margin: 0;
    margin-top: 30px;
    text-align: center;
}
.info-box .infotitle .days {
    margin: 0;
    margin-top: 10px;
    text-align: center;
}
.info-box .details {
    background-color: #F7F9FB;

    text-align: left;
	padding-left: 15px;
	padding-top: 15px;
}
.info-box .details ul {
    list-style-type: none;
    padding: 10px 0;
}
.info-box .details ul li span{
    margin-right: 5px;
    width: 20px;
    text-align: center;
}
.info-box .services {
    margin: 5px 0;
}
.info-box .services .item{
    text-align: center;
}
.info-box .services .item img{
    height: 160px;
	display: block;
	padding: 5px 5px 5px 5px;
}
.info-box .services .item span{
    font-size: 12px;
    text-align: center;
}
.info-box .copy_text {
    margin-bottom: 20px;
}
</style>
	<!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        {{-- <div class="bigBanner-overlay"></div> --}}
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;">
            <img src="{{asset('images/ufcadelaide2018v2/background.png')}}" style="width: 100%" class="img-responsive" alt="UFC Fight Night Adelaide">
        </div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/ufcadelaide2018v2/mobile-background.png')}}" style="width: 100%" class="img-responsive" alt="UFC Fight Night Adelaide">
        </div>
        {{-- <div class="col-md-12 col-xs-12 sect-count">
			<div class="col-md-12 col-xs-12"> 
				<h2 class="text-center text-white open-sans h2-count">Countdown to Special Packages to UFC Singapore!</h2>
				<div class="text-center" id="countdown" style="padding-top: 40px; padding-bottom: 80px;"></div>
			</div>
			<div class="col-md-12 col-xs-12 text-center btn-wrapper"> 
				<a class="btn btn-danger" href="https://docs.google.com/forms/d/e/1FAIpQLSe9tyIQ8OK4juJo_HAr-iOGnF6dTviNEAlhvE7GOX3cHxZ00A/viewform?usp=sf_link" role="button">Register Your Interest</a>
			</div>
		</div> --}}
		<!-- Title and Price -->
		<div id="priceFixed" class="section-black">
        	<div class="container">
            	<div class="row priceNbtn">
                	<div class="col-sm-offset-1 col-sm-10">
                    	<div class="row">
                        	<div class="col-sm-9 leftBox">
                            	<h6>UFC ADELAIDE</h6> DECEMBER 2, SUN<br/>ADELAIDE ENTERTAINMENT CENTRE
                        	</div>
                        	<div class="col-sm-3 text-center">
                            	<a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        	</div>
                    	</div>
                	</div>
            	</div>          
        	</div>
    	</div>
    </section>
    <!-- /Banner Section -->
	<!-- Content Section -->
	<section class="pageContent">
	  <!-- Main Body -->
		<div class="mainBodyContent no-btm-mar" style="padding-top: 0px;">
			<section id="darkgreen">
				
				<section class="pageCategory-section last">
					<div class="container tixPrice">
						<div class="row">
                        	<div class="col-sm-offset-1 col-sm-10">
                            	{{-- <div class="text-center col-sm-12 col-xs-12">
                            		<h5 class="titleOpenSans" style="padding-top:50px;">UFC TICKETS +<span class="title-red"> HOTELS / FLIGHTS</span></h5>
                                </div>
                                <div class="clearfix"></div>
                                <div class="clearfix"></div>
                                <!-- dekstop tab-->
	                            <div class="col-sm-12 hidden-xs hidden-md hidden-sm">
									<div class="col-sm-7">
	                                    <button data-toggle="tab" href="#ticket_hotel_single" class="tixOptBtn center-block btn" style="width: 100%;"><i class="fa fa-ticket"></i> <i class="fa fa-bed"></i> TICKET + HOTEL</button>
	                                </div>
                                    <div class="col-sm-5">
                                        <button data-toggle="tab" href="#ticket_flight" class="tixOptBtn center-block btn" style="width: 100%;"><i class="fa fa-ticket"></i> <i class="fa fa-plane"></i> TICKET + FLIGHT</button>
                                    </div>
                                    <!-- <div class="col-sm-4">
                                        <button data-toggle="tab" href="#ticket_hotel_double" class="tixOptBtn center-block btn active" style="width: 100%;"><i class="fa fa-ticket"></i><i class="fa fa-bed"></i>TICKET + HOTEL <i class="fa fa-user" aria-hidden="true"></i><i class="fa fa-user" aria-hidden="true"></i></button>
                                    </div> -->
	                            </div>

	                            <!-- mobile tab -->
	                            <div class="col-xs-12 hidden-lg pd-0">
									<div class="col-xs-12" style="width: 100%">
	                                    <button data-toggle="tab" href="#ticket_hotel_single" class="tixOptBtn btn" style="width: 100%; margin-top: 10px; padding: 0px;">
	                                        <i class="fa fa-ticket" style="padding-top:20px;"></i>
	                                        <i class="fa fa-bed"></i>
	                                        <p> TICKET + HOTEL</p>
                                            <!-- <i class="fa fa-user" aria-hidden="true"></i> -->
	                                    </button>
                                    </div>
                                    <div class="col-xs-12" style="width: 100%">
                                        <button data-toggle="tab" href="#ticket_flight" class="tixOptBtn btn" style="width: 100%; margin-top: 10px; padding: 0px;">
                                            <i class="fa fa-ticket" style="padding-top:20px;"></i>
                                            <i class="fa fa-plane"></i>
                                            <p> TICKET + FLIGHT</p>
                                        </button>
                                    </div>
                                    <!-- <div class="col-xs-12" style="width: 100%">
                                        <button data-toggle="tab" href="#ticket_hotel_double" class="tixOptBtn btn" style="width: 100%; margin-top: 10px; padding: 0px;">
                                            <i class="fa fa-ticket"></i>
                                            <i class="fa fa-bed"></i>
                                            <p>TICKET + HOTEL</p>
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                        </button>
	                                </div> --> 
	                            </div>--}}
                                <div class="clearfix"></div>
								<div class="text-center col-sm-12 col-xs-12">
									<h5 class="titleOpenSans" style="padding-top:50px;">SEATING<span class="title-red"> PLAN</span></h5>
									<img class="img-responsive center-block" src="images/ufcadelaide2018v2/seat-plan.png" alt="Adelaide Entertainment Centre">
								</div>
                                <div class="clearfix"></div>
	                            <div class="tab-content" id="anchorPrice">
	                            	<div id="ticket_hotel_single" class="tab-pane fade in active">
									@foreach($ticket_hotel as $ticket)
	                            		<div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
	                                        {{-- <small class="discountTag"> SOLD OUT</small> --}}
                                            <div class="col-sm-3 text-center ticketTitle">
                                                <div class="row">
                                                    {{-- <p class="tier text-green">Tier 1</p> --}}
                                                    <h1>{{$ticket["Ticket Tier"]}}</h1>
													<hr style="	background-color: {{$ticket["Ticket Color"]}} !important; color: {{$ticket["Ticket Color"]}} !important;">
                                                </div> 
                                            </div>
                                            {{-- <div class="col-sm-3 text-center border-left ticketDesc"> --}}
                                                <!-- <h6>{{$ticket["Ticket Web Description"]}}</h6> -->
												<?php
													$splitDesc = explode(' + ', $ticket["Ticket Web Description"], 2); // Restricts it to only 2 values, for description
													$first_desc = $splitDesc[0];
													$last_desc= !empty($splitDesc[1]) ? $splitDesc[1] : ''; // If last name doesn't exist, make it empty
												?>
                                                {{-- <h6>{{$first_desc}}<br />+<br />{{$last_desc}}</h6>
                                            </div> --}}
                                            <div class="col-sm-6 ticketAdmission border-left ticketInfo">
                                                <p style="float: left;">
													{{$ticket["Ticket Web Description"]}}<br/>
								
													<i class="fa fa-angle-right" style="color:#1FC19D;" aria-hidden="true"></i> {{$ticket["Ticket Info"]}}<br/>
													<i class="fa fa-angle-right" style="color:#1FC19D;" aria-hidden="true"></i> {{$ticket["Room Type"]}}
													
                                                </p>
                                                {{-- <div style="position: absolute;right: 10px;cursor: pointer;" data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}">
                                                    <img src="images/ufcadelaide2018v2/info-btn.png" width="20">
                                                </div> --}}
                                            </div>
                                            <div class="col-sm-3 ticketBuy border-left">
                                                <div class="col-sm-12 text-center">
                                                    <div class="prices">
                                                        <span class="discountPrice fw-100">AUD${{$ticket["Website Price"]}} / RM{{$ticket["Total Price"]}}</span>
                                                        <a class="btn ufcBuyBtnRed center-block" datetime="Jun 1 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}" onclick="location.href=this.href&linkerParam;return false;">BUY NOW</a>
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>

                                        {{-- mobile version --}}
                                        <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard">
                                            <div class="col-xs-12 card-body pd-0">
                                                <div class="col-xs-12">
                                                    {{-- <p class="text-green text-center">Tier 1</p> --}}
                                                    <h2 class="text-center">{{$ticket["Ticket Tier"]}}</h2>
													<hr style="	background-color: {{$ticket["Ticket Color"]}} !important; color: {{$ticket["Ticket Color"]}} !important;">
                                                </div>
                                                {{-- <div class="cardBottom col-xs-12 text-center">
                                                    <p>{{$ticket["Ticket Web Description"]}}</p>
													<a data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}"><img src="images/ufcadelaide2018v2/info-btn.png" width="20"></a>
												</div> --}}
												<div class="cardBottom col-xs-12">
													{{$ticket["Ticket Web Description"]}}<br/>
													<i class="fa fa-angle-right" style="color:#1FC19D;" aria-hidden="true"></i> {{$ticket["Ticket Info"]}}<br/>
													<i class="fa fa-angle-right" style="color:#1FC19D;" aria-hidden="true"></i> {{$ticket["Room Type"]}}
												</div>
												<div class="cardBottom col-xs-12 text-center">
                                                    <h6>AUD${{$ticket["Website Price"]}} / RM{{$ticket["Total Price"]}}</h6>
                                                    <a class="btn ufcBuyBtnRed center-block" datetime="Jun 1 2018 00:00:00 GMT+0800" target="_blank" href="{{ $ticket["Ticketserv URL"] }}" onclick="location.href=this.href&linkerParam;return false;">BUY NOW</a>
                                                </div>
                                            </div>
                                        </div>
									@endforeach
	                            	</div>

                                    <div id="ticket_hotel_double" class="tab-pane fade in">
                                        <div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
                                            {{-- <small class="discountTag"> SOLD OUT</small> --}}
                                            <div class="col-sm-2 text-center ticketTitle">
                                                <div class="row">
                                                    <p class="tier text-green">Tier 1</p>
                                                    <h1>Cart 1</h1>  
                                                </div> 
                                            </div>
                                            <div class="col-sm-3 text-center ticketDay border-left">
                                                <h6>Cart 1 + Double Hotel</h6>
                                            </div>
                                            <div class="col-sm-4 ticketAdmission border-left">
                                                <p style="float: left;">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's<br>
                                                </p>
                                            </div>
                                            <div class="col-sm-3 ticketBuy border-left">
                                                <div class="col-sm-12 text-center">
                                                    <div class="prices">
                                                        <span class="discountPrice fw-100">SGD $185.00</span>
                                                        <a class="btn ufcBuyBtnRed center-block" data-toggle="modal" data-target="#buyModalRegistered">BUY NOW</a>
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>

                                        {{-- mobile version --}}
                                        <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard">
                                            <div class="col-xs-12 card-body pd-0">
                                                <div class="col-xs-12">
                                                    <p class="text-green text-center">Tier 1</p>
                                                    <h2 class="text-center">Cart 1</h2>
                                                </div>
                                                <div class="cardBottom col-xs-12 text-center">
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                                                    <h6>SGD $185.00</h6>
                                                    <a class="btn ufcBuyBtnRed center-block" data-toggle="modal" data-target="#buyModalRegistered">BUY NOW</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

	                            	<div id="ticket_flight" class="tab-pane fade in">
	                            		<div class="col-sm-12 col-xs-12" style="margin-top: 40px;">
	                                        <p style="color: #fff">Select the country you will be flying from*:</p>
	                                        <select class="selectpicker" id="select-country">
	                                            <option value="1">KUALA LUMPUR</option>
	                                            {{-- <option value="2">KUCHING</option>
	                                            <option value="3">PENANG</option>
	                                            <option value="4">BANGKOK</option>
	                                            <option value="5">JAKARTA</option> --}}
	                                        </select>
	                                    </div>

	                                    <div id="country-1" class="tab-content flightTab">
	                                    	{{-- ticket + flight --}}
											<div class="col-sm-12 col-xs-12 ticketRowFlight hidden-xs hidden-sm">
											    {{-- <small class="discountTag"> 40% OFF</small> --}}
											    {{-- <a class="infobtn pull-right" data-toggle="modal" data-target="#flightModal{{ $ticket["Sorting ID"] }}"><i class="material-icons">info_outline</i></a> --}}
											    <div class="col-sm-12 flightTitle"><h6>1 Cat 2 + Return Flight (KUL-SIN)</h6></div>
											    <div class="col-sm-2 text-center ticketTitle">
											        <div class="row">
											            {{-- <p>EARLY BIRD</p> --}}
											            <div class="clearfix" style="height: 10px;">&nbsp;</div>
											            {{-- <p class="tier">Tier 3</p> --}}
											            <h1>CAT 2</h1>
													<hr style="	background-color:#4FA5D6 !important; color:#4FA5D6 !important;">
											        </div>
											    </div>
											    <div class="col-sm-7 flightDescription">
											        <div class="col-sm-6 text-center border-left">
											            <p class="plane">Departure</p>
											            <p class="schedule">Thu, Jun 23, 2018, 09:55am</p>
											            <p class="destination">KUL</p>
											            <p class="schedule">Thu, Jun 24, 2018, 07:10pm</p>
											            <p class="destination">SIN</p>
											        </div>
											        <div class="col-sm-5 text-center">
											            <p class="plane">Arrival</p>
											            <p class="schedule">Sun, Jun 23, 2018, 11:10am</p>
											            <p class="destination">SIN</p>
											            <p class="schedule">Sun, Jun 24, 2018, 08:10pm</p>
											            <p class="destination">KUL</p>
											        </div>
											        {{-- <div class="col-sm-1 text-center">
											            <a data-toggle="modal" data-target="#flightModal{{ $ticket["Sorting ID"] }}"><img src="images/ultrasingapore2018/i.png" width="20"></a>
											        </div> --}}
											    </div>
											    <div class="col-sm-3 ticketBuy border-left">
											    	<div class="col-sm-12 text-center">
											                {{-- <h6>{{ $ticket["Private Sale"] }}</h6> --}}
											                <div class="prices">
											                    {{-- <span class="strikethrougPrice">SGD{{$ticket["Actual Price Tier 3"]}}</span> --}}
											                    <span class="discountPrice" style="margin-left: auto;">SGD$374.80</span>
											                </div>
											                <a class="btn ufcBuyBtnRed center-block" id="buyButton" datetime="Jun 1 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/sgp/en-AU/shows/ufc fight night singapore - cat 2 + return flight (kul-sin)/events">BUY NOW</a>
											                {{-- <p class="small" style="padding-top: 5px; line-height: 1.3; font-size: 80%;">Price excludes ticketing fee, Event Protect  insurance & credit card charges</p> --}}
											                <div class="clearfix" style="height:30px;">&nbsp;</div>
											        </div>
											    </div>

											</div>

											{{-- mobile version --}}
	                                        <div class="col-xs-12 col-sm-6 hidden-lg hidden-md ticketCard">
	                                            <div class="col-xs-12 card-body pd-0">
	                                                <div class="col-xs-12">
	                                                    {{-- <p class="text-green text-center">Tier 1</p> --}}
	                                                    <h2 class="text-center">Cat 2</h2>
													</div>
	                                                <div class="cardBottom col-xs-12 text-center">
	                                                    <p>1 Cat 2 + Return Flight (KUL-SIN)</p>

														<div class="col-xs-6 text-center">
															<p class="plane">Departure</p>
															<p class="schedule">Thu, Jun 23, 2018, 09:55am</p>
															<p class="destination">KUL</p>
															<p class="schedule">Thu, Jun 24, 2018, 07:10pm</p>
															<p class="destination">SIN</p>
														</div>
														<div class="col-xs-6 text-center">
															<p class="plane">Arrival</p>
															<p class="schedule">Sun, Jun 23, 2018, 11:10am</p>
															<p class="destination">SIN</p>
															<p class="schedule">Sun, Jun 24, 2018, 08:10pm</p>
															<p class="destination">KUL</p>
														</div>

													</div>
													<div class="cardBottom col-xs-12 text-center">
	                                                    <h6>SGD$374.80</h6>
														<a class="btn ufcBuyBtnRed center-block" datetime="Jun 1 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/sgp/en-AU/shows/ufc fight night singapore - cat 2 + return flight (kul-sin)/events">BUY NOW</a>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                            	</div>
	                            </div>
                            </div>
                        </div>
					</div>
				</section>

				<section class="pageCategory-section last text-white" style="padding-top: 50px;">
					<div class="container intro">
						<div class="row"> 
							<h5 class="title">ABOUT<span class="title-red"> THE EVENT</span></h5>
							<div class="col-md-12 col-xs-12 font-Lato"> 
								<div class="col-md-6 col-xs-12">
									<p class="text-justify">UFC®, is a premium global sports brand and the largest Pay-Per-View event provider in the world. Celebrating its 25th Anniversary in 2018, UFC boasts more than 284 million fans worldwide and has produced over 440 events in 22 countries since its inception in 1993.</p>
								</div>
								<div class="col-md-6 col-xs-12">
									<p class="text-justify">Acquired in 2016 by global sports, entertainment and fashion leader Endeavor (formerly WME | IMG), together with strategic partners Silver Lake Partners and KKR, UFC is headquartered in Las Begas with a network of employees around the world</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="pageCategory-section last">
					<div class="container">
						<h5 class="title"><span class="title-red"> HIGHLIGHT</span></h5>
						<div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/s2jRKJ-ha5U" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                        </div>
					</div>
				</section>
				<section class="pageCategory-section last">
					<div class="container">
						<h5 class="title">PAST<span class="title-red"> PHOTOS</span></h5>
						<div class="row center-block past-photos hidden-xs">
							<div class="col-sm-12">
								<div class="col-sm-2 col-sm-offset-3">
									<a href="images/ufcadelaide2018v2/gallery-1.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufcadelaide2018v2/gallery-1.jpg" /></a>
								</div>
								<div class="col-sm-2">
									<a href="images/ufcadelaide2018v2/gallery-2.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufcadelaide2018v2/gallery-2.jpg" /></a>
								</div>
								<div class="col-sm-2">
									<a href="images/ufcadelaide2018v2/gallery-3.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufcadelaide2018v2/gallery-3.jpg" /></a>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="col-sm-2 col-sm-offset-4">
									<a href="images/ufcadelaide2018v2/gallery-4.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufcadelaide2018v2/gallery-4.jpg" /></a>
								</div>
								<div class="col-sm-2">
									<a href="images/ufcadelaide2018v2/gallery-5.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufcadelaide2018v2/gallery-5.jpg" /></a>
								</div>
							</div>
						</div>

						<div class="gallery text-center hidden-lg hidden-md hidden-sm">
							<div class="swiper-container">
								<div class="swiper-wrapper">
									<div class="swiper-slide">
										<a href="images/ufcadelaide2018v2/gallery-1.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufcadelaide2018v2/gallery-1.jpg" /></a>
									</div>
									<div class="swiper-slide">
										<a href="images/ufcadelaide2018v2/gallery-2.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufcadelaide2018v2/gallery-2.jpg" /></a>
									</div>
									<div class="swiper-slide">
										<a href="images/ufcadelaide2018v2/gallery-3.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufcadelaide2018v2/gallery-3.jpg" /></a>
									</div>
									<div class="swiper-slide">
										<a href="images/ufcadelaide2018v2/gallery-4.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufcadelaide2018v2/gallery-4.jpg" /></a>
									</div>
									<div class="swiper-slide">
										<a href="images/ufcadelaide2018v2/gallery-5.jpg" data-featherlight="image"><img class="img-responsive" src="images/ufcadelaide2018v2/gallery-5.jpg" /></a>
									</div>
								</div>
								<div class="swiper-pagination"></div>
								<div class="swiper-button-next swiper-button-white"></div>
								<div class="swiper-button-prev swiper-button-white"></div>
							</div>
						</div>
					</div>
				</section>
				<section class="pageCategory-section last text-white">
					<div class="container">
						<h5 class="title">FIGHT<span class="title-red"> CARD</span></h5>
						
						<!-- fight card 1 -->
						<div class="col-md-11 col-md-offset-2 fighterRow">
							<div class="col-md-11 fighterTitle">
								<h1>Heavyweight:</h1>
							</div>
							<div class="col-md-4 fighterCard">
								<div class="fighterName hidden-xs col-sm-12">
									<h1>Junior dos Santos</h1>
								</div> 
								<div class="fighterName hidden-sm hidden-md hidden-lg" style="line-height: 52px;">
									<h1>Junior dos Santos</h1>
								</div> 
								<div class="fighterDetails">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-12 col-xs-12">
											<span>19 - 5 - 0 <font style="color:#999999;">(W - L - D)</font></span>
											<p>
												Age : 34<br>
												Height: 6'4" (193 cm)<br>
												Reach: 77" <br>
												Weight : 238 lb (108kg)<br>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="fighterVs">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-6 col-xs-6">
											<i>Vs</i>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 fighterCard">
								<div class="fighterName hidden-xs col-sm-12">
									<h1>Tai Tuivasa</h1>
								</div> 
								<div class="fighterName hidden-sm hidden-md hidden-lg" style="line-height: 52px;">
									<h1>Tai Tuivasa</h1>
								</div> 
								<div class="fighterDetails">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-12 col-xs-12">
											<span>10 - 0 - 0 <font style="color:#999999;">(W - L - D)</font></span>
											<p>
												Age : 25<br>
												Height: 6'2" (187 cm)<br>
												Reach: 75" <br>
												Weight : 264 lb (119kg)<br>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- fight card 2 -->
						<div class="col-md-11 col-md-offset-2 fighterRow">
							<div class="col-md-11 fighterTitle">
								<h1>Heavyweight:</h1>
							</div>
							<div class="col-md-4 fighterCard">
								<div class="fighterName hidden-xs col-sm-12">
									<h1>Mark Hunt</h1>
								</div> 
								<div class="fighterName hidden-sm hidden-md hidden-lg" style="line-height: 52px;">
									<h1>Mark Hunt</h1>
								</div>
								<div class="fighterDetails">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-12 col-xs-12">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="fighterVs">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-6 col-xs-6">
											<i>Vs</i>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 fighterCard">
								<div class="fighterName hidden-xs col-sm-12">
									<h1>Justin Willis</h1>
								</div> 
								<div class="fighterName hidden-sm hidden-md hidden-lg" style="line-height: 52px;">
									<h1>Justin Willis</h1>
								</div>
								<div class="fighterDetails">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-12 col-xs-12">
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- fight card 3 -->
						<div class="col-md-11 col-md-offset-2 fighterRow">
							<div class="col-md-11 fighterTitle">
								<h1>Light Heavyweight:</h1>
							</div>
							<div class="col-md-4 fighterCard">
								<div class="fighterName hidden-xs col-sm-12">
									<h1>Tyson Pedro</h1>
								</div> 
								<div class="fighterName hidden-sm hidden-md hidden-lg" style="line-height: 52px;">
									<h1>Tyson Pedro</h1>
								</div> 
								<div class="fighterDetails">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-12 col-xs-12">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="fighterVs">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-6 col-xs-6">
											<i>Vs</i>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 fighterCard">
								<div class="fighterName hidden-xs col-sm-12">
									<h1>Mauricio Rua</h1>
								</div> 
								<div class="fighterName hidden-sm hidden-md hidden-lg" style="line-height: 52px;">
									<h1>Mauricio Rua</h1>
								</div> 
								<div class="fighterDetails">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-12 col-xs-12">
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- fight card 4 -->
						<div class="col-md-11 col-md-offset-2 fighterRow">
							<div class="col-md-11 fighterTitle">
								<h1>Flyweight:</h1>
							</div>
							<div class="col-md-4 fighterCard">
								<div class="fighterName hidden-xs col-sm-12">
									<h1>Wilson Reis</h1>
								</div> 
								<div class="fighterName hidden-sm hidden-md hidden-lg" style="line-height: 52px;">
									<h1>Wilson Reis</h1>
								</div>
								<div class="fighterDetails">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-12 col-xs-12">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="fighterVs">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-6 col-xs-6">
											<i>Vs</i>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 fighterCard">
								<div class="fighterName hidden-xs col-sm-12">
									<h1>Ben Nguyen</h1>
								</div> 
								<div class="fighterName hidden-sm hidden-md hidden-lg" style="line-height: 52px;">
									<h1>Ben Nguyen</h1>
								</div>
								<div class="fighterDetails">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-12 col-xs-12">
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- fight card 5 -->
						<div class="col-md-11 col-md-offset-2 fighterRow">
							<div class="col-md-11 fighterTitle">
								<h1>Light Heavyweight:</h1>
							</div>
							<div class="col-md-4 fighterCard">
								<div class="fighterName hidden-xs col-sm-12">
									<h1>Paul Craig</h1>
								</div> 
								<div class="fighterName hidden-sm hidden-md hidden-lg" style="line-height: 52px;">
									<h1>Paul Craig</h1>
								</div>
								<div class="fighterDetails">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-12 col-xs-12">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="fighterVs">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-6 col-xs-6">
											<i>Vs</i>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 fighterCard">
								<div class="fighterName hidden-xs col-sm-12">
									<h1>Jim Crute</h1>
								</div> 
								<div class="fighterName hidden-sm hidden-md hidden-lg" style="line-height: 52px;">
									<h1>Jim Crute</h1>
								</div>
								<div class="fighterDetails">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-12 col-xs-12">
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- fight card 6 -->
						<div class="col-md-11 col-md-offset-2 fighterRow">
							<div class="col-md-11 fighterTitle">
								<h1>Featherweight:</h1>
							</div>
							<div class="col-md-4 fighterCard">
								<div class="fighterName hidden-xs col-sm-12">
									<h1>Kai Kara France</h1>
								</div> 
								<div class="fighterName hidden-sm hidden-md hidden-lg" style="line-height: 52px;">
									<h1>Kai Kara France</h1>
								</div>
								<div class="fighterDetails">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-12 col-xs-12">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="fighterVs">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-6 col-xs-6">
											<i>Vs</i>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 fighterCard">
								<div class="fighterName hidden-xs col-sm-12">
									<h1>Ashkan Mokhtarian</h1>
								</div> 
								<div class="fighterName hidden-sm hidden-md hidden-lg" style="line-height: 52px;">
									<h1>Ashkan Mokhtarian</h1>
								</div>
								<div class="fighterDetails">
									<div class="col-md-12 col-xs-12">
										<div class="col-md-12 col-xs-12">
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</section>

				<section class="pageCategory-section last text-white">
					<div id="priceFixed">
					<div class="container text-center">

            	<div class="row priceNbtn2">
                	<div class="col-sm-offset-1 col-sm-10">
                    	<div class="row">
                        	<div class="col-sm-5 leftBox">
                        	</div>
                        	<div class="col-sm-3 text-center">
                            	<a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        	</div>
                    	</div>
                	</div>
            	</div>          
							</div>
					</div>
				</section>
				{{-- <section class="pageCategory-section last ptb-50" id="anchorPrice">
					<div class="container text-center">
						<h5 class="titleOpenSans">GET<span class="title-red"> TICKETS</span></h5>
						<span class="subTitle openSansRoboto"><p>Prices below include 1 x UFC 221 ticket (Perth) and 1 x Return Flight Ticket to Perth.</p></span>
						<div class="row" style="padding-bottom: 50px;">
							<div class="col-sm-offset-1 col-sm-10 ">
								<div class="col-sm-12 col-xs-12" style="margin-top: 20px; margin-bottom: 40px;">
									<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
										<div class="ticket-card text-center center-block">
											<div id="head">
												<div class="image">
													<div class="overlay-soldout"></div>
													<div class="middle">
														<div class="text-red"><strike>RM3,960.00</strike></div>
														<div class="text-grey">RM3,260.00</div>
													</div>
												</div>
											</div>
											<p>P1 Ticket + Return Flight<br>tickets from<br>Kuala Lumpur</p>
											<p style="color:red;font-size:14px;">SOLD OUT</p>
											<div class="datesFlight">Flight Dates Available:</div>
											<div class="scheduleFlight">Depart (Kuala Lumpur - Perth):<br>Fri, 09 Feb 00:05<br>Sat, 10 Feb 00:05<br><br>Return (Perth - Kuala Lumpur):<br>Mon, 12 Feb 06:05</div>
										</div>
									</div>
									<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
									  	<div class="ticket-card text-center center-block">
											<div id="head">
												<div class="image">
												<div class="overlay"></div>
												<div class="middle">
												<div class="text-red"><strike>RM3,250.00</strike></div>
													<div class="text">RM2,550.00</div>
												</div>
										  		</div>
											</div>
											<p>P2 Ticket + Return Flight<br>tickets from<br>Kuala Lumpur</p>
											<p style="color:#999999;font-size:10px;">&nbsp;</p>
											
											<div class="datesFlight">Flight Dates Available:</div>
											<div class="scheduleFlight">Depart (Kuala Lumpur - Perth):<br>Fri, 09 Feb 00:05<br>Sat, 10 Feb 00:05<br><br>Return (Perth - Kuala Lumpur):<br>Mon, 12 Feb 06:05</div>
									  	</div>
									</div>
									<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
									  	<div class="ticket-card text-center center-block">
											<div id="head">
												<div class="image">
												  	<div class="overlay"></div>
												  	<div class="middle">
														<div class="text-red"><strike>RM2,890.00</strike></div>
														<div class="text">RM2,190.00</div>
												  	</div>
												</div>
											</div>
											<p>P3 Ticket + Return Flight<br>tickets from<br>Kuala Lumpur</p>
											<p style="color:#999999;font-size:10px;">&nbsp;</p>
											<div class="datesFlight">Flight Dates Available:</div>
											<div class="scheduleFlight">Depart (Kuala Lumpur - Perth):<br>Fri, 09 Feb 00:05<br>Sat, 10 Feb 00:05<br><br>Return (Perth - Kuala Lumpur):<br>Mon, 12 Feb 06:05</div>
									  	</div>
									</div>
									<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
									  	<div class="ticket-card text-center center-block">
											<div id="head">
												<div class="image">
												  	<div class="overlay"></div>
												  	<div class="middle">
														<div class="text-red"><strike>RM2,560.00</strike></div>
														<div class="text">RM1,860.00</div>
												  	</div>
											  	</div>
											</div>
											<p>P5 Ticket + Return Flight<br>tickets from<br>Kuala Lumpur</p>
											<p style="color:#999999;font-size:10px;">&nbsp;</p>
											<div class="datesFlight">Flight Dates Available:</div>
											<div class="scheduleFlight">Depart (Kuala Lumpur - Perth):<br>Fri, 09 Feb 00:05<br>Sat, 10 Feb 00:05<br><br>Return (Perth - Kuala Lumpur):<br>Mon, 12 Feb 06:05</div>
											
									  	</div>
									</div>
								</div>
								<div class="row text-center">
									<a class="btn btn-danger disabled" id="buyButton" datetime="Feb 10 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/ufc 221, perth/events" role="button">BUY TICKETS</a>
								</div>
							</div>
						</div>
					</div>
				</section> --}}
			
			{{-- <section class="pageCategory-section section-grey last" id="getTicket">
				<div class="container tixPrice">
					<div class="row">
						<div class="col-sm-offset-1 col-sm-10 ">
							<!--  <div class="note text-left">
								<p class="reminder">Upon purchase, our staff will contact you for further<br> details to assist in making your flight booking.<br><small class="smallReminder">Prices above are all inclusive. Only snap on price would be the SGD2.50<br> transaction fee.</small></p>
							</div> -->
							<h6>Upon purchase, our staff will contact you for further details to assist in making your flight booking.</h6>
							<p><i>Prices above are all inclusive. Only snap on price would be the SGD2.50 transaction fee.</i></p>
							<!-- <div class="note text-left">
                                <h2>Notes</h2>
                                <ol>
                                    <li>Upon purchase, our staff will contact you for further details to assist in making your flight booking.</li>
                                    <li>Prices above are all inclusive. Only snap on price would be the RM2.50 transaction fee.</li>
                                </ol>
                            </div> -->
						</div>
					</div>
				</div>
			</section> --}}

		</div><!-- /Main Body -->
  
	</section><!-- /Content Section -->

@endsection

@section('customjs')
	{{-- countdown --}}
	<script src="js/jquery.time-to.js"></script>
	<script type="text/javascript">
		$('#countdown').timeTo({
		    timeTo: new Date(new Date('Thursday May 10 2018 00:00:00 GMT+0800 (+08)')),
		    displayDays: 2,
		    theme: "black",
		    displayCaptions: true,
		    fontSize: 48,
		    captionSize: 14
		}); 
	</script>
	{{-- /countdown --}}

	<script type="text/javascript">
	//Initialize Swiper
	var swiper = new Swiper('.swiper-container', {
		pagination: '.swiper-pagination',        
		paginationClickable: true,
		slidesPerView: 'auto',
		spaceBetween: 10,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		freeMode: true
	});

	// Enlarge Seat Plan Image
	$(function() {
		$('.seatPlanImg').on('click', function() {
		$('.enlargeImageModalSource').attr('src', $(this).attr('src'));
		$('#enlargeImageModal').modal('show');
		});
	});

	// Hide top Banner when page scroll
	// var header = $('.eventBanner');
	// var range = 450;

	// $(window).on('scroll', function () {
		
	// 	var scrollTop = $(this).scrollTop();
	// 	var offset = header.offset().top;
	// 	var height = header.outerHeight();
	// 	offset = offset + height;
	// 	var calc = 1 - (scrollTop - offset + range) / range;

	// 	header.css({ 'opacity': calc });

	// 	if ( calc > '1' ) {
	// 	header.css({ 'opacity': 1 });
	// 	} else if ( calc < '0' ) {
	// 	header.css({ 'opacity': 0 });
	// 	}
	// });

	// Smooth scroll for acnhor links
	$('a[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		if (target.length) {
			$('html, body').animate({
			scrollTop: target.offset().top
			}, 1000);
			return false;
		}
		}
	});

	// Modal Announcement
	//$(document).ready(function(){
    //  $("#announcementModal").modal('show');
  	//});	

	</script>
	<script type="text/javascript">
	   var deadline = 'Feb 11 2018 00:00:00 GMT+0800';
	  // calculate time remaining
	  function time_remaining(endtime){
		  var t = Date.parse(endtime) - Date.parse(new Date());
		  var seconds = Math.floor( (t/1000) % 60 );
		  var minutes = Math.floor( (t/1000/60) % 60 );
		  var hours = Math.floor( (t/(1000*60*60)) % 24 );
		  var days = Math.floor( t/(1000*60*60*24) );
		  return {'total':t, 'days':days, 'hours':hours, 'minutes':minutes, 'seconds':seconds};
	  }

	  //output to html
	  function run_clock(id,endtime){
		  var clock = document.getElementById(id);
		  
		  // get spans where our clock numbers are held
		  var days_span = clock.querySelector('.days');
		  var hours_span = clock.querySelector('.hours');
		  var minutes_span = clock.querySelector('.minutes');
		  var seconds_span = clock.querySelector('.seconds');

		  function update_clock(){
			  var t = time_remaining(endtime);
			  
			  // update the numbers in each part of the clock
			  days_span.innerHTML = t.days;
			  hours_span.innerHTML = ('0' + t.hours).slice(-2);
			  minutes_span.innerHTML = ('0' + t.minutes).slice(-2);
			  seconds_span.innerHTML = ('0' + t.seconds).slice(-2);
			  
			  if(t.total<=0){ clearInterval(timeinterval); }
		  }
		  update_clock();
		  var timeinterval = setInterval(update_clock,1000);
	  }
	  run_clock('clockdiv',deadline);
	</script>

	{{-- Buy button disable --}}
	<script type="text/javascript">
		$(function() {
			$('a[id^=buyButton]').each(function() {
				var date = new Date();
				var enddate = $(this).attr('datetime'); 
				if ( Date.parse(date) >= Date.parse(enddate)) {
				  $(this).addClass('disabled');
				}
			});
		});
	</script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
	<script type="text/javascript">
        $('.flightTab').hide();
        //show the first tab content
        $('#country-1').show();

        $('#select-country').change(function () {
           dropdown = $('#select-country').val();
          //first hide all tabs again when a new option is selected
          $('.flightTab').hide();
          //then show the tab content of whatever option value was selected
          $('#' + "country-" + dropdown).show();                                    
        });
    </script>
	
@endsection

@section('modal')
	@include('layouts.partials.modals._seatplan')
	@include('layouts.partials.modals._getTixCustom')

@foreach($ticket_hotel as $ticket)
    {{-- ticket info modal --}}
    <div id="infoModal{{ $ticket["Sorting ID"] }}" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-body text-center pd-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="row info-box">


                    <div class="infotitle">
                        <h6 class="type">{{$ticket["Ticket Tier"]}}</h6>
                        <h6 class="days">TICKET + HOTEL</h6>
                    </div>
					{{-- Desktop --}}
                    <div class="services col-lg-12 col-md-12 hidden-xs hidden-sm">
                        <div class="col-lg-6 col-md-6 item">
							<div class="details">
								<p><b>Airport</b>
								<br />- {{ $ticket["Airport"] }}</li>
								</p>
								<p><b>Public Transport</b>
								<br />- {{ $ticket["Public Transport 1"] }}
								<br />- {{ $ticket["Public Transport 2"] }}
								</p>
							</div>
                        </div> 
                        <div class="col-lg-6 col-md-6 item hidden-xs hidden-sm">
							<div class="details">
								<img src="{{ $ticket["Hotel Image"] }}" class="img-responsive"/>
							</div>
							<p>{{ $ticket["Hotel Name"] }}</p>
                        </div> 
					</div>

                    <div class="col-xs-12 col-sm-6 hidden-lg hidden-md">
                        <div class="col-xs-12">
                            <div class="col-xs-12 item">
								<div class="details">
									<p><b>Airport</b>
									<br />- {{ $ticket["Airport"] }}</li>
									</p>
									<p><b>Public Transport</b>
									<br />- {{ $ticket["Public Transport 1"] }}
									<br />- {{ $ticket["Public Transport 2"] }}
									</p>
								</div>
                            </div>
							<div class="col-xs-12 item">
								<div class="details">
									<img src="{{ $ticket["Hotel Image"] }}" class="img-responsive"/>
								</div>
								<p>{{ $ticket["Hotel Name"] }}</p>
							</div>
                        </div>
                    </div>

                    <div class="copy_text">
						*No Cancellation &amp; No Refund allowed*
                    </div>
                </div> 
            </div>
          </div>
        </div>
    </div>


@endforeach

    <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>UFC Singapore 2018 VIP Experience Contest</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
						<img src="{{asset('images/ufc2018/fb-ufc-600px.jpg')}}" style="width: 100%" class="img-responsive" alt="ONE: QUEST FOR GREATNESS"><br />
                        <p style="text-align:left;">Follow These 3 Simple Steps to Enter:<br />
						a. Participants need to fill up their name and email in the link: <a href="https://goo.gl/forms/Hrjk7qVWlu7uWFvr1">Click here</a><br />
						b. Follow RedTix’s Facebook page (www.facebook.com/RedTix) for the contest winner announcement and<br />
						c. Share the contest posting on Facebook before or by 23:59 (GMT+8) on 20 Jun 2018 (“Deadline”)<br />
						(https://www.facebook.com/254324383944/posts/10156562760418945/)</p>
                    </div>
					<div class="row text-center">
						<a class="btn btn-danger" id="buyButton" datetime="Jun 20 2018 00:00:00 GMT+0800" target="_blank" href="https://goo.gl/forms/Hrjk7qVWlu7uWFvr1" role="button">JOIN THE CONTEST NOW</a>
					</div>
                    <div class="clearfix">&nbsp;</div>
					<div class="well">
						<p style="text-align:left;">Winner will be announced on 21st Jun. Specially thanks to AirAsia Big, BigPay, Vidi, ROKKI and AirAsia. <a href="https://www.facebook.com/notes/redtix/ufc-singapore-2018-vip-experience-contest/10156562773108945/">T&amp;C</a> apply.</p>
					</div>
                </div>
            </div>
        </div>
    </div>


@endsection