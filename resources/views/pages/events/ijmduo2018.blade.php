@extends('master')
@section('title')
    IJM Duo Highway Challenge 2018 - NPE Highway [E10]
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="IJM Duo Highway Challenge 2018" />
    <meta property="og:description" content="IJM Duo Highway Challenge 2018" />
    <meta property="og:image" content="{{ Request::Url().'images/ijmduo2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/ijmduo2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="IJM Duo Highway Challenge 2018"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/ijmduo2018/thumbnail1.jpg')}}" style="width: 100%" class="img-responsive" alt="IJM Duo Highway Challenge 2018">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>IJM ALLIANZ DUO HIGHWAY CHALLENGE 2018 - NPE Highway [E10]</h6>Tickets from <span>RM55</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  29th July 2018 (Sunday)</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Sunway Pyramid / NPE Highway [E10] {{-- <a target="_blank" href="https://goo.gl/maps/5GtagxPJ36H2">View Map</a> --}}</div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 5.30am for 21KM Challenge Run</div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 6.10am for 10KM Run</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p>An annual event, The IJM Allianz Duo Highway Challenge 2018 @ NPE. Fun-filled, Safe Route on a closed highway! Come and experience yourself! NPE Challenge will be offering 21km &amp; 10km!</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/ijmduo2018/poster.jpg" data-featherlight="image"><img class="" src="images/ijmduo2018/poster.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ijmduo2018/gallery1.jpg" data-featherlight="image"><img class="" src="images/ijmduo2018/gallery1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ijmduo2018/gallery2.jpg" data-featherlight="image"><img class="" src="images/ijmduo2018/gallery2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ijmduo2018/gallery3.jpg" data-featherlight="image"><img class="" src="images/ijmduo2018/gallery3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ijmduo2018/gallery4.jpg" data-featherlight="image"><img class="" src="images/ijmduo2018/gallery4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ijmduo2018/gallery5.jpg" data-featherlight="image"><img class="" src="images/ijmduo2018/gallery5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ijmduo2018/gallery6.jpg" data-featherlight="image"><img class="" src="images/ijmduo2018/gallery6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ijmduo2018/gallery7.jpg" data-featherlight="image"><img class="" src="images/ijmduo2018/gallery7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ijmduo2018/gallery8.jpg" data-featherlight="image"><img class="" src="images/ijmduo2018/gallery8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ijmduo2018/gallery9.jpg" data-featherlight="image"><img class="" src="images/ijmduo2018/gallery9.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/ijmduo2018/web-banner1.jpg" data-featherlight="image"><img class="" src="images/ijmduo2018/web-banner1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>ENTITLEMENTS</strong></h1>
                                {{-- <p>Select ticket</p> --}}
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>21 KM</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img class="image-responsive" src="images/ijmduo2018/npe-fb-ad-1.jpg" style="width:100%; height:auto;" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>10 KM</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img class="image-responsive" src="images/ijmduo2018/npe-fb-ad-2.jpg" style="width:100%; height:auto;" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Special Package/th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img class="image-responsive" src="images/ijmduo2018/npe-fb-ad-3.jpg" style="width:100%; height:auto;" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Race Pack</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img class="image-responsive" src="images/ijmduo2018/race-pack.jpg" style="width:100%; height:auto;" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Tee Measurement</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img class="image-responsive" src="images/ijmduo2018/tee-measure-size.jpg" style="width:100%; height:auto;" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <hr>
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="note text-left">
                                <h2>REMINDER</h2>
                                <ul>
                                    <li>21KM are open to Malaysian, PR Citizenship &amp; International</li>
                                    <li>10KM Run are only applicable for Malaysian &amp; PR Citizenship Only</li>
                                    <li>Closing Date for NPE Highway (E10): 30th June 2018</li>
                                </ul>
                                <h2>CUT OFF TIME</h2>
                                <ul>
                                    <li>21km : @15km mark 2hr 30min.</li>
                                    <li>21km mark 3hr 50min.</li>
                                </ul>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Age</th>
                                            <th>Fees</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>21KM Men’s (Closed Category)</td>
                                            <td>Age 16 – 39</td>
                                            <td rowspan="6">RM90.00</td>
                                        </tr>
                                        <tr>
                                            <td>21KM Men’s Veteran (Closed Category)</td>
                                            <td>Age 40 &amp; above</td>
                                        </tr>
                                        <tr>
                                            <td>21KM Women’s (Closed Category)</td>
                                            <td>Age 16 – 39</td>
                                        </tr>
                                        <tr>
                                            <td>21KM Women’s Veteran (Closed Category)</td>
                                            <td>Age 40 &amp; above</td>
                                        </tr>

                                        <tr>
                                            <td>21KM International Men’s (Open Category)</td>
                                            <td>Age 16 &amp; above</td>
                                        </tr>
                                        <tr>
                                            <td>21KM International Women’s (Open Category)</td>
                                            <td>Age 16 &amp; above</td>
                                        </tr>
                                        <tr>
                                            <td>10KM Men’s (Closed Category)</td>
                                            <td>Age 16 – 39</td>
                                            <td rowspan="4">RM55.00</td>
                                        </tr>
                                        <tr>
                                            <td>10KM Men’s Veteran (Closed Category)</td>
                                            <td>Age 40 &amp; above</td>
                                        </tr>
                                        <tr>
                                            <td>10KM Women’s (Closed Category)</td>
                                            <td>Age 16 – 39</td>
                                        </tr>
                                        <tr>
                                            <td>10KM Women’s Veteran (Closed Category)</td>
                                            <td>Age 40 &amp; above</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                             <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="June 30 2018 00:00:00 GMT+0800" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/ijm allianz duo highway challenge 2018 - npe highway [e10]/events" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            {{-- <span class="importantNote">*Additional charges will be applied for credit/debit card transaction, optionally customer may pay cash for outlet purchase to avoid the charges.</span> --}}
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown include RM4.00 AirAsiaRedTix fee & other taxes.</li> --}}
                                    <li>Prices shown include RM4.00 AirAsiaRedTix fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM30.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <!-- <li>Strictly no replacement for missing tickets, torn tickets and cancellation.</li> -->
                                    <li>Online ticket selling will close on 30th June 2018.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection