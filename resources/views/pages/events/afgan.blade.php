@extends('master')
@section('title')
    AFGAN SIDES LIVE IN MIRI 2016
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/afgan/afgan_event.jpg')">
        {{-- <video id="video-background" preload muted autoplay loop>
          <source src="images/neon/top-banner2000x450.mp4" type="video/mp4">
        </video> --}}
      </div>
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>AFGAN SIDES LIVE IN MIRI 2016</h6>
                  Tickets from <span>RM310</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 24 December 2016</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Stadium Bandaraya Miri, Sarawak <a target="_blank" href="https://www.google.com/maps/place/Stadium+Bandaraya+Miri/@4.4042565,113.9952272,17.5z/data=!4m5!3m4!1s0x321f4f2f51b31cb3:0x508d429bdd73c390!8m2!3d4.4044532!4d113.9960469">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8 pm</div>
                <hr>
                <p>Indonesian sensation Afgansyah Reza, better known as AFGAN, makes his way back to Malaysia for his most anticipated concert. AFGAN “SIDES” Live in Miri, Sarawak Malaysia which will be held at Stadium Bandaraya Miri on Saturday, 24th December at 8:00pm. Afgan’s Miri, Sarawak stop will be the last stop destination for his Asian Tour, which includes Indonesia, Malaysia and Singapore and will showcase his journey as an artist from his first hit song to his latest single Kunci Hati.</p>
                <p>This closing performance in Miri will be the first of its kind in the region of Borneo and will be a strategic location to hold this event as it caters to Afgan’s fans primarily from Borneo which include his die hard fans from Brunei. This neighbouring country which is situated just 90 minutes away via ground transportation. All of his concerts held were all “SOLD OUT”.</p>
                <p>Afganisme, what his fans are called, doesn’t necessarily come from his homeland but as well as Brunei, Singapore and of course Malaysia ranging from fans of all ages. Afgan burst into the music scene in 2008 with his first single Terima Kasih Cinta, which became an instant megahit not in Indonesia, but also in the region. Since then, Afgan was destined for regional superstardom.</p>
                <p>Organized by Orpheus Events, this one night only Grand spectacular show is in support of his album SIDES, his first album in 3 years. SIDES features more mature and diverse sounds compared to his older works. In this latest offering, AFGAN sticks to his R&B roots but fuses elements of blues, electronic and orchestra, where he gets to do more than love songs and uncover different parts of himself.</p>
                <p>He recently released his 5th solo album SIDES, a musical escape into world of romance, imagination and magic, and the inspiration for his three-nation concert tour.</p>
                <p><strong>This concert is brought to you by Orpheus Events.</strong></p>
                <p class="tile">With a social media following of more than 10 million followers, this multitalented singer has also garnered 44 music awards in Indonesia and internationally during his eight years as a recording artist. His awards include the prestigious Planet Music Award (3-time winner in 2009 and 2015), Best Asian Artist at the Asia Song Festival in 2014 and also the World Music Awards in 2014. He was once nominated at Anugerah Industri Muzik (AIM), Malaysia’s equivalent to the Grammys.</p>
                <iframe width="100%" height="400" src="https://www.youtube.com/embed/JjPyTj1p4Os?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            {{-- <div class="lineup text-center">
                <h1 class="subSecTitle"><strong>THE LINEUP</strong></h1>
                <p>6 out of 20 performers that will be performed</p>
                <ul class="list-unstyled list-inline lineupList">
                    <li><img src="images/profile.png" alt="">Fifth Harmony</li>
                    <li><img src="images/profile.png" alt="">Adelle</li>
                    <li><img src="images/profile.png" alt="">Coldplay</li>
                    <li><img src="images/profile.png" alt="">Frank Ocean</li>
                    <li><img src="images/profile.png" alt="">The Fray</li>
                    <li><img src="images/profile.png" alt="">Taylor Swift</li>
                </ul>
            </div> --}}
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/afgan/afgan-poster.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/afgan/afgan_event.jpg" alt="">
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Normal<br><br>Ticket Price in Ringgit Malaysia (RM)</th>
                          <th>Normal<br><br>Ticket Price in Brunei Dollar (BND)</th>
                          <th>Remarks</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                            <td><span class="box yellow" data-toggle="collapse" data-target="#vvip">Diamond VIP</span></td>
                            <td>RM842</td>
                            <td>BND280</td>
                            <td>Meet & Greet package</td>
                            <td rowspan="11"><img class="img-responsive seatPlanImg" src="images/afgan/afgan_seating.png" alt=""></td>
                          </tr>
                            <td><span class="box green">Diamond</span></td>
                            <td>RM629</td>
                            <td>BND208</td>
                            <td>Numbered Seating</td>
                          </tr>
                          <tr>
                            <td><span class="box purple">Platinum</span></td>
                            <td>RM522</td>
                            <td>BND173</td>
                            <td>Numbered Seating</td>
                          </tr>
                          <tr>
                            <td><span class="box blue">Gold</span></td>
                            <td>RM416</td>
                            <td>BND138</td>
                            <td>Numbered Seating</td>
                          </tr>
                          <tr>
                            <td><span class="box red">Silver</span></td>
                            <td>RM310</td>
                            <td>BND103</td>
                            <td>Sold Out</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/events/afgan%20sides%20live%20in%20miri%202016/2016-12-24_20.00/miri%20indoor%20stadium?hallmap" role="button">BUY TICKETS</a>
                  <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>
                <p><strong>For Arena Seating (category Diamond VIP & Diamond) best seat starts with small seat numbering. Example: A/01, B/01, C/01</strong></p>
                <span class="importantNote">* Ticket prices INCLUSIVE of RM3 ticketing fee, agent fees, credit card charges and 6% GST. </span>
                <div class="note text-left">
                  <h2>Important Notes</h2>
                  <ol>
                    <li>Prices shown inclusive RM3.00 AirAsiaRedTix fee & 6% GST.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                  </ol>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="pageCategory-section last">
          <div class="buyNowSec" style="background-image: url('images/event2.jpg')">
            <div class="bannerOverlay"></div>
            <div class="container">
              <div class="col-sm-offset-1 col-sm-10 quoteArea">
                <h1>You’ve heard him on the radio, you’ve seen him on TV. It’s about time. See him live!</h1>
              </div>
              <div class="btnArea">
                <a class="btn btn-black btn-lg" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/events/afgan%20sides%20live%20in%20miri%202016/2016-12-24_20.00/miri%20indoor%20stadium?hallmap" role="button">GET TICKET ONLINE NOW <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <div class="clearfix">&nbsp;</div>
                <span data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets</span>
              </div>
            </div>
          </div>
        </section>

        {{-- <section class="pageCategory-section last">
          <div class="container">
            <div class="partner text-center">
                <h1 class="subSecTitle"><strong>PARTNERS</strong></h1>
                <div class="clearfix">&nbsp;</div>
                <ul class="list-unstyled list-inline partnerList">
                    <li><img src="images/neon/aabig_logo (offical partner).png" alt=""></li>
                    <li><img src="images/neon/BAC.png" alt=""></li>
                    <li><img src="images/neon/durex (official partner).png" alt=""></li>
                    <li><img src="images/neon/fac3 (co-organiser).jpg" alt=""></li>
                    <li><img src="images/neon/seven friday (official watch).jpeg" alt=""></li>
                    <li><img src="images/neon/sunway lagoon (official venue).png" alt=""></li>
                </ul>
            </div>
          </div>
        </section> --}}

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                <dd>The Curve (TEL: 03 - 7733 1139)</dd>
                <dd>Bangsar Village (TEL: 03-22021139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
                <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Istana Budaya Jalan<br>Tun Razak:</dt>
                <dd>03 40265558</dd>
            </dl>
            </div>
        </div>
        </div>
    </div>

@endsection