@extends('master')
@section('title')
    K Fun Stage – So Fresh
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/kfunstage/kfunstage-banner.png')">
      </div>  
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>K Fun Stage – So Fresh</h6>
                  Tickets from <span>RM142</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 15 July 2017 (Saturday)</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Surf Beach, Sunway Lagoon<a target="_blank" href="https://goo.gl/maps/Ge2Fg3AMY4C2"> View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8.00 PM</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <p>Let's hit one of the Best Kpop Concert in Malaysia together with HYUNA, JAYPARK and TRIPLE H, which also the first ever concert after Triple H debuted!</p>
                </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="col-sm-12 lineup text-center">
                <h1 class="subSecTitle"><strong>THE LINEUP</strong></h1>
                    <div class="row justify-content-md-center">
                       
                      <div class="col-sm-12 text-center" style="float:left">
                                                                                              
                        <div class="col-sm-3">
                          <div class="row"><img src="images/kfunstage/hyuna.png" alt=""></div>
                          <div class="row"><strong>HYUNA</strong></div>
                          <div class="row">Former member of Wonder Girls &amp; 4 Minute. Released her first extended play Bubble Pop! in 2011. Appeared in PSY’s worldwide hit "Gangnam Style", and now she forms a band with the trio Triple H.</div>
                          <div class="row clearfix"></div>                          
                        </div>
                       <div class="col-sm-1 clearfix"></div>                                                 
                        <div class="col-sm-4">
                          <div class="row"><img src="images/kfunstage/jaypark.png" alt=""></div>
                          <div class="row"><strong>JAYPARK</strong></div>
                          <div class="row">Former member of 2PM. Founded the independent hip hop label - AOMG. He is also a member of a Seattle-based b-boy crew Art of Movement (AOM). </div>      
                          <div class="row clearfix"></div>                                              
                        </div>
                       <div class="col-sm-1 clearfix"></div>                                                   
                        <div class="col-sm-4">
                          <div class="row"><img src="images/kfunstage/triple-h.png" alt=""></div>
                          <div class="row"><strong>TRIPLE-H</strong></div>
                          <div class="row">A band composed of HyunA and Pentagon members Hui and E'Dawn. Hui used to be a JYP Trainee &amp; E'Dawn took part in HyunA's "Roll Deep" Performance stages</div>                         
                          <div class="row clearfix"></div>                          
                        </div>
                        
                      </div>
                                          

                    </div>
                    <div class="row">
                      <div class="col-sm-10 clearfix"></div>
                    </div>
                    <div class="row justify-content-md-center">
                       <div class="col-sm-1 clearfix"></div>
                       <div class="col-sm-10">
                       <div class="col-sm-2 clearfix"></div>                                                 
                        <div class="col-sm-4">
                          <div class="row"><img src="images/kfunstage/vincent.png" alt=""></div>
                          <div class="row"><strong>VINCENT</strong></div>
                          <div class="row clearfix"></div>              
                        </div>
                       <div class="col-sm-1 clearfix"></div>  
                       <div class="col-sm-4">
                          <div class="row"><img src="images/kfunstage/dj-wegun.png" alt=""></div>
                          <div class="row"><strong>DJ-WEGUN</strong></div>
                          <div class="row"></div>
                          <div class="row clearfix"></div>                                               
                        </div>
                       <div class="col-sm-1 clearfix"></div>    
                    </div>
            </div>

            {{-- <video style="display:block; margin: 30px auto;" width="60%" height="400" controls>
              <source src="images/kfunstage/kfun-vid.mp4" type="video/mp4">Your browser does not support the video tag.
            </video> --}}

            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/kfunstage/kfunstage-poster.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/kfunstage/kfunstage-img.jpg" alt="">
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Ticket Category</th>
                          <th>Price</th>
                          <th>Remarks</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td><span class="box midblue">Rock Zone</td>
                              <td>RM 342</td>
                              <td>
                                - Free limited edition Light Stick<br>
                                - Free limited edition T-shirt<br>
                                - Chances to win a photo op with one of your idol<br>
                            </td>
                              <td rowspan="3"><img class="img-responsive seatPlanImg" src="images/kfunstage/kfunstage-plan.jpg" alt=""></td>
                          </tr>
                          <tr>
                              <td><span class="box red">Cat 1</td>
                              <td>RM 242</td>
                              <td>Free limited edition Light Stick</td>
                          </tr>
                          <tr>
                              <td><span class="box yellow">Cat 2</td>
                              <td>RM 142</td>
                              <td>Normal</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger" id="buyButton" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/k fun stage - so fresh/events" role="button">BUY TICKETS</a>
                  <span class="or">/</span>                 
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>                  
                <span class="importantNote">* Additional charges will be applied for credit/debit card transaction, optionally customer may pay cash for outlet purchase to avoid the charges.</span>
                <span class="importantNote">* Prices stated are inclusive RM4.00 ticketing fee.</span>
                <div class="note text-left">
                  <h2>Important Notes</h2>
                  <ol>
                    <li>Prices shown inclusive RM4.00 AirAsiaRedTix fee.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                    <li>Flash photography is not allowed​​.</li>
                  </ol>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('modal')

  <!--Modal Seat Plan image-->
  <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <div class="clearfix">&nbsp;</div>
          <img src="" class="enlargeImageModalSource" style="width: 100%;">
          </div>
      </div>
      </div>
  </div>
  
  <!-- Modal Where to Get Tix Location-->
  <div id="modalGetTixLoc" class="modal fade" role="dialog">
      <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
          <h2>GET YOUR TICKETS FROM:</h2>
          <dl class="dl-horizontal">
              <dt>Online:</dt>
              <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Rock Corner outlets:</dt>
              <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
              <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
              <dd>Bangsar Village (TEL: 03-22021139)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Victoria Music outlets:</dt>
              <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
              <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
          </dl>
          <dl class="dl-horizontal">
              <dt>Penang outlets:</dt>
              <dd>Artist Gallery, Gurney Plaza (04-228 5648)</dd>
              <dd>Artist Gallery, Queensbay Mall (04-637 0191)</dd>
          </dl>
          <!--<dl class="dl-horizontal">
              <dt>Istana Budaya Jalan<br>Tun Razak:</dt>
              <dd>03 40265558</dd>-->
          </dl>
          </div>
      </div>
      </div>
  </div>

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });

    // Modal Announcement
    $(document).ready(function(){
      $("#announcementModal").modal('show');
    });
    </script>
    
@endsection