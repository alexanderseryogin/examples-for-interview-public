@extends('master')
@section('title')
    Teater Muzikal Dia Semanis Honey
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/semanishoney/teater_muzikal_dia_semanis_honey_event.jpg')">
        {{-- <video id="video-background" preload muted autoplay loop>
          <source src="images/neon/top-banner2000x450.mp4" type="video/mp4">
        </video> --}}
      </div>
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>Teater Muzikal Dia Semanis Honey</h6>
                  Tickets from <span>RM48.75</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 3th - 9th January 2017</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Panggung Sari, Istana Budaya <a target="_blank" href="https://www.google.com/maps/place/National+Theatre+(Istana+Budaya)/@3.1743733,101.7015273,17z/data=!3m1!4b1!4m5!3m4!1s0x31cc481ef641f96b:0xfe0d3f27f31db1ce!8m2!3d3.1743733!4d101.703716?q=Panggung+Sari,+Istana+Budaya&um=1&ie=UTF-8&sa=X&ved=0ahUKEwi4v-nU54bRAhUBro8KHdevC8UQ_AUICCgB">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:30 pm</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <p>Sebuah kisah cinta antara Ahnaf Wafiy seorang C.E.O. di Syarikat Blue Corporation dengan setiausahanya, Hani Safira yang penuh dengan babak-babak romantis dan komedi serta drama yang menyentuh perasaan. Ahnaf Wafiy yang merantau ke luar negara oleh kerana kecewa dengan kekasihnya, Ariana, kini kembali ke tanah air dan telah berkenalan dengan setiausaha syarikatnya iaitu Hani Safira.</p>
                <p>Perhubungan mereka yang dipenuhi dengan pertengkaran, usik- mengusik dan sakat-menyakat telah membawa kepada sebuah rasa cinta yang manis. Percintaan ini juga membawa mereka kepada ikatan pertunangan dan sekali gus menanti hari bahagia ke jinjang pelamin. Pertemuan Ahnaf dan Ariana, bekas kekasihnya , secara tidak sengaja telah membuatkan Ahnaf membatalkan penikahannya bersama Hani pada hari nikah mereka. Ahnaf dan Ariana berpindah ke luar negara meninggalkan keluarga dan juga Hani yang sangat kecewa.</p>
                <p>Setelah tiga tahun berlalu, Ahnaf pulang ke tanah air semula dan kembali menagih cinta Hani yang saat itu sudah bertunang dengan Adam. Ikatan pertunangan Hani dan Adam mula goyah oleh kehadiran Ahnaf. Bella yang menyimpan perasaan kepada Ahnaf yang sudah duda ketika itu membuat perancangan bagi menghalangi cinta Ahnaf dan Hani bersatu.</p>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="lineup text-center">
                <h1 class="subSecTitle"><strong>THE LINEUP</strong></h1>
                <p>Performers that will be performed</p>
                <ul class="list-unstyled list-inline lineupList">
                    <li><img src="images/semanishoney/Siti-Nordiana.jpg" alt="">Siti Nordiana</li>
                    <li><img src="images/semanishoney/Alif-Satar.jpg" alt="">Alif Satar</li>
                    <li><img src="images/semanishoney/Nadya-Syahera.jpg" alt="">Nadya Syahera</li>
                    <li><img src="images/semanishoney/Nina-Juren.jpg" alt="">Datin Nina Juren</li>
                    <li><img src="images/semanishoney/Dafi.jpg" alt="">Dafi</li>
                    <li><img src="images/semanishoney/Aleza-Shadan.jpg" alt="">Aleza Shadan</li>
                </ul>
            </div>
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/semanishoney/poster.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/semanishoney/teater_muzikal_dia_semanis_honey_event.jpg" alt="">
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Normal Price</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td rowspan="5"><span class="box grey">Stalls</span></td>
                              <td>RM 197.15</td>
                              <td rowspan="11"><img class="img-responsive seatPlanImg" src="images/semanishoney/seat.jpg" alt=""></td>
                          </tr>
                          <tr>
                              <td>RM 170.65</td>
                          </tr>
                          <tr>
                              <td>RM 128.25</td>
                          </tr>
                          <tr>
                              <td>RM 120.85</td>
                          </tr>
                          <tr>
                              <td>RM 110.25</td>
                          </tr>
                          <tr>
                              <td><span class="box grey">Gold Seats</span></td>
                              <td>N/A</td>
                          </tr>
                          <tr>
                              <td rowspan="2"><span class="box grey">Grand Circle</span></td>
                              <td>RM 99.65</td>
                          </tr>
                          <tr>
                              <td>RM 75.25</td>
                          </tr>
                          <tr>
                              <td rowspan="2"><span class="box grey">Upper Circle</span></td>
                              <td>RM 64.65</td>
                          </tr>
                          <tr>
                              <td>RM 48.75</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/teater%20muzikal%20dia%20semanis%20honey/events" role="button">BUY TICKETS</a>
                  <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>
                <span class="importantNote">*Additional charges will be applied for credit/debit card transaction, optionally customer may pay cash for outlet purchase to avoid the charges.</span>
                <div class="note text-left">
                  <h2>Reminder</h2>
                  <p><strong>Dress code</strong><br>Neat and decent. Worn out / faded / shabby jeans, t-shirt without collar, shorts, slippers, and sandals are STRICTLY NOT ALLOWED.</p>
                  <p><strong>Minimum age</strong><br>6 years & above</p>
                  <p>Food and drinks are not allowed in the auditorium.</p>
                  <h2>Important Notes</h2>
                  <ol>
                    <li>Prices shown inclusive RM3.00 AirAsiaRedTix fee & 6% GST.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                  </ol>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="pageCategory-section last">
          <div class="buyNowSec" style="background-image: url('images/event2.jpg')">
            <div class="bannerOverlay"></div>
            <div class="container">
              <div class="col-sm-offset-1 col-sm-10 quoteArea">
                <h1>You’ve heard him on the radio, you’ve seen him on TV. It’s about time. See him live!</h1>
              </div>
              <div class="btnArea">
                <a class="btn btn-black btn-lg" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/teater%20muzikal%20dia%20semanis%20honey/events" role="button">GET TICKET ONLINE NOW <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <div class="clearfix">&nbsp;</div>
                <span data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets</span>
              </div>
            </div>
          </div>
        </section>

        {{-- <section class="pageCategory-section last">
          <div class="container">
            <div class="partner text-center">
                <h1 class="subSecTitle"><strong>PARTNERS</strong></h1>
                <div class="clearfix">&nbsp;</div>
                <ul class="list-unstyled list-inline partnerList">
                    <li><img src="images/neon/aabig_logo (offical partner).png" alt=""></li>
                    <li><img src="images/neon/BAC.png" alt=""></li>
                    <li><img src="images/neon/durex (official partner).png" alt=""></li>
                    <li><img src="images/neon/fac3 (co-organiser).jpg" alt=""></li>
                    <li><img src="images/neon/seven friday (official watch).jpeg" alt=""></li>
                    <li><img src="images/neon/sunway lagoon (official venue).png" alt=""></li>
                </ul>
            </div>
          </div>
        </section> --}}

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                <dd>The Curve (TEL: 03 - 7733 1139)</dd>
                <dd>Bangsar Village (TEL: 03-22021139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
                <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Istana Budaya Jalan<br>Tun Razak:</dt>
                <dd>03 40265558</dd>
            </dl>
            </div>
        </div>
        </div>
    </div>

@endsection