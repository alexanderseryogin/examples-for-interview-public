@extends('master')
@section('title')
    RETRO RAHMAN 2.0 - Relive the Magic
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="RETRO RAHMAN 2.0 - Relive the Magic" />
    <meta property="og:description" content="Jason Mraz first performed in Malaysia in 2009, followed by Tour Is A Four Letter Word in 2012."/>
    <meta property="og:image" content="{{ Request::Url().'images/retrorahman2019/thumbnail.jpg' }}" />

    <style type="text/css">
    .blink {
        -webkit-animation: blink 1s step-end infinite;
            animation: blink 1s step-end infinite;
    }
    @-webkit-keyframes blink { 50% { visibility: hidden; }}
        @keyframes blink { 50% { visibility: hidden; }}
    </style>
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/retrorahman2019/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="RETRO RAHMAN 2.0 - Relive the Magic"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/retrorahman2019/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="RETRO RAHMAN 2.0 - Relive the Magic">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>RETRO RAHMAN 2.0 - Relive the Magic</h6> Tickets from <span>RM60</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 3rd Aug 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  StarXpo Centre KWC, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/hR5fB5Gizxz5S44Q6">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Saturday 3 Aug (7.30pm - 11.30pm)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <p>
                                    <h2>RETRO RAHMAN 2.0 - Relive the Magic</h2><br/>
                                    Mojo Projects is back with Retro Rahman 2.0 – Relive the Magic
                                </p>                            
                                <p>
                                    Mojo Projects presents Retro Rahman 2.0, the second tribute concert to maestro A.R. Rahman. Reliving the magic after a sold-out night last year, expect a fresh set of stars to entertain you, including two legends that were here previously.
                                </p>
                                <p>
                                    Happening on August 3rd at Star Xpo Centre Kuala Lumpur, the evening will see veterans Mano and Srinivas who are currently on Sun TV’s SaReGaMa. Joining them will be Sadhana Sargam (of Alaipayuthey fame), Harini and Sathyaprakash.                                    
                                </p>
                                <p>
                                    Rahman, known as the game changer of his generation, introduced a new genre of music with catchy, melodious, sing-along tunes in the 90s. Songs like Urvasi Urvasi, Puthu Vellai Mazhai, Chinna Chinna Asai, Ennavale Adi Ennavale, and Hello Doctor became instant hits back in the day.
                                </p>
                                <p>
                                    This year’s line-up will be enhanced by the musical direction of pianist and stellar entertainer, Anil Srinivasan, together with the energetic Mani & Band ensemble (of Vijay TV Airtel Super Singer talent hunt show fame) featuring Nikhil Ram on flutes.
                                </p>
                                <p>
                                    Mano’s relationship with Rahman began in 1994 when he was approached by the maestro to sing Mukkabla for the film Kadhalan.  A professional vocal modulator, he hit the ball out of the park with Aathangara Marame for Kizhakku Seemaiyile, a tune that is forever embedded in the minds of Rahman fans.
                                </p>
                                <p>
                                   Srinivas on the other hand, gained recognition with Maana Madurai from Rahman’s hit musical direction for Minsaara Kanavu, followed by En Uyire from Mani Ratnam's Uyire (the Tamil version of Dil Se). These Rahman compositions catapulted Srinivas into fame. 
                                </p>
                                <p>
                                    With a career spanning over three decades, Sadhana Sargam is one of the very few Indian singers who could surpass Rahman’s expectations with her renditions of his songs. Her most critically acclaimed songs were in the film Water, composed by Rahman, in which she sang three songs: Aayo Re Sakhi, Piya Ho and Naina Neer. Sargam has recorded songs for Rahman in Tamil, Hindi and Telugu for Jeans, Minsara Kanavu, Ratchagan, Alaipayuthey, Kandukonden Kandukonden, 1947 Earth, Thenali, New, Anbe Aaruyire, Boys, Saathiya, Parthale Paravasam, Alli Arjuna, Udhaya, Naani, Warriors of Heaven and Earth, Enthiran, and Kochadaiiyaan.
                                </p>
                                <p>
                                    Harini was approached by Rahman to sing Nila Kaigirathu in the award-winning film Indira at the age of 15. She’s sung more than 2,000 songs in Tamil, including Telephone Manipol from Indian and Hello Mr. Edhirkatchi from Iruvar.
                                </p>
                                <p>
                                    Retro Rahman 2.0, as it's been called this year, will also see the youthful exuberance of Sathyaprakash. His contribution to award winning songs such as Nallai Allai for the movie Kaatru Veliyidai and Aalaporan Thamizhan for the movie Mersal has ensured his steady growth, making waves in the process with his mellifluous voice.
                                </p>
                                <p>
                                    Catch them all at the indoor Star Xpo Centre, from 7.30pm till midnight on August 3. This year, Mojo Projects promises a different set of hits by the musical wizard. Truly a night not to be missed by Rahmaniacs and music lovers alike. Get your tickets now from AirAsiaRedtix.com or call 012-2000505.
                                </p>
                                <p>For more details, visit fb.com/mymojoprojects or www.mojoprojects.asia</p>

                            </div>
                            {{-- <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/user/spotify/playlist/37i9dQZF1DX1N0BBPf4rGP" width="300" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div> --}}
                        </div>
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ym_DBtkEkwk?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {{-- <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/retrorahman2019/gallery-1.jpg" data-featherlight="image"><img class="" src="images/retrorahman2019/gallery-1.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section> --}}

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Early Bird</th>
                                            <th>Normal</th>
                                            <th>Seat Plan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="2">VIP</td>
                                            <td>RM 400</td>
                                            <td>RM 500</td>
                                            <td rowspan="10"><img class="img-responsive seatPlanImg" src="images/retrorahman2019/seat-plan.jpg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td>
                                            <ul style="text-align:left;font-size:11px;">
                                                    <li>Free Seating</li>
                                                    <li>F&B voucher worth RM50</li>
                                                    <li>Access to Rock Zone</li>
                                                    <li style="font-weight: bold; font-size:12px;">First 200 ticket buyers will be invited to a special event before the concert to meet the artistes</li>
                                                    <li>Free exclusive Mojo T-shirt for the first 200 buyers</li>
                                                </ul>
                                                <font style="color:red;font-size:12px;" class="blink">Limited Seats</font></td>
                                            </td>
                                            <td>
                                                <ul style="text-align:left;font-size:11px;">
                                                    <li>Free Seating</li>
                                                    <li>F&B voucher worth RM50</li>
                                                    <li>Access to Rock Zone</li>
                                                </ul>
                                            </td>
                                        </tr>

                                        <tr style="background-color:#FBF2E5;">
                                            <td rowspan="2">Zone A</td>
                                            <td>RM 290</td>
                                            <td>RM 390</td>
                                        </tr>
                                        <tr style="background-color:#FBF2E5;">
                                            <td>
                                            <ul style="text-align:left;font-size:11px;">
                                                    <li>Free Seating</li>
                                                    <li>F&B voucher worth RM25</li>
                                                    <li>Access to Rock Zone</li>
                                                    <li>Free exclusive Mojo T-shirt for the first 200 buyers</li>
                                                </ul>
                                                <font style="color:red;font-size:12px;" class="blink">Limited Seats</font></td>
                                            </td>
                                            <td>
                                                <ul style="text-align:left;font-size:11px;">
                                                    <li>Free Seating</li>
                                                    <li>F&B voucher worth RM25</li>
                                                    <li>Access to Rock Zone</li>                                                    
                                                </ul>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td rowspan="2">Zone B</td>
                                            <td>RM 190</td>
                                            <td>RM 290</td>
                                        </tr>
                                        <tr>
                                            <td>
                                            <ul style="text-align:left;font-size:11px;">
                                                    <li>Free Seating</li>
                                                    <li>Free exclusive Mojo T-shirt for the first 200 buyers</li>
                                                    <font style="color:red;font-size:12px;" class="blink">Limited Seats</font></td>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul style="text-align:left;font-size:11px;">
                                                    <li>Free Seating</li>
                                                </ul>
                                            </td>
                                        </tr>

                                        <tr style="background-color:#FBF2E5;">
                                            <td rowspan="2">Rock Zone</td>
                                            <td>RM 160</td>
                                            <td>RM 260</td> 
                                        </tr>
                                        <tr style="background-color:#FBF2E5;">
                                            <td>
                                                <ul style="text-align:left;font-size:11px;">
                                                    <li>Free Standing</li>                                                    
                                                    <li>Free exclusive Mojo T-shirt for the first 200 buyers</li>
                                                </ul>
                                                <font style="color:red;font-size:12px;" class="blink">Limited Seats</font></td>
                                            </td>
                                            <td>
                                                <ul style="text-align:left;font-size:11px;">
                                                    <li>Free Standing</li>
                                                </ul>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td rowspan="2">Zone C</td>
                                            <td>RM 60</td>
                                            <td>RM 160</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul style="text-align:left;font-size:11px;">
                                                    <li>Free Seating</li>
                                                </ul>
                                                <font style="color:red;font-size:12px;" class="blink">Limited Seats</font></td>
                                            </td>
                                            <td>
                                                <ul style="text-align:left;font-size:11px;">
                                                    <li>Free Seating</li>
                                                </ul>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Aug 3 2019 00:00:00 GMT+0800" target="_blank" href="https://tickets.airasiaredtix.com/airasia-redtix/retrorahman2019/booking" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude of RM4.00 ticket fee.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });


    // Modal Announcement
	//$(document).ready(function(){
    //  $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. Special Limited offer at AirAsia RedTix only</p>
                        <p>2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection