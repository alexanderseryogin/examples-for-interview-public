@extends('master')
@section('title')
    Malaysian Independent Live Fusion Festival
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/milff/milff-banner.jpg')">
      </div>
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>Malaysian Independent Live Fusion Festival</h6>
                  Tickets from <span>RM146.05</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 9th September 2017, (Saturday)</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> StarXpo Centre KWC Fashion Mall, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/WFpW4zA2sKu">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 6.00 PM</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <p>Mojo Projects is back with A Night of Musical Mayhem ! </p>
                <p>Malaysian Independent Live Fusion Festival [MILFF2017]</p>
                <p>We are back again this year for #MILFF2017 – a Bigger and Better music festival, 5 bands with artistes such as:</p>
                <ul>
                    <li>SID SRIRAM & BAND featuring Keba Jeremiah and Leon James.</li>
                    <li>VIJAY PRAKASH, an award-winning playback singer, with ARB band</li>
                    <li>AALAAP RAJU with the Aalaap Raju Band, playback singer and bass guitarist</li>
                    <li>AGAM, a renowned Carnatic – Progressive Rock band lead by Harish Sivaramakrishnan</li>
                    <li>SOORAJ SANTHOSH featuring JATAYU – a semi classical pop rock band.</li>
                </ul>
                <p>They will be performing at the indoor StarXpo Centre, Kuala Lumpur – they will set a new benchmark for the Malaysian Indian entertainment scene, with a night of musical mayhem! The festival starts at 6pm so that we have enough time for all the superstars to perform and end by 12am.</p>
                <p>Don’t miss out the opportunity to see Sid Sriram belt out his mega hits live, such as Thalli Pogathey or the soothing Yennai Matrum Kadhale with his unique band comprising of superb musicians such as Keba Jeremiah and music director Leon James on keyboards. </p>
                <p>For those of you who are fans of Vijay Prakash, this is one of his rare appearances with a full-fledged band. In 2010, he became a regular singer under composer A. R. Rahman, as he performed several of his numbers such as "Hosanna" (Vinnaithaandi Varuvaaya, Ye Maaya Chesave), "Beera Beera, Veera Veera (in Tamil & Telugu Versions)" (Raavan & Raavanan), "Powerstar" (Komaram Puli) and "Kadhal Anukkal" (Enthiran). Prakash has debuted as a music composer for the 2013 Kannada film Andar Bahar starring Shivrajkumar. He also won the Vijay Music Awards for the Best Male Playback for the song "Hosanna" from Vinnaithaandi Varuvaayaa.</p>
                <p>Aalaap Raju is a playback singer and a bass player from Chennai, India. His rendition of Ennamo Aedho from the movie Ko composed by Harris Jayaraj topped the music charts for several months in 2011 and won him the Filmfare Award for Best Male Playback Singer – 2011. He has sung for music directors like Harris Jayaraj, Thaman, G.V Prakash, Deepak Dev, D.Imman, and Sreekanth Deva. His other noticeable songs include Vaaya moodi summa iru da from Mugamoodi, Engeyum Kadhal from Engeyum Kadhal, Endhan Kann Munnae from Nanban, Kadhal Oru Butterfly and Akila Akila from Oru Kal Oru Kannadi, Anjana Anjana from Vandhan Vendran, Kuthu Kuthu from Ayyanar, Endhuko Yemo from Rangam, Nenjodu Cherthu from Yuvvh, Theeyae Theeyae from Maattrraan, Jal Jal Osai from Manam Kothi Paravai, and Maya Bazaar from Ennai Arindhaal. Be prepared for some retro magic and superhits alike from Aalaap and his super talented band.</p>
                <p>Agam's musical style is Carnatic Progressive Rock. Their music is a blend of Carnatic music and Rock. The band draws inspirations from Traditional Carnatic music and progressive rock acts like Dream Theater. The band borrows their name from the Tamil word 'Agam' which means the 'Heart, soul or the inner soul'. The band was formed in the year 2003. The current lineup consists of Harish Sivaramakrishnan (vocals and violin), Ganesh Ram Nagarajan (drums and backing vocals), Swamy Seetharaman (keyboards and lyricist), T Praveen Kumar (lead guitar), Aditya Kasyap (bass guitar and backing vocals), Sivakumar Nagarajan (ethnic percussions), Jagadish Natarajan (rhythm guitar), Yadhunandan (Drummer). Agam received its big break in the year 2007 when it participated in a band hunt called “Ooh la la la,” hosted by the Tamil television channel Sun TV. The competition was judged by the Oscar-winning Indian music composer A. R. Rahman. Rahman was impressed by the band’s potential and adjudged them one of the winners of the contest. Their success in “Ooh la la la” brought them into mainstream and they performed in concerts and music festivals in South Indian cities like Bangalore, Chennai, and Hyderabad.  Agam has collaborated with many individual musicians over the course of 5 years. Among them was their collaboration with acclaimed Indian playback singer Shreya Ghoshal for a song titled Live Again and with Aruna Sairam for Navotsavam.</p>
                <p>Sooraj Santhosh is an Indian playback singer who sings mainly in Tamil, Telugu, and Malayalam languages and also sung in Hindi, Kannada and Badaga. He is a member of the popular band Masala Coffee. This time however, he will be featured with Jatayu, a fresh, Carnatic fusion band led by Shylu Ravindran on Electric carnatic guitar. This combination is explosive and we are going to see some very new experimental music and brilliant covers from them.</p>
                <p>Get your tickets now from AirAsiaRedtix.com, while you can catch our latest videos from www.fb.com/mymojoprojects Don’t forget to “like” our page!</p>
                </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/milff/milff-poster.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/milff/milff-banner.jpg" alt="">
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Category</th>
                          <th>Early Bird Price</th>
                          <th>Normal Price</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td><span class="box midgreen">S1</td>
                              <td>RM 217.70</td>
                              <td>RM 241.45</td>
                              <td rowspan="4"><img class="img-responsive seatPlanImg" src="images/milff/milff-seatplan.jpg" alt="">
                          </tr>
                          <tr>
                              <td><span class="box yellow">Category A</td>
                              <td>RM 274.95</td>
                              <td>RM 305.05</td>
                          </tr>
                          <tr>
                              <td><span class="box red">Category B</td>
                              <td>RM 179.55</td>
                              <td>RM 199.05</td>
                          </tr>
                          <tr>
                              <td><span class="box purple">Category C</td>
                              <td>RM 131.85</td>
                              <td>RM 146.05</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/malaysian independent live fusion festival 2017/events" role="button">BUY TICKETS</a>
                  <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>
                <span class="importantNote">* Prices shown include RM4.00 AirAsiaRedTix fee & 6% GST</span>
                <div class="note text-left">
                    <h2>Important Notes</h2>
                    <ol>
                      <li>Prices shown include RM4.00 AirAsiaRedTix fee & 6% GST</li>
                      <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                      <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                      <li>Strictly no replacement for missing tickets and cancellation.</li>
                      <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                    </ol>
                    <h2>For enquiry only:</h2>
                    <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        {{-- <section class="pageCategory-section last">
          <div class="buyNowSec" style="background-image: url('images/event2.jpg')">
            <div class="bannerOverlay"></div>
            <div class="container">
              <div class="col-sm-offset-1 col-sm-10 quoteArea">
                <h1>You’ve heard him on the radio, you’ve seen him on TV. It’s about time. See him live!</h1>
              </div>
              <div class="btnArea">
                <a class="btn btn-black btn-lg" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/konsert%20gegar%20pop%20yeh%20yeh/events" role="button">GET TICKET ONLINE NOW <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <div class="clearfix">&nbsp;</div>
                <span data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets</span>
              </div>
            </div>
          </div>
        </section> --}}

        {{-- <section class="pageCategory-section last">
          <div class="container">
            <div class="partner text-center">
                <h1 class="subSecTitle"><strong>PARTNERS</strong></h1>
                <div class="clearfix">&nbsp;</div>
                <ul class="list-unstyled list-inline partnerList">
                    <li><img src="images/neon/aabig_logo (offical partner).png" alt=""></li>
                    <li><img src="images/neon/BAC.png" alt=""></li>
                    <li><img src="images/neon/durex (official partner).png" alt=""></li>
                    <li><img src="images/neon/fac3 (co-organiser).jpg" alt=""></li>
                    <li><img src="images/neon/seven friday (official watch).jpeg" alt=""></li>
                    <li><img src="images/neon/sunway lagoon (official venue).png" alt=""></li>
                </ul>
            </div>
          </div>
        </section> --}}

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                <dd>Bangsar Village (TEL: 03-22021139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
                <dd>Tropicana</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
                <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
            </dl>
            </div>
        </div>
        </div>
    </div>

@endsection