@extends('master')
@section('title')
    FARAHDHIYA LIVE AT HARD ROCK CAFÉ KL
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/farahdhiya/farahdhiya-banner.jpg')">
      </div>
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>FARAHDHIYA LIVE AT HARD ROCK CAFÉ KL</h6>
                  Tickets from <span>RM54</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 30th April 2017, (Sunday)</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Hard Rock Café, Kuala Lumpur <a target="_blank" href="https://goo.gl/maps/9ZLpaezMUrQ2">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 2.30pm – 7.00pm</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <h2>PRESENTED BY FARAHDHIYA & MELODY UNITED MUSIC</h2>
                <p>Farahdhiya adalah naib juara Gegar Vaganza Musim Ke 2,  sebuah program realiti terbitan Astro yang menyaksikan 12 penyanyi professional lama atau berpengalaman dalam industri muzik di Malaysia bersaing dalam satu pertandingan nyanyian.<br>
                Farahdhiya Mohd Amin ialah seorang penyanyi, pelakon dan pengacara wanita yang bergiat di dalam dunia hiburan Malaysia. Beliau yang berbintang Taurus dilahirkan pada 28 April 1984 di Sarzana, Itali.</p>

                <p>Beliau merupakan anak bongsu daripada dua beradik. Ibu berasal dari Portsmouth, England manakala ayahnya berasal dari Johor. Mendapat pelajaran awal di Sekolah Menengah Telok Gadung, Klang, Selangor kemudian berpindah ke Sekolah Menengah Taman Melawati, Ulu Kelang, Selangor.</p>

                <p>Farahdhiya mula menjinakkan diri di dalam dunia nyanyian sejak berusia belasan tahun lagi di bawah naungan KRU Records. Kini bernaung di bawah NAR Records. Beliau telah menghasilkan dua buah album, Bertakhta Di Hati dalam tahun 2004 dan Yang Ku Puja di akhir 2006. Bertakhta Di Hati mendapat tempat di dalam carta carta di dalam radio Era dan Hot.FM serta mendapat pencalonan di dalam Anugerah AIM sebagai Artis Baru Terbaik.[perlu rujukan]</p>

                <p>Selain menyanyi, Farah Dhiya berkecimpung di dalam bidang pengacaraan di mana beliau menjadi hos rancangan realiti, Jaguh Futsal RTM selama beberapa episod. Selain dari itu, Farahdhiya juga telah berjinak-jinak dengan dunia lakonan. Drama pertama lakonannya adalah Mencari Arah Kiblat yang disiarkan di TV sempena Awal Muharam 2007. Pada masa kini, beliau membintangi drama popular di TV3, dalam slot akasia, iaitu Spa Q untuk 60 episod, yang disiarkan pada 23 Oktober 2007 hingga 31 Januari 2008 lalu, sebagai Fatin. Drama inilah yang menaikkan namanya sekarang.</p>

                <p>Kemunculan semula beliau dalam pertandingan Gegar Vaganza, Nama beliau kembali diperkatakan dalam bidang muzik bila berjaya memenangi tempat kedua pertandingan tersebut dengan perbezaan markah kurang dari 2 markah dari pemenangnya Siti Nordiana.</p>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/farahdhiya/poster.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/farahdhiya/banner.jpg" alt="">
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Normal Price* <br>(include ONE soft drink)</th>
                          <th>Remarks</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td><span class="box yellow">VVIP</span></td>
                              <td>RM 354</td>
                              <td>Meet & Greet Buffet Package</td>
                              <td rowspan="11"><img class="img-responsive seatPlanImg" src="images/farahdhiya/seatplan.jpg" alt=""></td>
                          </tr>
                          <tr>
                              <td><span class="box green">Platinum</span></td>
                              <td>RM 204</td>
                              <td rowspan="3">Seated</td>
                          </tr>
                          <tr>
                              <td><span class="box orange">Gold</span></td>
                              <td>RM 154</td>
                          </tr>
                          <tr>
                              <td><span class="box red">Silver</span></td>
                              <td><del>RM 104</del><br><strong class="text-danger">(Sold Out)</strong></td>
                          </tr>
                          <tr>
                              <td><span class="box purple">Bronze</span></td>
                              <td>RM 54</td>
                              <td>Free Standing</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/events/faradhiya%20live%20at%20hard%20rock%20café%20kuala%20lumpur/2017-4-30_14.30/hard%20rock%20café%20kuala%20lumpur?hallmap" role="button">BUY TICKETS</a>
                  <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>
                <div class="note text-left">
                    <h2>Note</h2>
                    <ol>
                        <li>* Prices shown include RM4.00 AirAsiaRedTix fee</li>
                        <li>* Cover Charge Price includes One Soft Drink</li>
                        <li>* The organiser reserves the right to change the flow of the agenda and times of performances as they may be additional guest artist invited to perform. The showcase is from 2.30pm-7.00pm</li>
                    </ol>
                    <h2>Reminder</h2>
                    <p><strong>Dress code</strong><br>Neat and decent., For Males shorts, slippers, and sandals are STRICTLY NOT ALLOWED.</p>
                    <p><strong>Minimum age</strong><br>6 years & above</p>
                    <p><strong>Outside F&B</strong><br>Outside Food and drinks are not allowed.</p>
                    <p>Each ticket is valid for one-time admission only.</p>
                    <p>Child must be of 95cm height and below to be eligible for child ticket priced at RM50 without seat, while child at height above 95cm must purchase tickets according to price scale.</p>
                    <h2>Important Notes</h2>
                    <ol>
                      <li>Prices shown include RM4.00 AirAsiaRedTix fee & 6% GST.</li>
                      <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                      <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                      <li>Strictly no replacement for missing tickets and cancellation.</li>
                      <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                    </ol>
                    <h2>For enquiry only:</h2>
                    <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        {{-- <section class="pageCategory-section last">
          <div class="buyNowSec" style="background-image: url('images/event2.jpg')">
            <div class="bannerOverlay"></div>
            <div class="container">
              <div class="col-sm-offset-1 col-sm-10 quoteArea">
                <h1>You’ve heard him on the radio, you’ve seen him on TV. It’s about time. See him live!</h1>
              </div>
              <div class="btnArea">
                <a class="btn btn-black btn-lg" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/konsert%20gegar%20pop%20yeh%20yeh/events" role="button">GET TICKET ONLINE NOW <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <div class="clearfix">&nbsp;</div>
                <span data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets</span>
              </div>
            </div>
          </div>
        </section> --}}

        {{-- <section class="pageCategory-section last">
          <div class="container">
            <div class="partner text-center">
                <h1 class="subSecTitle"><strong>PARTNERS</strong></h1>
                <div class="clearfix">&nbsp;</div>
                <ul class="list-unstyled list-inline partnerList">
                    <li><img src="images/neon/aabig_logo (offical partner).png" alt=""></li>
                    <li><img src="images/neon/BAC.png" alt=""></li>
                    <li><img src="images/neon/durex (official partner).png" alt=""></li>
                    <li><img src="images/neon/fac3 (co-organiser).jpg" alt=""></li>
                    <li><img src="images/neon/seven friday (official watch).jpeg" alt=""></li>
                    <li><img src="images/neon/sunway lagoon (official venue).png" alt=""></li>
                </ul>
            </div>
          </div>
        </section> --}}

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                <dd>Bangsar Village (TEL: 03-22021139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
                <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
            </dl>
            </div>
        </div>
        </div>
    </div>

@endsection