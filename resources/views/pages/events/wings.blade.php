@extends('master')
@section('title')
    Konsert Wings & Superfriends - Belenggu Irama
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    {{-- <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/wings/wings-banner.jpg')">
        <video id="video-background" class="hidden-xs" preload muted autoplay loop>
          <source src="images/wings/wings-vidbanner.mp4" type="video/mp4">
        </video>
      </div>
    </section> --}}<!-- /Banner Section -->

    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/wings/wings-banner.jpg')}}" style="width: 100%" class="img-responsive"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/wings/wings-thumb.jpg')}}" style="width: 100%" class="img-responsive">
        </div>
    </section>

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>Konsert Wings & Superfriends - Belenggu Irama</h6>
                  Tickets from <span>RM80</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 6th - 9th April 2017</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Panggung Sari, Istana Budaya <a target="_blank" href="https://www.google.com/maps/place/National+Theatre+(Istana+Budaya)/@3.1743733,101.7015273,17z/data=!3m1!4b1!4m5!3m4!1s0x31cc481ef641f96b:0xfe0d3f27f31db1ce!8m2!3d3.1743733!4d101.703716?q=Panggung+Sari,+Istana+Budaya&um=1&ie=UTF-8&sa=X&ved=0ahUKEwi4v-nU54bRAhUBro8KHdevC8UQ_AUICCgB">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:30 pm</div>
                <div class="clearfix">&nbsp;</div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- /sharing -->
                <hr>
                <p>Nama Awie tidak asing lagi dalam industri muzik tanahair.  Artis serba boleh ini seringkali mendapat perhatian di dalam semua aktiviti dan hasil karya beliau.  Vokalis kumpulan muzik terkenal tanahair ini juga mempunyai suara unik yang pastinya akan memukau seluruh peminat muzuk.  Dengan karisma dan kebolehan beliau, Awie melangkah setapak lagi kebidang pengarahan persembahan pentas dengan menghasilkan KONSERT WINGS AND SUPERFRIENDS – BELENGGU IRAMA.</p>
                
                <p>KONSERT WINGS AND SUPERFRIENDS – BELENGGU IRAMA akan menggegarkan Istana Budaya pada 6 hingga 9 April 2017 selama 2 jam setiap malam dan merupakan sebuah konsert berkonsep memperjuangkan nasib para insan seni tanahair.  Gandingan artis-artis hebat dalam satu pentas dengan soroton adegan dari rintihan lagu selama 2 jam akan menjelajahi jiwa dan rasa penonton terhadap keluhan hati nasib artis yang di perjuangkan. </p>
                
                <p>Tujuan konsert ini diadakan bagi mendidik masyarakat untuk menghibur sambil menderma. Awie Entertainment Sdn Bhd yang menganjurkan konsert ini mengumpulkan persatuan–persatuan di Malaysia untuk membentangkan permasalahan di dalam persada seni terutamanya dari segi kebajikan anak – anak seni yang tidak pernah berubah.  Dato’ Awie, teraju utama Awie Entertainment Sdn Bhd berharap gabungan dengan persatuan-persatuan seperti Karyawan, Seniman, Yayasan 1Malaysia, MACP, RPM dan RIM dapat memperbaiki taraf anak–anak seni tanah air.  </p>
                
                <p>Bagi Dato’ Awie, nasib para artis Malaysia tidak seharusnya malang seperti yang telah berlaku selama ini kerana para artis adalah golongan yang amat berpengaruh.  Pengaruh dan nama besar mereka hanya ketika popular dan sekiranya tidak ada langkah yang betul di ambil maka ianya tidak akan menjanjikan masa depan yang baik dari segi kewangan. </p>

                <p>Ikon konsert ini ialah pemain drum ternama, YAZID dari KUMPULAN SEARCH.  Tiada seorang peminat muzik tanahair yang tidak kenal beliau.  Menganggotai sebuah kumpulan muzik rock ternama selama ini tetapi setelah beliau mengidap penyakit buah pinggang, mata pencariannya terhad.  Dato’ Awie sebagai salah seorang peneroka muzik rock tanahair mengambil inisiatif untuk membantu beliau dan tidak mahu anak – anak seni di negara ini hidup dalam kesempitan di zaman kejatuhan. </p> 
                
                <p>Artis – arits ternama iaitu Dato’ Awie, Eddie Wings, Black Wings, Dato’ M.Nasir, Datuk Ramli Sarip, Dato’ Nash, Ito, Syamel AF, Maya Karim, Liza Hanim, Haiza, Sahara Yaacob, Billa AF, Ucop Bukan Sekadar Rupa, Aniq Ceria Pop Star adalah antara para artis yang berpadu tenaga untuk menjayakan KONSERT WINS AND SUPERFRIENDS – BELENGGU IRAMA.  Para pemuzik yang turut terlibat adalah Eddrie Hashim, Yan Lefthanded, Sham Guitarist, Kudin Headwind, Lola, Rodi Kristal, Hamzah, Pae, Fly dan Adi.
                Harga tiket bemula daripada RM80.00, RM150.00, RM200.00, RM250.00, RM300.00 dan RM350.00 boleh di beli melalui ONLINE di Website Redtix, www.airasiaredtix.com atau pun di Kaunter Pembelian Tiket di Istana Budaya mulai eosk, 1 Mac 2017. Sebarang pertanyaan boleh hubungi Awie Entertainment di talian 019 2932436 (Ejat) atau 019 2975260 (Sam).  Juga boleh lawati Facebook Sayap-Sayap Wings atau www.sayap2wings.com.</p>
                
                <p>Awie Entertainment Sdn Bhd yang disokong padu oleh rakan seperjuangan di dalam persada seni amat berharap kejayaan konsert ini akan berpihak kepada semua insan seni tanahair. </p>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="gallery text-center">
            <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img class="" src="images/wings/poster.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/wings/wings-banner.jpg" alt="">
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                  <table class="table infoTable-D table-bordered">
                      <thead>
                        <tr>
                          <th>Price Category</th>
                          <th>Normal Price</th>
                          <th>Seating Plan</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td rowspan="3"><span class="box grey">Stalls</span></td>
                              <td>RM 303</td>
                              <td rowspan="11"><img class="img-responsive seatPlanImg" src="images/wings/seatplan.jpg" alt=""></td>
                          </tr>
                          <tr>
                              <td>RM 253</td>
                          </tr>
                          <tr>
                              <td>RM 203</td>
                          </tr>
                          <tr>
                              <td rowspan="3"><span class="box grey">Grand Circle</span></td>
                              <td>RM 350</td>
                          </tr>
                          <tr>
                              <td>RM 203</td>
                          </tr>
                          <tr>
                              <td>RM 153</td>
                          </tr>
                          <tr>
                              <td rowspan="2"><span class="box grey">Upper Circle</span></td>
                              <td>RM 80</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/wings%20-%20superfriends/events" role="button">BUY TICKETS</a>
                  <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>
                <span class="importantNote">*Additional charges will be applied for credit/debit card transaction, optionally customer may pay cash for outlet purchase to avoid the charges.</span>
                <div class="note text-left">
                  <h2>Reminder</h2>
                  <p><strong>Dress code</strong><br>Neat and decent. Worn out / faded / shabby jeans, t-shirt without collar, shorts, slippers, and sandals are STRICTLY NOT ALLOWED.</p>
                  <p><strong>Minimum age</strong><br>6 years & above</p>
                  <p>Food and drinks are not allowed in the auditorium.</p>
                  <h2>Important Notes</h2>
                  <ol>
                    <li>Prices shown include RM3.00 AirAsiaRedTix fee & exclude 6% GST.</li>
                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                    <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                    <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                  </ol>
                  <h2>For enquiry only:</h2>
                  <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <h2>GET YOUR TICKETS FROM:</h2>
            <dl class="dl-horizontal">
                <dt>Online:</dt>
                <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Rock Corner outlets:</dt>
                <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                <dd>Bangsar Village (TEL: 03-22021139)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Victoria Music outlets:</dt>
                <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Penang outlets:</dt>
                <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Istana Budaya Jalan<br>Tun Razak:</dt>
                <dd>03 40265520</dd>
            </dl>
            </div>
        </div>
        </div>
    </div>

@endsection