{{-- route(@extends('master') --}}
@extends('masterforultra')
@section('title')
    Ultra Singapore 2018 DBS Staff Sales
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="css/custom2.css">
    <link type="text/css" rel="stylesheet" href="css/clock.css">
    <link type="text/css" rel="stylesheet" href="/css/custom/ultra2018.css">

    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/ultrasingapore2018/ultra-banner-mock.jpg')}}" style="width: 100%" class="img-responsive" alt="ultrasingapore2018"><a href="#usgIntroduction"><img src="images/ultrasingapore2018/arrow.png" class="center-block arrow img-responsive"></a></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/ultrasingapore2018/mobile-splash-3.jpg')}}" style="width: 100%" class="img-responsive" alt="ultrasingapore2018">
            <a href="#usgIntroduction"><img src="images/ultrasingapore2018/arrow.png" class="center-block arrow-mobile img-responsive"></a>
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white" style="padding-top: 0px;" id="usgIntroduction">
            <section class="pageCategory-section section-black last">
                <div class="container tixPrice">
                <div class="row">
                        <div class="col-sm-offset-1 col-sm-10" style="text-align:center;">
                            <h6 style="color:#fff;">Ultra Singapore 2018</h6>
                            <p>Friday, 15 June 2018 - Saturday, 16 June 2018</p>
                            <p>Ultra Park, 1 Bayfront Avenue, Singapore</p>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10">
                            <h6 class="text-center" style="margin-top: 40px; color: #fff;">ESCAPE THE ORDINARY. BE ULTRA.</h6>
                            <p>The music festival that has been taking the whole world by storm is landing at Singapore. This will be Ultra’s 3rd consecutive year partying at the Lion City. Hordes of ‘Ultranauts’ will travel from all over the globe to enjoy some world-class entertainment and soak up the ever stunning destination while they’re at it. Be ready for 2 days of explosive energy from the people, to the DJs on stage.</p>
                            <p>Wave your national flag and tell 45,000 revellers which part of the earth are you from. Enjoy the best of EDM from the melting pot of culture for Ultra. Leave all your worries behind when the beat drops, and enjoy the unbelievable energy from the Ultra family.</p>
                            <p>Ultra is the biggest party in the world, and you Ultranauts are the stars. Lights will guide you, music will revive you and fireworks will captivate you at beautiful Singapore with Ultra.</p>
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/oN9uCVPBB3Q" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last" id="ticketSection">
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10">
                            <div class="text-center col-sm-12 col-xs-12">
                                <h1 class="subSecTitle"><strong>TICKETS</strong></h1>
                                {{-- <p>This Exclusive Sale is for DBS Live Fresh cardmembers and DBS/POSB cardmembers. We reserve the right to cancel and refund any transactions that were not purchased with a DBS card.</p> --}}
                                 {{-- <p>DBS/POSB Cardholders Promo: 2 Free drinks per 2-day GA purchased (limited to the first 1,000 tickets)<br/>
                                 Live Fresh Promo: Free upgrade to 2-day PGA per 2-day GA purchased (limited to the first 500 tickets)<br/>
                                 <a href="https://intercom.help/airasia-redtix/your-guide-to-ultra-singapore-2018">Click here to find out more</a></p> --}}
                            </div>
                            <div class="clearfix"></div>
                            {{-- dekstop --}}
                            <div class="col-sm-12 hidden-xs hidden-md hidden-sm">
                                <div class="col-sm-5">
                                    <button data-toggle="tab" href="#ticket_only" class="tixOptBtn btn active"><i class="fa fa-ticket"></i>TICKET ONLY</button>
                                </div>
                                <div class="col-sm-7">
                                    <button data-toggle="tab" href="" class="tixOptBtn btn disabled"><i class="fa fa-ticket"></i><i class="fa fa-plane"></i>TICKET + FLIGHT</button>
                                </div>
                                {{-- <div class="col-sm-5">
                                    <button data-toggle="tab" href="" class="tixOptBtn btn disabled"><i class="fa fa-ticket"></i><i class="fa fa-plane"></i><i class="fa fa-building-o"></i>TICKET + FLIGHT + HOTEL</button>
                                </div> --}}
                            </div>
                            {{-- mobile --}}
                            <div class="col-xs-12 hidden-lg pd-0">
                               <div class="col-xs-12 ps-9">
                                    <button data-toggle="tab" href="#ticket_only" class="tixOptBtn btn">
                                        <i class="fa fa-ticket"></i>
                                        <p>TICKET ONLY</p>
                                    </button>
                                </div>
                                <div class="col-xs-12 ps-9">
                                    <button data-toggle="tab" href="" class="tixOptBtn btn disabled">
                                        <i class="fa fa-ticket"></i>
                                        <i class="fa fa-plane"></i>
                                        <p>TICKET + FLIGHT</p>
                                    </button>
                                </div>
                                {{-- <div class="col-xs-5 ps-9">
                                    <button data-toggle="tab" href="" class="tixOptBtn btn disabled">
                                        <i class="fa fa-ticket"></i>
                                        <i class="fa fa-plane"></i>
                                        <i class="fa fa-building-o"></i>
                                        <p>TICKET + FLIGHT + HOTEL</p>
                                    </button>
                                </div> --}}
                            </div>
                            <div class="tab-content">
                                <div id="ticket_only" class="tab-pane fade in active">
                                    <div class="col-xs-12" id="DBS">
                                        <!-- <p style="font-size: 24px; padding-top: 80px;" class="text-center"><strong>EXCLUSIVE </strong><img src="images/ultrasingapore2018/dbs_logo.png" height="25"> <strong> STAFF TICKETS</strong></p> -->
                                        <p style="font-size: 24px; padding-top: 80px;" class="text-center"><strong>EXCLUSIVE DBS/POSB STAFF TICKETS</strong></p>
                                        <!-- <p class="text-center">
                                        Stand a chance to win the Ultimate ULTRA Singapore Experience when you purchase your tickets with a DBS Live Fresh card!<br />
                                        - Complimentary upgrade to standing VVIP tickets<br />
                                        - 2 night stay at MBS<br />
                                        - Backstage Tour<br />
                                        - Guaranteed access to ULTRA After Party with a complimentary bottle of champagne<br />
                                        </p> -->
                                        <p class="text-center">
                                        Get your ULTRA Singapore 2-day General Admission Tickets at S$155 (U.P. S$255) with your DBS/POSB Card!
                                        </p>
                                    </div>

                                    <div class="col-sm-12">
                                        @foreach($ticket_only as $ticket)
                                            @if($ticket["Card Type"] == "DBSStaff") 
                                                <div class="col-sm-12 col-xs-12 ticketRow hidden-xs hidden-sm">
                                                    <small class="discountTag">Exclusive</small>
                                                    <div class="col-sm-2 text-center ticketTitle">
                                                        <div class="row">
                                                            <p class="tier">Tier 1</p>
                                                            <h1>{{$ticket["Ticket Type"]}}</h1>  
                                                        </div> 
                                                    </div>
                                                    <div class="col-sm-3 text-center ticketDay border-left">
                                                        <h6>2 DAY COMBO TICKET</h6>
                                                    </div>
                                                    <div class="col-sm-4 ticketAdmission border-left">
                                                        <p style="float: left;">
                                                            {{ $ticket["Ticket Description"] }}<br><span class="smallnote">Limited to 500 tickets on a first come first serve basis</span>
                                                        </p>
                                                        <div style="position: absolute;right: 10px;cursor: pointer;" data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}">
                                                            <img src="images/ultrasingapore2018/i.png" width="20">
                                                        </div>

                                                    </div>
                                                    <div class="col-sm-3 ticketBuy border-left">
                                                        <div class="col-sm-12 text-center">
                                                            <div class="prices">
                                                                <span class="strikethrougPrice">SGD{{$ticket["Normal Price Front End"]}}</span>
                                                                <span class="discountPrice">SGD{{$ticket["Private Sale Front End"]}}</span>
                                                            </div>    
                                                            <a class="btn ultraBuyBtn center-block" data-toggle="modal" data-target="#buyModalDBSStaff{{ $ticket["Sorting ID"] }}">BUY NOW</a>
                                                            {{-- <small>Price excludes ticketing fee, Event Protect insurance &amp; credit card charges</small> --}}
                                                            <small>Price excludes ticketing fee, Event Protect &amp; card charges</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- mobile ticket ONLY card responsive -->
                                                <div class="col-sm-12 hidden-lg hidden-md ticketCard">
                                                    {{-- <small class="discountTag"> 40% OFF</small> --}}
                                                    <small class="discountTag">Exclusive</small>
                                                    <div class="col-sm-12 card-body pd-0">
                                                    

                                                        <div class="col-sm-12 cardTop">
                                                        
                                                            <div class="col-sm-12 pd-0 ticketTitle">
                                                                <p class="ticketFlight">{{ $ticket["Ticket Name"] }}</p>
                                                                <p class="ticketTier">Tier 1<span class="ticketCategory">{{ $ticket["Ticket Type"] }}</span></p>
                                                                <a data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}"><img src="images/ultrasingapore2018/i.png" width="20"></a>
                                                                {{-- <div style="position: absolute;right: -80px;top: 0;cursor: pointer;" data-toggle="modal" data-target="#infoModal{{ $ticket["Sorting ID"] }}">
                                                                    <img src="images/ultrasingapore2018/i.png" width="20">
                                                                </div> --}}
                                                            </div>
                                                            <div class="col-sm-12 ticketPrice">
                                                                @if(strlen($ticket["Private Sale Front End"]) !== 0)
                                                                   {{-- <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}<br><small>10 tickets left at this tier</small></p> --}}
                                                                   <p class="pull-right text-right">{{ $ticket["Private Sale Front End"] }}</p>
                                                                @else
                                                                    <p class="pull-right text-right">N/A</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="cardBottom col-sm-12 text-center">
                                                            <div class="prices">
                                                            <p>{{ $ticket["Ticket Description"] }}<br><span class="smallnote">Limited to 500 tickets on a first come first serve basis</span></p>
                                                            </div>
                                                            <a class="btn ultraBuyBtn center-block" data-toggle="modal" data-target="#buyModalDBSStaff{{ $ticket["Sorting ID"] }}">BUY NOW</a>
                                                                <small>Price excludes ticketing fee, Event Protect &amp; card charges</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>

                                    <div class="col-sm-12 text-center">
                                        {{-- <p>Plus, stand a chance to win the Ultimate ULTRA Singapore Experience when you purchase your tickets with your DBS Live Fresh Card!
                                        <br />This includes:
                                        <ul>
                                            <li>Complimentary upgrade to standing VVIP tickets</li>
                                            <li>Backstage Tour</li>
                                            <li>Guaranteed access to ULTRA After Party with a complimentary bottle of champagne</li>
                                            <li>2 nights’ stay at MBS</li>
                                        </ul>
                                        Limited to 500 tickets capped at 2 tickets per staff. All transactions must be checked out with a DBS Staff email address.
                                        </p> --}}
                                        <p>Plus, stand a chance to win the Ultimate ULTRA Singapore Experience<br />when you purchase your tickets with your DBS Live Fresh Card!
                                        <br /><br />This includes:
                                        <br />&middot; Complimentary upgrade to standing VVIP tickets
                                        <br />&middot; Backstage Tour
                                        <br />&middot; Guaranteed access to ULTRA After Party with a complimentary bottle of champagne
                                        <br />&middot; 2 nights’ stay at MBS
                                        <br /><br />Limited to 500 tickets capped at 2 tickets per staff.<br />All transactions must be checked out with a DBS/POSB Staff email address.
                                        </p>

                                        <!-- <p class="text-center">*Limited to 2 tickets per person<br />
                                        ** All transactions must be checked out WITH a DBS staff email<br /></p> -->
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center col-sm-12 col-xs-12">
                                <h1 class="subSecTitle"><strong>CONCERT GALLERY</strong></h1>
                            </div>
                            <div class="row hidden-xs">
                                <div class="col-sm-12 text-center pd-0">
                                    <div class="col-sm-8 pd-0">
                                        <div class="col-sm-6 collageImg">
                                            <a href="images/ultrasingapore2018/gallery1-1.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery1-1.png" alt=""></a>
                                        </div>
                                        <div class="col-sm-6 collageImg">
                                            <a href="images/ultrasingapore2018/gallery1-2.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery1-2.png" alt=""></a>
                                        </div>
                                        <div class="col-sm-12 collageImg">
                                            <a href="images/ultrasingapore2018/gallery4.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery4.png" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 pd-0">
                                        <div class="col-sm-12 collageImg">
                                            <a href="images/ultrasingapore2018/gallery3.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery3.png" alt=""></a>
                                        </div>  
                                        <div class="col-sm-12 collageImg">
                                            <a href="images/ultrasingapore2018/gallery5.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery5.png" alt=""></a>
                                        </div>  
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center pd-0">
                                    <div class="col-sm-12 pd-0">
                                        <div class="col-sm-4 collageImg">
                                            <a href="images/ultrasingapore2018/gallery6.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery6.png" alt=""></a>
                                        </div>
                                        <div class="col-sm-8 collageImg">
                                            <a href="images/ultrasingapore2018/gallery7.png" data-featherlight="image"><img class="img-responsive" src="images/ultrasingapore2018/gallery7.png" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="gallery text-center hidden-lg hidden-md hidden-sm">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery1-1.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery1-1.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery1-2.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery1-2.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery4.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery4.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery3.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery3.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery5.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery5.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery6.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery6.png" alt=""></a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="images/ultrasingapore2018/gallery7.png" data-featherlight="image"><img src="images/ultrasingapore2018/gallery7.png" alt=""></a>
                                        </div>
                                    </div>

                                    <div class="swiper-pagination"></div>

                                    <div class="swiper-button-next swiper-button-white"></div>
                                    <div class="swiper-button-prev swiper-button-white"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            
             <section class="pageCategory-section last bottom">
                <img src="images/ultrasingapore2018/white-logo1.png" class="img-responsive logoBottom hidden-xs"> 
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="note text-left">
                                <h4>please contact us at <a href="mailto:ultra@airasiaredtix.com">ultra@airasiaredtix.com</a><br> if you have any further questions for custom packages</h4>
                            </div>
                            <div class="note text-left">
                                <h4>For enquiry only:</h4>
                                <p>Email to <a href="mailto:ultra@airasiaredtix.com">ultra@airasiaredtix.com</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="center-block text-center">
                        <div class="faq center-block">
                            {{-- FAQ button --}}
                            <img src="images/ultrasingapore2018/question.svg" style="width: 30px;"> Need more info? <a class="tixOptBtn" href="https://intercom.help/airasia-redtix/your-guide-to-ultra-singapore-2018">FAQ</a>
                        </div>
                    </div>
                </div>
            </section>
 
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });
    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    
    <script type="text/javascript">
        $('.flightTab').hide();
        //show the first tab content
        $('#country-1').show();

        $('#select-country').change(function () {
           dropdown = $('#select-country').val();
          //first hide all tabs again when a new option is selected
          $('.flightTab').hide();
          //then show the tab content of whatever option value was selected
          $('#' + "country-" + dropdown).show();                                    
        });
    </script>

    @if(!empty(Session::get('error_code')) && Session::get('error_code') == 1)
        <script>
        $(function() {
            $('#errorModal').modal('show');
        });
        </script>
    @endif

    @if(!empty(Session::get('error_code')) && Session::get('error_code') == 2)
        <script>
        $(function() {
            $('#errorModal2').modal('show');
        });
        </script>
    @endif


    @if(!empty(Session::get('success_dbs')))
        <script>
        $(function() {
            $('#successdbsModal{{ session()->get('ticket_id') }}').modal('show');
        });
        </script>
    @endif

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTixCustom')
{{-- @foreach($ticket_flight as $ticket)
    <div id="flightModal{{ $ticket["Sorting ID"] }}" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                <h4>Flight Dates Available</h4>
                <div class="clearfix">&nbsp;</div>
                <div class="well">
                <p>Depart ({{ $ticket["From City"] }} - Singapore):</p>
                <p>{{ date(' D, M j Y', strtotime($ticket["Outbound Departure Date"])).", ". date('h:i a', strtotime($ticket["Outbound Departure Time"])) }}</p>
                <p>Return (Singapore - {{ $ticket["From City"] }}):</p>
                <p>{{ date(' D, M j Y', strtotime($ticket["Inbound Departure Date"])).", ". date('h:i a', strtotime($ticket["Inbound Departure Time"])) }}</p>
                </div>
            </div>
          </div>
        </div>
    </div>
@endforeach --}}
@foreach($ticket_only as $ticket)

    <div id="buyModalDBSStaff{{ $ticket["Sorting ID"] }}" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-body text-center pd-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="row ultraModalRow">
                    <div class="col-sm-7 col-xs-12 colUltraImg">
                        <img class="img-responsive imgUltraModal hidden-xs" src="images/ultrasingapore2018/modal-picture2.jpg">
                        <img class="img-responsive imgUltraModal hidden-lg hidden-md hidden-sm" src="images/ultrasingapore2018/modal-picture-mobile.jpg">
                    </div>
                    <div class="col-sm-5 text-center ultraform">

                        <p>Please can you enter your staff email</p>
                        <form action="{{route('ultradbsstaff')}}" method="get" enctype="multipart/form-data" class="buy_form">
                            {{ csrf_field() }}
                            <input type='hidden' name='ticket_id' value='{{$ticket['Sorting ID']}}'>
                            <div class="form-group center-block">
                                <input id="bin_num{{ $ticket["Sorting ID"] }}" type="email" class="form-control" name="bin_num" autofocus required="required" maxlength="100">
                            </div>
                            <div class="form-group center-block">
                                <button class="btn ultraBuyBtn" type="submit">SUBMIT</button>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
          </div>
        </div>
    </div>


    {{-- ticket info modal --}}
    <div id="infoModal{{ $ticket["Sorting ID"] }}" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-body text-center pd-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
                <div class="row ultraModalRow info-box">

                    @if($ticket["Ticket Type"] == "PGA")
                    <div class="details">
                        <h6 class="type">Premium General Admission</h6>
                        <h6 class="days">2 DAY TICKET COMBO</h6>
                    </div>
                    <div class="details">
                        <ul>
                            <li><span class="fa fa-tag"></span>Premium General Admission, free standing.</li>
                            <li><span class="fa fa-calendar"></span>Friday &amp; Saturday, 15 - 16 June 2018</li>
                            <li><span class="fa fa-map-marker"></span>Ultra Park, 1 Bayfront Avenue, Singapore</li>
                        </ul>   
                    </div>
                    @endif
                    @if($ticket["Ticket Type"] == "GA")
                    <div class="details">
                        <h6 class="type">General Admission</h6>
                        <h6 class="days">2 DAY TICKET COMBO</h6>
                    </div>
                    <div class="details">
                        <ul>
                            <li><span class="fa fa-tag"></span>General Admission, free standing.</li>
                            <li><span class="fa fa-calendar"></span>Friday &amp; Saturday, 15 - 16 June 2018</li>
                            <li><span class="fa fa-map-marker"></span>Ultra Park, 1 Bayfront Avenue, Singapore</li>
                        </ul>   
                    </div>
                    @endif
                        
                    @if($ticket["Ticket Type"] == "PGA")
                        <div class="services col-sm-12 col-xs-12 col-sm-push-1 col-xs-push-1">
                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/flag.png">
                                <span>Express Lane</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/drink.png">
                                <span>Bar Access</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/beach.png">
                                <span>Private Lounge</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/rfid-chip.png">
                                <span>Express RFID Top-up</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/watch-resolution.png" style="margin-left: 25px;">
                                <span>RFID Wristband</span>
                            </div> 
                        </div>  
                    @endif

                    @if($ticket["Ticket Type"] == "PGA")
                        <div class="services col-sm-12 col-xs-12 col-sm-push-1 col-xs-push-1">
                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/ep_mono.png">
                                <span>Insurance</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/restroom-sign.png">
                                <span>Private Restroom</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/18-plus.png">
                                <span>Above 18+</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/no-entry.png">
                                <span>No re-entry</span>
                            </div> 
                        </div>  
                    @endif

                    @if($ticket["Ticket Type"] == "GA")
                       <div class="services col-sm-12 col-xs-12 col-sm-push-1 col-xs-push-1">

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/drink.png">
                                <span>Bar Access</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/ep_mono.png">
                                <span>Insurance</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/watch-resolution.png" style="margin-left: 25px;">
                                <span>RFID Wristband</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/18-plus.png">
                                <span>Above 18+</span>
                            </div> 

                            <div class="col-xs-2 col-sm-2 item">
                                <img class="list-icon" src="images/ultrasingapore2018/no-entry.png">
                                <span>No re-entry</span>
                            </div> 

                        </div>  
                    @endif

                    <div class="copy_text">
                        *Ticketing fee, Event Protect insurance &amp; credit card <br/>
                        charges will be added at the checkout basket.<br/>
                        <a href="purchaseterms">Terms &amp; Conditions</a> apply.
                    </div>    

                </div> 
            </div>
          </div>
        </div>
    </div>

@endforeach

<div id="successdbsModal{{ $ticket_dbs["Sorting ID"] }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow">
                <div class="col-sm-7 col-xs-12 colUltraImg">
                    <img class="img-responsive imgUltraModal hidden-xs" src="images/ultrasingapore2018/modal-picture2.jpg">
                    <img class="img-responsive imgUltraModal hidden-lg hidden-md hidden-sm" src="images/ultrasingapore2018/modal-picture-mobile.jpg">
                </div>
                <div class="col-sm-5 text-center ultraform">
                    {{-- <p>{{ $ticket["Private Sale Front End"] }}</p> --}}
                    <h6>Congratulations!</h6>
                    <p>As a staff, you are entitled to purchase a {{ $ticket_dbs["Ticket Name"] }} {{ $ticket_dbs["Ticket Type"] }} ticket at {{ $ticket_dbs["Private Sale Front End"] }}</p>
                    <a class="btn ultraBuyBtn center-block" href="{{ $ticket_dbs["Ticketserv URL"] }}" onclick="location.href=this.href&linkerParam;return false;">BUY TICKET</a>
                    <small>Price excludes ticketing fee, Event Protect insurance &amp; credit card charges</small>
                </div>
            </div> 
        </div>
      </div>
    </div>
</div>

<div id="errorModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow">
                <div class="col-sm-7 col-xs-12 colUltraImg">
                    <img class="img-responsive imgUltraModal hidden-xs" src="images/ultrasingapore2018/modal-picture2.jpg">
                    <img class="img-responsive imgUltraModal hidden-lg hidden-md hidden-sm" src="images/ultrasingapore2018/modal-picture-mobile.jpg">
                </div>
                <div class="col-sm-5 text-center ultraform">
                    {{-- <p>Please can you enter in the <b>first 6 digits</b> of  your card number to see if you are entitled to an extra discount</p> --}}
                    {{-- <p>Sorry! you are not a DBS cardholder and cannot purchase a ticket now and will have to wait until the tickets go on sale publicly or alternatively you can sign up to a DBS LiveFresh card now.</p> --}}
                    <p>Sorry! This exclusive sale is for DBS Staff only.</p>
                    {{-- <p>To make sure you secure your ULTRA SINGAPORE 2018 Tickets please register below!</p>
                    <a class="btn ultraBuyBtn center-block" href="usg2018dbsstaff">Close</a> --}}
                    </form>
                </div>
            </div> 
        </div>
      </div>
    </div>
</div>

<!-- Purchased before -->
<div id="errorModal2" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body text-center pd-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 16px;"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="row ultraModalRow">
                <div class="col-sm-7 col-xs-12 colUltraImg">
                    <img class="img-responsive imgUltraModal hidden-xs" src="images/ultrasingapore2018/modal-picture2.jpg">
                    <img class="img-responsive imgUltraModal hidden-lg hidden-md hidden-sm" src="images/ultrasingapore2018/modal-picture-mobile.jpg">
                </div>
                <div class="col-sm-5 text-center ultraform">
                    <p>Sorry! You have made a purchase as a DBS/POSB Staff before.</p>
                    <a class="btn ultraBuyBtn center-block" href="usg2018dbsstaff">Close</a>
                    </form>
                </div>
            </div> 
        </div>
      </div>
    </div>
</div>
@endsection
