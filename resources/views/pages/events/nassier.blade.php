@extends('master')
@section('title')
    Nassier Wahab Celebrating 30 Years
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')

    <!-- Banner Section -->
    <section class="innerPageBanner">
      <div class="bigBanner-overlay"></div>
      <div class="jumbotron eventBanner" style="background-image: url('images/nassier/Nassier_Wahab_30Years_event.jpg')">
      </div>
    </section><!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
          <div class="row priceNbtn">
            <div class="col-sm-offset-1 col-sm-10">
              <div class="row">
                <div class="col-sm-9 leftBox">
                  <h6>Nassier Wahab Celebrating 30 Years</h6>
                  Tickets from <span>RM65</span>
                </div>
                <div class="col-sm-3 text-center">
                  <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
      <div class="mainBodyContent no-btm-mar section-white">

        <section class="pageCategory-section last">
          
          <div class="container intro">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 leftBar">
                <!--<h2>AFGAN SIDES LIVE IN MIRI 2016</h2>-->
                <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 1st January 2017</div>
                <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i> Hard Rock Café, Kuala Lumpur <a target="_blank" href="https://www.google.com/maps/place/Hard+Rock+Cafe+Kuala+Lumpur/@3.1554345,101.7039387,17z/data=!4m5!3m4!1s0x31cc482a13ccf737:0xa72d5847a2f848b0!8m2!3d3.1555095!4d101.705709">View Map</a></div>
                <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 2.30 pm - 7.00 pm</div>
                <hr>
                <p>Nassier Wahab is one of the most popular Malaysian Male Entertainer having sung his way into the hearts of thousands of fans since the 1980's. Born in 1962 he was brought up in Penang. He graduated from Joseph Chamberlain College in Birmingham England in Music Theory and Technology. He is not only a singer but an actor too. His songs have been used as the theme song for many drama. He plays the guitar, keyboard and percussion.</p>
                <p>His fan base is not only in Malaysia but includes Brunei, Singapore and Indonesia. The highlight of his career was when he was nominated for the best Male Singer Award at the Anugerah Bintang Popular Berita Harian in 1987 and 2004. Starting out his career in the mid 80’s.</p>
                <p>Nassier took part in Gegar Vaganza 2 and finished fourth. Being one of the favorites to win, the show showed how popular he was with both the older and younger fans till today. He is well known for his youthful looks as if he has not aged at all from the 80’s.</p>
                <p>Nassier is still very active in the music industry doing private shows in Singapore and Malaysia. He is actively participating in many entertainment and charity related events.</p>
                <p>This showcase at the Hard Rock Cafe Kuala Lumpur is to thank his loyal fans for their unwavering support over the years. Nassier will perform a tribute for his fans.</p>
                <p>The opening act will be by Melody United's singers Kris Patt and Alfi Alwi backed by the Patriot Band, Percussionist Calvin Clarke and dancers from JC Productions.</p>
                <p>The Showcase is presented to you by Melody United and Nassier Wahab Productions.</p>
                <p>For more information please email <a href="mailto:info@melodyunited.com">info@melodyunited.com</a> or call +6019 3611535.</p>
                <br>
                <table class="table table-striped infoTable-D">
                    <thead>
                      <tr>
                        <th colspan="3">Nassier albums and singles include</th>
                      </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td  style="width: 30%;"><p>1985 - Cinta dan Perasaan</p></td>
                        <td  style="width: 30%;"><p>1986 - Wajah Kekasih</p></td>
                        <td  style="width: 30%;"><p>1988 - Sebuah Janji Tak Bererti</p></td>
                    </tr> 
                    <tr>
                        <td><p>1989 - Sekali Aku Jatuh Cinta</p></td>
                        <td><p>1990 - Undangan Rindu</p></td>
                        <td><p>1992  - Album Koleksi (Lagu-lagu Negeri Kedah)</p></td>
                    </tr>
                    <tr>
                        <td><p>1994 - Album Koleksi “Suara Hat”(Brunei)</p></td>
                        <td><p>1996 - Album Koleksi Lagu-Lagu Filem (bersama Noraniza Idris)</p></td>
                        <td><p>1997 - Album Pedoman (bersama artis Syed Sobrie, Ebby Saiful dan Ahmad Fauzee)</p></td>      
                    </tr>
                    <tr>
                        <td><p>1999 - Album - Hanya Untuk Mu (bersama Raja Ema dan Dilla Allie)</p></td>
                        <td><p>1999 - Album Duet bersama Alma</p></td>
                        <td><p>2000 - Puncak Persada (Etnik Irama Malaysia)</p></td>
                    </tr>
                    <tr>
                        <td><p>2004 - Album - Memori Cinta Luka</p></td>
                        <td><p>2005 - Album - Ikatan Kasih</p></td>
                        <td><p>2006 - Album Koleksi 20 Tahun Nassier Wahab</p></td>
                    </tr>
                    <tr>
                        <td><p>2006 - MTV Karaoke Koleksi 20 Tahun Nassier Wahab</p></td>
                        <td><p>2006 - Duet bersama Kumpulan nasyid ‘Far East’ lagu bertajuk ‘Melakar Cinta di Pintu Syurga’</p></td>
                        <td><p>2006 - Album Duet bersama Rahimah Rahim (Sejak Ku Bertemu)</p></td>
                    </tr>
                    <tr>
                        <td><p>2007 - Album - Jangan Hapus Cintaku (Indonesia)</p></td>
                        <td><p>2007 - Album Lagu-lagu Raya “Syoknya Raya”</p></td>
                        <td><p>2007 - Album Lagu-lagu Raya Karaoke “Syoknya Raya”</p></td>
                    </tr>
                    <tr>
                        <td><p>2008 - Album  Koleksi “Memori Hits Nassier Wahab”</p></td>
                        <td><p>2008 - MTV Karaoke  Koleksi - “Memori Hits Nassier Wahab”</p></td>
                        <td><p>2008  - 2 Raja Pop Malaysia - Nassier Wahab / Jamal Abdillah</p></td>      
                    </tr><tr>
                        <td><p>2008 - 2 Raja Pop Malaysia - Karaoke Nassier Wahab / Jamal Abdillah</p></td>
                        <td><p>2009 - Single “Nafas Dua Hati” tema filem  “Dua Hati Satu Jiwa”</p></td>
                        <td><p>2009 - Koleksi  Pop Terbaik gandingan Nassier Wahab / Ibnor  Riza</p></td>      
                    </tr>
                    <tr>
                        <td><p>2009 - Koleksi  Pop Terbaik Karaoke gandingan Nassier Wahab / Ibnor  Riza</p></td>
                        <td><p>2009 - Lagu Tema Kementerian Kesihatan “Kami Sedia Membantu”</p></td>
                        <td><p>2010 - Album Kempen Konsumer Hak &amp; Peranan Pengguna “FOMC”</p></td>
                    </tr>
                    <tr>
                        <td><p>2011 - Album lagu-lagu Raya “Senandung Aidilfitri”</p></td>
                        <td><p>2011 - Dua buah lagu baru “Jangan Pernah Ada benci“ dan  “Selamat Datang Cinta“</p></td>
                        <td><p>2012 - Single terbaru “Puisi Lara“</p></td>
                    </tr>
                    <tr>
                        <td><p>2012 - Album Nassier Wahab “Cinta Farhat” “Airmata dan Doa”</p></td>
                        <td><p>2013 - Album Pedoman 2 - “ Tak kan Berpaling Darimu” (bersama artis Dato’  Seri Shah Rezza, Dato’ Syed Sobrie, Ebby Saiful dan Ahmad Fauzee)</p></td>
                        <td><p>2014 - Single terbaru “ Lukisan Cinta”</p></td>
                    </tr>
                    <tr>
                        <td><p>2014 - Album Himpunan Lagu-lagu Raya “Seni Merindu Di”Aidilfitri’</p></td>
                        <td><p>2014 - Lagu Tema Filem “ Permata Hati”</p></td>
                        <td><p>2015 - Album terbaru Religi dan ketuhanan -  “ Lukisan Cinta”</p></td>
                    </tr>
                    <tr>
                        <td><p>2016 - Single Duet bersama Adibah Noor - Mainan Jiwa</p></td>
                        <td><p>2016 - Album “Evergreen Klasik“</p></td>
                        <td></td>
                    </tr>
                        </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </section>

        <section class="pageCategory-section last section-grey">
          <div class="container">
            <div class="lineup text-center">
                <h1 class="subSecTitle"><strong>THE LINEUP</strong></h1>
                <p>Performers that will be performed</p>
                <ul class="list-unstyled list-inline lineupList">
                    <li><img src="images/nassier/nassier.jpg" alt="">Nassier Wahab</li>
                    <li><img src="images/nassier/kris-patt.jpg" alt="">Kris Patt</li>
                </ul>
            </div>
            <div class="gallery">
              <!-- Swiper -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img class="" src="images/nassier/Nassier_Wahab_30Years2.jpg" alt="">
                  </div>
                  <div class="swiper-slide">
                    <img class="" src="images/nassier/Nassier_Wahab_30Years_event.jpg" alt="">
                  </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
              </div><!-- /Swiper -->
            </div>
          </div>
        </section>

        <section class="pageCategory-section last"><a id="anchorPrice"></a>
          <div class="container tixPrice">
            <div class="row">
              <div class="col-sm-offset-1 col-sm-10 ">
                <div class="text-center">
                  <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                  <p>Select ticket</p>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="table-responsive">
                    <table class="table infoTable-D">
                        <thead>
                        <tr>
                            <th>Price Category</th>
                            <th>Early Bird*<br>(Till 20 Dec 2016)</th>
                            <th>Normal Price*</th>
                            <th>Remarks</th>
                            <th>Seating Plan</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><span class="box yellow">VVIP</span></td>
                                <td>RM 300</td>
                                <td>RM 300</td>
                                <td>Meet & Greet + New Year Buffet Package</td>
                                <td rowspan="11"><img class="img-responsive seatPlanImg" src="images/nassier/map.jpg" alt=""></td>
                            </tr>
                            <tr>
                                <td><span class="box green">Platinum</span></td>
                                <td>RM 150</td>
                                <td>RM 200</td>
                                <td>Free seating</td>
                            </tr>
                            <tr>
                                <td><span class="box orange">Gold</span></td>
                                <td>RM 100</td>
                                <td>RM 150</td>
                                <td>Free seating</td>
                            </tr>
                            <tr>
                                <td><span class="box red">Silver</span></td>
                                <td>RM 80</td>
                                <td>RM 100</td>
                                <td>Free seating</td>
                            </tr>
                            <tr>
                                <td><span class="box purple">Bronze</span></td>
                                <td>RM 65</td>
                                <td>RM 80	</td>
                                <td>Free Standing</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="buyAlert-bar">
                  <a class="btn btn-danger disabled" id="buyButton" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/nassier%20wahab%20celebrating%2030%20years/events" role="button">BUY TICKETS</a>
                  <span class="or">/</span>
                  <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span>
                </div>
                <span class="importantNote">* Prices shown EXCLUDE RM3.00 AirAsiaRedTix fee. </span>
                <span class="importantNote">* Cover Charge Price includes One Soft Drink.</span>
                <span class="importantNote">* The organiser reserves the right to change the flow of the agenda and times of performances as they may be additional guest artist invited to perform. The showcase is from 2.30pm-7.00pm.</span>
                
                <div class="note text-left">
                    <h2>Reminder</h2>
                    <p><strong>Dress code</strong><br>Neat and decent. For Males shorts, slippers, and sandals are STRICTLY NOT ALLOWED.</p>
                    <p><strong>Minimum age</strong><br>6 years & above</p>
                    <p><strong>Outside F&B</strong><br>Outside Food and drinks are not allowed.</p>
                    <p>1. Each ticket is valid for one-time admission only.</p>
                    <p>2. Child must be of 95cm height and below to be eligible for child ticket priced at RM50 without seat, while child at height above 95cm must purchase tickets according to price scale.</p>
                    <h2>IMPORTANT NOTE:</h2>
                    <ol>
                        <li>Prices shown inclusive RM3.00 AirAsiaRedTix fee and 6% GST.</li>
                        <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                        <li>RM5.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                        <li>Strictly no replacement for missing tickets and cancellation.</li>
                        <li>Online ticket selling will close 2 days prior to event day, subject to availability.</li>
                    </ol>
                    <h2>For enquiry only:</h2>
                    <p>Email to <a href="mailto:support_redtix@airasia.com">support_redtix@airasia.com</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="pageCategory-section last">
          <div class="buyNowSec" style="background-image: url('images/event2.jpg')">
            <div class="bannerOverlay"></div>
            <div class="container">
              <div class="col-sm-offset-1 col-sm-10 quoteArea">
                <h1>You’ve heard him on the radio, you’ve seen him on TV. It’s about time. See him live!</h1>
              </div>
              <div class="btnArea">
                <a class="btn btn-black btn-lg" target="_blank" href="http://redtix-tickets.airasia.com/en-AU/shows/nassier%20wahab%20celebrating%2030%20years/events" role="button">GET TICKET ONLINE NOW <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                <div class="clearfix">&nbsp;</div>
                <span data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets</span>
              </div>
            </div>
          </div>
        </section>

        {{-- <section class="pageCategory-section last">
          <div class="container">
            <div class="partner text-center">
                <h1 class="subSecTitle"><strong>PARTNERS</strong></h1>
                <div class="clearfix">&nbsp;</div>
                <ul class="list-unstyled list-inline partnerList">
                    <li><img src="images/neon/aabig_logo (offical partner).png" alt=""></li>
                    <li><img src="images/neon/BAC.png" alt=""></li>
                    <li><img src="images/neon/durex (official partner).png" alt=""></li>
                    <li><img src="images/neon/fac3 (co-organiser).jpg" alt=""></li>
                    <li><img src="images/neon/seven friday (official watch).jpeg" alt=""></li>
                    <li><img src="images/neon/sunway lagoon (official venue).png" alt=""></li>
                </ul>
            </div>
          </div>
        </section> --}}

      </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 450;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>
    
@endsection

@section('modal')

    <!--Modal Seat Plan image-->
    <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
            </div>
        </div>
        </div>
    </div>

    <!-- Modal Where to Get Tix Location-->
    <div id="modalGetTixLoc" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                <h2>GET YOUR TICKETS FROM:</h2>
                <dl class="dl-horizontal">
                    <dt>Online:</dt>
                    <dd><a href="http://www.airasiaredtix.com">www.AirAsiaRedtix.com</a></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Rock Corner outlets:</dt>
                    <dd>The Gardens, Mid Valley (TEL: 03 - 2201 4893)</dd>
                    <dd>Subang Parade (TEL: 03 - 5613 1139)</dd>
                    <dd>The Curve (TEL: 03 - 7733 1139)</dd>
                    <dd>Bangsar Village (TEL: 03-22021139)</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Victoria Music outlets:</dt>
                    <dd>Sg Wang(TEL: 03 - 2148 7208)</dd>
                    <dd>Amcorp (TEL: 03 - 7956 0592)</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Penang outlets:</dt>
                    <dd>Artist Gallery, Gurney Plaza (04-637 0191)</dd>
                    <dd>Artist Gallery, Queensbay Mall (04-228 5648)</dd>
                </dl>
            </div>
        </div>
        </div>
    </div>

@endsection