@extends('master')
@section('title')
    Isle to Isle: Design Forum
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Isle to Isle: Design Forum" />
    <meta property="og:description" content="Isle to Isle: Design Forum"/>
    <meta property="og:image" content="{{ Request::Url().'images/isletoisle/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/isletoisle/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Isle to Isle: Design Forum"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/isletoisle/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Isle to Isle: Design Forum">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Isle to Isle: Design Forum</h6> Tickets from <span>RM80</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  19th July 2019</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Royale Chulan Penang, Malaysia <a target="_blank" href="https://goo.gl/maps/dm3aReXPfLJETHTv7">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i>  9.00am - 5.00pm</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <div class="col-sm-12">
                                <h2>Isle to Isle: Design Forum</h2><br/>                                

                                <p>Creative Design Drives Competitiveness</p>

                                <p>The 6 top-notch designers and curators, all the way from Taiwan, will gather and take centre stage in Penang at George Town Festival. These big names in the industry are in fact, the ones who design and curate some iconic events as well as awards ceremonies in Taiwan which comprehensively showcase design aesthetics, with Creative Expo Taiwan, World Design Capital, Golden Horse Awards, Golden Melody Awards, and Wordwave Festival to name a few. We cordially invite you to join us for a fruitful period of 8 hours as these influential elites share some insightful experiences and thoughts with you.</p>

                            </div>
                            {{-- <div class="col-sm-4">
                                <iframe src="https://open.spotify.com/embed/artist/6deZN1bslXzeGvOLaLMOIF" width="300" height="400" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div> --}}
                        </div>
                        {{-- <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="embed-responsive embed-responsive-16by9" style="margin-top: 40px; margin-bottom: 40px;">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/P8ZRL8FGMNw?ecver=2" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </section> 

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/isletoisle/gallery-1.jpg" data-featherlight="image"><img class="" src="images/isletoisle/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/isletoisle/gallery-2.jpg" data-featherlight="image"><img class="" src="images/isletoisle/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/isletoisle/gallery-3.jpg" data-featherlight="image"><img class="" src="images/isletoisle/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/isletoisle/gallery-4.jpg" data-featherlight="image"><img class="" src="images/isletoisle/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/isletoisle/gallery-5.jpg" data-featherlight="image"><img class="" src="images/isletoisle/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/isletoisle/gallery-6.jpg" data-featherlight="image"><img class="" src="images/isletoisle/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/isletoisle/gallery-7.jpg" data-featherlight="image"><img class="" src="images/isletoisle/gallery-7.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>
         
            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Normal Price</th>
                                            <th>Early Bird<br/>(ends 30th Jun 11.59pm)</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                        
                                        <tr>
                                            <td>Standard</td>
                                            <td class="text-muted">RM 120</td>
                                            <td>RM 96</td>
                                        </tr>
                                        <tr>                                            
                                            <td>Concession</td>
                                            <td class="text-muted">RM 100</td>
                                            <td>RM 80</td>
                                        </tr>
                                        <tr>                                            
                                            <td>Group of 4</td>
                                            <td colspan="2">RM 360</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="Jul 24 2019 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/en-AU/shows/gtf 2019 - isle to isle/events" role="button">BUY TICKETS</a>
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    {{-- <li>Prices shown include RM4.00 AirAsiaRedTix fee & other taxes.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM30.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li> --}}
                                    <li>Price shown is inclusive of RM 4.00 AirAsiaRedTix fee and will be borne by the organiser. </li>
                                    <li>Transaction fee of RM8.00 will be applied for online purchase.</li>
                                    <li>Reprint fee of RM5.00 will be applied if the online ticket purchaser fails to present the e-ticket for redemption on the day of the show.</li>
                                    <li>No replacement will be made for lost ticket(s) or cancellation.</li>
                                    <li>Tickets are non-transferable and must not be resold or transferred to any person or entity for commercial gain or otherwise.</li>
                                    <li>Online ticket sales will close 1 day prior to the day of the show, subject to availability.</li>                                    
                                    <!-- <li>Strictly no replacement for missing tickets, torn tickets and cancellation.</li> -->
                                    {{-- <li>Online ticket selling will close 1 day prior to event day, subject to availability.</li> --}}
                                </ol>                                
                            </div>
                            <div class="note text-left">
                                <h2>Terms & Conditions</h2>
                                <ol>
                                    <li>The show will commence on time, you are strongly advised to plan your journey in advance to avoid being late for the show.</li>
                                    <li>Late arrival will result in non-admittance until a suitable break in the show, but admission cannot be guaranteed.</li>
                                    <li>Every member of the audience must strictly adhere to the rule pursuant to age limitation which is set for each show by the organiser. The organiser reserves the right to eject any members of the audience who are below the age of 12 from the performance venue.</li>
                                    <li>Each ticket is valid for one-time admission only.</li>
                                    <li>Only students, senior citizens aged 60 and above as well as the disabled (OKU) are entitled for concession ticket.</li>
                                    <li>No audio or video recording and photography are allowed during the show. The organiser reserves the right to delete any unauthorised recordings and/or photos as well as to eject any members of the audience who are suspected of making recordings and/or taking photos in the performance venue.</li>
                                    <li>The use of video cameras, cameras and tablet computers such as iPad is strictly prohibited in the performance venue.</li>
                                    <li>Food and drinks are not allowed in the performance venue.</li>
                                    <li>Lost or damaged ticket(s) will not be entertained.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });


    // Modal Announcement
	//$(document).ready(function(){
    //  $("#announcementModal").modal('show');
  	//});	
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')

   <div id="announcementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <h1><i class="fa fa-bullhorn text-warning" aria-hidden="true"></i></h1>
                    <h4>Announcement</h4>
                    <div class="clearfix">&nbsp;</div>
                    <div class="well">
                        <p>1. Special Limited offer at AirAsia RedTix only</p>
                        <p>2. Malaysian MyKad required at ticket redemption and entry into event for GA Standing MY &amp; GA Standing MY2 tickets</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection