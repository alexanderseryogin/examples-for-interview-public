@extends('master')
@section('title')
    Sunny Side Up Presents: Nick Murphy f.k.a Chet Faker
@endsection

@section('header')
    @include('layouts.partials._header')
    <meta property="og:url" content="{{ Request::fullUrl() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Sunny Side Up" />
    <meta property="og:description" content="Sunny Side Up" />
    <meta property="og:image" content="{{ Request::Url().'images/sunnysideup2018/thumbnail.jpg' }}" />
@endsection

@section('content')
    <!-- Banner Section -->
    <section class="innerPageBanner" style="width: 100%">
        <div class="bigBanner-overlay"></div>
        <div class="jumbotron eventBanner hidden-xs" style="height: auto;"><img src="{{asset('images/sunnysideup2018/web-banner.jpg')}}" style="width: 100%" class="img-responsive" alt="Sunny Side Up Presents: Nick Murphy f.k.a Chet Faker"></div>
        <div class="widewrapper main hidden-lg hidden-md hidden-sm">
            <img src="{{asset('images/sunnysideup2018/thumbnail.jpg')}}" style="width: 100%" class="img-responsive" alt="Sunny Side Up Presents: Nick Murphy f.k.a Chet Faker">
        </div>
    </section>
    <!-- /Banner Section -->

    <!-- Title and Price -->
    <div id="priceFixed" class="section-grey">
        <div class="container">
            <div class="row priceNbtn">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="row">
                        <div class="col-sm-9 leftBox">
                            <h6>Sunny Side Up Presents: Nick Murphy f.k.a Chet Faker</h6>Tickets from <span>RM183</span>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="btn btn-danger btn-lg getTix-btn btn-block" href="#anchorPrice" role="button">Get Tickets <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    </div>

    <!-- Content Section -->
    <section class="pageContent">
      <!-- Main Body -->
        <div class="mainBodyContent no-btm-mar section-white">
            <section class="pageCategory-section last">
                <div class="container intro">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 leftBar">
                            <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i>  21st July 2018</div>
                            <div class="vanue"><i class="fa fa-map-pin" aria-hidden="true"></i>  Potato Head Beach Club, Bali   <a target="_blank" href="https://goo.gl/maps/ViaZE8gHKT62">View Map</a></div>
                            <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> Saturday, 21 Jul (4.30 pm - 2.00am)</div>
                            <div class="clearfix">&nbsp;</div>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <div class="addthis_inline_share_toolbox"></div>
                            <!-- /sharing -->
                            <hr>
                            <p>Sunny Side Up returns to Bali this July with a stellar line-up of music, mixology, food and culture in the tropical, open-air setting of Potato Head Beach Club. 2018’s performers include Nick Murphy f.k.a Chet Faker, Basenji and Moon Boots.</p>
                            <p>More info: <a href="http://www.sunnysideupfest.com" target="_blank">www.sunnysideupfest.com</a>
                            
                            <h2>Line Up</h2>
                            <p>
                            <img class="image-responsive" src="images/sunnysideup2018/dj-lineup-1.jpg" style="width:10%; height:auto;" alt=""><br/>
                            {{-- <b>Nick Murphy AKA Chet Faker</b><br /> --}}
                            Nick Murphy (formerly Chet Faker) is an award-winning multi-platinum singer/songwriter responsible for that rendition of Blackstreet's "No Diggity" we all know and love. He has a soulful voice which he uses in a wide range of hits such as "Lockjaw" with Flume and his studio album Built on Glass.</p>

                            <p>
                            <img class="image-responsive" src="images/sunnysideup2018/dj-lineup-2.jpg" style="width:10%; height:auto;" alt=""><br/>
                            {{-- <b><u>Moon Boots</</b><br /> --}}
                            Moon Boots is an American DJ/producer based out of Brooklyn. His style is unique and can be described as pop house with a blend of jazz, funk and soul which really can't be categorized. He's toured with Lupe Fiasco, collaborated with Perseus, and is part of Anjunadeep.</p>
                            
                            <p>
                            <img class="image-responsive" src="images/sunnysideup2018/dj-lineup-3.jpg" style="width:10%; height:auto;" alt=""><br/>
                            {{-- <b>Basenji</b><br /> --}}
                            Basenji is an Australian DJ and electronic musician whose breakout moment was being featured in Triple J's Unearthed Competition. He was soon signed on to Future Classic where he has been producing pumping pop-oriented dance tracks along with cutting edge remixes.</p>
                            
                            <p>
                            <img class="image-responsive" src="images/sunnysideup2018/dj-lineup-4.jpg" style="width:10%; height:auto;" alt=""><br/>
                            {{-- <b>HMGNC</b><br /> --}}
                            HMGNC (Homogenic) is an Indonesian electropop trio from Bandung made up of Amandia Syachridar, Dina Dellyana and Grahadea Kusuf. Their tracks are both in Indonesian and English and can be described as synth pop with a fair dose of retro and vapor wave.</p>
                            
                            <p>
                            <img class="image-responsive" src="images/sunnysideup2018/dj-lineup-5.jpg" style="width:10%; height:auto;" alt=""><br/>
                            {{-- <b>Seabass</b><br /> --}}
                            Seabass is a locally based DJ with an eclectic style that blends disco, funk, soul, jazz, rock, electronica and a whole lot more. His musical roots date back to Jakarta warehouse parties and the alt disco scene in mid 2000s San Francisco.</p>
                            
                            <p>
                            <img class="image-responsive" src="images/sunnysideup2018/dj-lineup-7.jpg" style="width:10%; height:auto;" alt=""><br/>
                            {{-- <b>Stuart Mclellan</b><br /> --}}
                            Stuart started DJing in London in 1990, and in ‘94 he founded Pacific Records label, which would become the defining techno and house label of that generation. Having now been based in Bali for several years, Stu has garnered a reputation as one of the foremost DJs on the island.</p>
                            
                            <p>
                            <img class="image-responsive" src="images/sunnysideup2018/dj-lineup-6.jpg" style="width:10%; height:auto;" alt=""><br/>
                            {{-- <b>Dea</b><br /> --}}
                            Dea Barandana is a DJ, multi-instrumentalist and sound designer who's spent years hunting obscure records in charity shops across Europe. Now the mastermind behind Bali’s only audiophile space, Studio Eksotika, Dea continues to showcase vinyl collected from every corner of the planet.</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>

            <section class="pageCategory-section last section-grey">
                <div class="container">
                    <div class="gallery text-center">
                        <h1 class="subSecTitle"><strong>GALLERY</strong></h1>
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-dj-1.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-dj-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-dj-2.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-dj-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-dj-3.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-dj-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-dj-4.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-dj-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-dj-5.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-dj-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-dj-6.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-dj-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-dj-7.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-dj-7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-1.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-1.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-2.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-2.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-3.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-3.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-4.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-4.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-5.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-5.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-6.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-6.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-7.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-7.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-8.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-8.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-9.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-9.jpg" alt=""></a>
                                </div>
                                <div class="swiper-slide">
                                    <a href="images/sunnysideup2018/gallery-10.jpg" data-featherlight="image"><img class="" src="images/sunnysideup2018/gallery-10.jpg" alt=""></a>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div><!-- /Swiper -->
                    </div>
                </div>
            </section>

            <section class="pageCategory-section last"><a id="anchorPrice"></a>
                <div class="container tixPrice">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10 ">
                            <div class="text-center">
                                <h1 class="subSecTitle"><strong>TICKET PRICE</strong></h1>
                                <p>Select ticket</p>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="table-responsive">
                                <table class="table infoTable-D table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Ticket Price</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>GA - Early Bird</td>
                                            <td>IDR 600,000 / RM 183.00</td>
                                            <td>Limited Tickets Available</td>
                                        </tr>
                                        <tr>
                                            <td>GA - Pre Sale</td>
                                            <td>IDR 700,000 / RM 214.00</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>GA - Door Sale</td>
                                            <td>IDR 600,000 / RM 183.00</td>
                                            {{-- <td> Not available for online purchase<br />Early entry (Before 6pm arrival &amp; limited numbers)</td> --}}
                                            <td>(Arrive before 6pm limited numbers)</td>
                                        </tr>
                                        {{-- <tr>
                                            <td>VIP &amp; Lounge</td>
                                            <td>IDR 1,100,000 / RM 336.00</td>
                                            <td></td>
                                        </tr> --}}

                                    </tbody>
                                </table>
                            </div>
                            <div class="buyAlert-bar">
                                <a class="btn btn-danger" id="buyButton" datetime="July 20 2018 00:00:00 GMT+0800" target="_blank" href="https://redtix-tickets.airasia.com/loader.aspx/?target=hall.aspx?event=5338" role="button">BUY TICKETS</a>
                                {{-- <span class="or">/</span>
                                <span class="popData-btn" data-toggle="modal" data-target="#modalGetTixLoc">Buy Ticket From Physical Outlets <i class="fa fa-info-circle" aria-hidden="true"></i></span> --}}
                            </div>
                            <div class="note text-left">
                                <h2>Important Notes</h2>
                                <ol>
                                    <li>Prices shown exclude RM8.00 AirAsiaRedTix fee.</li>
                                    <li>All ticket prices will be paid in Malaysian Ringgit. Indonesian Rupiah prices are shown for reference purposes only.</li>
                                    <li>Prices quoted in IDR may differ due to currency fluctuations.</li>
                                    <li>Transaction fee of RM8.00 per event applicable for Internet purchase.</li>
                                    <li>RM10.00 reprint fee applicable if the online ticket purchaser fail to present the e-ticket for redemption on the event day.</li>
                                    <li>Strictly no replacement for missing tickets and cancellation.</li>
                                    <li>Online ticket selling will close 1 days prior to event day, subject to availability.</li>
                                    <li>Adult: 18 years old and above.</li>
                                </ol>
                                <h2>For enquiry only:</h2>
                                <p>Email to <a href="mailto:support@airasiaredtix.com">support@airasiaredtix.com</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /Main Body -->
    </section><!-- /Content Section -->

@endsection

@section('customjs')

    <script type="text/javascript">
    //Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',        
        paginationClickable: true,
        slidesPerView: 'auto',
        spaceBetween: 10,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: true
    });

    // Enlarge Seat Plan Image
    $(function() {
        $('.seatPlanImg').on('click', function() {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
        });
    });

    // Hide top Banner when page scroll
    var header = $('.eventBanner');
    var range = 350;

    $(window).on('scroll', function () {
        
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height;
        var calc = 1 - (scrollTop - offset + range) / range;

        header.css({ 'opacity': calc });

        if ( calc > '1' ) {
        header.css({ 'opacity': 1 });
        } else if ( calc < '0' ) {
        header.css({ 'opacity': 0 });
        }
    });

    // Smooth scroll for acnhor links
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: target.offset().top
            }, 1000);
            return false;
        }
        }
    });
    </script>

    {{-- Buy button disable --}}
    <script type="text/javascript">
        $(function() {
            $('a[id^=buyButton]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).addClass('disabled');
                }
            });
        });
    </script>

@endsection

@section('modal')
    @include('layouts.partials.modals._seatplan')
    @include('layouts.partials.modals._getTix')
@endsection