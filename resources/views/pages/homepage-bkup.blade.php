@extends('master')

@section('title')
    The Passport to Entertainment
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')
<link type="text/css" rel="stylesheet" href="css/maincustom.css">
<link type="text/css" rel="stylesheet" href="css/bannercustom.css">
    <!-- Main Page Banner Section -->
    <section id="pageSlider2">
      <div id="pageSlider-slide" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#pageSlider-slide" data-slide-to="0" class="active"></li>
          <li data-target="#pageSlider-slide" data-slide-to="1"></li>
          <li data-target="#pageSlider-slide" data-slide-to="2"></li>
          <li data-target="#pageSlider-slide" data-slide-to="3"></li>
          <li data-target="#pageSlider-slide" data-slide-to="4"></li>
          <li data-target="#pageSlider-slide" data-slide-to="5"></li>
          <li data-target="#pageSlider-slide" data-slide-to="6"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

            <div class="item active">
                <a href="siamsongkran">
                    <picture>
                        <source media="(max-width: 479px)" srcset="images/herobanner/siamsong-thumbnail.jpg">
                        <img class="bannerPoster img-responsive" src="images/herobanner/siamsong-web-banner.jpg"/>
                    </picture>
                </a>
            </div>
            <div class="item">
                <a href="mrscruff">
                    <picture>
                        <source media="(max-width: 479px)" srcset="images/herobanner/mrscruff-thumbnail.jpg">
                        <img class="bannerPoster img-responsive" src="images/herobanner/mrscruff-web-banner.jpg"/>
                    </picture>
                </a>
            </div>
            <div class="item">
                <a href="joeshowcase">
                    <picture>
                        <source media="(max-width: 479px)" srcset="images/herobanner/joeshowcase-thumbnail.jpg">
                        <img class="bannerPoster img-responsive" src="images/herobanner/joeshowcase-web-banner.jpg"/>
                    </picture>
                </a>
            </div>
            <div class="item">
                <a href="soulsisters">
                    <picture>
                        <source media="(max-width: 479px)" srcset="images/herobanner/soul-thumbnail.jpg">
                        <img class="bannerPoster img-responsive" src="images/herobanner/soul-web-banner.jpg"/>
                    </picture>
                </a>
            </div>
            <div class="item">
                <a href="zeeavinjwa">
                    <picture>
                        <source media="(max-width: 479px)" srcset="images/herobanner/zeeavi-thumbnail1.jpg">
                        <img class="bannerPoster img-responsive" src="images/herobanner/zeeavi-web-banner1.jpg"/>
                    </picture>
                </a>
            </div>
            <div class="item">
                <a href="#trending">
                    <picture>
                        <source media="(max-width: 479px)" srcset="images/herobanner/mar2019-thumbnail.jpg">
                        <img class="bannerPoster img-responsive" src="images/herobanner/mar2019-web-banner.jpg"/>
                    </picture>
                </a>
            </div>
            <div class="item">
                <a href="#genting-events">
                    <picture>
                        <source media="(max-width: 479px)" srcset="images/herobanner/genting-2019-thumbnail.jpg">
                        <img class="bannerPoster img-responsive" src="images/herobanner/genting-2019-web-banner.jpg"/>
                    </picture>
                </a>
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" data-target="#pageSlider-slide" role="button" data-slide="prev">
          <span class="fa fa-angle-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" data-target="#pageSlider-slide" role="button" data-slide="next">
          <span class="fa fa-angle-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </section><!-- /Main Page Banner Section -->

    <!-- Event Cards -->
    <section class="pageContent-Home">
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- Main Body -->
                <div class="mainBodyContent col-xs-12">

                    <section class="homeCategory-section" id="trending">
                        {{-- <h1 class="mainSecTitle" id="trendingCount">Trending Events <span>(8)</span></h1> --}}
                        <h1 class="mainSecTitle" id="trendingCount"></h1>
                        {{-- <span class="seeAllBtn"><a href="">See All <i class="fa fa-angle-right" aria-hidden="true"></i></a></span> --}}
                        <div class="row">
                            <div class="eventList-Home">

                                <!--Ticket List-->
                                {{-- <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 1 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="balispirit2019">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/balispirit2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">24 - 31 Mar '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="balispirit2019"><h2>BaliSpirit Festival 2019</h2></a></div>
                                            <div class="location"> Yayasan Bali Purnati, Bali</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From USD30</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 7 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="soulsisters">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/soulsisters2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">5 - 6 Apr '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Concert</div>
                                            <div class="titleEvent"><a href="soulsisters"><h2>Soul Sisters</h2></a></div>
                                            <div class="location"> Istana Budaya, Kuala Lumpur</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM155</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 8 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="joeshowcase">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/hardrock/joeshowcase2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">7 Apr '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Performance</div>
                                            <div class="titleEvent"><a href="joeshowcase"><h2>The Best of Joe Exclusive Showcase</h2></a></div>
                                            <div class="location">  Hard Rock Café, Singapore</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From SGD100</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 9 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="sdc027-mrscruff">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/potatohead/singles/mrscruff2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">8 Apr '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Performance</div>
                                            <div class="titleEvent"><a href="sdc027-mrscruff"><h2>Sun Down Circle #027: Mr. Scruff</h2></a></div>
                                            <div class="location"> Potato Head Beach Club, Bali</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From IDR150,000</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 10 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="asaprockyilluzion">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/illuzion/asaprocky2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">9 Apr '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Performance</div>
                                            <div class="titleEvent"><a href="asaprockyilluziony"><h2>A$AP ROCKY at Illuzion | TUE 09 APR</h2></a></div>
                                            <div class="location"> Illuzion Phuket, Thailand</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM134</div>
                                        </div>
                                    </div>
                                </div> --}}

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 14 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="sonarhongkong">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/sonarhk2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">13 Apr '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="sonarhongkong"><h2>Sonar Hong Kong</h2></a></div>
                                            <div class="location"> Hong Kong Science Park, Hong Kong</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From USD90</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 15 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="volofestival">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/volofestival2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">13 - 14 Apr '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="volofestival"><h2>VOLO Festival</h2></a></div>
                                            <div class="location"> Vana Nava Water Jungle, Huahin, Thailand</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM340</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 16 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="siamsongkran">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/siamsongkran2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">12 - 15 Apr '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="siamsongkran"><h2>Siam Songkran Music Festival</h2></a></div>
                                            <div class="location">  Show DC Arena, Rama 9, Bangkok</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM263</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 16 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="s2osongkran">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/s2osongkran2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">13 - 15 Apr '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="s2osongkran"><h2>Pepsi Presents S2O Songkran Music Festival 2019</h2></a></div>
                                            <div class="location">  Live Park Rama 9, Bangkok</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM2500</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 21 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="ningliveatzoonegara">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/ningzoo2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">20 Apr '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Concert</div>
                                            <div class="titleEvent"><a href="ningliveatzoonegara"><h2>Ning LIVE @ ZOO NEGARA</h2></a></div>
                                            <div class="location">  Show Amphitheatre - Zoo Negara, Kuala Lumpur</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM100</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 28 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="sufiansuhaimi">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/hardrock/sufiansuhaimi2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">27 Apr '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Concert</div>
                                            <div class="titleEvent"><a href="sufiansuhaimi"><h2>Sufian Suhaimi Live in Singapore</h2></a></div>
                                            <div class="location">  Hard Rock Café Singapore</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From SGD77</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                {{-- <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 29 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="beyondbordersfestival2019">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/beyondbordersfestival2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">30 Apr '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="beyondbordersfestival2019"><h2>Beyond Borders Festival</h2></a></div>
                                            <div class="location">  Allianz Ecopark Ancol, Jakarta</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From IDR 977,500</div>
                                        </div>
                                    </div>
                                </div> --}}

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="May 5 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="togetherfestival2019">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/togetherfestival2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">3 May '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="togetherfestival2019"><h2>Together Festival 2019</h2></a></div>
                                            <div class="location">  Bangkok Internatioal Trade and Exhibition Centre, Thailand</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM410</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="May 6 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="garminperformance">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/garminperformance2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">5 May '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Sports</div>
                                            <div class="titleEvent"><a href="garminperformance"><h2>Garmin The Performance Series Road Race 1: Emerge</h2></a></div>
                                            <div class="location"> Punggol Waterway, Singapore</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From SGD45</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="May 9 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="futureliveinsingapore">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/lamc/singles/future2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">9 May '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="futureliveinsingapore"><h2>Future - Live in Singapore</h2></a></div>
                                            <div class="location"> Zepp@Bigbox, Singapore</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM445</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                {{-- <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="May 18 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="summernoise2019">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/summernoise2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">18 May '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Performance</div>
                                            <div class="titleEvent"><a href="summernoise2019"><h2>Summer Noise 2019</h2></a></div>
                                            <div class="location"> Circuit Makati, Manila</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM270</div>
                                        </div>
                                    </div>
                                </div> --}}

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="May 26 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="aiaglowfestival">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/glowfest2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">25 May '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="aiaglowfestival"><h2>AIA Glow Festival</h2></a></div>
                                            <div class="location"> Palawan Beach, Sentosa, Singapore</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From SGD17.10</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="June 23 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="zeeavinjwa">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/intwomenzeeavinjwa2019/thumbnail1.jpg')"></div>
                                                    <div class="dateEvent concert">22 Jun '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Concert</div>
                                            <div class="titleEvent"><a href="zeeavinjwa"><h2>International Women's Day Zee Avi + NJWA Concert</h2></a></div>
                                            <div class="location"> PTWC, Kuala Lumpur</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM88</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                {{-- <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jul 22 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="internationalchampionscup">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/intchampionscup2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">20 - 21 Jul '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Sports</div>
                                            <div class="titleEvent"><a href="internationalchampionscup"><h2>International Champions Cup 2019</h2></a></div>
                                            <div class="location"> National Stadium, Singapore</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From SGD42</div>
                                        </div>
                                    </div>
                                </div> --}}

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Aug 5 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="tough-mudder-2019">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/toughmudder2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">4 Aug '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Sports</div>
                                            <div class="titleEvent"><a href="tough-mudder-2019"><h2>Tough Mudder 2019 - Philippines</h2></a></div>
                                            <div class="location"> Amore at Portofino, Alabang, Philippines</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM265</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jun 10 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="ultrasingapore2019">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/ultrasg2019/thumbnail-singtel.jpg')"></div>
                                                    <div class="dateEvent concert">8 - 9 Jun '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="ultrasingapore2019"><h2>Ultra Singapore 2019</h2></a></div>
                                            <div class="location"> Ultra Park - One Bayfront Avenue, Singapore</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From SGD188</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jul 8 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="wakeup">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/wakeup2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">5 - 7 Jul '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Festival</div>
                                            <div class="titleEvent"><a href="wakeup"><h2>Wake Up Festival 覺醒音樂祭</h2></a></div>
                                            <div class="location"> Chiayi City, Taiwan</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM302</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Sep 23 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="f1singapore2019">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/f1singapore2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert">20 - 22 Sep '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Sports</div>
                                            <div class="titleEvent"><a href="f1singapore2019"><h2>FORMULA 1 SINGAPORE AIRLINES SINGAPORE GRAND PRIX 2019</h2></a></div>
                                            <div class="location"> Marina Bay Circuit, Singapore</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From SGD188</div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end -->

                            </div>
                        </div>
                    </section>

                    {{-- <section class="homeCategory-section" id="georgetownfestival">
                        <h1 class="mainSecTitle" id="georgetownfestivalCount"></h1>
                        <!-- <span class="seeAllBtn"><a href="">See All <i class="fa fa-angle-right" aria-hidden="true"></i></a></span> -->
                        <div class="row">
                            <div class="eventList-Home">

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Aug 6 2018 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="kelantan">
                                                <div class="thumb" style="background-image: url('images/gtf2018/kelantan/thumbnail.jpg')"></div>
                                                <div class="dateEvent concert">4 - 5 Aug '18</div>
                                                <div class="viewEvent"><span>View Event</span></div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Music</div>
                                            <div class="titleEvent"><a href="kelantan"><h2>Kelantan</h2></a></div>
                                            <div class="location">Dewan Sri Pinang Auditorium, George Town, Penang</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM25</div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section> --}}

                    <section class="homeCategory-section last" id="genting-events">
                        {{-- <h1 class="mainSecTitle">Genting Events <span>(5)</span></h1> --}}
                        <h1 class="mainSecTitle" id="gentingCount"></h1>
                        {{-- <span class="seeAllBtn"><a href="">See All <i class="fa fa-angle-right" aria-hidden="true"></i></a></span> --}}
                        <div class="row">
                            <div class="eventList-Home">

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Mar 17 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="engelberthumperdinck">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/engelbert2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 16 Mar '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="engelberthumperdinck"><h2>Engelbert Humperdinck The Angel On My Shoulder Tour</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM200</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Mar 24 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="reneliu">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/reneliu2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 24 Mar '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="reneliu"><h2>Rene Liu 'All-In' World Tour 2019</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM204</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 1 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="secret">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/secret2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 30 Mar '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="secret"><h2>"SECRET" Stage Play in Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM192</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Apr 14 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="feiyuching">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/feiyuching2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Fri-Sat, 12-13 Apr '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="feiyuching"><h2>Fei Yu Ching Farewell World Tour Concert 2019</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM256</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="May 12 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="terrylin">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/terrylin2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 11 May '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="terrylin"><h2>Terry Lin ONE TAKE 2.0 Live in Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM210</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jun 2 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="comedyshowmariacodero">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/comedymaria2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 1 Jun '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="comedyshowmariacodero"><h2>Comedy Show Maria Cordero & Friends Live in Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM204</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jun 9 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="tsaichin">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/tsaichin2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 8 Jun '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="tsaichin"><h2>Tsai Chin Live in Genting 2019</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM130</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Jun 21 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="alin">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/alin2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 20 Jul '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="alin"><h2>A-Lin Live in Genting 2019</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM222</div>
                                        </div>
                                    </div>
                                </div>

                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="Aug 4 2019 00:00:00 GMT+0800">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="jackywu52019">
                                                <div class="top">
                                                    <div class="thumb" style="background-image: url('images/genting/jackywu2019/thumbnail.jpg')"></div>
                                                    <div class="dateEvent concert"> Sat, 3 Aug '19</div>
                                                    <div class="viewEvent"><span>View Event</span></div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">GENM</div>
                                            <div class="titleEvent"><a href="jackywu52019"><h2>Jacky Wu & Family 52019 Live in Genting</h2></a></div>
                                            <div class="location"> Arena Of Stars, Genting Highland</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">From RM160</div>
                                        </div>
                                    </div>
                                </div>


                                <!-- end -->

                            </div>
                        </div>
                    </section>

                  {{--   <section class="homeCategory-section last" id="upcoming">
                        <h1 class="mainSecTitle">Events You Might Be Interested <span>(1)</span></h1>
                        <div class="row">
                            <div class="eventList-Home">
                                <!--Ticket List-->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="eventCard">
                                        <div class="top">
                                            <a href="https://goo.gl/forms/pIC136YOLW6grTo43" target="_blank">
                                                <div class="thumb" style="background-image: url('images/milff2018/thumbnail.jpg')"></div>
                                                <div class="dateEvent sports">TBA 2018</div>
                                                <div class="viewEvent"><span>Register Now</span></div>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <div class="category">Concert</div>
                                            <div class="titleEvent"><a href="https://goo.gl/forms/pIC136YOLW6grTo43" target="_blank"><h2> MILFF Concert Series 2018</h2></a></div>
                                            <div class="location">Malaysia</div>
                                        </div>
                                        <div class="bottom">
                                            <div class="price">Price TBA</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section> --}}

                    {{-- <section class="homeCategory-section last">
                        <h1 class="mainSecTitle">Discounted / Promotions <span>(7)</span></h1>
                        <span class="seeAllBtn"><a href="">See All <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                        
                        <!-- Swiper -->
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide" data-swiper-autoplay="2000">
                                    <div class="discountedCard">
                                    <a href="">
                                        <div class="top">
                                        <div class="thumb" style="background-image: url('images/discount.jpg')"></div>
                                        <div class="titleEvent">
                                            <h2>CIMB 20% Off</h2>
                                        </div>
                                        </div>
                                    </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="discountedCard">
                                    <a href="">
                                        <div class="top">
                                        <div class="thumb" style="background-image: url('images/bigmainbanner.jpg')"></div>
                                        <div class="titleEvent">
                                            <h2>Maybank Cards 90% Off only on Tuesday</h2>
                                        </div>
                                        </div>
                                    </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="discountedCard">
                                    <a href="">
                                        <div class="top">
                                        <div class="thumb" style="background-image: url('images/bigmainbanner.jpg')"></div>
                                        <div class="titleEvent">
                                            <h2>Discount 10% off for student</h2>
                                        </div>
                                        </div>
                                    </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="discountedCard">
                                    <a href="">
                                        <div class="top">
                                        <div class="thumb" style="background-image: url('images/bigmainbanner.jpg')"></div>
                                        <div class="titleEvent">
                                            <h2>Free for AirAsia Staff</h2>
                                        </div>
                                        </div>
                                    </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="discountedCard">
                                    <a href="">
                                        <div class="top">
                                        <div class="thumb" style="background-image: url('images/bigmainbanner.jpg')"></div>
                                        <div class="titleEvent">
                                            <h2>Maybank Cards 90% Off</h2>
                                        </div>
                                        </div>
                                    </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="discountedCard">
                                    <a href="">
                                        <div class="top">
                                        <div class="thumb" style="background-image: url('images/bigmainbanner.jpg')"></div>
                                        <div class="titleEvent">
                                            <h2>Maybank Cards 90% Off</h2>
                                        </div>
                                        </div>
                                    </a>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="discountedCard">
                                    <a href="">
                                        <div class="top">
                                        <div class="thumb" style="background-image: url('images/bigmainbanner.jpg')"></div>
                                        <div class="titleEvent">
                                            <h2>Maybank Cards 90% Off</h2>
                                        </div>
                                        </div>
                                    </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Scrollbar -->
                            <div class="swiper-scrollbar"></div>
                        </div><!-- /Swiper -->
                    </section> --}}
                </div>
                <!-- /Main Body -->
            </div>
            <!-- /row -->
        </div>
    </section>
    <!-- /Event Cards -->
    
@endsection

@section('customjs')
    <script type="text/javascript">
    // Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        scrollbar: '.swiper-scrollbar',
        scrollbarHide: false,
        slidesPerView: 4,
        spaceBetween: 15,
        grabCursor: true,
        breakpoints: {
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });

    // Main Slider Interval Time
    $('.carousel').carousel({
        interval: 1000 * 2.5
    });

     $(document).ready(function(){
        // Session based Pop up modal
        //  if(typeof(Storage) !== "undefined") {
        //     if(!sessionStorage.getItem('modal')) {                          // if the session key is not exist
        //         sessionStorage.setItem('modal', 'true');                    // create new session
        //         $("#announcementModal").modal('show')                       // show modal dialog
        //     }
        // } 

        // Add smooth scrolling to all links
        $("a").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1000, function(){
        
                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
            } // End if
        });

        if ($('#priceFixed').length > 0){

            $('#priceFixed').scrollToFixed({
                marginTop: $('.mainHeader').outerHeight(true),
                limit: $('.tixPrice').offset().top - $('.mainHeader').outerHeight()
            });
        };

    });
    </script>

    {{-- Hide expired event --}}
    <script type="text/javascript">
        $(function() {
            $('div[id^=hideExpired]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).remove();
                } else {
                    $(this).addClass('eventLaunced');
                }
            });
        });
    </script>

    {{-- Show Launched Event --}}
    <script type="text/javascript">
    $(function() {
        $('div[id^=showToLaunch]').each(function() {
            var date = new Date();
            var launchdate = $(this).attr('datetime'); 
            if ( Date.parse(date) < Date.parse(launchdate)) {
              $(this).remove();
            } 
            else {
                $(this).addClass('eventLaunced');
            }
        });
    });
    </script>

    {{-- event counter --}}
    <script type="text/javascript">
        $(function() {
            var countTrending = $('#trending .row .eventList-Home .eventLaunced').length;
            var countGeorgetownFestival = $('#georgetownfestival .row .eventList-Home .eventLaunced').length;
            var countGenting = $('#genting-events .row .eventList-Home .eventLaunced').length;
            $("#trendingCount").append("Trending Events <span>" + "("+ countTrending +")" + "</span>");
            $("#georgetownfestivalCount").append("Georgetown Festival Events <span>" + "("+ countGeorgetownFestival +")" + "</span>");            
            $("#gentingCount").append("Genting Events <span>" + "("+ countGenting +")" + "</span>");
         });
    </script>

@endsection


@section('modal')

<!--Modal Announcement-->
{{-- <div id="announcementModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" style="width: 100%">
        <div class="modal-body text-center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <div class="modcustom">
                <p><center><img class="img-responsive" src="images/main/soundvalley/sv-popup.png" align="middle"></center></p>
                <a class="btn btn-danger enabled" id="buyButton" target="_blank" href="https://goo.gl/7EiKxy" role="button">GET TICKETS</a>
            </div>
        </div>
      </div>
    </div>
  </div> --}}
@endsection

