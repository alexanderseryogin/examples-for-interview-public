@extends('errorpage')
@section('title')
    Checkout
@endsection

@section('content')

    <!-- Content Section -->
    <section class="pageCheckout pageContent">
      <div class="container">
        
        <div class="row">

            <!-- Sidebar -->
            <aside class="col-sm-5 col-sm-push-7">
                <!--Order Summary-->
                <div class="order-summary">
                    <div class="eventInfo">
                        <img class="img-responsive" src="images/gnr/gnr-thumb.jpg" alt="">
                        <h2>GUNS N' ROSES - NOT IN THIS LIFETIME TOUR - LIVE IN SINGAPORE 2017</h2>
                        <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> Sat, 25 Feb 2017</div>
                        <div class="location"><i class="fa fa-map-pin" aria-hidden="true"></i> Changi Exhibition Centre</div>
                        <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm</div>
                    </div>

                    <div class="form-coupon">

                        <form method="POST" action="{{ route('coupons.apply') }}" class="form-inline">

                            <h3>Apply Coupon</h3>

                            <div class="form-group">

                                <input type="text" name="coupon" placeholder="Apply your coupon here" class="form-control" required>

                                @if($errors->has('coupon'))

                                    <div class="alert alert-danger" role="alert">{{ $errors->first('coupon') }}</div>

                                @endif

                                @if (session('message'))
                                    <div class="alert alert-success">
                                        {{ session('message') }}
                                    </div>
                                @endif

                            </div>

                            {{ csrf_field() }}

                            <button class="btn btn-success pull-right" type="submit">Apply</button>

                        </form>

                    </div>

                        <table class="table table-hover">
                        <tbody>
                        @foreach($cartitems as $item)
                            <tr>
                                <td>{{ $item->name }} <a href="{{ route('cart.remove.item', $item->rowId) }}">Remove from cart</a></td>
                                <td>{{ $item->qty }}</td>
                                <td>RM {{ $item->price * $item->qty }}</td>
                            </tr>
                        @endforeach
                        <tr class="total">
                            <td>Total</td>
                            <td>{{ $count }}</td>
                            <td>RM {{ $total }}</td>
                        </tr>
                        </tbody>
                    </table>
                    
                    <div class="eventInfo-note">
                        <p>
                            We will send you two emails upon payment:
                            <ol type="a">
                                <li>Payment Confirmation Email</li>
                                <li>Your E-Ticket Email</li>
                            </ol>
                        </p>
                        <p>We will provide the AirAsia promo code in the E-Ticket email.</p>
                    </div>
                </div>
            </aside>
            <!-- /Sidebar -->

          <!-- Main Body -->
          <div class="col-sm-7 col-sm-pull-5">

            <section class="pageCategory-section last main">
                <h1 class="hidden-xs">AirAsiaRedTix</h1>
                <ol class="breadcrumb hidden-xs">
                    <li><a href="#">Back</a></li>
                    <li class="active">Checkout</li>
                </ol>

                    <h2>Customer Information</h2>
                    <p>We need your details so we can send the ticket on your email.</p>

                    <form action="{{ route('order.store') }}" method="POST">


                    <div class="form-group">

                        <input type="text" name="first_name" class="form-control" placeholder="First Name" required>

                        @if($errors->has('first_name'))
                            <br>
                            <div class="alert alert-danger" role="alert">{{ $errors->first('first_name') }}</div>
                        @endif

                    </div>

                    <div class="form-group">

                        <input type="text" name="last_name" class="form-control" placeholder="Last Name" required>

                        @if($errors->has('last_name'))
                            <br>
                            <div class="alert alert-danger" role="alert">{{ $errors->first('last_name') }}</div>
                        @endif

                    </div>

                    <div class="form-group">

                        <input type="text" name="email" class="form-control" placeholder="Email" required>
                        @if($errors->has('email'))
                            <br>
                            <div class="alert alert-danger" role="alert">{{ $errors->first('email') }}</div>
                        @endif

                    </div>
                    
                    <h2>Payment Method</h2>
                    <p>Enter your credit card details below or use PayPal to pay.</p>
                    <div id="dropin-container">

                    </div>

                    <input type="hidden" name="amount" value="{{ $total }}">

                    {{ csrf_field() }}

                    <button class="btn btn-danger btn-lg" role="button">Make Payment</button>

                </form>

                <div class="clearfix">&nbsp;</div>

                @if(Session::has('txn_message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('txn_message') }}</p>
                @endif

                <p>By completing this purchase, you hereby agree to our <a target="_blank" href="purchaseterms">Terms & Conditions</a></p>

                <div class="clearfix hidden-xs">&nbsp;</div>

                <div class="clearfix">&nbsp;</div>

                <hr>

                <small>All rights reserved AirAsiaRedTix</small>
                
            </section>

          </div><!-- /Main Body -->

          

        </div><!-- /row -->

      </div>
    </section><!-- /Content Section -->

@endsection