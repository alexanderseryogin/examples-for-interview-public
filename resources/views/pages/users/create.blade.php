@extends('layouts.app')

@section('title')
    The Passport to Entertainment
@endsection

@section('header')
    @include('layouts.partials._appheader')
@endsection

@section('content')
    <div class="content-box-large">
        @include('layouts.components.breadcrumbs')
    </div>
    <div class="content-box-large">
        <div class="panel-heading">
            <div class="panel-title"><h4>Create user</h4></div>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/users/store') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="col-md-2 control-label">Display Name</label>
                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label for="first_name" class="col-md-2 control-label">First Name</label>
                    <div class="col-md-6">
                        <input id="first_name" type="text" class="form-control" name="first_name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="last_name" class="col-md-2 control-label">Last Name</label>
                    <div class="col-md-6">
                        <input id="last_name" type="text" class="form-control" name="last_name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="address1" class="col-md-2 control-label">Address 1</label>
                    <div class="col-md-6">
                        <input id="address1" type="text" class="form-control" name="address1">
                    </div>
                </div>
                <div class="form-group">
                    <label for="address2" class="col-md-2 control-label">Address 2</label>
                    <div class="col-md-6">
                        <input id="address2" type="text" class="form-control" name="address2">
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-md-2 control-label">City</label>
                    <div class="col-md-6">
                        <input id="city" type="text" class="form-control" name="city">
                    </div>
                </div>
                <div class="form-group">
                    <label for="state" class="col-md-2 control-label">State</label>
                    <div class="col-md-6">
                        <input id="state" type="text" class="form-control" name="state">
                    </div>
                </div>
                <div class="form-group">
                    <label for="postal_code" class="col-md-2 control-label">Postal Code</label>
                    <div class="col-md-6">
                        <input id="postal_code" type="text" class="form-control" name="postal_code">
                    </div>
                </div>
                <div class="form-group">
                    <label for="country" class="col-md-2 control-label">Country</label>
                    <div class="col-md-6">
                        <select class="form-control form-control--select" name="country" id="country">
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}">{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" for="users_timezone" >Timezone</label>
                    <div class="col-md-6">
                        <select class="form-control form-control--select" name="timezone" id="users_timezone">
                            <option value="-">Select your timezone</option>
                            @include('layouts.components.timezone_option_create')
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-md-2 control-label">Email</label>
                    <div class="col-md-6">
                        <input id="email" type="text" class="form-control" name="email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="Password" class="col-md-2 control-label">Password</label>
                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Position</label>
                    <div class="col-md-6">
                        <label class="checkbox-inline">
                          <input name="is_admin" type="checkbox" id="is_admin" value="1">Admin
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-2">
                        <button type="submit" class="btn btn-danger">
                            Create New User
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div> 
        
       
@endsection