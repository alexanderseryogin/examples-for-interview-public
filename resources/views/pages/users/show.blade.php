@extends('layouts.app')

@section('title')
    The Passport to Entertainment
@endsection

@section('header')
    @include('layouts.partials._appheader')
@endsection

@section('content')
	<div class="col-md-12">
		<div class="content-box-large">
	        @include('layouts.components.breadcrumbs')
		</div>
	</div>
	<div class="col-md-4">
		<div class="content-box-large">
			<div class="panel-heading text-center">
				@if(!is_null($user->account))
					@if(is_null($user->account->avatar))
					    <a href="/images/default/profile/profile.png" data-featherlight="image">
					        <img class="center-block" src="/images/default/profile/profile.png" alt="Profile Image" style="width:150px; height:150px; border-radius:50%; margin: 0 auto;">
					    </a>
					@else
						<a href="{{ $account->avatar }}" data-featherlight="image">
					        <img src="{{ $account->avatar }}" alt="{{ $user->name }}" style="width:150px; height:150px; border-radius:50%; margin: 0 auto;">
					    </a>
					@endif
				@else
					<a href="/images/default/profile/profile.png" data-featherlight="image">
				        <img class="center-block" src="/images/default/profile/profile.png" alt="Profile Image" style="width:150px; height:150px; border-radius:50%; margin: 0 auto;">
				    </a>
				@endif
	            <h2>{{ $user->name }}'s Profile</h2>
			</div>
			<a class="btn getTix-btn btn-danger center-block" href="{{ URL::to('/admin/users/'.$user->id.'/delete') }}">Delete user</a>
		</div>
	</div>
	<div class="col-md-8">
		<div class="content-box-large">
			<div class="panel-heading">
				<div class="panel-title"><h4>User's Details</h4></div>
			</div>
			<div class="panel-body">

				<table class="table">
					<tbody>
						<tr>
							<td>Name:</td>
							@if(!is_null($user->name))
								<td>{{ $user->name }}</td>
							@else
								<td>NULL</td>
							@endif
						</tr>
						<tr>
							<td>Email</td>
							@if(!is_null($user->email))
								<td>{{ $user->email }}</td>
							@else
								<td>NULL</td>
							@endif
						</tr>
						<tr>
							<td>Active</td>
							@if($user->is_active)
								<td class="text-success">Active</td>
							@else
								<td class="text-danger">Not Active</td>
							@endif
						</tr>
						<tr>
							<td>Banned</td>
							@if(!$user->is_banned)
								<td class="text-success">Not Banned</td>
							@else
								<td class="text-danger">Banned</td>
							@endif
						</tr>
						@if(!is_null($user->account))
							<tr>
								<td>First Name:</td>
								@if(!is_null($user->account->first_name))
									<td>{{ $user->account->first_name }}</td>
								@else
									<td>NULL</td>
								@endif
							</tr>
							<tr>
								<td>Last Name:</td>
								@if(!is_null($user->account->last_name))
									<td>{{ $user->account->last_name }}</td>
								@else
									<td>NULL</td>
								@endif
							</tr>
							<tr>
								<td>Position</td>
								@if($user->isSuperAdmin())
									<td class="text-success">Admin</td>
								@else
									<td>Regular</td>
								@endif
							</tr>
							<tr>
								<td>Address 1</td>
								@if(!is_null($user->account->address1))
									<td>{{ $user->account->address1 }}</td>
								@else
									<td>NULL</td>
								@endif
							</tr>
							<tr>
								<td>Address 2</td>
								@if(!is_null($user->account->address2))
									<td>{{ $user->account->address2 }}</td>
								@else
									<td>NULL</td>
								@endif
							</tr>
							<tr>
								<td>City</td>
								@if(!is_null($user->account->city))
									<td>{{ $user->account->city }}</td>
								@else
									<td>NULL</td>
								@endif
							</tr>
							<tr>
								<td>State</td>
								@if(!is_null($user->account->state))
									<td>{{ $user->account->state }}</td>
								@else
									<td>NULL</td>
								@endif
							</tr>
							<tr>
								<td>Postal Code</td>
								@if(!is_null($user->account->postal_code))
									<td>{{ $user->account->postal_code }}</td>
								@else
									<td>NULL</td>
								@endif
							</tr>
							<tr>
								<td>Country</td>
								@if(!is_null($user->account->country_id))
									<td>{{ $countries->where('id', $user->account->country_id)->first()->name }}</td>
								@else
									<td>NULL</td>
								@endif
							</tr>
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
@push('scripts')
	<script src="//code.jquery.com/jquery-latest.js"></script>
	<script src="//cdn.rawgit.com/noelboss/featherlight/1.7.12/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
@endpush