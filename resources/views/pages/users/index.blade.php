@extends('layouts.app')

@section('title')
	The Passport to Entertainment
@endsection

@section('header')
    @include('layouts.partials._appheader')
@endsection

@section('content')

	<div class="content-box-large">
        @include('layouts.components.breadcrumbs')
		<h4>Users</h4>
		<a class="btn getTix-btn btn-danger" href="{{ URL::to('/admin/users/create') }}"><i class="glyphicon glyphicon-pencil"></i> Create new user</a>
	</div>
	<!-- Main Body -->
	<div class="content-box-large ">
		<div class="panel-heading">
			<div class="panel-title">Users List</div>
		</div>
			<div class="panel-body table-responsive">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="dataTable">
				<thead>
					<tr>
						<th>#</th>
						<th>ID</th>
						<th>Name</th>
						<th>Status</th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr class="odd gradeX">
						<td>{{ $loop->iteration }}</td>
						<td>{{ $user->id }}</td>
						<td>{{ $user->name }}</td>
						<td>
							@if($user->isSuperAdmin())
								<span class="text-success">Admin</span>
							@else
								User
							@endif
						</td>
						@if(!$user->is_admin)
							<td><a class="btn getTix-btn btn-success" href="{{ URL::to('/admin/users/'.$user->id.'/adminPromote') }}">Promote</a></td>
						@else
							<td><a class="btn getTix-btn btn-danger" href="{{ URL::to('/admin/users/'.$user->id.'/adminPromote') }}">Demote</a></td>
						@endif
						<td><a class="btn getTix-btn btn-danger" href="{{ URL::to('/admin/users/'.$user->id) }}"><i class="glyphicon glyphicon-eye-open"></i> Show User</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection