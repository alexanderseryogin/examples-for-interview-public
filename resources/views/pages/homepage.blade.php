@extends('master')

@section('title')
    The Passport to Entertainment
@endsection

@section('header')
    @include('layouts.partials._header')
@endsection

@section('content')
<link type="text/css" rel="stylesheet" href="css/maincustom.css">
<link type="text/css" rel="stylesheet" href="css/bannercustom.css">
    <!-- Main Page Banner Section -->
    <section id="pageSlider2">
      <div id="pageSlider-slide" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            @foreach($slides as $key => $value)
                <li data-target="#pageSlider-slide" data-slide-to="{{$key}}" class="{{($value['active'] ? 'active' : '')}}"></li>
            @endforeach
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            @foreach($slides as $key => $value)
            <div class="item {{($value['active'] ? 'active' : '')}}">
                <a href="{{$value['href']}}">
                    <picture>
                        <source media="(max-width: 479px)" srcset="{{$value['thumbnail_url']}}">
                        <img class="bannerPoster img-responsive" src="{{$value['web_banner_url']}}"/>
                    </picture>
                </a>
            </div>
            @endforeach
        </div>
        <!-- Controls -->
        <a class="left carousel-control" data-target="#pageSlider-slide" role="button" data-slide="prev">
          <span class="fa fa-angle-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" data-target="#pageSlider-slide" role="button" data-slide="next">
          <span class="fa fa-angle-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </section><!-- /Main Page Banner Section -->
    <!-- Event Cards -->
    <section class="pageContent-Home">
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- Main Body -->
                <div class="mainBodyContent col-xs-12">

                    <!-- trending events-->
                    <section class="homeCategory-section" id="trending">
                        
                        <h1 class="mainSecTitle" id="trendingCount"></h1>
                        
                        <div class="row">
                            <div class="eventList-Home">

                                @foreach($trending['events'] as $event)
                                    
                                    @if(((env('APP_ENV') == "production" && $event['show']) || env('APP_ENV') != "production"))
                                    <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="{{$event['expiry_date']}}">
                                        <div class="eventCard">
                                            <div class="top">
                                                <a href="{{$event['href']}}">
                                                    <div class="top">
                                                        <div class="thumb" style="background-image: url('{{$event['thumbnail_url']}}')"></div>
                                                        <div class="dateEvent concert">{{$event['date']}}</div>
                                                        <div class="viewEvent"><span>View Event</span></div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="content">
                                                <div class="category">{{$event['category']}}</div>
                                                <div class="titleEvent"><a href="{{$event['href']}}"><h2>{{$event['name']}}</h2></a></div>
                                                <div class="location">{{$event['venue']}}</div>
                                            </div>
                                            <div class="bottom">
                                                <div class="price">From {{$event['currency_left'].$event['price'].$event['currency_right']}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach
                                 
                            </div>
                        </div>
                        
                        <div id="genting-events-section"></div>

                    </section>
                    
                    <!-- genting events --> 
                    <section class="homeCategory-section" id="GTF2019">
                        
                        <h1 class="mainSecTitle" id="georgetownfestivalCount"></h1>
                        
                        <div class="row">
                            <div class="eventList-Home">

                                @foreach($gtf['events'] as $event)
                                    @if(((env('APP_ENV') == "production" && $event['show']) || env('APP_ENV') != "production"))
                                    <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="{{$event['expiry_date']}}">
                                        <div class="eventCard">
                                            <div class="top">
                                                <a href="{{$event['href']}}">
                                                    <div class="top">
                                                        <div class="thumb" style="background-image: url('{{$event['thumbnail_url']}}')"></div>
                                                        <div class="dateEvent concert">{{$event['date']}}</div>
                                                        <div class="viewEvent"><span>View Event</span></div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="content">
                                                <div class="category">{{$event['category']}}</div>
                                                <div class="titleEvent"><a href="{{$event['href']}}"><h2>{{$event['name']}}</h2></a></div>
                                                <div class="location">{{$event['venue']}}</div>
                                            </div>
                                            <div class="bottom">
                                                <div class="price">From {{$event['currency_left'].$event['price'].$event['currency_right']}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach

                            </div>
                        </div>
                    </section>

                    <!-- genting events --> 
                    <section class="homeCategory-section last" id="genting-events">
                        
                        <h1 class="mainSecTitle" id="gentingCount"></h1>
                        
                        <div class="row">
                            <div class="eventList-Home">

                                @foreach($genting['events'] as $event)
                                    @if(((env('APP_ENV') == "production" && $event['show']) || env('APP_ENV') != "production"))
                                    <div class="col-sm-4 col-xs-12" id="hideExpired" datetime="{{$event['expiry_date']}}">
                                        <div class="eventCard">
                                            <div class="top">
                                                <a href="{{$event['href']}}">
                                                    <div class="top">
                                                        <div class="thumb" style="background-image: url('{{$event['thumbnail_url']}}')"></div>
                                                        <div class="dateEvent concert">{{$event['date']}}</div>
                                                        <div class="viewEvent"><span>View Event</span></div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="content">
                                                <div class="category">{{$event['category']}}</div>
                                                <div class="titleEvent"><a href="{{$event['href']}}"><h2>{{$event['name']}}</h2></a></div>
                                                <div class="location">{{$event['venue']}}</div>
                                            </div>
                                            <div class="bottom">
                                                <div class="price">From {{$event['currency_left'].$event['price'].$event['currency_right']}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach

                            </div>
                        </div>
                    </section>
                  
                </div>
                <!-- /Main Body -->
            </div>
            <!-- /row -->
        </div>
    </section>
    <!-- /Event Cards -->
    
@endsection

@section('customjs')
    <script type="text/javascript">
    // Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        scrollbar: '.swiper-scrollbar',
        scrollbarHide: false,
        slidesPerView: 4,
        spaceBetween: 15,
        grabCursor: true,
        breakpoints: {
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });

    // Main Slider Interval Time
    $('.carousel').carousel({
        interval: 1000 * 2.5
    });

     $(document).ready(function(){
        // Session based Pop up modal
        //  if(typeof(Storage) !== "undefined") {
        //     if(!sessionStorage.getItem('modal')) {                          // if the session key is not exist
        //         sessionStorage.setItem('modal', 'true');                    // create new session
        //         $("#announcementModal").modal('show')                       // show modal dialog
        //     }
        // } 

        // Add smooth scrolling to all links
        $("a").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1000, function(){
        
                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
            } // End if
        });

        if ($('#priceFixed').length > 0){

            $('#priceFixed').scrollToFixed({
                marginTop: $('.mainHeader').outerHeight(true),
                limit: $('.tixPrice').offset().top - $('.mainHeader').outerHeight()
            });
        };

    });
    </script>

    {{-- Hide expired event --}}
    <script type="text/javascript">
        $(function() {
            $('div[id^=hideExpired]').each(function() {
                var date = new Date();
                var enddate = $(this).attr('datetime'); 
                if ( Date.parse(date) >= Date.parse(enddate)) {
                  $(this).remove();
                } else {
                    $(this).addClass('eventLaunced');
                }
            });
        });
    </script>

    {{-- Show Launched Event --}}
    <script type="text/javascript">
    $(function() {
        $('div[id^=showToLaunch]').each(function() {
            var date = new Date();
            var launchdate = $(this).attr('datetime'); 
            if ( Date.parse(date) < Date.parse(launchdate)) {
              $(this).remove();
            } 
            else {
                $(this).addClass('eventLaunced');
            }
        });
    });
    </script>

    {{-- event counter --}}
    <script type="text/javascript">
        $(function() {
            var countTrending = $('#trending .row .eventList-Home .eventLaunced').length;
            var countGeorgetownFestival = $('#GTF2019 .row .eventList-Home .eventLaunced').length;
            var countGenting = $('#genting-events .row .eventList-Home .eventLaunced').length;            
            $("#trendingCount").append("Trending Events <span>" + "("+ countTrending +")" + "</span>");
            $("#georgetownfestivalCount").append("George Town Festival 2019 Events <span>" + "("+ countGeorgetownFestival +")" + "</span>");            
            $("#gentingCount").append("Genting Events <span>" + "("+ countGenting +")" + "</span>");                        
         });
    </script>

    <script>
        // $(document).ready(function () {
        //     $('.eventCard').click(function () {
        //         let url = $(this).find('a').attr('href');
        //         window.open(url, '_blank');
        //     })
        // })
    </script>

@endsection


@section('modal')

<!--Modal Announcement-->
{{-- <div id="announcementModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" style="width: 100%">
        <div class="modal-body text-center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
            <div class="clearfix">&nbsp;</div>
            <div class="modcustom">
                <p><center><img class="img-responsive" src="images/main/soundvalley/sv-popup.png" align="middle"></center></p>
                <a class="btn btn-danger enabled" id="buyButton" target="_blank" href="https://goo.gl/7EiKxy" role="button">GET TICKETS</a>
            </div>
        </div>
      </div>
    </div>
  </div> --}}
@endsection

