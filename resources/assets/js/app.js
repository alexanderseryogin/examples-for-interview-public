/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./index');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

window.Event = new Vue();

Vue.component('ordersummary', require('./components/Ordersummary.vue'));
Vue.component('tickets', require('./components/Tickets.vue'));
Vue.component('ticket', require('./components/Ticket.vue'));
Vue.component('coupon', require('./components/Coupon.vue'));
Vue.component('access-code-modal', require('./components/AccessCodeModal.vue'));
Vue.component('buy-button', require('./components/BuyButon.vue'));
Vue.component('search-panel', require('./components/SearchPanel.vue'));
Vue.component('package-order-summary', require('./components/PackageOrderSummary.vue'));

// const app = new Vue({
//     el: '.pageContent, .layout',
//     data: {},
//     methods: {},
//     mounted() {}
// });
import VueSingleSelect from "vue-single-select";

Vue.component('vue-single-select', VueSingleSelect);

Vue.use(require('vue-moment'));

Vue.component('event-info-fixtures', require('./components/EventInfoFixtures/EventInfoFixtures.vue'));
Vue.component('date-ranger', require('./components/DateRanger.vue'));
Vue.component('matches', require('./components/EventInfoFixtures/parts/Matches'));
Vue.component('pools', require('./components/EventInfoFixtures/parts/Pools'));
Vue.component('videos', require('./components/EventInfoFixtures/parts/Videos'));
Vue.component('gallery', require('./components/EventInfoFixtures/parts/Gallery'));

Vue.component('package-builder', require('./components/PackageBuilder/PackageBuilder.vue'));

Vue.component('package-builder', require('./components/PackageBuilder/PackageBuilder.vue'));
Vue.component('package-builder-order-summary', require('./components/PackageBuilder/PackageBuilderOrderSummary.vue'));
Vue.component('package-builder-search-panel', require('./components/PackageBuilder/PackageBuilderSearchPanel.vue'));

Vue.component('category', require('./components/PackageBuilder/Category.vue'));
Vue.component('flight', require('./components/PackageBuilder/Flight.vue'));
Vue.component('flight-details', require('./components/PackageBuilder/FlightDetails.vue'));
Vue.component('hotel', require('./components/PackageBuilder/Hotel.vue'));

Vue.component('package-detail', require('./components/PackageDetail/PackageDetail.vue'));
Vue.component('package-detail-ticket', require('./components/PackageDetail/PackageDetailTicket.vue'));
Vue.component('package-detail-flight', require('./components/PackageDetail/PackageDetailFlight.vue'));
Vue.component('package-detail-hotel', require('./components/PackageDetail/PackageDetailHotel.vue'));

Vue.component('package', require('./components/PackagesPage/Package.vue'));
Vue.component('packages', require('./components/PackagesPage/Packages.vue'));

Vue.component('sponsor', require('./components/Sponsor.vue'));
Vue.component('custom-modal', require('./components/EventInfoFixtures/parts/customModal'));
Vue.component('custom-sticky', require('./components/EventInfoFixtures/parts/custom-sticky'));

Vue.filter('currency', function (value) {
    let val = value ? value : 0;
    return val.toLocaleString("en-EN", {minimumFractionDigits: 2, maximumFractionDigits: 2});
})
Vue.filter('currencyShort', function (value) {
    let val = value ? value : 0;
    return val.toLocaleString("en-EN", {minimumFractionDigits: 0, maximumFractionDigits: 0});
})
import {createStore} from './store'

const store = createStore()

const app = new Vue({
    el: '#app',
    store,
    data() {
        return {
            customSticky: false,
        }
    },
    methods: {},
    mounted() {
        $('#customPackagePopup').modal();
        $('#customPackagePopup').on('hidden.bs.modal', () => {
            this.customSticky = true;
        })
    }

});


export const EventBus = new Vue();

const objectFitImages = require('object-fit-images');

$(function(){

    objectFitImages();
    initFixedScrollBlock();
    scrollUp();

    $('.j-toggle-nav').on('click', function (e) {
        e.preventDefault();
        $('html').toggleClass('open-nav');
    });

    $('.collapse').on('shown.bs.collapse', function () {
        $(this).closest('.card').addClass('active');
    });

    $('.collapse').on('hidden.bs.collapse', function () {
        $(this).closest('.card').removeClass('active');
    });

    $('.testimonials-carousel').owlCarousel({
        loop: true,
        nav: false,
        dots: true,
        navSpeed: 600,
        dotsSpeed: 600,
        items: 1,
        autoplay: true,
        autoplayTimeout: 4000,
        smartSpeed: 1000,
        autoplayHoverPause: true
    });

    $('[data-fancybox="gallery"]').fancybox({
        thumbs: {
            autoStart: true,
            axis: 'x'
        },
        mobile: {
            clickSlide: "close"
        }
    });
});

function initFixedScrollBlock() {
    const menu = $('.tabs');
    if(menu&&menu.offset()) {
        const menuPosition = menu.offset().top - 52;

        scrollMenu();
        $(window).scroll(scrollMenu);
    }

    function scrollMenu() {
        if ($(window).scrollTop() > menuPosition) {
            menu.addClass('fixed');
        } else {
            menu.removeClass('fixed');
        }
    }
};

function scrollUp() {
    const navlink =  $('.nav-tabs .nav-link');
    navlink.click(function () {
        $('body,html').animate({
            scrollTop: $('.tabs').offset().top - 52
        }, 500);
    });
}
