$(document).ready(function () {
    $('.sidebar-toggle').click(function () {
        let $body = $('body');
        $body.toggleClass('sidebar-collapse');
        localStorage.setItem('adminSidebar', JSON.stringify({collapsed: $body.hasClass('sidebar-collapse')}));
    });

    $('.main-sidebar .treeview').click(function (e) {
        let $body = $('body');

        if ($body.hasClass('sidebar-collapse')) {
            $body.removeClass('sidebar-collapse');
            if ($(this).hasClass('menu-open')) {
                e.stopPropagation();
            }
        }
    });

    function configureSidebar() {
        let adminSidebarSettings = JSON.parse(localStorage.getItem("adminSidebar"));

        if (!adminSidebarSettings) {
            adminSidebarSettings = {
                collapsed: false
            };
            localStorage.setItem('adminSidebar', JSON.stringify(adminSidebarSettings));
        }

        $('body').toggleClass('sidebar-collapse', adminSidebarSettings.collapsed);
    }

    configureSidebar();
});