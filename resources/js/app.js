/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

// window.Event = new Vue();
//
// Vue.component('ordersummary', require('./components/Ordersummary.vue'));
//
// Vue.component('example', require('./components/Example.vue'));
//
// Vue.component('tickets', require('./components/Tickets.vue'));
//
// Vue.component('ticket', require('./components/Ticket.vue'));
//
// Vue.component('coupon', require('./components/Coupon.vue'));
//
// Vue.component('countdown', require('./components/Countdown.vue'));

require('../assets/js/index');


/*--------------------*/
window.Event = new Vue();

import Tickets from './components/Tickets';
import Ordersummary from './components/Ordersummary';
import Example from './components/Example';
import Ticket from './components/Ticket';
import Coupon from './components/Coupon';
import CountDown from './components/Countdown'

Vue.component('ordersummary', Ordersummary);
Vue.component('example', Example);
Vue.component('tickets', Tickets);
Vue.component('ticket', Ticket);
Vue.component('coupon', Coupon);
Vue.component('countdown', CountDown);

import VueSingleSelect from "vue-single-select";
import EventInfoFixtures from '../../resources/assets/js/components/EventInfoFixtures/EventInfoFixtures';
import DateRanger from '../../resources/assets/js/components/DateRanger';
import Matches from '../../resources/assets/js/components/EventInfoFixtures/parts/Matches';
import Pools from '../../resources/assets/js/components/EventInfoFixtures/parts/Pools';
import Videos from '../../resources/assets/js/components/EventInfoFixtures/parts/Videos';
import Gallery from '../../resources/assets/js/components/EventInfoFixtures/parts/Gallery';
import Sponsor from '../../resources/assets/js/components/Sponsor';
import PackageBuilder from '../../resources/assets/js/components/PackageBuilder/PackageBuilder';
import PackageBuilderOrderSummary from '../../resources/assets/js/components/PackageBuilder/PackageBuilderOrderSummary';
import PackageBuilderSearchPanel from '../../resources/assets/js/components/PackageBuilder/PackageBuilderSearchPanel';
import Flight from '../../resources/assets/js/components/PackageBuilder/Flight';
import FlightDetails from '../../resources/assets/js/components/PackageBuilder/FlightDetails';
import Hotel from '../../resources/assets/js/components/PackageBuilder/Hotel';
import PackageDetail from '../../resources/assets/js/components/PackageDetail/PackageDetail';
import PackageDetailTicket from '../../resources/assets/js/components/PackageDetail/PackageDetailTicket';
import PackageDetailFlight from '../../resources/assets/js/components/PackageDetail/PackageDetailFlight';
import PackageDetailHotel from '../../resources/assets/js/components/PackageDetail/PackageDetailHotel';
import Package from '../../resources/assets/js/components/PackagesPage/Package';
import Packages from '../../resources/assets/js/components/PackagesPage/Packages';
import Category from '../../resources/assets/js/components/PackageBuilder/Category'

import CustomModal from '../../resources/assets/js/components/EventInfoFixtures/parts/customModal';
import CustomSticky from '../../resources/assets/js/components/EventInfoFixtures/parts/custom-sticky';


Vue.component('vue-single-select', VueSingleSelect);
Vue.use(require('vue-moment'));
Vue.component('event-info-fixtures', EventInfoFixtures);
Vue.component('date-ranger', DateRanger);
Vue.component('matches', Matches);
Vue.component('pools', Pools);
Vue.component('videos', Videos);
Vue.component('gallery', Gallery);

Vue.component('package-builder', PackageBuilder);

Vue.component('package-builder-order-summary', PackageBuilderOrderSummary);
Vue.component('package-builder-search-panel', PackageBuilderSearchPanel);
//
Vue.component('category', Category);
Vue.component('flight', Flight);
Vue.component('flight-details', FlightDetails);
Vue.component('hotel', Hotel);

Vue.component('package-detail', PackageDetail);
Vue.component('package-detail-ticket', PackageDetailTicket);
Vue.component('package-detail-flight', PackageDetailFlight);
Vue.component('package-detail-hotel', PackageDetailHotel);

Vue.component('package', Package);
Vue.component('packages', Packages);

Vue.component('sponsor', Sponsor);
Vue.component('custom-modal', CustomModal);
Vue.component('custom-sticky', CustomSticky);

Vue.filter('currency', function (value) {
    let val=value?value:0;
    return val.toLocaleString("en-EN",{minimumFractionDigits: 2, maximumFractionDigits: 2});
})
Vue.filter('currencyShort', function (value) {
    let val=value?value:0;
    return val.toLocaleString("en-EN",{minimumFractionDigits: 0, maximumFractionDigits: 0});
})
import { createStore } from '../../resources/assets/js/store';
const store = createStore()

const app = new Vue({
    el: '#app',
    store,
    data() {
        return {
            customSticky: false,
        }
    },
    methods: {

    },
    mounted() {
        $('#customPackagePopup').modal();
        $('#customPackagePopup').on('hidden.bs.modal', () => {
            this.customSticky = true;
        })
    }

});

export const EventBus = new Vue();
/*--------------------*/




// const app = new Vue({
//     el: '.pageContent',
// });

// Vue.filter('two_digits', function (value) {
//     if (value.toString().length <= 1) {
//         return "0" + value.toString();
//     }
//     return value.toString();
// });
//


