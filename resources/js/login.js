/* Login */
import {notifyModal} from './components/admin/shared/modals';

$(document).ready(function() {

    const $loginForm = $('#loginForm');
    const $loginSubmitBtn = $loginForm.find('[type=submit]');
    const $loginModal = $('#modalLogin');
    const $resetPasswordModal = $('#modalResetPassword');
    const $resetPasswordForm = $('#resetPasswordForm');
    const $resetPasswordSubmitBtn = $resetPasswordForm.find('[type=submit]');

    $loginModal.on('shown.bs.modal', function(){
        $loginForm.find('input[autofocus]').focus();
    });

    $('.form-group-animated .form-control').focus(function() {
        $(this).parents('.form-group-animated').addClass('focused');
    }).blur(function() {
            let inputValue = $(this).val();
            if (!inputValue) {
                $(this).removeClass('filled');
                $(this).parents('.form-group-animated').removeClass('focused');
            } else {
                $(this).addClass('filled');
            }
        }
    );

    $loginForm.find('#email, #password').on('input', function() {
        if ($('#email').val().trim() !== '' &&
            $('#password').val().trim() !== '') {
            $loginSubmitBtn.removeClass('disabled');
        } else {
            $loginSubmitBtn.addClass('disabled');
        }
    });

    $resetPasswordForm.find('#resetPasswordEmail').on('input', function() {
        if ($('#resetPasswordEmail').val().trim() !== '') {
            $resetPasswordSubmitBtn.removeClass('disabled')
        } else {
            $resetPasswordSubmitBtn.addClass('disabled')
        }
    });

    $loginForm.on('submit', function(e) {
        e.preventDefault();

        $.ajax({
            method: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
        })
        .done(function(data) {
            $loginModal.modal('hide');
            setTimeout(function() {
                window.location.href = '/admin';
            }, 2000)
        })
        .fail(function(data) {
            $.each(data.responseJSON.payload, function (key, value) {
                const $input = $('input[name=' + key + ']');
                $input.closest('.error-block').text(value[0]);
                $input.closest('.form-group').addClass('has-error');
            });
        });
    });

    $('#forgotPassword').on('click', function(e) {
        e.preventDefault();
        $loginModal.modal('hide');
        $resetPasswordModal.modal('show');
    });

    $resetPasswordForm.on('submit', function(e) {
        e.preventDefault();

        $.ajax({
            method: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
        })
        .done(function(data) {
            $resetPasswordModal.find('.form-group').removeClass('has-error');
            $resetPasswordModal.modal('hide');
            notifyModal({
                title: 'Password retrieval instructions have been sent',
                subTitle: 'Note that the link to reset your password will expire in 24 hours. \n' +
                    'Don’t forget to check your spam folder if you can’t find the email in your inbox.'
            });
        })
        .fail(function(data) {
            $.each(data.responseJSON.payload, function (key, value) {
                const $input = $resetPasswordModal.find('input[name=' + key + ']');
                $input.closest('.form-group').find('.error-block').text(value[0]);
                $input.closest('.form-group').addClass('has-error');
            });
        });
    });

});
