const $confirmModal = $('#confirmModal');
const $confirmBtn = $('#confirmBtn');
const $notifyModal = $('#notifyModal');
const title =  '.content-title';
const subtitle = '.content-sub-title';

/**
 * Confirmation modal
 * just show message and call done() when confirmed
 *
 * @param options
 */
export function confirmModal(options) {

    const $title = $(title, $confirmModal);
    const $subtitle = $(subtitle, $confirmModal);

    const defaults = {
        action: '',
        done: function () {},
        title: 'Are you sure you want to confirm this action?',
        subTitle: '',
        body: ''
    };

    options = Object.assign({}, defaults, options);

    $title.text(options.title);
    $subtitle.text(options.subTitle);
    $subtitle.after(options.body);

    $confirmModal.modal('show');

    $confirmBtn.one('click', function () {
        $confirmModal.modal('hide');
        if (typeof options.done === 'function') {
            options.done();
        }
    });
}


export function notifyModal(options) {

    const $title = $(title, $notifyModal);
    const $subtitle = $(subtitle, $notifyModal);

    const defaults = {
        title: 'Notify',
        subTitle: 'Something happened',
        body: ''
    };

    options = Object.assign({}, defaults, options);

    $title.text(options.title);
    $subtitle.text(options.subTitle);
    $subtitle.after(options.body);

    $notifyModal.modal('show');

}