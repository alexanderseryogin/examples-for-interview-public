import {showNotification} from './kendo';

export function init(form, onSuccess, onError) {
    let $form = form || $('form.autosave');

    $form.on('change', 'input, select, textarea', function () {
        setTimeout(function () {
            save($form, onSuccess, onError);
        }, 500);
    });
}

function save(form, onSuccess, onError) {
    if (form.hasClass('autosaving')) {
        return false;
    }

    form.addClass('autosaving');

    const method = form.attr('method').toLowerCase();
    const action = form.attr('action');
    let data = form.serialize();

    $.ajax({
        url: action,
        method: method,
        data: data,
    })
        .done(response => {
            let time = getDateTime();
            showNotification('Autosave success at ' + time, 'success');
            $('.autosave-time').text(time);
            if (onSuccess) {
                onSuccess(response)
            }
        })
        .fail(error => {
            if (onError) {
                onError(error)
            }
        })
        .always(() => {
            form.removeClass('autosaving');
        });
}

function getDateTime() {
    const date = new Date();

    return date.getHours() + ':' +
        String(date.getMinutes()).padStart(2, '0') + ', ' +
        date.getMonth() + '/' + date.getDate() + '/' + date.getFullYear();
}