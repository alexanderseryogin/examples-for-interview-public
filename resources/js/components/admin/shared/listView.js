/**
 * Save grid view state by name
 *
 * @param $grid
 * @param $selectView
 * @param type
 */
export function saveListView($grid, $selectView, type) {
    const name = $('#viewName').val();

    if (name) {
        let dataSource = $selectView.getKendoComboBox().dataSource;
        let isDefault = +$('#defaultViewName').is(':checked');
        let options = kendo.stringify($grid.data('kendoGrid').getOptions());

        $.ajax({
            url: '/admin/list-views',
            method: 'post',
            data: {
                'name': name,
                'options': options,
                'is_default': isDefault,
                'type': type
            }
        })
        .always(function (response) {
            if (response.code === 200) {
                dataSource.add(response.payload);

                $('#viewName').val('');
                $('#modalAddView').modal('hide');
            } else {
                alert('An error occurred');
            }
        });
    }
}

/**
 * 'Create new view' dialog with entered name
 *
 * @param value View name
 */
export function addNewView(value) {
    $('#viewName').val(value);
    $('#modalAddView').modal('show');
}