(function ($) {
    $.fn.dropCroppie = function (options) {
        if (this.length > 1) {
            this.each(function () {
                $(this).dropCroppie(options)
            });
            return this;
        }

        function buildSettings(options) {
            let settings = {};

            let cropBoundaryPadding = 20;

            settings.general = $.extend({
                result: '.drop-croppie-result',
                controls: '.drop-croppie-controls',
                modal: '#drop-croppie-modal',
                block: '#drop-croppie-block',
                submitButton: '#drop-croppie-submit-btn',
                rotationButton: '#dropCroppieRotateBtn',
                controlsTemplate: '<span class="drop-croppie-controls drop-zone-controls">' +
                    '<span class="drop-croppie-edit"><i class="k-icon k-i-crop"></i></span>' +
                    '<span class="drop-croppie-delete"><i class="k-icon k-i-delete"></i></span>' +
                    '</span>',
                controlsEditButton: '.drop-croppie-edit',
                controlsDeleteButton: '.drop-croppie-delete',
                placeholder: '.drop-croppie-placeholder',
                placeholderTemplate: '<span class="drop-croppie-placeholder">\n' +
                    '<i class="fa fa-cloud-download"></i>Drag & Drop your files or\n' +
                    '<button type="button" class="drop-zone-button">Browse</button>\n' +
                    '</span>',
            }, options.general || {});

            settings.dropZone = $.extend({
                maxFiles: 1,
                clickable: null,
                previewTemplate: '<i></i>',
                url: 'dummy',
                autoDiscover: false,
                autoProcessQueue: false,
                createImageThumbnails: false,
                addRemoveLinks: false,
                thumbnailMethod: 'contain',
                thumbnailWidth: null,
                thumbnailHeight: null,
                acceptedFiles: 'image/*',
            }, options.dropZone || {});

            settings.crop = {
                viewport: {},
                boundary: {},
                showZoomer: true,
                enableOrientation: true,
                result: {
                    size: 'viewport',
                    format: 'jpeg',
                    backgroundColor: '#ffffff',
                },
                zoom: 0
            };
            $.extend(true, settings.crop, options.crop || {});

            if (!settings.crop.viewport.width || !settings.crop.viewport.height) {
                throw new Error('Specify crop viewport size');
            }

            if ($.isEmptyObject(settings.crop.boundary)) {
                settings.crop.boundary.width = settings.crop.viewport.width + cropBoundaryPadding;
                settings.crop.boundary.height = settings.crop.viewport.height + cropBoundaryPadding;
            }

            return settings;
        }

        function getSelectorName(selector) {
            return selector.substring(1, selector.length);
        }

        this.initialize = function () {
            const $self = this;
            let element = $(this)[0];

            if (!$(element).length) {
                return false;
            }

            const settings = buildSettings(options);
            let $cropperModal = $(settings.general.modal);
            let $cropBlock = $(settings.general.block);
            let $cropSubmitButton = $(settings.general.submitButton);
            let $resultElement = $(settings.general.result, element);
            let $rotationButton;

            function addPlaceholderTemplate() {
                $(element).append(settings.general.placeholderTemplate);
            }

            addPlaceholderTemplate();

            function addRotationButton() {
                if (!$(settings.general.rotationButton).length) {
                    $cropBlock.append(
                        '<button class="btn btn-primary-blur btn-lg-small"' +
                                'id="' + getSelectorName(settings.general.rotationButton) + '"' +
                                ' data-deg="90">Rotate</button>'
                    );
                }

                return $(settings.general.rotationButton);
            }

            function getClickable() {
                if (settings.dropZone.clickable) {
                    return settings.dropZone.clickable;
                }

                let clickable = [];

                clickable.push(element);

                $(settings.general.placeholder, element).each(function (key, value) {
                    clickable.push(value);
                });

                return clickable;
            }

            function initDropZone() {
                let options = Object.assign({}, settings.dropZone);

                options.clickable = getClickable();

                $(element).addClass('drop-croppie-exists');

                return element.dropzone || new Dropzone(element, options);
            }

            let dropZone = initDropZone();

            function initCroppie() {
                $cropBlock.croppie('destroy');

                let croppie = $cropBlock.croppie(settings.crop);

                if (settings.crop.enableOrientation) {
                    $rotationButton = addRotationButton();
                    $rotationButton.on('click', function () {
                        croppie.croppie('rotate', parseInt($(this).data('deg')));
                    });
                }

                return croppie;
            }

            this.buildPreview = function(url, path) {
                if (url === undefined) {
                    url = $(element).attr('data-url') || '';
                } else {
                    $(element).attr('data-url', url);
                    $resultElement.val(path).trigger('change');
                }

                if (url) {
                    $(element).css('background-image', 'url(' + url + ')');
                    $(settings.general.placeholder, element).hide();
                    $(settings.general.controls, element).show();

                    if (!$(settings.general.controls, element).length) {
                        $(element).append(settings.general.controlsTemplate);

                        $(settings.general.controlsEditButton, element).on('click', showCroppieModal);
                        $(settings.general.controlsDeleteButton, element).on('click', deleteCroppedImage);
                    }

                    dropZone.disable();
                } else {
                    $(element).css('background-image', 'none');
                    $(settings.general.controls, element).hide();
                    $(settings.general.placeholder, element).show();

                    dropZone.enable();
                }
            };

            $self.buildPreview();

            function showCroppieModal() {
                let croppie = initCroppie();

                $cropperModal.on('shown.bs.modal', function () {
                    croppie.croppie('bind', {
                        url: $(element).attr('data-url'),
                        zoom: settings.crop.zoom
                    });
                });

                $cropperModal.on('hidden.bs.modal', function () {
                    $cropperModal.off('shown.bs.modal');
                    $cropperModal.off('hidden.bs.modal');
                    $cropSubmitButton.off('click');

                    if ($('.modal:visible').length) {
                        $('body').addClass('modal-open');
                    }

                    if ($rotationButton) {
                        $rotationButton.remove();
                    }
                });

                $cropSubmitButton.on('click', function () {
                    croppie.croppie('result', {
                        type: 'blob',
                        format: settings.crop.result.format,
                        backgroundColor: settings.crop.result.backgroundColor,
                        size: settings.crop.result.size
                    }).then(function (image) {
                        let fd = new FormData();

                        fd.append('image', image);

                        $.ajax({
                            url: "/admin/images",
                            type: "POST",
                            processData: false,
                            contentType: false,
                            data: fd,
                        }).done(function (response) {
                            $self.buildPreview(response.payload.url, response.payload.path);
                            $cropperModal.modal('hide');
                            $cropSubmitButton.unbind('click');
                        });
                    });
                });

                $cropperModal.modal('show');
            }

            function deleteCroppedImage() {
                $self.buildPreview('', '');
            }

            dropZone.on('addedfile', function (file) {
                if (!Dropzone.isValidFile(file, settings.dropZone.acceptedFiles)) {
                    return;
                }

                // initialize FileReader which reads uploaded file
                let reader = new FileReader();

                reader.onloadend = function () {
                    $(element).attr('data-url', reader.result);
                };

                // read uploaded file (triggers code above)
                reader.readAsDataURL(file);

                dropZone.removeFile(file);

                showCroppieModal();
            }).on('removedfile', function () {
                $(settings.general.placeholder, element).show();
            });

            return this;
        };

        return this.initialize();
    };
}(jQuery));