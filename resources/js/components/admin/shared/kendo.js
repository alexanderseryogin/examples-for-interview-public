export function kendoNiceScroll($el, options = {}) {
    const defaultOptions = {
        cursorcolor: '#d2d2d2',
        cursorwidth: '9px',
        cursorborderradius: '8px',
        autohidemode: false,
        background: '#FCFCFC',
        cursorborder: 'none',
        horizrailenabled: true
    };

    Object.assign(options, defaultOptions);

    $el.niceScroll(options);
}

export function kendoMenu($el, options = {}) {
    $el.kendoMenu(options);
}

/**
 * 'Create new view' dialog with entered name
 *
 * @param value View name
 */
export function addNewView(value) {
    $('#viewName').val(value);
    $('#modalAddView').modal('show');
}

/**
 *
 * @param $selectView
 * @param listDataSource
 * @param $grid
 * @param gridOptions
 */
export function saveViewStateInit($selectView, listDataSource, $grid, gridOptions)
{
    $selectView.kendoComboBox({
        filter: 'contains',
        dataTextField: 'name',
        dataValueField: 'id',
        dataSource: listDataSource,
        noDataTemplate: $('#noDataTemplate').html(),
        autoBind: true,
        select: function(e) {
            if (e.dataItem) {
                restoreSavedOptions($grid, e.dataItem.options);
            }
        },
        change: function() {
            if (this.value() === '') {
                $grid.data('kendoGrid').setOptions(gridOptions);
                $grid.data("kendoGrid").autoFitColumn();
            }
        },
    });
}

/**
 *
 * @param message
 * @param type
 */
export function showNotification(message, type) {
    let notification = $('#notification').kendoNotification({
        position: {
            pinned: true,
            top: 20,
            right: 90
        },
        autoHideAfter: 2000,
        templates: [
            {
                type: 'success',
                template: '<div class="success"><h4>#= message #</h4></div>'
            },
            {
                type: 'error',
                template: '<div class="error"><h4>#= message #</h4></div>'
            }
        ]

    }).data('kendoNotification');

    notification.show({message: message}, type);
}

/**
 *
 * @param $grid
 * @returns {any}
 */
export function getSelectedIds($grid) {
    let grid = $grid.data('kendoGrid');
    return grid.selectedKeyNames();
}

/**
 *
 * @param $grid
 * @param savedOptions string
 */
export function restoreSavedOptions($grid, savedOptions) {
    const options = $grid.data('kendoGrid').getOptions();
    $.extend(true, options, JSON.parse(savedOptions));
    $grid.data('kendoGrid').setOptions(options);
}

export function tableRefresh($grid) {
    $('.sidebar-toggle').on('click', function () {
        if($(window).outerWidth() >= 768 ){
            let kendoGrid = $grid.data('kendoGrid');

            if(kendoGrid) {
                setTimeout(function () {
                    kendoGrid.setOptions(kendoGrid.getOptions());
                    $('.k-grid-content.k-auto-scrollable').getNiceScroll().resize();
                }, 300)
            }
        }
    })
}