export function getFields() {
    return {
        id: {type: 'number', editable: false},
        title: {type: 'string', validation: {required: true}},
        start_date: {type: 'date'},
        start_time: {type: 'date'},
        venue_name: {type: 'string'},
        venue_city: {type: 'string'},
        city: {type: 'string'},
        status: {type: 'string'},
        promoter_name: {type: 'string'},
    }
}