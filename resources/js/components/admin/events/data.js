import {
    $grid,
    $selectView,
    crudServiceBaseUrl,
    listViewsUrl,
    pageSize,
} from './constants';
import {restoreSavedOptions} from '../shared/kendo';

export function getGridDataSource(options = {}) {
    const defaultOptions = {
        transport: {
            read: {
                url: crudServiceBaseUrl,
                data: function () {
                    return {
                        q: $("#events-search").val()
                    };
                }
            },
            parameterMap: function (data, type) {
                if (data.filter) {
                    data.filter.filters.map(function (item) {
                        if (item.field === 'start_date') {
                            item.value = kendo.toString(item.value, 'yyyy-MM-dd');
                        } else if (item.field === 'start_time') {
                            item.value = kendo.toString(item.value, 'HH:mm');
                        }
                    });
                }
                return $.extend({'page_size': data.pageSize}, data);
            }
        },
        schema: {
            model: {
                id: 'id',
                fields: {
                    id: {type: 'number', editable: false},
                    title: {type: 'string', validation: {required: true}},
                    start_date: {type: 'date'},
                    start_time: {type: 'date'},
                    venue_name: {type: 'string'},
                    venue_city: {type: 'string'},
                    city: {type: 'string'},
                    status: {type: 'string'},
                    promoter_name: {type: 'string'},
                }
            },
            groups: function (response) {
                return response.payload.items;
            },
            group: {
                field: 'start_date'
            },
            data: function (response) {
                return response.payload.items;
            },
            total: 'payload.total'
        },
        serverSorting: true,
        serverPaging: true,
        serverFiltering: true,
        serverGrouping: true,
        pageSize: pageSize,
        batch: true
    };

    Object.assign(options, defaultOptions);

    return new kendo.data.DataSource(options);
}

export function getListDataSource(options = {}) {
    const defaultOptions = {
        transport: {
            read: {
                url: listViewsUrl,
                data: {
                    type: 'events'
                },
                dataType: "json"
            }
        },
        schema: {
            data: function (response) {
                return response.payload ? response.payload : [];
            }
        },
        requestEnd: function (e) {
            if (e.response && e.response.code === 200) {
                e.response.payload.forEach(function (item) {
                    if (item.is_default === 1) {
                        $selectView.data('kendoComboBox').value(item.id);
                        restoreSavedOptions($grid, item.options);
                    }
                });
            }
        }
    };

    Object.assign(options, defaultOptions);

    return new kendo.data.DataSource(options);
}