import {
    getSelectedIds,
    kendoNiceScroll,
    saveViewStateInit,
    tableRefresh,
} from '../shared/kendo';
import {$grid, $selectView, crudServiceBaseUrl} from './constants';
import {getGridDataSource, getListDataSource} from './data';
import {
    batchUpdateEventStatusModal,
    getGridColumns,
    gridInit,
    updateEventStatusModal,
} from './kendo';
import {addNewView, saveListView} from '../shared/listView';

/* Function to be accessible in the global scope.
Can called by 'onclick' html attribute */

window.addNewView = addNewView;
window.updateEventStatusModal = updateEventStatusModal;
window.batchUpdateEventStatusModal = batchUpdateEventStatusModal;

$(document).ready(function () {
    function onOpen(e) {
        let wrapHeight = $(e.item).closest('.k-grid-content')[0].offsetHeight;
        if ($(e.item).position().top >= wrapHeight / 2) {
            $(e.item).find('.k-animation-container').addClass('top').removeClass('bottom')
        } else {
            $(e.item).find('.k-animation-container').addClass('bottom').removeClass('top')
        }
    }

    if ($grid.length) {

        let dataSource = getGridDataSource();
        const gridOptions = {
            columns: getGridColumns(),
            dataSource: dataSource,
            dataBound: function () {
                $(".button-menu").kendoMenu({
                    open: onOpen
                });
                kendoNiceScroll($('.k-grid-content.k-auto-scrollable'));
            }
        };

        gridInit($grid, gridOptions);

        saveViewStateInit($selectView, getListDataSource(), $grid, gridOptions);

        $('#saveListViewBtn').on('click', function (e) {
            e.preventDefault();
            saveListView($grid, $selectView, 'events')
        });
    }

    $('#events-search-btn').click(function () {
        $(this).closest('form').submit();
    });

    $('#updateEventStatus').click(function () {
        let action = $('#updateEventStatus').data('action');
        let id = $('#updateEventStatus').data('id');

        let actionStatusMap = {
            'delete': 'Deleted',
            'private': 'Private',
            'suspend': 'Suspended',
            'cancel': 'Cancelled',
            'publish': 'Published',
            'draft': 'Draft'
        };

        let ids = id ? [id] : getSelectedIds($grid);
        let status = actionStatusMap[action];

        if (ids.length && status) {
            $.ajax({
                url: crudServiceBaseUrl + '/statuses',
                method: 'post',
                data: {
                    'ids': ids,
                    'status': status,
                }
            })
                .always(function (response) {
                    if (response.code === 200) {
                        $grid.data('kendoGrid').setOptions($grid.data('kendoGrid').getOptions());
                        $('#modalUpdateEvents').modal('hide');
                    } else {
                        alert('An error occurred');
                    }
                });
        }
    });

    tableRefresh($grid);
});