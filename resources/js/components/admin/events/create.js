import {addNewEventOrganizer, kendoComboBox} from './kendo';
import * as CompanyCreate from '.././company/create';
import * as Autosave from '../shared/autosave';
import {getCompaniesDataSource} from "../companies/data";

window.addNewEventOrganizer = addNewEventOrganizer;

$(document).ready(function () {
    const $eventGeneralInfoForm = $('#event-general-info-form');

    if ($eventGeneralInfoForm.length) {
        const $companyField = $('#company_id');

        if ($companyField.length) {
            kendoComboBox($companyField, {
                placeholder: 'Type and search company name',
                dataSource: getCompaniesDataSource(),
            });
        }

        const $organizerField = $('#organizer_id');
        const companyType = 'Event organizer';

        Autosave.init();
        CompanyCreate.init($organizerField);

        /* Sidebar Toggle */
        $('.js-right-sidebar-toggle').on('click', function () {
            $('.right-sidebar').toggleClass('closed');
        });

        /* Tabs */

        $('#tabstrip').kendoTabStrip();

        /* Banners Uploads */

        $('#genres').kendoMultiSelect({
            placeholder: 'Type and select event genre',
            filter: 'contains',
            dataTextField: 'name',
            dataValueField: 'id',
            autoBind: true,
            minLength: 1,
            dataSource: {
                transport: {
                    read: {
                        url: '/admin/genres',
                    }
                },
                schema: {
                    data: function (response) {
                        return response.payload ? response.payload : [];
                    }
                }
            },
        });

        /* Event dates */

        $('#start_date, #end_date, #custom_date').kendoDatePicker({
            format: 'd MMMM yyyy',
            parseFormats: ['yyyy-MM-dd'],
        });

        let datepicker = $('#start_date').data('kendoDatePicker');

        datepicker.bind('change', function () {
            let startDate = this.value();
            $('#end_date').data('kendoDatePicker').value(startDate);
        });

        const showHideCustomDate = (triggerSelector, targetSelector) => {
            if ($(triggerSelector).prop('checked')) {
                $(targetSelector).addClass('open');
            } else {
                $(targetSelector).removeClass('open');
            }
        };

        showHideCustomDate('#add_custom_datetime', '#add_custom_datetime-wrap');

        $('#add_custom_datetime').on('change', function () {
            showHideCustomDate('#add_custom_datetime', '#add_custom_datetime-wrap');
        });

        /* Convert to slug Event name */

        const $nameInput = $('[name=name]');
        $nameInput.on('keyup', function () {
            $('[name=url]').val(
                convertToSlug($nameInput.val())
            );
        });

        /* Venue autocomplete */
        $('#venue_id').kendoComboBox({
            filter: 'contains',
            placeholder: 'Type event venue',
            minLength: 2,
            suggest: false,
            dataTextField: 'name',
            dataValueField: 'id',
            template: '#: data.name #',
            dataSource: {
                transport: {
                    read: '/admin/venues/all'
                },
                schema: {
                    model: {
                        id: 'id',
                        fields: {
                            id: {type: 'number', editable: false},
                            name: {type: 'string'},
                            address1: {type: 'string'},
                            address2: {type: 'string'},
                            city: {type: 'string'},
                            state: {type: 'string'},
                            postcode: {type: 'string'},
                        }
                    },
                    data: function (response) {
                        return response.payload || [];
                    }
                },
            },

            select: function (e) {
                const item = e.dataItem;
                const id = item.id;
                $('[name=venue_id]').val(id);
            }
        });

        /* Organizer select with autocomplete*/
        $organizerField.kendoComboBox({
            filter: 'contains',
            placeholder: 'Type and select your event organizer',
            minLength: 1,
            suggest: false,
            dataTextField: 'name',
            dataValueField: 'id',
            dataSource: {
                batch: false,
                transport: {
                    read: {
                        url: '/admin/companies/?type=' + companyType,
                    },
                    create: {
                        url: '/admin/companies',
                        type: 'POST',
                        data: {
                            type: companyType
                        }
                    },
                    parameterMap: function (data, operation) {
                        if (operation === 'create' && data) {
                            return data;
                        }
                    }
                },
                schema: {
                    model: {
                        id: 'id',
                        fields: {
                            id: {type: 'number', editable: false},
                            name: {type: 'string', required: true},
                            logo: {type: 'string'},
                        }
                    },
                    data: function (response) {
                        return response.payload ? response.payload : [];
                    }
                }
            },
            noDataTemplate: $("#no-data-template").html()
        });


        $('.drop-zone-desktop').dropCroppie({
            general: {
                result: '#banner_desktop'
            },
            crop: {
                viewport: {
                    width: 700,
                    height: 250,
                },
                result: {
                    size: {
                        width: 1440,
                        height: 537
                    }
                }
            }
        });

        $('.drop-zone-mobile').dropCroppie({
            general: {
                result: '#banner_mobile'
            },
            crop: {
                viewport: {
                    width: 414,
                    height: 670,
                }
            }
        });

        $('#js-copy-event-url').click(function () {
            let $temp = $("<input>");
            $("body").append($temp);
            $temp.val($('#js-event-url-start').text() + $('#js-event-url-end').val()).select();
            document.execCommand("copy");
            $temp.remove();
        });
    }
});