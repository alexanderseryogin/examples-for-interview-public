$(document).ready(function () {
    //Accordion
    let accordionPanelsClass = '.js-accordion-panel-content',
        $accordionBtn = $('.js-accordion-btn');

    $accordionBtn.on('click', function () {
        $accordionBtn.text('Show more');

        $('.js-accordion-panel').removeClass('open');

        if ($(this).parent().find(accordionPanelsClass).hasClass('active')) {
            $(this).parent().find(accordionPanelsClass).slideUp().removeClass('active');
        } else {
            $(accordionPanelsClass).slideUp().removeClass('active');
            $(this).text('Show less');
            $(this).siblings('.js-accordion-panel').addClass('open');
            $(this).parent().find(accordionPanelsClass).slideDown().addClass('active');
        }
    });

    $("#event-info .js-sortable-list").kendoSortable({
        handler: ".js-handler",
        filter: ".js-sortable",
        ignore: ".js-sortable-panel *",
        axis: "y",
        placeholder: function (element) {
            return element.clone().addClass("placeholder");
        },
        hint: function (element) {
            return element.clone().addClass("hint")
                .width(element.width());
        },
        change: function () {
            $('.info-block-pos').each(function (index, item) {
                $(item).val(index).trigger('change');
            })
        }
    });

    $('#event-info .js-editor').kendoEditor({
        resizable: {
            content: true,
            toolbar: true
        }
    });

    $('#event-info-add-quote').click(function () {
        const $block = $('#event-info-quotes');
        let number = +$block.find('.form-group:last-child').attr('data-number') + 1;

        $block.append(`<div class="form-group" data-number="${number}">
            <label for="quotes[${number}]" class="control-label">Quote ${number + 1}</label>
            <textarea id="quotes[${number}]" name="quotes[${number}][content]" class="js-editor"
                      placeholder="Please type your quote. Please state the spokesperson name"></textarea>
            <button class="btn-link text-primary event-info-remove-quote">- Remove quote</button>
        </div>`);

        $block.find('.form-group:last-child .js-editor').kendoEditor({
            resizable: {
                content: true,
                toolbar: true
            }
        });
    });

    $('#event-info-quotes').on('click', '.event-info-remove-quote', function () {
        $(this).closest('.form-group').remove();
    });

    function initGalleryDropCroppie($el) {
        if ($el.length) {
            $el.dropCroppie({
                general: {
                    result: '.gallery-image',
                    placeholder: '.gallery-image-placeholder',
                    placeholderTemplate: '<span class="gallery-image-placeholder"><i class="fa fa-cloud-download"></i>Upload attachment</span>',
                },
                crop: {
                    viewport: {
                        width: 500,
                        height: 500,
                    }
                }
            });
        }
    }

    initGalleryDropCroppie($('.drop-zone-gallery'));

    $('#info-add-gallery-image').click(function () {
        const $galleryBlock = $('#event-info-gallery');

        $galleryBlock.append('<div class="drop-zone drop-zone-gallery horizontal">\n' +
            '<input class="gallery-image" name="gallery[]" type="hidden" value="">\n' +
            '</div>');

        initGalleryDropCroppie($('.drop-zone-gallery:last-child', $galleryBlock));
    });

    function initArtistLineUpDropCroppie($el) {
        if ($el.length) {
            $el.dropCroppie({
                general: {
                    result: '.artist-line-up-image',
                    placeholder: '.artist-line-up-placeholder',
                    placeholderTemplate: '<span class="artist-line-up-placeholder"><i class="fa fa-cloud-download"></i>Upload attachment</span>',
                },
                crop: {
                    viewport: {
                        width: 500,
                        height: 500,
                    }
                }
            });
        }
    }

    initArtistLineUpDropCroppie($('.drop-zone-artist'));

    $('#info-add-artist-line-up-image').click(function () {
        const $artistLineUpBlock = $('#event-info-artist-line-up');

        $artistLineUpBlock.append('<div class="drop-zone drop-zone-artist horizontal">\n' +
            '<input class="artist-line-up-image" name="artist_line_up[]" type="hidden" value="">\n' +
            '</div>');

        initArtistLineUpDropCroppie($('.drop-zone-artist:last-child', $artistLineUpBlock));
    });
});
