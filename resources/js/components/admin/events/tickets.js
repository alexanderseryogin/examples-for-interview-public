import {showNotification} from '../shared/kendo';

$(document).ready(function () {
    if ($('.drop-croppie-header').length) {
        var headerCropCroppie = $('.drop-croppie-header').dropCroppie({
            general: {
                result: '#ticket_header_image'
            },
            crop: {
                viewport: {
                    width: 640,
                    height: 360,
                },
                result: {
                    size: {
                        width: 1920,
                        height: 1080
                    }
                }
            }
        });
    }

    $('#ticket_sales_end_date').kendoDatePicker({
        format: 'd MMMM yyyy',
        parseFormats: ['yyyy-MM-dd'],
        open: function (e) {
            $('.k-calendar .k-today').removeClass('k-today');
        }
    });

    var $ticketsGridElement = $('#event-tickets-grid');
    var validator;

    const ticketsGridBaseUrl = $ticketsGridElement.data('base-url');
    var $editTicketModal = $('#edit-event-ticket-modal');
    var editTicketForm = $('#edit-event-ticket-form');
    var eventStartDate = $ticketsGridElement.attr('data-event-start-date') ? new Date($ticketsGridElement.attr('data-event-start-date')) : null;

    var ticketViewModel = kendo.observable({
        dataSource: new kendo.data.DataSource({
            schema: {
                model: {
                    id: 'id',
                    fields: {
                        id: {editable: false, nullable: true},
                        pos: {editable: false, type: 'number', nullable: false, defaultValue: 0},
                        category: {type: 'string', validation: {required: true}},
                        tier: {type: 'string', validation: {required: true}},
                        name: {editable: true, validation: {required: true}},
                        currency_id: {field: "currency_id", type: "number", defaultValue: 1, validation: {required: true}},
                        price: {type: 'number', validation: {required: true}},
                        has_discount: {type: 'number', defaultValue: 0},
                        discount_price: {type: 'number', defaultValue: 0},
                        status: {type: 'string', field: "status", defaultValue: 'On Sale', validation: {required: true}},
                        sales_end_date: {
                            type: 'date',
                            defaultValue: (eventStartDate ? kendo.toString(eventStartDate, 'd MMMM yyyy') : null),
                            validation: {required: true}
                        },
                        sales_end_date_hour: {
                            from: "salesEndDateHour",
                            type: 'number',
                            defaultValue: (eventStartDate ? eventStartDate.getHours() : null)
                        },
                        sales_end_date_minute: {
                            from: "salesEndDateMinute",
                            type: 'number',
                            defaultValue: (eventStartDate ? eventStartDate.getMinutes() : null)
                        },
                        show_countdown: {type: 'number', defaultValue: 0},
                        header_image: {type: 'string', defaultValue: null},
                        header_image_url: {type: 'string', from: "headerImageUrl"},
                        description: {type: 'string', defaultValue: null},
                        details: {type: 'string', defaultValue: null},
                    }
                },
                data: function (response) {
                    return response.payload ? response.payload : [];
                }
            },
            transport: {
                read: {
                    url: ticketsGridBaseUrl,
                },
                create: {
                    url: ticketsGridBaseUrl,
                    type: 'POST'
                },
                update: {
                    url: ticketsGridBaseUrl,
                    type: 'POST'
                },
                destroy: {
                    url: function (options) {
                        return '/admin/tickets/' + options.id
                    },
                    type: 'DELETE'
                },
                parameterMap: function (data) {
                    data.sales_end_date = kendo.toString(data.sales_end_date, 'd MMMM yyyy');
                    data.sales_end_date_hour = data.salesEndDateHour;
                    data.sales_end_date_minute = data.salesEndDateMinute;
                    data.has_discount = data.discount_price > 0 ? 1 : 0;
                    data.show_countdown = +$('#ticket_show_countdown').is(':checked');
                    if (data) {
                        return data;
                    }
                }
            },
            change: function () {
                ticketViewModel.set("hasChanges", this.hasChanges());
            },
            error: function () {
                showNotification('An error occurred', 'error');
            }
        }),
        selected: {},
        hasChanges: false,
        sync: function () {
            if (validator.validate()) {
                this.dataSource.sync();

                $editTicketModal.modal('hide');
            }
        },
        cancel: function () {
            $ticketsGridElement.data("kendoGrid").cancelRow();
            validator.hideMessages();
            $editTicketModal.modal('hide');
        },
        changeHasDiscount: function () {
            let checked = $('#ticket_has_discount').is(':checked');

            if (!checked) {
                this.set('selected.discount_price', 0);
            }

            $('#ticket_discount_price').prop('disabled', !checked);
        }
    });

    kendo.bind(editTicketForm, ticketViewModel);
    validator = editTicketForm.kendoValidator().data("kendoValidator");

    if ($ticketsGridElement.length) {
        $ticketsGridElement.kendoGrid({
            noRecords: true,
            sortable: false,
            groupable: false,
            pageable: false,
            scrollable: false,
            navigatable: true,
            editable: 'inline',
            batch: false,
            dataSource: ticketViewModel.dataSource,
            columns: [
                {title: '', width: '30px', template: "<i class='k-icon k-i-more-vertical handler'></i>"},
                {field: 'category', title: 'Ticket Category'},
                {field: 'tier', title: 'Ticket Tier'},
                {field: 'name', title: 'Ticket Name'},
                {
                    field: 'currency_id', title: 'Currency', values: getGridCurrencies()
                },
                {field: 'price', title: 'Ticket Price'},
                {field: 'discount_price', title: 'Discount Price'},
                {
                    field: 'status', title: 'Ticket Status', editor: statusDropDownEditor, template: "#= status #"
                },
                {
                    template: kendo.template($("#ticket-button-menu-template").html()),
                    title: ''
                }
            ],
            dataBound: function () {
                let row = this.tbody.find(">tr[data-uid=" + ticketViewModel.selected.uid + "]");
                if (row) {
                    this.select(row);
                }
                $('.button-menu').kendoMenu();
            },
            change: function () {
                let model = this.dataItem(this.select());
                validator.hideMessages();
                ticketViewModel.set("selected", model);
            },
            edit: function (e) {
                $('.button-menu').kendoMenu();
                $('.save-event-ticket-wrap', e.container).show();
                $('.cancel-event-ticket-wrap', e.container).show();
                $('.edit-event-ticket-wrap', e.container).hide();
                $('.edit-details-event-ticket-wrap', e.container).hide();
                $('.delete-event-ticket-wrap', e.container).hide();
            },
            saveChanges: function (e) {
                $('.save-event-ticket-wrap', e.container).hide();
                $('.cancel-event-ticket-wrap', e.container).hide();
                $('.edit-event-ticket-wrap', e.container).show();
                $('.edit-details-event-ticket-wrap', e.container).show();
                $('.delete-event-ticket-wrap', e.container).show();

                $editTicketModal.modal('hide');
            },
        });

        $ticketsGridElement.data("kendoGrid").table.kendoSortable({
            filter: ">tbody >tr:not('.k-grid-edit-row')",
            hint: $.noop,
            cursor: "move",
            placeholder: function (element) {
                return element.clone().addClass("k-state-hover").css("opacity", 0.65);
            },
            container: "#event-tickets-grid tbody",
            change: function (e) {
                let grid = $ticketsGridElement.data("kendoGrid"),
                    skip = grid.dataSource.skip(),
                    oldIndex = e.oldIndex + skip,
                    newIndex = e.newIndex + skip,
                    data = grid.dataSource.data(),
                    dataItem = grid.dataSource.getByUid(e.item.data("uid"));

                dataItem.pos = e.newIndex;

                grid.dataSource.remove(dataItem);
                grid.dataSource.insert(e.newIndex, dataItem);

                if (dataItem.id) {
                    let reorderData = [];
                    grid.dataSource.data().forEach(function (item, index) {
                        if (item.id) {
                            reorderData.push({'id': item.id, 'pos': index});
                        }
                    });

                    $.ajax({
                        url: '/admin/tickets/reorder',
                        type: 'post',
                        dataType: 'json',
                        data: {'tickets': reorderData}
                    });
                }
            }
        });

    }

    function getGridCurrencies() {
        let gridCurrencies = [];
        for (let i = 0; i < currencies.length; ++i) {
            gridCurrencies.push({value: currencies[i].id, text: currencies[i].code});
        }

        return gridCurrencies;
    }

    function statusDropDownEditor(container, options) {
        $('<input name="' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                autoBind: false,
                dataSource: ticketStatuses
            });
    }

    $(".js-ticket-editor").kendoEditor({
        resizable: {
            content: true,
            toolbar: true
        }
    });

    $('#event-tickets-grid-add').click(function () {
        $ticketsGridElement.data("kendoGrid").addRow();
    });

    $ticketsGridElement
        .on('click', '.save-event-ticket', function (e) {
            $ticketsGridElement.data("kendoGrid").saveChanges();
        })
        .on('click', '.edit-event-ticket', function (e) {
            let row = $(e.target).closest("tr");
            $ticketsGridElement.data("kendoGrid").editRow(row);
        })
        .on('click', '.edit-details-event-ticket', function () {
            let model = $ticketsGridElement.data("kendoGrid").dataSource.getByUid($(this).data('uid'));

            model.sales_end_date_hour = model.sales_end_date ? model.sales_end_date.getHours() : null;
            model.sales_end_date_minute = model.sales_end_date ? model.sales_end_date.getMinutes() : null;

            validator.hideMessages();
            ticketViewModel.set("selected", model);

            $('#ticket_discount_price').prop('disabled', !model.has_discount);
            $('.drop-croppie-header').attr('data-url', model.header_image_url);

            headerCropCroppie.buildPreview();

            $editTicketModal.modal('show');
        })
        .on('click', '.cancel-event-ticket', function (e) {
            $ticketsGridElement.data("kendoGrid").cancelChanges();
        })
        .on('click', '.delete-event-ticket', function () {
            $ticketsGridElement.data("kendoGrid").removeRow($(this).closest('tr'));
        });

    $(".number-increment-input-wrap .button").on("click", function () {
        let $button = $(this),
            value = parseInt($button.parent().find(".form-control").val());

        if ($button.hasClass('inc')) {
            ++value;
        } else {
            if (value >= 1) {
                --value;
            } else {
                value = 0;
            }
        }

        $button.parent().find(".form-control").val(value).trigger('change');
    });
});
