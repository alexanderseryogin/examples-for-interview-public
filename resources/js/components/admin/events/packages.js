$(document).ready(function () {
    if ($('#event-packages-form').length) {
        var $packagesGridElement = $('#event-packages-grid');
        var validator;

        const packagesGridBaseUrl = $packagesGridElement.data('base-url');
        var $editPackageModal = $('#edit-event-package-modal');
        var editPackageForm = $('#edit-event-package-form');
        var packageViewModel = kendo.observable({
            dataSource: new kendo.data.DataSource({
                schema: {
                    model: {
                        id: 'id',
                        fields: {
                            id: {editable: false, nullable: true},
                            pos: {editable: false, type: 'number', nullable: false, defaultValue: 0},
                            is_visible: {type: 'number', defaultValue: 1},
                            use_same_more_info: {type: 'number', defaultValue: 0},
                            name: {editable: true, validation: {required: true}},
                            card_size: {type: 'string', field: "card_size", defaultValue: 'Small 1x1', validation: {required: true}},
                            call_to_action_url: {type: 'string', validation: {required: true}},
                            currency_id: {field: "currency_id", type: "number", defaultValue: 1, validation: {required: true}},
                            price: {type: 'number', validation: {required: true}},
                            status: {type: 'string', field: "status", defaultValue: 'On Sale', validation: {required: true}},
                            card_image: {type: 'string', defaultValue: null},
                            card_image_url: {type: 'string', from: "cardImageUrl"},
                            header_image: {type: 'string', defaultValue: null},
                            header_image_url: {type: 'string', from: "headerImageUrl"},
                            description: {type: 'string', defaultValue: null},
                            details: {type: 'string', defaultValue: null},
                            blocks: {type: 'object', defaultValue: null},
                        }
                    },
                    data: function (response) {
                        if (!response.payload) {
                            return [];
                        }

                        return response.payload;
                    }
                },
                transport: {
                    read: {
                        url: packagesGridBaseUrl,
                    },
                    create: {
                        url: packagesGridBaseUrl,
                        type: 'POST'
                    },
                    update: {
                        url: packagesGridBaseUrl,
                        type: 'POST'
                    },
                    destroy: {
                        url: function (options) {
                            return '/admin/event-packages/' + options.id
                        },
                        type: 'DELETE'
                    },
                    parameterMap: function (data) {
                        if (data) {
                            if (data.blocks) {
                                data.blocks.forEach(function (item) {
                                    item.is_active = +$('#package_' + item.type + '_block_is_active').is(':checked');
                                });
                            }
                            data.use_same_more_info = +$('#package_use_same_more_info').is(':checked');
                            return data;
                        }
                    }
                },
                change: function () {
                    packageViewModel.set("hasChanges", this.hasChanges());
                    packageViewModel.selected.dirty = true;
                }
            }),
            selected: {},
            hasChanges: false,
            sync: function () {
                if (validator.validate()) {
                    this.dataSource.sync();

                    $editPackageModal.modal('hide');
                }
            },
            cancel: function () {
                $packagesGridElement.data("kendoGrid").cancelRow();
                validator.hideMessages();
                $editPackageModal.modal('hide');
            },
            isHiddenChecked: function(e) {
                e.data.selected.is_visible = +!$('#package_is_hidden').is(':checked');
            }
        });

        kendo.bind(editPackageForm, packageViewModel);
        validator = editPackageForm.kendoValidator().data("kendoValidator");

        $packagesGridElement.kendoGrid({
            noRecords: true,
            sortable: false,
            groupable: false,
            pageable: false,
            scrollable: false,
            navigatable: true,
            editable: 'inline',
            batch: false,
            dataSource: packageViewModel.dataSource,
            columns: [
                {title: '', width: '30px'},
                {field: 'name', title: 'Package Name'},
                {
                    field: 'currency_id', title: 'Currency', values: getGridCurrencies()
                },
                {field: 'price', title: 'Package Price'},
                {
                    field: 'card_size', title: 'Card size', editor: cardSizeDropDownEditor, template: "#= card_size #"
                },
                {
                    field: 'status', title: 'Package Status', editor: statusDropDownEditor, template: "#= status #"
                },
                {
                    template: kendo.template($("#package-button-menu-template").html()),
                    title: ''
                }
            ],
            dataBound: function () {
                let row = this.tbody.find(">tr[data-uid=" + packageViewModel.selected.uid + "]");
                if (row) {
                    this.select(row);
                }
                $('.button-menu').kendoMenu();
            },
            change: function () {
                let model = this.dataItem(this.select());
                validator.hideMessages();
                packageViewModel.set("selected", model);
            },
            edit: function (e) {
                $('.button-menu').kendoMenu();
                $('.save-event-package-wrap', e.container).show();
                $('.cancel-event-package-wrap', e.container).show();
                $('.edit-event-package-wrap', e.container).hide();
                $('.edit-details-event-package-wrap', e.container).hide();
                $('.delete-event-package-wrap', e.container).hide();
            },
            saveChanges: function (e) {
                $('.save-event-package-wrap', e.container).hide();
                $('.cancel-event-package-wrap', e.container).hide();
                $('.edit-event-package-wrap', e.container).show();
                $('.edit-details-event-package-wrap', e.container).show();
                $('.delete-event-package-wrap', e.container).show();

                $editPackageModal.modal('hide');
            },
        });

        $packagesGridElement.data("kendoGrid").table.kendoSortable({
            filter: ">tbody >tr:not('.k-grid-edit-row')",
            hint: $.noop,
            cursor: "move",
            placeholder: function (element) {
                return element.clone().addClass("k-state-hover").css("opacity", 0.65);
            },
            container: "#event-packages-grid tbody",
            change: function (e) {
                let grid = $packagesGridElement.data("kendoGrid"),
                    skip = grid.dataSource.skip(),
                    oldIndex = e.oldIndex + skip,
                    newIndex = e.newIndex + skip,
                    data = grid.dataSource.data(),
                    dataItem = grid.dataSource.getByUid(e.item.data("uid"));

                dataItem.pos = e.newIndex;

                grid.dataSource.remove(dataItem);
                grid.dataSource.insert(e.newIndex, dataItem);

                if (dataItem.id) {
                    let reorderData = [];
                    grid.dataSource.data().forEach(function (item, index) {
                        if (item.id) {
                            reorderData.push({'id': item.id, 'pos': index});
                        }
                    });

                    console.info(reorderData);
                    $.ajax({
                        url: '/admin/event-packages/reorder',
                        type: 'post',
                        dataType: 'json',
                        data: {'packages': reorderData}
                    });
                }
            }
        });

        function getGridCurrencies() {
            let gridCurrencies = [];
            for (let i = 0; i < currencies.length; ++i) {
                gridCurrencies.push({value: currencies[i].id, text: currencies[i].code});
            }

            return gridCurrencies;
        }

        function cardSizeDropDownEditor(container, options) {
            $('<input name="' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    dataSource: eventPackageCardSizes
                });
        }

        function statusDropDownEditor(container, options) {
            $('<input name="' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    dataSource: eventPackageStatuses
                });
        }

        $("#package_details").kendoEditor({
            resizable: {
                content: true,
                toolbar: true
            }
        });

        $('#event-packages-grid-add').click(function () {
            $packagesGridElement.data("kendoGrid").addRow();
        });

        var perksBlocIconCropCroppie;

        $packagesGridElement
            .on('click', '.save-event-package', function (e) {
                $packagesGridElement.data("kendoGrid").saveChanges();
            })
            .on('click', '.edit-event-package', function (e) {
                let row = $(e.target).closest("tr");
                $packagesGridElement.data("kendoGrid").editRow(row);
            })
            .on('click', '.edit-details-event-package', function () {
                let model = $packagesGridElement.data("kendoGrid").dataSource.getByUid($(this).data('uid'));

                model.is_hidden = !model.is_visible;

                validator.hideMessages();
                packageViewModel.set("selected", model);

                $('.drop-croppie-package-card').attr('data-url', model.card_image_url);
                $('.drop-croppie-package-header').attr('data-url', model.header_image_url);

                cardDropCroppie.buildPreview();
                headerCropCroppie.buildPreview();

                $('#drop-croppie-modal').on('hidden.bs.modal', function () {
                    if ($('#edit-event-package-modal:visible').length) {
                        $('body').addClass('modal-open');
                    }
                });

                if (!$('.drop-croppie-perks-icon').hasClass('drop-croppie-exists')) {
                    perksBlocIconCropCroppie = $('.drop-croppie-perks-icon').dropCroppie({
                        general: {
                            result: '#package_perks_block_icon'
                        },
                        crop: {
                            viewport: {
                                width: 30,
                                height: 30
                            }
                        }
                    });
                }

                perksBlocIconCropCroppie.buildPreview();

                $editPackageModal.modal('show');
            })
            .on('click', '.cancel-event-package', function (e) {
                $packagesGridElement.data("kendoGrid").cancelChanges();
            })
            .on('click', '.delete-event-package', function () {
                $packagesGridElement.data("kendoGrid").removeRow($(this).closest('tr'));
            });

        $("#js-event-package-sortable-list").kendoSortable({
            handler: ".js-event-package-handler",
            filter: ".js-event-package-sortable",
            ignore: ".js-event-package-sortable-panel *",
            axis: "y",
            placeholder: function (element) {
                return element.clone().addClass("placeholder");
            },
            hint: function (element) {
                return element.clone().addClass("hint")
                    .width(element.width());
            },
            change: function () {
                $('.event-package-block-pos').each(function (index, item) {
                    $(item).val(index).trigger('change');
                });
            }
        });

        var cardDropCroppie = $('.drop-croppie-package-card').dropCroppie({
            general: {
                result: '#package_card_image'
            },
            crop: {
                viewport: {
                    width: 555,
                    height: 148
                },
                result: {
                    size: {
                        width: 1110,
                        height: 295
                    }
                }
            }
        });

        var headerCropCroppie = $('.drop-croppie-package-header').dropCroppie({
            general: {
                result: '#package_header_image'
            },
            crop: {
                viewport: {
                    width: 788,
                    height: 300,
                }
            }
        });
    }
});

