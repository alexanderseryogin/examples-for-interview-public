import * as Autosave from '../shared/autosave';

$(document).ready(function () {
    let $list = $('#event-faqs-list');

    Autosave.init($('#event-faqs-form'), formAutoSaved);

    $('#event-add-faq').click(function (e) {
        let $this = $(this);

        e.preventDefault();
        $.get($this.attr('data-url'), function (response) {
            $('#event-faqs-list').append(response.payload.html);
            initTitleAutoComplete($('input.event-faq-title:last'));
            $('#event-faqs-list .js-editor:last').kendoEditor({
                resizable: {
                    content: true,
                    toolbar: true
                }
            });
        });
    });

    $list.on('click', '.event-remove-faq', function (e) {
        e.preventDefault();

        $(this).closest('.event-faq').remove();
    });

    $('#event-faqs .js-editor').kendoEditor({
        encoded: false,
        resizable: {
            content: true,
            toolbar: true
        }
    });

    $("#event-faqs .js-sortable-list").kendoSortable({
        handler: ".js-handler",
        filter: ".js-sortable",
        ignore: ".js-sortable-panel *",
        axis: "y",
        placeholder: function (element) {
            return element.clone().addClass("placeholder");
        },
        hint: function (element) {
            return element.clone().addClass("hint")
                .width(element.width());
        },
        change: function () {
            $('.event-faq-pos').each(function (index, item) {
                $(item).val(index).trigger('change');
            });
            $("#event-faqs .js-editor").each(function (index, item) {
                $(item).data("kendoEditor").refresh();
            });
        }
    });

    initTitleAutoComplete($('input.event-faq-title'));

    function initTitleAutoComplete(element) {
        element.kendoAutoComplete({
            dataTextField: 'title',
            dataValueField: 'title',
            filter: 'contains',
            minLength: 3,
            enforceMinLength: true,
            dataSource: {
                serverFiltering: true,
                transport: {
                    read: {
                        url: $list.attr('data-url-all')
                    },
                    parameterMap: function (data) {
                        if (data.filter && data.filter.filters && data.filter.filters.length) {
                            return {
                                q: data.filter.filters[0].value
                            };
                        }
                    }
                },
                schema: {
                    data: function (response) {
                        return response.payload || [];
                    }
                }
            },
            noDataTemplate: $('#noDataTemplate').html(),
            select: function (e) {
                let body = e.dataItem && e.dataItem.body ? e.dataItem.body : '';
                let bodyElement = $(this.element).closest('.js-sortable-panel').find('.js-event-faq-body');
                bodyElement.data("kendoEditor").value(body);
            }
        });
    }

    function formAutoSaved(response) {
        if (response.payload) {
            for (let key in response.payload) {
                $('#faqs_' + key + '_id').val(response.payload[key].id);
                $('#faqs_' + key + '_pos').val(response.payload[key].pos);
            }
        }
    }
});
