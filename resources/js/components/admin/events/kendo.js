import {getSelectedIds} from '../shared/kendo';
import {$grid, crudServiceBaseUrl} from './constants';

export function gridInit($el, options) {
    const defaultOptions = {
        sortable: true,
        groupable: true,
        pageable: true,
        scrollable: true,
        resizable: true,
        navigatable: true,
        columnMenu: true,
        toolbar: ['create', 'save', 'cancel'],
        editable: "popup",
        filterable: {
            extra: false,
            operators: {
                string: {
                    contains: 'Contains'
                },
                date: {
                    eq: "Is equal to",
                    gt: "Is after",
                    lt: "Is before"
                }
            }
        },
        columnHide: function() {
            $('.k-grid-content.k-auto-scrollable').getNiceScroll().resize();
        },
        columnShow: function() {
            $('.k-grid-content.k-auto-scrollable').getNiceScroll().resize();
        }
    };
    Object.assign(options, defaultOptions);

    $el.kendoGrid(options);

    $el.data("kendoGrid").autoFitColumn(0);
}

export function getGridColumns() {
    return [
        {
            title: 'Select All',
            selectable: true,
            width: "50px"
        },
        {field: 'name', title: 'Event name', template: `<a href="${crudServiceBaseUrl}/#= id #">#: name # </a>`, width: "220px"},
        {field: 'start_date', format: '{0:dd/MM/yyyy}', title: 'Start date', width: "120px"},
        {field: 'start_time', title: 'Start time', format: "{0:HH:mm}", filterable: {ui: "timepicker"}, width: "115px"},
        {field: 'venue_name', title: 'Venue', width: '150px'},
        {field: 'venue_city', title: 'City', width: '150px'},
        {
            field: 'status',
            title: 'Event status',
            template: "<div class='label label-#= status.toLowerCase()#'>#= status #</div>",
            groupHeaderTemplate: "Event status: <div class='label label-#= value.toLowerCase() #'>#= value #</div>",
            filterable: {
                operators: {
                    eq: "Is equal to"
                },
                ui: statusFilter,
            },
            width: '140px'
        },
        {field: 'company_name', title: 'Event Organizer', width: '200px'},
        {
            template: kendo.template($("#buttonMenuTemplate").html()),
            title: "Actions",
            width: "90px"
        }
    ];
}

export function batchUpdateEventStatusModal(action) {
    let ids = getSelectedIds($grid);

    if (!ids.length) {
        alert('Please select an event');
        return;
    }

    let $modal = $('#modalUpdateEvents');
    let $updateBtn = $('#updateEventStatus');

    $('.content-title', $modal).text(getUpdateEventStatusModalTitle(action, true));
    $('.content-sub-title', $modal).text(getUpdateEventStatusModalSubTitle(action, true));

    $updateBtn.data('action', action);
    $updateBtn.data('id', null);

    $modal.modal('show');
}

export function updateEventStatusModal(id, action) {
    let $modal = $('#modalUpdateEvents');
    let $updateBtn = $('#updateEventStatus');

    $('.content-title', $modal).text(getUpdateEventStatusModalTitle(action));
    $('.content-sub-title', $modal).text(getUpdateEventStatusModalSubTitle(action));

    $updateBtn.data('action', action);
    $updateBtn.data('id', id);

    $modal.modal('show');
}

export function addNewEventOrganizer(value) {
    $('#company_name').val(value);
    $('#create-new-company-modal').modal('show');
}

function statusFilter(element) {
    element.kendoDropDownList({
        dataSource: eventStatuses
    });
}

function getUpdateEventStatusModalTitle(action, batch = false) {
    return 'Are you sure you want to ' + action + ' ' + (batch ? 'these events' : 'this event') + '?';
}

function getUpdateEventStatusModalSubTitle(action, batch = false) {
    let subTitle = '';
    let strPart = batch ? 'these events' : 'this event';

    switch (action) {
        case 'delete':
            subTitle = batch
                ? 'Delete these will remove them completely from the system.'
                : 'Delete this will remove it completely from the system.';
            break;
        default:
            action = action.charAt(0).toUpperCase() + action.slice(1);
            subTitle = action + 'ing ' + strPart + ' will also affect the front end interface';
            break;
    }

    return subTitle;
}

export function kendoComboBox($el, options) {
    const defaults = {
        dataValueField: 'id',
        dataTextField: 'name',
        filter: 'contains',
        minLength: 2,
        suggest: false,
        noDataTemplate: $('#no-data-template').html(),
        placeholder: 'Type and select item'
    };
    options = Object.assign({}, defaults, options);

    $el.kendoComboBox(options);
}