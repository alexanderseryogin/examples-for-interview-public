export const crudServiceBaseUrl = '/admin/events';
export const listViewsUrl = '/admin/list-views';
export const pageSize = 20;
export const $grid = $('#grid');
export const $selectView = $('#select-view');