export function getFields() {
    return {
        id: {type: 'number', editable: false},
        email: {type: 'string', editable: false},
        company_name: {type: 'string', editable: false},
        role_name: {type: 'string'}
    }
}