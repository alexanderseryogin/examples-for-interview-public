import * as CompanyCreate from '../company/create';
import {getCompaniesDataSource} from '../companies/data';
import {getTeamsDataSource} from '../teams/data';
import {getEventsDataSource} from './data';
import {
    addNewCompany,
    kendoComboBox,
    kendoMultiSelect,
    multiSelectEmails,
} from './kendo';

window.addNewCompany = addNewCompany;

$(document).ready(function () {

    multiSelectEmails($('#emails'));

    kendoMultiSelect($('#events'), {
        placeholder: 'Type and select event',
        dataSource: getEventsDataSource(),
    });

    const companyField = $('#company_id');

    kendoComboBox(companyField, {
        filter: 'contains',
        minLength: 1,
        suggest: false,
        dataTextField: 'name',
        dataValueField: 'id',
        placeholder: 'Type and search company name',
        dataSource: getCompaniesDataSource()
    });

    CompanyCreate.init(companyField);

    kendoComboBox($('#team_id'), {
        placeholder: 'Type and search team',
        dataSource: getTeamsDataSource(),
    });
});