export function gridInit($el, options) {
    const defaultOptions = {
        sortable: {allowUnsort: true},
        groupable: true,
        pageable: true,
        scrollable: true,
        resizable: true,
        navigatable: true,
        columnMenu: true,
        editable: false,
        filterable: {
            extra: false,
            operators: {
                string: {
                    contains: 'Contains'
                },
                date: {
                    eq: 'Is equal to',
                    gt: 'Is after',
                    lt: 'Is before'
                }
            }
        },
        columnHide: function () {
            $('.k-grid-content.k-auto-scrollable').getNiceScroll().resize();
        },
        columnShow: function () {
            $('.k-grid-content.k-auto-scrollable').getNiceScroll().resize();
        }
    };
    Object.assign(options, defaultOptions);

    $el.kendoGrid(options);
}

export function getGridColumns() {
    return [
        {field: 'email', title: 'Email address', width: '300px'},
        {field: 'company_name', title: 'Company', width: '200px'},
        {field: 'role_name', title: 'Role', width: '200px'}
    ];
}

export function kendoMultiSelect($el, options) {
    const defaults = {
        dataValueField: 'id',
        dataTextField: 'name',
        filter: 'contains',
        minLength: 2,
        autoBind: true,
        suggest: false,
        placeholder: 'Select item'
    };
    options = Object.assign({}, defaults, options);

    $el.kendoMultiSelect(options);
}

export function kendoComboBox($el, options) {
    const defaults = {
        dataValueField: 'id',
        dataTextField: 'name',
        filter: 'contains',
        minLength: 2,
        suggest: false,
        noDataTemplate: $('#no-data-template').html(),
        placeholder: 'Type and select item'
    };
    options = Object.assign({}, defaults, options);

    $el.kendoComboBox(options);
}

export function multiSelectEmails($el) {

    const KEY_CODE_SEMICOLON = 186;
    const KEY_CODE_COLON = 188;
    const KEY_CODE_SPACE = 32;
    const KEY_CODE_ENTER = 13;

    const keyCodes = [
        KEY_CODE_SEMICOLON,
        KEY_CODE_COLON,
        KEY_CODE_SPACE,
        KEY_CODE_ENTER
    ];

    function onDataBound() {
        let $kendoInput = $('.k-multiselect #emails_taglist + .k-input');
        $kendoInput.unbind('keyup');
        $kendoInput.on('keydown change', onClickEnter);
    }

    function onClickEnter(e) {
        if (keyCodes.includes(e.keyCode) || e.type === 'change') {
            let widget = $el.getKendoMultiSelect();
            let dataSource = widget.dataSource;
            let value = $(this).val().replace(/[,;]/, '').trim();
            if (!value || value.length === 0) {
                return;
            }
            let newItem = {
                email: value
            };

            dataSource.add(newItem);
            let newValue = newItem.email;
            widget.value(widget.value().concat([newValue]));

            return false;
        }
    }

    kendoMultiSelect($el, {
        dataTextField: 'email',
        dataValueField: 'email',
        dataSource: {
            data: []
        },
        placeholder: 'Type user email',
        dataBound: onDataBound
    })
}

export function addNewCompany(value) {
    $('#company_name').val(value);
    $('#create-new-company-modal').modal('show');
}