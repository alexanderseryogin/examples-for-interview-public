import {crudEventsBaseUrl, $grid, $selectView, crudServiceBaseUrl, listViewsUrl, pageSize} from './constants';
import {restoreSavedOptions} from "../shared/kendo";

export function getEventsDataSource(options) {
    const defaults = {
        transport: {
            read: {
                url: crudEventsBaseUrl
            }
        },
        schema: {
            data: function (response) {
                return response.payload && response.payload.items || [];
            }
        }
    };

    options = Object.assign({}, defaults, options);

    return new kendo.data.DataSource(options);
}

export function getGridDataSource(options) {
    const defaultOptions = {
        transport: {
            read: {
                url: crudServiceBaseUrl,
                data: function () {
                    return {
                        q: $('#invites-search').val()
                    };
                }
            },
            parameterMap: function (data, type) {
                return $.extend({'page_size': data.pageSize}, data);
            }
        },
        serverSorting: true,
        serverPaging: true,
        serverFiltering: true,
        serverGrouping: true,
        pageSize: pageSize,
        batch: true
    };

    Object.assign(options, defaultOptions);

    return new kendo.data.DataSource(options);
}

export function getListDataSource(options = {}) {
    const defaultOptions = {
        transport: {
            read: {
                url: listViewsUrl,
                data: {
                    type: 'invites'
                },
                dataType: 'json'
            }
        },
        schema: {
            data: function (response) {
                return response.payload || [];
            }
        },
        requestEnd: function (e) {
            if (e.response && e.response.code === 200) {
                e.response.payload.forEach(function (item) {
                    if (item.is_default === 1) {
                        $selectView.data('kendoComboBox').value(item.id);
                        restoreSavedOptions($grid, item.options);
                    }
                });
            }
        }
    };

    Object.assign(options, defaultOptions);

    return new kendo.data.DataSource(options);
}