export const crudEventsBaseUrl = '/admin/events';
export const crudServiceBaseUrl = '/admin/invites';
export const listViewsUrl = '/admin/list-views';
export const pageSize = 10;
export const $grid = $('#grid-invites');
export const $selectView = $('#select-view');