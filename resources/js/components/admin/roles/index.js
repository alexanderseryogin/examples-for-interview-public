import {kendoMenu, kendoNiceScroll} from '../shared/kendo';
import {deleteRole} from './api';

window.deleteRole = deleteRole;

$(document).ready(function () {

    const $btnMenuTemplate = $('#buttonMenuTemplate');

    if ($btnMenuTemplate.length) {
        const template = kendo.template($btnMenuTemplate.html());

        $('.role-menu').each(function () {
            $(this).append(template({
                id: $(this).data('role-id')
            }));
            kendoMenu($('.button-menu'))
        });
    }

    $('.disabled').on('click', function () {
        return false;
    });

    kendoNiceScroll($('.grid-wrap'));
});