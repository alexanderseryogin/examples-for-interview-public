import {confirmModal} from '../shared/modals';
import {crudServiceBaseUrl} from './constants';

/**
 * Delete role
 *
 * @param id role id for delete
 * @param e
 */
export function deleteRole(id, e) {
    e.preventDefault();
    confirmModal({
        action: 'delete',
        title: 'Are you sure you want to delete\n' + 'this role?',
        subTitle: 'Delete this role will remove it completely from the system',
        done: function (id) {

            $.ajax({
                url: crudServiceBaseUrl + '/' + id,
                method: 'delete'
            })
            .always(function (response) {
                if (response.code === 200) {
                    window.location.href = crudServiceBaseUrl;
                } else {
                    alert('An error occurred');
                }
            });

        }.bind(this, id)
    });
}