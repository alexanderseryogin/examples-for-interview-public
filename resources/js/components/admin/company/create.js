const $targetDropZone = $('.drop-croppie-company');
const $modal = $('#create-new-company-modal');
const $save = $('#company-save');

const options = {
    general: {
        result: '#company_logo'
    },
    crop: {
        viewport: {
            width: 300,
            height: 300,
        }
    }
};

export function init(field) {
    let companyCroppie = $targetDropZone.dropCroppie(options);

    $save.on('click', function () {
        const widget = field.getKendoComboBox();
        const dataSource = widget.dataSource;

        dataSource.add({
            id: 0,
            name: $('#company_name').val(),
            type: $('#company_type').val(),
            logo: $('#company_logo').val()
        });

        dataSource.one('sync', function () {
            widget.select(dataSource.view().length - 1);
        });

        dataSource.sync();

        $targetDropZone.attr('data-url', '');

        companyCroppie.buildPreview();

        $modal.modal('hide');
    });

}

