import {crudBaseUrl as crudCompaniesBaseUrl} from './constants';

export function getCompaniesDataSource(options) {
    const defaults = {
        batch: false,
        transport: {
            read: {
                url: crudCompaniesBaseUrl,
            },
            create: {
                url: crudCompaniesBaseUrl,
                type: 'POST'
            },
            parameterMap: function (data, operation) {
                if (operation === 'create' && data) {
                    return data;
                }
            }
        },
        schema: {
            model: {
                id: 'id',
                fields: {
                    id: {type: 'number', editable: false},
                    name: {type: 'string'},
                }
            },
            data: function (response) {
                return response.payload ? response.payload : [];
            }
        }
    };

    options = Object.assign({}, defaults, options);

    return new kendo.data.DataSource(options);
}