import {getSelectedIds} from '../shared/kendo';
import {
    $changeEmailBtn,
    $changeEmailModal,
    $gridUsers as $grid,
    crudServiceBaseUrl,
} from './constants';
import {getModalSubTitle, getModalTitle} from './kendo';
import {confirmModal, notifyModal} from '../shared/modals';

/**
 * Delete user on it profile page
 *
 * @param id role id for delete
 * @param e
 */
export function deleteUserProfile(id, e) {
    e.preventDefault();
    confirmModal({
        action: 'delete',
        title: 'Are you sure you want to delete\n' + 'this user?',
        subTitle: 'Deleting this user will disable their access to this system, \n' +
            'but their data will remain in the system.',
        done: function (id) {

            $.ajax({
                url: crudServiceBaseUrl + '/' + id,
                method: 'delete'
            })
            .always(function (response) {
                if (response.code === 200) {
                    window.location.href = crudServiceBaseUrl;
                } else {
                    alert('An error occurred');
                }
            });

        }.bind(this, id)
    });
}

/**
 * Delete user on users table
 *
 * @param id venue id for delete
 * @param action
 * @param e
 */
export function deleteUser(id, action, e) {
    e.preventDefault();
    confirmModal({
        action: action,
        title: getModalTitle(action),
        subTitle: getModalSubTitle(action),
        done: function (id) {
            $.ajax({
                url: crudServiceBaseUrl + '/' + id,
                method: 'delete',
                data: {}
            })
            .always(function (response) {
                if (response.code === 200) {
                    let dataSource = $grid.data('kendoGrid').dataSource;
                    let dataItem = dataSource.get(id);
                    dataSource.remove(dataItem);
                } else {
                    alert('An error occurred');
                }
            });

        }.bind(this, id)
    });
}

export function batchDeleteUsers(action, e) {
    e.preventDefault();
    let ids = getSelectedIds($grid);

    if (!ids.length) {
        alert('Please select an user');
        return;
    }
    confirmModal({
        action: action,
        title: getModalTitle(action, true),
        subTitle: getModalSubTitle(action, true),
        done: function (ids) {
            if (ids.length) {
                $.ajax({
                    url: crudServiceBaseUrl + '/batch',
                    method: 'delete',
                    data: {
                        'ids': ids,
                    }
                })
                .always(function (response) {
                    if (response.code === 200) {
                        $grid.data('kendoGrid').setOptions(
                            $grid.data('kendoGrid').getOptions()
                        );
                    } else {
                        alert('An error occurred');
                    }
                });
            }

        }.bind(this, ids)
    });
}

export function updateUser(id, action, e) {
    e.preventDefault();

    confirmModal({
        action: action,
        title: getModalTitle(action, true),
        subTitle: getModalSubTitle(action, true),
        done: function (id) {
            $.ajax({
                url: crudServiceBaseUrl + '/batch',
                method: 'post',
                data: {
                    'ids': [id],
                    'status': action
                }
            })
            .always(function (response) {
                if (response.code === 200) {
                    $grid.data('kendoGrid').setOptions(
                        $grid.data('kendoGrid').getOptions()
                    );
                } else {
                    alert('An error occurred');
                }
            });
        }.bind(this, id)
    });
}

export function batchUpdateUsers(action, e) {
    e.preventDefault();
    let ids = getSelectedIds($grid);
    if (!ids.length) {
        alert('Please select an user');
        return;
    }

    confirmModal({
        action: action,
        title: getModalTitle(action, true),
        subTitle: getModalSubTitle(action, true),
        done: function (ids) {
            if (ids.length) {
                $.ajax({
                    url: crudServiceBaseUrl + '/batch',
                    method: 'post',
                    data: {
                        'ids': ids,
                        'status': action
                    }
                })
                .always(function (response) {
                    if (response.code === 200) {
                        $grid.data('kendoGrid').setOptions(
                            $grid.data('kendoGrid').getOptions()
                        );
                    } else {
                        alert('An error occurred');
                    }
                });
            }

        }.bind(this, ids)
    });
}

export function changeUserEmail(id, currentEmail, e) {
    e.preventDefault();
    $changeEmailModal.find('#current-email').text(currentEmail);
    $changeEmailModal.find('[name=email]').val(currentEmail);
    $changeEmailModal.modal('show');
    $changeEmailBtn.on('click', function() {
        const email = $changeEmailModal.find('[name=email]').val().trim();
        if (!email) {
            alert('Please enter email');
            return;
        }
        $.ajax({
            url: crudServiceBaseUrl + '/' + id + '/email',
            method: 'put',
            data: {
                'email': email,
            },
        }).always(function(response) {
            if (response.code === 200) {
                $grid.data('kendoGrid').
                    setOptions($grid.data('kendoGrid').getOptions());
            } else {
                alert('An error occurred');
            }
        });
        $changeEmailModal.modal('hide');
        $changeEmailBtn.off('click');
    });
}

export function resetUserPassword(email, e) {
    e.preventDefault();
    $.ajax({
        url: '/admin/password/email',
        method: 'post',
        data: {
            'email': email,
        },
    }).always(function(response) {
        if (response.code === 200) {
            notifyModal({
                title: 'Reset password',
                subTitle: 'A password reset email has been sent to this user.',
            });
        } else {
            alert('An error occurred');
        }
    });
}