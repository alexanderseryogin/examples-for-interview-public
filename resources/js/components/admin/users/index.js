import {$selectView} from '../shared/constants';
import {kendoMenu, kendoNiceScroll, saveViewStateInit, tableRefresh} from '../shared/kendo';
import {addNewView, saveListView} from '../shared/listView';
import {
    batchDeleteUsers,
    batchUpdateUsers,
    changeUserEmail,
    deleteUser,
    resetUserPassword,
    updateUser,
} from './api';
import {$gridUsers as $grid} from './constants';
import {
    getListDataSource,
    getUserGridDataSource as getGridDataSource,
} from './data';
import {
    getUsersGridColumns as getGridColumns,
    gridUsersInit as gridInit,
} from './kendo';
import {getUsersFields as getFields} from './models';

/* Function to be accessible in the global scope.
Can called by 'onclick' html attribute */

window.addNewView = addNewView;
window.deleteUser = deleteUser;
window.updateUser = updateUser;
window.batchDeleteUsers = batchDeleteUsers;
window.batchUpdateUsers = batchUpdateUsers;
window.changeUserEmail = changeUserEmail;
window.resetUserPassword = resetUserPassword;


$(document).ready(function () {
    if ($grid.length) {
        const dataSource = getGridDataSource({
            autoSync: true,
            schema: {
                model: {
                    id: 'id',
                    fields: getFields()
                },
                groups: function (response) {
                    return response.payload.items;
                },
                data: function (response) {
                    return response.payload.items;
                },
                total: 'payload.total'
            },

        });

        const gridOptions = {
            columns: getGridColumns(),
            dataSource: dataSource,
            dataBound: function () {
                kendoNiceScroll($('.k-grid-content.k-auto-scrollable'));
                kendoMenu($('.button-menu'), {
                    open: onOpen
                });
            }
        };

        gridInit($grid, gridOptions);

        saveViewStateInit($selectView, getListDataSource(), $grid, gridOptions);

        $('#saveListViewBtn').on('click', function (e) {
            e.preventDefault();
            saveListView($grid, $selectView, 'users')
        });
    }

    $('#users-search-btn').click(function (e) {
        e.preventDefault();
        $(this).closest('form').submit();
    });

    function onOpen(e) {
        let wrapHeight = $(e.item).closest('.k-grid-content')[0].offsetHeight;
        if ($(e.item).position().top >= wrapHeight / 2) {
            $(e.item).find('.k-animation-container').addClass('top').removeClass('bottom')
        } else {
            $(e.item).find('.k-animation-container').addClass('bottom').removeClass('top')
        }
    }

    const $inviteSentModal = $('#inviteSentModal');
    if ($inviteSentModal.length) {
        $inviteSentModal.modal('show');
    }

    tableRefresh($grid);
});