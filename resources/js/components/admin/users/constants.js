export const crudServiceBaseUrl = '/admin/users';
export const listViewsUrl = '/admin/list-views';
export const pageSize = 10;
export const $gridUsers = $('#grid.grid-users');
export const $gridEvents = $('#grid.grid-events');
export const $selectView = $('#select-view');
export const $changeEmailModal = $('#change-email-modal');
export const $changeEmailBtn = $('#change-email-btn');