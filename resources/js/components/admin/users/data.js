import {
    $gridUsers as $grid,
    $selectView,
    crudServiceBaseUrl,
    listViewsUrl,
    pageSize,
} from './constants';
import {restoreSavedOptions} from '../shared/kendo';

export function getUserGridDataSource(options) {
    const defaultOptions = {
        transport: {
            read: {
                url: crudServiceBaseUrl,
                data: function () {
                    return {
                        q: $('#users-search').val()
                    };
                },
            },
            update: {
                url: function (options) {
                    return crudServiceBaseUrl + '/' + options.id
                },
                method: "put"
            },
            parameterMap: function (data, type) {
                if (data.filter) {
                    data.filter.filters.map(function (item) {
                        if (item.field === 'last_active_at') {
                            item.value = kendo.toString(item.value, 'yyyy-MM-dd');
                        }
                    });
                }
                return $.extend({'page_size': data.pageSize}, data);
            }
        },
        serverSorting: true,
        serverPaging: true,
        serverFiltering: true,
        pageSize: pageSize,
        serverGrouping: true,
        batch: false
    };

    options = Object.assign({}, defaultOptions, options);

    return new kendo.data.DataSource(options);
}

export function getEventGridDataSource(options) {
    const defaultOptions = {
        transport: {
            read: {
                url: crudServiceBaseUrl + '/' + userId + '/events',
                data: {
                    'creator_id': userId
                }
            },
        },
        serverSorting: true,
        serverPaging: true,
        pageSize: pageSize,
    };

    options = Object.assign({}, defaultOptions, options);

    return new kendo.data.DataSource(options);
}

export function getListDataSource(options = {}) {
    const defaultOptions = {
        transport: {
            read: {
                url: listViewsUrl,
                data: {
                    type: 'users'
                },
                dataType: 'json'
            }
        },
        schema: {
            data: function (response) {
                return response.payload || [];
            }
        },
        requestEnd: function (e) {
            if (e.response && e.response.code === 200) {
                e.response.payload.forEach(function (item) {
                    if (item.is_default === 1) {
                        $selectView.data('kendoComboBox').value(item.id);
                        restoreSavedOptions($grid, item.options);
                    }
                });
            }
        }
    };

    options = Object.assign({}, defaultOptions, options);

    return new kendo.data.DataSource(options);
}