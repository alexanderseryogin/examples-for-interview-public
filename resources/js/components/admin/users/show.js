import {kendoNiceScroll, tableRefresh} from '../shared/kendo';
import {deleteUserProfile} from './api';
import {$gridEvents as $grid} from './constants';
import {getEventGridDataSource as getGridDataSource} from './data';
import {
    getEventsGridColumns as getGridColumns,
    gridEventsInit as gridInit,
} from './kendo';
import {getEventsFields as getFields} from './models';

window.deleteUserProfile = deleteUserProfile;

$(document).ready(function () {

    if ($grid.length) {

        const dataSource = getGridDataSource({

            schema: {
                model: {
                    id: 'id',
                    fields: getFields()
                },
                data: function (response) {
                    return response.payload.items;
                },
                total: 'payload.total'
            },

        });


        const gridOptions = {
            columns: getGridColumns(),
            dataSource: dataSource,
            dataBound: function () {
                kendoNiceScroll($('.k-grid-content.k-auto-scrollable'));
            }
        };

        gridInit($grid, gridOptions);

        $('.js-get-company-event-btn').on('click', function() {
            $(this).addClass('active');
            $('.js-get-user-event-btn').removeClass('active');
            gridOptions.dataSource.transport.options.read.data = {
                'company_id': companyId,
            };
            let grid = $grid.data('kendoGrid');

            grid.dataSource.read();
            grid.refresh();

        });

        $('.js-get-user-event-btn').on('click', function() {
            $(this).addClass('active');
            $('.js-get-company-event-btn').removeClass('active');
            gridOptions.dataSource.transport.options.read.data = {
                'creator_id': userId,
            };
            let grid = $grid.data('kendoGrid');

            grid.dataSource.read();
            grid.refresh();

        });
    }

    tableRefresh($grid);
});