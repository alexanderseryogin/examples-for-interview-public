const $targetDropZone = $('.drop-croppie-avatar');
const result = '#avatar';

const options = {
    general: {
        result: result
    },
    crop: {
        viewport: {
            width: 400,
            height: 400,
        }
    }
};

export function init() {

    $targetDropZone.dropCroppie(options);

}

