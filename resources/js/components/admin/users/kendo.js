import {crudServiceBaseUrl as crudUsersBaseUrl} from '../users/constants';
import {crudServiceBaseUrl as crudVenuesBaseUrl} from '../venues/constants';
import {crudServiceBaseUrl as crudEventsBaseUrl} from '../events/constants';

export function gridUsersInit($el, options) {
    const defaults = {
        sortable: {allowUnsort: true},
        groupable: true,
        pageable: true,
        scrollable: true,
        navigatable: true,
        resizable: true,
        columnMenu: true,
        editable: true,
        filterable: {
            extra: false,
            operators: {
                string: {
                    contains: 'Contains'
                },
                date: {
                    eq: 'Is equal to',
                    gt: 'Is after',
                    lt: 'Is before'
                }
            }
        },
        columnHide: function() {
            $('.k-grid-content.k-auto-scrollable').getNiceScroll().resize();
        },
        columnShow: function() {
            $('.k-grid-content.k-auto-scrollable').getNiceScroll().resize();
        }
    };
    options = $.extend(true, {}, defaults, options);

    $el.kendoGrid(options);

    $el.data("kendoGrid").autoFitColumn(0);
}

export function gridEventsInit($el, options) {
    const defaults = {
        sortable: true,
        pageable: true,
        scrollable: true,
        navigatable: true,
        resizable: true,
        columnMenu: false,
        editable: false,
    };
    options = $.extend(true, {}, defaults, options);

    $el.kendoGrid(options);

    $el.data("kendoGrid").autoFitColumn(0);
}

export function getUsersGridColumns() {
    return [
        {
            title: 'Select All',
            selectable: true,
            width: '50px'
        },
        {
            title: 'User ID',
            field: 'id',
            hidden: true
        },
        {
            field: 'first_name',
            title: 'First name',
            template: `<a href="${crudUsersBaseUrl}/#= id #">#= first_name #</a>`,
            width: '120px'
        },
        {
            field: 'last_name',
            title: 'Last name',
            template: `<a href="${crudUsersBaseUrl}/#= id #">#= last_name #</a>`,
            width: '150px'
        },
        {
            field: 'role',
            title: 'Role',
            filterable: {
                operators: {
                    eq: "Is equal to"
                },
                ui: roleFilter,
            },
            values: getGridRoles(),
            width: '140px'
        },
        {field: 'company', title: 'Company', value: 'Company name',width: '140px'},
        {field: 'email', title: 'Email address', width: '220px'},
        {
            title: 'Account status',
            field: 'status',
            template: '<dd><span class="round-status #= status #"></span>#= status #</dd>',
            filterable: {
                operators: {
                    eq: "Is equal to"
                },
                ui: statusFilter,
            },
            editor: statusDropDownEditor,
            width: '150px',
        },
        {field: 'last_active_at', title: 'Last time active', format: '{0:dd/M/yyyy, H:MM}', width: '150px'},
        {
            template: kendo.template($('#buttonMenuTemplate').html()),
            title: 'Actions',
            width: '100px'
        }
    ];
}

export function getEventsGridColumns() {
    return [
        {
            field: 'name',
            title: 'Event name',
            template: `<a href="${crudEventsBaseUrl}/#= id #">#: name # </a>`,
            width: '120px'
        },
        {field: 'start_date', format: '{0:MM/dd/yyyy}', title: 'Start date', width: '130px'},
        {field: 'start_date', title: 'Start time', format: '{0:HH:mm}', width: '130px'},
        {
            field: 'venue_name',
            title: 'Venue',
            template: function (dataItem) {
                return dataItem.venue_id ? `<a href="${crudVenuesBaseUrl}/${dataItem.venue_id}">${dataItem.venue_name}</a>` : '';
            },
            width: '150px'
        },
        {
            field: 'status',
            title: 'Event status',
            template: "<div class='label label-#= status.toLowerCase()#'>#= status #</div>",
            width: '150px',
        }
    ];
}

/**
 *
 * @param action
 * @param batch
 * @returns {string}
 */
export function getModalTitle(action, batch = false) {
    let title = '';
    let strPart = batch ? 'these users' : 'this user';

    switch (action) {
        case 'delete':
            title = 'Are you sure you want to ' + action + ' ' + strPart + '?';
            break;
        default:
            title = 'Are you sure you want to confirm this action?';
            break;
    }

    return title;
}

/**
 *
 * @param action
 * @param batch
 * @returns {string}
 */
export function getModalSubTitle(action, batch = false) {
    let subTitle = '';
    let strPart = batch ? 'these users' : 'this user';

    switch (action) {
        case 'delete':
            subTitle = batch
                ? 'Delete these will remove them completely from the system.'
                : 'Delete this will remove it completely from the system.';
            break;
        default:
            action = action.charAt(0).toUpperCase() + action.slice(1);
            subTitle = action + 'ing ' + strPart + ' will also affect the front end interface';
            break;
    }

    return subTitle;
}

function statusFilter(element) {
    element.kendoDropDownList({
        dataSource: userStatuses
    });
}

function roleFilter(element) {
    element.kendoDropDownList({
        dataSource: userRoles
    });
}

function statusDropDownEditor(container, options) {
    $('<input name="' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            autoBind: false,
            dataSource: userStatuses
        });
}

function getGridRoles() {
    let gridRoles = [];
    for (let i = 0; i < userRoles.length; ++i) {
        gridRoles.push({value: userRoles[i].id, text: userRoles[i].name});
    }

    return gridRoles;
}