export function getUsersFields() {
    return {
        id: {type: 'number', editable: false},
        last_active_at: {type: 'date', editable: false},
        last_name: {type: 'string', editable: true},
        first_name: {type: 'string', editable: true},
        role_name: {type: 'string', editable: true},
        role: {type: 'number', editable: true},
        company: {type: 'string', editable: false},
        company_id: {type: 'number', editable: false},
        email: {type: 'string', editable: false},
        status: {type: 'string', editable: true},
    };
}

export function getEventsFields() {
    return {
        id: {type: 'number', editable: false},
        title: {type: 'string', validation: {required: true}},
        start_date: {type: 'date'},
        start_time: {type: 'date'},
        venue_name: {type: 'string'},
        status: {type: 'string'},
    }
}