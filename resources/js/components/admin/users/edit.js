import {getCompaniesDataSource} from '../companies/data';
import {kendoComboBox} from '../invites/kendo';
import {getTeamsDataSource} from '../teams/data';
import * as AvatarDropCroppie from './drop-croppie';

$(document).ready(function () {

    AvatarDropCroppie.init();

    kendoComboBox($('#company_id'), {
        placeholder: 'Type and search company name',
        dataSource: getCompaniesDataSource(),
    });

    kendoComboBox($('#team_id'), {
        placeholder: 'Type and search team',
        dataSource: getTeamsDataSource(),
    });

});