import * as googleMap from './googleMap';
import {kendoComboBox} from './kendo';
import {getCompaniesDataSource} from "../companies/data";

$(document).ready(function () {

    googleMap.init();

    const $seatingLayout = $('#seating_layout_file');

    if ($seatingLayout.length) {
        $seatingLayout.kendoUpload({
            async: {
                saveUrl: '/admin/images',
                autoUpload: true,
                saveField: 'image'

            },
            dropZone: '.drop-zone',
            validation: {
                allowedExtensions: ['.jpg', '.jpeg', '.png', '.bmp', '.gif'],
            },
            upload: function (e) {
                e.data = {
                    '_token': $("meta[name='csrf-token']").attr('content')
                };
            },
            success: function (e) {
                if (e.operation === 'upload') {
                    let file = e.response.payload;
                    $('#drop-zone-seating-layout-uploaded').prepend('<img src="' + file.url + '" alt="">').show();
                    $('#drop-zone-seating-layout-upload').hide();
                    $('#seating_layout_image').val(file.path);
                    seatingLayoutKendoData.disable();
                }
            },
            showFileList: true,
            multiple: false,
        });

        let seatingLayoutKendoData = $seatingLayout.data("kendoUpload");

        $('.drop-zone-seating-layout').on('click', function (e) {
            e.preventDefault();
            $('#seating_layout_file').click();
        });

        $('.drop-zone-delete').on('click', function (e) {
            e.stopPropagation();
            $('#seating_layout_image').val('');
            $('#drop-zone-seating-layout-uploaded').hide().find('img').remove();
            $('#drop-zone-seating-layout-upload').show();
            seatingLayoutKendoData.enable();
        });
    }

    const $companyField = $('#company_id');

    if ($companyField.length) {
        kendoComboBox($companyField, {
            placeholder: 'Type and search company name',
            dataSource: getCompaniesDataSource(),
        });
    }
});