export const crudServiceBaseUrl = '/admin/venues';
export const listViewsUrl = '/admin/list-views';
export const pageSize = 10;
export const $grid = $('#grid');
export const $selectView = $('#select-view');

export const gmapIframeRegexp = /<iframe\s*src="https:\/\/www\.google\.com\/maps\/embed\?[^"]+"*\s*[^>]+>*<\/iframe>/;
export const $mapBlock = $('#google-map');
export const $mapLink = $('#map_link');