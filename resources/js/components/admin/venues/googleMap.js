/* Insert google maps iframe */
import {$mapBlock, $mapLink, gmapIframeRegexp} from './constants';

export function init() {
    $mapLink.on('input', function () {
        const embedHtml = $(this).val();
        if (gmapIframeRegexp.test(embedHtml)) {
            $mapBlock.html(embedHtml)
        } else {
            $mapBlock.text($mapBlock.data('placeholder'))
        }
    });

    if ($mapLink.val() !== '') {
        $mapLink.trigger('input');
    }
}
