export function getFields() {
    return {
        id: {type: 'number', editable: false},
        name: {type: 'string', validation: {required: true}},
        street_name: {type: 'string'},
        address1: {type: 'string'},
        address2: {type: 'string'},
        postcode: {type: 'string'},
        state: {type: 'string'},
        country_name: {type: 'string'},
        city: {type: 'string'},
        events_count: {type: 'number', editable: false}
    }
}