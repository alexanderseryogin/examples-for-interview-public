import {kendoMenu, kendoNiceScroll, saveViewStateInit, tableRefresh} from '../shared/kendo';
import {$grid, $selectView} from './constants';
import {getGridDataSource, getListDataSource} from './data';
import {
    batchDeleteVenue,
    deleteVenue,
    getGridColumns,
    gridInit,
} from './kendo';
import {getFields} from './models';
import {addNewView, saveListView} from '../shared/listView';

/* Function to be accessible in the global scope.
Can called by 'onclick' html attribute */

window.addNewView = addNewView;
window.deleteVenue = deleteVenue;
window.batchDeleteVenue = batchDeleteVenue;

$(document).ready(function () {

    const dataSource = getGridDataSource({
        schema: {
            model: {
                id: 'id',
                fields: getFields()
            },
            groups: function (response) {
                return response.payload.items;
            },
            data: function (response) {
                return response.payload.items;
            },
            total: 'payload.total'
        },
    });


    if ($grid.length) {

        const gridOptions = {
            columns: getGridColumns(),
            dataSource: dataSource,
            dataBound: function () {
                kendoMenu($('.button-menu'));
                kendoNiceScroll($('.k-grid-content.k-auto-scrollable'));
            }
        };

        gridInit($grid, gridOptions);

        saveViewStateInit($selectView, getListDataSource(), $grid, gridOptions);

        $('#saveListViewBtn').on('click', function (e) {
            e.preventDefault();
            saveListView($grid, $selectView, 'venues')
        });

    }


    $('#venues-search-btn').click(function () {
        $(this).closest('form').submit();
    });

    tableRefresh($grid);
});