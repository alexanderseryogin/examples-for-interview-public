import {confirmModal} from '../shared/modals';
import {getSelectedIds} from '../shared/kendo';
import {$grid, crudServiceBaseUrl} from './constants';

export function gridInit($el, options) {
    const defaultOptions = {
        sortable: {allowUnsort: true},
        groupable: true,
        pageable: true,
        scrollable: true,
        resizable: true,
        navigatable: true,
        columnMenu: true,
        toolbar: ['create', 'save', 'cancel'],
        editable: false,
        filterable: {
            extra: false,
            operators: {
                string: {
                    contains: 'Contains'
                },
                date: {
                    eq: 'Is equal to',
                    gt: 'Is after',
                    lt: 'Is before'
                }
            }
        },
        columnHide: function() {
            $('.k-grid-content.k-auto-scrollable').getNiceScroll().resize();
        },
        columnShow: function() {
            $('.k-grid-content.k-auto-scrollable').getNiceScroll().resize();
        }
    };
    Object.assign(options, defaultOptions);

    $el.kendoGrid(options);

    $el.data("kendoGrid").autoFitColumn(0);
}

export function getGridColumns() {
    return [
        {
            title: 'Select All',
            selectable: true,
            width: "50px"
        },
        {
            field: 'name',
            title: 'Venue name',
            template: `<a href="${crudServiceBaseUrl}/#= id #">#: name # </a>`,
            width: "200px",
        },
        {field: 'street_name', title: 'Street name', width: "200px"},
        {field: 'postcode', title: 'Postcode', width: '120px'},
        {field: 'city', title: 'City', width: '170px'},
        {field: 'country_name', title: 'Country', width: '170px'},
        {field: 'events_count', title: 'No. of event', width: '130px'},
        {
            template: kendo.template($("#buttonMenuTemplate").html()),
            title: 'Actions',
            width: '80px'
        }
    ];
}

export function batchDeleteVenue(action, e) {
    e.preventDefault();
    let ids = getSelectedIds($grid);

    if (!ids.length) {
        alert('Please select a venue');
        return;
    }
    confirmModal({
        action: action,
        title: getModalTitle(action, true),
        subTitle: getModalSubTitle(action, true),
        done: function (ids) {

            if (ids.length) {
                $.ajax({
                    url: crudServiceBaseUrl + '/batch',
                    method: 'delete',
                    data: {
                        'ids': ids,
                    }
                })
                .always(function (response) {
                    if (response.code === 200) {
                        let dataSource = $grid.data('kendoGrid').dataSource;
                        ids.forEach(function (id) {
                            let dataItem = dataSource.get(id);
                            dataSource.remove(dataItem);
                        });
                    } else {
                        alert('An error occurred');
                    }
                });
            }

        }.bind(this, ids)
    });
}

/**
 * Delete venue
 *
 * @param id venue id for delete
 * @param action
 * @param e
 */
export function deleteVenue(id, action, e) {
    e.preventDefault();
    confirmModal({
        action: action,
        title: getModalTitle(action),
        subTitle: getModalSubTitle(action),
        done: function (id) {
            $.ajax({
                url: crudServiceBaseUrl + '/' + id,
                method: 'delete',
                data: {}
            })
            .always(function (response) {
                if (response.code === 200) {
                    let dataSource = $grid.data('kendoGrid').dataSource;
                    let dataItem = dataSource.get(id);
                    dataSource.remove(dataItem);
                } else {
                    alert('An error occurred');
                }
            });

        }.bind(this, id)
    });
}

/* Private */

/**
 *
 * @param action
 * @param batch
 * @returns {string}
 */
function getModalTitle(action, batch = false) {
    let title = '';
    let strPart = batch ? 'these venues' : 'this venue';

    switch (action) {
        case 'delete':
            title = 'Are you sure you want to ' + action + ' ' + strPart + '?';
            break;
        default:
            title = 'Are you sure you want to confirm this action?';
            break;
    }

    return title;
}

/**
 *
 * @param action
 * @param batch
 * @returns {string}
 */
function getModalSubTitle(action, batch = false) {
    let subTitle = '';
    let strPart = batch ? 'these venues' : 'this venue';

    switch (action) {
        case 'delete':
            subTitle = batch
                ? 'Delete these will remove them completely from the system.'
                : 'Delete this will remove it completely from the system.';
            break;
        default:
            action = action.charAt(0).toUpperCase() + action.slice(1);
            subTitle = action + 'ing ' + strPart + ' will also affect the front end interface';
            break;
    }

    return subTitle;
}

export function kendoComboBox($el, options) {
    const defaults = {
        dataValueField: 'id',
        dataTextField: 'name',
        filter: 'contains',
        minLength: 2,
        suggest: false,
        noDataTemplate: $('#no-data-template').html(),
        placeholder: 'Type and select item'
    };
    options = Object.assign({}, defaults, options);

    $el.kendoComboBox(options);
}



