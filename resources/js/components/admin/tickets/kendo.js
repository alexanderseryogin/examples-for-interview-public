import {confirmModal} from '../shared/modals';
import {getSelectedIds} from '../shared/kendo';
import {$grid} from '../tickets/constants';
import {crudServiceBaseUrl as crudEventsBaseUrl} from '../events/constants';

export function gridInit($el, options) {
    const defaultOptions = {
        noRecords: true,
        sortable: true,
        groupable: true,
        pageable: true,
        scrollable: true,
        navigatable: true,
        resizable: true,
        columnMenu: true,
        toolbar: ['create', 'save', 'cancel'],
        editable: false,
        filterable: {
            extra: false,
            operators: {
                string: {
                    contains: 'Contains'
                },
                date: {
                    eq: 'Is equal to',
                    gt: 'Is after',
                    lt: 'Is before'
                },
                number: {
                    eq: 'Is equal to',
                    gt: 'Is greater than',
                    lt: 'Is less than'
                }
            }
        },
        columnHide: function () {
            $('.k-grid-content.k-auto-scrollable').getNiceScroll().resize();
        },
        columnShow: function () {
            $('.k-grid-content.k-auto-scrollable').getNiceScroll().resize();
        }
    };
    Object.assign(options, defaultOptions);

    $el.kendoGrid(options);

    $el.data("kendoGrid").autoFitColumn(0);
}

export function getGridColumns() {
    return [
        {
            title: 'Select All',
            selectable: true,
            width: "50px"
        },
        {field: 'event_name', title: 'Event name', width: '230px', template: `<a href="${crudEventsBaseUrl}/#= event_id #">#: event_name # </a>`},
        {field: 'category', title: 'Category', width: '150px'},
        {field: 'tier', title: 'Tier', width: '120px'},
        {
            field: 'status',
            title: 'Status',
            filterable: {
                extra: false,
                operators: {
                    string: {
                        eq: "Is equal to"
                    }
                },
                ui: statusFilter
            },
            width: '120px'
        },
        {
            field: 'currency',
            title: 'Currency',
            filterable: {
                extra: false,
                operators: {
                    string: {
                        eq: "Is equal to"
                    }
                },
                ui: currencyFilter
            },
            width: '110px'
        },
        {field: 'price', title: 'Price', width: '100px'},
        {field: 'discount_price', title: 'Discount Price', width: '150px'},
        {field: 'sales_end_date', format: '{0:dd/MM/yyyy}', title: 'Tier Ends', width: '125px'},
        {
            template: kendo.template($("#buttonMenuTemplate").html()),
            title: 'Actions',
            width: '80px'
        }
    ];
}

function statusFilter(element) {
    element.kendoDropDownList({
        dataSource: ticketStatuses
    });
}

function currencyFilter(element) {
    element.kendoDropDownList({
        dataTextField: 'code',
        dataValueField: 'id',
        dataSource: ticketCurrencies
    });
}

export function batchDeleteTicket(action, e) {
    e.preventDefault();
    let ids = getSelectedIds($grid);

    if (!ids.length) {
        alert('Please select an ticket');
        return;
    }
    confirmModal({
        action: action,
        title: getModalTitle(action, true),
        subTitle: getModalSubTitle(action, true),
        done: function (ids) {
            if (ids.length) {
                $.ajax({
                    url: '/admin/tickets/batch',
                    method: 'delete',
                    data: {
                        'ids': ids,
                    }
                })
                    .always(function (response) {
                        if (response.code === 200) {
                            let dataSource = $grid.data('kendoGrid').dataSource;
                            ids.forEach(function (id) {
                                let dataItem = dataSource.get(id);
                                dataSource.remove(dataItem);
                            });
                        } else {
                            alert('An error occurred');
                        }
                    });
            }

        }.bind(this, ids)
    });
}

export function deleteTicket(id, action, e) {
    e.preventDefault();
    confirmModal({
        action: action,
        title: getModalTitle(action),
        subTitle: getModalSubTitle(action),
        done: function (id) {
            $.ajax({
                url: '/admin/tickets/' + id,
                method: 'delete'
            })
                .always(function (response) {
                    if (response.code === 200) {
                        let dataSource = $grid.data('kendoGrid').dataSource;
                        let dataItem = dataSource.get(id);
                        dataSource.remove(dataItem);
                    } else {
                        alert('An error occurred');
                    }
                });

        }.bind(this, id)
    });
}

/**
 *
 * @param action
 * @param batch
 * @returns {string}
 */
function getModalTitle(action, batch = false) {
    let title = '';
    let strPart = batch ? 'these tickets' : 'this ticket';

    switch (action) {
        case 'delete':
            title = 'Are you sure you want to ' + action + ' ' + strPart + '?';
            break;
        default:
            title = 'Are you sure you want to confirm this action?';
            break;
    }

    return title;
}

/**
 *
 * @param action
 * @param batch
 * @returns {string}
 */
function getModalSubTitle(action, batch = false) {
    let subTitle = '';
    let strPart = batch ? 'these tickets' : 'this ticket';

    switch (action) {
        case 'delete':
            subTitle = batch
                ? 'Delete these will remove them completely from the system.'
                : 'Delete this will remove it completely from the system.';
            break;
        default:
            action = action.charAt(0).toUpperCase() + action.slice(1);
            subTitle = action + 'ing ' + strPart + ' will also affect the front end interface';
            break;
    }

    return subTitle;
}