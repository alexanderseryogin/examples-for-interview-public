import {
    $grid,
    $selectView,
    crudServiceBaseUrl,
    listViewsUrl,
    pageSize,
} from './constants';
import {restoreSavedOptions} from '../shared/kendo';

export function getGridDataSource(options) {
    const defaults = {
        transport: {
            read: {
                url: crudServiceBaseUrl,
                data: function () {
                    return {
                        q: $('#tickets-search').val()
                    };
                }
            },
            parameterMap: function (data, type) {
                if (data.filter) {
                    data.filter.filters.map(function (item) {
                        if (item.field === 'sales_end_date') {
                            item.value = kendo.toString(item.value, 'yyyy-MM-dd');
                        }
                    });
                }
                return $.extend({'page_size': data.pageSize}, data);
            }
        },
        serverSorting: true,
        serverPaging: true,
        serverFiltering: true,
        serverGrouping: true,
        pageSize: pageSize,
        batch: true
    };

    options = Object.assign({}, defaults, options);

    return new kendo.data.DataSource(options);
}

export function getListDataSource(options = {}) {
    const defaults = {
        transport: {
            read: {
                url: listViewsUrl,
                data: {
                    type: 'tickets'
                },
                dataType: 'json'
            }
        },
        schema: {
            data: function (response) {
                return response.payload || [];
            }
        },
        requestEnd: function (e) {
            if (e.response && e.response.code === 200) {
                e.response.payload.forEach(function (item) {
                    if (item.is_default === 1) {
                        $selectView.data('kendoComboBox').value(item.id);
                        restoreSavedOptions($grid, item.options);
                    }
                });
            }
        }
    };

    options = Object.assign({}, defaults, options);

    return new kendo.data.DataSource(options);
}