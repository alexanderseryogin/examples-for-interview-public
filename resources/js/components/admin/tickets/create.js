$(document).ready(function () {
    $('#event_id').kendoComboBox({
        filter: 'contains',
        placeholder: 'Type your event name',
        minLength: 1,
        suggest: false,
        dataTextField: 'name',
        dataValueField: 'id',
        dataSource: {
            transport: {
                read: '/admin/events/all'
            },
            schema: {
                data: function (response) {
                    return response.payload;
                }
            },
        },
        noDataTemplate: $("#no-data-template").html()
    });

    $('#sales_end_date').kendoDatePicker({
        format: 'd MMMM yyyy',
        parseFormats: ['yyyy-MM-dd']
    });

    $(".js-editor").kendoEditor({
        resizable: {
            content: true,
            toolbar: true
        }
    });

    $('#has_discount').change(function () {
        $('#discount_price').prop('disabled', !$(this).is(':checked'));
    });

    if ($('.drop-croppie-header').length) {
        $('.drop-croppie-header').dropCroppie({
            general: {
                result: '#header_image'
            },
            crop: {
                viewport: {
                    width: 640,
                    height: 360,
                },
                result: {
                    size: {
                        width: 1920,
                        height: 1080
                    }
                }
            }
        });
    }
});