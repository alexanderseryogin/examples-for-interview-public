export function getFields() {
    return {
        id: {type: 'number'},
        event_name: {type: 'string'},
        name: {type: 'string'},
        price: {type: 'number'},
        discount_price: {type: 'number'},
        tier: {type: 'string'},
        status: {type: 'string'},
        currency: {type: 'string'},
        sales_end_date: {type: 'date'}
    }
}