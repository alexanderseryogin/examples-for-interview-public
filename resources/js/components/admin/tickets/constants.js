export const crudServiceBaseUrl = '/admin/tickets';
export const listViewsUrl = '/admin/list-views';
export const pageSize = 10;
export const $grid = $('#grid');
export const $selectView = $('#select-view');