const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/app.scss', 'public/css')
   .options({
       processCssUrls: false
   })
    .sass('resources/assets/sass/app.scss', 'public/css/rwc2019.css')
   .js('resources/js/app.js', 'public/js')
   .combine([
       'node_modules/croppie/croppie.min.js',
       'resources/js/shared/dropzone.js',
       'resources/js/components/admin/shared/drop-croppie.js',
       'resources/js/admin.js',
   ], 'public/js/admin.js')
   .js('resources/js/components/admin/events/*.js', 'public/js/components/admin/events.js')
   .js('resources/js/components/admin/venues/**/*.js', 'public/js/components/admin/venues.js')
   .js('resources/js/components/admin/tickets/**/*.js', 'public/js/components/admin/tickets.js')
   .js('resources/js/components/admin/roles/**/*.js', 'public/js/components/admin/roles.js')
   .js('resources/js/components/admin/users/**/*.js', 'public/js/components/admin/users.js')
   .js('resources/js/components/admin/invites/**/*.js', 'public/js/components/admin/invites.js')
   .js('resources/js/login.js', 'public/js/login.js')
   .copy('resources/js/global.js', 'public/js/global.js')
   .copy('node_modules/croppie/croppie.css', 'public/css/croppie.css');