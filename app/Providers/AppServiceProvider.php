<?php

namespace App\Providers;

use App\Services\Package\IPackageService;
use App\Services\Package\PackageService;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Validator;
use Hash;

use Braintree_Configuration;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Braintree_Configuration::environment(config('services.braintree.environment'));
        Braintree_Configuration::merchantId(config('services.braintree.merchant_id'));
        Braintree_Configuration::publicKey(config('services.braintree.public_key'));
        Braintree_Configuration::privateKey(config('services.braintree.private_key'));

        if (getenv('APP_ENV') === 'production') {
            URL::forceScheme('https');
        }

        Validator::extend('match_password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, current($parameters));

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IPackageService::class, PackageService::class);
    }
}
