<?php

namespace App\Providers;

use Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('update-user', 'App\Admin\Policies\UserPolicy@update');
        Gate::define('update-event', 'App\Admin\Policies\EventPolicy@update');
        Gate::define('update-ticket', 'App\Admin\Policies\TicketPolicy@update');
        Gate::define('update-event-package', 'App\Admin\Policies\EventPackagePolicy@update');
        Gate::define('update-venue', 'App\Admin\Policies\VenuePolicy@update');
    }
}
