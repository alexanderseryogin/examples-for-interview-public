<?php


namespace App\Services\Sendgrid;

use Barryvdh\DomPDF\PDF;
use QrCode;

class TicketService
{

    public function generatePDF($fullBarcode)
    {
        $pdf = PDF::loadView('usg2019.pdf.usg2019-ticket-success', ['ticketId' => $fullBarcode]);
        return @$pdf->stream('usg2019.pdf.usg2019-ticket-success');
    }

    public function generateQRCode($fullBarcode, $size, $encode = true)
    {
        if (!$encode) {
            $qrcode = QrCode::format('png')
                ->size($size)
                ->generate($fullBarcode, public_path('/images/qrcode/' . $fullBarcode . '.png'));
        } else {
            $qrcode = QrCode::format('png')
                ->size($size)
                ->generate($fullBarcode);
            $qrcode = base64_encode($qrcode);
        }

        return $qrcode;
    }

    public function generateImageFromPDF($locationPDF) // not working
    {
        $pdf_file = escapeshellarg("mysafepdf.pdf");
        $jpg_file = escapeshellarg("output.jpg");
        $result = 0;
        exec("convert -density 300 {$pdf_file} {$jpg_file}", null, $result);
    }

}
