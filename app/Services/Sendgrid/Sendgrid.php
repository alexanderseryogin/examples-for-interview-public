<?php


namespace App\Services\Sendgrid;

use SendGrid\Mail\Mail;
use SendGrid\Mail\Attachment;
use Validator;

class Sendgrid
{
    protected $validator;
    protected $data;
    
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function sendgrid()
    {
        if ($this->validator($this->data)->fails()) {
            return json_encode($this->validator->errors());
        }
        return $this->send();
    }

    private function send()
    {
        $email = new Mail();
        $email->setFrom($this->data['fromEmail'], $this->data['fromName']);
        $email->setSubject($this->data['subject']);
        $email->addTo($this->data['toEmail'], $this->data['toName']);
        $email->setTemplateId(getenv('TEMPLATE_ID'));

        foreach ($this->data['params'] as $key => $value) {
            $email->addDynamicTemplateData($key , $value);
        }

        if (count($this->data['attachments']) > 0) {
            foreach ($this->data['attachments'] as $attach) {
                $attachment = new Attachment();
                $attachment->setContent($attach);
                $attachment->setType("application/pdf");
                $attachment->setFilename("Ultra-Ticket.pdf");
                $attachment->setDisposition("attachment");
                $attachment->setContentId("Balance Sheet");

                $email->addAttachment($attachment);
            }
        }

        if (!empty($this->data['cc'])) {
            $email->addCc($this->data['cc']['emailTo'], $this->data['cc']['toName']);
        }


        $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
        try {
            $response = $sendgrid->send($email);
            return response()->json($response->headers(), $response->statusCode());
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
    }

    private function validator()
    {
        $rules = [
            'fromEmail' => 'required|email',
            'fromName' => 'required|string|max:255',
            'toEmail' => 'required|email',
            'toName' => 'required|string|max:255',
            'subject' => 'required|string|max:255',
            'params' => 'required|array',
        ];

        return $this->validator = Validator::make($this->data, $rules);
    }


}