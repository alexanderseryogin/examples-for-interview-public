<?php


namespace App\Services\Transportation;


use App\Models\City;
use App\Models\Transportation;
use Illuminate\Support\Facades\DB;

class TransportationService
{
    public function getFullRoute(int $cityOrigin, int $cityDestination)
    {
        $cityTransit = $this->hasCityTransit($cityOrigin, $cityDestination);
        $fullRoute = [];
        // has transit
        if (strlen($cityTransit->city_transit) > 0) {
            $routeAfterTransit = DB::table('transportations')
                ->where('id', $cityTransit->city_transit)
                ->limit(1)
                ->get();
            $fullRoute['before'] = $this->getTransportationBeforeTransit($cityOrigin, $cityDestination);
            $fullRoute['after'] = $this->getTransportationAfterTransit($routeAfterTransit[0]->city_origin, $routeAfterTransit[0]->city_destination);
            return $fullRoute;
        } // no transit
        else {
            $fullRoute = $this->getTransportationBeforeTransit($cityOrigin, $cityDestination);
            return $fullRoute;
        }
    }

    public function hasCityTransit(int $cityOrigin, int $cityDestination)
    {
//        die('hasCityTransit');
        $result = DB::table('transportations')
            ->where('city_origin', $cityOrigin)
            ->where('city_destination', $cityDestination)
            ->first();
        return $result;
    }

    public function getCitiesBeforeTransit(int $cityOrigin, int $cityDestination)
    {
        $citiesBeforeTransit = City::whereIn('id', [$cityOrigin, $cityDestination])
            ->with('transportation')
            ->get()
            ->toArray();
        return $citiesBeforeTransit;
    }

    public function getCitiesAfterTransit(int $cityOriginAfterTransit, int $cityDestinationAfterTransit)
    {
        $citiesAfterTransit = City::whereIn('id', [$cityOriginAfterTransit, $cityDestinationAfterTransit])
            ->get()
            ->toArray();

        $citiesAfterTransit = City::whereIn('id', [$cityOriginAfterTransit, $cityDestinationAfterTransit])
            ->with('transportation')
            ->get()
            ->toArray();
//        dd($citiesAfterTransit);
        return $citiesAfterTransit;
    }

    public function getTransportationBeforeTransit(int $cityOrigin, int $cityDestination)
    {
        $transportationBeforeTransit = Transportation::where('transportations.city_origin', $cityOrigin)
            ->where('transportations.city_destination', $cityDestination)
            ->limit(1)
            ->get()
            ->toArray();

        $citiesBeforeTransit[0] = City::where('id', $transportationBeforeTransit[0]['city_origin'])
            ->limit(1)
            ->get()
            ->toArray();
        $citiesBeforeTransit[1] = City::where('id', $transportationBeforeTransit[0]['city_destination'])
            ->limit(1)
            ->get()
            ->toArray();
        $transit = ['transportations' => $transportationBeforeTransit, 'cities' => $citiesBeforeTransit];

        return $transit;
    }

    public function getTransportationAfterTransit(int $cityOriginAfterTransit, int $cityDestinationAfterTransit)
    {
        $transportationAfterTransit = Transportation::where('transportations.city_origin', $cityOriginAfterTransit)
            ->where('transportations.city_destination', $cityDestinationAfterTransit)
            ->limit(1)
            ->get()
            ->toArray();

        $citiesAfterTransit[0] = City::where('id', $transportationAfterTransit[0]['city_origin'])
            ->get()
            ->toArray();
        $citiesAfterTransit[1] = City::where('id', $transportationAfterTransit[0]['city_destination'])
            ->get()
            ->toArray();
        $transit = ['transportations' => $transportationAfterTransit, 'cities' => $citiesAfterTransit];

        return $transit;
    }
}