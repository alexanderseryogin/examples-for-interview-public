<?php

namespace App\Admin\Contracts;

/**
 * Trait FilterableList
 *
 * @package App\Admin\Contracts
 */
interface FilterableList
{
    const OPERATOR_GREATER = 'gt';

    const OPERATOR_LESS = 'lt';

    const OPERATOR_EQUAL = 'eq';

    const OPERATOR_CONTAINS = 'contains';

    CONST FIELD_DATE = 'date';

    CONST FIELD_TIME = 'time';

    CONST FIELD_NUMBER = 'number';
}
