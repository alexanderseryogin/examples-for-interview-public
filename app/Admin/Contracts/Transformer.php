<?php

namespace App\Admin\Contracts;

/**
 * Interface Transformer
 *
 * @package App\Admin\Contracts
 */
interface Transformer
{
    /**
     * @param $data
     *
     * @return mixed
     */
    public function transform($data);
}
