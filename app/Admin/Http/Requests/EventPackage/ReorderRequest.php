<?php

namespace App\Admin\Http\Requests\EventPackage;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ReorderRequest
 *
 * @package App\Admin\Http\Requests\EventPackage
 */
class ReorderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'packages'       => 'required|array',
            'packages.*.id'  => 'required|integer|exists:event_packages',
            'packages.*.pos' => 'required|integer'
        ];
    }
}