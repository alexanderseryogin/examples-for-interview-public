<?php

namespace App\Admin\Http\Requests\Invite;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest
 *
 * @package App\Admin\Http\Requests\Штмшеу
 */
class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'emails'     => 'required|array|min:1|max:10',
            'emails.*'   => 'required|email|unique:users,email',
            'role'       => 'required|integer|exists:roles,id',
            'company_id' => 'required|integer|exists:companies,id',
            'events'     => 'nullable|array',
            'events.*'   => 'nullable|integer|exists:events,id',
            'team_id'    => 'nullable|integer|exists:teams,id',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'emails.*.email'  => 'The ":input" must be a valid email address.',
            'emails.*.unique' => 'The user with ":input" has already registered.',
        ];
    }
}
