<?php

namespace App\Admin\Http\Requests\Image;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreTempRequest
 *
 * @package App\Admin\Http\Requests\Image
 */
class StoreTempRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|image',
        ];
    }
}