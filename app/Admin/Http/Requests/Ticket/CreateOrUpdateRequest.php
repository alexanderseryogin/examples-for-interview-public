<?php

namespace App\Admin\Http\Requests\Ticket;

use App\Models\Ticket;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class StoreRequest
 *
 * @package App\Admin\Http\Requests\Ticket
 */
class CreateOrUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'event_id'              => 'required|integer|exists:events,id',
            'name'                  => 'required|string',
            'checkout_url'          => 'required|url',
            'category'              => 'required|string',
            'tier'                  => 'nullable|string',
            'status'                => ['required', 'string', Rule::in(Ticket::getStatuses())],
            'has_discount'          => 'nullable|boolean',
            'discount_price'        => 'required_with:has_discount|numeric',
            'currency_id'           => 'required|integer|exists:currencies,id',
            'sales_end_date'        => 'nullable|date_format:j F Y',
            'sales_end_date_hour'   => 'nullable|integer',
            'sales_end_date_minute' => 'nullable|integer',
            'show_countdown'        => 'nullable|boolean',
            'price'                 => 'required|numeric',
            'header_image'          => 'nullable|string',
            'description'           => 'nullable|string',
            'details'               => 'nullable|string',
        ];
    }
}
