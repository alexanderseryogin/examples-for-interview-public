<?php

namespace App\Admin\Http\Requests\Ticket;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ReorderRequest
 *
 * @package App\Admin\Http\Requests\Ticket
 */
class ReorderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tickets'       => 'required|array',
            'tickets.*.id'  => 'required|integer|exists:tickets',
            'tickets.*.pos' => 'required|integer'
        ];
    }
}