<?php

namespace App\Admin\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreEventListViewRequest
 *
 * @package App\Admin\Http\Requests\EventListView
 */
class StoreRolesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'roles'     => 'required|array',
            'roles.*'   => 'array',
            'roles.*.*' => 'exists:permissions,id',
        ];
    }
}