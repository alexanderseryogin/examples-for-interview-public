<?php

namespace App\Admin\Http\Requests;

use App\Admin\Traits\Lists\Filterable;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DefaultIndexRequest
 *
 * @package App\Admin\Http\Requests
 */
class DefaultIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'q'                         => 'nullable|string',
            'page_size'                 => 'nullable|integer',
            'page'                      => 'nullable|integer',
            'sort'                      => 'nullable|array',
            'sort.*.field'              => 'string',
            'sort.*.dir'                => 'string|in:asc,desc',
            'filter'                    => 'nullable|array',
            'filter.filters'            => 'nullable|array',
            'filter.filters.*.field'    => 'string',
            'filter.filters.*.operator' => 'string|in:' . implode(',', Filterable::getOperators()),
            'filter.filters.*.value'    => 'string',
            'group'                     => 'nullable|array',
            'group.*.field'             => 'string',
            'group.*.dir'               => 'string|in:asc,desc',
        ];
    }
}