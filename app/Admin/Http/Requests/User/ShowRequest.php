<?php

namespace App\Admin\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class IndexRequest
 *
 * @package App\Admin\Http\Requests\User
 */
class ShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'creator_id'   => 'nullable|integer',
            'company_id'   => 'nullable|integer',
            'page_size'    => 'nullable|integer',
            'page'         => 'nullable|integer',
            'sort'         => 'nullable|array',
            'sort.*.field' => 'string',
            'sort.*.dir'   => 'string|in:asc,desc',
        ];
    }
}