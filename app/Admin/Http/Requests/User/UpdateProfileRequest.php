<?php

namespace App\Admin\Http\Requests\User;

use App\Helpers\AuthHelper;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CreateRequest
 *
 * @package App\Admin\Http\Requests\Event
 */
class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status'       => ['nullable', 'string', Rule::in(User::getStatuses())],
            'avatar'       => 'nullable|string',
            'first_name'   => 'required|string|max:255',
            'last_name'    => 'required|string|max:255',
            'email'        => 'required|email|unique:users,email,' . AuthHelper::id(),
            'password_old' => 'nullable|match_password:' . AuthHelper::user()->password,
            'password'     => [
                'nullable',
                'required_unless:password_old,',
                'bail',
                'min:6',
                'confirmed',
                'different:password_old'
            ],
        ];
    }

    public function messages()
    {
        return [
            'match_password'  => 'Password not matched',
            'required_unless' => 'The :attribute field is required'
        ];
    }
}
