<?php

namespace App\Admin\Http\Requests\User;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CreateRequest
 *
 * @package App\Admin\Http\Requests\Event
 */
class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status'     => ['required', 'string', Rule::in(User::getStatuses())],
            'avatar'     => 'nullable|string',
            'first_name' => 'required|string|max:255',
            'last_name'  => 'required|string|max:255',
            'email'      => 'required|email|unique:users,email,'. $this->route('user')->id,
            'company_id' => 'required|integer|exists:companies,id',
            'team_id'    => 'nullable|integer|exists:teams,id',
            'role'       => 'required|integer|exists:roles,id',
        ];
    }
}
