<?php

namespace App\Admin\Http\Requests\Venue;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class EventSearchRequest
 *
 * @package App\Admin\Http\Requests\Event
 */
class BatchDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'required|integer',
        ];
    }
}