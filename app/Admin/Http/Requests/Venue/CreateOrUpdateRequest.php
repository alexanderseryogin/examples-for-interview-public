<?php

namespace App\Admin\Http\Requests\Venue;

use App\Rules\GoogleIframe;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateOrUpdateRequest
 *
 * @package App\Admin\Http\Requests\Venue
 */
class CreateOrUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                      => 'required|string',
            'address1'                  => 'nullable|string',
            'address2'                  => 'nullable|string',
            'postcode'                  => 'nullable|string',
            'state'                     => 'nullable|string',
            'city'                      => 'nullable|string',
            'country_id'                => 'nullable|integer',
            'map_link'                  => ['nullable', 'string', new GoogleIframe],
            'capacity'                  => 'nullable|integer',
            'seating_layout_image_file' => 'nullable|file',
            'seating_layout_image'      => 'nullable',
        ];
    }
}
