<?php

namespace App\Admin\Http\Requests\Event;

use App\Models\EventPackage;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class StorePackageRequest
 *
 * @package App\Admin\Http\Requests\Event
 */
class StorePackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'                  => 'nullable|integer|exists:event_packages,id',
            'pos'                 => 'present|integer',
            'is_visible'          => 'present|boolean',
            'use_same_more_info'  => 'present|boolean',
            'name'                => 'required|string',
            'card_size'           => ['required', Rule::in(EventPackage::getCardSizes())],
            'card_image'          => 'nullable|string',
            'currency_id'         => 'required|integer|exists:currencies,id',
            'price'               => 'required|numeric',
            'call_to_action_link' => 'nullable|string',
            'status'              => ['required', Rule::in(EventPackage::getStatuses())],
            'header_image'        => 'nullable|string',
            'description'         => 'nullable|string',
            'details'             => 'nullable|string',
            'blocks'              => 'nullable|array',
        ];
    }
}