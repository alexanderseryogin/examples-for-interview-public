<?php

namespace App\Admin\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateRequest
 *
 * @package App\Admin\Http\Requests\Event
 */
class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|string',
            'url'                   => 'required|string|unique:events,slug',
            'start_date'            => 'nullable|date_format:j F Y',
            'start_date_hour'       => 'nullable|integer',
            'start_date_minute'     => 'nullable|integer',
            'end_date'              => 'nullable|date_format:j F Y',
            'end_date_hour'         => 'nullable|string',
            'end_date_minute'       => 'nullable|string',
            'is_estimated_end_time' => 'nullable|boolean',
            'venue_id'              => 'nullable|integer',
            'organizer_id'          => 'nullable|integer',
            'has_redtix_guarantee'  => 'nullable|integer',
            'genres'                => 'array|min:1',
            'genres.*'              => 'nullable|integer',
            'category_id'           => 'nullable|integer',
            'description'           => 'nullable|string',
            'show_countdown'        => 'nullable|boolean',
            'add_custom_datetime'   => 'nullable|boolean',
            'custom_date'           => 'nullable|date_format:j F Y',
            'custom_date_hour'      => 'nullable|integer',
            'custom_date_minute'    => 'nullable|integer',
            'banner_desktop'        => 'nullable|string',
            'banner_mobile'         => 'nullable|string',
        ];
    }
}
