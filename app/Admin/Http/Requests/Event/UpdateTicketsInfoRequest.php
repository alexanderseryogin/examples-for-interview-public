<?php

namespace App\Admin\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateTicketsInfoRequest
 *
 * @package App\Admin\Http\Requests\Event
 */
class UpdateTicketsInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'max_purchase_tickets_quantity' => 'required|integer'
        ];
    }
}
