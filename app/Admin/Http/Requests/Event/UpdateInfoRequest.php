<?php

namespace App\Admin\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateInfoRequest
 *
 * @package App\Admin\Http\Requests\Event
 */
class UpdateInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'info_tab_name'           => 'nullable|string',
            'is_info_tab_visible'     => 'nullable|boolean',
            'info_video'              => 'nullable|string',
            'infoBlocks'              => 'array',
            'infoBlocks.*.id'         => 'required|integer',
            'infoBlocks.*.pos'        => 'required|integer',
            'infoBlocks.*.is_visible' => 'nullable|boolean',
            'gallery'                 => 'nullable|array',
            'gallery.*'               => 'nullable|string',
            'artist_line_up'          => 'nullable|array',
            'artist_line_up.*'        => 'nullable|string',
            'quotes'                  => 'nullable|array',
            'quotes.*'                => 'array',
        ];
    }
}
