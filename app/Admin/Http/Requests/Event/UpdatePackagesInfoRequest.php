<?php

namespace App\Admin\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdatePackagesInfoRequest
 *
 * @package App\Admin\Http\Requests\Event
 */
class UpdatePackagesInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'packages_tab_name'       => 'nullable|string',
            'is_packages_tab_visible' => 'nullable|boolean'
        ];
    }
}
