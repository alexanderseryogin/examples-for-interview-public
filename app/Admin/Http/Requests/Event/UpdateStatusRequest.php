<?php

namespace App\Admin\Http\Requests\Event;

use App\Models\Event;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdateStatusRequest
 *
 * @package App\Admin\Http\Requests\Event
 */
class UpdateStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ids'    => 'required|array',
            'ids.*'  => 'required|integer',
            'status' => ['required', Rule::in(Event::getStatuses())],
        ];
    }
}