<?php

namespace App\Admin\Http\Requests\Event;

use App\Models\Ticket;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class StoreTicketRequest
 *
 * @package App\Admin\Http\Requests\Event
 */
class StoreTicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'                    => 'nullable|integer|exists:tickets,id',
            'name'                  => 'required|string',
            'checkout_url'          => 'nullable|url',
            'category'              => 'required|string',
            'tier'                  => 'nullable|string',
            'pos'                   => 'present|integer',
            'status'                => ['required', 'string', Rule::in(Ticket::getStatuses())],
            'currency_id'           => 'required|integer|exists:currencies,id',
            'sales_end_date'        => 'nullable|date_format:j F Y',
            'sales_end_date_hour'   => 'nullable|integer',
            'sales_end_date_minute' => 'nullable|integer',
            'price'                 => 'required|numeric',
            'has_discount'          => 'nullable|boolean',
            'discount_price'        => 'nullable|numeric',
            'show_countdown'        => 'nullable|boolean',
            'header_image'          => 'nullable|string',
            'description'           => 'nullable|string',
            'details'               => 'nullable|string',
        ];
    }
}