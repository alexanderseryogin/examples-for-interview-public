<?php

namespace App\Admin\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateFaqsRequest
 *
 * @package App\Admin\Http\Requests\Event
 */
class UpdateFaqsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'faqs'               => 'nullable|array',
            'faqs.*.id'          => 'nullable|integer',
            'faqs.*.title'       => 'required|string',
            'faqs.*.body'        => 'nullable|string',
            'faqs.*.pos'         => 'nullable|integer',
            'faqs.*.is_template' => 'nullable|integer',
            'faqs.*.is_visible'  => 'nullable|integer',
        ];
    }
}
