<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\ListView\IndexRequest;
use App\Admin\Http\Requests\ListView\StoreRequest;
use App\Admin\Repositories\ListViewRepository;
use App\Admin\Services\ListViewService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

/**
 * Class ListViewController
 *
 * @package App\Admin\Http\Controllers
 */
class ListViewController extends Controller
{
    /**
     * @var ListViewService
     */
    private $listViewService;
    /**
     * @var ListViewRepository
     */
    private $listViewRepository;

    /**
     * ListViewController constructor.
     *
     * @param ListViewRepository $listViewRepository
     * @param ListViewService    $listViewService
     */
    public function __construct(ListViewRepository $listViewRepository, ListViewService $listViewService)
    {
        $this->listViewRepository = $listViewRepository;
        $this->listViewService = $listViewService;
    }

    /**
     * @param IndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(IndexRequest $request): JsonResponse
    {
        $views = $this->listViewRepository->getForCurrentUser($request->input('type'));

        return response()->success($views);
    }

    /**
     * @param StoreRequest $request
     *
     * @return JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        $view = $this->listViewService->storeForCurrentUser($request->validated());

        return response()->success($view);
    }
}
