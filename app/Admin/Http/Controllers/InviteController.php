<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\DefaultIndexRequest;
use App\Admin\Http\Requests\Invite\StoreRequest;
use App\Admin\Repositories\EventRepository;
use App\Admin\Repositories\InviteRepository;
use App\Admin\Repositories\RoleRepository;
use App\Admin\Services\InviteService;
use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\View\View;

/**
 * Class InviteController
 *
 * @package App\Admin\Http\Controllers
 */
class InviteController extends Controller
{
    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var RoleRepository
     */
    private $roleRepository;
    /**
     * @var InviteRepository
     */
    private $inviteRepository;
    /**
     * @var InviteService
     */
    private $inviteService;

    /**
     * InviteController constructor.
     *
     * @param RoleRepository   $roleRepository
     * @param InviteService    $inviteService
     * @param InviteRepository $inviteRepository
     * @param EventRepository  $eventRepository
     */
    public function __construct(
        RoleRepository $roleRepository,
        InviteService $inviteService,
        InviteRepository $inviteRepository,
        EventRepository $eventRepository
    ) {
        $this->roleRepository = $roleRepository;
        $this->inviteService = $inviteService;
        $this->inviteRepository = $inviteRepository;
        $this->eventRepository = $eventRepository;
    }

    /**
     * @param DefaultIndexRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|View
     */
    public function index(DefaultIndexRequest $request)
    {
        if ($request->expectsJson()) {
            $invites = $this->inviteRepository->kendoList($request->validated());

            return response()->success($invites);
        }

        return view('admin.pages.invites.index', [
            'q' => $request->input('q')
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return view('admin.pages.invites.create', [
            'invite' => $this->inviteRepository->newInstance(),
            'roles'  => $this->roleRepository->allSimple(),
            'events' => $this->eventRepository->allSimple(),
            'types'  => Company::getTypesList()
        ]);
    }

    /**
     * @param StoreRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $invites = $this->inviteService->store($request->validated());

        return redirect()
            ->route('admin.invites.index')
            ->with('success', 'Invitations sent')
            ->with('emails', $invites->implode('email', ', '));
    }
}
