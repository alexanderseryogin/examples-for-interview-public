<?php

namespace App\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use File;
use Carbon;
use App\Models\MainBanner;
use App\Models\MainAnnouncement;
use App\Models\Country;
use App\Models\Venue;
use App\Models\EventCollection;

/**
 * Class DashboardController
 *
 * @package App\Admin\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return redirect()->route('admin.events.index');
    }

    public function settings()
    {
        $banners = MainBanner::orderBy('num_placed', 'asc')->get();
        $banners_num_placed = $banners->pluck('num_placed')->toArray();
        $announcement = MainAnnouncement::first();
        $countries = Country::get();
        $venues = Venue::get();
        $eventcollections = EventCollection::get();

        $data = [
            'banners'            => $banners,
            'banners_num_placed' => $banners_num_placed,
            'announcement'       => $announcement,
            'countries'          => $countries,
            'venues'             => $venues,
            'eventcollections'   => $eventcollections
        ];

        return view('pages.settings.index', $data);
    }

    public function eventCollectionStore(Request $request)
    {
        $event_collection = new EventCollection;
        $event_collection->name = $request->name;

        $event_collection->save();

        return Redirect::to(URL::previous())->with('successMessage', 'New event collection section created');
    }

    public function eventCollectionDelete(Request $request, $event_collection_id)
    {
        $event_collection = EventCollection::findOrFail($event_collection_id);
        if (is_null($event_collection)) {
            abort(404, 'Not Found');
        }
        $event_collection->delete();

        return Redirect::to(URL::previous())->with('successMessage', 'Section deleted');
    }

    public function mainBannerStore(Request $request)
    {
        $this->validate($request, [
            'title'   => 'required',
            'website' => 'image|required|dimensions:max-width=2000px,max-height=530px',
            'mobile'  => 'image|required|dimensions:min-width=370px,min-height=230px'
        ]);

        $path = public_path() . '/images/homepage';
        $banners = MainBanner::get();

        if ($banners->count() <= 6) {

            if (!File::exists($path)) {
                File::makeDirectory($path);
            }

            $mainbanner = new MainBanner;
            $mainbanner->title = $request->title;
            $mainbanner->links = $request->links ? $request->links : null;

            if ($banners->count() != 0) {
                $num_placed_banners = MainBanner::pluck('num_placed')->toArray();
                if (in_array($request->number_placed, $num_placed_banners)) {
                    foreach ($num_placed_banners as $num_placed) {
                        if ($num_placed == $request->number_placed) {
                            $other_banner = MainBanner::where('num_placed', $request->number_placed)->first();
                            $other_banner->num_placed = $banner->num_placed;
                            $other_banner->save();
                            $banner->num_placed = $request->number_placed;
                        }
                    }
                } else {

                    $num_not_placed = [];
                    for ($i = 1; $i <= 6; $i++) {
                        if (!in_array($i, $num_placed_banners)) {
                            $num_not_placed[] = $i;
                        }
                    }

                    if (is_null($request->number_placed)) {
                        $mainbanner->num_placed = $request->number_placed ? $request->number_placed : array_first($num_not_placed);
                    }

                    $mainbanner->num_placed = $request->number_placed;
                }
            } else {
                $mainbanner->num_placed = 1;
            }

            if ($request->file('mobile')) {
                $mobile_banner = $request->file('mobile');
                $mobile_filename = str_random(4) . $mobile_banner->getClientOriginalName();
                $mobile_banner->move($path, $mobile_filename);
                $mainbanner->mobile_path = '/images/homepage' . '/' . $mobile_filename;
            }

            if ($request->file('website')) {
                $website_banner = $request->file('website');
                $web_filename = str_random(4) . $website_banner->getClientOriginalName();
                $website_banner->move($path, $web_filename);
                $mainbanner->web_path = '/images/homepage' . '/' . $web_filename;
            }

            $mainbanner->save();
        } else {
            return Redirect::to(URL::previous())->with('warningMessage', 'Banners limit exceeded (only 6 allowed).');
        }

        return Redirect::to(URL::previous())->with('successMessage', 'Banner settings updated');
    }

    public function mainBannerEdit($banner_id, Request $request)
    {

        $path = public_path() . '/images/homepage';
        $banners = MainBanner::get();
        $banner = MainBanner::findOrFail($banner_id);
        $banner->title = $request->title ? $request->title : $banner->title;
        $banner->links = $request->links ? $request->links : null;
        $num_placed_banners = MainBanner::pluck('num_placed')->toArray();

        if (in_array($request->number_placed, $num_placed_banners)) {
            foreach ($num_placed_banners as $num_placed) {
                if ($num_placed == $request->number_placed) {
                    $other_banner = MainBanner::where('num_placed', $request->number_placed)->first();
                    $other_banner->num_placed = $banner->num_placed;
                    $other_banner->save();
                    $banner->num_placed = $request->number_placed;
                }
            }
        } else {
            $num_not_placed = [];
            for ($i = 1; $i <= 6; $i++) {
                if (!in_array($i, $num_placed_banners)) {
                    $num_not_placed[] = $i;
                }
            }

            if (is_null($request->number_placed)) {
                $mainbanner->num_placed = $request->number_placed ? $request->number_placed : array_first($num_not_placed);
            }

            $mainbanner->num_placed = $request->number_placed;
        }

        if ($request->file('website') || $request->file('mobile')) {
            $this->validate($request, [
                'website' => 'nullable|image|dimensions:width=2000px,height=530px',
                'mobile'  => 'nullable|image|dimensions:width=370px,height=230px'
            ]);
        }

        if (!is_null($request->file('website'))) {
            if ($banner->web_path) {
                File::delete(public_path() . $banner->web_path);
                $banner->web_path = null;
                $banner->save();
            }

            $website_banner = $request->file('website');
            $web_filename = str_random(4) . $website_banner->getClientOriginalName();
            $website_banner->move($path, $web_filename);
            $banner->web_path = '/images/homepage' . '/' . $web_filename;
        }

        if (!is_null($request->file('mobile'))) {
            if ($banner->mobile_path) {
                File::delete(public_path() . $banner->mobile_path);
                $banner->mobile_path = null;
                $banner->save();
            }

            $mobile_banner = $request->file('mobile');
            $mobile_filename = str_random(4) . $mobile_banner->getClientOriginalName();
            $mobile_banner->move($path, $mobile_filename);
            $banner->mobile_path = '/images/homepage' . '/' . $mobile_filename;
        }

        $banner->save();

        return Redirect::to(URL::previous())->with('successMessage', 'Banner updated');
    }

    public function mainBannerUpdate(Request $request)
    {
        $banners = MainBanner::get();
        $num_placed_banners = MainBanner::pluck('num_placed')->toArray();

        foreach ($request->number_placed as $key => $number_placed) {
            $banner = MainBanner::findOrFail($key);
            $banner->num_placed = $number_placed;
            $banner->save();
        }

        return Redirect::to(URL::previous())->with('successMessage', 'Banner Updated');
    }

    public function mainBannerDelete($banner_id)
    {
        $banner = MainBanner::findOrFail($banner_id);
        if (is_null($banner)) {
            abort(404, 'Not Found');
        }
        File::delete(public_path() . $banner->web_path);
        File::delete(public_path() . $banner->mobile_path);
        $banner->delete();

        return Redirect::to(URL::previous())->with('successMessage', 'Banner deleted');
    }

    public function mainAnnouncementStore(Request $request)
    {
        $this->validate($request, [
            'img_link' => 'image'
        ]);

        if (MainAnnouncement::count() < 1) {
            $announcement = new MainAnnouncement;
        } else {
            $announcement = MainAnnouncement::first();
        }

        $announcement->content = $request->content ? $request->content : null;
        $announcement->btn_link = $request->btn_link ? $request->btn_link : null;
        $announcement->is_published = $request->is_published ? 1 : 0;

        if ($request->is_published == 1) {
            $announcement->published_at = Carbon\Carbon::now();
        } else {
            $announcement->published_at = null;
        }

        if ($request->file('img_link')) {
            $path = public_path() . '/images/announcement';

            if (!File::exists($path)) {
                File::makeDirectory($path);
            }

            if ($request->file('img_link')) {
                if ($announcement->img_link) {
                    File::delete(public_path() . $announcement->img_link);
                    $announcement->img_link = null;
                    $announcement->save();
                }
                $announcement_img = $request->file('img_link');
                $announcement_img_filename = str_random(4) . $announcement_img->getClientOriginalName();
                $announcement_img->move($path, $announcement_img_filename);
                $announcement->img_link = '/images/announcement' . '/' . $announcement_img_filename;
            }
        }
        $announcement->save();

        return Redirect::to(URL::previous())->with('successMessage', 'Announcement settings stored!');
    }

    public function storeVenue(Request $request)
    {
        $this->validate($request, [
            'name'       => 'required',
            'address1'   => 'required',
            'address2'   => 'required',
            'city'       => 'required',
            'state'      => 'required',
            'postcode'   => 'required',
            'country_id' => 'required',
        ]);

        if (Venue::where('name', $request->name)->count()) {
            return redirect()->back()->with('warningMessage', 'Venue name is already in th database');
        }
        $venue = new Venue;
        $venue->name = $request->name ? $request->name : null;
        $venue->address1 = $request->address1 ? $request->address1 : null;
        $venue->address2 = $request->address2 ? $request->address2 : null;
        $venue->city = $request->city ? $request->city : null;
        $venue->state = $request->state ? $request->state : null;
        $venue->postcode = $request->postcode ? $request->postcode : null;
        $venue->country_id = $request->country_id ? $request->country_id : null;
        $venue->save();

        return redirect()->back();
    }

    public function editVenue(Request $request, $venue_id)
    {
        $this->validate($request, [
            'name'       => 'required',
            'address1'   => 'required',
            'address2'   => 'required',
            'city'       => 'required',
            'state'      => 'required',
            'postcode'   => 'required',
            'country_id' => 'required',
        ]);

        $venue = Venue::findOrFail($venue_id);

        $venue->name = $request->name ? $request->name : null;
        $venue->address1 = $request->address1 ? $request->address1 : null;
        $venue->address2 = $request->address2 ? $request->address2 : null;
        $venue->city = $request->city ? $request->city : null;
        $venue->state = $request->state ? $request->state : null;
        $venue->postcode = $request->postcode ? $request->postcode : null;
        $venue->country_id = $request->country_id ? $request->country_id : null;
        $venue->save();

        return redirect()->back();
    }
}
