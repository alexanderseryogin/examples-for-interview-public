<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\Genre\StoreRequest;
use App\Admin\Repositories\GenreRepository;
use App\Models\Genre;
use App\Http\Controllers\Controller;

class GenreController extends Controller
{
    /**
     * @var GenreRepository
     */
    private $genreRepository;

    /**
     * GenreController constructor.
     *
     * @param GenreRepository $genreRepository
     */
    public function __construct(GenreRepository $genreRepository)
    {
        $this->genreRepository = $genreRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Genre[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return response()->success(
            $this->genreRepository->all('name')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $genre = $this->genreRepository->create($request->validated());

        return response()->success($genre, 'Genre Created');
    }
}
