<?php

namespace App\Admin\Http\Controllers;

use App\Helpers\AuthHelper;
use App\Http\Controllers\Controller;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function index()
    {
        if (AuthHelper::hasCmsAccess()) {
            return redirect()->route('admin.events.index');
        }

        return redirect()->route('admin.pages.homepage');
    }

    public function signout()
    {
        Auth::guard('web')->logout();
        return redirect('/');
    }
}
