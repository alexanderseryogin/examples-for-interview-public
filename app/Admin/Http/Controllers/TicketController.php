<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\DefaultIndexRequest;
use App\Admin\Http\Requests\Ticket\BatchDeleteRequest;
use App\Admin\Http\Requests\Ticket\ReorderRequest;
use App\Admin\Http\Requests\Ticket\CreateOrUpdateRequest;
use App\Admin\Repositories\CurrencyRepository;
use App\Admin\Repositories\EventRepository;
use App\Admin\Repositories\TicketRepository;
use App\Admin\Services\TicketService;
use App\Http\Controllers\Controller;
use App\Models\Ticket;
use Illuminate\Http\JsonResponse;

/**
 * Class TicketController
 *
 * @package App\Admin\Http\Controllers
 */
class TicketController extends Controller
{
    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;
    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var TicketRepository
     */
    private $ticketRepository;
    /**
     * @var TicketService
     */
    private $ticketService;

    /**
     * TicketController constructor.
     *
     * @param TicketRepository   $ticketRepository
     * @param TicketService      $ticketService
     * @param EventRepository    $eventRepository
     * @param CurrencyRepository $currencyRepository
     */
    public function __construct(
        TicketRepository $ticketRepository,
        TicketService $ticketService,
        EventRepository $eventRepository,
        CurrencyRepository $currencyRepository
    ) {
        $this->ticketService = $ticketService;
        $this->ticketRepository = $ticketRepository;
        $this->eventRepository = $eventRepository;
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @param DefaultIndexRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(DefaultIndexRequest $request)
    {
        if ($request->expectsJson()) {
            $venues = $this->ticketRepository->kendoList($request->validated());

            return response()->success($venues);
        }

        return view('admin.pages.tickets.index', [
            'q'          => $request->input('q'),
            'statuses'   => Ticket::getStatuses(),
            'currencies' => $this->currencyRepository->all('code'),
        ]);
    }

    /**
     * @param ReorderRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(ReorderRequest $request)
    {
        $this->ticketService->reorder($request->input('tickets'));

        return response()->success();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.pages.tickets.create', [
            'ticket'     => $this->ticketRepository->newInstance(),
            'events'     => $this->eventRepository->allSimple(),
            'currencies' => $this->currencyRepository->allSimple('code'),
            'statuses'   => Ticket::getStatusesList(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateOrUpdateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOrUpdateRequest $request)
    {
        $ticket = $this->ticketService->create($request->validated());

        if (!$ticket) {
            return redirect()
                ->route('admin.tickets.create')
                ->with('error', 'Ticket create error');
        }

        return redirect()
            ->route('admin.tickets.edit', ['id' => $ticket->id])
            ->with('success', 'Ticket has been saved');
    }

    /**
     * @param Ticket $ticket
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Ticket $ticket)
    {
        return view('admin.pages.tickets.edit', [
            'ticket'     => $ticket,
            'events'     => $this->eventRepository->allSimple(),
            'currencies' => $this->currencyRepository->allSimple('code'),
            'statuses'   => Ticket::getStatusesList(),
        ]);
    }

    /**
     * Update resource in storage.
     *
     * @param Ticket                $ticket
     * @param CreateOrUpdateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Ticket $ticket, CreateOrUpdateRequest $request)
    {
        $ticket = $this->ticketService->update($ticket, $request->validated());

        if (!$ticket) {
            return redirect()
                ->route('admin.tickets.edit', ['id' => $ticket->id])
                ->with('error', 'Ticket update error');
        }

        return redirect()
            ->route('admin.tickets.edit', ['id' => $ticket->id])
            ->with('success', 'Ticket has been updated');
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(int $id): JsonResponse
    {
        $deleted = $this->ticketService->destroy($id);

        return $deleted
            ? response()->success()
            : response()->error();
    }

    /**
     * @param BatchDeleteRequest $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function batchDestroy(BatchDeleteRequest $request): JsonResponse
    {
        $deleted = $this->ticketService->batchDestroy($request->input('ids'));

        return $deleted
            ? response()->success()
            : response()->error();
    }
}
