<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\Role\CreateOrUpdateRoleRequest;
use App\Admin\Http\Requests\Role\StoreRolesRequest;
use App\Admin\Repositories\PermissionRepository;
use App\Admin\Repositories\RoleRepository;
use App\Admin\Services\RoleService;
use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class RoleController extends Controller
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;
    /**
     * @var PermissionRepository
     */
    private $permissionRepository;
    /**
     * @var RoleService
     */
    private $roleService;

    /**
     * RoleController constructor.
     *
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     * @param RoleService          $roleService
     */
    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository, RoleService $roleService)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
        $this->roleService = $roleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Role[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return view('admin.pages.roles.index', [
            'roles'       => $this->roleRepository->with(['permissions', 'users'])->get(),
            'permissions' => $this->permissionRepository->with(['roles'])->get()
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.pages.roles.create', [
            'role'        => $this->roleRepository->newInstance(),
            'permissions' => $this->permissionRepository->all()
        ]);
    }

    /**
     * @param CreateOrUpdateRoleRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateOrUpdateRoleRequest $request)
    {
        $role = $this->roleService->store($request->validated());

        return redirect()
            ->route('admin.roles.index')
            ->with('success', 'Role have been created');
    }

    /**
     * @param Role $role
     *
     * @return View
     */
    public function edit(Role $role): View
    {
        return view('admin.pages.roles.edit', [
            'role'        => $role->load('permissions'),
            'permissions' => $this->permissionRepository->all()
        ]);
    }

    /**
     * @param CreateOrUpdateRoleRequest $request
     * @param Role                      $role
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CreateOrUpdateRoleRequest $request, Role $role): RedirectResponse
    {
        $this->roleService->update($role, $request->validated());

        return redirect()
            ->route('admin.roles.index')
            ->with('success', 'Role have been updated');
    }

    /**
     * @param Role $role
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Role $role): JsonResponse
    {
        return $this->roleService->destroy($role)
            ? response()->success()
            : response()->error();
    }

    /**
     * @param StoreRolesRequest $request
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function batchUpdate(StoreRolesRequest $request): RedirectResponse
    {

        $updated = $this->roleService->batchUpdate($request->input('roles'));

        if (!$updated) {
            return redirect()
                ->route('admin.roles.index')
                ->with('error', 'An error occurred');
        }

        return redirect()
            ->route('admin.roles.index')
            ->with('success', 'Roles have been stored');
    }
}
