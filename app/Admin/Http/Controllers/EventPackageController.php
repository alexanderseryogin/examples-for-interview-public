<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\EventPackage\ReorderRequest;
use App\Admin\Services\EventPackageService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

/**
 * Class EventPackageController
 *
 * @package App\Admin\Http\Controllers
 */
class EventPackageController extends Controller
{
    /**
     * @var EventPackageService
     */
    private $eventPackageService;

    /**
     * EventPackageController constructor.
     *
     * @param EventPackageService $eventPackageService
     */
    public function __construct(EventPackageService $eventPackageService) {
        $this->eventPackageService = $eventPackageService;
    }

    /**
     * @param ReorderRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(ReorderRequest $request)
    {
        $this->eventPackageService->reorder($request->input('packages'));

        return response()->success();
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(int $id): JsonResponse
    {
        $deleted = $this->eventPackageService->destroy($id);

        return $deleted
            ? response()->success()
            : response()->error();
    }
}
