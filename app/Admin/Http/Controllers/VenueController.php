<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\DefaultIndexRequest;
use App\Admin\Http\Requests\Venue\BatchDeleteRequest;
use App\Admin\Http\Requests\Venue\CreateOrUpdateRequest;
use App\Admin\Repositories\CountryRepository;
use App\Admin\Repositories\VenueRepository;
use App\Admin\Services\ActionLogService;
use App\Admin\Services\ImageService;
use App\Admin\Services\VenueService;
use App\Http\Controllers\Controller;
use App\Models\Venue;
use Illuminate\Http\JsonResponse;

/**
 * Class VenueController
 *
 * @package App\Http\Controllers\Admin
 */
class VenueController extends Controller
{
    /**
     * @var VenueRepository
     */
    private $venueRepository;
    /**
     * @var VenueService
     */
    private $venueService;
    /**
     * @var CountryRepository
     */
    private $countryRepository;
    /**
     * @var ImageService
     */
    private $imageService;
    /**
     * @var ActionLogService
     */
    private $actionLogService;

    /**
     * VenueController constructor.
     *
     * @param VenueRepository   $venueRepository
     * @param VenueService      $venueService
     * @param CountryRepository $countryRepository
     * @param ImageService      $imageService
     * @param ActionLogService  $actionLogService
     */
    public function __construct(
        VenueRepository $venueRepository,
        VenueService $venueService,
        CountryRepository $countryRepository,
        ImageService $imageService,
        ActionLogService $actionLogService
    ) {
        $this->venueRepository = $venueRepository;
        $this->venueService = $venueService;
        $this->countryRepository = $countryRepository;
        $this->imageService = $imageService;
        $this->actionLogService = $actionLogService;
    }

    /**
     * @param DefaultIndexRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DefaultIndexRequest $request)
    {
        if ($request->expectsJson()) {
            $venues = $this->venueRepository->kendoList($request->validated());

            return response()->success($venues);
        }

        return view('admin.pages.venues.index', [
            'q' => $request->input('q')
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function all()
    {
        return response()->success($this->venueRepository->all('name'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.pages.venues.create', [
            'venue'     => $this->venueRepository->newInstance(),
            'countries' => $this->countryRepository->allSimple()
        ]);
    }

    /**
     * @param CreateOrUpdateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateOrUpdateRequest $request)
    {
        $venue = $this->venueService->create($request->validated());

        return redirect()
            ->route('admin.venues.edit', ['id' => $venue->id])
            ->with('success', 'Venue has been saved');
    }

    /**
     * @param Venue $venue
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Venue $venue)
    {
        $venue->load([
            'country',
            'logsLatest.user.account',
        ]);

        return view('admin.pages.venues.edit', [
            'venue'     => $venue,
            'countries' => $this->countryRepository->allSimple(),
        ]);
    }

    public function update(CreateOrUpdateRequest $request, Venue $venue)
    {
        $this->venueService->update($venue, $request->validated());

        return redirect()
            ->route('admin.venues.edit', ['id' => $venue->id])
            ->with('success', 'Venue has been updated');
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(int $id): JsonResponse
    {
        $result = $this->venueService->destroy($id);

        return $result
            ? response()->success()
            : response()->error();
    }

    /**
     * @param BatchDeleteRequest $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function batchDestroy(BatchDeleteRequest $request): JsonResponse
    {
        $result = $this->venueService->batchDestroy($request->input('ids'));

        return $result
            ? response()->success()
            : response()->error();
    }
}
