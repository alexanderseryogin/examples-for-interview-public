<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\DefaultIndexRequest;
use App\Admin\Http\Requests\Event\CreateRequest;
use App\Admin\Http\Requests\Event\SearchFaqRequest;
use App\Admin\Http\Requests\Event\StorePackageRequest;
use App\Admin\Http\Requests\Event\StoreTicketRequest;
use App\Admin\Http\Requests\Event\UpdateFaqsRequest;
use App\Admin\Http\Requests\Event\UpdateGeneralInfoRequest;
use App\Admin\Http\Requests\Event\UpdateInfoRequest;
use App\Admin\Http\Requests\Event\UpdatePackagesInfoRequest;
use App\Admin\Http\Requests\Event\UpdateStatusRequest;
use App\Admin\Http\Requests\Event\UpdateTicketsInfoRequest;
use App\Admin\Repositories\ActionLogRepository;
use App\Admin\Repositories\CountryRepository;
use App\Admin\Repositories\CurrencyRepository;
use App\Admin\Repositories\EventCategoryRepository;
use App\Admin\Repositories\EventFaqRepository;
use App\Admin\Repositories\EventPackageRepository;
use App\Admin\Repositories\EventRepository;
use App\Admin\Repositories\GenreRepository;
use App\Admin\Repositories\TicketRepository;
use App\Admin\Repositories\UserRepository;
use App\Admin\Repositories\VenueRepository;
use App\Admin\Services\EventService;
use App\Admin\Services\ImageService;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Event;
use App\Models\EventPackage;
use App\Models\Ticket;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;

/**
 * Class EventController
 *
 * @package App\Admin\Http\Controllers
 */
class EventController extends Controller
{
    /**
     * @var EventFaqRepository
     */
    private $eventFaqRepository;
    /**
     * @var EventPackageRepository
     */
    private $eventPackageRepository;
    /***
     * @var TicketRepository
     */
    private $ticketRepository;
    /**
     * @var EventCategoryRepository
     */
    private $eventCategoryRepository;
    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;
    /**
     * @var EventService
     */
    private $eventService;
    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var ImageService
     */
    private $imageService;
    /**
     * @var ActionLogRepository
     */
    private $actionLogRepository;
    /**
     * @var VenueRepository
     */
    private $venueRepository;
    /**
     * @var CountryRepository
     */
    private $countryRepository;
    /**
     * @var GenreRepository
     */
    private $genreRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * EventsController constructor.
     *
     * @param EventRepository         $eventRepository
     * @param EventService            $eventService
     * @param ImageService            $imageService
     * @param ActionLogRepository     $actionLogRepository
     * @param VenueRepository         $venueRepository
     * @param CountryRepository       $countryRepository
     * @param GenreRepository         $genreRepository
     * @param UserRepository          $userRepository
     * @param CurrencyRepository      $currencyRepository
     * @param EventCategoryRepository $eventCategoryRepository
     * @param TicketRepository        $ticketRepository
     * @param EventPackageRepository  $eventPackageRepository
     * @param EventFaqRepository      $eventFaqRepository
     */
    public function __construct(
        EventRepository $eventRepository,
        EventService $eventService,
        ImageService $imageService,
        ActionLogRepository $actionLogRepository,
        VenueRepository $venueRepository,
        CountryRepository $countryRepository,
        GenreRepository $genreRepository,
        UserRepository $userRepository,
        CurrencyRepository $currencyRepository,
        EventCategoryRepository $eventCategoryRepository,
        TicketRepository $ticketRepository,
        EventPackageRepository $eventPackageRepository,
        EventFaqRepository $eventFaqRepository
    ) {
        $this->eventRepository = $eventRepository;
        $this->eventService = $eventService;
        $this->imageService = $imageService;
        $this->actionLogRepository = $actionLogRepository;
        $this->venueRepository = $venueRepository;
        $this->countryRepository = $countryRepository;
        $this->genreRepository = $genreRepository;
        $this->userRepository = $userRepository;
        $this->currencyRepository = $currencyRepository;
        $this->eventCategoryRepository = $eventCategoryRepository;
        $this->ticketRepository = $ticketRepository;
        $this->eventPackageRepository = $eventPackageRepository;
        $this->eventFaqRepository = $eventFaqRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param DefaultIndexRequest $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(DefaultIndexRequest $request)
    {
        if ($request->expectsJson()) {
            $events = $this->eventRepository->kendoList($request->validated());

            return response()->success($events);
        }

        return view('admin.pages.events.index', [
            'statuses' => Event::getStatuses(),
            'q'        => $request->input('q')
        ]);
    }

    /**
     * @param UpdateStatusRequest $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function updateStatuses(UpdateStatusRequest $request)
    {
        $this->eventService->updateStatuses($request->input('ids'), $request->input('status'));

        return response()->success();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.events.create', [
            'event'      => $this->eventRepository->newInstance(),
            'venues'     => $this->venueRepository->allSimple(),
            'genres'     => $this->genreRepository->allSimple(),
            'users'      => $this->userRepository->allNames(),
            'categories' => $this->eventCategoryRepository->allSimple(),
            'types'      => Company::getTypesList()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Admin\Http\Requests\Event\CreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $event = $this->eventService->create($request->validated());

        if ($request->input('action') === EventService::ACTION_PUBLISH) {
            return redirect()
                ->route('admin.events.index')
                ->with('success', 'Event have been published');
        }

        return redirect()
            ->route('admin.events.edit', ['id' => $event->id])
            ->with('success', 'Event have been saved');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Event $event
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        $event->load([
            'creator.account',
            'logsLatest.user.account',
            'venue',
            'organizer',
            'category',
            'genres',
        ]);

        return view('admin.pages.events.edit', [
            'event'            => $event,
            'venues'           => $this->venueRepository->allSimple(),
            'genres'           => $this->genreRepository->allSimple(),
            'currencies'       => $this->currencyRepository->all(),
            'ticketStatuses'   => Ticket::getStatuses(),
            'packageStatuses'  => EventPackage::getStatuses(),
            'packageCardSizes' => EventPackage::getCardSizes(),
            'users'            => $this->userRepository->allNames(),
            'categories'       => $this->eventCategoryRepository->allSimple(),
            'types'            => Company::getTypesList()
        ]);
    }

    /**
     * @param Event                    $event
     * @param UpdateGeneralInfoRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function updateGeneralInfo(Event $event, UpdateGeneralInfoRequest $request)
    {
        $event = $this->eventService->updateGeneralInfo($event, $request->validated());

        $event->load([
            'creator.account',
            'logsLatest.user.account',
            'venue',
            'organizer',
            'category',
            'genres',
        ]);

        if ($request->expectsJson()) {
            return response()->success([], 'General Info autosaved');
        }

        return redirect()->route('admin.events.edit', ['id' => $event->id])->with('success', 'Event general info has been saved');
    }

    /**
     * @param Event $event
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function publish(Event $event)
    {
        $this->eventService->publish($event);

        return redirect()->route('admin.events.index')->with('success', 'Event has been published');
    }

    /**
     * @param Event $event
     *
     * @return JsonResponse
     */
    public function tickets(Event $event)
    {
        return response()->success($this->ticketRepository->transform($event->tickets));
    }

    /**
     * @param Event              $event
     * @param StoreTicketRequest $request
     *
     * @return JsonResponse
     */
    public function saveTicket(Event $event, StoreTicketRequest $request)
    {
        $ticket = $this->eventService->saveTicket($event, $request->validated());

        return response()->success($ticket);
    }

    /**
     * @param Event             $event
     * @param UpdateInfoRequest $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function updateInfo(Event $event, UpdateInfoRequest $request)
    {
        $this->eventService->updateInfo($event, $request->validated());

        if ($request->expectsJson()) {
            return response()->success([], 'Event Info autosaved');
        }

        return redirect()
            ->route('admin.events.edit', ['id' => $event->id])
            ->with('success', 'Event info has been saved');
    }

    /**
     * @param Event                    $event
     * @param UpdateTicketsInfoRequest $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function updateTicketsInfo(Event $event, UpdateTicketsInfoRequest $request)
    {
        $this->eventService->updateTicketsInfo($event, $request->validated());

        if ($request->expectsJson()) {
            return response()->success([], 'Tickets autosaved');
        }

        return redirect()
            ->route('admin.events.edit', ['id' => $event->id])
            ->with('success', 'Event info has been saved');
    }

    /**
     * @param Event                     $event
     * @param UpdatePackagesInfoRequest $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function updatePackagesInfo(Event $event, UpdatePackagesInfoRequest $request)
    {
        $this->eventService->updatePackagesInfo($event, $request->validated());

        return redirect()
            ->route('admin.events.edit', ['id' => $event->id])
            ->with('success', 'Event packages info has been saved');
    }

    /**
     * @param Event             $event
     * @param UpdateFaqsRequest $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function updateFaqs(Event $event, UpdateFaqsRequest $request)
    {
        $result = $this->eventService->updateFaqs($event, $request->input('faqs'));

        if ($request->expectsJson()) {
            return response()->success($result, 'Faq was autosaved');
        }

        return redirect()
            ->route('admin.events.edit', ['id' => $event->id])
            ->with('success', 'Event Faqs has been saved');
    }

    /**
     * @return JsonResponse
     */
    public function all()
    {
        return response()->success($this->eventRepository->all('name'));
    }

    /**
     * @param Event $event
     *
     * @return JsonResponse
     */
    public function packages(Event $event)
    {
        $event->load(['packages', 'packages.blocks']);

        return response()->success($this->eventPackageRepository->transform($event->packages));
    }

    /**
     * @param Event               $event
     * @param StorePackageRequest $request
     *
     * @return JsonResponse
     */
    public function savePackage(Event $event, StorePackageRequest $request)
    {
        $package = $this->eventService->savePackage($event, $request->validated());

        $package->load(['blocks']);

        return response()->success($this->eventPackageRepository->transform($package));
    }

    /**
     * @param Event $event
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function getEmptyFaqBlock(Event $event)
    {
        $html = view('admin.pages.events.partials.tabs.faq.index', [
            'key' => Str::random(),
            'faq' => $this->eventService->getEmptyFaq($event)
        ])->render();

        return response()->success(['html' => $html]);
    }

    /**
     * @param Event            $event
     * @param SearchFaqRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAllFaq(Event $event, SearchFaqRequest $request)
    {
        return response()->success($this->eventFaqRepository->allForCompany($event->company_id, $request->input('q')));
    }
}
