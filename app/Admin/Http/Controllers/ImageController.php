<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\Image\StoreTempRequest;
use App\Admin\Services\ImageService;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;

/**
 * Class ImageController
 *
 * @package App\Http\Controllers\Admin
 */
class ImageController extends Controller
{
    /**
     * @var ImageService
     */
    private $imageService;

    /**
     * ImageController constructor.
     *
     * @param ImageService $imageService
     */
    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    /**
     *
     * @param StoreTempRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTempRequest $request)
    {
        /** @var UploadedFile $file */
        $file = $request->file('image');

        return response()->success($this->imageService->uploadTemp($file));
    }
}
