<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\DefaultIndexRequest;
use App\Admin\Http\Requests\User\BatchDeleteRequest;
use App\Admin\Http\Requests\User\BatchUpdateRequest;
use App\Admin\Http\Requests\User\ChangeEmailRequest;
use App\Admin\Http\Requests\User\ShowRequest;
use App\Admin\Http\Requests\User\StoreUserRequest;
use App\Admin\Http\Requests\User\UpdateProfileRequest;
use App\Admin\Http\Requests\User\UpdateUserRequest;
use App\Admin\Repositories\CompanyRepository;
use App\Admin\Repositories\EventRepository;
use App\Admin\Repositories\InviteRepository;
use App\Admin\Repositories\RoleRepository;
use App\Admin\Repositories\TeamRepository;
use App\Admin\Repositories\UserRepository;
use App\Admin\Services\InviteService;
use App\Admin\Services\UserService;
use App\Helpers\AuthHelper;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

/**
 * Class UserController
 *
 * @package App\Admin\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var RoleRepository
     */
    private $roleRepository;
    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var InviteRepository
     */
    private $inviteRepository;
    /**
     * @var InviteService
     */
    private $inviteService;
    /**
     * @var TeamRepository
     */
    private $teamRepository;
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepository    $userRepository
     * @param RoleRepository    $roleRepository
     * @param EventRepository   $eventRepository
     * @param InviteService     $inviteService
     * @param InviteRepository  $inviteRepository
     * @param TeamRepository    $teamRepository
     * @param UserService       $userService
     * @param CompanyRepository $companyRepository
     */
    public function __construct(
        UserRepository $userRepository,
        RoleRepository $roleRepository,
        EventRepository $eventRepository,
        InviteService $inviteService,
        InviteRepository $inviteRepository,
        TeamRepository $teamRepository,
        UserService $userService,
        CompanyRepository $companyRepository
    ) {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->eventRepository = $eventRepository;
        $this->inviteService = $inviteService;
        $this->inviteRepository = $inviteRepository;
        $this->teamRepository = $teamRepository;
        $this->userService = $userService;
        $this->companyRepository = $companyRepository;
    }

    /**
     * @param DefaultIndexRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|View
     */
    public function index(DefaultIndexRequest $request)
    {
        if ($request->expectsJson()) {
            $users = $this->userRepository->kendoList($request->validated());

            return response()->success($users);
        }

        return view('admin.pages.users.index', [
            'q'        => $request->input('q'),
            'statuses' => User::getStatuses(),
            'roles'    => $this->roleRepository->list(),
        ]);
    }

    /**
     * @param User $user
     *
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function show(User $user)
    {
        return view('admin.pages.users.show', [
            'user'          => $user->load('company'),
            'account'       => $user->account->load('team'),
            'eventStatuses' => Event::getStatuses(),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|View
     */
    public function profile()
    {
        $user = AuthHelper::user()->load('company');

        return view('admin.pages.users.show', [
            'user'          => $user,
            'account'       => $user->account->load('team'),
            'eventStatuses' => Event::getStatuses(),
        ]);
    }

    /**
     * @param ShowRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function events(ShowRequest $request): JsonResponse
    {
        $events = $this->eventRepository->kendoUserEventList($request->validated());

        return response()->success($events);
    }

    /**
     * @param string $token
     *
     * @return View
     */
    public function acceptInvite(string $token): View
    {
        $invite = $this->inviteRepository
            ->findOneByOrFail(['token' => $token])
            ->load('roles', 'company', 'team', 'events');

        return view('admin.pages.users.create', [
            'invite' => $invite,
            'user'   => $this->userRepository->newInstance()
        ]);
    }

    /**
     * @param StoreUserRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreUserRequest $request)
    {
        $user = $this->userService->store($request->validated());

        if (!$user) {
            return redirect()
                ->back()
                ->with('error', 'User creating error');
        }

        return redirect()
            ->route('admin.users.success')
            ->with('success', 'Your account has been created.');
    }

    /**
     * @param User $user
     *
     * @return View
     */
    public function edit(User $user): View
    {
        return view('admin.pages.users.edit', [
            'user'     => $user,
            'account'  => $user->account,
            'roles'    => $this->roleRepository->allSimple(),
            'statuses' => User::getStatusesList()
        ]);
    }

    /**
     * @return View
     */
    public function profileEdit(): View
    {
        $user = AuthHelper::user();

        return view('admin.pages.users.edit-profile', [
            'user'     => $user,
            'account'  => $user->account,
            'roles'    => $this->roleRepository->allSimple(),
            'statuses' => User::getStatusesList()
        ]);
    }

    /**
     * @param User              $user
     * @param UpdateUserRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(User $user, UpdateUserRequest $request)
    {
        $updated = $this->userService->update($user, $request->validated());

        if ($request->expectsJson()) {
            return $updated
                ? response()->success()
                : response()->error();
        }

        return $updated
            ? redirect()->route('admin.users.show', $user->id)->with('success', 'User account has been updated.')
            : redirect()->back()->with('error', 'User updating error');
    }

    /**
     * @param User               $user
     * @param ChangeEmailRequest $request
     *
     * @return JsonResponse
     */
    public function changeEmail(User $user, ChangeEmailRequest $request): JsonResponse
    {
        $result = $this->userService->changeEmail($user, $request->input('email'));

        return $result
            ? response()->success($result)
            : response()->error();
    }

    /**
     * @param UpdateProfileRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function profileUpdate(UpdateProfileRequest $request)
    {
        $user = AuthHelper::user();

        $updated = $this->userService->updateProfile($user, $request->validated());

        if (!$updated) {
            return redirect()
                ->back()
                ->with('error', 'Profile updating error');
        }

        return redirect()
            ->route('admin.users.profile.show')
            ->with('success', 'Your account has been updated.');
    }

    /**
     * @param User $user
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        return $this->userService->destroy($user)
            ? response()->success()
            : response()->error();
    }

    /**
     * @param BatchDeleteRequest $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function batchDestroy(BatchDeleteRequest $request): JsonResponse
    {
        $result = $this->userService->batchDestroy($request->input('ids'));

        return $result
            ? response()->success()
            : response()->error();
    }

    /**
     * @param BatchUpdateRequest $request
     *
     * @return JsonResponse
     */
    public function batchUpdate(BatchUpdateRequest $request): JsonResponse
    {
        $result = $this->userService->batchUpdate(
            $request->input('ids'),
            array_except($request->validated(), ['ids'])
        );

        return $result
            ? response()->success()
            : response()->error();
    }
}
