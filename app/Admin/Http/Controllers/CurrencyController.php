<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Repositories\CurrencyRepository;
use App\Http\Controllers\Controller;

/**
 * Class CurrencyController
 *
 * @package App\Http\Controllers\Admin
 */
class CurrencyController extends Controller
{
    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    /**
     * CurrencyController constructor.
     *
     * @param CurrencyRepository $currencyRepository
     */
    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->success($this->currencyRepository->all());
    }
}
