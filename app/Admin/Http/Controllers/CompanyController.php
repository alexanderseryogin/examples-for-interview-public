<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\Company\StoreRequest;
use App\Admin\Http\Requests\Company\IndexRequest;
use App\Admin\Repositories\CompanyRepository;
use App\Admin\Services\CompanyService;
use App\Http\Controllers\Controller;
use App\Models\Company;

/**
 * Class CompanyController
 *
 * @package App\Admin\Http\Controllers
 */
class CompanyController extends Controller
{
    /**
     * @var CompanyService
     */
    private $companyService;
    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    /**
     * CompanyController constructor.
     *
     * @param CompanyRepository $companyRepository
     * @param CompanyService    $companyService
     */
    public function __construct(CompanyRepository $companyRepository, CompanyService $companyService)
    {
        $this->companyRepository = $companyRepository;
        $this->companyService = $companyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     *
     * @return Company[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(IndexRequest $request)
    {
        return response()->success(
            $request->has('type')
                ? $this->companyRepository->findBy(['type' => $request->input('type')])
                : $this->companyRepository->all('name')
        );
    }

    /**
     * @param StoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $company = $this->companyService->create($request->validated());

        return response()->success($company, 'Company Created');
    }
}
