<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Repositories\TeamRepository;
use App\Http\Controllers\Controller;
use App\Models\Team;

class TeamController extends Controller
{
    /**
     * @var TeamRepository
     */
    private $teamRepository;

    /**
     * TeamController constructor.
     *
     * @param TeamRepository $teamRepository
     */
    public function __construct(TeamRepository $teamRepository)
    {
        $this->teamRepository = $teamRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Team[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return response()->success(
            $this->teamRepository->all('name')
        );
    }
}
