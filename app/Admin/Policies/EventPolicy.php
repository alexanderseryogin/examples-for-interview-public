<?php

namespace App\Admin\Policies;

use App\Models\Event;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class EventPolicy
 *
 * @package App\Admin\Policies
 */
class EventPolicy
{
    use HandlesAuthorization;

    /**
     * @param User  $user
     * @param Event $event
     *
     * @return bool
     */
    public function update(User $user, Event $event): bool
    {
        return $user->can(Permission::GLOBAL_EVENTS_MANAGEMENT)
            || $user->company_id === $event->company_id
            || $user->hasCustomAccessToEvent($event->id);
    }
}