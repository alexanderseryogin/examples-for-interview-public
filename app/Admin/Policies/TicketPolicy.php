<?php

namespace App\Admin\Policies;

use App\Models\Permission;
use App\Models\Ticket;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class TicketPolicy
 *
 * @package App\Admin\Policies
 */
class TicketPolicy
{
    use HandlesAuthorization;

    /**
     * @param User   $user
     * @param Ticket $ticket
     *
     * @return bool
     */
    public function update(User $user, Ticket $ticket): bool
    {
        return $user->can(Permission::GLOBAL_TICKETS_MANAGEMENT)
            || $user->company_id === $ticket->company_id
            || $user->hasCustomAccessToEvent($ticket->event_id);
    }
}