<?php

namespace App\Admin\Policies;

use App\Models\Venue;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class VenuePolicy
 *
 * @package App\Admin\Policies
 */
class VenuePolicy
{
    use HandlesAuthorization;

    /**
     * @param User  $user
     * @param Venue $venue
     *
     * @return bool
     */
    public function update(User $user, Venue $venue): bool
    {
        return $user->can(Permission::GLOBAL_VENUES_MANAGEMENT) || $user->company_id === $venue->company_id;
    }
}