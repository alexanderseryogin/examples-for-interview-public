<?php

namespace App\Admin\Policies;

use App\Models\EventPackage;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class EventPolicy
 *
 * @package App\Admin\Policies
 */
class EventPackagePolicy
{
    use HandlesAuthorization;

    /**
     * @param User         $user
     * @param EventPackage $eventPackage
     *
     * @return bool
     */
    public function update(User $user, EventPackage $eventPackage): bool
    {
        return $user->can(Permission::GLOBAL_EVENTS_MANAGEMENT)
            || $user->company_id === $eventPackage->company_id
            || $user->hasCustomAccessToEvent($eventPackage->event_id);
    }
}