<?php

namespace App\Admin\Policies;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class UserPolicy
 *
 * @package App\Admin\Policies
 */
class UserPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param User $profileUser
     *
     * @return bool
     */
    public function update(User $user, User $profileUser): bool
    {
        return $user->can(Permission::GLOBAL_USERS_MANAGEMENT) || $user->id === $profileUser->id;
    }
}