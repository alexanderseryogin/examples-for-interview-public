<?php

namespace App\Admin\Traits\Models;

use App\Models\Company;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Trait HasCompany
 *
 * @package App\Admin\Traits\Models
 * @property int $company_id
 */
trait HasCompany
{
    /**
     * @return BelongsTo
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }
}