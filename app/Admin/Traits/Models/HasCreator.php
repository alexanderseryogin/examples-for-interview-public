<?php

namespace App\Admin\Traits\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Trait HasCreator
 *
 * @package App\Admin\Traits\Models
 * @property int $creator_id
 */
trait HasCreator
{
    /**
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}