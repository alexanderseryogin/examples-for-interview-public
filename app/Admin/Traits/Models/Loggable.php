<?php

namespace App\Admin\Traits\Models;

use App\Models\ActionLog;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait Loggable
{
    /**
     * @return MorphMany
     */
    public function logs(): MorphMany
    {
        return $this->morphMany(ActionLog::class, 'item');
    }

    /**
     * @return MorphMany
     */
    public function logsLatest(): MorphMany
    {
        return $this->logs()->latest()->take(2);
    }
}