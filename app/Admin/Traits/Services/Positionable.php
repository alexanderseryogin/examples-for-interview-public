<?php

namespace App\Admin\Traits\Services;

use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Positionable
 *
 * @package App\Admin\Traits\Services
 */
trait Positionable
{
    /**
     * @return BaseRepository
     */
    abstract protected function getPositionableRepository(): BaseRepository;

    /**
     * @param array $data
     *
     * @return void
     */
    public function reorder(array $data): void
    {
        foreach ($data as $item) {
            $this->getPositionableRepository()->update($item['id'], ['pos' => $item['pos']]);
        }
    }

    /**
     * @param Model       $insertedModel
     * @param string|null $groupField
     */
    protected function reorderAfterInsert(Model $insertedModel, ?string $groupField = 'event_id'): void
    {
        $reorderData = [];

        $where = [
            ['pos', '>=', $insertedModel->pos],
            ['id', '!=', $insertedModel->id]
        ];

        if ($groupField) {
            $where[] = [$groupField, '=', $insertedModel->{$groupField}];
        }

        $items = $this->getPositionableRepository()->findBy($where);

        foreach ($items as $item) {
            $reorderData[] = [
                'id'  => $item->id,
                'pos' => ++$item->pos
            ];
        }

        $this->reorder($reorderData);
    }
}
