<?php

namespace App\Admin\Traits\Services;

use App\Helpers\StorageHelper;
use Illuminate\Database\Eloquent\Model;

/**
 * Class HasImage
 *
 * @package App\Admin\Traits\Services
 */
trait HasImage
{
    /**
     * @return string
     */
    abstract protected function getModelFolder(): string;

    /**
     * @param Model       $model
     * @param string|null $tempPath
     * @param string      $field
     *
     * @return string|null
     */
    protected function handleImage(Model $model, ?string $tempPath, string $field): ?string
    {
        if ($tempPath != $model->{$field}) {
            if ($model->{$field}) {
                StorageHelper::deleteFile($model->{$field});

                $model->{$field} = null;
            }

            if ($tempPath) {
                $folder = $this->getImagesFolder($model->id);
                $model->{$field} = StorageHelper::copy($tempPath, $folder);
            }
        }

        return $model->{$field};
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    protected function deleteImages(int $id): bool
    {
        return StorageHelper::deleteDirectory($this->getImagesFolder($id));
    }

    /**
     * @param int $id
     *
     * @return string
     */
    protected function getImagesFolder(int $id): string
    {
        $folder = $this->getModelFolder();

        return str_finish($folder, "/$id/images");
    }
}
