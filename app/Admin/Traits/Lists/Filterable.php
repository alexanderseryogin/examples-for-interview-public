<?php

namespace App\Admin\Traits\Lists;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Builder;
use App\Admin\Contracts\FilterableList as FilterableListContract;
use Illuminate\Database\Query\Expression;

/**
 * Trait Filterable
 *
 * @package App\Admin\Traits\Lists
 */
trait Filterable
{
    /** @var array */
    protected $filterWhereColumns = [];
    /** @var array */
    protected $filterHavingColumns = [];

    /**
     * @param Builder $query
     * @param array   $filters
     */
    protected function applyFilters(Builder $query, array $filters): void
    {
        if ($filters) {
            foreach ($filters as $filter) {
                $this->applyFilter($query, $filter);
            }
        }
    }

    /**
     * @param Builder $query
     * @param array   $filter
     *
     * @return Builder
     */
    protected function applyFilter(Builder $query, array $filter)
    {
        if (!$filter) {
            return $query;
        }

        $field = $filter['field'];
        $operator = $filter['operator'];
        $value = $filter['value'];

        if (!empty($this->filterWhereColumns[$field])) {
            $columnConditions = $this->filterWhereColumns[$field];

            $query->where(function ($query) use ($columnConditions, $value, $operator) {
                /* @var Builder $query */
                if (is_array($columnConditions)) {
                    $this->applyComplexFilterWhereColumnConditions($query, $columnConditions, $value, $operator);
                } else {
                    $query->where(DB::raw($columnConditions), 'like', '%' . $value . '%');
                }
            });
        }

        if (!empty($this->filterHavingColumns[$field])) {
            $columnConditions = $this->filterHavingColumns[$field];

            if (is_array($columnConditions)) {
                $this->applyComplexFilterHavingColumnConditions($query, $columnConditions, $value, $operator);
            } else {
                $query->having(DB::raw($columnConditions), 'like', '%' . $value . '%');
            }
        }
    }

    /**
     * Apply complex where filter to query
     *
     * @param Builder $query
     * @param array   $columnConditions
     * @param string  $value
     * @param string  $operator
     */
    protected function applyComplexFilterWhereColumnConditions(Builder $query, array $columnConditions, string $value, string $operator): void
    {
        $field = $columnConditions['field'];
        $fieldType = $columnConditions['type'];

        $field = $this->prepareFilterField($field, $fieldType);
        $value = $this->prepareFilterValue($value, $fieldType);

        switch ($operator) {
            case FilterableListContract::OPERATOR_GREATER:
                $query->where($field, '>', $value);
                break;
            case FilterableListContract::OPERATOR_LESS:
                $query->where($field, '<', $value);
                break;
            case FilterableListContract::OPERATOR_EQUAL:
                $query->where($field, '=', $value);
                break;
            default:
                $query->where($field, 'like', '%' . $value . '%');
                break;
        }
    }

    /**
     * Apply complex where filter to query
     *
     * @param Builder $query
     * @param array   $columnConditions
     * @param string  $value
     * @param string  $operator
     */
    protected function applyComplexFilterHavingColumnConditions(Builder $query, array $columnConditions, string $value, string $operator): void
    {
        $field = $columnConditions['field'];
        $fieldType = $columnConditions['type'];

        $field = $this->prepareFilterField($field, $fieldType);
        $value = $this->prepareFilterValue($value, $fieldType);

        switch ($operator) {
            case FilterableListContract::OPERATOR_GREATER:
                $query->having($field, '>', $value);
                break;
            case FilterableListContract::OPERATOR_LESS:
                $query->having($field, '<', $value);
                break;
            case FilterableListContract::OPERATOR_EQUAL:
                $query->having($field, $value);
                break;
            default:
                $query->having($field, 'like', '%' . $value . '%');
                break;
        }
    }

    /**
     * @param string      $field
     * @param string|null $fieldType
     *
     * @return Expression
     */
    protected function prepareFilterField(string $field, string $fieldType = null): Expression
    {
        switch ($fieldType) {
            case FilterableListContract::FIELD_DATE:
                $field = DB::raw("DATE($field)");
                break;
            case FilterableListContract::FIELD_TIME:
                $field = DB::raw("DATE_FORMAT($field, '%H:%i')");
                break;
            default:
                $field = DB::raw($field);
                break;
        }

        return $field;
    }

    /**
     * @param string      $value
     * @param string|null $fieldType
     *
     * @return string
     */
    protected function prepareFilterValue(string $value, string $fieldType = null): string
    {
        switch ($fieldType) {
            case FilterableListContract::FIELD_DATE:
                $value = (new Carbon($value))->format('Y-m-d');
                break;
        }

        return $value;
    }

    /**
     * @return array
     */
    public static function getOperators(): array
    {
        return [
            FilterableListContract::OPERATOR_GREATER,
            FilterableListContract::OPERATOR_LESS,
            FilterableListContract::OPERATOR_EQUAL,
            FilterableListContract::OPERATOR_CONTAINS
        ];
    }
}
