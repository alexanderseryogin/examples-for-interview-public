<?php

namespace App\Admin\Traits\Lists;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Trait Groupable
 *
 * @package App\Admin\Traits\Lists
 */
trait Groupable
{
    /**
     * @param Collection $items
     * @param array      $groups
     *
     * @return Collection
     */
    protected function buildGroupedItems(Collection $items, array $groups): Collection
    {
        $items = $items->groupBy($groups);

        return $this->formatGroupedItems($items, $groups);
    }

    /**
     * @param Collection $items
     * @param array      $groups
     *
     * @return Collection
     */
    protected function formatGroupedItems(Collection $items, array $groups): Collection
    {
        $nextGroups = $groups;

        $group = array_shift($nextGroups);

        $result = $items->map(function ($item, $key) use ($group, $nextGroups) {
            return $item instanceof Collection
                ? [
                    'field'        => $group,
                    'value'        => $key,
                    'items'        => $this->formatGroupedItems($item, $nextGroups),
                    'hasSubgroups' => $item->first() instanceof Collection,
                    'aggregates'   => [
                        'field'     => $group,
                        'aggregate' => $item->count()
                    ]
                ] : $item;
        });

        return $result->values();
    }

    /**
     * @param Builder $query
     * @param array   $grouping
     */
    protected function applyGrouping(Builder $query, array $grouping): void
    {
        if ($grouping) {
            foreach ($grouping as $item) {
                $query->orderBy($item['field'], $item['dir']);
            }
        }
    }
}
