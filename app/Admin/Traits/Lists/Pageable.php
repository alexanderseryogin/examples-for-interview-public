<?php

namespace App\Admin\Traits\Lists;

use Illuminate\Database\Eloquent\Builder;

/**
 * Trait Pageable
 *
 * @package App\Admin\Traits\Lists
 */
trait Pageable
{
    /**
     * @param Builder $query
     * @param array   $params
     */
    protected function applyPagination(Builder $query, array $params): void
    {
        $pageSize = $params['page_size'] ?? $this->pageSize;
        $page = $params['page'] ?? 1;

        $query->offset(($page - 1) * $pageSize)
            ->limit($pageSize);
    }
}
