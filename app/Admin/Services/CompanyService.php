<?php

namespace App\Admin\Services;

use App\Admin\Repositories\CompanyRepository;
use App\Helpers\StorageHelper;
use App\Models\Company;

/**
 * Class CompanyService
 *
 * @package App\Admin\Services
 */
class CompanyService
{
    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    /**
     * CompanyService constructor.
     *
     * @param CompanyRepository $companyRepository
     */
    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    /**
     * @param array $data
     *
     * @return Company|null
     */
    public function create(array $data): ?Company
    {
        if (isset($data['logo'])) {
            $logo = $data['logo'];
            unset($data['logo']);
        } else {
            $logo = null;
        }

        $company = $this->companyRepository->create($data);

        if (!$company) {
            return null;
        }

        $this->handleLogo($company, $logo);

        $company->save();

        return $company->fresh();
    }

    /**
     * @param Company   $company
     * @param string|null $tempPath
     *
     * @return string|null
     */
    private function handleLogo(Company $company, ?string $tempPath): ?string
    {
        if ($tempPath != $company->logo) {
            if ($company->logo) {
                StorageHelper::deleteFile($company->logo);

                $company->logo = null;
            }

            if ($tempPath) {
                $company->logo = StorageHelper::move($tempPath, Company::FOLDER . '/images/' . $company->id);
            }
        }

        return $company->logo;
    }
}
