<?php

namespace App\Admin\Services;

use App\Admin\Repositories\AccountRepository;
use App\Admin\Repositories\InviteRepository;
use App\Admin\Repositories\UserRepository;
use App\Admin\Traits\Services\HasImage;
use App\Helpers\StorageHelper;
use App\Mail\EmailChanged;
use App\Mail\UserRegistered;
use App\Models\ActionLog;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Hash;
use Laravel\Socialite\Contracts\User as SocialiteUser;
use Mail;

class UserService
{
    use HasImage;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var InviteRepository
     */
    private $inviteRepository;
    /**
     * @var AccountRepository
     */
    private $accountRepository;
    /**
     * @var ActionLogService
     */
    private $actionLogService;

    /**
     * UserService constructor.
     *
     * @param UserRepository    $userRepository
     * @param InviteRepository  $inviteRepository
     * @param AccountRepository $accountRepository
     * @param ActionLogService  $actionLogService
     */
    public function __construct(
        UserRepository $userRepository,
        InviteRepository $inviteRepository,
        AccountRepository $accountRepository,
        ActionLogService $actionLogService
    ) {
        $this->userRepository = $userRepository;
        $this->inviteRepository = $inviteRepository;
        $this->accountRepository = $accountRepository;
        $this->actionLogService = $actionLogService;
    }

    /**
     * @param array $data
     *
     * @return User|null
     */
    public function store(array $data): ?User
    {
        $invite = $this->inviteRepository->findOneByOrFail(['token' => $data['token']]);

        $user = $this->userRepository->create([
            'email'      => $invite->email,
            'company_id' => $invite->company_id,
            'password'   => Hash::make($data['password']),
            'status'     => User::STATUS_ACTIVE
        ]);

        if (!$user) {
            return null;
        }

        $account = $this->accountRepository->create([
            'last_name'  => $data['last_name'],
            'first_name' => $data['first_name'],
            'email'      => $invite->email,
            'team_id'    => $invite->team_id,
        ]);

        /** @var User $user */
        $user->account()->save($account);
        $user->syncRoles($invite->roles);

        $user->accessEvents()->sync($invite->events->pluck('id'));

        $this->inviteRepository->deleteWhere(['email' => $invite->email]);

        Mail::to(config('mail.admin.address'))->send(new UserRegistered($user));

        return $user->fresh();
    }

    /**
     * @param User  $user
     * @param array $data
     *
     * @return User|null
     */
    public function update(User $user, array $data): ?User
    {
        $oldEmail = $user->email;

        $updated = $this->accountRepository->update($user->account->id, array_except($data, ['role', 'avatar', 'status']));

        if (!$updated) {
            return null;
        }

        $userData = array_only($data, ['email', 'status', 'company_id']);

        if (!empty($data['status']) && $data['status'] === User::STATUS_DELETED) {
            $userData['deleted_at'] = Carbon::now();
        }

        $this->userRepository->update($user->id, $userData);

        $user->syncRoles($data['role']);
        $this->handleImage($user->account, $data['avatar'] ?? null, 'avatar');

        $user->account->save();

        $this->actionLogService->userLog($user->id, ActionLog::ACTION_USER_UPDATED);

        if ($oldEmail !== $data['email']) {
            Mail::to($oldEmail)->send(new EmailChanged($user->fresh()));
        }

        return $user;
    }

    /**
     * @param User   $user
     * @param string $email
     *
     * @return User|null
     */
    public function changeEmail(User $user, string $email): ?User
    {
        $oldEmail = $user->email;
        $updated = $this->userRepository->update($user->id, ['email' => $email]);

        if (!$updated) {
            return null;
        }

        $this->actionLogService->userLog($user->id, ActionLog::ACTION_USER_UPDATED);
        Mail::to($oldEmail)->send(new EmailChanged($user->fresh()));

        return $user;
    }

    /**
     * @param User  $user
     * @param array $data
     *
     * @return User|null
     */
    public function updateProfile(User $user, array $data): ?User
    {
        $updated = $this->accountRepository->update($user->account->id, array_except($data, ['avatar', 'status']));

        if (!$updated) {
            return null;
        }

        $this->handleImage($user->account, $data['avatar'] ?? null, 'avatar');
        $user->account->save();

        $this->userRepository->update($user->id, array_only($data, ['email', 'status']));

        if (!empty($data['password'])) {
            $this->userRepository->update($user->id, [
                'password' => \Hash::make($data['password'])
            ]);
        }

        $this->actionLogService->userLog($user->id, ActionLog::ACTION_USER_UPDATED);

        return $user;
    }

    /**
     * @return void
     */
    public function updateLastActive(): void
    {
        $this->userRepository->update(Auth::id(), [
            'last_active_at' => Carbon::now()
        ]);
    }

    /**
     * @param User $user
     *
     * @return bool|null
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $deleted = $this->userRepository->update($user->id, ['status' => User::STATUS_DELETED, 'deleted_at' => Carbon::now()]);

        if ($deleted) {
            $this->actionLogService->userLog($user->id, ActionLog::ACTION_USER_DELETED, $user->email);
        }

        return $deleted;
    }

    /**
     * @param array $ids
     *
     * @return int
     * @throws \Exception
     */
    public function batchDestroy(array $ids): int
    {
        $deleted = $this->userRepository->batchUpdate($ids, ['status' => User::STATUS_DELETED, 'deleted_at' => Carbon::now()]);

        if ($deleted) {
            foreach ($ids as $id) {
                $this->actionLogService->userLog($id, ActionLog::ACTION_USER_DELETED, $id);
            }
        }

        return $deleted;
    }

    /**
     * @param array $ids
     * @param array $data
     *
     * @return int
     */
    public function batchUpdate(array $ids, array $data): int
    {
        $updated = $this->userRepository->batchUpdate($ids, $data);

        if ($updated) {
            foreach ($ids as $id) {
                $this->actionLogService->userLog($id, ActionLog::ACTION_USER_UPDATED, $id);
            }
        }

        return $updated;
    }

    /**
     * @param SocialiteUser $socialiteUser
     *
     * @return User
     */
    public function createFromSocial(SocialiteUser $socialiteUser): User
    {
        /**
         * @var User $user
         */
        $user = $this->userRepository->firstOrCreate(['email' => $socialiteUser->getEmail()], [
            'email' => $socialiteUser->getEmail(),
        ]);

        if (!$user->account) {

            $account = $this->accountRepository->create([
                'first_name' => explode(' ', $socialiteUser->getName())[0],
                'last_name'  => explode(' ', $socialiteUser->getName())[1],
                'user_id'    => $user->id,
            ]);

            $this->handleImage($account, StorageHelper::downloadFile($socialiteUser->getAvatar()), 'avatar');

            $user->account()->save($account);
        }

        return $user;
    }

    /**
     * @return string
     */
    protected function getModelFolder(): string
    {
        return User::FOLDER;
    }
}