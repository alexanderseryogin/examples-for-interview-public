<?php

namespace App\Admin\Services;

use App\Admin\Repositories\EventQuoteRepository;

/**
 * Class EventQuoteService
 *
 * @package App\Admin\Services
 */
class EventQuoteService
{
    /**
     * @var EventQuoteRepository
     */
    private $eventQuoteRepository;

    /**
     * EventQuoteService constructor.
     *
     * @param EventQuoteRepository $eventQuoteRepository
     */
    public function __construct(EventQuoteRepository $eventQuoteRepository)
    {
        $this->eventQuoteRepository = $eventQuoteRepository;
    }
}
