<?php

namespace App\Admin\Services;

use App\Admin\Repositories\EventPackageRepository;
use App\Admin\Traits\Services\HasImage;
use App\Admin\Traits\Services\Positionable;
use App\Models\EventPackage;
use App\Models\EventPackageBlock;

/**
 * Class EventPackageService
 *
 * @package App\Admin\Services
 */
class EventPackageService
{
    use Positionable, HasImage;
    /**
     * @var EventPackageBlockService
     */
    private $eventPackageBlockService;
    /**
     * @var EventPackageRepository
     */
    private $eventPackageRepository;

    /**
     * EventPackageService constructor.
     *
     * @param EventPackageRepository   $eventPackageRepository
     * @param EventPackageBlockService $eventPackageBlockService
     */
    public function __construct(EventPackageRepository $eventPackageRepository, EventPackageBlockService $eventPackageBlockService)
    {
        $this->eventPackageRepository = $eventPackageRepository;
        $this->eventPackageBlockService = $eventPackageBlockService;
    }

    /**
     * @return EventPackageRepository
     */
    protected function getPositionableRepository(): EventPackageRepository
    {
        return $this->eventPackageRepository;
    }

    /**
     * @return string
     */
    protected function getModelFolder(): string
    {
        return EventPackage::FOLDER;
    }

    /**
     * @param array $data
     *
     * @return EventPackage|null
     */
    public function create(array $data): ?EventPackage
    {
        $createData = array_except($data, ['card_image', 'header_image']);
        $data['pos'] = $data['pos'] ?? 0;

        /** @var EventPackage $eventPackage */
        $eventPackage = $this->eventPackageRepository->create($createData);

        if (!$eventPackage) {
            return null;
        }

        $this->reorderAfterInsert($eventPackage);

        $this->addDefaultInfoBlocks($eventPackage);

        $this->handleImage($eventPackage, $data['card_image'] ?? null, 'card_image');

        if (!empty($data['use_same_more_info'])) {
            $eventPackage->header_image = $eventPackage->card_image;
        } else {
            $this->handleImage($eventPackage, $data['header_image'] ?? null, 'header_image');
        }

        $eventPackage->save();

        return $eventPackage->fresh();
    }

    /**
     * @param EventPackage $eventPackage
     *
     * @return void
     */
    private function addDefaultInfoBlocks(EventPackage $eventPackage): void
    {
        $packageBlockTypes = EventPackageBlock::getTypesTitles();

        $pos = 0;

        foreach ($packageBlockTypes as $type => $title) {
            $infoBlockData = [
                'event_package_id' => $eventPackage->id,
                'type'             => $type,
                'title'            => $title,
                'pos'              => $pos++
            ];
            $this->eventPackageBlockService->create($infoBlockData);
        }
    }

    /**
     * @param EventPackage $eventPackage
     * @param array        $data
     *
     * @return EventPackage|null
     */
    public function update(EventPackage $eventPackage, array $data): ?EventPackage
    {
        $updateData = array_except($data, ['card_image', 'header_image']);
        $updated = $this->eventPackageRepository->update($eventPackage->id, $updateData);

        if (!$updated) {
            return null;
        }

        $this->handleImage($eventPackage, $data['card_image'] ?? null, 'card_image');

        if (!empty($data['use_same_more_info'])) {
            $eventPackage->header_image = $eventPackage->card_image;
        } else {
            $this->handleImage($eventPackage, $data['header_image'] ?? null, 'header_image');
        }

        $eventPackage->save();

        if (!empty($data['blocks'])) {
            foreach ($data['blocks'] as $block) {
                $this->eventPackageBlockService->update($block['id'], array_except($block, ['id', 'event_package_id']));
            }
        }

        return $eventPackage->fresh();
    }

    /**
     * @param array $data
     *
     * @return EventPackage|null
     */
    public function save(array $data): ?EventPackage
    {
        if (!empty($data['id'])) {
            $ticket = $this->eventPackageRepository->find($data['id']);

            if (!$ticket) {
                return null;
            }

            unset($data['id']);

            return $this->update($ticket, $data);
        } else {
            return $this->create($data);
        }
    }

    /**
     *
     * @param int $id
     *
     * @return bool
     * @throws \Exception
     */
    public function destroy(int $id): bool
    {
        $deleted = $this->eventPackageRepository->delete($id);

        if ($deleted) {
            $this->deleteImages($id);
        }

        return $deleted;
    }
}
