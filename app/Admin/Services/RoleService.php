<?php

namespace App\Admin\Services;

use App\Admin\Repositories\RoleRepository;
use App\Models\ActionLog;
use App\Models\Role;
use DB;

class RoleService
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;
    /**
     * @var ActionLogService
     */
    private $actionLogService;

    /**
     * RoleService constructor.
     *
     * @param RoleRepository   $roleRepository
     * @param ActionLogService $actionLogService
     */
    public function __construct(RoleRepository $roleRepository, ActionLogService $actionLogService)
    {
        $this->roleRepository = $roleRepository;
        $this->actionLogService = $actionLogService;
    }

    /**
     *
     * @param array $data
     *
     * @return mixed
     */
    public function store(array $data): Role
    {
        $role = $this->roleRepository->create(array_only($data, ['name']));
        $role->givePermissionTo($data['permissions'] ?? null);

        return $role;
    }

    /**
     * @param Role  $role
     * @param array $data
     *
     * @return Role|null
     */
    public function update(Role $role, array $data): ?Role
    {
        if ($role->name === Role::SUPER_ADMIN) {
            return null;
        }

        $this->roleRepository->update($role->id, array_only($data, ['name']));
        $role->syncPermissions($data['permissions'] ?? null);

        return $role;
    }

    /**
     *
     * @param Role $role
     *
     * @return bool
     * @throws \Exception
     */
    public function destroy(Role $role): bool
    {
        $deleted = $this->roleRepository->delete($role->id);

        if ($deleted) {
            $this->actionLogService->roleLog($role->id, ActionLog::ACTION_ROLE_DELETED, $role->name);
        }

        return $deleted;
    }

    /**
     * @param array $roles
     *
     * @return bool
     * @throws \Exception
     */
    public function batchUpdate(array $roles): bool
    {
        /**
         * @var $role Role
         */
        $rolesAll = $this->roleRepository->all();

        DB::beginTransaction();

        try {
            foreach ($rolesAll as $role) {
                if ($role->name === Role::SUPER_ADMIN) {
                    continue;
                }
                // If given roles contains in user input (checked one or more checkboxes)
                if (isset($roles[$role->id])) {
                    $permissionIds = $roles[$role->id];
                    // reassign permissions
                    $role->syncPermissions($permissionIds);
                } else {
                    // No one permission in role not checked --> clear all permission
                    $role->syncPermissions();
                }
            }

            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollback();

            return false;
        }
    }
}