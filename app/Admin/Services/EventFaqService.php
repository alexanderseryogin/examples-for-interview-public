<?php

namespace App\Admin\Services;

use App\Admin\Repositories\EventFaqRepository;
use App\Admin\Traits\Services\Positionable;
use App\Models\EventFaq;

/**
 * Class EventFaqService
 *
 * @package App\Admin\Services
 */
class EventFaqService
{
    use Positionable;
    /**
     * @var EventFaqRepository
     */
    private $eventFaqRepository;

    /**
     * EventFaqService constructor.
     *
     * @param EventFaqRepository $eventFaqRepository
     */
    public function __construct(EventFaqRepository $eventFaqRepository)
    {
        $this->eventFaqRepository = $eventFaqRepository;
    }

    /**
     * @return EventFaqRepository
     */
    protected function getPositionableRepository(): EventFaqRepository
    {
        return $this->eventFaqRepository;
    }

    /**
     * @param array $data
     *
     * @return EventFaq|null
     */
    public function create(array $data): ?EventFaq
    {
        $data['pos'] = $data['pos'] ?? 0;
        $data['is_visible'] = $data['is_visible'] ?? 0;
        $data['is_template'] = $data['is_template'] ?? 0;

        /** @var EventFaq $e */
        $eventFaq = $this->eventFaqRepository->create($data);

        if (!$eventFaq) {
            return null;
        }

        $this->reorderAfterInsert($eventFaq);

        return $eventFaq->fresh();
    }

    /**
     * @param EventFaq $eventFaq
     * @param array    $data
     *
     * @return EventFaq|null
     */
    public function update(EventFaq $eventFaq, array $data): ?EventFaq
    {
        $data['is_visible'] = $data['is_visible'] ?? 0;
        $data['is_template'] = $data['is_template'] ?? 0;

        $updated = $this->eventFaqRepository->update($eventFaq->id, $data);

        if (!$updated) {
            return null;
        }

        return $eventFaq->fresh();
    }

    /**
     * @param array $data
     *
     * @return EventFaq|null
     */
    public function save(array $data): ?EventFaq
    {
        if (!empty($data['id'])) {
            $faq = $this->eventFaqRepository->find($data['id']);

            if (!$faq) {
                return null;
            }

            unset($data['id']);

            return $this->update($faq, $data);
        } else {
            return $this->create($data);
        }
    }

    /**
     *
     * @param int $id
     *
     * @return bool
     * @throws \Exception
     */
    public function destroy(int $id): bool
    {
        return $this->eventFaqRepository->delete($id);
    }
}
