<?php

namespace App\Admin\Services;

use App\Admin\Repositories\EventImageRepository;
use App\Helpers\StorageHelper;
use App\Models\EventImage;

/**
 * Class EventImageService
 *
 * @package App\Admin\Services
 */
class EventImageService
{
    /**
     * @var EventImageRepository
     */
    private $eventImageRepository;

    /**
     * EventImageRepository constructor.
     *
     * @param EventImageRepository $eventImageRepository
     */
    public function __construct(EventImageRepository $eventImageRepository)
    {
        $this->eventImageRepository = $eventImageRepository;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data)
    {
        $data['path'] = StorageHelper::copy($data['path'], EventImage::FOLDER);

        return $this->eventImageRepository->create($data);
    }

    /**
     * @param int $id
     *
     * @return bool|null
     * @throws \Exception
     */
    public function destroy(int $id): ?bool
    {
        $eventImage = $this->eventImageRepository->find($id);

        $deleted = $this->eventImageRepository->delete($id);

        if ($deleted) {
            StorageHelper::deleteFile($eventImage->path);
        }

        return $deleted;
    }
}