<?php

namespace App\Admin\Services;

use App\Helpers\StorageHelper;
use Illuminate\Http\UploadedFile;

/**
 * Class ImageService
 *
 * @package App\Admin\Services
 */
class ImageService
{
    /**
     * @param UploadedFile $image
     *
     * @return array
     */
    public function uploadTemp(UploadedFile $image): array
    {
        $path = StorageHelper::uploadTempFile($image);

        return [
            'path' => $path,
            'url'  => StorageHelper::url($path),
            'name' => StorageHelper::name($path)
        ];
    }
}