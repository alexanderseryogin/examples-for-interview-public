<?php

namespace App\Admin\Services;

use App\Admin\Repositories\EventInfoBlockRepository;
use App\Models\EventInfoBlock;

/**
 * Class EventInfoBlockService
 *
 * @package App\Admin\Services
 */
class EventInfoBlockService
{
    /**
     * @var EventInfoBlockRepository
     */
    private $eventInfoBlockRepository;

    /**
     * EventInfoBlockService constructor.
     *
     * @param EventInfoBlockRepository $eventInfoBlockRepository
     */
    public function __construct(EventInfoBlockRepository $eventInfoBlockRepository)
    {
        $this->eventInfoBlockRepository = $eventInfoBlockRepository;
    }

    /**
     * @param array $data
     *
     * @return EventInfoBlock|false
     */
    public function create(array $data)
    {
        return $this->eventInfoBlockRepository->create($data);
    }

    /**
     * @param int   $id
     * @param array $data
     *
     * @return bool
     */
    public function update(int $id, array $data): bool
    {
        return $this->eventInfoBlockRepository->update($id, $data);
    }
}
