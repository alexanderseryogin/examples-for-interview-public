<?php

namespace App\Admin\Services;

use App\Admin\Repositories\ListViewRepository;
use App\Helpers\AuthHelper;
use App\Models\ListView;

/**
 * Class ListViewService
 *
 * @package App\Admin\Services
 */
class ListViewService
{
    /**
     * @var ListViewRepository
     */
    private $listViewRepository;

    /**
     * ListViewService constructor.
     *
     * @param ListViewRepository $listViewRepository
     */
    public function __construct(ListViewRepository $listViewRepository)
    {
        $this->listViewRepository = $listViewRepository;
    }

    /**
     * @param array $params
     *
     * @return ListView|null
     */
    public function storeForCurrentUser(array $params): ?ListView
    {
        /** @var ListView $view */
        $view = $this->listViewRepository->create([
            'name'       => $params['name'],
            'user_id'    => AuthHelper::id(),
            'options'    => $params['options'],
            'is_default' => $params['is_default'],
            'type'       => $params['type']
        ]);

        if ($view && $view->is_default) {
            $this->listViewRepository
                ->updateWhere(
                    [
                        ['user_id', '=', AuthHelper::id()],
                        ['type', '=', $params['type']],
                        ['id', '!=', $view->id]
                    ],
                    [
                        'is_default' => 0
                    ]);
        }

        return $view;
    }
}
