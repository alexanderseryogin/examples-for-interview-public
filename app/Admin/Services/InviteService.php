<?php

namespace App\Admin\Services;

use App\Admin\Repositories\InviteRepository;
use App\Mail\InviteCreated;
use App\Models\Invite;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Mail;

class InviteService
{
    /**
     * @var InviteRepository
     */
    private $inviteRepository;

    /**
     * InviteService constructor.
     *
     * @param InviteRepository $inviteRepository
     */
    public function __construct(InviteRepository $inviteRepository)
    {
        $this->inviteRepository = $inviteRepository;
    }

    /**
     * @param array $data
     *
     * @return Collection|null
     */
    public function store(array $data): ?Collection
    {
        $invites = new Collection();

        foreach ($data['emails'] as $email) {
            $inviteData = $this->prepareData(array_merge($data, ['email' => $email]));

            $invite = $this->inviteRepository->create($inviteData);

            if (!$invite) {
                return null;
            }

            /** @var Invite $invite */
            $invite->syncRoles($data['role']);

            if (isset($data['events'])) {
                $invite->events()->sync($data['events']);
            }

            Mail::to($inviteData['email'])->send(new InviteCreated($invite));

            $invites->push($invite);
        }

        return $invites;
    }

    private function prepareData(array $data): array
    {
        $token = Str::random(32);

        return [
            'email'      => $data['email'],
            'token'      => $token,
            'company_id' => $data['company_id'],
            'team_id'    => $data['team_id'] ?? null
        ];
    }
}