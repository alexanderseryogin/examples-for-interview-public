<?php

namespace App\Admin\Services;

use App\Admin\Repositories\EventPackageBlockRepository;
use App\Admin\Traits\Services\HasImage;
use App\Models\EventPackageBlock;

/**
 * Class EventPackageBlockService
 *
 * @package App\Admin\Services
 */
class EventPackageBlockService
{
    use HasImage;
    /**
     * @var EventPackageBlockRepository
     */
    private $eventPackageBlockRepository;

    /**
     * EventPackageBlockService constructor.
     *
     * @param EventPackageBlockRepository $eventPackageBlockRepository
     */
    public function __construct(EventPackageBlockRepository $eventPackageBlockRepository)
    {
        $this->eventPackageBlockRepository = $eventPackageBlockRepository;
    }

    /**
     * @return string
     */
    protected function getModelFolder(): string
    {
        return EventPackageBlock::FOLDER;
    }

    /**
     * @param array $data
     *
     * @return EventPackageBlock|null
     */
    public function create(array $data): ?EventPackageBlock
    {
        $eventPackageBlock = $this->eventPackageBlockRepository->create(array_except($data, 'icon'));

        if (!$eventPackageBlock) {
            return null;
        }

        $this->handleImage($eventPackageBlock, $data['icon'] ?? null, 'icon');

        $eventPackageBlock->save();

        return $eventPackageBlock;
    }

    /**
     * @param int   $id
     * @param array $data
     *
     * @return EventPackageBlock|null
     */
    public function update(int $id, array $data): ?EventPackageBlock
    {
        $updated = $this->eventPackageBlockRepository->update($id, array_except($data, 'icon'));

        if (!$updated) {
            return null;
        }

        $eventPackageBlock = $this->eventPackageBlockRepository->find($id);

        $this->handleImage($eventPackageBlock, $data['icon'] ?? null, 'icon');

        $eventPackageBlock->save();

        return $eventPackageBlock;
    }
}
