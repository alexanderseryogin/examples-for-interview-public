<?php

namespace App\Admin\Services;

use App\Admin\Repositories\EventFaqRepository;
use App\Admin\Repositories\EventQuoteRepository;
use App\Admin\Repositories\EventRepository;
use App\Admin\Traits\Services\HasImage;
use App\Helpers\AuthHelper;
use App\Helpers\DateHelper;
use App\Models\ActionLog;
use App\Models\Event;
use App\Models\EventFaq;
use App\Models\EventImage;
use App\Models\EventInfoBlock;
use App\Models\EventPackage;
use App\Models\Ticket;

/**
 * Class EventService
 *
 * @package App\Admin\Services
 */
class EventService
{
    use HasImage;

    const ACTION_PUBLISH = 'Publish';

    const ACTION_SAVE = 'Save';

    /**
     * @var EventFaqRepository
     */
    private $eventFaqRepository;
    /**
     * @var EventFaqService
     */
    private $eventFaqService;
    /**
     * @var EventPackageService
     */
    private $eventPackageService;
    /**
     * @var EventImageService
     */
    private $eventImageService;
    /**
     * @var EventQuoteRepository
     */
    private $eventQuoteRepository;
    /**
     * @var EventInfoBlockService
     */
    private $eventInfoBlockService;
    /**
     * @var TicketService
     */
    private $ticketService;
    /**
     * @var ImageService
     */
    private $imageService;
    /**
     * @var ActionLogService
     */
    private $actionLogService;
    /**
     * @var EventRepository
     */
    private $eventRepository;

    /**
     * EventService constructor.
     *
     * @param EventRepository       $eventRepository
     * @param ActionLogService      $actionLogService
     * @param ImageService          $imageService
     * @param TicketService         $ticketService
     * @param EventInfoBlockService $eventInfoBlockService
     * @param EventQuoteRepository  $eventQuoteRepository
     * @param EventImageService     $eventImageService
     * @param EventPackageService   $eventPackageService
     * @param EventFaqService       $eventFaqService
     * @param EventFaqRepository    $eventFaqRepository
     */
    public function __construct(
        EventRepository $eventRepository,
        ActionLogService $actionLogService,
        ImageService $imageService,
        TicketService $ticketService,
        EventInfoBlockService $eventInfoBlockService,
        EventQuoteRepository $eventQuoteRepository,
        EventImageService $eventImageService,
        EventPackageService $eventPackageService,
        EventFaqService $eventFaqService,
        EventFaqRepository $eventFaqRepository
    ) {
        $this->eventRepository = $eventRepository;
        $this->actionLogService = $actionLogService;
        $this->imageService = $imageService;
        $this->ticketService = $ticketService;
        $this->eventInfoBlockService = $eventInfoBlockService;
        $this->eventQuoteRepository = $eventQuoteRepository;
        $this->eventImageService = $eventImageService;
        $this->eventPackageService = $eventPackageService;
        $this->eventFaqService = $eventFaqService;
        $this->eventFaqRepository = $eventFaqRepository;
    }

    /**
     * @return string
     */
    protected function getModelFolder(): string
    {
        return Event::FOLDER;
    }

    /**
     * @param array $data
     *
     * @return Event
     */
    public function create(array $data): Event
    {
        $eventData = $this->prepareGeneralInfoData($data);

        $eventData['creator_id'] = AuthHelper::id();
        $eventData['company_id'] = AuthHelper::companyId();
        $eventData['status'] = Event::STATUS_DRAFT;

        $event = $this->eventRepository->create($eventData);

        if (!empty($data['genres'])) {
            $event->genres()->sync($data['genres']);
        }

        $this->handleImage($event, $data['banner_desktop'] ?? null, 'banner_desktop');
        $this->handleImage($event, $data['banner_mobile'] ?? null, 'banner_mobile');

        $event->save();

        $this->addDefaultInfoBlocks($event);

        $this->actionLogService->eventLog($event->id, ActionLog::ACTION_EVENT_CREATED);

        return $event;
    }

    /**
     * @param Event $event
     *
     * @return void
     */
    private function addDefaultInfoBlocks(Event $event): void
    {
        $infoBlockTypes = EventInfoBlock::getTypesTitles();

        $pos = 0;

        foreach ($infoBlockTypes as $type => $title) {
            $infoBlockData = [
                'event_id' => $event->id,
                'type'     => $type,
                'title'    => $title,
                'pos'      => $pos++
            ];
            $this->eventInfoBlockService->create($infoBlockData);
        }
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function prepareGeneralInfoData(array $data): array
    {
        $fullStartDate = DateHelper::generateDate($data['start_date'], $data['start_date_hour'], $data['start_date_minute']);
        $fullEndDate = DateHelper::generateDate($data['end_date'], $data['end_date_hour'], $data['end_date_minute']);
        $fullCustomDate = DateHelper::generateDate($data['custom_date'], $data['custom_date_hour'], $data['custom_date_minute']);

        $eventData = [
            'name'                  => $data['name'],
            'venue_id'              => $data['venue_id'],
            'organizer_id'          => $data['organizer_id'],
            'category_id'           => $data['category_id'],
            'has_redtix_guarantee'  => $data['has_redtix_guarantee'] ?? 0,
            'slug'                  => $data['url'],
            'start_date'            => $fullStartDate,
            'end_date'              => $fullEndDate,
            'is_estimated_end_time' => !empty($data['is_estimated_end_time']),
            'show_countdown'        => !empty($data['show_countdown']),
            'description'           => $data['description'],
        ];

        $eventData['custom_date'] = !empty($data['add_custom_datetime']) ? $fullCustomDate : null;

        return $eventData;
    }

    /**
     * @param Event|null $event
     * @param array      $data
     *
     * @return Event
     */
    public function updateGeneralInfo(Event $event, array $data): ?Event
    {
        $eventData = $this->prepareGeneralInfoData($data);

        $updated = $this->eventRepository->update($event->id, $eventData);

        if (!$updated) {
            return null;
        }

        $event->genres()->sync($data['genres'] ?? []);

        $this->handleImage($event, $data['banner_desktop'] ?? null, 'banner_desktop');
        $this->handleImage($event, $data['banner_mobile'] ?? null, 'banner_mobile');

        $event->save();

        $this->actionLogService->eventLog($event->id, ActionLog::ACTION_EVENT_UPDATED);

        return $event->fresh();
    }

    /**
     * @param Event $event
     *
     * @return Event|null
     */
    public function publish(Event $event): ?Event
    {
        $updated = $this->eventRepository->update($event->id, [
            'status' => Event::STATUS_PUBLISHED
        ]);

        if (!$updated) {
            return null;
        }

        return $event->fresh();
    }

    /**
     * @param Event|null $event
     * @param array      $data
     *
     * @return Event
     * @throws \Exception
     */
    public function updateInfo(Event $event, array $data): ?Event
    {
        $eventData = [
            'info_tab_name'       => $data['info_tab_name'],
            'is_info_tab_visible' => $data['is_info_tab_visible'] ?? 0,
            'info_video'          => $data['info_video'],
        ];

        $updated = $this->eventRepository->update($event->id, $eventData);

        if (!$updated) {
            return null;
        }

        foreach ($data['infoBlocks'] as $infoBlock) {
            $this->eventInfoBlockService->update($infoBlock['id'], [
                'pos'        => $infoBlock['pos'],
                'is_visible' => $infoBlock['is_visible'] ?? 0
            ]);
        }

        $data['quotes'] = $data['quotes'] ?? [];

        $currentQuotesIds = $event->quotes->pluck('id')->toArray();
        $deletedQuotes = array_diff($currentQuotesIds, array_filter(array_column($data['quotes'], 'id')));

        if ($deletedQuotes) {
            $this->eventQuoteRepository->batchDelete($deletedQuotes);
        }

        foreach ($data['quotes'] as $quote) {
            if (!empty($quote['id'])) {
                $this->eventQuoteRepository->update($quote['id'], [
                    'content' => $quote['content'],
                ]);
            } elseif (!empty($quote['content'])) {
                $this->eventQuoteRepository->create([
                    'event_id' => $event->id,
                    'content'  => $quote['content'],
                ]);
            }
        }

        $this->handleImages($event, $data['gallery'] ?? [], EventImage::CATEGORY_GALLERY);
        $this->handleImages($event, $data['artist_line_up'] ?? [], EventImage::CATEGORY_ARTIST_LINE_UP);

        $this->actionLogService->eventLog($event->id, ActionLog::ACTION_EVENT_UPDATED);

        return $event->fresh();
    }

    /**
     * @param Event $event
     * @param array $data
     *
     * @return bool
     */
    public function updateTicketsInfo(Event $event, array $data): bool
    {
        if (!isset($data['max_purchase_tickets_quantity'])) {
            $data['is_packages_tab_visible'] = 0;
        }

        return $this->eventRepository->update($event->id, $data);
    }

    /**
     * @param Event $event
     * @param array $data
     *
     * @return bool
     */
    public function updatePackagesInfo(Event $event, array $data): bool
    {
        if (!isset($data['is_packages_tab_visible'])) {
            $data['is_packages_tab_visible'] = 0;
        }

        return $this->eventRepository->update($event->id, $data);
    }

    /**
     * @param Event      $event
     * @param array|null $data
     *
     * @return array
     * @throws \Exception
     */
    public function updateFaqs(Event $event, ?array $data = []): array
    {
        if (!$data) {
            $data = [];
        }

        $currentFaqsIds = $event->faqs->pluck('id')->toArray();
        $deletedFaqs = array_diff($currentFaqsIds, array_filter(array_column($data, 'id')));

        $result = [];

        if ($deletedFaqs) {
            $this->eventFaqRepository->batchDelete($deletedFaqs);
        }

        $lastFaq = $event->faqs->last();
        $nextPosition = $lastFaq ? ++$lastFaq->pos : 0;

        foreach ($data as $key => $faq) {
            if (empty($faq['id'])) {
                $faq['pos'] = $nextPosition++;
            }

            $faq['event_id'] = $event->id;

            $saved = $this->eventFaqService->save($faq);

            if ($saved) {
                $result[$key] = $saved;
            }
        }

        return $result;
    }

    /**
     * @param array  $ids
     * @param string $status
     *
     * @return bool|null
     */
    public function updateStatuses(array $ids, string $status): ?bool
    {
        $result = false;

        $events = $this->eventRepository->findByIds($ids);

        foreach ($events as $event) {
            if ($event->status === $status) {
                continue;
            }

            $result = $this->eventRepository->update($event->id, ['status' => $status]);

            if ($result) {
                $this->actionLogService->eventLog($event->id, ActionLog::ACTION_STATUS_CHANGED, $status);
            }
        }

        return $result;
    }

    /**
     * @param Event  $event
     * @param array  $images
     * @param string $category
     *
     * @return void
     * @throws \Exception
     */
    private function handleImages(Event $event, array $images, string $category): void
    {
        $images = array_filter($images);
        $currentImages = $event->images->where('category', $category);

        foreach ($currentImages as $image) {
            $key = array_search($image->path, $images);

            if ($key === false) {
                $this->eventImageService->destroy($image->id);
            } else {
                unset($images[$key]);
            }
        }

        foreach ($images as $image) {
            $imageData = [
                'event_id' => $event->id,
                'category' => $category,
                'path'     => $image
            ];

            $this->eventImageService->create($imageData);
        }
    }

    /**
     * @param Event $event
     * @param array $data
     *
     * @return Ticket|null
     */
    public function saveTicket(Event $event, array $data): ?Ticket
    {
        $data['event_id'] = $event->id;
        $data['company_id'] = $event->company_id;

        return $this->ticketService->save($data);
    }

    /**
     * @param Event $event
     * @param array $data
     *
     * @return Ticket|null
     */
    public function savePackage(Event $event, array $data): ?EventPackage
    {
        $data['event_id'] = $event->id;
        $data['company_id'] = $event->company_id;

        return $this->eventPackageService->save($data);
    }

    /**
     * @param Event $event
     *
     * @return EventFaq
     */
    public function getEmptyFaq(Event $event): EventFaq
    {
        /** @var EventFaq $faq */
        $faq = $this->eventFaqRepository->newInstance();

        $lastFaq = $event->faqs->last();
        $faq->pos = $lastFaq ? ++$lastFaq->pos : 0;

        return $faq;
    }
}
