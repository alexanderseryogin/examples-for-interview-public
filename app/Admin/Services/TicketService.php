<?php

namespace App\Admin\Services;

use App\Admin\Repositories\EventRepository;
use App\Admin\Repositories\TicketRepository;
use App\Admin\Traits\Services\HasImage;
use App\Admin\Traits\Services\Positionable;
use App\Helpers\AuthHelper;
use App\Helpers\DateHelper;
use App\Models\Event;
use App\Models\Ticket;

/**
 * Class TicketService
 *
 * @package App\Admin\Services
 */
class TicketService
{
    use Positionable, HasImage;
    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var TicketRepository
     */
    protected $ticketRepository;
    /**
     * @var ImageService
     */
    private $imageService;

    /**
     * TicketService constructor.
     *
     * @param TicketRepository $ticketRepository
     * @param ImageService     $imageService
     * @param EventRepository  $eventRepository
     */
    public function __construct(TicketRepository $ticketRepository, ImageService $imageService, EventRepository $eventRepository)
    {
        $this->imageService = $imageService;
        $this->ticketRepository = $ticketRepository;
        $this->eventRepository = $eventRepository;
    }

    /**
     * @return TicketRepository
     */
    protected function getPositionableRepository(): TicketRepository
    {
        return $this->ticketRepository;
    }

    /**
     * @return string
     */
    protected function getModelFolder(): string
    {
        return Ticket::FOLDER;
    }

    /**
     * @param array $data
     *
     * @return Ticket|null
     */
    public function create(array $data): ?Ticket
    {
        $ticketData = $this->prepareData($data);
        $ticketData['pos'] = $data['pos'] ?? 0;

        $ticket = $this->ticketRepository->create($ticketData);

        if (!$ticket) {
            return null;
        }

        $this->reorderAfterInsert($ticket);

        $this->handleImage($ticket, $data['seat_plan'] ?? null, 'seat_plan');
        $this->handleImage($ticket, $data['header_image'] ?? null, 'header_image');

        $ticket->save();

        return $ticket->fresh();
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function prepareData(array $data): array
    {
        $salesEndDate = DateHelper::generateDate($data['sales_end_date'], $data['sales_end_date_hour'], $data['sales_end_date_minute']);

        /** @var Event $event */
        $event = $this->eventRepository->find($data['event_id']);

        return [
            'name'           => $data['name'],
            'event_id'       => $data['event_id'],
            'checkout_url'   => $data['checkout_url'] ?? null,
            'category'       => $data['category'],
            'tier'           => $data['tier'],
            'currency_id'    => $data['currency_id'],
            'price'          => $data['price'],
            'has_discount'   => $data['has_discount'] ?? 0,
            'discount_price' => $data['discount_price'] ?? 0,
            'description'    => $data['description'],
            'details'        => $data['details'],
            'status'         => $data['status'],
            'show_countdown' => $data['show_countdown'] ?? 0,
            'sales_end_date' => $salesEndDate,
            'creator_id'     => AuthHelper::id(),
            'company_id'     => $event->company_id
        ];
    }

    /**
     * @param Ticket $ticket
     * @param array  $data
     *
     * @return Ticket|null
     */
    public function update(Ticket $ticket, array $data): ?Ticket
    {
        $ticketData = $this->prepareData($data);

        $updated = $this->ticketRepository->update($ticket->id, $ticketData);

        if (!$updated) {
            return null;
        }

        $this->handleImage($ticket, $data['seat_plan'] ?? null, 'seat_plan');
        $this->handleImage($ticket, $data['header_image'] ?? null, 'header_image');

        $ticket->save();

        return $ticket->fresh();
    }

    /**
     * @param array $data
     *
     * @return Ticket|null
     */
    public function save(array $data): ?Ticket
    {
        if (!empty($data['id'])) {
            $ticket = $this->ticketRepository->find($data['id']);

            if (!$ticket) {
                return null;
            }

            unset($data['id']);

            return $this->update($ticket, $data);
        } else {
            return $this->create($data);
        }
    }

    /**
     *
     * @param int $id
     *
     * @return bool
     * @throws \Exception
     */
    public function destroy(int $id): bool
    {
        $deleted = $this->ticketRepository->delete($id);

        if ($deleted) {
            $this->deleteImages($id);
        }

        return $deleted;
    }

    /**
     * @param array $ids
     *
     * @return int
     * @throws \Exception
     */
    public function batchDestroy(array $ids): int
    {
        $deleted = $this->ticketRepository->batchDelete($ids);

        if ($deleted) {
            foreach ($ids as $id) {
                $this->deleteImages($id);
            }
        }

        return $deleted;
    }
}
