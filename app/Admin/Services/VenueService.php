<?php

namespace App\Admin\Services;

use App\Admin\Repositories\VenueRepository;
use App\Admin\Traits\Services\HasImage;
use App\Helpers\AuthHelper;
use App\Models\ActionLog;
use App\Models\Venue;
use Illuminate\Support\Arr;

/**
 * Class VenueService
 *
 * @package App\Admin\Services
 */
class VenueService
{
    use HasImage;
    /**
     * @var VenueRepository
     */
    private $venueRepository;
    /**
     * @var ActionLogService
     */
    private $actionLogService;

    /**
     * VenueService constructor.
     *
     * @param VenueRepository  $venueRepository
     * @param ActionLogService $actionLogService
     */
    public function __construct(VenueRepository $venueRepository, ActionLogService $actionLogService)
    {
        $this->venueRepository = $venueRepository;
        $this->actionLogService = $actionLogService;
    }

    /**
     * @return string
     */
    protected function getModelFolder(): string
    {
        return Venue::FOLDER;
    }

    /**
     * @param array $params
     *
     * @return Venue
     */
    public function create(array $params): Venue
    {
        $data = Arr::except($params, 'seating_layout_image');

        $data['creator_id'] = AuthHelper::id();
        $data['company_id'] = AuthHelper::companyId();

        $venue = $this->venueRepository->create($data);

        if (!$venue) {
            return null;
        }

        $this->handleImage($venue, $params['seating_layout_image'] ?? null, 'seating_layout_image');

        $venue->save();

        $this->actionLogService->venueLog($venue->id, ActionLog::ACTION_VENUE_CREATED);

        return $venue;
    }

    /**
     * @param Venue $venue
     * @param array $params
     *
     * @return Venue|null
     */
    public function update(Venue $venue, array $params): ?Venue
    {
        $data = Arr::except($params, 'seating_layout_image');

        $updated = $this->venueRepository->update($venue->id, $data);

        if (!$updated) {
            return null;
        }

        $this->handleImage($venue, $params['seating_layout_image'] ?? null, 'seating_layout_image');

        $venue->save();

        $this->actionLogService->venueLog($venue->id, ActionLog::ACTION_VENUE_UPDATED);

        return $venue->fresh();
    }

    /**
     *
     * @param int $id
     *
     * @return bool
     * @throws \Exception
     */
    public function destroy(int $id): bool
    {
        $deleted = $this->venueRepository->delete($id);

        if ($deleted) {
            $this->deleteImages($id);
            $this->actionLogService->venueLog($id, ActionLog::ACTION_VENUE_DELETED);
        }

        return $deleted;
    }

    /**
     * @param array $ids
     *
     * @return int
     * @throws \Exception
     */
    public function batchDestroy(array $ids): int
    {
        $deleted = $this->venueRepository->batchDelete($ids);

        if ($deleted) {
            foreach ($ids as $id) {
                $this->deleteImages($id);
                $this->actionLogService->venueLog($id, ActionLog::ACTION_VENUE_DELETED);
            }
        }

        return $deleted;
    }
}