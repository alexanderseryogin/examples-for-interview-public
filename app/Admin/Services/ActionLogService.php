<?php

namespace App\Admin\Services;

use App\Admin\Repositories\ActionLogRepository;
use App\Helpers\AuthHelper;
use App\Models\ActionLog;

/**
 * Class ActionLogService
 *
 * @package App\Admin\Services
 */
class ActionLogService
{
    /**
     * @var ActionLogRepository
     */
    private $actionLogRepository;

    /**
     * ActionLogService constructor.
     *
     * @param ActionLogRepository $actionLogRepository
     */
    public function __construct(ActionLogRepository $actionLogRepository)
    {
        $this->actionLogRepository = $actionLogRepository;
    }

    /**
     * @param int         $itemId
     * @param string      $action
     * @param string|null $value
     */
    public function eventLog(int $itemId, string $action, ?string $value = null): void
    {
        $this->log([
            'item_id'     => $itemId,
            'item_type'   => ActionLog::ITEM_EVENT,
            'action'      => $action,
            'description' => $this->getActionDescription(ActionLog::ITEM_EVENT, $action, $value),
        ]);
    }

    /**
     * @param int         $itemId
     * @param string      $action
     * @param string|null $value
     */
    public function venueLog(int $itemId, string $action, ?string $value = null): void
    {
        $this->log([
            'item_id'     => $itemId,
            'item_type'   => ActionLog::ITEM_VENUE,
            'action'      => $action,
            'description' => $this->getActionDescription(ActionLog::ITEM_VENUE, $action, $value),
        ]);
    }

    /**
     * @param int         $itemId
     * @param string      $action
     * @param string|null $value
     */
    public function roleLog(int $itemId, string $action, ?string $value = null): void
    {
        $this->log([
            'item_id'     => $itemId,
            'item_type'   => ActionLog::ITEM_ROLE,
            'action'      => $action,
            'description' => $this->getActionDescription(ActionLog::ITEM_ROLE, $action, $value),
        ]);
    }

    /**
     * @param int         $itemId
     * @param string      $action
     * @param string|null $value
     */
    public function userLog(int $itemId, string $action, ?string $value = null): void
    {
        $this->log([
            'item_id'     => $itemId,
            'item_type'   => ActionLog::ITEM_USER,
            'action'      => $action,
            'description' => $this->getActionDescription(ActionLog::ITEM_USER, $action, $value),
        ]);
    }

    private function log(array $params): void
    {
        $params = array_merge($params, [
            'user_id' => AuthHelper::id(),
            'ip'      => AuthHelper::ip(),
        ]);

        $this->actionLogRepository->create($params);
    }

    /**
     * @param string      $item
     * @param string      $action
     * @param string|null $value
     *
     * @return string
     */
    private function getActionDescription(string $item, string $action, ?string $value = null): string
    {
        $descriptions = [
            ActionLog::ITEM_EVENT => [
                ActionLog::ACTION_STATUS_CHANGED => 'changed status to ' . $value,
                ActionLog::ACTION_EVENT_UPDATED  => 'updated event',
                ActionLog::ACTION_EVENT_CREATED  => 'created event',
            ],
            ActionLog::ITEM_VENUE => [
                ActionLog::ACTION_VENUE_CREATED  => 'created venue',
                ActionLog::ACTION_VENUE_UPDATED  => 'updated venue',
                ActionLog::ACTION_VENUE_DELETED  => 'deleted venue',
            ],
            ActionLog::ITEM_ROLE => [
                ActionLog::ACTION_ROLE_CREATED  => 'created role ' . $value,
                ActionLog::ACTION_ROLE_UPDATED  => 'updated role ' . $value,
                ActionLog::ACTION_ROLE_DELETED  => 'deleted role ' . $value,
            ],
            ActionLog::ITEM_USER => [
                ActionLog::ACTION_USER_CREATED  => 'created user ' . $value,
                ActionLog::ACTION_USER_UPDATED  => 'updated user ' . $value,
                ActionLog::ACTION_USER_DELETED  => 'deleted user ' . $value,
            ]
        ];

        return $descriptions[$item][$action] ?? '';
    }
}
