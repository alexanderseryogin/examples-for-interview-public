<?php

namespace App\Admin\Transformers;

use App\Models\EventPackageBlock;
use Illuminate\Support\Collection;

/**
 * Trait EventPackageBlockTransformer
 *
 * @package App\Admim\Transformers
 */
trait EventPackageBlockTransformer
{
    /**
     * @param EventPackageBlock|Collection $data
     *
     * @return EventPackageBlock|Collection
     */
    public function transform($data)
    {
        if ($data instanceof Collection) {
            return $data->map(function ($item) {
                return $this->transform($item);
            });
        }

        /** @var EventPackageBlock $data */
        $data->iconUrl = $data->getIconUrlAttribute();

        return $data;
    }
}
