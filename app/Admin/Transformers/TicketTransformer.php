<?php

namespace App\Admin\Transformers;

use App\Models\Ticket;
use Illuminate\Support\Collection;

/**
 * Trait TicketTransformer
 *
 * @package App\Admim\Transformers
 */
trait TicketTransformer
{
    /**
     * @param Ticket|Collection $data
     *
     * @return Ticket|Collection
     */
    public function transform($data)
    {
        if ($data instanceof Collection) {
            return $data->map(function ($item) {
                return $this->transform($item);
            });
        }

        /** @var Ticket $data */
        $data->salesEndDateHour = $data->getSalesEndDateHourAttribute();
        $data->salesEndDateMinute = $data->getSalesEndDateMinuteAttribute();
        $data->seatPlanUrl = $data->getSeatPlanUrlAttribute();
        $data->headerImageUrl = $data->getHeaderImageUrlAttribute();

        return $data;
    }
}
