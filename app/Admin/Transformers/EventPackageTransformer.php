<?php

namespace App\Admin\Transformers;

use App\Models\EventPackage;
use Illuminate\Support\Collection;

/**
 * Trait EventPackageTransformer
 *
 * @package App\Admim\Transformers
 */
trait EventPackageTransformer
{
    use EventPackageBlockTransformer {
        EventPackageBlockTransformer::transform as blockTransform;
    }

    /**
     * @param EventPackage|Collection $data
     *
     * @return EventPackage|Collection
     */
    public function transform($data)
    {
        if ($data instanceof Collection) {
            return $data->map(function ($item) {
                return $this->transform($item);
            });
        }

        /** @var EventPackage $data */
        $data->cardImageUrl = $data->getCardImageUrlAttribute();
        $data->headerImageUrl = $data->getHeaderImageUrlAttribute();

        foreach ($data->blocks as $block) {
            $this->blockTransform($block);
        }

        return $data;
    }
}
