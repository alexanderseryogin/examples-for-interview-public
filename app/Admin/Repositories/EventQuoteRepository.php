<?php

namespace App\Admin\Repositories;

use App\Models\EventQuote;
use App\Repositories\BaseRepository;

/**
 * Class EventQuotesRepository
 *
 * @package App\Admin\Repositories
 */
class EventQuoteRepository extends BaseRepository
{
    /**
     * EventQuotesRepository constructor.
     *
     * @param EventQuote $eventQuote
     */
    public function __construct(EventQuote $eventQuote)
    {
        $this->model = $eventQuote;
    }
}