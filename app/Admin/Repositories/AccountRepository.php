<?php

namespace App\Admin\Repositories;

use App\Models\Account;
use App\Repositories\BaseRepository;

class AccountRepository extends BaseRepository
{
    /**
     * AccountRepository constructor.
     *
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->model = $account;
    }
}