<?php

namespace App\Admin\Repositories;

use App\Models\EventCategory;
use App\Repositories\BaseRepository;

/**
 * Class EventCategoryRepository
 *
 * @package App\Admin\Repositories
 */
class EventCategoryRepository extends BaseRepository
{
    /**
     * EventCategoryRepository constructor.
     *
     * @param EventCategory $eventCategory
     */
    public function __construct(EventCategory $eventCategory)
    {
        $this->model = $eventCategory;
    }
}
