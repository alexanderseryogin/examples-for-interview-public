<?php

namespace App\Admin\Repositories;

use App\Admin\Transformers\EventPackageBlockTransformer;
use App\Models\EventPackageBlock;
use App\Repositories\BaseRepository;

/**
 * Class EventPackageBlockRepository
 *
 * @package App\Admin\Repositories
 */
class EventPackageBlockRepository extends BaseRepository
{
    use EventPackageBlockTransformer;

    /**
     * EventPackageBlockRepository constructor.
     *
     * @param EventPackageBlock $eventPackageBlock
     */
    public function __construct(EventPackageBlock $eventPackageBlock)
    {
        $this->model = $eventPackageBlock;
    }
}