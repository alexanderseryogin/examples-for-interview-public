<?php

namespace App\Admin\Repositories;

use App\Admin\Traits\Lists\Filterable;
use App\Admin\Traits\Lists\Groupable;
use App\Admin\Traits\Lists\Pageable;
use App\Models\Invite;
use App\Repositories\BaseRepository;
use Illuminate\Database\Query\JoinClause;

/**
 * Class InviteRepository
 *
 * @package App\Admin\Repositories
 */
class InviteRepository extends BaseRepository
{
    use Filterable, Groupable, Pageable;
    /**
     * @var array
     */
    protected $searchWhereColumns = [
        'invites.email',
        'companies.name'
    ];

    /**
     * InviteRepository constructor.
     *
     * @param Invite $invite
     */
    public function __construct(Invite $invite)
    {
        $this->model = $invite;

        $this->filterWhereColumns = [
            'email'        => 'invites.email',
            'role_name'    => 'roles.name',
            'company_name' => 'companies.name'
        ];
    }

    /**
     * Return paginated results of the given model from the database
     *
     * @param array $params
     *
     * @return array
     */
    public function kendoList(array $params = []): array
    {
        $query = $this->newQuery();

        $query->leftJoin('companies', 'invites.company_id', '=', 'companies.id');
        $query->leftJoin(
            'model_has_roles',
            function (JoinClause $join) {
                $join->on('model_has_roles.model_id', '=', 'invites.id');
                $join->where('model_type', Invite::class);
            }
        );
        $query->leftJoin('roles', 'roles.id', '=', 'model_has_roles.role_id');

        if (!empty($params['q'])) {
            $query = $this->applySearch($query, $params['q']);
        }

        $this->applyFilters($query, $params['filter']['filters'] ?? []);

        $grouping = $params['group'] ?? [];

        $this->applyGrouping($query, $grouping);

        $orderBy = $params['sort'][0]['field'] ?? 'invites.id';
        $order = $params['sort'][0]['dir'] ?? 'desc';

        $query->orderBy($orderBy, $order);

        $total = $query->count();

        $this->applyPagination($query, $params);

        $items = $query
            ->select([
                'invites.id',
                'invites.email',
                'companies.name as company_name',
                'roles.name as role_name'
            ])
            ->groupBy('invites.id')
            ->get();

        if ($grouping) {
            $items = $this->buildGroupedItems($items, array_column($grouping, 'field'));
        }

        return [
            'items' => $items,
            'total' => $total
        ];
    }
}