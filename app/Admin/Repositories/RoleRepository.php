<?php

namespace App\Admin\Repositories;

use App\Models\Role;
use App\Repositories\BaseRepository;

class RoleRepository extends BaseRepository
{
    /**
     * RoleRepository constructor.
     *
     * @param Role $role
     */
    public function __construct(Role $role)
    {
        $this->model = $role;
    }

    /**
     * @return array
     */
    public function list(): array
    {
        $roles = [];
        $this->allSimple()->each(function ($item, $key) use (&$roles) {
            array_push($roles, ['id' => $key, 'name' => $item]);
        });

        return $roles;
    }
}