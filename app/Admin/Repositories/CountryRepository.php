<?php

namespace App\Admin\Repositories;

use App\Models\Country;
use App\Repositories\BaseRepository;

class CountryRepository extends BaseRepository
{
    /**
     * EventListViewRepository constructor.
     *
     * @param Country $country
     */
    public function __construct(Country $country)
    {
        $this->model = $country;
    }
}