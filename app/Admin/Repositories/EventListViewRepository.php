<?php

namespace App\Admin\Repositories;

use App\Helpers\AuthHelper;
use App\Models\EventListView;
use App\Repositories\BaseRepository;
use Illuminate\Support\Collection;

/**
 * Class EventListViewRepository
 *
 * @package App\Admin\Repositories
 */
class EventListViewRepository extends BaseRepository
{
    /**
     * EventListViewRepository constructor.
     *
     * @param EventListView $eventListView
     */
    public function __construct(EventListView $eventListView)
    {
        $this->model = $eventListView;
    }

    /**
     * @return Collection|null
     */
    public function getForCurrentUser(): Collection
    {
        return $this->findBy(['user_id' => AuthHelper::id()]);
    }
}
