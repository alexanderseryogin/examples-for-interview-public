<?php

namespace App\Admin\Repositories;

use App\Models\ActionLog;
use App\Repositories\BaseRepository;

/**
 * Class ActionLogRepository
 *
 * @package App\Admin\Repositories
 */
class ActionLogRepository extends BaseRepository
{
    /**
     * ActionLogRepository constructor.
     *
     * @param ActionLog $event
     */
    public function __construct(ActionLog $event)
    {
        $this->model = $event;
    }
}
