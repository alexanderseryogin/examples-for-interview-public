<?php

namespace App\Admin\Repositories;

use App\Admin\Contracts\FilterableList as FilterableListContract;
use App\Admin\Traits\Lists\Filterable;
use App\Admin\Traits\Lists\Groupable;
use App\Helpers\AuthHelper;
use App\Models\Permission;
use App\Models\Venue;
use App\Repositories\BaseRepository;
use DB;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class VenueRepository
 *
 * @package App\Admin\Repositories
 */
class VenueRepository extends BaseRepository implements FilterableListContract
{
    use Filterable, Groupable;
    /**
     * @var array
     */
    protected $searchWhereColumns = [
        'venues.name',
        'CONCAT_WS(" ", venues.address1, venues.address2)',
        'CONCAT_WS(", ", venues.address1, venues.address2)'
    ];

    /**
     * EventListViewRepository constructor.
     *
     * @param Venue $venue
     */
    public function __construct(Venue $venue)
    {
        $this->model = $venue;

        $this->filterWhereColumns = [
            'name'         => 'venues.name',
            'street_name'  => 'CONCAT_WS(", ", venues.address1, venues.address2)',
            'postcode'     => 'venues.postcode',
            'city'         => 'venues.city',
            'country_name' => 'countries.name',
        ];

        $this->filterHavingColumns = [
            'events_count' => [
                'field'    => 'COUNT(events.id)',
                'operator' => self::OPERATOR_EQUAL,
                'type'     => self::FIELD_NUMBER
            ]
        ];
    }

    /**
     * Return paginated results of the given model from the database
     *
     * @param array $params
     *
     * @return array
     */
    public function kendoList(array $params = []): array
    {
        $query = $this->newQuery()
            ->select([
                'venues.id',
                'venues.name',
                DB::raw('CONCAT_WS(", ", venues.address1, venues.address2) as street_name'),
                'venues.postcode',
                'venues.city',
                'countries.name as country_name',
                DB::raw('COUNT(events.id) as events_count')
            ]);

        $query
            ->leftJoin('countries', 'countries.id', '=', 'venues.country_id')
            ->leftJoin('events', 'venues.id', '=', 'events.venue_id');

        if (!empty($params['q'])) {
            $query = $this->applySearch($query, $params['q']);
        }

        $this->limitAccess($query);

        $this->applyFilters($query, $params['filter']['filters'] ?? []);

        $orderBy = $params['sort'][0]['field'] ?? 'venues.id';
        $order = $params['sort'][0]['dir'] ?? 'desc';
        $pageSize = $params['page_size'] ?? $this->pageSize;
        $page = $params['page'] ?? 1;

        $grouping = $params['group'] ?? [];

        $this->applyGrouping($query, $grouping);

        $all = $query
            ->orderBy($orderBy, $order)
            ->groupBy('venues.id')
            ->get();

        $items = $all
            ->slice(($page - 1) * $pageSize, $pageSize);

        if ($grouping) {
            $items = $this->buildGroupedItems($items, array_column($grouping, 'field'));
        }

        return [
            'items' => $items->values(),
            'total' => $all->count()
        ];
    }

    /**
     * @param $query
     */
    private function limitAccess(Builder $query): void
    {
        if (!AuthHelper::can(Permission::GLOBAL_VENUES_MANAGEMENT)) {
            $query->where('venues.company_id', AuthHelper::companyId());
        }
    }
}