<?php

namespace App\Admin\Repositories;

use App\Admin\Transformers\EventPackageTransformer;
use App\Models\EventPackage;
use App\Repositories\BaseRepository;

/**
 * Class EventPackageRepository
 *
 * @package App\Admin\Repositories
 */
class EventPackageRepository extends BaseRepository
{
    use EventPackageTransformer;

    /**
     * EventPackageRepository constructor.
     *
     * @param EventPackage $eventPackage
     */
    public function __construct(EventPackage $eventPackage)
    {
        $this->model = $eventPackage;
    }
}