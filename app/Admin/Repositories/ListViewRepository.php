<?php

namespace App\Admin\Repositories;

use App\Helpers\AuthHelper;
use App\Models\ListView;
use App\Repositories\BaseRepository;
use Illuminate\Support\Collection;

/**
 * Class ListViewRepository
 *
 * @package App\Admin\Repositories
 */
class ListViewRepository extends BaseRepository
{
    /**
     * ListViewRepository constructor.
     *
     * @param ListView $listView
     */
    public function __construct(ListView $listView)
    {
        $this->model = $listView;
    }

    /**
     * @param string $type
     *
     * @return Collection|null
     */
    public function getForCurrentUser(string $type): Collection
    {
        return $this->findBy(['user_id' => AuthHelper::id(), 'type' => $type]);
    }
}
