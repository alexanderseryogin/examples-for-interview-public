<?php

namespace App\Admin\Repositories;

use App\Models\Company;
use App\Repositories\BaseRepository;

class CompanyRepository extends BaseRepository
{
    /**
     * EventListViewRepository constructor.
     *
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->model = $company;
    }
}