<?php

namespace App\Admin\Repositories;

use App\Models\EventInfoBlock;
use App\Repositories\BaseRepository;

/**
 * Class EventInfoBlockRepository
 *
 * @package App\Admin\Repositories
 */
class EventInfoBlockRepository extends BaseRepository
{
    /**
     * EventInfoBlockRepository constructor.
     *
     * @param EventInfoBlock $eventInfoBlock
     */
    public function __construct(EventInfoBlock $eventInfoBlock)
    {
        $this->model = $eventInfoBlock;
    }
}