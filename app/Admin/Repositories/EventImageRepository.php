<?php

namespace App\Admin\Repositories;

use App\Models\EventImage;
use App\Repositories\BaseRepository;

/**
 * Class EventImageRepository
 *
 * @package App\Admin\Repositories
 */
class EventImageRepository extends BaseRepository
{
    /**
     * EventImageRepository constructor.
     *
     * @param EventImage $eventImage
     */
    public function __construct(EventImage $eventImage)
    {
        $this->model = $eventImage;
    }
}