<?php

namespace App\Admin\Repositories;

use App\Models\EventFaq;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class EventFaqRepository
 *
 * @package App\Admin\Repositories
 */
class EventFaqRepository extends BaseRepository
{
    /**
     * EventFaqRepository constructor.
     *
     * @param EventFaq $eventFaq
     */
    public function __construct(EventFaq $eventFaq)
    {
        $this->model = $eventFaq;
    }

    public function allForCompany(int $companyId, string $search)
    {
        return $this
            ->newQuery()
            ->whereHas('event', function (Builder $query) use ($companyId) {
                $query->where('events.company_id', $companyId);
            })
            ->where('title', 'like', "%$search%")
            ->where('is_template', 1)
            ->orderBy('title')
            ->get();
    }
}