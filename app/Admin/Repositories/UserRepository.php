<?php

namespace App\Admin\Repositories;

use App\Admin\Contracts\FilterableList as FilterableListContract;
use App\Admin\Traits\Lists\Filterable;
use App\Admin\Traits\Lists\Groupable;
use App\Admin\Traits\Lists\Pageable;
use App\Models\User;
use App\Repositories\BaseRepository;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;

/**
 * Class UserRepository
 *
 * @package App\Admin\Repositories
 */
class UserRepository extends BaseRepository implements FilterableListContract
{
    use Filterable, Groupable, Pageable;
    /**
     * @var array
     */
    protected $searchWhereColumns = [
        'CONCAT(accounts.first_name, " ", accounts.last_name)',
        'accounts.email',
        'companies.name'
    ];

    /**
     * UserRepository constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;

        $this->filterWhereColumns = [
            'first_name'     => 'accounts.first_name',
            'last_name'      => 'accounts.last_name',
            'email'          => 'accounts.email',
            'role'           => 'roles.id',
            'company'        => 'companies.name',
            'last_active_at' => 'DATE(users.last_active_at)',
            'status'         => 'users.status'
        ];
    }

    public function allNames(): Collection
    {
        $users = $this->with(['account'])->get();

        return $users->map(function ($item) {
            $item->full_name = $item->account->full_name;

            return $item;
        })
            ->pluck('full_name', 'id');
    }

    /**
     * Return paginated results of the given model from the database
     *
     * @param array $params
     *
     * @return array
     */
    public function kendoList(array $params = []): array
    {
        $query = $this->newQuery();

        $query->leftJoin('accounts', 'users.id', '=', 'accounts.user_id');
        $query->leftJoin('companies', 'users.company_id', '=', 'companies.id');
        $query->leftJoin(
            'model_has_roles',
            function (JoinClause $join) {
                $join->on('model_has_roles.model_id', '=', 'users.id');
                $join->where('model_type', User::class);
            }
        );
        $query->leftJoin('roles', 'roles.id', '=', 'model_has_roles.role_id');

        if (!empty($params['q'])) {
            $query = $this->applySearch($query, $params['q']);
        }

        $this->applyFilters($query, $params['filter']['filters'] ?? []);

        $grouping = $params['group'] ?? [];

        $this->applyGrouping($query, $grouping);

        $orderBy = $params['sort'][0]['field'] ?? 'users.id';
        $order = $params['sort'][0]['dir'] ?? 'desc';

        $query->orderBy($orderBy, $order);

        $total = $query->count();

        $this->applyPagination($query, $params);

        $items = $query
            ->select([
                'users.id',
                'users.last_active_at',
                'users.email',
                'accounts.first_name',
                'accounts.last_name',
                'users.status',
                'companies.name as company',
                'companies.id as company_id',
                'roles.name as role_name',
                'roles.id as role'
            ])
            ->groupBy('users.id')
            ->get();

        if ($grouping) {
            $items = $this->buildGroupedItems($items, array_column($grouping, 'field'));
        }

        return [
            'items' => $items,
            'total' => $total
        ];
    }
}