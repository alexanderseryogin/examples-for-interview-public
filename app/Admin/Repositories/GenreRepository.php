<?php

namespace App\Admin\Repositories;

use App\Models\Genre;
use App\Repositories\BaseRepository;

class GenreRepository extends BaseRepository
{
    /**
     * EventListViewRepository constructor.
     *
     * @param Genre $genre
     */
    public function __construct(Genre $genre)
    {
        $this->model = $genre;
    }
}