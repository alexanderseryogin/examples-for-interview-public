<?php

namespace App\Admin\Repositories;

use App\Models\Team;
use App\Repositories\BaseRepository;

class TeamRepository extends BaseRepository
{
    /**
     * EventListViewRepository constructor.
     *
     * @param Team $team
     */
    public function __construct(Team $team)
    {
        $this->model = $team;
    }
}