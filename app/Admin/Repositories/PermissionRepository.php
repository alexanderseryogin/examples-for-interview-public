<?php

namespace App\Admin\Repositories;

use App\Models\Permission;
use App\Repositories\BaseRepository;

class PermissionRepository extends BaseRepository
{
    /**
     * PermissionRepository constructor.
     *
     * @param Permission $role
     */
    public function __construct(Permission $role)
    {
        $this->model = $role;
    }
}