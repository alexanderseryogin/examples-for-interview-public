<?php

namespace App\Admin\Repositories;

use App\Admin\Contracts\FilterableList;
use App\Admin\Contracts\Transformer;
use App\Admin\Traits\Lists\Filterable;
use App\Admin\Traits\Lists\Groupable;
use App\Admin\Traits\Lists\Pageable;
use App\Admin\Transformers\TicketTransformer;
use App\Helpers\AuthHelper;
use App\Models\Permission;
use App\Models\Ticket;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use DB;

/**
 * Class TicketRepository
 *
 * @package App\Admin\Repositories
 */
class TicketRepository extends BaseRepository implements Transformer, FilterableList
{
    use TicketTransformer, Filterable, Groupable, Pageable;
    /**
     * @var array
     */
    protected $searchWhereColumns = [
        'tickets.name',
        'events.name'
    ];

    /**
     * TicketRepository constructor.
     *
     * @param Ticket $ticket
     */
    public function __construct(Ticket $ticket)
    {
        $this->model = $ticket;

        $this->filterWhereColumns = [
            'name'           => 'tickets.name',
            'event_name'     => 'events.name',
            'category'       => 'tickets.category',
            'currency'       => 'tickets.currency_id',
            'price'          => 'tickets.price',
            'discount_price' => 'tickets.discount_price',
            'status'         => 'tickets.status',
            'tier'           => 'tickets.tier',
            'sales_end_date' => 'tickets.sales_end_date'
        ];
    }

    /**
     * Return paginated results of the given model from the database
     *
     * @param array $params
     *
     * @return array
     */
    public function kendoList(array $params = []): array
    {
        $query = $this->newQuery();

        $query
            ->leftJoin('events', 'events.id', '=', 'tickets.event_id')
            ->leftJoin('currencies', 'currencies.id', '=', 'tickets.currency_id');

        if (!empty($params['q'])) {
            $query = $this->applySearch($query, $params['q']);
        }

        $this->limitAccess($query);

        $this->applyFilters($query, $params['filter']['filters'] ?? []);

        $grouping = $params['group'] ?? [];

        $this->applyGrouping($query, $grouping);

        $orderBy = $params['sort'][0]['field'] ?? 'id';
        $order = $params['sort'][0]['dir'] ?? 'desc';

        $query->orderBy($orderBy, $order);

        $total = $query->count();

        $this->applyPagination($query, $params);

        $items = $query
            ->select([
                'tickets.id',
                'tickets.name',
                'tickets.tier',
                'tickets.category',
                'tickets.status',
                'tickets.sales_end_date',
                'tickets.price',
                'tickets.discount_price',
                DB::raw('(tickets.quantity - tickets.sold_quantity) as remaining_quantity'),
                DB::raw('IF(tickets.quantity, ROUND((tickets.sold_quantity) / tickets.quantity * 100, 0), 100) as percentage_sold'),
                'events.name as event_name',
                'events.id as event_id',
                'currencies.code as currency',
            ])
            ->get();

        if ($grouping) {
            $items = $this->buildGroupedItems($items, array_column($grouping, 'field'));
        }

        return [
            'items' => $items,
            'total' => $total
        ];
    }

    /**
     * @param $query
     */
    private function limitAccess(Builder $query): void
    {
        if (!AuthHelper::can(Permission::GLOBAL_TICKETS_MANAGEMENT)) {
            $query->where('tickets.company_id', AuthHelper::companyId());
        }
    }
}