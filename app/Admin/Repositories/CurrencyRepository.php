<?php

namespace App\Admin\Repositories;

use App\Models\Currency;
use App\Repositories\BaseRepository;

/**
 * Class CurrencyRepository
 *
 * @package App\Admin\Repositories
 */
class CurrencyRepository extends BaseRepository
{
    /**
     * ActionLogRepository constructor.
     *
     * @param Currency $currency
     */
    public function __construct(Currency $currency)
    {
        $this->model = $currency;
    }
}
