<?php

namespace App\Admin\Repositories;

use App\Admin\Contracts\FilterableList as FilterableListContract;
use App\Admin\Traits\Lists\Filterable;
use App\Admin\Traits\Lists\Groupable;
use App\Admin\Traits\Lists\Pageable;
use App\Helpers\AuthHelper;
use App\Models\Event;
use App\Models\Permission;
use App\Repositories\BaseRepository;
use DB;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class EventRepository
 *
 * @package App\Admin\Repositories
 */
class EventRepository extends BaseRepository implements FilterableListContract
{
    use Filterable, Groupable, Pageable;
    /**
     * @var array
     */
    protected $searchWhereColumns = [
        'events.name',
        'venues.name',
        'venues.city',
        'companies.name'
    ];
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * EventRepository constructor.
     *
     * @param Event          $event
     * @param UserRepository $userRepository
     */
    public function __construct(Event $event, UserRepository $userRepository)
    {
        $this->model = $event;

        $this->filterWhereColumns = [
            'creator_id'   => 'users.id',
            'title'        => 'events.name',
            'status'       => 'events.status',
            'start_date'   => [
                'field' => 'events.start_date',
                'type'  => self::FIELD_DATE
            ],
            'start_time'   => [
                'field' => 'events.start_date',
                'type'  => self::FIELD_TIME
            ],
            'venue_name'   => 'venues.name',
            'venue_city'   => 'venues.city',
            'company_name' => 'companies.name',
        ];
        $this->userRepository = $userRepository;
    }

    /**
     * Return paginated results of the given model from the database
     *
     * @param array $params
     *
     * @return array
     */
    public function kendoList(array $params = []): array
    {
        $query = $this->newQuery();

        $query->leftJoin('venues', 'venues.id', '=', 'events.venue_id');
        $query->leftJoin('companies', 'companies.id', '=', 'events.organizer_id');

        if (!empty($params['q'])) {
            $query = $this->applySearch($query, $params['q']);
        }

        $this->limitAccess($query);

        $this->applyFilters($query, $params['filter']['filters'] ?? []);

        $grouping = $params['group'] ?? [];

        $this->applyGrouping($query, $grouping);

        $orderBy = $params['sort'][0]['field'] ?? 'id';
        $order = $params['sort'][0]['dir'] ?? 'desc';

        $query->orderBy($orderBy, $order);

        $total = $query->count(DB::raw('DISTINCT(events.id)'));

        $this->applyPagination($query, $params);

        $items = $query
            ->select([
                'events.*',
                'events.start_date as start_time',
                'venues.name as venue_name',
                'companies.name as company_name',
                'venues.city as venue_city',
            ])
            ->groupBy('events.id')
            ->get();

        if ($grouping) {
            $items = $this->buildGroupedItems($items, array_column($grouping, 'field'));
        }

        return [
            'items' => $items,
            'total' => $total
        ];
    }

    /**
     * Get events list created by used on user profile page
     *
     * @param array $params
     *
     * @return array
     */
    public function kendoUserEventList(array $params = []): array
    {
        $query = $this->newQuery()
            ->leftJoin('venues', 'events.venue_id', '=', 'venues.id');

        if (!empty($params['creator_id'])) {
            $query->where('events.creator_id', $params['creator_id']);
        }

        $this->limitAccess($query);

        $orderBy = $params['sort'][0]['field'] ?? 'events.id';
        $order = $params['sort'][0]['dir'] ?? 'desc';

        $query->orderBy($orderBy, $order);

        $total = $query->count();

        $this->applyPagination($query, $params);

        $items = $query
            ->select([
                'events.*',
                'venues.id as venue_id',
                DB::raw('CONCAT(venues.name, ", ", venues.city) AS venue_name'),
            ])
            ->get();

        return [
            'items' => $items,
            'total' => $total
        ];
    }

    /**
     * @param string $orderBy
     * @param string $sortBy
     * @param array  $relations
     * @param array  $columns
     *
     * @return mixed
     */
    public function all(string $orderBy = 'id', string $sortBy = 'asc', array $relations = [], array $columns = ['*'])
    {
        $query = $this->newQuery();

        $this->limitAccess($query);

        return $query->with($relations)->orderBy($orderBy, $sortBy)->get($columns);
    }

    /**
     * @param $query
     *
     * @return void
     */
    private function limitAccess(Builder $query): void
    {
        if (!AuthHelper::can(Permission::GLOBAL_EVENTS_MANAGEMENT)) {
            $query->where(function (Builder $query) {
                $query->where('events.company_id', AuthHelper::companyId());

                $accessEvents = AuthHelper::user()->accessEventsPivot()->pluck('event_id')->toArray();

                if ($accessEvents) {
                    $query->orWhereIn('events.id', $accessEvents);
                }
            });
        }
    }
}