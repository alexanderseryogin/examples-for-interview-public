<?php

namespace App\Listeners;

use App\Events\TicketSold;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordSuccessfulPayment
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TicketSold  $event
     * @return void
     */
    public function handle(TicketSold $event)
    {
        $event->order->payment()->create([

            'order_id' => $event->order->id,
            'failed' => false,
            'transaction_id' => $event->result->transaction->id

        ]);
    }
}
