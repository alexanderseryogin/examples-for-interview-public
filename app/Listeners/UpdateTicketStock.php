<?php

namespace App\Listeners;

use App\Events\TicketSold;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Order;

use App\Models\Ticket2;

class UpdateTicketStock
{


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  TicketSold  $event
     * @return void
     */
    public function handle(TicketSold $event)
    {
        $ticket_type_id = [];

        $tickets = $event->order->tickets()->get();

        foreach($tickets as $ticket)
        {
            $ticket_type_id[] = $ticket->ticket_type_id;
        }

        foreach ($ticket_type_id as $ticket_type) {
            $ticket = Ticket2::find($ticket_type);
            $ticket->stock = $ticket->stock - 1;
            $ticket->save();
        }

    }
}
