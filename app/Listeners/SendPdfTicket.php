<?php

namespace App\Listeners;

use App\Events\TicketSold;
use App\Mail\EventTicket;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendPdfTicket
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TicketSold  $event
     * @return void
     */
    public function handle(TicketSold $event)
    {

        if($event->order){
            $order = $event->order;
            $user = $order->customer()->firstOrFail();

            Mail::to($user->email)->send(new EventTicket($order));
        }
    }
}
