<?php

namespace App\Helpers;

use App\Models\User;
use Auth;

/**
 * Class AuthHelper
 *
 * @package App\Helpers
 */
abstract class AuthHelper
{
    /**
     * @return User|null
     */
    public static function user(): ?User
    {
        return Auth::user();
    }

    /**
     * @return int|null
     */
    public static function id(): ?int
    {
        return Auth::id();
    }

    /**
     * @return string|null
     */
    public static function ip(): ?string
    {
        return request()->ip();
    }

    /**
     * @return int|null
     */
    public static function companyId(): ?int
    {
        return Auth::user()->company_id;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public static function hasCmsAccess(): bool
    {
        return Auth::check();
    }

    /**
     * @param string $permission
     *
     * @return bool
     */
    public static function can(string $permission): bool
    {
        return self::user()->can($permission);
    }

    /**
     * @param string $role
     *
     * @return bool
     */
    public static function hasRole(string $role): bool
    {
        return self::user()->hasRole($role);
    }
}