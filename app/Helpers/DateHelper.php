<?php

namespace App\Helpers;

use Carbon\Carbon;

/**
 * Class DateHelper
 *
 * @package App\Helpers
 */
abstract class DateHelper
{
    /**
     * @return array
     */
    public static function hoursList(): array
    {
        return array_combine(range(0, 24), range(0, 24));
    }

    /**
     * @return array
     */
    public static function minutesList(): array
    {
        return array_combine(range(0, 45, 15), range(0, 45, 15));
    }

    /**
     * @param Carbon      $date
     * @param string|null $format
     *
     * @return string
     */
    public static function date(Carbon $date, string $format = null): string
    {
        return $date->format($format ?? self::dateFormat());
    }

    /**
     * @param Carbon      $date
     * @param string|null $format
     *
     * @return string
     */
    public static function dateTime(Carbon $date, string $format = null): string
    {
        return $date->format($format ?? self::dateTimeFormat());
    }

    /**
     * @return string
     */
    public static function dateFormat(): string
    {
        return config('app.date_format');
    }

    /**
     * @return string
     */
    public static function dateTimeFormat(): string
    {
        return config('app.datetime_format');
    }

    /**
     * @param string   $date
     * @param int|null $hours
     * @param int|null $minutes
     *
     * @return string|null
     */
    public static function generateDate(?string $date, ?int $hours = 0, ?int $minutes = 0): ?string
    {
        if (empty($date)) {
            return null;
        }

        return Carbon::parse($date)->addHours($hours ?: 0)->addMinutes($minutes ?: 0)->toDateTimeString();
    }
}