<?php

namespace App\Helpers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

/**
 * Class StorageHelper
 *
 * @package App\Helpers
 */
abstract class StorageHelper
{
    public const PUBLIC_DISK = 'public';

    public const TEMP_FOLDER = 'temp';

    /**
     * @param string|resource $context
     * @param string          $path
     * @param string          $disk
     *
     * @return bool
     */
    public static function put($context, string $path, string $disk = self::PUBLIC_DISK)
    {
        return Storage::disk($disk)->put($path, $context);
    }

    /**
     * @param UploadedFile $file
     * @param null|string  $folder
     * @param null|string  $name
     * @param bool         $autoSubFolder
     * @param string       $disk
     *
     * @return false|string
     */
    public static function uploadFile(
        UploadedFile $file,
        ?string $folder = null,
        bool $autoSubFolder = false,
        ?string $name = null,
        string $disk = self::PUBLIC_DISK
    ) {
        $name = is_null($name) ? self::randomName() : File::name($name);

        if ($autoSubFolder) {
            $folder .= '/' . self::prependWithSubFolders($name, true);
        }

        return $file->storeAs(
            $folder,
            $name . "." . $file->guessClientExtension(),
            ['disk' => $disk]
        );
    }

    /**
     * @param string      $url
     * @param string|null $folder
     * @param bool        $autoSubFolder
     * @param string|null $name
     * @param string      $disk
     *
     * @return bool|string
     */
    public static function downloadFile(
        string $url,
        ?string $folder = self::TEMP_FOLDER,
        bool $autoSubFolder = true,
        ?string $name = null,
        string $disk = self::PUBLIC_DISK
    ) {
        $contents = file_get_contents($url);
        if (is_null($name)) {
            $name = self::randomName();
        }
        if ($autoSubFolder) {
            $folder .= DIRECTORY_SEPARATOR . self::prependWithSubFolders($name, true);
        }
        $path = $folder . DIRECTORY_SEPARATOR . $name;
        if (self::put($contents, $path, $disk)) {
            return $path;
        } else {
            return false;
        }
    }

    /**
     * @param UploadedFile $file
     * @param string|null  $filename
     * @param null|string  $folder
     * @param string       $disk
     *
     * @return mixed
     */
    public static function safeUploadFile(
        UploadedFile $file,
        string $filename = null,
        ?string $folder = null,
        string $disk = self::PUBLIC_DISK
    ) {
        if (self::exists($filename, $disk, $folder)) {
            $filename = File::name($filename) . '_' . uniqid() . '.' . $file->guessClientExtension();
        }

        return self::uploadFile($file, $folder, false, $filename, $disk);
    }

    /**
     * @param UploadedFile $file
     *
     * @return false|string
     */
    public static function uploadTempFile(UploadedFile $file)
    {
        return self::uploadFile($file, StorageHelper::TEMP_FOLDER, true);
    }

    /**
     * @param string $path
     * @param string $disk
     *
     * @return false|string
     */
    public static function deleteFile(string $path, string $disk = self::PUBLIC_DISK)
    {
        return Storage::disk($disk)->delete($path);
    }

    /**
     * @param string      $from
     * @param string|null $folder
     * @param string|null $filename
     * @param string      $disk
     *
     * @return false|string
     */
    public static function copy(string $from, string $folder = null, ?string $filename = null, string $disk = self::PUBLIC_DISK)
    {
        $filename = is_null($filename) ? self::randomName(File::extension($from)) : File::basename($from);

        $to = $folder . '/' . $filename;

        $result = Storage::disk($disk)->copy($from, $folder . '/' . $filename);

        return $result ? $to : false;
    }

    /**
     * @param string      $from
     * @param string|null $folder
     * @param string|null $filename
     * @param string      $disk
     *
     * @return false|string
     */
    public static function move(string $from, string $folder = null, ?string $filename = null, string $disk = self::PUBLIC_DISK)
    {
        $filename = is_null($filename) ? self::randomName(File::extension($from)) : File::basename($from);

        $to = $folder . '/' . $filename;

        $result = Storage::disk($disk)->move($from, $folder . '/' . $filename);

        return $result ? $to : false;
    }

    /**
     * @param string      $path
     * @param string      $disk
     * @param null|string $folder
     *
     *
     * @return bool
     */
    public static function exists(string $path, string $disk = self::PUBLIC_DISK, ?string $folder = null): bool
    {
        $path = $folder ? $folder . '/' . $path : $path;

        return Storage::disk($disk)->exists($path);
    }

    /**
     * @param string      $path
     * @param string      $disk
     * @param null|string $folder
     *
     * @return string
     */
    public static function path(string $path, string $disk = self::PUBLIC_DISK, ?string $folder = null): string
    {
        $path = $folder ? $folder . '/' . $path : $path;

        return Storage::disk($disk)->path($path);
    }

    /**
     * @param string      $path
     * @param string      $disk
     * @param null|string $folder
     *
     * @return string
     */
    public static function url(string $path, string $disk = self::PUBLIC_DISK, ?string $folder = null): string
    {
        $path = $folder ? $folder . '/' . $path : $path;

        return Storage::disk($disk)->url($path);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public static function name(string $path): string
    {
        return pathinfo($path, PATHINFO_BASENAME);
    }

    /**
     * @param string $path
     * @param string $disk
     *
     * @return string
     */
    public static function createDirectory(string $path, string $disk = self::PUBLIC_DISK): string
    {
        return Storage::disk($disk)->makeDirectory($path);
    }

    /**
     * @param string $path
     * @param string $disk
     *
     * @return bool
     */
    public static function deleteDirectory(string $path, string $disk = self::PUBLIC_DISK): bool
    {
        return Storage::disk($disk)->deleteDirectory($path);
    }

    /**
     * @param string $path
     * @param bool   $recursive
     * @param string $disk
     *
     * @return array
     */
    public static function files(string $path, bool $recursive = true, string $disk = self::PUBLIC_DISK): array
    {
        return Storage::disk($disk)->files($path, $recursive);
    }

    /**
     * @param string $name
     * @param bool   $onlyFolder
     *
     * @return string
     */
    public static function prependWithSubFolders(string $name, bool $onlyFolder = false): string
    {
        $folder = substr($name, 0, 1);

        if (strlen($name) > 1) {
            $folder .= '/' . substr($name, 1, 1);
        }

        return $onlyFolder ? $folder : $folder . '/' . $name;
    }

    /**
     * @param string $path
     *
     * @return UploadedFile
     */
    public static function pathToUploadedFile(string $path): UploadedFile
    {
        $originalName = File::basename($path);
        $mimeType = File::mimeType($path);

        return new UploadedFile($path, $originalName, $mimeType, null, false);
    }

    /**
     * @param string|null $extension
     *
     * @return string
     */
    public static function randomName(?string $extension = null): string
    {
        $name = str_random(25);

        return $extension ? $name . '.' . $extension : $name;
    }
}