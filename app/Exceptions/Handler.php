<?php

namespace App\Exceptions;

use App\Helpers\ArrayHelper;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Log;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->expectsJson()) {
            switch (true) {
                case $exception instanceof AuthenticationException:
                    $message = $exception->getMessage();
                    $errors = [$exception->getMessage()];
                    $code = JsonResponse::HTTP_UNAUTHORIZED;
                    break;
                case $exception instanceof NotFoundHttpException:
                    $message = $exception->getMessage();
                    $errors = [$exception->getMessage()];
                    $code = JsonResponse::HTTP_NOT_FOUND;
                    break;
                case $exception instanceof ValidationException:
                    $message = $exception->getMessage();
                    $errors = $exception->errors();
                    $errors = ArrayHelper::dotToMulti($errors);
                    $code = JsonResponse::HTTP_UNPROCESSABLE_ENTITY;
                    break;
                case $exception instanceof MethodNotAllowedHttpException:
                    $message = $exception->getMessage();
                    $errors = [$exception->getMessage()];
                    $code = JsonResponse::HTTP_METHOD_NOT_ALLOWED;
                    break;
                case $exception instanceof BadRequestHttpException:
                    $message = $exception->getMessage();
                    $errors = [$exception->getMessage()];
                    $code = JsonResponse::HTTP_METHOD_NOT_ALLOWED;
                    break;
                case $exception instanceof UnauthorizedException:
                case $exception instanceof AuthorizationException:
                    $message = $exception->getMessage();
                    $errors = [$exception->getMessage()];
                    $code = JsonResponse::HTTP_FORBIDDEN;
                    break;
                default:
                    if (!false) {
                        return parent::render($request, $exception);
                    }
                    $message = $exception->getMessage();
                    $errors = [$exception->getMessage()];
                    $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
            }

            Log::info($exception->getMessage());

            return response()->error($errors, $message, $code);
        }

        if ($this->isHttpException($exception)) {
            switch ($exception->getStatusCode()) {
                // not found
                case 404:
                    return \Response::view('errors.404', [], 404);
                    break;
                // internal error
                case '500':
                    return \Response::view('error.500', [], 500);
                    break;

                default:
                    return $this->renderHttpException($exception);
                    break;
            }
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        $guard = array_get($exception->guards(), 0);
        switch ($guard) {
        case 'ultrasingapore':
            $login = 'ultrasingapore.login';
            break;
        default:
            $login = 'login';
            break;
        }

        return redirect()->guest(route($login));
    }
}
