<?php

namespace App\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

/**
 * Class BaseRepository
 *
 * @package App\Repositories
 */
abstract class BaseRepository
{
    /** @var int */
    protected $pageSize = 10;
    /** @var Model */
    protected $model;
    /** @var array */
    protected $searchWhereColumns = [];
    /** @var array */
    protected $searchHavingColumns = [];

    /**
     * @param array $attributes
     *
     * @return mixed
     */
    public function create(array $attributes)
    {
        return $this->newQuery()->create($attributes);
    }

    /**
     * @param array $attributes
     *
     * @param array $values
     *
     * @return mixed
     */
    public function updateOrCreate(array $attributes, array $values = [])
    {
        return $this->newQuery()->updateOrCreate($attributes, $values);
    }

    /**
     * @param array $attributes
     *
     * @param array $values
     *
     * @return mixed
     */
    public function firstOrCreate(array $attributes, array $values = [])
    {
        return $this->newQuery()->firstOrCreate($attributes, $values);
    }

    /**
     * @param int   $id
     * @param array $data
     *
     * @return bool
     */
    public function update(int $id, array $data): bool
    {
        return $this->find($id)->update($data);
    }

    /**
     * @param array $ids
     * @param array $data
     *
     * @return bool|null
     */
    public function batchUpdate(array $ids, array $data)
    {
        return $this->newQuery()->whereIn('id', $ids)->update($data);
    }

    /**
     * @param array $where
     * @param array $data
     *
     * @return bool|null
     */
    public function updateWhere(array $where, array $data)
    {
        return $this->newQuery()->where($where)->update($data);
    }

    /**
     * @param string $column
     * @param array  $values
     * @param array  $data
     *
     * @return int
     */
    public function updateWhereIn(string $column, array $values, array $data): int
    {
        return $this->newQuery()->whereIn($column, $values)->update($data);
    }

    /**
     * @param string $orderBy
     * @param string $sortBy
     * @param array  $relations
     * @param array  $columns
     *
     * @return mixed
     */
    public function all(string $orderBy = 'id', string $sortBy = 'asc', array $relations = [], array $columns = ['*'])
    {
        return $this->newQuery()->with($relations)->orderBy($orderBy, $sortBy)->get($columns);
    }

    /**
     *
     * @param string $field
     *
     * @return Collection
     */
    public function allSimple(string $field = 'name'): Collection
    {
        return $this->all($field, 'asc', [], ['id', $field])->pluck($field, 'id');
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function find(int $id)
    {
        return $this->newQuery()->find($id);
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function findOne(int $id)
    {
        return $this->find($id)->first();
    }

    /**
     * @param  int $id
     *
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findOneOrFail(int $id)
    {
        return $this->newQuery()->findOrFail($id);
    }

    /**
     * @param array $data
     *
     * @param array $relations
     *
     * @return Collection
     */
    public function findBy(array $data, array $relations = [])
    {
        return $this->newQuery()->with($relations)->where($data)->get();
    }

    /**
     * @param array $ids
     *
     * @return Collection
     */
    public function findByIds(array $ids)
    {
        return $this->newQuery()->whereIn('id', $ids)->get();
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function findOneBy(array $data)
    {
        return $this->newQuery()->where($data)->first();
    }

    /**
     * @param string $where
     *
     * @return mixed
     */
    public function findOneByRaw(string $where)
    {
        return $this->newQuery()->whereRaw($where)->first();
    }

    /**
     * @param array $data
     *
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findOneByOrFail(array $data)
    {
        return $this->newQuery()->where($data)->firstOrFail();
    }

    /**
     * Create a new Eloquent Query Builder instance
     *
     * @return Builder
     */
    public function newQuery(): Builder
    {
        return $this->model->newQuery();
    }

    /**
     * Load relation
     *
     * @param array $relations
     *
     * @return Builder
     */
    public function with(array $relations): Builder
    {
        return $this->newQuery()->with($relations);
    }

    /**
     * @param array $ids
     *
     * @return int
     * @throws \Exception
     */
    public function batchDelete(array $ids): int
    {
        return $this->newQuery()->whereIn('id', $ids)->delete();
    }

    /**
     * @param int $id
     *
     * @return bool|null
     * @throws \Exception
     */
    public function delete(int $id): ?bool
    {
        return $this->find($id)->delete();
    }

    /**
     *
     * @param array $where
     *
     * @return int
     */
    public function deleteWhere(array $where): int
    {
        return $this->newQuery()->where($where)->delete();
    }

    /**
     * @param Model $model
     *
     * @return bool|null
     * @throws \Exception
     */
    public function deleteModel(Model $model): ?bool
    {
        return $model->delete();
    }

    /**
     * @return void
     */
    public function truncate(): void
    {
        $this->newQuery()->truncate();
    }

    /**
     * @return Model
     */
    public function newInstance(): Model
    {
        return $this->model->newInstance();
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param array $data
     * @param int   $perPage
     *
     * @return LengthAwarePaginator
     */
    public function paginateArrayResults(array $data, int $perPage = 50)
    {
        $page = Input::get('page', 1);
        $offset = ($page * $perPage) - $perPage;

        return new \Illuminate\Pagination\LengthAwarePaginator(
            array_slice($data, $offset, $perPage, true),
            count($data),
            $perPage,
            $page,
            [
                'path'  => app('request')->url(),
                'query' => app('request')->query()
            ]
        );
    }

    /**
     * @param Builder     $query
     * @param array       $params
     * @param array       $relationships
     *
     * @param string|null $groupBy
     *
     * @return LengthAwarePaginator
     */
    protected function processList(Builder $query, array $params = [], array $relationships = [], ?string $groupBy = null): LengthAwarePaginator
    {
        if ($relationships) {
            $query->with($relationships);
        }

        if (!empty($params['q'])) {
            $query = $this->applySearch($query, $params['q']);
        }

        $orderBy = $params['order_by'] ?? 'id';
        $order = $params['order'] ?? 'desc';
        $perPage = $params['page_size'] ?? $this->pageSize;

        if ($groupBy) {
            $query->groupBy($groupBy);
        }

        return $query
            ->orderBy($orderBy, $order)
            ->paginate($perPage);
    }

    /**
     * @param Builder|QueryBuilder $query
     * @param string               $search
     *
     * @return Builder
     */
    protected function applySearch($query, string $search)
    {
        if (!$this->searchWhereColumns && !$this->searchHavingColumns) {
            return $query;
        }

        $query->where(function ($query) use ($search) {
            /* @var Builder $query */
            foreach ($this->searchWhereColumns as $column) {
                $query->orWhere(DB::raw($column), 'like', '%' . $search . '%');
            }
        });

        foreach ($this->searchHavingColumns as $column) {
            $query->orHaving($column, 'like', '%' . $search . '%');
        }

        return $query;
    }
}