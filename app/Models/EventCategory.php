<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Class Category
 *
 * @package App\Models
 * @property int                $id
 * @property string             $slug
 * @property string             $name
 * @property Collection|Event[] $events
 * @mixin \Eloquent
 */
class EventCategory extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return HasMany
     */
    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }
}
