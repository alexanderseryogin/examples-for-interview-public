<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Class ActionLog
 *
 * @package App\Models
 * @property int         $id
 * @property int         $user_id
 * @property int         $item_id
 * @property string      $item_type
 * @property string      $action
 * @property string      $description
 * @property Carbon|null $created_at
 * @property User        $user
 * @property Model       $item
 * @property string      $userFullName
 * @mixin \Eloquent
 */
class ActionLog extends Model
{
    const LOG_COUNT = 2;

    const ITEM_EVENT = 'App\Models\Event';

    const ITEM_VENUE = 'App\Models\Venue';

    const ITEM_ROLE = 'App\Models\Role';

    const ITEM_USER = 'App\Models\User';

    const ACTION_EVENT_CREATED = 'event_created';

    const ACTION_EVENT_UPDATED = 'event_updated';

    const ACTION_STATUS_CHANGED = 'status_changed';

    const ACTION_VENUE_CREATED = 'venue_created';

    const ACTION_VENUE_UPDATED = 'venue_updated';

    const ACTION_VENUE_DELETED = 'venue_deleted';

    const ACTION_ROLE_CREATED = 'role_created';

    const ACTION_ROLE_UPDATED = 'role_updated';

    const ACTION_ROLE_DELETED = 'role_deleted';

    const ACTION_USER_CREATED = 'user_created';

    const ACTION_USER_UPDATED = 'user_updated';

    const ACTION_USER_DELETED = 'user_deleted';

    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var array
     */
    protected $dates = ['created_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'item_id',
        'item_type',
        'action',
        'description',
        'ip'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function (ActionLog $model) {
            $model->created_at = $model->freshTimestamp();
        });
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get all of the owning item models.
     */
    public function item()
    {
        return $this->morphTo();
    }

    /**
     * @return string
     */
    public function getUserFullNameAttribute(): string
    {
        return $this->user->account->fullName ?? $this->user->email;
    }
}
