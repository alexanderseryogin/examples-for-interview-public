<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EventListView
 *
 * @package App\Models
 * @property int     $id
 * @property int     $user_id
 * @property string  $name
 * @property string  $options
 * @property boolean $is_default
 * @property Carbon  $created_at
 * @property Carbon  $updated_at
 * @mixin \Eloquent
 */
class ListView extends Model
{
    const TYPE_EVENT = 'events';

    const TYPE_VENUE = 'venues';

    const TYPE_TICKET = 'tickets';

    const TYPE_USER = 'users';

    const TYPE_INVITE = 'invites';

    /**
     * @return array
     */
    public static function getTypes(): array
    {
        return [
            self::TYPE_EVENT,
            self::TYPE_VENUE,
            self::TYPE_TICKET,
            self::TYPE_USER,
            self::TYPE_INVITE
        ];
    }

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'options',
        'is_default',
        'type'
    ];
}
