<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class EventQuote
 *
 * @package App\Models
 * @property int    $id
 * @property int    $event_id
 * @property string $content
 * @property Event  $event
 * @mixin \Eloquent
 */
class EventQuote extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'event_id',
        'content'
    ];

    /**
     * @return BelongsTo
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }
}
