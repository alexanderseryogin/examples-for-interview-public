<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EventListView
 *
 * @package App\Models
 * @property int     $id
 * @property int     $user_id
 * @property string  $name
 * @property string  $options
 * @property boolean $is_default
 * @property Carbon  $created_at
 * @property Carbon  $updated_at
 * @mixin \Eloquent
 */
class EventListView extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'options',
        'is_default'
    ];
}
