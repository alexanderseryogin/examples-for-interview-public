<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transportation extends Model
{
    public function city()
    {
        return $this->hasMany(City::class, 'id', 'city_origin');
    }
}
