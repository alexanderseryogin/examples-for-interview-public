<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticketcode extends Model
{

    protected $table = 'ticket_codes';

    public function order()
    {
        return $this->belongsTo(Order::class, 'orders_tickets');
    }

}
