<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function transportation()
    {
        return $this->hasMany(Transportation::class, 'city_origin', 'id');
    }
}
