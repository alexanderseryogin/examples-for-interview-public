<?php

namespace App\Models;

use App\Admin\Traits\Models\HasCompany;
use App\Admin\Traits\Models\HasCreator;
use App\Helpers\StorageHelper;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Class EventPackage
 *
 * @package App\Models
 * @property int                            $id
 * @property int                            $event_id
 * @property string                         $name
 * @property string                         $card_size
 * @property string                         $card_image
 * @property int                            $currency_id
 * @property float                          $price
 * @property string                         $call_to_action_link
 * @property string                         $header_image
 * @property string                         $description
 * @property string                         $details
 * @property int                            $pos
 * @property int                            $is_visible
 * @property string                         $status
 * @property Carbon|null                    $created_at
 * @property Carbon|null                    $updated_at
 * @property Event                          $event
 * @property Collection|EventPackageBlock[] $blocks
 * @property Currency                       $currency
 * @property string|null                    $cardImageUrl
 * @property string|null                    $headerImageUrl
 * @mixin \Eloquent
 */
class EventPackage extends Model
{
    use HasCreator, HasCompany;

    const STATUS_DRAFT = 'Draft';

    const STATUS_CLOSED = 'Closed';

    const STATUS_COMING_SOON = 'Coming soon';

    const STATUS_ON_SALE = 'On Sale';

    const STATUS_SOLD_OUT = 'Sold out';

    const CARD_SIZE_SMALL = 'Small 1x1';

    const CARD_SIZE_MEDIUM = 'Medium 3x1';

    const CARD_SIZE_LARGE = 'Large 3x1';

    const FOLDER = 'event-packages';

    /**
     * @var array
     */
    protected $attributes = [
        'status' => self::STATUS_DRAFT
    ];
    /**
     * @var array
     */
    protected $fillable = [
        'event_id',
        'creator_id',
        'company_id',
        'name',
        'card_size',
        'card_image',
        'currency_id',
        'price',
        'call_to_action_link',
        'header_image',
        'description',
        'details',
        'pos',
        'is_visible',
        'status',
    ];

    /**
     * @return array
     */
    public static function getStatuses(): array
    {
        return [
            self::STATUS_DRAFT,
            self::STATUS_CLOSED,
            self::STATUS_COMING_SOON,
            self::STATUS_ON_SALE,
            self::STATUS_SOLD_OUT
        ];
    }

    /**
     * @return array
     */
    public static function getCardSizes(): array
    {
        return [
            self::CARD_SIZE_SMALL,
            self::CARD_SIZE_MEDIUM,
            self::CARD_SIZE_LARGE
        ];
    }

    /**
     * @return array
     */
    public static function getStatusesList(): array
    {
        return [
            self::STATUS_DRAFT       => self::STATUS_DRAFT,
            self::STATUS_CLOSED      => self::STATUS_CLOSED,
            self::STATUS_COMING_SOON => self::STATUS_COMING_SOON,
            self::STATUS_ON_SALE     => self::STATUS_ON_SALE,
            self::STATUS_SOLD_OUT    => self::STATUS_SOLD_OUT
        ];
    }

    /**
     * @return BelongsTo
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * @return HasMany
     */
    public function blocks(): HasMany
    {
        return $this->hasMany(EventPackageBlock::class)->orderBy('pos');
    }

    /**
     * @return string
     */
    public function getCardImageUrlAttribute(): string
    {
        return $this->card_image ? StorageHelper::url($this->card_image) : '';
    }

    /**
     * @return string
     */
    public function getHeaderImageUrlAttribute(): string
    {
        return $this->header_image ? StorageHelper::url($this->header_image) : '';
    }
}
