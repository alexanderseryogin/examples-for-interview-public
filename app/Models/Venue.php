<?php

namespace App\Models;

use App\Admin\Traits\Models\HasCompany;
use App\Admin\Traits\Models\HasCreator;
use App\Admin\Traits\Models\Loggable;
use App\Helpers\StorageHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Venue
 *
 * @property int                    $id
 * @property string|null            $name
 * @property string|null            $address1
 * @property string|null            $address2
 * @property string|null            $city
 * @property string|null            $state
 * @property string|null            $postcode
 * @property int|null               $country_id
 * @property string|null            $map_link
 * @property int|null               $capacity
 * @property string|null            $seating_layout_image
 * @property Carbon|null            $created_at
 * @property Carbon|null            $updated_at
 * @property Country|null           $country
 * @property Collection|Event[]     $events
 * @property string                 $seatingLayoutImageUrl
 * @property Collection|ActionLog[] $logs
 * @mixin \Eloquent
 */
class Venue extends Model
{
    use Loggable, HasCreator, HasCompany;

    const FOLDER = 'venues';

    /**
     * @var array
     */
    protected $fillable = [
        'creator_id',
        'company_id',
        'name',
        'address1',
        'address2',
        'postcode',
        'city',
        'state',
        'country_id',
        'map_link',
        'capacity',
        'seating_layout_image',
    ];

    /**
     * @return HasMany
     */
    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }

    /**
     * @return BelongsTo
     */
    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return string
     */
    public function getSeatingLayoutImageUrlAttribute(): string
    {
        return $this->seating_layout_image ? StorageHelper::url($this->seating_layout_image) : '';
    }
}
