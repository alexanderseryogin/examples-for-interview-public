<?php

namespace App\Models;

use App\Admin\Traits\Models\HasCompany;
use App\Admin\Traits\Models\HasCreator;
use App\Helpers\StorageHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Ticket
 *
 * @package App\Models
 * @property int         $id
 * @property int         $event_id
 * @property string      $name
 * @property string      $tier
 * @property string      $category
 * @property string      $checkout_url
 * @property int         $currency_id
 * @property float       $price
 * @property int         $has_discount
 * @property float       $discount_price
 * @property int         $show_countdown
 * @property int         $quantity
 * @property int         $sold_quantity
 * @property string      $header_image
 * @property string      $seat_plan
 * @property string      $description
 * @property string      $details
 * @property int         $pos
 * @property string      $status
 * @property Carbon|null $sales_end_date
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Event       $event
 * @property Currency    $currency
 * @property string|null $salesEndDateHour
 * @property string|null $salesEndDateMinute
 * @property string|null $headerImageUrl
 * @property string|null $seatPlanUrl
 * @mixin \Eloquent
 */
class Ticket extends Model
{
    use HasCreator, HasCompany;

    const STATUS_ON_SALE = 'On Sale';

    const STATUS_SELLING_FAST = 'Selling fast';

    const STATUS_COMING_SOON = 'Coming soon';

    const STATUS_SOLD_OUT = 'Sold out';

    const STATUS_HIDDEN = 'Hidden';

    const FOLDER = 'tickets';

    /**
     * @var array
     */
    protected $dates = [
        'sales_end_date'
    ];
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'event_id',
        'company_id',
        'checkout_url',
        'category',
        'tier',
        'sales_end_date',
        'show_countdown',
        'status',
        'price',
        'has_discount',
        'discount_price',
        'seat_plan',
        'header_image',
        'quantity',
        'sold_quantity',
        'description',
        'details',
        'pos',
        'currency_id',
        'creator_id',
        'company_id'
    ];
    /**
     * @return array
     */
    public static function getStatuses(): array
    {
        return [
            self::STATUS_ON_SALE,
            self::STATUS_SELLING_FAST,
            self::STATUS_COMING_SOON,
            self::STATUS_SOLD_OUT,
            self::STATUS_HIDDEN
        ];
    }

    /**
     * @return array
     */
    public static function getStatusesList(): array
    {
        return [
            self::STATUS_ON_SALE      => self::STATUS_ON_SALE,
            self::STATUS_SELLING_FAST => self::STATUS_SELLING_FAST,
            self::STATUS_COMING_SOON  => self::STATUS_COMING_SOON,
            self::STATUS_SOLD_OUT     => self::STATUS_SOLD_OUT,
            self::STATUS_HIDDEN       => self::STATUS_HIDDEN
        ];
    }

    /**
     * @return BelongsTo
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * @return string|null
     */
    public function getSalesEndDateHourAttribute(): ?string
    {
        return $this->sales_end_date ? $this->sales_end_date->hour : null;
    }

    /**
     * @return string|null
     */
    public function getSalesEndDateMinuteAttribute(): ?string
    {
        return $this->sales_end_date ? $this->sales_end_date->minute : null;
    }

    /**
     * @return string
     */
    public function getHeaderImageUrlAttribute()
    {
        return $this->header_image ? StorageHelper::url($this->header_image) : '';
    }

    /**
     * @return string
     */
    public function getSeatPlanUrlAttribute()
    {
        return $this->seat_plan ? StorageHelper::url($this->seat_plan) : '';
    }
}
