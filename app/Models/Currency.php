<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Currency
 *
 * @package App\Models
 * @property int    $id
 * @property string $name
 * @property string $code
 * @property string $symbol
 * @mixin \Eloquent
 */
class Currency extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;
}
