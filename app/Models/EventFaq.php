<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class EventFaq
 *
 * @package App\Models
 * @property int         $id
 * @property int         $event_id
 * @property string      $title
 * @property string      $body
 * @property int         $pos
 * @property int         $is_template
 * @property int         $is_visible
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Event       $event
 * @mixin \Eloquent
 */
class EventFaq extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'event_id',
        'title',
        'body',
        'pos',
        'is_template',
        'is_visible',
    ];

    /**
     * @return BelongsTo
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }
}
