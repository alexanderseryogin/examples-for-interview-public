<?php

namespace App\Models;

use App\Helpers\StorageHelper;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class EventPackageBlock
 *
 * @package App\Models
 * @property int          $id
 * @property int          $event_package_id
 * @property string       $type
 * @property string       $title
 * @property string       $icon
 * @property string       $content
 * @property int          $pos
 * @property string       $is_active
 * @property Carbon|null  $created_at
 * @property Carbon|null  $updated_at
 * @property EventPackage $eventPackage
 * @property string|null  $iconUrl
 * @mixin \Eloquent
 */
class EventPackageBlock extends Model
{
    const TYPE_TICKET = 'ticket';

    const TYPE_FLIGHT = 'flight';

    const TYPE_HOTEL = 'hotel';

    const TYPE_MERCHANDISE = 'merchandise';

    const TYPE_PERKS = 'perks';

    const FOLDER = 'event-package-blocks';

    /**
     * @var array
     */
    protected $fillable = [
        'event_package_id',
        'type',
        'title',
        'icon',
        'content',
        'pos',
        'is_active',
    ];

    /**
     * @return BelongsTo
     */
    public function eventPackage(): BelongsTo
    {
        return $this->belongsTo(EventPackage::class);
    }

    /**
     * @return string
     */
    public function getIconUrlAttribute(): string
    {
        return $this->icon ? StorageHelper::url($this->icon) : '';
    }

    /**
     * @return array
     */
    public static function getTypes(): array
    {
        return [
            self::TYPE_TICKET,
            self::TYPE_FLIGHT,
            self::TYPE_HOTEL,
            self::TYPE_MERCHANDISE,
            self::TYPE_PERKS
        ];
    }

    /**
     * @return array
     */
    public static function getTypesTitles(): array
    {
        return [
            self::TYPE_TICKET      => 'Ticket',
            self::TYPE_FLIGHT      => 'Flight',
            self::TYPE_HOTEL       => 'Hotel',
            self::TYPE_MERCHANDISE => 'Merchandise',
            self::TYPE_PERKS       => 'Other Perks'
        ];
    }
}
