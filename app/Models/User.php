<?php

namespace App\Models;

use App\Admin\Traits\Models\HasCompany;
use App\Admin\Traits\Models\Loggable;
use App\Helpers\StorageHelper;
use App\Notifications\ResetPassword;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 *
 * @package App\Models
 * @property int                          $id
 * @property string                       $name
 * @property string                       $email
 * @property string                       $digest
 * @property string                       $password
 * @property string                       $remember_token
 * @property string                       $timezone
 * @property int                          $company_id
 * @property int                          $team_id
 * @property Carbon                       $created_at
 * @property Carbon|null                  $updated_at
 * @property Carbon|null                  $deleted_at
 * @property Account                      $account
 * @property Collection|Event[]           $events
 * @property Collection|UserAccessEvent[] $accessEventsPivot
 * @property Collection|Event[]           $accessEvents
 * @property Company                      $company
 * @property Collection|Order[]           $orders
 * @property Collection|EventListView[]   $settings
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable, HasRoles, Loggable, HasCompany, SoftDeletes;

    const STATUS_ACTIVE = 'Active';

    const STATUS_SUSPEND = 'Suspend';

    const STATUS_PENDING = 'Pending';

    const STATUS_DELETED = 'Deleted';

    const FOLDER = 'users';

    protected $dates = ['last_active_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'email',
        'status',
        'password',
        'last_active_at',
        'company_id',
        'deleted_at'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @return array
     */
    public static function getStatuses(): array
    {
        return [
            self::STATUS_ACTIVE,
            self::STATUS_PENDING,
            self::STATUS_SUSPEND,
            self::STATUS_DELETED,
        ];
    }

    /**
     * @return array
     */
    public static function getStatusesList(): array
    {
        return [
            self::STATUS_ACTIVE  => self::STATUS_ACTIVE,
            self::STATUS_PENDING => self::STATUS_PENDING,
            self::STATUS_SUSPEND => self::STATUS_SUSPEND,
            self::STATUS_DELETED => self::STATUS_DELETED
        ];
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return trim($this->account->first_name . ' ' . $this->account->last_name);
    }

    /**
     * @return string
     */
    public function getAvatarUrlAttribute(): ?string
    {
        return $this->account->avatar ? StorageHelper::url($this->account->avatar) : null;
    }

    /**
     * @return HasOne
     */
    public function account(): HasOne
    {
        return $this->hasOne(Account::class);
    }

    /**
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    /**
     * @return HasMany
     */
    public function events(): HasMany
    {
        return $this->hasMany(Event::class, 'creator_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function accessEventsPivot(): HasMany
    {
        return $this->hasMany(UserAccessEvent::class);
    }

    /**
     * @return BelongsToMany
     */
    public function accessEvents(): BelongsToMany
    {
        return $this->belongsToMany(Event::class, 'user_access_events');
    }

    /**
     * @return HasMany
     */
    public function settings(): HasMany
    {
        return $this->hasMany(EventListView::class);
    }

    /**
     * @return bool
     */
    public function isSuperAdmin(): bool
    {
        return $this->hasRole(Role::SUPER_ADMIN);
    }

    /**
     * @return string
     */
    public function getRoleListAttribute(): string
    {
        return $this->roles->implode('name');
    }

    /**
     * @param string $token
     */
    public function sendPasswordResetNotification($token)
    {
        $email = $this->getEmailForPasswordReset();
        $this->notify(new ResetPassword($token, $email));
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function hasCustomAccessToEvent(int $id): bool
    {
        return $this->accessEventsPivot()
            ->where('event_id', $id)
            ->exists();
    }
}
