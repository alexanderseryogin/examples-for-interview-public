<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Company
 *
 * @package App\Models
 * @property int         $id
 * @property string      $name
 * @property string      $logo
 * @property string      $type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @mixin \Eloquent
 */
class Company extends Model
{
    const TYPE_EVENT_ORGANIZER = 'Event organizer';

    const TYPE_VENUE_MANAGER = 'Venue manager';

    const TYPE_PROMOTER = 'Promoter';

    const TYPE_SALES_AGENT = 'Sales agent';

    const FOLDER = 'companies';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'logo',
        'type'
    ];

    /**
     * @return array
     */
    public static function getTypes(): array
    {
        return [
            self::TYPE_EVENT_ORGANIZER,
            self::TYPE_VENUE_MANAGER,
            self::TYPE_PROMOTER,
            self::TYPE_SALES_AGENT,
        ];
    }

    /**
     * @return array
     */
    public static function getTypesList(): array
    {
        return array_combine(self::getTypes(), self::getTypes());
    }

    /**
     * @return HasMany
     */
    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }
}
