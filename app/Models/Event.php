<?php

namespace App\Models;

use App\Admin\Traits\Models\HasCompany;
use App\Admin\Traits\Models\HasCreator;
use App\Admin\Traits\Models\Loggable;
use App\Helpers\StorageHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Class Event
 *
 * @package App\Models
 * @property int                         $id
 * @property string                      $name
 * @property int                         $venue_id
 * @property int                         $organizer_id
 * @property int                         $promoter_id
 * @property int                         $has_redtix_guarantee
 * @property int                         $category_id
 * @property int                         $slug
 * @property int                         $max_purchase_tickets_quantity
 * @property Carbon|null                 $start_date
 * @property Carbon|null                 $end_date
 * @property Carbon|null                 $custom_date
 * @property int                         $is_estimated_end_time
 * @property int                         $show_countdown
 * @property string                      $description
 * @property string                      $banner_desktop
 * @property string                      $banner_mobile
 * @property string                      $info_tab_name
 * @property int                         $is_info_tab_visible
 * @property string                      $info_video
 * @property string                      $status
 * @property Carbon|null                 $created_at
 * @property Carbon|null                 $updated_at
 * @property User                        $creator
 * @property Collection|ActionLog[]      $logs
 * @property Collection|Ticket[]         $tickets
 * @property Collection|EventPackage[]   $packages
 * @property Collection|EventInfoBlock[] $infoBlocks
 * @property Collection|EventFaq[]       $faqs
 * @property Collection|EventImage[]     $images
 * @property Collection|EventImage[]     $artistLineUp
 * @property Collection|EventImage[]     $gallery
 * @property Collection|EventQuote[]     $quotes
 * @property string                      $creatorName
 * @property string                      $bannerDesktopUrl
 * @property string                      $bannerMobileUrl
 * @mixin \Eloquent
 */
class Event extends Model
{
    use Loggable, HasCreator, HasCompany;

    const STATUS_DRAFT = 'Draft';

    const STATUS_PUBLISHED = 'Published';

    const STATUS_PRIVATE_SALE = 'Private';

    const STATUS_SUSPENDED = 'Suspended';

    const STATUS_CLOSED = 'Closed';

    const STATUS_DELETED = 'Deleted';

    const STATUS_CANCELLED = 'Cancelled';

    const FOLDER = 'events';

    const MAX_PURCHASE_TICKETS_QUANTITY = 8;

    /**
     * @var array
     */
    protected $dates = [
        'start_date',
        'end_date',
        'custom_date'
    ];
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var array
     */
    protected $attributes = [
        'has_redtix_guarantee'          => 1,
        'max_purchase_tickets_quantity' => self::MAX_PURCHASE_TICKETS_QUANTITY
    ];

    /**
     * @return array
     */
    public static function getStatusesList(): array
    {
        return array_combine(self::getStatuses(), self::getStatuses());
    }

    /**
     * @return array
     */
    public static function getStatuses(): array
    {
        return [
            self::STATUS_DRAFT,
            self::STATUS_PUBLISHED,
            self::STATUS_PRIVATE_SALE,
            self::STATUS_SUSPENDED,
            self::STATUS_CLOSED,
            self::STATUS_DELETED,
            self::STATUS_CANCELLED
        ];
    }

    /**
     * @return string|null
     */
    public function getUrlAttribute(): ?string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getBannerDesktopUrlAttribute(): string
    {
        return $this->banner_desktop ? StorageHelper::url($this->banner_desktop) : '';
    }

    /**
     * @return string
     */
    public function getBannerMobileUrlAttribute(): string
    {
        return $this->banner_mobile ? StorageHelper::url($this->banner_mobile) : '';
    }

    /**
     * @return BelongsToMany
     */
    public function invites(): BelongsToMany
    {
        return $this->belongsToMany(Invite::class);
    }

    /**
     * @return BelongsTo
     */
    public function organizer(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return BelongsTo
     */
    public function venue(): BelongsTo
    {
        return $this->belongsTo(Venue::class);
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(EventCategory::class);
    }

    /**
     * @return BelongsToMany
     */
    public function genres(): BelongsToMany
    {
        return $this->belongsToMany(Genre::class);
    }

    /**
     * @return HasMany
     */
    public function tickets(): HasMany
    {
        return $this->hasMany(Ticket::class)->orderBy('pos');
    }

    /**
     * @return HasMany
     */
    public function packages(): HasMany
    {
        return $this->hasMany(EventPackage::class)->orderBy('pos');
    }

    /**
     * @return HasMany
     */
    public function quotes(): HasMany
    {
        return $this->hasMany(EventQuote::class);
    }

    /**
     * @return HasMany
     */
    public function infoBlocks(): HasMany
    {
        return $this->hasMany(EventInfoBlock::class)->orderBy('pos');
    }

    /**
     * @return HasMany
     */
    public function faqs(): HasMany
    {
        return $this->hasMany(EventFaq::class)->orderBy('pos');
    }

    /**
     * @return HasMany
     */
    public function gallery(): HasMany
    {
        return $this->images()->where('category', EventImage::CATEGORY_GALLERY);
    }

    /**
     * @return HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany(EventImage::class);
    }

    /**
     * @return HasMany
     */
    public function artistLineUp(): HasMany
    {
        return $this->images()->where('category', EventImage::CATEGORY_ARTIST_LINE_UP);
    }

    /**
     * @return string
     */
    public function getCreatorNameAttribute(): string
    {
        return $this->creator->account->fullName;
    }
}
