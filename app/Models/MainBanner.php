<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainBanner extends Model
{
    public $table = 'main_banners';
}
