<?php

namespace App\Models;

/**
 * Class Role
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Permission extends \Spatie\Permission\Models\Permission
{
    const ROLE_ASSIGNMENT_REMOVAL = 'role-assignment-removal';

    const CHANGE_USER_ACCOUNT_STATUSES = 'change-user-account-statuses';

    const GLOBAL_EVENTS_MANAGEMENT = 'global-events-management';

    const GLOBAL_VENUES_MANAGEMENT = 'global-venues-management';

    const GLOBAL_TICKETS_MANAGEMENT = 'global-tickets-management';

    const GLOBAL_USERS_MANAGEMENT = 'users-management';
}
