<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class EventInfoBlock
 *
 * @package App\Models
 * @property int    $id
 * @property int    $event_id
 * @property string $type
 * @property string $title
 * @property int    $pos
 * @property int    $is_visible
 * @property Event  $event
 * @mixin \Eloquent
 */
class EventInfoBlock extends Model
{
    const TYPE_ARTIST_LINE_UP = 'artist-line-up';

    const TYPE_GALLERY = 'gallery';

    const TYPE_QUOTES = 'quotes';

    const TYPE_VIDEO = 'video';

    /**
     * @var array
     */
    protected $fillable = [
        'event_id',
        'type',
        'title',
        'pos',
        'is_visible'
    ];

    /**
     * @return BelongsTo
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * @return array
     */
    public static function getTypes(): array
    {
        return [
            self::TYPE_VIDEO,
            self::TYPE_ARTIST_LINE_UP,
            self::TYPE_GALLERY,
            self::TYPE_QUOTES
        ];
    }

    /**
     * @return array
     */
    public static function getTypesTitles(): array
    {
        return [
            self::TYPE_VIDEO          => 'Video',
            self::TYPE_ARTIST_LINE_UP => 'Artist Line Up',
            self::TYPE_GALLERY        => 'Gallery',
            self::TYPE_QUOTES         => 'Quotes'
        ];
    }
}
