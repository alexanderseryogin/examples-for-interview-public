<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Class UserAccessEvent
 *
 * @package App\Models
 * @property int         $id
 * @property int         $user_id
 * @property int         $event_id
 * @property Carbon      $created_at
 * @property Carbon|null $updated_at
 * @property User        $user
 * @property Event       $event
 * @mixin \Eloquent
 */
class UserAccessEvent extends Model
{
    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }
}
