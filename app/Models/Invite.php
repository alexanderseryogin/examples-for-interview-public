<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class Invite
 *
 * @package App\Models
 * @property int                      $id
 * @property string                   $email
 * @property string                   $token
 * @property int                      $company_id
 * @property int                      $team_id
 * @property Carbon                   $created_at
 * @property Carbon|null              $updated_at
 * @property Company                  $company
 * @property Collection|Event[]       $events
 */
class Invite extends Model
{
    use HasRoles;
    protected $guard_name = 'web';
    protected $fillable = [
        'email',
        'token',
        'company_id',
        'team_id'
    ];

    /**
     * @return BelongsTo
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return BelongsTo
     */
    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * @return BelongsToMany
     */
    public function events(): BelongsToMany
    {
        return $this->belongsToMany(Event::class);
    }
}
