<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainAnnouncement extends Model
{
    public $table = 'main_announcements';
}
