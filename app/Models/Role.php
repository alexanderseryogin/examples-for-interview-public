<?php

namespace App\Models;

use Carbon\Carbon;

/**
 * Class Role
 *
 * @package App\Models
 * @property int         $id
 * @property string      $name
 * @property string      $description
 * @property string      $guard_name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @mixin \Eloquent
 */
class Role extends \Spatie\Permission\Models\Role
{
    const SUPER_ADMIN = 'Super Admin';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'guard_name'
    ];
}
