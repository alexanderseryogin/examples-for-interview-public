<?php

namespace App\Models;

use App\Helpers\StorageHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class EventImage
 *
 * @package App\Models
 * @property int    $id
 * @property int    $event_id
 * @property string $category
 * @property string $path
 * @property Event  $event
 * @property string $url
 * @mixin \Eloquent
 */
class EventImage extends Model
{
    const CATEGORY_ARTIST_LINE_UP = 'artist_line_up';

    const CATEGORY_GALLERY = 'gallery';

    const FOLDER = 'event-images';

    /**
     * @var array
     */
    protected $fillable = [
        'event_id',
        'category',
        'path'
    ];

    /**
     * @return BelongsTo
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * @return string
     */
    public function getUrlAttribute(): string
    {
        return $this->path ? StorageHelper::url($this->path) : '';
    }
}
