<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventCollection extends Model
{
    public $table = 'event_collections';

    public function events()
    {
      return $this->hasMany(Event::class);
    }
}
