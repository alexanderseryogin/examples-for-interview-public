<?php

namespace App\Mail;

use App\Models\Invite;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InviteCreated extends Mailable
{
    use Queueable, SerializesModels;

    const MAIL_SUBJECT = 'You\'ve been invited to the RedTix CMS. Please create your account now.';

    /**
     * @var Invite
     */
    private $invite;

    /**
     * Create a new message instance.
     *
     * @param Invite $invite
     */
    public function __construct(Invite $invite)
    {
        $this->invite = $invite;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(self::MAIL_SUBJECT)
            ->view('emails.user.invite', ['invite' => $this->invite]);
    }
}
