<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailChanged extends Mailable
{
    use Queueable, SerializesModels;

    const MAIL_SUBJECT = 'Your email on RedTix has been changed.';

    /**
     * @var User
     */
    private $user;

    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(self::MAIL_SUBJECT)
            ->view('emails.user.email-changed', ['user' => $this->user]);
    }
}
