<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    const GA_T1_URL = 'https://redtix-tickets.airasia.com/sgp/en-AU/shows/sgd- ultra 2019 - general admission - weekend ticket/events';
    const GA_T2_URL = 'https://redtix-tickets.airasia.com/sgp/en-AU/shows/sgd- ultra 2019 - general admission - weekend/events';

    const PGA_T1_URL = 'https://redtix-tickets.airasia.com/sgp/en-AU/shows/sgd- ultra 2019 - premium general admission - weekend ticket/events';
    const PGA_T2_URL = 'https://redtix-tickets.airasia.com/sgp/en-AU/shows/sgd- ultra 2019 - premium general admission - weekend/events';

    protected $fillable = ['code'];
    protected $dates = ['expired_at'];

    public function getIsUsedAttribute()
    {
        return $this->expired_at < Carbon::now();
    }
}
