<?php

namespace App\Http\Controllers\Auth;

use App\Admin\Repositories\AccountRepository;
use App\Admin\Repositories\UserRepository;
use App\Admin\Services\UserService;
use App\Http\Controllers\Controller;
use Auth;
use Socialite;

class SocialAuthFacebookController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var AccountRepository
     */
    private $accountRepository;
    /**
     * @var UserService
     */
    private $userService;

    /**
     * SocialAuthFacebookController constructor.
     *
     * @param UserRepository    $userRepository
     * @param AccountRepository $accountRepository
     * @param UserService       $userService
     */
    public function __construct(UserRepository $userRepository, AccountRepository $accountRepository, UserService $userService)
    {

        $this->userRepository = $userRepository;
        $this->accountRepository = $accountRepository;
        $this->userService = $userService;
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     */
    public function handleProviderCallback()
    {
        $socialiteUser = Socialite::driver('facebook')->user();
        $user = $this->userService->createFromSocial($socialiteUser);
        Auth::login($user);

        return redirect()->intended();
    }
}
