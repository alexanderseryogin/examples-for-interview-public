<?php

namespace App\Http\Controllers;

use App\Services\Sendgrid\Sendgrid;

class SendgridController extends Controller
{
    public function sendgrid()
    {
        $attachment = file_get_contents('/home/developer/Documents/Alice.pdf');
        $data = [
            'fromEmail' => 'testfrom@example.com',
            'fromName' => 'From Company',
            'toEmail' => 'testto@example.com',
            'toName' => 'Johnny Doe',
            'subject' => 'Subject',
            'params' => [
                'fullname' =>'User Fullname',
                "invoice_id" => "1234",
                "category" => "PGA 2-DAY TICKET",
                "barcode" => "125487",
                "passportID" => "A234H8d",
                "qrcode" => "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAIAAAD2HxkiAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAExUlEQVR4nO3dsW7bMBRA0SbI/3+y0aVD4UFowtL3UT5nryJLuuXyQH48Ho9fQOezvgF4dyKEmAghJkKIiRBiIoSYCCEmQoiJEGIihJgIISZCiIkQYiKEmAghJkKIiRBiIoSYCCEmQoiJEGIihJgIISZCiIkQYiKEmAghJkKIiRBiIoSYCCEmQoiJEGIihJgIISZCiIkQYiKEmAghJkKIiRBiIoSYCCEmQoh91Tfwx+fn3f47eDweP/6310/j+sorT3LflWdaeUf/0d0eKxxHhBATIcRECDERQkyEEBMhxEQIMRFCbMrEzLUhkw1PViZIqumTfU/yfu/oZQ64Rbg3EUJMhBATIcRECDERQkyEEBMhxEQIsTMmZq7tm4qYOQVybWWfmH2/1zu6YCWEmAghJkKIiRBiIoSYCCEmQoiJEGIihNgdJmZOtO/8o2omhh+zEkJMhBATIcRECDERQkyEEBMhxEQIMRFCzMRMY99cy8w9ZrhgJYSYCCEmQoiJEGIihJgIISZCiIkQYiKE2B0mZsx5/G3f+UcrvKMLE18YvBURQkyEEBMhxEQIMRFCTIQQEyHERAixMyZmZk6BVFb2idn3JL2jH/PgICZCiIkQYiKEmAghJkKIiRBiIoSYCCH2YfOPxMz5Eh9DYuKnAG9FhBATIcRECDERQkyEEBMhxEQIMRFCbMoeMysTJCu7qqzMiFT3vHLlFfumfE58Gv+RlRBiIoSYCCEmQoiJEGIihJgIISZCiIkQYmfsMWPq5TWq5zzzDb6MlRBiIoSYCCEmQoiJEGIihJgIISZCiIkQYmfsMbNvF5mVK584q7Fvn5h9b/Daviu/zAG3CPcmQoiJEGIihJgIISZCiIkQYiKEmAghNmViZkU1M3G/84+umYnZ5PgfAKcTIcRECDERQkyEEBMhxEQIMRFCTIQQm3IqU7VfSzVvMXNyZea5S7dnJYSYCCEmQoiJEGIihJgIISZCiIkQYiKE2JSJmXczc3Ll3fa2GfLxWwkhJkKIiRBiIoSYCCEmQoiJEGIihJgIITZlYuYGZ+s82TfJMXMKZOYbHPJ5X5v44OCtiBBiIoSYCCEmQoiJEGIihJgIISZCiH3VN/BPZs49VCdJXavuauaUz8zpoidWQoiJEGIihJgIISZCiIkQYiKEmAghJkKInTExc23mWUIrql904s43R8zEXLMSQkyEEBMhxEQIMRFCTIQQEyHERAgxEULsDhMzJ5o551HN0+wz8zk/sRJCTIQQEyHERAgxEUJMhBATIcRECDERQszETGNlVmPmTjD7ToM6cVLnWw64Rbg3EUJMhBATIcRECDERQkyEEBMhxEQIsTtMzAzZKeRb9k1y3O+MqpW/e8S3YSWEmAghJkKIiRBiIoSYCCEmQoiJEGIihNgZEzNH7BRyA9W0zcxdc17Gxw0xEUJMhBATIcRECDERQkyEEBMhxEQIsY8jRgrgxqyEEBMhxEQIMRFCTIQQEyHERAgxEUJMhBATIcRECDERQkyEEBMhxEQIMRFCTIQQEyHERAgxEUJMhBATIcRECDERQkyEEBMhxEQIMRFCTIQQEyHERAgxEUJMhBATIcRECDERQkyEEBMhxEQIMRFCTIQQEyHERAgxEULsN2LVE1G3USP+AAAAAElFTkSuQmCC",
            ],
            'attachment' => $attachment,
        ];
        return (new Sendgrid($data))->sendgrid();
    }
}
