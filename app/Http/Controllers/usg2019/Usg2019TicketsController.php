<?php

namespace App\Http\Controllers\usg2019;

use App\Services\Ticket\TicketService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use QrCode;
use Intervention\Image\Imagick;


class Usg2019TicketsController extends Controller
{
    private $ticketService;

    public function __construct(TicketService $ticketService)
    {
        $this->ticketService = $ticketService;
    }



    public function generateQRCode($fullBarcode)
    {
        $qrcode = $this->ticketService->generateQRCode($fullBarcode, true);

        return view('usg2019/images-from-text/templates/template', ['fullBarcode' => $fullBarcode]);
    }

    public function generateImage($fullBarcode)
    {
        $this->ticketService->generateImageFromPDF('/kendoui.for.jquery/examples/content/web/pdfprocessing/Invalid.pdf', 'testPDF.jpg');
    }

}

