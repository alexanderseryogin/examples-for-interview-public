<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;

use App\Models\User;
use App\Models\Country;
use App\Models\Account;

use Auth;
use Image;
use Storage;
use Route;
use File;

class ProfileController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $account = Account::where('user_id', $user->id)->first();
        $countries = Country::get();
    
        return view('pages.profile.index', [ 'user' => $user, 'countries' => $countries, 'account' => $account ]);
    }

    public function edit()
    {
        $user = Auth::user();
        return view('pages.profile.edit', [ 'user' => $user ]);
    }

    public function update(Request $request) 
    {
        $user = Auth::user();

        $user->name = $request->name;
        $user->digest = str_slug($request->name);
        $user->email = $request->email;
        $user->timezone = $request->timezone;
        // $user->password = $request->password;
        $user->save();

        $user->account->first_name = $request->first_name;
        $user->account->last_name = $request->last_name;
        $user->account->email = $request->email;
        $user->account->address1 = $request->address1;
        $user->account->address2 = $request->address2;
        $user->account->city = $request->city;
        $user->account->state = $request->state;
        $user->account->postal_code = $request->postal_code;
        $user->account->country_id = $request->country;

        $user->account->save();
        return redirect('/profile/'.$user->digest)->with('successMessage', 'Profile has been updated.');

    }

    public function avatar(Request $request)
    {

        $user = Auth::user();
        $account = Account::findOrFail($user->id);

        $user_folder = $user->digest.'-'.$user->id;
        $path = public_path().'/images/avatars/'.$user_folder;


        if($request->hasFile('avatar')){

            $avatar = $request->file('avatar');

            if(!File::exists($path)) {
                File::makeDirectory($path);
                $filename = 'profile-'.$str_random(3).'.'. $avatar->getClientOriginalExtension();
                $avatar->move($path, $filename);
            } else {
                if($account->avatar){
                    $current_avatar = $account->avatar;
                    // dd($current_avatar);
                    File::delete(public_path().$current_avatar);
                }
                $filename = 'profile' . '.' . $avatar->getClientOriginalExtension();
                $avatar->move($path, $filename);

            }

            $account->avatar = '/images/avatars/'.$user_folder.'/'.$filename;
            $account->save();
        } 

        return redirect('/profile/'.$user->digest)->with('successMessage', 'Avatar has been updated.');
    }
    public function changePassword(Request $request)
    {
        $user = Auth::user();

        return view('pages.profile.changepassword', [ 'user' => $user ]);
    }
    public function updatePassword(Request $request)
    {   
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("errorMessage","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("errorMessage","New Password cannot be same as your current password. Please choose a different password.");
        }
        
        $this->validate($request, [
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
 
        return redirect('/profile/'.$user->digest)->with("successMessage","Password changed successfully !");
    }
}
