<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Code;

class CodeController extends Controller
{
    const STATUS_OK = 'ok';

    const STATUS_USED = 'used';

    const STATUS_INVALID = 'invalid';

    protected $links = [
        'ga-t1'  => Code::GA_T1_URL,
        'ga-t2'  => Code::GA_T2_URL,
        'pga-t1' => Code::PGA_T1_URL,
        'pga-t2' => Code::PGA_T2_URL,
    ];

    public function apply(Request $request)
    {

        $type = $request->get('type', 'ga-t1');

        $this->validate($request, [
            'code' => 'required|alpha_num|size:6'
        ]);

        $code = Code::where('code', $request->code)->first();

        if (!$code) {
            return response()->json([
                'error'   => true,
                'message' => 'Sorry, your access code is invalid.',
                'status'  => self::STATUS_INVALID
            ]);
        }

        if ($code->isUsed) {
            return response()->json([
                'error'   => true,
                'message' => 'Sorry, your access code has been used.',
                'status'  => self::STATUS_USED
            ]);
        }

        $this->setUsed($code);

        return response()->json([
            'success' => true,
            'message' => 'Great, you have entered in a valid access code!',
            'status'  => self::STATUS_OK,
            'url'     => array_key_exists($type, $this->links) ? $this->links[$type] : ''
        ]);
    }

    protected function setUsed($code)
    {
        $code->expired_at = Carbon::now();

        return $code->save();
    }
}
