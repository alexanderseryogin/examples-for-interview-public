<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Transportation;
use App\Services\Transportation\TransportationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransportationController extends Controller
{
    private $transportationService;
    private $cityOrigin;
    private $cityTransit;

    public function __construct(TransportationService $transportationService)
    {
        $this->transportationService = $transportationService;
    }

    public function checkCityTransit(int $cityOrigin, int $cityDestination)
    {
        $cityTransit = $this->transportationService->hasCityTransit($cityOrigin, $cityDestination);
        return $cityTransit ? $cityTransit->id : null;
    }

    public function getFullRoute(int $cityOrigin, int $cityDestination)
    {
//        $cityOrigin = request()->get('city_origin');
//        $cityDestination = request()->get('city_destination');
//        dd($cityOrigin, $cityDestination);
        $fullRoute = $this->transportationService->getFullRoute($cityOrigin, $cityDestination);
    }

    public function getAbsoluteDestinationFromTransit(int $transportation_id)
    {
        $transportation = Transportation::where('id', $transportation_id)->firstOrFail();
        dd($transportation_id);
//        $city = City::where('id', $city_destination)->get();
//        return $city;
    }


}
