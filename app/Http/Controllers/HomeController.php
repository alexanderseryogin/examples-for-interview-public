<?php

namespace App\Http\Controllers;

use App\Helpers\AuthHelper;
use Auth, DB, QrCode, File, PDF;
use App\Services\Sendgrid\{Sendgrid,TicketService};

class HomeController extends Controller
{
    private $ticketService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TicketService $ticketService)
    {
        $this->ticketService = $ticketService;
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function index()
    {
        if (AuthHelper::hasCmsAccess()) {
            return redirect()->route('admin.events.index');
        }

        return redirect()->route('homepage');
    }

    public function signout()
    {
        Auth::guard('web')->logout();
        return redirect('/');
    }
    public function cgx(){
        // $last_order_id = DB::table('cognitix')->select('last_order_id')->where('type','glownet')->first();

        $this->generatePdf();
        // $this->glownet_ticket();


    }

    protected function generatePdf(){

        $email = DB::table('cognitix')->select('last_order_id')->where('type','email')->first();

        $last_order_id = $email->last_order_id;

        $orders = $this->getOrder($last_order_id);

        foreach($orders as $invoice => $order){

            foreach($order['details'] as $detail){

                $this->_pdf($detail);
            }

            //send email


        }
    }

    protected function _pdf($data)
    {

        //create folder if not exist (QR)
        $path_qr = public_path().'/images/uploads/qr/';
        File::isDirectory($path_qr) or File::makeDirectory($path_qr, 0775, true, true);

        //create folder if not exist (PDF)
        $path_pdf = public_path().'/images/uploads/pdf/';
        File::isDirectory($path_pdf) or File::makeDirectory($path_pdf, 0775, true, true);

        //generate qr code
        $qr_output_path = $path_qr.$data['full_barcode'].'.png';

        QrCode::format('png')->margin(2)->size(100)->generate($data['full_barcode'], $qr_output_path);

        $logo_path = public_path().'/images/redtix_logo.png';
        $logo = pathinfo($logo_path, PATHINFO_EXTENSION);
        $logo_data = file_get_contents($logo_path);
        $logo_base64 = 'data:image/' . $logo . ';base64,' . base64_encode($logo_data);

        $empty_ticket_path = public_path().'/images/ultra_ticket.png';
        $empty_ticket_type = pathinfo($empty_ticket_path, PATHINFO_EXTENSION);
        $empty_ticket_data = file_get_contents($empty_ticket_path);
        $empty_ticket_base64 = 'data:image/' . $empty_ticket_type . ';base64,' . base64_encode($empty_ticket_data);

        $qr_type = pathinfo($qr_output_path, PATHINFO_EXTENSION);
        $qr_data = file_get_contents($qr_output_path);
        $qr_base64 = 'data:image/' . $qr_type . ';base64,' . base64_encode($qr_data);

        $view_data = [
            'ticket_url' => $empty_ticket_base64,
            'qr_url' => $qr_base64,
            'logo_url' => $logo_base64,
            'data' => $data
        ];

        return PDF::loadView('pdf.ultrasg2019',$view_data)->setPaper('a4', 'potrait')->setWarnings(false)->save($path_pdf.$data['full_barcode'].'.pdf');

    }

    protected function getOrder($last_order_id = null){

        $query = "SELECT o.id, 
                        o.invoice, 
                        o.customer_fullname AS purchaser_fullname, 
                        o.customer_email AS purchaser_email, 
                        o.customer_phone AS purchaser_phone,
                        oc.id AS order_customer_id, 
                        oc.fullname, 
                        oc.email, 
                        oc.customer_data,
                        o.currency, 
                        o.order_total, 
                        ot.temp_barcode, 
                        ot.full_barcode, 
                        oi.event_name, 
                        ev.name AS event_variant_name, 
                        oi.event_variant_id, 
                        oi.qty, 
                        oi.price, 
                        oi.total_price 
                FROM orders AS o
                LEFT JOIN order_items AS oi ON oi.order_id = o.id
                LEFT JOIN order_customers AS oc ON oc.order_item_id = oi.id 
                INNER JOIN order_tikets AS ot ON ot.order_customer_id = oc.id
                LEFT JOIN event_variants AS ev ON ev.id = oi.event_variant_id
                WHERE o.order_status_id = 3
                AND o.is_test = 0             
                AND oi.event_id IN (11,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,77,86,87,91,96,97,98,99) LIMIT 5";

        if($last_order_id != null && $last_order_id != ''){

            $query .= 'AND o.id >='.$last_order_id;

        }

        $data = DB::connection('mysql_cgx')->select($query);

        $orders = [];

        foreach($data as $row){

            $customer_data = json_decode($row->customer_data, true);
            $orders[$row->invoice]['id'] = $row->id;
            $orders[$row->invoice]['purchaser'] = array("fullname" => $row->purchaser_fullname, "email" => $row->purchaser_email, "phone" => $row->purchaser_phone);
            $orders[$row->invoice]['details'][] = array(
                "fullname" => $row->fullname,
                "email" => $row->email,
                "passport_no" => isset($customer_data['nric_passport_no']) ? $customer_data['nric_passport_no'] : '',
                "date_of_birth" => isset($customer_data['date_of_birth']) ? $customer_data['date_of_birth'] : '',
                "temp_barcode" => $row->temp_barcode,
                "full_barcode" => $row->full_barcode,
                "event_variant_name" => $row->event_variant_name,
                "event_variant_id" => $row->event_variant_id
            );
        }

        return $orders;
    }

    protected function glownet_ticket(){

        set_time_limit(0);
        ignore_user_abort(true);

        $GA_1DAY_SAT = 54870;
        $GA_1DAY_SUN = 54871;
        $GA_2DAY = 54872;
        $PGA_1DAY_SAT = 54873;
        $PGA_1DAY_SUN = 54874;
        $PGA_2DAY = 54875;

        $event_id = 932;
        $token = 'c7332045c8a46a7ba1620de2bb4cf04f';
        $api_endpoint = "https://glownet.com/api/v2/events/$event_id/tickets";

        //PGA 2 DAY - 54869 (glownet ticket_type_id)
        $ticket_type['73'] = $PGA_2DAY;
        $ticket_type['276'] = $PGA_2DAY;
        $ticket_type['277'] = $PGA_2DAY;
        $ticket_type['279'] = $PGA_2DAY;
        $ticket_type['307'] = $PGA_2DAY;
        $ticket_type['320'] = $PGA_2DAY;
        //ts
        $ticket_type['318'] = $PGA_2DAY;
        $ticket_type['319'] = $PGA_2DAY;
        $ticket_type['210'] = $PGA_2DAY;
        //flight and hotel 1
        $ticket_type['123'] = $PGA_2DAY;
        //comfort 1
        $ticket_type['131'] = $PGA_2DAY;
        //comfort 2
        $ticket_type['132'] = $PGA_2DAY;

        //GA 2 DAY - 54866 (glownet ticket_type_id)
        $ticket_type['72'] = $GA_2DAY;
        $ticket_type['240'] = $GA_2DAY;
        $ticket_type['274'] = $GA_2DAY;
        $ticket_type['275'] = $GA_2DAY;
        $ticket_type['278'] = $GA_2DAY;
        $ticket_type['306'] = $GA_2DAY;

        $ticket_type['315'] = $GA_2DAY;
        $ticket_type['316'] = $GA_2DAY;
        $ticket_type['317'] = $GA_2DAY;

        //GA 1 DAY SAT - 54864 (glownet ticket_type_id)
        $ticket_type['222'] = $GA_1DAY_SAT;

        //GA 1 DAY SUN - 54865 (glownet ticket_type_id)
        $ticket_type['223'] = $GA_1DAY_SUN;

        //PGA 1 DAY SAT - 54867 (glownet ticket_type_id)
        $ticket_type['224'] = $PGA_1DAY_SAT;

        //PGA 1 DAY SUN - 54868 (glownet ticket_type_id)
        $ticket_type['225'] = $PGA_1DAY_SUN;

        $cognitix = DB::table('cognitix')->select('last_order_id')->where('type','glownet')->first();

        $last_order_id = $cognitix->last_order_id;

        $orders = $this->getOrder($last_order_id);

        $headers = array(
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$token
        );

        foreach($orders as $invoice => $order){

            foreach($order['details'] as $detail){

                $body = array(

                    'ticket' => array(
                        'code' => $detail['full_barcode'],
                        'purchaser_email' => $detail['email'],
                        'ticket_type_id' => $ticket_type[$detail['event_variant_id']],
                        'purchaser_first_name' => $detail['fullname'],
                    )
                );

                $client = new \GuzzleHttp\Client();

                try{

                    $res = $client->post($api_endpoint, [ 'headers' => $headers, 'body' => json_encode($body), 'http_errors' => false ]);

                    DB::table('cognitix_log')->insert([
                        'order_id' =>  $order['id'],
                        'event_variant_id' => $detail['event_variant_id'],
                        'full_barcode' => $detail['full_barcode'],
                        'payload' => $res->getBody()
                    ]);

                }
                catch(Exception $e){

                    DB::table('cognitix_log')->insert([
                        'order_id' =>  NULL,
                        'event_variant_id' => NULL,
                        'full_barcode' => NULL,
                        'payload' => $res->getBody()
                    ]);
                }


            }

            DB::table('cognitix')->where('type','glownet')->update(['last_order_id' => $order['id']]);

        }

    }

    public function sendGridExample()
    {
        $path_pdf = public_path().'/images/uploads/pdf/';
//        dd($path_pdf);
        [
            $details = [
                'purchaser' => [
                    'fullname' => 'User name',
                    'email' => 'serTo@text.com',
                    ],
                'details' => [
                    [
                        'barcode'=>'1234553fyhukfy',
                        'fullname'=>'User name',
                        'email'=>'userTo@text.com',
                        'passportID'=>'4662730993',
                        'category'=>'PGA 2-Day Ticket (Tier 3)',
                        'invoice_id'=>'2233434-234'
                     ],
                    [
                        'barcode'=>'1234553',
                        'fullname'=>'User name2',
                        'email'=>'userTo@text.com',
                        'passportID'=>'46627309932',
                        'category'=>'PGA 2-Day Ticket (Tier 3)2',
                        'invoice_id'=>'2233434-234-2'
                     ],
                    [
                        'barcode'=>'1234553',
                        'fullname'=>'User name3',
                        'email'=>'userTo@text.com',
                        'passportID'=>'46627309933',
                        'category'=>'PGA 2-Day Ticket (Tier 3)3',
                        'invoice_id'=>'2233434-234-3'
                    ],            [
                        'barcode'=>'1234553',
                        'fullname'=>'User name4',
                        'email'=>'userTo@text.com',
                        'passportID'=>'46627309934',
                        'category'=>'PGA 2-Day Ticket (Tier 3)4',
                        'invoice_id'=>'2233434-234-2'
                         ],
                    [
                        'barcode'=>'1234553',
                        'fullname'=>'User name5',
                        'email'=>'userTo@text.com',
                        'passportID'=>'46627309935',
                        'category'=>'PGA 2-Day Ticket (Tier 3)5',
                        'invoice_id'=>'2233434-234-5'
                     ],
                ]
            ]
        ];
        for ($i = 0; $i <= 3; $i++) {
            $attachments[] = $this->sendGridAttachmentGenerate($details['details'][$i], $path_pdf);
        }

        $data = [
            'fromEmail' => 'userFrom@text.com',
            'fromName' => 'From Company',
            'toEmail' => $details['purchaser']['email'],
            'toName' => $details['purchaser']['fullname'],
            'subject' => 'Subject',
            'params' => $details['purchaser'],
            'attachments' => $attachments
        ];

        // add CC

        $data['cc'] = ['emailTo' =>'backupemails@airasiaredtix.com', 'toName' => 'Redtix'];

        return (new Sendgrid($data))->sendgrid();
    }

    public function sendGridAttachmentGenerate($detail, $path_pdf)
    {
        $detail['full_barcode']=$detail['barcode'];
        $qrcode = $this->ticketService->generateQRCode($detail['barcode'], 100,true);
        $detail['qrcode']='data:image/png;base64,'.$qrcode;
        $this->_pdf($detail);

        return $attachment = file_get_contents($path_pdf.$detail['full_barcode'].'.pdf');

    }
}
