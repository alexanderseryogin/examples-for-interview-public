<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;

use App\Models\Event;
use App\Models\Talent;

class TalentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     $talent = new Talent;
    //     $talent->name = $request->name;
    //     $talent->position = $request->position;
    //     $talent->company = $request->company;
    //     $talent->field = $request->field;

    //     $path = public_path().'/images/talents';

    //     if($request->file('img_path'))
    //     {
    //         $talent_img = $request->file('mobile');
    //         $talent_img_filename = str_random(4).$talent_img->getClientOriginalName();
    //         $talent_img->move($path, $talent_img_filename);
    //         $talent->img_path = '/images/talents'.'/'.$talent_img_filename;
    //     } 

    //     $talent->save();

    //     return Redirect::to(URL::previous())->with('successMessage', 'New talent created');
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $talent = Talent::findOrFail($id);
        $talent->name = $request->name;
        $talent->position = $request->position;
        $talent->company = $request->company;
        $talent->field = $request->field;

        $talent->save();

        return Redirect::to(URL::previous())->with('successMessage', 'Talent updated');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $talent_id)
    {
        $talent = Talent::findOrFail($id);
        $talent->delete();

        return redirect()->back()->with('successMessage', 'Talent deleted');
    }
}
