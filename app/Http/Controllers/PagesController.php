<?php

namespace App\Http\Controllers;

use App\Services\Package\PackageService;
use App\Models\Event;
use App\Models\EventCollection;
use App\Models\MainAnnouncement;
use App\Models\MainBanner;
use App\Models\Subscriber;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    protected $packageService;

    public function __construct(PackageService $packageService)
    {
        $this->packageService = $packageService;
    }

    public function index()
    {
        $path = resource_path('/json/homepage.json');
        $data = json_decode(file_get_contents($path), true);
        ksort($data['trending']['events']);
        ksort($data['genting']['events']);
        ksort($data['gtf']['events']);
        return view('pages/homepage', $data);
    }

    public function rwc2019()
    {
        $tickets = $this->packageService->getTicketsData();
        $packages = $this->packageService->getAllPackages();

        return view('rugby2019/master', compact('tickets', 'packages'));
    }

    public function adminIndex(Request $request)
    {

        $banners = MainBanner::get();
        $announcement = MainAnnouncement::first();
        $eventcollections = EventCollection::get();

        $data = [
            'banners'             => $banners,
            'announcement'        => $announcement,
            'eventcollections'    => $eventcollections,
            'showAdminNavigation' => true
        ];

        if ($request->has('action') && $request->input('action') === 'login') {
            $data['showLoginModal'] = true;
        }

        return view('admin.pages.homepage', $data);
    }

    //event detail page
    public function event($slug)
    {

        $event = Event::where('slug', $slug)->with('tickets', 'gallery', 'artistLineup', 'venue', 'category', 'company', 'infoBlocks', 'quotes')->first();
        // print json_encode($event, true);exit;
        // $banner = $event->images->where('type', 'hero-banner')->first();
        // $thumbnail = $event->images()->where('type', 'thumbnail')->first();
        // $seatplan = $event->images()->where('type', 'seat-plan')->get();
        // $announcement_img = $event->images()->where('type', 'announcement')->first();
        $gallery = $event->gallery;
        // $videos = $event->videos()->get();

        //process and group tickets
        $tickets = [];
        foreach($event['tickets'] as $ticket){
            $tickets[$ticket['category']][] = $ticket;
        }

        $data = [
            'event'            => $event,
            // 'tickets'           => $tickets,
            // 'thumbnail'        => $thumbnail,
            'gallery'          => $gallery,
            // 'seatplan'         => $seatplan,
            // 'announcement_img' => $announcement_img,
            // 'videos'           => $videos
        ];

        print json_encode($data, true); exit;

        if ($event->status == 1) {
            return view('pages.show', $data);
        }

        return view('errors.404');
    }

    public function eventList(Request $request)
    {
        $eventcollections = EventCollection::get();

        $filter_title = '%' . $request->title . '%';
        $filter_duration = $request->duration;
        $filter_category = $request->category;
        $filter_collection = $request->eventcollection;

        $query = Event::with('tickets', 'event_collection')->where('status', 1);

        if (!is_null($filter_title) && strlen($filter_title)) {
            $query = $query->where('title', 'LIKE', $filter_title);
        }
        if (!is_null($filter_duration) && strlen($filter_duration)) {
            $query = $query->where('start_date', $filter_duration);
        }

        if (!empty($filter_category)) {
            $query = $query->where('category', $filter_category);
        }

        if (!empty($filter_collection)) {

            if ($filter_collection == 'all') {
                $query = $query;
            } else {
                $query = $query->whereHas('event_collection', function ($q) use ($filter_collection) {
                    $q->where('name', $filter_collection);
                });
            }
        }

        $events = $query->orderBy('created_at', 'desc')->paginate(30);

        $data = [
            'events'           => $events,
            'eventcollections' => $eventcollections
        ];

        return view('pages.events', $data);
    }

    public function about()
    {
        return view('pages/about');
    }

    public function faq()
    {
        return view('pages/faq');
    }

    public function privacy()
    {
        return view('pages/privacy');
    }

    public function purchaseterms()
    {
        return view('pages.purchaseterms');
    }

    public function webterms()
    {
        return view('pages/webterms');
    }

    public function redicon()
    {
        return view('pages/redicon');
    }

    public function subscribe(Request $request)
    {
        $email = Subscriber::find($request->email);
        if (is_null($email)) {
            $subscriber = new Subscriber;
            $subscriber->email = $request->email;

            $subscriber->save();
        }

        return redirect()->back();
    }
}
