<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Transportation;
use Illuminate\Http\Request;
use App\Services\Package\IPackageService;
use App\Services\CognitixAPI;


class PackageController extends Controller
{
    //start ultrasingapure 2019
    public function flight()
    {
        return view('package.index',[
            'title'=>'Flight Package',
            'imgtop' =>'img12top',
            'type' =>'flight'
        ]);
    }

    public function hotel()
    {
        return view('package.index',[
            'title'=>'The Comfort Package',
            'imgtop' =>'img11top',
            'type' =>'hotel'
        ]);
    }

    public function flightHotel()
    {
        return view('package.index',[
            'title'=>'Flight + Hotel Package',
            'imgtop' =>'img10top',
            'type' =>'flight-hotel'
        ]);
    }
    //end ultrasingapure 2019
    //start Rugby2019
    private $package;
    private $cognitixAPI;

    public function __construct(CognitixAPI $api, IPackageService $IPackageService)
    {
        $this->package = $IPackageService;
        $this->cognitixAPI = $api;
    }

    public function PackageBuilder($package)
    {
        $cities = $this->package->getFlightsByPackage();
        $packInfo = $this->package->getPackageGeneralInfoByPack($package);

        $packageInfo = [
            'name' => $package,
            'flightNames' => $cities,
            'dt_start' => $packInfo->start_date,
            'dt_end' => $packInfo->end_date,
            'title' => $packInfo->name,
            'hotel_id' => $packInfo->hotel_id,
        ];
        return view('rugby2019.package.packagebuilder', compact('packageInfo'));
    }

    public function PackageBuilderGetData($package, $flight)
    {
        $packageInfo = $this->package->getPackageInfoByPackageAndFlight($package, $flight);
//        dd($packageInfo);
//        todo get package details
//        $packageInfo=[
//            'name'=>$package,
//            'title'=>'Opening Ceremony Package',
//            'flight'=>['original'=>[
//                            'cityAbbr'=>$flight,
//                            'dt'=>'2019-09-19 13:00',
//                            'dt2'=>'2019-09-19 13:00',
//                            'name'=>'Changi International Airport',
//                            'number'=>'QZ-234'
//                        ],
//                        'destination'=>[
//                            'cityAbbr'=>'HND',
//                            'dt'=>'2019-09-19 21:10',
//                            'dt2'=>'2019-09-19 21:10',
//                            'name'=>'Tokyo International Airport',
//                            'number'=>'KL-432'
//                        ],
//                        'price'=>3000,
//                    ],
//            'hotel'=>['name'=>'Hearton Hotel Higashishinagawa',
//                'address'=>'405 Funaya-cho, Nakagyo-Ku, Kyoto-shi, Kyoto, 604-0836, Japan',
//                'roomType'=>'Single',
//                'days'=>4,
//                'nights'=>3,
//                'picture'=>'singapore-2017-club-room-marina-bay.png',
//                'price'=>1200,
//            ],
//            'tickets'=>[
//                0=>[
//                'id'=>0,
//                'type'=>'D',
//                'date'=>'2019-09-20 19:45',
//                'price'=>350,
//                'participants'=>[
//                    0=>['name'=>'japan','flag'=>'japan.png'],
//                    1=>['name'=>'russia','flag'=>'russia.png'],
//                    ],
//                ],
//                1=>[
//                    'id'=>1,
//                    'type'=>'C',
//                    'date'=>'2019-09-21 18:32',
//                    'price'=>450,
//                    'participants'=>[
//                        0=>['name'=>'new zealand','flag'=>'new-zealand.png'],
//                        1=>['name'=>'south africa','flag'=>'south-africa.png'],
//                    ],
//                ],
//            ],
//        ];
        return response($packageInfo, 200);
    }

    /**
     * @param String $package
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function PackageDetail($package)
    {
        //$data = $this->package->getPackageInfoByPackageAndFlight($package, 'KUL'); //get by package and flight
        //$data = $this->package->getFlightsByPackage(); //get all origin
        $data = $this->package->getDetailPackageByPack($package); //get detail by package
        //$data = $this->package->getAllTickets(); //get all games

        //dd($data);

        return view('rugby2019.package.packagedetail', [
            'data' => collect($data),
            'package' => collect($package)
        ]);
    }

    public function PrepareDataToCognitixAPI(Request $request)
    {
        $res = $this->cognitixAPI->sendRequestToCognitix($request['data']);
        dd($res);
        return $res;
    }

    public function getFinalDestinationByTransportationId($transportation_id)
    {
        $endDestination = $this->package->getFinalDestinationByTransportationId($transportation_id);
    }

    public function checkOrGetFinalDestinationByCities($city_origin, $city_destination)
    {
        $data = $this->package->checkOrGetFinalDestinationByCities($city_origin, $city_destination);
        return $data;
    }
}
