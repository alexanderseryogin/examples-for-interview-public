<?php

namespace App\Http\Middleware;

use App\Admin\Services\UserService;
use Closure;
use Auth;

class LastActiveMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $userService = app()->make(UserService::class);
            $userService->updateLastActive();
        }

        return $next($request);
    }
}
