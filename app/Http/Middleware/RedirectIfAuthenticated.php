<?php

namespace App\Http\Middleware;

use App\Helpers\AuthHelper;
use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Class RedirectIfAuthenticated
 *
 * @package App\Http\Middleware
 */
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param  string|null              $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        switch ($guard) {
        case 'ultrasingapore':
          if (Auth::guard($guard)->check()) {
            return redirect()->route('ultrasingapore.add.passenger');
          }
          break;
        default:
            if (Auth::guard($guard)->check()) {
                if (AuthHelper::hasCmsAccess()) {
                    return redirect('/admin');
                }

                return redirect('/');
            }
          break;
      }
      return $next($request);
    }
}
